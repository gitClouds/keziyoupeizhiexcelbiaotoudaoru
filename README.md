可自由配置excel表头导入

#### 介绍
可以根据数据库表字段自由灵活在web页面配置需要导入对应的字段，自动对应数据将excel文件数据导入数据库，可根据excel文件表头创建数据库表，前端使用bootstrap、及bootstrap-table

1. 效果图：
1. ![输入图片说明](https://images.gitee.com/uploads/images/2020/0613/155903_183af01d_1550956.jpeg "1592035023(1).jpg")
1. 
sql基本任务调度功能脚本如下：

本导入功能首次使用需要在数据库创建存储表信息，在表中添加需要导入的数据库表，sql如下
-- Create table
create table USER_TABLE_INFO
(
  id         VARCHAR2(200) not null,
  table_name VARCHAR2(200),
  table_type VARCHAR2(200),
  comments   VARCHAR2(2000)
)
tablespace ZJFX
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the table
comment on table USER_TABLE_INFO
  is '用户表信息';
-- Add comments to the columns
comment on column USER_TABLE_INFO.id
  is 'ID';
comment on column USER_TABLE_INFO.table_name
  is '表名';
comment on column USER_TABLE_INFO.table_type
  is '类型 需要导入的表类型配置为1';
comment on column USER_TABLE_INFO.comments
  is '中文名（注释）';
-- Create/Recreate primary, unique and foreign key constraints
alter table USER_TABLE_INFO
  add constraint ID primary key (ID)
  using index
  tablespace ZJFX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

注意事项：0.配置表信息必须配置table_type字段值为'1'。
        1.如果表建有主键，则自动识别主键，并插入id。
        2.如果表中需要增加 录入人账号、录入人名称、本地入库时间、数据来源、有效性、导入批次 字段时，字段名可以任取，字段注释必须要且与前面列出的中文名一致。
        3.导入列表显示名称基于表字段注释，当没有注释时显示为字段名。
        4.页面配置表头时，首先下拉框选择要查询的表，加载出数据库表字段中文名（基于字段注释），然后选择需要导入的excel文件，确认读取文件后此时读取到excel表头中文名，
        配置需要导入到相应数据库表字段时，首先单击数据库字段中文名选中（再次单击取消）然后再单击excel表头对应字段（再次单击取消），然后确认导入即可。
        5.导入数据后支持全字段模糊匹配查询表数据，将匹配的数据全部列出。
----------------------------创建web建表配置表sql-----------------------------------
drop table FIELD_PROPERTY_INFO_TB cascade constraints;

/*==============================================================*/
/* Table: FIELD_PROPERTY_INFO_TB                                */
/*==============================================================*/
-- Create table
create table FIELD_PROPERTY_INFO_TB
(
  table_name       VARCHAR2(200),
  table_china_name VARCHAR2(200),
  own_tablespace   VARCHAR2(200),
  field_name       VARCHAR2(200),
  field_china_name VARCHAR2(200),
  field_type       VARCHAR2(200),
  field_lenth      VARCHAR2(200),
  isnull           VARCHAR2(200),
  is_pk            VARCHAR2(200),
  is_index         VARCHAR2(200),
  id               VARCHAR2(200) not null,
  field_order      VARCHAR2(200),
  field_kind       VARCHAR2(200),
  state            VARCHAR2(200),
  create_time      DATE
)
tablespace ZJFX
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the table
comment on table FIELD_PROPERTY_INFO_TB
  is '字段与属性信息表';
-- Add comments to the columns
comment on column FIELD_PROPERTY_INFO_TB.table_name
  is '表名';
comment on column FIELD_PROPERTY_INFO_TB.table_china_name
  is '表中文名';
comment on column FIELD_PROPERTY_INFO_TB.own_tablespace
  is '所属表空间';
comment on column FIELD_PROPERTY_INFO_TB.field_name
  is '字段名';
comment on column FIELD_PROPERTY_INFO_TB.field_china_name
  is '字段中文名';
comment on column FIELD_PROPERTY_INFO_TB.field_type
  is '字段类型';
comment on column FIELD_PROPERTY_INFO_TB.field_lenth
  is '字段长度';
comment on column FIELD_PROPERTY_INFO_TB.isnull
  is '是否为空';
comment on column FIELD_PROPERTY_INFO_TB.is_pk
  is '是否为主键';
comment on column FIELD_PROPERTY_INFO_TB.is_index
  is '是否为索引';
comment on column FIELD_PROPERTY_INFO_TB.id
  is '主键';
comment on column FIELD_PROPERTY_INFO_TB.field_order
  is '字段顺序';
comment on column FIELD_PROPERTY_INFO_TB.field_kind
  is '字段种类';
comment on column FIELD_PROPERTY_INFO_TB.state
  is '状态';
comment on column FIELD_PROPERTY_INFO_TB.create_time
  is '创建时间';
-- Create/Recreate indexes
create unique index FIELD_PROPERTY_INFO_TB_PK_ID on FIELD_PROPERTY_INFO_TB (ID)
  tablespace ZJFX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index SUOYIN on FIELD_PROPERTY_INFO_TB (FIELD_NAME)
  tablespace ZJFX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints
alter table FIELD_PROPERTY_INFO_TB
  add constraint FIELD_PROPERTY_INFO_TB_ID primary key (ID);


drop table SQL_SCRIPT_INFO_TAB cascade constraints;

-- Create table
create table SQL_SCRIPT_INFO_TAB
(
  sql_statement       VARCHAR2(4000),
  task_batch          VARCHAR2(200),
  statement_order     VARCHAR2(200),
  is_open_task        VARCHAR2(200),
  task_schedule       VARCHAR2(200),
  success_num         VARCHAR2(20),
  fail_cause          VARCHAR2(2000),
  task_interval       VARCHAR2(20),
  task_execution_time DATE,
  create_time         DATE,
  task_type           VARCHAR2(200),
  task_name           VARCHAR2(200),
  task_type_name      VARCHAR2(200),
  statement_rype      VARCHAR2(200),
  table_name          VARCHAR2(200),
  table_china_name    VARCHAR2(200),
  state               VARCHAR2(200),
  id                  VARCHAR2(200) not null
)
tablespace ZJFX
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the table
comment on table SQL_SCRIPT_INFO_TAB
  is 'SQL脚本信息表';
-- Add comments to the columns
comment on column SQL_SCRIPT_INFO_TAB.sql_statement
  is 'SQL语句';
comment on column SQL_SCRIPT_INFO_TAB.task_batch
  is '任务批次';
comment on column SQL_SCRIPT_INFO_TAB.statement_order
  is '语句排序';
comment on column SQL_SCRIPT_INFO_TAB.is_open_task
  is '是否开启任务';
comment on column SQL_SCRIPT_INFO_TAB.task_schedule
  is '任务进度';
comment on column SQL_SCRIPT_INFO_TAB.success_num
  is '成功次数';
comment on column SQL_SCRIPT_INFO_TAB.fail_cause
  is '失败原因';
comment on column SQL_SCRIPT_INFO_TAB.task_interval
  is '任务间隔时间(分钟)';
comment on column SQL_SCRIPT_INFO_TAB.task_execution_time
  is '任务执行时间';
comment on column SQL_SCRIPT_INFO_TAB.create_time
  is '创建时间';
comment on column SQL_SCRIPT_INFO_TAB.task_type
  is '任务种类';
comment on column SQL_SCRIPT_INFO_TAB.task_name
  is '任务名称';
comment on column SQL_SCRIPT_INFO_TAB.task_type_name
  is '任务种类名称';
comment on column SQL_SCRIPT_INFO_TAB.statement_rype
  is '语句类型';
comment on column SQL_SCRIPT_INFO_TAB.table_name
  is '表名';
comment on column SQL_SCRIPT_INFO_TAB.table_china_name
  is '表中文名';
comment on column SQL_SCRIPT_INFO_TAB.state
  is '状态';
comment on column SQL_SCRIPT_INFO_TAB.id
  is '主键';
-- Create/Recreate primary, unique and foreign key constraints
alter table SQL_SCRIPT_INFO_TAB
  add constraint PK_SQL_SCRIPT_INFO_TAB primary key (ID)
  using index
  tablespace ZJFX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );




drop table FIELD_PROPERTY_DIC_TAB cascade constraints;

-- Create table
create table FIELD_PROPERTY_DIC_TAB
(
  id          VARCHAR2(200),
  dic_type    VARCHAR2(200),
  dic_name    VARCHAR2(200),
  dic_value   VARCHAR2(1000),
  state       CHAR(10),
  create_time DATE
)
tablespace ZJFX
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the table
comment on table FIELD_PROPERTY_DIC_TAB
  is '字段属性字典表';
-- Add comments to the columns
comment on column FIELD_PROPERTY_DIC_TAB.id
  is '主键';
comment on column FIELD_PROPERTY_DIC_TAB.dic_type
  is '字典类型';
comment on column FIELD_PROPERTY_DIC_TAB.dic_name
  is '字典名称';
comment on column FIELD_PROPERTY_DIC_TAB.dic_value
  is '字典值';
comment on column FIELD_PROPERTY_DIC_TAB.state
  is '状态';
comment on column FIELD_PROPERTY_DIC_TAB.create_time
  is '创建时间';



 ---------------字典表数据---------------------
 insert into field_property_dic_tab (ID, DIC_TYPE, DIC_NAME, DIC_VALUE, STATE, CREATE_TIME)
 values ('414e19b8be9a4bb89c777dc2393ae5ae', '1', 'NUMBER', 'NUMBER', '1', to_date('28-05-2020 18:19:31', 'dd-mm-yyyy hh24:mi:ss'));

 insert into field_property_dic_tab (ID, DIC_TYPE, DIC_NAME, DIC_VALUE, STATE, CREATE_TIME)
 values ('6aca63a7e869428099615f902f38c8ae', '1', 'NCHAR', 'NCHAR', '1', null);

 insert into field_property_dic_tab (ID, DIC_TYPE, DIC_NAME, DIC_VALUE, STATE, CREATE_TIME)
 values ('0d089ea0ca7349fba789c9ba2ff29004', '1', 'CHAR', 'CHAR', '1', null);

 insert into field_property_dic_tab (ID, DIC_TYPE, DIC_NAME, DIC_VALUE, STATE, CREATE_TIME)
 values ('441e6040e6c64e6baf1711a9d2c51b20', '1', 'VARCHAR2', 'VARCHAR2', '1', null);

 insert into field_property_dic_tab (ID, DIC_TYPE, DIC_NAME, DIC_VALUE, STATE, CREATE_TIME)
 values ('2635cee5b79e499aa16e2150ae547420', '1', 'NVARCHAR2', 'NVARCHAR2', '1', null);

 insert into field_property_dic_tab (ID, DIC_TYPE, DIC_NAME, DIC_VALUE, STATE, CREATE_TIME)
 values ('4c6753da83e643efae53a661b92725bc', '1', 'DATE', 'DATE', '1', null);

 insert into field_property_dic_tab (ID, DIC_TYPE, DIC_NAME, DIC_VALUE, STATE, CREATE_TIME)
 values ('e26d427f452a47b9aeaeeef6f4b0cd56', '1', 'TIMESTAMP', 'TIMESTAMP', '1', null);

 insert into field_property_dic_tab (ID, DIC_TYPE, DIC_NAME, DIC_VALUE, STATE, CREATE_TIME)
 values ('384ff71b5fe149669968d8538e8ada0e', '2', '不能为空', 'not null', '1', to_date('26-05-2020 09:01:28', 'dd-mm-yyyy hh24:mi:ss'));

 insert into field_property_dic_tab (ID, DIC_TYPE, DIC_NAME, DIC_VALUE, STATE, CREATE_TIME)
 values ('c025885b3056492c8ab1186b2a69bc39', '3', 'ZJFX', 'ZJFX', '1', to_date('28-05-2020 10:07:55', 'dd-mm-yyyy hh24:mi:ss'));

 --------用户表信息配置-----------------------------------

 insert into user_table_info (ID, TABLE_NAME, TABLE_TYPE, COMMENTS)
 values ('45ad4be1c28e5800yb752637e3efae43', 'USER_TABLES', '6', '当前用户表名信息');

 insert into user_table_info (ID, TABLE_NAME, TABLE_TYPE, COMMENTS)
 values ('5', 'FIELD_PROPERTY_INFO_TB', '3', '字段与属性信息表');

 insert into user_table_info (ID, TABLE_NAME, TABLE_TYPE, COMMENTS)
 values ('7', 'user_tab_comments', '6', '当前用户表信息字典');

 insert into user_table_info (ID, TABLE_NAME, TABLE_TYPE, COMMENTS)
 values ('10', 'SQL_SCRIPT_INFO_TAB', '4', 'SQL脚本信息表');

 insert into user_table_info (ID, TABLE_NAME, TABLE_TYPE, COMMENTS)
 values ('11', 'FIELD_PROPERTY_DIC_TAB', '1', '字段属性字典表');

 insert into user_table_info (ID, TABLE_NAME, TABLE_TYPE, COMMENTS)
 values ('3', 'USER_TABLE_INFO', '5', '存储信息');

----创建表注意事项：
    1.根据excel读取表头创建表时，确认读取后列表将展示出所有表头字段的简称、中文字段名、表名简称、中文表名等信息，然后
    在保存数据之前，选择所要的表空间，及根据需求修改表字段名后保存数据，再到表选择下拉框选中所保存的表名，然后点击生成表
    按钮即可在oracle数据库中创建表。
    2.创建表和根据excel创建表时 表名、表中文名、表空间只填写第一行的就可以了，保存后会填充全字段。
------定时执行sql脚本任务的使用
     定时任务使用线程池执行任务，任务批次为不同的任务，按批次顺序执行，每个批次包含多个sql任务，按照sql语句排序执行，
     有多少个批次的任务则开辟多少条线程，每个线程执行一种任务直至这个任务线上所有脚本任务按顺序执行结束后则关闭线程池，当
     某个任务执行超过30分分钟后则立即关闭该线程，然后重新开始任务，执行任务间隔时间可手动设置分钟数，在填写任务列表时表格批次和
     每个批次下的语句顺序按照正常顺序填写，是否开启任务中0为不开启1为开启，任务进度中0代表任务还未进行，1代表任务已经开始执行，
     2代表任务执行成功，3代表任务执行失败。任务执行失败后可以查看线管失败原因，修改sql脚本再重新保存数据，然后修改任务进度为0，
     会自动根据任务间隔时间继续执行任务，区分任务时可以填写任务种类名称和任务批次一致。


