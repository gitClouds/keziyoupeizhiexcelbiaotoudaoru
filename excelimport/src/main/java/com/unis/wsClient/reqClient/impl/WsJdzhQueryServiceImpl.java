package com.unis.wsClient.reqClient.impl;

import com.unis.model.jdzh.*;
import com.unis.server.WsJdzhQueryService;
import com.unis.wsClient.bean.*;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("WsJdzhQueryService")
public class WsJdzhQueryServiceImpl implements WsJdzhQueryService {
    @Value("${reqJdzhQuery.reqJdzhQueryUrl}")
    private String reqUrl;

    @Override
    public WSsdryxx querySdryByString(List<String> queryStrings, String sendcode) {
        JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
        factory.setServiceClass(WsJdzhQueryService.class);
        factory.setAddress(reqUrl);
        WsJdzhQueryService service = (WsJdzhQueryService) factory
                .create();
        Client client = ClientProxy.getClient (service);
        HTTPConduit http = (HTTPConduit) client.getConduit();
        HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
        httpClientPolicy.setConnectionTimeout(120000);
        httpClientPolicy.setAllowChunking(false);
        httpClientPolicy.setReceiveTimeout(120000);
        http.setClient(httpClientPolicy);
        return service.querySdryByString(queryStrings,sendcode);
    }

    @Override
    public WSxdryxx queryXdryByString(List<String> queryStrings, String sendcode) {
        JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
        factory.setServiceClass(WsJdzhQueryService.class);
        factory.setAddress(reqUrl);
        WsJdzhQueryService service = (WsJdzhQueryService) factory
                .create();
        Client client = ClientProxy.getClient (service);
        HTTPConduit http = (HTTPConduit) client.getConduit();
        HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
        httpClientPolicy.setConnectionTimeout(120000);
        httpClientPolicy.setAllowChunking(false);
        httpClientPolicy.setReceiveTimeout(120000);
        http.setClient(httpClientPolicy);
        return service.queryXdryByString(queryStrings,sendcode);
    }


    /*public static void main(String[] args) {
        JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
        factory.setServiceClass(WsJdzhQueryService.class);
        factory.setAddress("http://localhost:8087/jdzhJk/webservice/WsJdzhQueryService?wsdl");
        WsJdzhQueryService service = (WsJdzhQueryService) factory
                .create();
        List<String> a = new ArrayList<String>();
        a.add("1");
        WSxdryxx xds = service.queryXdryByString(a,"440400");
        List<WSTbXdryJbxx> wsTbXdryJbxxList = xds.getWsTbXdryJbxxs();
        List<WSTbXdryChxx> wsTbXdryChxxes = xds.getwChxxs();
        for (WSTbXdryJbxx as:wsTbXdryJbxxList) {
            TbXdryJbxx tbXdryJbxx = new TbXdryJbxx();
            BeanUtils.copyProperties(as,tbXdryJbxx);
            System.out.println(tbXdryJbxx.getLrsj());
            System.out.println(tbXdryJbxx.getCsrq());
        }
        for (WSTbXdryChxx aas:wsTbXdryChxxes) {
            TbXdryChxx tbXdryJbxx = new TbXdryChxx();
            BeanUtils.copyProperties(aas,tbXdryJbxx);
            System.out.println(tbXdryJbxx.getLrsj());
        }
        WSsdryxx sds = service.querySdryByString(a,"440400");
        List<WSTbDpajSdryJbxx> wsTbDpajSdryJbxxes = sds.getwSdryJbxxs();
        List<WSTbDpajSdryChxx> wSdryChxxs = sds.getwSdryChxxs();
        List<WSTbDpajJbxx> wsTbDpajJbxxs = sds.getWsTbDpajJbxxs();
        TbDpajSdryJbxx tbDpajSdryJbxx = new TbDpajSdryJbxx();
        TbDpajSdryChxx tbDpajSdryChxx = new TbDpajSdryChxx();
        TbDpajJbxx tbDpajJbxx = new TbDpajJbxx();
        BeanUtils.copyProperties(wsTbDpajSdryJbxxes.get(0),tbDpajSdryJbxx);
        System.out.println(tbDpajSdryJbxx.getCsrq());
        BeanUtils.copyProperties(wSdryChxxs.get(0),tbDpajSdryChxx);
        System.out.println(tbDpajSdryChxx.getDjrq());
        BeanUtils.copyProperties(wsTbDpajJbxxs.get(0),tbDpajJbxx);
        System.out.println(tbDpajJbxx.getFxsj());
    }*/
}
