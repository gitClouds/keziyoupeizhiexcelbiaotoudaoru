package com.unis.wsClient.bean;

import java.io.Serializable;
import java.util.List;


public class WSxdryxx implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String msg;
	private List<WSTbXdryJbxx> wsTbXdryJbxxs;
	private List<WSTbXdryChxx> wChxxs;
	
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public List<WSTbXdryJbxx> getWsTbXdryJbxxs() {
		return wsTbXdryJbxxs;
	}
	public void setWsTbXdryJbxxs(List<WSTbXdryJbxx> wsTbXdryJbxxs) {
		this.wsTbXdryJbxxs = wsTbXdryJbxxs;
	}
	public List<WSTbXdryChxx> getwChxxs() {
		return wChxxs;
	}
	public void setwChxxs(List<WSTbXdryChxx> wChxxs) {
		this.wChxxs = wChxxs;
	}
}
