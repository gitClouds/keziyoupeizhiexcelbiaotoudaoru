package com.unis.wsClient.bean;

import java.io.Serializable;
import java.util.List;


public class WSsdryxx implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String msg;
	private List<WSTbDpajSdryJbxx> wSdryJbxxs;
	private List<WSTbDpajSdryChxx> wSdryChxxs;
	private List<WSTbDpajJbxx> wsTbDpajJbxxs;
	
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public List<WSTbDpajSdryJbxx> getwSdryJbxxs() {
		return wSdryJbxxs;
	}
	public void setwSdryJbxxs(List<WSTbDpajSdryJbxx> wSdryJbxxs) {
		this.wSdryJbxxs = wSdryJbxxs;
	}
	public List<WSTbDpajSdryChxx> getwSdryChxxs() {
		return wSdryChxxs;
	}
	public void setwSdryChxxs(List<WSTbDpajSdryChxx> wSdryChxxs) {
		this.wSdryChxxs = wSdryChxxs;
	}
	public List<WSTbDpajJbxx> getWsTbDpajJbxxs() {
		return wsTbDpajJbxxs;
	}
	public void setWsTbDpajJbxxs(List<WSTbDpajJbxx> wsTbDpajJbxxs) {
		this.wsTbDpajJbxxs = wsTbDpajJbxxs;
	}
}
