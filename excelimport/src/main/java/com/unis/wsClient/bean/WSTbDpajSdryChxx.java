/*************************************************************************
 Copyright (C) Unpublished Unis Software, Inc. All rights reserved.
 Unis Software, Inc., Confidential and Proprietary.

 This software is subject to copyright protection
 under the laws of the Public of China and other countries.

 Unless otherwise explicitly stated, this software is provided
 by Unis "AS IS".
 *************************************************************************/
package com.unis.wsClient.bean;

import java.io.Serializable;
import java.util.Date;


/**
 * 毒品案件涉毒人员查获信息
 *
 * @author Administrator
 * @version 1.0
 * @since 2020-03-10
 */
public class WSTbDpajSdryChxx implements Serializable {

    private static final long serialVersionUID = 1L;

    /****************************************
     * Basic fields
     ****************************************/
    /**
     * 记录编号--DExxxxx--编号规则为“XXXXXXXXXXXX（公安机关机构代码）+YYYYMM（年月）+XXXXXXXXXXXXXX（顺序号）”                         : varchar
     */
    private String jlbh;

    /**
     * 立案编号--DE00092--编号规则为“A+XXXXXXXXXXXX（公安机关机构代码）+YYYYMM（年月）+XXXX（顺序号）”                         : varchar
     */
    private String ajbh;

    /**
     * 涉毒人员编号--DE00038--编号规则为：R+XXXXXXXXXXXX(公安机关机构代码)+XXXXXX(年月)+XXXX(顺序号)              : varchar
     */
    private String xyrbh;

    /**
     * 填报单位名称--DE00065--: varchar
     */
    private String tbdw_dwmc;

    /**
     * 登记人--DE00002--: varchar
     */
    private String djr_xm;

    /**
     * 登记日期--DE00554--: timestamp
     */
    private Date djrq;

    /**
     * 涉毒类型--DExxxxx--: varchar
     */
    private String sdlx;

    /**
     * 查获日期--DE00554--: timestamp
     */
    private Date chrq;

    /**
     * 查获地点--DE00768--: varchar
     */
    private String ddmc;

    /**
     * 活动区域--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码: varchar
     */
    private String hdqh_xzqh;

    /**
     * 查获单位--DE00065--: varchar
     */
    private String chdw_dwmc;

    /**
     * 处置情况--DExxxxx--: varchar
     */
    private String czqk;

    /**
     * 查获地区--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码: varchar
     */
    private String chd_xzqh;

    /**
     * 处置日期--DE00554--: timestamp
     */
    private Date czrq;

    /**
     * 违法犯罪事实--DE00100--: varchar
     */
    private String jyaq;

    /**
     * 银行卡发卡银行--DE00243--: varchar
     */
    private String khyhmc;

    /**
     * 银行卡号--DExxxxx--: varchar
     */
    private String xykh;

    /**
     * 银行账号--DE00241--: varchar
     */
    private String yhzh;

    /**
     * 车牌号--DE00307--: varchar
     */
    private String jdchphm;

    /**
     * 是否吸毒--DE00838--采用GA/T XXXXX-XXXX《吸毒记录核查结果代码》: varchar
     */
    private String xdjlhcjgdm;

    /**
     * 在逃人员编号--DExxxxx、DE00618--: varchar
     */
    private String ztrybh;

    /**
     * 立案地--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码: varchar
     */
    private String lad_xzqh;

    /**
     * 上网追逃日期--DE00554--: timestamp
     */
    private Date swzjrq;

    /**
     * 抓获地--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码: varchar
     */
    private String zhd_xzqh;

    /**
     * 通缉级别--DE00232--采用GA240.54-2003《通缉级别代码》: varchar
     */
    private String tjjbdm;

    /**
     * 抓获方式--DE01017--采用 GA/T XXX《抓获方式代码》: varchar
     */
    private String zhfsdm;

    /**
     * 抓获线索来源--DE00170--采用GA 332.9 《禁毒信息管理代码 第9部分:线索提供方式代码》: varchar
     */
    private String xstgfsdm;

    /**
     * 抓获经过--DE01012--: varchar
     */
    private String zcxwnrms;

    /**
     * 户籍地--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码: varchar
     */
    private String hjd_xzqh;

    /**
     * 是否逃跑--DExxxxx--: varchar
     */
    private String sftp;

    /**
     * 审判后执行情况--DExxxxx--: varchar
     */
    private String sphzxqk;

    /**
     * 移交地区--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码: varchar
     */
    private String yjd_xzqh;

    /**
     * 结伙作案中作用--DExxxxx--: varchar
     */
    private String jhzazzy;

    /**
     * 是/否特殊人群--DExxxxx--: varchar
     */
    private String sftsrq;

    /**
     * 是/否高危预警人员--DExxxxx--: varchar
     */
    private String sfgwyjry;

    /**
     * 逮捕证号--DExxxxx--: varchar
     */
    private String dbzh;

    /**
     * 查获单位代码: varchar
     */
    private String chdwdm;

    /**
     * 填报单位: varchar
     */
    private String tbdw_dwdm;

    /**
     * 刑期-年: decimal
     */
    private Integer xqn;

    /**
     * 刑期-月: decimal
     */
    private Integer xqy;

    /**
     * 执行截止日期: timestamp
     */
    private Date zxqzrq;

    /**
     * 更新时间: timestamp
     */
    private Date gxsj;

    /**
     * 有效性: decimal
     */
    private Integer yxx;

    /**
     * 更新时间戳--DEXXXXX--: varchar
     */
    private String gxsjc;

    /**
     * 修改部署单位--DEXXXXX--: varchar
     */
    private String xgbsdw;

    /**
     * 法律文书编号: varchar
     */
    private String flwsbh;

    /**
     * 是否主犯: varchar
     */
    private String sfzf;

    /**
     * 是否在逃: varchar
     */
    private String sfzt;

    /**
     * 注释: varchar
     */
    private String sftsrqlx;

    /**
     * 注释: varchar
     */
    private String sfjhgk;

    /**
     * 注释: timestamp
     */
    private Date zhsj;

    /**
     * 注释: varchar
     */
    private String zhd;

    /**
     * 注释: varchar
     */
    private String zhd_xxdq;

    /**
     * 注释: varchar
     */
    private String zhd_xxdz;

    /**
     * 注释: varchar
     */
    private String fzqk;

    /**
     * 注释: varchar
     */
    private String fzlx;

    /**
     * 操作标识: varchar
     */
    private String czbs;

    /**
     * 注释: varchar
     */
    private String ryly;

    /**
     * 传输状态。0:未传输。1:已传输（新老系统数据传输）: varchar
     */
    private String cszt;

    /**
     * 注释: varchar
     */
    private String gjdm;

    /**
     * 注释: varchar
     */
    private String sdryjlbh;

    /**
     * 注释: varchar
     */
    private String chdwxc;

    /**
     * 旧系统特殊人群代码: varchar
     */
    private String sftsrq_old;

    /**
     * 注释: varchar
     */
    private String sjly;


    /****************************************
     * JavaBean setters & getters
     ****************************************/
    public String getJlbh() {
        return jlbh;
    }

    public void setJlbh(String jlbh) {
        this.jlbh = jlbh;
    }

    public String getAjbh() {
        return ajbh;
    }

    public void setAjbh(String ajbh) {
        this.ajbh = ajbh;
    }

    public String getXyrbh() {
        return xyrbh;
    }

    public void setXyrbh(String xyrbh) {
        this.xyrbh = xyrbh;
    }

    public String getTbdw_dwmc() {
        return tbdw_dwmc;
    }

    public void setTbdw_dwmc(String tbdw_dwmc) {
        this.tbdw_dwmc = tbdw_dwmc;
    }

    public String getDjr_xm() {
        return djr_xm;
    }

    public void setDjr_xm(String djr_xm) {
        this.djr_xm = djr_xm;
    }

    public Date getDjrq() {
        return djrq;
    }

    public void setDjrq(Date djrq) {
        this.djrq = djrq;
    }

    public String getSdlx() {
        return sdlx;
    }

    public void setSdlx(String sdlx) {
        this.sdlx = sdlx;
    }

    public Date getChrq() {
        return chrq;
    }

    public void setChrq(Date chrq) {
        this.chrq = chrq;
    }

    public String getDdmc() {
        return ddmc;
    }

    public void setDdmc(String ddmc) {
        this.ddmc = ddmc;
    }

    public String getHdqh_xzqh() {
        return hdqh_xzqh;
    }

    public void setHdqh_xzqh(String hdqh_xzqh) {
        this.hdqh_xzqh = hdqh_xzqh;
    }

    public String getChdw_dwmc() {
        return chdw_dwmc;
    }

    public void setChdw_dwmc(String chdw_dwmc) {
        this.chdw_dwmc = chdw_dwmc;
    }

    public String getCzqk() {
        return czqk;
    }

    public void setCzqk(String czqk) {
        this.czqk = czqk;
    }

    public String getChd_xzqh() {
        return chd_xzqh;
    }

    public void setChd_xzqh(String chd_xzqh) {
        this.chd_xzqh = chd_xzqh;
    }

    public Date getCzrq() {
        return czrq;
    }

    public void setCzrq(Date czrq) {
        this.czrq = czrq;
    }

    public String getJyaq() {
        return jyaq;
    }

    public void setJyaq(String jyaq) {
        this.jyaq = jyaq;
    }

    public String getKhyhmc() {
        return khyhmc;
    }

    public void setKhyhmc(String khyhmc) {
        this.khyhmc = khyhmc;
    }

    public String getXykh() {
        return xykh;
    }

    public void setXykh(String xykh) {
        this.xykh = xykh;
    }

    public String getYhzh() {
        return yhzh;
    }

    public void setYhzh(String yhzh) {
        this.yhzh = yhzh;
    }

    public String getJdchphm() {
        return jdchphm;
    }

    public void setJdchphm(String jdchphm) {
        this.jdchphm = jdchphm;
    }

    public String getXdjlhcjgdm() {
        return xdjlhcjgdm;
    }

    public void setXdjlhcjgdm(String xdjlhcjgdm) {
        this.xdjlhcjgdm = xdjlhcjgdm;
    }

    public String getZtrybh() {
        return ztrybh;
    }

    public void setZtrybh(String ztrybh) {
        this.ztrybh = ztrybh;
    }

    public String getLad_xzqh() {
        return lad_xzqh;
    }

    public void setLad_xzqh(String lad_xzqh) {
        this.lad_xzqh = lad_xzqh;
    }

    public Date getSwzjrq() {
        return swzjrq;
    }

    public void setSwzjrq(Date swzjrq) {
        this.swzjrq = swzjrq;
    }

    public String getZhd_xzqh() {
        return zhd_xzqh;
    }

    public void setZhd_xzqh(String zhd_xzqh) {
        this.zhd_xzqh = zhd_xzqh;
    }

    public String getTjjbdm() {
        return tjjbdm;
    }

    public void setTjjbdm(String tjjbdm) {
        this.tjjbdm = tjjbdm;
    }

    public String getZhfsdm() {
        return zhfsdm;
    }

    public void setZhfsdm(String zhfsdm) {
        this.zhfsdm = zhfsdm;
    }

    public String getXstgfsdm() {
        return xstgfsdm;
    }

    public void setXstgfsdm(String xstgfsdm) {
        this.xstgfsdm = xstgfsdm;
    }

    public String getZcxwnrms() {
        return zcxwnrms;
    }

    public void setZcxwnrms(String zcxwnrms) {
        this.zcxwnrms = zcxwnrms;
    }

    public String getHjd_xzqh() {
        return hjd_xzqh;
    }

    public void setHjd_xzqh(String hjd_xzqh) {
        this.hjd_xzqh = hjd_xzqh;
    }

    public String getSftp() {
        return sftp;
    }

    public void setSftp(String sftp) {
        this.sftp = sftp;
    }

    public String getSphzxqk() {
        return sphzxqk;
    }

    public void setSphzxqk(String sphzxqk) {
        this.sphzxqk = sphzxqk;
    }

    public String getYjd_xzqh() {
        return yjd_xzqh;
    }

    public void setYjd_xzqh(String yjd_xzqh) {
        this.yjd_xzqh = yjd_xzqh;
    }

    public String getJhzazzy() {
        return jhzazzy;
    }

    public void setJhzazzy(String jhzazzy) {
        this.jhzazzy = jhzazzy;
    }

    public String getSftsrq() {
        return sftsrq;
    }

    public void setSftsrq(String sftsrq) {
        this.sftsrq = sftsrq;
    }

    public String getSfgwyjry() {
        return sfgwyjry;
    }

    public void setSfgwyjry(String sfgwyjry) {
        this.sfgwyjry = sfgwyjry;
    }

    public String getDbzh() {
        return dbzh;
    }

    public void setDbzh(String dbzh) {
        this.dbzh = dbzh;
    }

    public String getChdwdm() {
        return chdwdm;
    }

    public void setChdwdm(String chdwdm) {
        this.chdwdm = chdwdm;
    }

    public String getTbdw_dwdm() {
        return tbdw_dwdm;
    }

    public void setTbdw_dwdm(String tbdw_dwdm) {
        this.tbdw_dwdm = tbdw_dwdm;
    }

    public Integer getXqn() {
        return xqn;
    }

    public void setXqn(Integer xqn) {
        this.xqn = xqn;
    }

    public Integer getXqy() {
        return xqy;
    }

    public void setXqy(Integer xqy) {
        this.xqy = xqy;
    }

    public Date getZxqzrq() {
        return zxqzrq;
    }

    public void setZxqzrq(Date zxqzrq) {
        this.zxqzrq = zxqzrq;
    }

    public Date getGxsj() {
        return gxsj;
    }

    public void setGxsj(Date gxsj) {
        this.gxsj = gxsj;
    }

    public Integer getYxx() {
        return yxx;
    }

    public void setYxx(Integer yxx) {
        this.yxx = yxx;
    }

    public String getGxsjc() {
        return gxsjc;
    }

    public void setGxsjc(String gxsjc) {
        this.gxsjc = gxsjc;
    }

    public String getXgbsdw() {
        return xgbsdw;
    }

    public void setXgbsdw(String xgbsdw) {
        this.xgbsdw = xgbsdw;
    }

    public String getFlwsbh() {
        return flwsbh;
    }

    public void setFlwsbh(String flwsbh) {
        this.flwsbh = flwsbh;
    }

    public String getSfzf() {
        return sfzf;
    }

    public void setSfzf(String sfzf) {
        this.sfzf = sfzf;
    }

    public String getSfzt() {
        return sfzt;
    }

    public void setSfzt(String sfzt) {
        this.sfzt = sfzt;
    }

    public String getSftsrqlx() {
        return sftsrqlx;
    }

    public void setSftsrqlx(String sftsrqlx) {
        this.sftsrqlx = sftsrqlx;
    }

    public String getSfjhgk() {
        return sfjhgk;
    }

    public void setSfjhgk(String sfjhgk) {
        this.sfjhgk = sfjhgk;
    }

    public Date getZhsj() {
        return zhsj;
    }

    public void setZhsj(Date zhsj) {
        this.zhsj = zhsj;
    }

    public String getZhd() {
        return zhd;
    }

    public void setZhd(String zhd) {
        this.zhd = zhd;
    }

    public String getZhd_xxdq() {
        return zhd_xxdq;
    }

    public void setZhd_xxdq(String zhd_xxdq) {
        this.zhd_xxdq = zhd_xxdq;
    }

    public String getZhd_xxdz() {
        return zhd_xxdz;
    }

    public void setZhd_xxdz(String zhd_xxdz) {
        this.zhd_xxdz = zhd_xxdz;
    }

    public String getFzqk() {
        return fzqk;
    }

    public void setFzqk(String fzqk) {
        this.fzqk = fzqk;
    }

    public String getFzlx() {
        return fzlx;
    }

    public void setFzlx(String fzlx) {
        this.fzlx = fzlx;
    }

    public String getCzbs() {
        return czbs;
    }

    public void setCzbs(String czbs) {
        this.czbs = czbs;
    }

    public String getRyly() {
        return ryly;
    }

    public void setRyly(String ryly) {
        this.ryly = ryly;
    }

    public String getCszt() {
        return cszt;
    }

    public void setCszt(String cszt) {
        this.cszt = cszt;
    }

    public String getGjdm() {
        return gjdm;
    }

    public void setGjdm(String gjdm) {
        this.gjdm = gjdm;
    }

    public String getSdryjlbh() {
        return sdryjlbh;
    }

    public void setSdryjlbh(String sdryjlbh) {
        this.sdryjlbh = sdryjlbh;
    }

    public String getChdwxc() {
        return chdwxc;
    }

    public void setChdwxc(String chdwxc) {
        this.chdwxc = chdwxc;
    }

    public String getSftsrq_old() {
        return sftsrq_old;
    }

    public void setSftsrq_old(String sftsrq_old) {
        this.sftsrq_old = sftsrq_old;
    }

    public String getSjly() {
        return sjly;
    }

    public void setSjly(String sjly) {
        this.sjly = sjly;
    }
}
