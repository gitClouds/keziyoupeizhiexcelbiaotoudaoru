/*************************************************************************
 Copyright (C) Unpublished Unis Software, Inc. All rights reserved.
 Unis Software, Inc., Confidential and Proprietary.

 This software is subject to copyright protection
 under the laws of the Public of China and other countries.

 Unless otherwise explicitly stated, this software is provided
 by Unis "AS IS".
 *************************************************************************/
package com.unis.wsClient.bean;



import java.io.Serializable;
import java.util.Date;

/**
 * 吸毒人员查获信息
 *
 * @author Administrator
 * @version 1.0
 * @since 2020-03-10
 */
public class WSTbXdryChxx implements Serializable {

    private static final long serialVersionUID = 1L;

    /****************************************
     * Basic fields
     ****************************************/
    /**
     * 查获序号--DEXXXXX--: varchar
     */
    private String chxh;

    /**
     * 人员编号--DE00038--编号规则为：R+XXXXXXXXXXXX(公安机关机构代码)+XXXXXX(年月)+XXXX(顺序号)              : varchar
     */
    private String xyrbh;

    /**
     * 查获日期--DE01160--: timestamp
     */
    private Date chrq;

    /**
     *   查获单位_行政区划代码--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码: varchar
     */
    private String chdw_xzqhdm;

    /**
     *   查获单位_区划内详细地址--DE00076--: varchar
     */
    private String chdw_qhnxxdz;

    /**
     *   查获单位_单位名称--DE00065--: varchar
     */
    private String chdw_dwmc;

    /**
     * 吸毒场所--DE01135--采用GA/TXXXX《吸毒场所种类代码》: varchar
     */
    private String xdcszldm;

    /**
     * 本次滥用毒品种类--DEXXXXX--: varchar
     */
    private String bclydpzl;

    /**
     * 毒品来源--DE00166--采用GA 332.3《禁毒信息管理代码 第3部分:毒品来源代码》: varchar
     */
    private String dplydm;

    /**
     * 违法事实_简要案情--DE00100--: varchar
     */
    private String wfss_jyaq;

    /**
     * 查获类型--DE01133--吸毒人员查获类型代码: varchar
     */
    private String xdrychlxdm;

    /**
     * 查获来源--DE01132--采用GA/TXXXX《吸毒人员查获来源代码》: varchar
     */
    private String xdrychlydm;

    /**
     * 尿检结果--DE01137--尿检结果代码: varchar
     */
    private String njjgdm;

    /**
     * 吸毒方式--DE01136--采用GA/TXXXX《吸毒方式代码》: varchar
     */
    private String xdfsdm;

    /**
     * 复吸次数----: decimal
     */
    private Integer xfcs;

    /**
     * 娱乐场所查获序号----: varchar
     */
    private String ylcschxh;

    /**
     * 娱乐场所名称----: varchar
     */
    private String ylcsmc;

    /**
     * 是否特殊人群----: decimal
     */
    private Integer sftsrq;

    /**
     * 特殊人群种类----: varchar
     */
    private String tsrqzl;

    /**
     * 阳性结果----: varchar
     */
    private String yxjg;

    /**
     * 是否疾病----: decimal
     */
    private Integer sfjb;

    /**
     * 疾病类型----: varchar
     */
    private String jblx;

    /**
     * 是否末次查获----: decimal
     */
    private Integer sfmcch;

    /**
     * 查获次数----: decimal
     */
    private Integer chcs;

    /**
     *   填表人_姓名--DE00002--: varchar
     */
    private String tbr_xm;

    /**
     *   填表人_联系电话--DE00216--固定电话号码、移动电话号码: varchar
     */
    private String tbr_lxdh;

    /**
     * 审核人_姓名--DE00002--: varchar
     */
    private String shr_xm;

    /**
     * 填表单位_单位名称--DE00065--: varchar
     */
    private String tbdw_dwmc;

    /**
     * 填表日期--DE01158--: timestamp
     */
    private Date tbrq;

    /**
     * 录入人_姓名--DE00002--: varchar
     */
    private String lrr_xm;

    /**
     * 录入单位_单位名称--DE00065--: varchar
     */
    private String lrdw_dwmc;

    /**
     * 录入时间--DE00739--: timestamp
     */
    private Date lrsj;

    /**
     * 备注--DE00503--: varchar
     */
    private String bz;

    /**
     * 人员标识--DEXXXXX--: varchar
     */
    private String rybs;

    /**
     * 修改部署单位--DEXXXXX--: varchar
     */
    private String xgbsdw;

    /**
     * 归属单位--DEXXXXX--: varchar
     */
    private String gsdw;

    /**
     * 更新时间戳--DEXXXXX--: varchar
     */
    private String gxsjc;

    /**
     * 操作标识--DEXXXXX--: varchar
     */
    private String czbs;

    /**
     * 内外网标志--DEXXXXX--: varchar
     */
    private String inoutbz;

    /**
     * 有效性--DEXXXXX--: decimal
     */
    private Integer yxx;

    /**
     * 更新时间--DEXXXXX--: timestamp
     */
    private Date gxsj;

    /**
     *   查获单位_单位代码--DE00065--: varchar
     */
    private String chdw_dwdm;

    /**
     * 录入单位_单位代码--DE00065--: varchar
     */
    private String lrdw_dwdm;

    /**
     * 填表单位_单位代码--DE00065--: varchar
     */
    private String tbdw_dwdm;

    /**
     * 嫌疑人类别代码---XYRLB: varchar
     */
    private String xyrlb;

    /**
     * 注释: varchar
     */
    private String fjmc;

    /**
     * 注释: varchar
     */
    private String wlgjfjxh;

    /**
     * 注释: varchar
     */
    private String chjclx;

    /**
     * 注释: varchar
     */
    private String chjc_dwdm;

    /**
     * 注释: varchar
     */
    private String chjc_dwmc;

    /**
     * 注释: varchar
     */
    private String chsnl;

    /**
     * 吞食异物描述: varchar
     */
    private String tsywms;

    /**
     * 是否吞食异物: decimal
     */
    private Integer sftsyw;

    /**
     * 对应的所有处置情况: varchar
     */
    private String czqk;

    /**
     * 对应的所有处置情况名称: varchar
     */
    private String czqkmc;

    /**
     * 本次查获之前的管控现状代码: varchar
     */
    private String bczhzqgkxzdm;


    /****************************************
     * JavaBean setters & getters
     ****************************************/
    public String getChxh() {
        return chxh;
    }

    public void setChxh(String chxh) {
        this.chxh = chxh;
    }

    public String getXyrbh() {
        return xyrbh;
    }

    public void setXyrbh(String xyrbh) {
        this.xyrbh = xyrbh;
    }

    public Date getChrq() {
        return chrq;
    }

    public void setChrq(Date chrq) {
        this.chrq = chrq;
    }

    public String getChdw_xzqhdm() {
        return chdw_xzqhdm;
    }

    public void setChdw_xzqhdm(String chdw_xzqhdm) {
        this.chdw_xzqhdm = chdw_xzqhdm;
    }

    public String getChdw_qhnxxdz() {
        return chdw_qhnxxdz;
    }

    public void setChdw_qhnxxdz(String chdw_qhnxxdz) {
        this.chdw_qhnxxdz = chdw_qhnxxdz;
    }

    public String getChdw_dwmc() {
        return chdw_dwmc;
    }

    public void setChdw_dwmc(String chdw_dwmc) {
        this.chdw_dwmc = chdw_dwmc;
    }

    public String getXdcszldm() {
        return xdcszldm;
    }

    public void setXdcszldm(String xdcszldm) {
        this.xdcszldm = xdcszldm;
    }

    public String getBclydpzl() {
        return bclydpzl;
    }

    public void setBclydpzl(String bclydpzl) {
        this.bclydpzl = bclydpzl;
    }

    public String getDplydm() {
        return dplydm;
    }

    public void setDplydm(String dplydm) {
        this.dplydm = dplydm;
    }

    public String getWfss_jyaq() {
        return wfss_jyaq;
    }

    public void setWfss_jyaq(String wfss_jyaq) {
        this.wfss_jyaq = wfss_jyaq;
    }

    public String getXdrychlxdm() {
        return xdrychlxdm;
    }

    public void setXdrychlxdm(String xdrychlxdm) {
        this.xdrychlxdm = xdrychlxdm;
    }

    public String getXdrychlydm() {
        return xdrychlydm;
    }

    public void setXdrychlydm(String xdrychlydm) {
        this.xdrychlydm = xdrychlydm;
    }

    public String getNjjgdm() {
        return njjgdm;
    }

    public void setNjjgdm(String njjgdm) {
        this.njjgdm = njjgdm;
    }

    public String getXdfsdm() {
        return xdfsdm;
    }

    public void setXdfsdm(String xdfsdm) {
        this.xdfsdm = xdfsdm;
    }

    public Integer getXfcs() {
        return xfcs;
    }

    public void setXfcs(Integer xfcs) {
        this.xfcs = xfcs;
    }

    public String getYlcschxh() {
        return ylcschxh;
    }

    public void setYlcschxh(String ylcschxh) {
        this.ylcschxh = ylcschxh;
    }

    public String getYlcsmc() {
        return ylcsmc;
    }

    public void setYlcsmc(String ylcsmc) {
        this.ylcsmc = ylcsmc;
    }

    public Integer getSftsrq() {
        return sftsrq;
    }

    public void setSftsrq(Integer sftsrq) {
        this.sftsrq = sftsrq;
    }

    public String getTsrqzl() {
        return tsrqzl;
    }

    public void setTsrqzl(String tsrqzl) {
        this.tsrqzl = tsrqzl;
    }

    public String getYxjg() {
        return yxjg;
    }

    public void setYxjg(String yxjg) {
        this.yxjg = yxjg;
    }

    public Integer getSfjb() {
        return sfjb;
    }

    public void setSfjb(Integer sfjb) {
        this.sfjb = sfjb;
    }

    public String getJblx() {
        return jblx;
    }

    public void setJblx(String jblx) {
        this.jblx = jblx;
    }

    public Integer getSfmcch() {
        return sfmcch;
    }

    public void setSfmcch(Integer sfmcch) {
        this.sfmcch = sfmcch;
    }

    public Integer getChcs() {
        return chcs;
    }

    public void setChcs(Integer chcs) {
        this.chcs = chcs;
    }

    public String getTbr_xm() {
        return tbr_xm;
    }

    public void setTbr_xm(String tbr_xm) {
        this.tbr_xm = tbr_xm;
    }

    public String getTbr_lxdh() {
        return tbr_lxdh;
    }

    public void setTbr_lxdh(String tbr_lxdh) {
        this.tbr_lxdh = tbr_lxdh;
    }

    public String getShr_xm() {
        return shr_xm;
    }

    public void setShr_xm(String shr_xm) {
        this.shr_xm = shr_xm;
    }

    public String getTbdw_dwmc() {
        return tbdw_dwmc;
    }

    public void setTbdw_dwmc(String tbdw_dwmc) {
        this.tbdw_dwmc = tbdw_dwmc;
    }

    public Date getTbrq() {
        return tbrq;
    }

    public void setTbrq(Date tbrq) {
        this.tbrq = tbrq;
    }

    public String getLrr_xm() {
        return lrr_xm;
    }

    public void setLrr_xm(String lrr_xm) {
        this.lrr_xm = lrr_xm;
    }

    public String getLrdw_dwmc() {
        return lrdw_dwmc;
    }

    public void setLrdw_dwmc(String lrdw_dwmc) {
        this.lrdw_dwmc = lrdw_dwmc;
    }

    public Date getLrsj() {
        return lrsj;
    }

    public void setLrsj(Date lrsj) {
        this.lrsj = lrsj;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }

    public String getRybs() {
        return rybs;
    }

    public void setRybs(String rybs) {
        this.rybs = rybs;
    }

    public String getXgbsdw() {
        return xgbsdw;
    }

    public void setXgbsdw(String xgbsdw) {
        this.xgbsdw = xgbsdw;
    }

    public String getGsdw() {
        return gsdw;
    }

    public void setGsdw(String gsdw) {
        this.gsdw = gsdw;
    }

    public String getGxsjc() {
        return gxsjc;
    }

    public void setGxsjc(String gxsjc) {
        this.gxsjc = gxsjc;
    }

    public String getCzbs() {
        return czbs;
    }

    public void setCzbs(String czbs) {
        this.czbs = czbs;
    }

    public String getInoutbz() {
        return inoutbz;
    }

    public void setInoutbz(String inoutbz) {
        this.inoutbz = inoutbz;
    }

    public Integer getYxx() {
        return yxx;
    }

    public void setYxx(Integer yxx) {
        this.yxx = yxx;
    }

    public Date getGxsj() {
        return gxsj;
    }

    public void setGxsj(Date gxsj) {
        this.gxsj = gxsj;
    }

    public String getChdw_dwdm() {
        return chdw_dwdm;
    }

    public void setChdw_dwdm(String chdw_dwdm) {
        this.chdw_dwdm = chdw_dwdm;
    }

    public String getLrdw_dwdm() {
        return lrdw_dwdm;
    }

    public void setLrdw_dwdm(String lrdw_dwdm) {
        this.lrdw_dwdm = lrdw_dwdm;
    }

    public String getTbdw_dwdm() {
        return tbdw_dwdm;
    }

    public void setTbdw_dwdm(String tbdw_dwdm) {
        this.tbdw_dwdm = tbdw_dwdm;
    }

    public String getXyrlb() {
        return xyrlb;
    }

    public void setXyrlb(String xyrlb) {
        this.xyrlb = xyrlb;
    }

    public String getFjmc() {
        return fjmc;
    }

    public void setFjmc(String fjmc) {
        this.fjmc = fjmc;
    }

    public String getWlgjfjxh() {
        return wlgjfjxh;
    }

    public void setWlgjfjxh(String wlgjfjxh) {
        this.wlgjfjxh = wlgjfjxh;
    }

    public String getChjclx() {
        return chjclx;
    }

    public void setChjclx(String chjclx) {
        this.chjclx = chjclx;
    }

    public String getChjc_dwdm() {
        return chjc_dwdm;
    }

    public void setChjc_dwdm(String chjc_dwdm) {
        this.chjc_dwdm = chjc_dwdm;
    }

    public String getChjc_dwmc() {
        return chjc_dwmc;
    }

    public void setChjc_dwmc(String chjc_dwmc) {
        this.chjc_dwmc = chjc_dwmc;
    }

    public String getChsnl() {
        return chsnl;
    }

    public void setChsnl(String chsnl) {
        this.chsnl = chsnl;
    }

    public String getTsywms() {
        return tsywms;
    }

    public void setTsywms(String tsywms) {
        this.tsywms = tsywms;
    }

    public Integer getSftsyw() {
        return sftsyw;
    }

    public void setSftsyw(Integer sftsyw) {
        this.sftsyw = sftsyw;
    }

    public String getCzqk() {
        return czqk;
    }

    public void setCzqk(String czqk) {
        this.czqk = czqk;
    }

    public String getCzqkmc() {
        return czqkmc;
    }

    public void setCzqkmc(String czqkmc) {
        this.czqkmc = czqkmc;
    }

    public String getBczhzqgkxzdm() {
        return bczhzqgkxzdm;
    }

    public void setBczhzqgkxzdm(String bczhzqgkxzdm) {
        this.bczhzqgkxzdm = bczhzqgkxzdm;
    }
}
