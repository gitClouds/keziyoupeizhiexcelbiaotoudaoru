package com.unis.dto;

import org.apache.poi.ss.formula.functions.T;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2019/1/9/009.
 */
public class ResultDto implements Serializable {

    /*jsonReader : {
        root: "rows",    // json中代表实际模型数据的入口
                page: "page",    // json中代表当前页码的数据
                total: "total",    // json中代表页码总数的数据
                records: "records", // json中代表数据行总数的数据
                repeatitems: true, // 如果设为false，则jqGrid在解析json时，会根据name来搜索对应的数据元素（即可以json中元素可以不按顺序）；而所使用的name是来自于colModel中的name设定。
                cell: "cell",      //当前行的所有单元格
                id: "id",          //行id
                userdata: "userdata",  //从服务器端返回一些参数但并不想直接把他们显示到表格中，而是想在别的地方显示
                subgrid: {
            root:"rows",
                    repeatitems: true,
                    cell:"cell"
        }
    }
*/
    //当前页
  /*  private int pageNum;
    //每页的数量
    private int pageSize;
    //当前页的数量
    private int size;
    //由于startRow和endRow不常用，这里说个具体的用法
    //可以在页面中"显示startRow到endRow 共size条数据"

    //当前页面第一个元素在数据库中的行号
    private int startRow;
    //当前页面最后一个元素在数据库中的行号
    private int endRow;
    //总记录数
    private long total;
    //总页数
    private int pages;
    //结果集
    private List<T> list;

    //第一页
    private int firstPage;
    //前一页
    private int prePage;

    //是否为第一页
    private boolean isFirstPage = false;
    //是否为最后一页
    private boolean isLastPage = false;
    //是否有前一页
    private boolean hasPreviousPage = false;
    //是否有下一页
    private boolean hasNextPage = false;
    //导航页码数
    private int navigatePages;
    //所有导航页号
    private int[] navigatepageNums;
*/
    private int total;//总页数
    private int page;//当前页码的数据
    private List rows;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public List getRows() {
        return rows;
    }

    public void setRows(List rows) {
        this.rows = rows;
    }
}
