package com.unis.server;

import com.unis.wsClient.bean.WSTbDpajSdryJbxx;
import com.unis.wsClient.bean.WSTbXdryJbxx;
import com.unis.wsClient.bean.WSsdryxx;
import com.unis.wsClient.bean.WSxdryxx;

import javax.jws.WebService;
import java.util.List;


@WebService
public interface WsJdzhQueryService {
	WSsdryxx querySdryByString(List<String> queryStrings, String sendcode);

	WSxdryxx queryXdryByString(List<String> queryStrings, String sendcode);
}
