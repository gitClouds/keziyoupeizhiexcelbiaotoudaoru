package com.unis.service.tool.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.unis.common.exception.app.AppRuntimeException;
import com.unis.dto.ResultDto;
import com.unis.mapper.tool.TbXxInfoMapper;
import com.unis.model.tool.Sdh;
import com.unis.model.tool.TbXxInfo;
import com.unis.service.impl.BaseServiceImpl;
import com.unis.service.tool.TbXxInfoService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * <pre>
 * @see TbXxInfoService
 * </pre>
 *
 * @author Qx
 * @version 1.0
 * @since 2020-05-21
 */
@Service("tbXxInfoService")
public class TbXxInfoServiceImpl extends BaseServiceImpl implements TbXxInfoService {
    private static final Logger logger = LoggerFactory.getLogger(TbXxInfoServiceImpl.class);
    @Autowired
    private TbXxInfoMapper tbXxInfoMapper;

    @Override
    public int updateByPrimaryKey(TbXxInfo tbXxInfo) throws Exception {
        if (tbXxInfo!=null){
            if(StringUtils.isBlank(tbXxInfo.getXx_id())){
                logger.error("TbXxInfoServiceImpl.updateByPrimaryKey时tbXxInfo.Pk为空。");
                throw new AppRuntimeException("TbXxInfoServiceImpl.updateByPrimaryKey时tbXxInfo.Pk为空。");
            }
            return tbXxInfoMapper.updateByPrimaryKeySelective(tbXxInfo);
        }else{
            logger.error("TbXxInfoServiceImpl.updateByPrimaryKey时tbXxInfo数据为空。");
            throw new AppRuntimeException("TbXxInfoServiceImpl.updateByPrimaryKey时tbXxInfo数据为空。");
        }
    }
    @Override
    public TbXxInfo queryAsObject(TbXxInfo tbXxInfo) throws Exception {
        if (tbXxInfo!=null){
            return tbXxInfoMapper.selectOne(tbXxInfo);
        }else{
            logger.error("SdhServiceImpl.queryAsObject时sdh数据为空。");
            throw new AppRuntimeException("SdhServiceImpl.queryAsObject时sdh数据为空。");
        }
    }
    /**
     * @see TbXxInfoService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {
        if(example!=null){
            PageHelper.startPage(pageNum,pageSize);
            return new PageInfo(tbXxInfoMapper.selectByExample(example));
        }else{
            logger.error("TbXxInfoServiceImpl.queryListByExample时example数据为空。");
            throw new AppRuntimeException("TbXxInfoServiceImpl.queryListByExample时example数据为空。");
        }
    }
    /**
     * @see TbXxInfoService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
        if (parmMap!=null){
            Example example = new Example(TbXxInfo.class);
            example.setOrderByClause("xx_lrsj DESC ");
            Example.Criteria criteria = example.createCriteria();
            //criteria设置
            // = --> andEqualTo(field,value)
            // like --> andLike(field,likeValue)；likeValue 包含‘%’
            // is null --> andIsNull(field)
            // is not null --> andIsNotNull(field)
            // <> --> andNotEqualTo(field)
            // > --> andGreaterThan(field,value)
            // >= --> andGreaterThanOrEqualTo(field,value)
            // < --> andLessThan(field,value)
            // <= --> andLessThanOrEqualTo(field,value)
            // in --> andIn(field,Iterable value)
            // not in --> andNotIn(field,Iterable value)
            // between --> andBetween(field,beginValue,endValue)
            // not like --> andNotLike(field,likeValue)；likeValue 包含‘%’

            // or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)

            //criteria.andEqualTo("yxx",1);设置yxx=1
            /**
             *此方法请根据需要修改下方条件
             */
            //此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
            //生成条件中均为 and field = value
            //生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改

            boolean flag = false;
            if(parmMap.get("xx_id")!=null && StringUtils.isNotBlank(parmMap.get("xx_id").toString())){
                criteria.andEqualTo("xx_id",parmMap.get("xx_id").toString().trim());
                flag = true;
            }
            if(parmMap.get("xx_lx")!=null && StringUtils.isNotBlank(parmMap.get("xx_lx").toString())){
                criteria.andEqualTo("xx_lx",parmMap.get("xx_lx").toString().trim());
                flag = true;
            }
            if(parmMap.get("xxly_id")!=null && StringUtils.isNotBlank(parmMap.get("xxly_id").toString())){
                criteria.andEqualTo("xxly_id",parmMap.get("xxly_id").toString().trim());
                flag = true;
            }
            if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
                criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
                flag = true;
            }
            if(parmMap.get("xx_lrsj")!=null && StringUtils.isNotBlank(parmMap.get("xx_lrsj").toString())){
                criteria.andEqualTo("xx_lrsj",parmMap.get("xx_lrsj").toString().trim());
                flag = true;
            }
            if(parmMap.get("xx_nr")!=null && StringUtils.isNotBlank(parmMap.get("xx_nr").toString())){
                criteria.andEqualTo("xx_nr",parmMap.get("xx_nr").toString().trim());
                flag = true;
            }
            if(parmMap.get("xx_sl")!=null && StringUtils.isNotBlank(parmMap.get("xx_sl").toString())){
                criteria.andEqualTo("xx_sl",parmMap.get("xx_sl").toString().trim());
                flag = true;
            }
            //一个条件都没有的时候给一个默认条件
            if(!flag){

            }
            PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
            ResultDto result = new ResultDto();
            result.setRows(pageInfo.getList());
            result.setPage(pageInfo.getPageNum());
            //result.setTotal(pageInfo.getPages());
            result.setTotal((int)pageInfo.getTotal());
            return result;
        }else{
            logger.error("TbXxInfoServiceImpl.queryListByPage时parmMap数据为空。");
            throw new AppRuntimeException("TbXxInfoServiceImpl.queryListByPage时parmMap数据为空。");
        }
    }
    public List<TbXxInfo> queryList(Map<String, Object> paramMap) throws Exception{
        return tbXxInfoMapper.queryList(paramMap);
    }
    @Override
    public List<Map<String, String>> queryAccountNumEveryDay(Map<String, Object> map) throws Exception{
        return tbXxInfoMapper.queryAccountNumEveryDay(map);
    }
    @Override
    public List<Map<String, String>> queryXijkPhone() throws Exception{
        return tbXxInfoMapper.queryXijkPhone();
    }

}
