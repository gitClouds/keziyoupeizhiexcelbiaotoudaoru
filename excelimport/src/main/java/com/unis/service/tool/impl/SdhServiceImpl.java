package com.unis.service.tool.impl;

import java.util.List;
import java.util.Map;

import com.unis.common.util.TemplateUtil;
import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.tool.SdhMapper;
import com.unis.model.tool.Sdh;
import com.unis.service.tool.SdhService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see SdhService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-04-25
 */
@Service("sdhService")
public class SdhServiceImpl extends BaseServiceImpl implements SdhService {
	private static final Logger logger = LoggerFactory.getLogger(SdhServiceImpl.class);
    @Autowired
    private SdhMapper sdhMapper;

    /**
     * @see SdhService#insert(Sdh sdh)
     */
    @Override
    public int insert(Sdh sdh) throws Exception {
    	if (sdh!=null){
	        sdh.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return sdhMapper.insertSelective(sdh);
    	}else{
    		logger.error("SdhServiceImpl.insert时sdh数据为空。");
    		throw new AppRuntimeException("SdhServiceImpl.insert时sdh数据为空。");
    	}        
    }

    /**
     * @see SdhService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("SdhServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("SdhServiceImpl.delete时pk为空。");
    	}else{
    		return sdhMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see SdhService#updateByPrimaryKey(Sdh sdh)
     */
    @Override
    public int updateByPrimaryKey(Sdh sdh) throws Exception {
        if (sdh!=null){
        	if(StringUtils.isBlank(sdh.getPk())){
        		logger.error("SdhServiceImpl.updateByPrimaryKey时sdh.Pk为空。");
        		throw new AppRuntimeException("SdhServiceImpl.updateByPrimaryKey时sdh.Pk为空。");
        	}
	        return sdhMapper.updateByPrimaryKeySelective(sdh);
    	}else{
    		logger.error("SdhServiceImpl.updateByPrimaryKey时sdh数据为空。");
    		throw new AppRuntimeException("SdhServiceImpl.updateByPrimaryKey时sdh数据为空。");
    	}
    }
    /**
     * @see SdhService#querySdhByPrimaryKey(String pk)
     */
    @Override
    public Sdh querySdhByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("SdhServiceImpl.querySdhByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("SdhServiceImpl.querySdhByPrimaryKey时pk为空。");
    	}else{
    		return sdhMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see SdhService#queryAsObject(Sdh sdh)
     */
    @Override
    public Sdh queryAsObject(Sdh sdh) throws Exception {
        if (sdh!=null){
	        return sdhMapper.selectOne(sdh);
    	}else{
    		logger.error("SdhServiceImpl.queryAsObject时sdh数据为空。");
    		throw new AppRuntimeException("SdhServiceImpl.queryAsObject时sdh数据为空。");
    	}
    }
    
    /**
     * @see SdhService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return sdhMapper.selectCountByExample(example);
    	}else{
    		logger.error("SdhServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("SdhServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see SdhService#queryListByExample(Example example)
     */
    @Override
    public List<Sdh> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return sdhMapper.selectByExample(example);
    	}else{
    		logger.error("SdhServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("SdhServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see SdhService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(sdhMapper.selectByExample(example));
    	}else{
    		logger.error("SdhServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("SdhServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see SdhService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(Sdh.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jtype")!=null && StringUtils.isNotBlank(parmMap.get("jtype").toString())){
				criteria.andEqualTo("jtype",parmMap.get("jtype").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jgh")!=null && StringUtils.isNotBlank(parmMap.get("jgh").toString())){
				criteria.andEqualTo("jgh",parmMap.get("jgh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("dqm")!=null && StringUtils.isNotBlank(parmMap.get("dqm").toString())){
				criteria.andEqualTo("dqm",parmMap.get("dqm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("dwmc")!=null && StringUtils.isNotBlank(parmMap.get("dwmc").toString())){
				//criteria.andEqualTo("dwmc",parmMap.get("dwmc").toString().trim());
				criteria.andLike("dwmc","%"+parmMap.get("dwmc").toString().trim()+"%");
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("SdhServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("SdhServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
