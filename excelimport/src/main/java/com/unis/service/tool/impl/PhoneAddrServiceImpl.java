package com.unis.service.tool.impl;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.tool.PhoneAddrMapper;
import com.unis.model.tool.PhoneAddr;
import com.unis.service.tool.PhoneAddrService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see PhoneAddrService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-04-29
 */
@Service("phoneAddrService")
public class PhoneAddrServiceImpl extends BaseServiceImpl implements PhoneAddrService {
	private static final Logger logger = LoggerFactory.getLogger(PhoneAddrServiceImpl.class);
    @Autowired
    private PhoneAddrMapper phoneAddrMapper;
    /**
     * @see PhoneAddrService#queryPhoneAddrByPrimaryKey(String pk)
     */
    @Override
    public PhoneAddr queryPhoneAddrByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("PhoneAddrServiceImpl.queryPhoneAddrByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("PhoneAddrServiceImpl.queryPhoneAddrByPrimaryKey时pk为空。");
    	}else{
    		return phoneAddrMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see PhoneAddrService#queryAsObject(PhoneAddr phoneAddr)
     */
    @Override
    public PhoneAddr queryAsObject(PhoneAddr phoneAddr) throws Exception {
        if (phoneAddr!=null){
	        return phoneAddrMapper.selectOne(phoneAddr);
    	}else{
    		logger.error("PhoneAddrServiceImpl.queryAsObject时phoneAddr数据为空。");
    		throw new AppRuntimeException("PhoneAddrServiceImpl.queryAsObject时phoneAddr数据为空。");
    	}
    }
    
    /**
     * @see PhoneAddrService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return phoneAddrMapper.selectCountByExample(example);
    	}else{
    		logger.error("PhoneAddrServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("PhoneAddrServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see PhoneAddrService#queryListByExample(Example example)
     */
    @Override
    public List<PhoneAddr> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return phoneAddrMapper.selectByExample(example);
    	}else{
    		logger.error("PhoneAddrServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("PhoneAddrServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see PhoneAddrService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(phoneAddrMapper.selectByExample(example));
    	}else{
    		logger.error("PhoneAddrServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("PhoneAddrServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see PhoneAddrService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(PhoneAddr.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
			if(parmMap.get("phone")!=null && StringUtils.isNotBlank(parmMap.get("phone").toString()) && StringUtils.length(parmMap.get("phone").toString().trim()) >=7){
				criteria.andEqualTo("phone",StringUtils.substring(parmMap.get("phone").toString().trim(),0,7));
			}else{
				criteria.andEqualTo("phone","0");
			}
    		/*boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("prefix")!=null && StringUtils.isNotBlank(parmMap.get("prefix").toString())){
				criteria.andEqualTo("prefix",parmMap.get("prefix").toString().trim());
				flag = true;
			}
    		if(parmMap.get("phone")!=null && StringUtils.isNotBlank(parmMap.get("phone").toString())){
				criteria.andEqualTo("phone",parmMap.get("phone").toString().trim());
				flag = true;
			}
    		if(parmMap.get("pmc")!=null && StringUtils.isNotBlank(parmMap.get("pmc").toString())){
				criteria.andEqualTo("pmc",parmMap.get("pmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cmc")!=null && StringUtils.isNotBlank(parmMap.get("cmc").toString())){
				criteria.andEqualTo("cmc",parmMap.get("cmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ispmc")!=null && StringUtils.isNotBlank(parmMap.get("ispmc").toString())){
				criteria.andEqualTo("ispmc",parmMap.get("ispmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("codedm")!=null && StringUtils.isNotBlank(parmMap.get("codedm").toString())){
				criteria.andEqualTo("codedm",parmMap.get("codedm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zipdm")!=null && StringUtils.isNotBlank(parmMap.get("zipdm").toString())){
				criteria.andEqualTo("zipdm",parmMap.get("zipdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("typesmc")!=null && StringUtils.isNotBlank(parmMap.get("typesmc").toString())){
				criteria.andEqualTo("typesmc",parmMap.get("typesmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sjdm")!=null && StringUtils.isNotBlank(parmMap.get("sjdm").toString())){
				criteria.andEqualTo("sjdm",parmMap.get("sjdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("dm")!=null && StringUtils.isNotBlank(parmMap.get("dm").toString())){
				criteria.andEqualTo("dm",parmMap.get("dm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}*/
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("PhoneAddrServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("PhoneAddrServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
