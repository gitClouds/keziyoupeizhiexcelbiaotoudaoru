package com.unis.service.tool;

import com.github.pagehelper.PageInfo;
import com.unis.dto.ResultDto;
import com.unis.model.tool.Sdh;
import com.unis.model.tool.TbXxInfo;
import com.unis.service.BaseService;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * <pre>
 * TODO
 * </pre>
 *
 * @author Qx
 * @version 1.0
 * @since 2020-05-21
 */

public interface TbXxInfoService extends BaseService {
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int updateByPrimaryKey(TbXxInfo tbXxInfo) throws Exception;

    /**
     * 根据条件获取数据集合(分页),需在example 设定排序
     *
     * @param example
     * @param pageNum
     * @param pageSize
     * @throws Exception
     */
    PageInfo queryPageInfoByExample(Example example, int pageNum, int pageSize) throws Exception;

    /**
     * 根据入参获取数据集合(分页)
     *
     * @param parmMap
     * @param pageNum
     * @param pageSize
     * @return
     * @throws Exception
     */
    ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws Exception;


    TbXxInfo queryAsObject(TbXxInfo tbXxInfo) throws Exception;

    List<TbXxInfo> queryList(Map<String, Object> paramMap) throws Exception;
    List<Map<String, String>> queryAccountNumEveryDay(Map<String, Object> map) throws Exception;
    List<Map<String, String>> queryXijkPhone() throws Exception;
}
