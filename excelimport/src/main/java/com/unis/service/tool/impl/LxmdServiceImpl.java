package com.unis.service.tool.impl;

import java.util.List;
import java.util.Map;

import com.unis.common.util.TemplateUtil;
import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.tool.LxmdMapper;
import com.unis.model.tool.Lxmd;
import com.unis.service.tool.LxmdService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see LxmdService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-04-26
 */
@Service("lxmdService")
public class LxmdServiceImpl extends BaseServiceImpl implements LxmdService {
	private static final Logger logger = LoggerFactory.getLogger(LxmdServiceImpl.class);
    @Autowired
    private LxmdMapper lxmdMapper;

    /**
     * @see LxmdService#insert(Lxmd lxmd)
     */
    @Override
    public int insert(Lxmd lxmd) throws Exception {
    	if (lxmd!=null){
	        lxmd.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return lxmdMapper.insertSelective(lxmd);
    	}else{
    		logger.error("LxmdServiceImpl.insert时lxmd数据为空。");
    		throw new AppRuntimeException("LxmdServiceImpl.insert时lxmd数据为空。");
    	}        
    }

    /**
     * @see LxmdService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("LxmdServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("LxmdServiceImpl.delete时pk为空。");
    	}else{
    		return lxmdMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see LxmdService#updateByPrimaryKey(Lxmd lxmd)
     */
    @Override
    public int updateByPrimaryKey(Lxmd lxmd) throws Exception {
        if (lxmd!=null){
        	if(StringUtils.isBlank(lxmd.getPk())){
        		logger.error("LxmdServiceImpl.updateByPrimaryKey时lxmd.Pk为空。");
        		throw new AppRuntimeException("LxmdServiceImpl.updateByPrimaryKey时lxmd.Pk为空。");
        	}
	        return lxmdMapper.updateByPrimaryKeySelective(lxmd);
    	}else{
    		logger.error("LxmdServiceImpl.updateByPrimaryKey时lxmd数据为空。");
    		throw new AppRuntimeException("LxmdServiceImpl.updateByPrimaryKey时lxmd数据为空。");
    	}
    }
    /**
     * @see LxmdService#queryLxmdByPrimaryKey(String pk)
     */
    @Override
    public Lxmd queryLxmdByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("LxmdServiceImpl.queryLxmdByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("LxmdServiceImpl.queryLxmdByPrimaryKey时pk为空。");
    	}else{
    		return lxmdMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see LxmdService#queryAsObject(Lxmd lxmd)
     */
    @Override
    public Lxmd queryAsObject(Lxmd lxmd) throws Exception {
        if (lxmd!=null){
	        return lxmdMapper.selectOne(lxmd);
    	}else{
    		logger.error("LxmdServiceImpl.queryAsObject时lxmd数据为空。");
    		throw new AppRuntimeException("LxmdServiceImpl.queryAsObject时lxmd数据为空。");
    	}
    }
    
    /**
     * @see LxmdService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return lxmdMapper.selectCountByExample(example);
    	}else{
    		logger.error("LxmdServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("LxmdServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see LxmdService#queryListByExample(Example example)
     */
    @Override
    public List<Lxmd> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return lxmdMapper.selectByExample(example);
    	}else{
    		logger.error("LxmdServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("LxmdServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see LxmdService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(lxmdMapper.selectByExample(example));
    	}else{
    		logger.error("LxmdServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("LxmdServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see LxmdService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(Lxmd.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jtype")!=null && StringUtils.isNotBlank(parmMap.get("jtype").toString())){
				criteria.andEqualTo("jtype",parmMap.get("jtype").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jgh")!=null && StringUtils.isNotBlank(parmMap.get("jgh").toString())){
				criteria.andEqualTo("jgh",parmMap.get("jgh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("dqm")!=null && StringUtils.isNotBlank(parmMap.get("dqm").toString())){
				criteria.andEqualTo("dqm",parmMap.get("dqm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("dwmc")!=null && StringUtils.isNotBlank(parmMap.get("dwmc").toString())){
				//criteria.andEqualTo("dwmc",parmMap.get("dwmc").toString().trim());
				criteria.andLike("dwmc","%"+parmMap.get("dwmc").toString().trim()+"%");
				flag = true;
			}
    		if(parmMap.get("dzsx")!=null && StringUtils.isNotBlank(parmMap.get("dzsx").toString())){
				criteria.andEqualTo("dzsx",parmMap.get("dzsx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lxr")!=null && StringUtils.isNotBlank(parmMap.get("lxr").toString())){
				criteria.andEqualTo("lxr",parmMap.get("lxr").toString().trim());
				flag = true;
			}
    		if(parmMap.get("telephone")!=null && StringUtils.isNotBlank(parmMap.get("telephone").toString())){
				criteria.andEqualTo("telephone",parmMap.get("telephone").toString().trim());
				flag = true;
			}
    		if(parmMap.get("phone")!=null && StringUtils.isNotBlank(parmMap.get("phone").toString())){
				criteria.andEqualTo("phone",parmMap.get("phone").toString().trim());
				flag = true;
			}
    		if(parmMap.get("email")!=null && StringUtils.isNotBlank(parmMap.get("email").toString())){
				criteria.andEqualTo("email",parmMap.get("email").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xz")!=null && StringUtils.isNotBlank(parmMap.get("xz").toString())){
				criteria.andEqualTo("xz",parmMap.get("xz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("LxmdServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("LxmdServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
