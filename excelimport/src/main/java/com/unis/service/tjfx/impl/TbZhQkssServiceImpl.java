package com.unis.service.tjfx.impl;

import java.util.Map;

import com.unis.mapper.tjfx.TbZhQkssMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.service.tjfx.TbZhQkssService;
import com.unis.dto.ResultDto;

import javax.annotation.Resource;

/**
 * <pre>
 * @see TbZhQkssService
 * </pre>
 *
 * @author gongjl
 * @version 1.0
 * @since 2020-06-05
 */
@Service("tbZhQkssService")
public class TbZhQkssServiceImpl extends BaseServiceImpl implements TbZhQkssService {
	private static final Logger logger = LoggerFactory.getLogger(TbZhQkssServiceImpl.class);
    @Resource
    private TbZhQkssMapper tbZhQkssMapper;

    /**
     * @see TbZhQkssService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
	/**
     * @see TbZhQkssService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map<Object, Object> parmMap, int pageNum, int pageSize) throws  Exception{
		PageHelper.startPage(pageNum,pageSize);
    	PageInfo pageInfo = new PageInfo(tbZhQkssMapper.queryqk(parmMap));
		ResultDto result = new ResultDto();
		result.setRows(pageInfo.getList());
		result.setPage(pageInfo.getPageNum());
		//result.setTotal(pageInfo.getPages());
		result.setTotal((int)pageInfo.getTotal());
		return result;
    }

}
