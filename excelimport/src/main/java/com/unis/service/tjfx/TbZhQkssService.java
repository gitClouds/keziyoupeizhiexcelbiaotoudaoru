package com.unis.service.tjfx;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import com.unis.model.tjfx.TbZhQkss;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.unis.service.BaseService;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;

/**
 * <pre>
 * TODO
 * </pre>
 *
 * @author gongjl
 * @version 1.0
 * @since 2020-06-05
 */
public interface TbZhQkssService extends BaseService {


	/**
     * 根据入参获取数据集合(分页)
     * @param parmMap
     * @param pageNum
     * @param pageSize
     * @return
     * @throws Exception
     */
    ResultDto queryListByPage(Map<Object, Object> parmMap, int pageNum, int pageSize) throws  Exception;

}
