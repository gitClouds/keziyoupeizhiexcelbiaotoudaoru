package com.unis.service.tjfx.impl;

import java.util.List;
import java.util.Map;

import com.unis.common.util.TemplateUtil;
import com.unis.common.util.TimeUtil;
import com.unis.dto.ResultDto;
import com.unis.mapper.tjfx.XskMapper;
import com.unis.model.tjfx.Xsk;
import com.unis.service.tjfx.XskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see XskService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2020-03-03
 */
@Service("xskService")
public class XskServiceImpl extends BaseServiceImpl implements XskService {
	private static final Logger logger = LoggerFactory.getLogger(XskServiceImpl.class);
    @Autowired
    private XskMapper xskMapper;

    /**
     * @see XskService#insert(Xsk xsk)
     */
    @Override
    public int insert(Xsk xsk) throws Exception {
    	if (xsk!=null){
	        xsk.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return xskMapper.insertSelective(xsk);
    	}else{
    		logger.error("xskServiceImpl.insert时xsk数据为空。");
    		throw new AppRuntimeException("xskServiceImpl.insert时xsk数据为空。");
    	}        
    }

    /**
     * @see XskService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("xskServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("xskServiceImpl.delete时pk为空。");
    	}else{
    		return xskMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see XskService#updateByPrimaryKey(Xsk xsk)
     */
    @Override
    public int updateByPrimaryKey(Xsk xsk) throws Exception {
        if (xsk!=null){
        	if(StringUtils.isBlank(xsk.getPk())){
        		logger.error("xskServiceImpl.updateByPrimaryKey时xsk.Pk为空。");
        		throw new AppRuntimeException("xskServiceImpl.updateByPrimaryKey时xsk.Pk为空。");
        	}
	        return xskMapper.updateByPrimaryKeySelective(xsk);
    	}else{
    		logger.error("xskServiceImpl.updateByPrimaryKey时xsk数据为空。");
    		throw new AppRuntimeException("xskServiceImpl.updateByPrimaryKey时xsk数据为空。");
    	}
    }
    /**
     * @see XskService#queryxskByPrimaryKey(String pk)
     */
    @Override
    public Xsk queryxskByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("xskServiceImpl.queryxskByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("xskServiceImpl.queryxskByPrimaryKey时pk为空。");
    	}else{
    		return xskMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see XskService#queryAsObject(Xsk xsk)
     */
    @Override
    public Xsk queryAsObject(Xsk xsk) throws Exception {
        if (xsk!=null){
	        return xskMapper.selectOne(xsk);
    	}else{
    		logger.error("xskServiceImpl.queryAsObject时xsk数据为空。");
    		throw new AppRuntimeException("xskServiceImpl.queryAsObject时xsk数据为空。");
    	}
    }
    
    /**
     * @see XskService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return xskMapper.selectCountByExample(example);
    	}else{
    		logger.error("xskServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("xskServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see XskService#queryListByExample(Example example)
     */
    @Override
    public List<Xsk> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return xskMapper.selectByExample(example);
    	}else{
    		logger.error("xskServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("xskServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see XskService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(xskMapper.selectByExample(example));
    	}else{
    		logger.error("xskServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("xskServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see XskService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(Xsk.class);
    		example.setOrderByClause("jf DESC,rksj DESC");//设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		criteria.andEqualTo("yxx",1);//设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zh")!=null && StringUtils.isNotBlank(parmMap.get("zh").toString())){
				criteria.andEqualTo("zh",parmMap.get("zh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xm")!=null && StringUtils.isNotBlank(parmMap.get("xm").toString())){
				criteria.andEqualTo("xm",parmMap.get("xm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfzh")!=null && StringUtils.isNotBlank(parmMap.get("sfzh").toString())){
				criteria.andEqualTo("sfzh",parmMap.get("sfzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("nc")!=null && StringUtils.isNotBlank(parmMap.get("nc").toString())){
				criteria.andEqualTo("nc",parmMap.get("nc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj", TimeUtil.strToDate(parmMap.get("rksj").toString().trim(),"yyyy-MM-dd"));
				flag = true;
			}
    		if(parmMap.get("lrdwdm")!=null && StringUtils.isNotBlank(parmMap.get("lrdwdm").toString())){
				criteria.andEqualTo("lrdwdm",parmMap.get("lrdwdm").toString().trim());
				flag = true;
			}
			if(parmMap.get("lrdwmc")!=null && StringUtils.isNotBlank(parmMap.get("lrdwmc").toString())){
				criteria.andEqualTo("lrdwmc",parmMap.get("lrdwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jf")!=null && StringUtils.isNotBlank(parmMap.get("jf").toString())){
				criteria.andEqualTo("jf",parmMap.get("jf").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jfms")!=null && StringUtils.isNotBlank(parmMap.get("jfms").toString())){
    			String ms = (String) parmMap.get("jfms");
				if (ms.equals("1")){
					criteria.andGreaterThan("gxr",0);
				}else if(ms.equals("2")){
					criteria.andGreaterThan("sdgxr",0);
				}else if(ms.equals("3")){
					criteria.andGreaterThan("sdje",0);
				}else if(ms.equals("4")){
					criteria.andGreaterThan("zje",0);
				}else if(ms.equals("5")){
					criteria.andGreaterThan("fxf",0);
				}
				flag = true;
			}
    		if(parmMap.get("gxr")!=null && StringUtils.isNotBlank(parmMap.get("gxr").toString())){
				criteria.andEqualTo("gxr",parmMap.get("gxr").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sdgxr")!=null && StringUtils.isNotBlank(parmMap.get("sdgxr").toString())){
				criteria.andEqualTo("sdgxr",parmMap.get("sdgxr").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zje")!=null && StringUtils.isNotBlank(parmMap.get("zje").toString())){
				criteria.andEqualTo("zje",parmMap.get("zje").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sdje")!=null && StringUtils.isNotBlank(parmMap.get("sdje").toString())){
				criteria.andEqualTo("sdje",parmMap.get("sdje").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fxf")!=null && StringUtils.isNotBlank(parmMap.get("fxf").toString())){
				criteria.andEqualTo("fxf",parmMap.get("fxf").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zhdwdm")!=null && StringUtils.isNotBlank(parmMap.get("zhdwdm").toString())){
				criteria.andEqualTo("zhdwdm",parmMap.get("zhdwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zhdwmc")!=null && StringUtils.isNotBlank(parmMap.get("zhdwmc").toString())){
				criteria.andEqualTo("zhdwmc",parmMap.get("zhdwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
			if(parmMap.get("zhpk")!=null && StringUtils.isNotBlank(parmMap.get("zhpk").toString())){
				criteria.andEqualTo("zhpk",parmMap.get("zhpk").toString().trim());
				flag = true;
			}
			if(parmMap.get("zhjgmc")!=null && StringUtils.isNotBlank(parmMap.get("zhjgmc").toString())){
				criteria.andEqualTo("zhjgmc",parmMap.get("zhjgmc").toString().trim());
				flag = true;
			}
			if(parmMap.get("zhjgdm")!=null && StringUtils.isNotBlank(parmMap.get("zhjgdm").toString())){
				criteria.andEqualTo("zhjgdm",parmMap.get("zhjgdm").toString().trim());
				flag = true;
			}
			if(parmMap.get("nlevel")!=null && StringUtils.isNotBlank(parmMap.get("nlevel").toString())){
				criteria.andEqualTo("nlevel",parmMap.get("nlevel").toString().trim());
				flag = true;
			}
			if(parmMap.get("jltpk")!=null && StringUtils.isNotBlank(parmMap.get("jltpk").toString())){
				criteria.andEqualTo("jltpk",parmMap.get("jltpk").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("xskServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("xskServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }

	@Override
	public ResultDto queryjoin(Map parmMap, int pageNum, int pageSize) throws Exception {
		PageHelper.startPage(pageNum,pageSize);
		PageInfo pageInfo = new PageInfo(xskMapper.queryjoin(parmMap));
		ResultDto result = new ResultDto();
		result.setRows(pageInfo.getList());
		result.setPage(pageInfo.getPageNum());
		//result.setTotal(pageInfo.getPages());
		result.setTotal((int)pageInfo.getTotal());
		return result;
	}

	@Override
	public Xsk queryByJltpk(String jltpk) throws Exception {
		if (jltpk!=null){
			return xskMapper.queryByJltpk(jltpk);
		}else{
			logger.error("xskServiceImpl.queryByJltpk时jltpk数据为空。");
			throw new AppRuntimeException("xskServiceImpl.queryByJltpk时jltpkk数据为空。");
		}
	}
}
