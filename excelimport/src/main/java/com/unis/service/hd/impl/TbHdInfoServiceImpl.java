package com.unis.service.hd.impl;

import java.util.List;
import java.util.Map;

import com.unis.common.secure.authc.UserInfo;
import com.unis.common.util.TemplateUtil;
import com.unis.dto.ResultDto;
import com.unis.model.zhxx.TbJdzjfxSfZh;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.hd.TbHdInfoMapper;
import com.unis.model.hd.TbHdInfo;
import com.unis.service.hd.TbHdInfoService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see TbHdInfoService
 * </pre>
 *
 * @author fdzptwx
 * @version 1.0
 * @since 2020-03-09
 */
@Service("tbHdInfoService")
public class TbHdInfoServiceImpl extends BaseServiceImpl implements TbHdInfoService {
    private static final Logger logger = LoggerFactory.getLogger(TbHdInfoServiceImpl.class);
    @Autowired
    private TbHdInfoMapper tbHdInfoMapper;

    /**
     * @see TbHdInfoService#insert(TbHdInfo tbHdInfo)
     */
    @Override
    public int insert(TbHdInfo tbHdInfo) throws Exception {
        if (tbHdInfo != null) {
            //tbHdInfo.setHd_id(TemplateUtil.genUUID());
            //menu.setHd_id(getPk("seqName","jgdm","A"));

            return tbHdInfoMapper.insertSelective(tbHdInfo);
        } else {
            logger.error("TbHdInfoServiceImpl.insert时tbHdInfo数据为空。");
            throw new AppRuntimeException("TbHdInfoServiceImpl.insert时tbHdInfo数据为空。");
        }
    }

    @Override
    public int insertToZhInfo(TbJdzjfxSfZh tbJdzjfxSfZh) throws Exception {
        tbJdzjfxSfZh.setHd_cx(0);
        if (tbJdzjfxSfZh == null || StringUtils.isBlank(tbJdzjfxSfZh.getLxfs())) {
            logger.info("TbHdInfoServiceImpl.insertToZhInfo时根据号码增加话单信息,无联系方式" + tbJdzjfxSfZh.getId());
            return -1;
        }
        tbJdzjfxSfZh.setHd_cx(1);
        logger.info("TbHdInfoServiceImpl.insertToZhInfo时根据号码增加话单信息,ID:" + tbJdzjfxSfZh.getId() + ",电话:" + tbJdzjfxSfZh.getLxfs());
        TbHdInfo tbHdInfo = new TbHdInfo();
        tbHdInfo.setHd_hm(tbJdzjfxSfZh.getLxfs());
        UserInfo userInfo = this.getUserInfo();
        tbHdInfo.setHd_ly(1);//系统录入
        tbHdInfo.setHd_lrr_dm(userInfo.getJh());
        tbHdInfo.setHd_lrr_xm(userInfo.getXm());
        tbHdInfo.setHd_lrr_dw_mc(userInfo.getJgmc());
        tbHdInfo.setHd_lrr_dw_dm(userInfo.getJgdm());
        tbHdInfo.setZh_id(tbJdzjfxSfZh.getId());
        tbHdInfo.setHd_id(TemplateUtil.genUUID());
        tbHdInfo.setHd_hz(tbJdzjfxSfZh.getXm());
        return tbHdInfoMapper.insertSelective(tbHdInfo);
    }

    /**
     * @see TbHdInfoService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
        if (StringUtils.isBlank(pk)) {
            logger.error("TbHdInfoServiceImpl.delete时pk为空。");
            throw new AppRuntimeException("TbHdInfoServiceImpl.delete时pk为空。");
        } else {
            return tbHdInfoMapper.deleteByPrimaryKey(pk);
        }
    }

    /**
     * @see TbHdInfoService#updateByPrimaryKey(TbHdInfo tbHdInfo)
     */
    @Override
    public int updateByPrimaryKey(TbHdInfo tbHdInfo) throws Exception {
        if (tbHdInfo != null) {
            if (StringUtils.isBlank(tbHdInfo.getHd_id())) {
                logger.error("TbHdInfoServiceImpl.updateByPrimaryKey时tbHdInfo.Hd_id为空。");
                throw new AppRuntimeException("TbHdInfoServiceImpl.updateByPrimaryKey时tbHdInfo.Hd_id为空。");
            }
            return tbHdInfoMapper.updateByPrimaryKeySelective(tbHdInfo);
        } else {
            logger.error("TbHdInfoServiceImpl.updateByPrimaryKey时tbHdInfo数据为空。");
            throw new AppRuntimeException("TbHdInfoServiceImpl.updateByPrimaryKey时tbHdInfo数据为空。");
        }
    }

    /**
     * @see TbHdInfoService#queryTbHdInfoByPrimaryKey(String pk)
     */
    @Override
    public TbHdInfo queryTbHdInfoByPrimaryKey(String pk) throws Exception {
        if (StringUtils.isBlank(pk)) {
            logger.error("TbHdInfoServiceImpl.queryTbHdInfoByPrimaryKey时pk为空。");
            throw new AppRuntimeException("TbHdInfoServiceImpl.queryTbHdInfoByPrimaryKey时pk为空。");
        } else {
            return tbHdInfoMapper.selectByPrimaryKey(pk);
        }
    }


    /**
     * @see TbHdInfoService#queryAsObject(TbHdInfo tbHdInfo)
     */
    @Override
    public TbHdInfo queryAsObject(TbHdInfo tbHdInfo) throws Exception {
        if (tbHdInfo != null) {
            return tbHdInfoMapper.selectOne(tbHdInfo);
        } else {
            logger.error("TbHdInfoServiceImpl.queryAsObject时tbHdInfo数据为空。");
            throw new AppRuntimeException("TbHdInfoServiceImpl.queryAsObject时tbHdInfo数据为空。");
        }
    }

    /**
     * @see TbHdInfoService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
        if (example != null) {
            return tbHdInfoMapper.selectCountByExample(example);
        } else {
            logger.error("TbHdInfoServiceImpl.queryCountByExample时example数据为空。");
            throw new AppRuntimeException("TbHdInfoServiceImpl.queryCountByExample时example数据为空。");
        }
    }

    /**
     * @see TbHdInfoService#queryListByExample(Example example)
     */
    @Override
    public List<TbHdInfo> queryListByExample(Example example) throws Exception {
        if (example != null) {
            return tbHdInfoMapper.selectByExample(example);
        } else {
            logger.error("TbHdInfoServiceImpl.queryListByExample时example数据为空。");
            throw new AppRuntimeException("TbHdInfoServiceImpl.queryListByExample时example数据为空。");
        }
    }

    /**
     * @see TbHdInfoService#queryPageInfoByExample(Example example, int pageNum, int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example, int pageNum, int pageSize) throws Exception {
        if (example != null) {
            PageHelper.startPage(pageNum, pageSize);
            return new PageInfo(tbHdInfoMapper.selectByExample(example));
        } else {
            logger.error("TbHdInfoServiceImpl.queryListByExample时example数据为空。");
            throw new AppRuntimeException("TbHdInfoServiceImpl.queryListByExample时example数据为空。");
        }
    }

    /**
     * @see TbHdInfoService#queryListByPage(Map parmMap, int pageNum, int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws Exception {
        if (parmMap != null) {
            Example example = new Example(TbHdInfo.class);
            //example.setOrderByClause("rksj DESC,pk asc");设置排序
            Example.Criteria criteria = example.createCriteria();
            //criteria设置
            // = --> andEqualTo(field,value)
            // like --> andLike(field,likeValue)；likeValue 包含‘%’
            // is null --> andIsNull(field)
            // is not null --> andIsNotNull(field)
            // <> --> andNotEqualTo(field)
            // > --> andGreaterThan(field,value)
            // >= --> andGreaterThanOrEqualTo(field,value)
            // < --> andLessThan(field,value)
            // <= --> andLessThanOrEqualTo(field,value)
            // in --> andIn(field,Iterable value)
            // not in --> andNotIn(field,Iterable value)
            // between --> andBetween(field,beginValue,endValue)
            // not like --> andNotLike(field,likeValue)；likeValue 包含‘%’

            // or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)

            //criteria.andEqualTo("yxx",1);设置yxx=1
            /**
             *此方法请根据需要修改下方条件
             */
            //此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
            //生成条件中均为 and field = value
            //生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改

            boolean flag = false;
            if (parmMap.get("hd_id") != null && StringUtils.isNotBlank(parmMap.get("hd_id").toString())) {
                criteria.andEqualTo("hd_id", parmMap.get("hd_id").toString().trim());
                flag = true;
            }
            if (parmMap.get("hd_hm") != null && StringUtils.isNotBlank(parmMap.get("hd_hm").toString())) {
                criteria.andEqualTo("hd_hm", parmMap.get("hd_hm").toString().trim());
                flag = true;
            }
            if (parmMap.get("hd_hz") != null && StringUtils.isNotBlank(parmMap.get("hd_hz").toString())) {
                criteria.andEqualTo("hd_hz", parmMap.get("hd_hz").toString().trim());
                flag = true;
            }
            if (parmMap.get("hd_ly") != null && StringUtils.isNotBlank(parmMap.get("hd_ly").toString())) {
                criteria.andEqualTo("hd_ly", parmMap.get("hd_ly").toString().trim());
                flag = true;
            }
            if (parmMap.get("hd_lrr_xm") != null && StringUtils.isNotBlank(parmMap.get("hd_lrr_xm").toString())) {
                criteria.andEqualTo("hd_lrr_xm", parmMap.get("hd_lrr_xm").toString().trim());
                flag = true;
            }
            if (parmMap.get("hd_lrr_dm") != null && StringUtils.isNotBlank(parmMap.get("hd_lrr_dm").toString())) {
                criteria.andEqualTo("hd_lrr_dm", parmMap.get("hd_lrr_dm").toString().trim());
                flag = true;
            }
            if (parmMap.get("hd_lrr_dw_mc") != null && StringUtils.isNotBlank(parmMap.get("hd_lrr_dw_mc").toString())) {
                criteria.andEqualTo("hd_lrr_dw_mc", parmMap.get("hd_lrr_dw_mc").toString().trim());
                flag = true;
            }
            if (parmMap.get("hd_lrr_dw_dm") != null && StringUtils.isNotBlank(parmMap.get("hd_lrr_dw_dm").toString())) {
                criteria.andEqualTo("hd_lrr_dw_dm", parmMap.get("hd_lrr_dw_dm").toString().trim());
                flag = true;
            }
            if (parmMap.get("hd_yxx") != null && StringUtils.isNotBlank(parmMap.get("hd_yxx").toString())) {
                criteria.andEqualTo("hd_yxx", parmMap.get("hd_yxx").toString().trim());
                flag = true;
            }
            if (parmMap.get("hd_lrsj") != null && StringUtils.isNotBlank(parmMap.get("hd_lrsj").toString())) {
                criteria.andEqualTo("hd_lrsj", parmMap.get("hd_lrsj").toString().trim());
                flag = true;
            }
            if (parmMap.get("hd_phone_type") != null && StringUtils.isNotBlank(parmMap.get("hd_phone_type").toString())) {
                criteria.andEqualTo("hd_phone_type", parmMap.get("hd_phone_type").toString().trim());
                flag = true;
            }
            if (parmMap.get("hd_cx") != null && StringUtils.isNotBlank(parmMap.get("hd_cx").toString())) {
                criteria.andEqualTo("hd_cx", parmMap.get("hd_cx").toString().trim());
                flag = true;
            }
            if (parmMap.get("hd_last_cxsj") != null && StringUtils.isNotBlank(parmMap.get("hd_last_cxsj").toString())) {
                criteria.andEqualTo("hd_last_cxsj", parmMap.get("hd_last_cxsj").toString().trim());
                flag = true;
            }
            example.setOrderByClause("HD_LRSJ DESC");
            //一个条件都没有的时候给一个默认条件
            if (!flag) {

            }
            PageInfo pageInfo = this.queryPageInfoByExample(example, pageNum, pageSize);
            ResultDto result = new ResultDto();
            result.setRows(pageInfo.getList());
            result.setPage(pageInfo.getPageNum());
            //result.setTotal(pageInfo.getPages());
            result.setTotal((int) pageInfo.getTotal());
            return result;
        } else {
            logger.error("TbHdInfoServiceImpl.queryListByPage时parmMap数据为空。");
            throw new AppRuntimeException("TbHdInfoServiceImpl.queryListByPage时parmMap数据为空。");
        }
    }
}
