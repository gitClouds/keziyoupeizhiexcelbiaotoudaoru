package com.unis.service.hd;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import com.unis.model.hd.TbHdInfo;
import com.unis.model.zhxx.TbJdzjfxSfZh;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.unis.service.BaseService;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;

/**
 * <pre>
 * TODO
 * </pre>
 *
 * @author fdzptwx
 * @version 1.0
 * @since 2020-03-09
 */
public interface TbHdInfoService extends BaseService {

    /**
     * 新增TbHdInfo实例
     * 
     * @param tbHdInfo
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int insert(TbHdInfo tbHdInfo) throws Exception;

    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int insertToZhInfo(TbJdzjfxSfZh tbJdzjfxSfZh) throws Exception;
    /**
     * 删除TbHdInfo实例
     * 
     * @param pk
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int delete(String pk) throws Exception;

    /**
     * 更新TbHdInfo实例
     * 
     * @param tbHdInfo
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int updateByPrimaryKey(TbHdInfo tbHdInfo) throws Exception;
    /**
     * 根据pk主键获取对象
     * 
     * @param pk
     * @throws Exception
     */
    TbHdInfo queryTbHdInfoByPrimaryKey(String pk) throws Exception;

    /**
     * 查询TbHdInfo实例
     * 
     * @param tbHdInfo
     * @throws Exception
     */
    TbHdInfo queryAsObject(TbHdInfo tbHdInfo) throws Exception;
    
    	
	/**
     * 根据条件获取条数
     * 
     * @param example
     * @throws Exception
     */
	int queryCountByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合
     * 
     * @param example
     * @throws Exception
     */
	List<TbHdInfo> queryListByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合(分页),需在example 设定排序
     * 
     * @param example
     * @param pageNum
     * @param pageSize
     * @throws Exception
     */
	PageInfo queryPageInfoByExample(Example example, int pageNum, int pageSize) throws Exception;

	/**
     * 根据入参获取数据集合(分页)
     * @param parmMap
     * @param pageNum
     * @param pageSize
     * @return
     * @throws Exception
     */
    ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception;
    
    

}
