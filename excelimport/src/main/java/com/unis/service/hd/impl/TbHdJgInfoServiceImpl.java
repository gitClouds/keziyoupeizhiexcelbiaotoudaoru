package com.unis.service.hd.impl;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.ObjectUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.hd.TbHdJgInfoMapper;
import com.unis.model.hd.TbHdJgInfo;
import com.unis.service.hd.TbHdJgInfoService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see TbHdJgInfoService
 * </pre>
 *
 * @author fdzptwx
 * @version 1.0
 * @since 2020-03-09
 */
@Service("tbHdJgInfoService")
public class TbHdJgInfoServiceImpl extends BaseServiceImpl implements TbHdJgInfoService {
	private static final Logger logger = LoggerFactory.getLogger(TbHdJgInfoServiceImpl.class);
    @Autowired
    private TbHdJgInfoMapper tbHdJgInfoMapper;

    /**
     * @see TbHdJgInfoService#insert(TbHdJgInfo tbHdJgInfo)
     */
    @Override
    public int insert(TbHdJgInfo tbHdJgInfo) throws Exception {
    	if (tbHdJgInfo!=null){
	        //tbHdJgInfo.setJg_id(TemplateUtil.genUUID());
	        //menu.setJg_id(getPk("seqName","jgdm","A"));
	                
	        return tbHdJgInfoMapper.insertSelective(tbHdJgInfo);
    	}else{
    		logger.error("TbHdJgInfoServiceImpl.insert时tbHdJgInfo数据为空。");
    		throw new AppRuntimeException("TbHdJgInfoServiceImpl.insert时tbHdJgInfo数据为空。");
    	}        
    }

    /**
     * @see TbHdJgInfoService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("TbHdJgInfoServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("TbHdJgInfoServiceImpl.delete时pk为空。");
    	}else{
    		return tbHdJgInfoMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see TbHdJgInfoService#updateByPrimaryKey(TbHdJgInfo tbHdJgInfo)
     */
    @Override
    public int updateByPrimaryKey(TbHdJgInfo tbHdJgInfo) throws Exception {
        if (tbHdJgInfo!=null){
        	if(StringUtils.isBlank(tbHdJgInfo.getJg_id())){
        		logger.error("TbHdJgInfoServiceImpl.updateByPrimaryKey时tbHdJgInfo.Jg_id为空。");
        		throw new AppRuntimeException("TbHdJgInfoServiceImpl.updateByPrimaryKey时tbHdJgInfo.Jg_id为空。");
        	}
	        return tbHdJgInfoMapper.updateByPrimaryKeySelective(tbHdJgInfo);
    	}else{
    		logger.error("TbHdJgInfoServiceImpl.updateByPrimaryKey时tbHdJgInfo数据为空。");
    		throw new AppRuntimeException("TbHdJgInfoServiceImpl.updateByPrimaryKey时tbHdJgInfo数据为空。");
    	}
    }
    /**
     * @see TbHdJgInfoService#queryTbHdJgInfoByPrimaryKey(String pk)
     */
    @Override
    public TbHdJgInfo queryTbHdJgInfoByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("TbHdJgInfoServiceImpl.queryTbHdJgInfoByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("TbHdJgInfoServiceImpl.queryTbHdJgInfoByPrimaryKey时pk为空。");
    	}else{
    		return tbHdJgInfoMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see TbHdJgInfoService#queryAsObject(TbHdJgInfo tbHdJgInfo)
     */
    @Override
    public TbHdJgInfo queryAsObject(TbHdJgInfo tbHdJgInfo) throws Exception {
        if (tbHdJgInfo!=null){
	        return tbHdJgInfoMapper.selectOne(tbHdJgInfo);
    	}else{
    		logger.error("TbHdJgInfoServiceImpl.queryAsObject时tbHdJgInfo数据为空。");
    		throw new AppRuntimeException("TbHdJgInfoServiceImpl.queryAsObject时tbHdJgInfo数据为空。");
    	}
    }
    
    /**
     * @see TbHdJgInfoService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return tbHdJgInfoMapper.selectCountByExample(example);
    	}else{
    		logger.error("TbHdJgInfoServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("TbHdJgInfoServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see TbHdJgInfoService#queryListByExample(Example example)
     */
    @Override
    public List<TbHdJgInfo> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return tbHdJgInfoMapper.selectByExample(example);
    	}else{
    		logger.error("TbHdJgInfoServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("TbHdJgInfoServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see TbHdJgInfoService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(tbHdJgInfoMapper.selectByExample(example));
    	}else{
    		logger.error("TbHdJgInfoServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("TbHdJgInfoServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see TbHdJgInfoService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(TbHdJgInfo.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("jg_id")!=null && StringUtils.isNotBlank(parmMap.get("jg_id").toString())){
				criteria.andEqualTo("jg_id",parmMap.get("jg_id").toString().trim());
				flag = true;
			}
    		if(parmMap.get("hd_id")!=null && StringUtils.isNotBlank(parmMap.get("hd_id").toString())){
				criteria.andEqualTo("hd_id",parmMap.get("hd_id").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jg_hm")!=null && StringUtils.isNotBlank(parmMap.get("jg_hm").toString())){
				criteria.andEqualTo("jg_hm",parmMap.get("jg_hm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jg_hmlb")!=null && StringUtils.isNotBlank(parmMap.get("jg_hmlb").toString())){
				criteria.andEqualTo("jg_hmlb",parmMap.get("jg_hmlb").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jg_yyslx")!=null && StringUtils.isNotBlank(parmMap.get("jg_yyslx").toString())){
				criteria.andEqualTo("jg_yyslx",parmMap.get("jg_yyslx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jg_createtime")!=null && StringUtils.isNotBlank(parmMap.get("jg_createtime").toString())){
				criteria.andEqualTo("jg_createtime",parmMap.get("jg_createtime").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jg_evidendesc")!=null && StringUtils.isNotBlank(parmMap.get("jg_evidendesc").toString())){
				criteria.andEqualTo("jg_evidendesc",parmMap.get("jg_evidendesc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jg_title")!=null && StringUtils.isNotBlank(parmMap.get("jg_title").toString())){
				criteria.andEqualTo("jg_title",parmMap.get("jg_title").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jg_file_dir")!=null && StringUtils.isNotBlank(parmMap.get("jg_file_dir").toString())){
				criteria.andEqualTo("jg_file_dir",parmMap.get("jg_file_dir").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jg_filepassword")!=null && StringUtils.isNotBlank(parmMap.get("jg_filepassword").toString())){
				criteria.andEqualTo("jg_filepassword",parmMap.get("jg_filepassword").toString().trim());
				flag = true;
			}
    		/*if(parmMap.get("jg_lrsj")!=null && StringUtils.isNotBlank(parmMap.get("jg_lrsj").toString())){
				criteria.andEqualTo("jg_lrsj",parmMap.get("jg_lrsj").toString().trim());
				flag = true;
			}*/
			if(!ObjectUtils.isEmpty(parmMap.get("jg_begin")) && !ObjectUtils.isEmpty(parmMap.get("jg_end"))){
				criteria.andBetween("jg_lrsj", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(String.valueOf(parmMap.get("jg_begin"))), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(String.valueOf(parmMap.get("jg_end"))));
				flag = true;
			}
    		if(parmMap.get("fj_mong_id")!=null && StringUtils.isNotBlank(parmMap.get("fj_mong_id").toString())){
				criteria.andEqualTo("fj_mong_id",parmMap.get("fj_mong_id").toString().trim());
				flag = true;
			}
    		example.setOrderByClause("JG_LRSJ DESC");
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("TbHdJgInfoServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("TbHdJgInfoServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
