package com.unis.service.impl;

import com.unis.common.secure.authc.UserInfo;
import com.unis.common.util.TimeUtil;
import com.unis.service.BaseService;
import com.unis.service.system.SysPkService;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Date;

/**
 * Created by Administrator on 2018/12/29/029.
 */
@Service("baseService")
public class BaseServiceImpl implements BaseService {
    @Autowired
    private SysPkService sysPkService;

    /**
     * @see BaseService#getUserInfo()
     * @return
     */
    @Override
    public UserInfo getUserInfo() {
        Subject currentUser = SecurityUtils.getSubject();
        return (UserInfo) currentUser.getPrincipal();
    }

    /**
     * @see BaseService#getStrDbDate(String)
     * @param pattern 此格式为java的格式化
     * @return
     * @throws ParseException
     */
    @Override
    public String getStrDbDate(String pattern) throws ParseException{
        String strDate = null;
        if (!StringUtils.isNotBlank(pattern)){
            strDate = getStrDbDate();
        }else{
            Date dbDate = getDbDate();
            strDate = TimeUtil.fmtDate(dbDate,pattern);
        }

        return  strDate;
    }

    /**
     * @see BaseService#getStrDbDate()
     * @return
     */
    @Override
    public String getStrDbDate(){
        return sysPkService.getDbDate();
    }

    /**
     * @see BaseService#getDbDate()
     * @return
     * @throws ParseException
     */
    @Override
    public Date getDbDate() throws ParseException {
        Date date = null;
        String dbDate = getStrDbDate();
        date = TimeUtil.strToDate(dbDate,null);
        return date;
    }

    /**
     * @see BaseService#getPk(String, String, String)
     * @param seqName sequence名
     * @param dwdm 单位
     * @param prefix 前缀 the prefix length  between 1 and 3
     * @return
     * @throws Exception
     */
    @Override
    public String getPk(String seqName, String dwdm, String prefix) throws Exception{
        return sysPkService.getPrimaryKey(seqName,dwdm,prefix);
    }
}
