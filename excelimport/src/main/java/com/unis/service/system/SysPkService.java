package com.unis.service.system;

import org.apache.ibatis.annotations.Param;
/**
 * <pre>
 * TODO
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2018-12-28
 */
public interface SysPkService {
    /**
     * 主键生成（32位）
     * prefix+dwdm+ yyyyMMdd + (sequence.nextval.substr(length-8))
     * 长度pkLength=32 = prefix.length + dwdm.length(12) + yyyyMMdd(8) +  sequence.substr(length-8) (8) + Random(32-28-prefix.length)(minlenght=1;maxlength=3)
     * @param seqName sequence名
     * @param dwdm 单位
     * @param prefix 前缀 the prefix length  between 1 and 3
     * @return pk
     * @throws Exception
     */
    String getPrimaryKey(@Param("seqName")String seqName, @Param("dwdm")String dwdm, @Param("prefix")String prefix) throws Exception;

    /**
     * 获取数据库时间(格式为：yyyy-MM-dd HH:mm:ss)
     * @return
     */
    String getDbDate();
}
