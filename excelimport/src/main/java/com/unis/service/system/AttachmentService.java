package com.unis.service.system;

import com.unis.pojo.Attachment;
import com.unis.common.exception.app.UploadFailureException;
import com.unis.pojo.CxFlwsJtl;
import com.unis.pojo.ZfflwsJtl;
import com.unis.service.BaseService;
import org.springframework.web.multipart.MultipartFile;

import java.awt.Image;
import java.io.IOException;
import java.util.List;

/**
 * Created by Administrator on 2019/1/30/030.
 */
public interface AttachmentService extends BaseService {

    /**
     * 上传
     *
     * @param attachment
     * @return
     * @throws UploadFailureException
     */
    Attachment upload(Attachment attachment) throws UploadFailureException, IOException;

    /**
     * 下载
     *
     * @param attachment
     * @return
     * @throws UploadFailureException
     */
    Attachment download(Attachment attachment) throws UploadFailureException;

    /**
     * 预览(针对图片)
     *
     * @param attachment
     * @return
     * @throws UploadFailureException
     */
    Image preview(Attachment attachment) throws UploadFailureException;

    /**
     * 删除
     *
     * @param attachment
     * @throws UploadFailureException
     */
    void remove(Attachment attachment) throws UploadFailureException;

    /**
     * 根据条件查看附件列表
     *
     * @param attachment
     * @return
     * @throws UploadFailureException
     */
    //List<Attachment> queryAttachments(Attachment attachment) throws UploadFailureException;

}
