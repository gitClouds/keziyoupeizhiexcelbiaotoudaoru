package com.unis.service.system.impl;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.system.SysLogMetaMapper;
import com.unis.model.system.SysLogMeta;
import com.unis.service.system.SysLogMetaService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see SysLogMetaService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-05-22
 */
@Service("sysLogMetaService")
public class SysLogMetaServiceImpl extends BaseServiceImpl implements SysLogMetaService {
	private static final Logger logger = LoggerFactory.getLogger(SysLogMetaServiceImpl.class);
    @Autowired
    private SysLogMetaMapper sysLogMetaMapper;

    /**
     * @see SysLogMetaService#insert(SysLogMeta sysLogMeta)
     */
    @Override
    public int insert(SysLogMeta sysLogMeta) throws Exception {
    	if (sysLogMeta!=null){
	        //sysLogMeta.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return sysLogMetaMapper.insertSelective(sysLogMeta);
    	}else{
    		logger.error("SysLogMetaServiceImpl.insert时sysLogMeta数据为空。");
    		throw new AppRuntimeException("SysLogMetaServiceImpl.insert时sysLogMeta数据为空。");
    	}        
    }

    /**
     * @see SysLogMetaService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("SysLogMetaServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("SysLogMetaServiceImpl.delete时pk为空。");
    	}else{
    		return sysLogMetaMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see SysLogMetaService#updateByPrimaryKey(SysLogMeta sysLogMeta)
     */
    @Override
    public int updateByPrimaryKey(SysLogMeta sysLogMeta) throws Exception {
        if (sysLogMeta!=null){
        	if(StringUtils.isBlank(sysLogMeta.getPk())){
        		logger.error("SysLogMetaServiceImpl.updateByPrimaryKey时sysLogMeta.Pk为空。");
        		throw new AppRuntimeException("SysLogMetaServiceImpl.updateByPrimaryKey时sysLogMeta.Pk为空。");
        	}
	        return sysLogMetaMapper.updateByPrimaryKeySelective(sysLogMeta);
    	}else{
    		logger.error("SysLogMetaServiceImpl.updateByPrimaryKey时sysLogMeta数据为空。");
    		throw new AppRuntimeException("SysLogMetaServiceImpl.updateByPrimaryKey时sysLogMeta数据为空。");
    	}
    }
    /**
     * @see SysLogMetaService#querySysLogMetaByPrimaryKey(String pk)
     */
    @Override
    public SysLogMeta querySysLogMetaByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("SysLogMetaServiceImpl.querySysLogMetaByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("SysLogMetaServiceImpl.querySysLogMetaByPrimaryKey时pk为空。");
    	}else{
    		return sysLogMetaMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see SysLogMetaService#queryAsObject(SysLogMeta sysLogMeta)
     */
    @Override
    public SysLogMeta queryAsObject(SysLogMeta sysLogMeta) throws Exception {
        if (sysLogMeta!=null){
	        return sysLogMetaMapper.selectOne(sysLogMeta);
    	}else{
    		logger.error("SysLogMetaServiceImpl.queryAsObject时sysLogMeta数据为空。");
    		throw new AppRuntimeException("SysLogMetaServiceImpl.queryAsObject时sysLogMeta数据为空。");
    	}
    }
    
    /**
     * @see SysLogMetaService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return sysLogMetaMapper.selectCountByExample(example);
    	}else{
    		logger.error("SysLogMetaServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("SysLogMetaServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see SysLogMetaService#queryListByExample(Example example)
     */
    @Override
    public List<SysLogMeta> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return sysLogMetaMapper.selectByExample(example);
    	}else{
    		logger.error("SysLogMetaServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("SysLogMetaServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see SysLogMetaService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(sysLogMetaMapper.selectByExample(example));
    	}else{
    		logger.error("SysLogMetaServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("SysLogMetaServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see SysLogMetaService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(SysLogMeta.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("czbm")!=null && StringUtils.isNotBlank(parmMap.get("czbm").toString())){
				criteria.andEqualTo("czbm",parmMap.get("czbm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("czdetail")!=null && StringUtils.isNotBlank(parmMap.get("czdetail").toString())){
				criteria.andEqualTo("czdetail",parmMap.get("czdetail").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("SysLogMetaServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("SysLogMetaServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }

	@Cacheable(value = "cachePool",key = "'allLogMeta'")
	@Override
	public Map<String, String> queryAllLogMeta() {
		List<SysLogMeta> sysLogMetas = sysLogMetaMapper.selectAll();
		return sysLogMetas.parallelStream().collect(Collectors.toMap(SysLogMeta::getCzbm,SysLogMeta::getCzdetail));
	}
}
