package com.unis.service.system;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import com.unis.model.system.BatchFreeze;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.unis.service.BaseService;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;

/**
 * <pre>
 * TODO
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-12-20
 */
public interface BatchFreezeService extends BaseService {

    /**
     * 新增BatchFreeze实例
     * 
     * @param batchFreeze
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int insert(BatchFreeze batchFreeze) throws Exception;

    /**
     * 删除BatchFreeze实例
     * 
     * @param pk
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int delete(String pk) throws Exception;

    /**
     * 更新BatchFreeze实例
     * 
     * @param batchFreeze
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int updateByPrimaryKey(BatchFreeze batchFreeze) throws Exception;
    /**
     * 根据pk主键获取对象
     * 
     * @param pk
     * @throws Exception
     */
    BatchFreeze queryBatchFreezeByPrimaryKey(String pk) throws Exception;

    /**
     * 查询BatchFreeze实例
     * 
     * @param batchFreeze
     * @throws Exception
     */
    BatchFreeze queryAsObject(BatchFreeze batchFreeze) throws Exception;
    
    	
	/**
     * 根据条件获取条数
     * 
     * @param example
     * @throws Exception
     */
	int queryCountByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合
     * 
     * @param example
     * @throws Exception
     */
	List<BatchFreeze> queryListByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合(分页),需在example 设定排序
     * 
     * @param example
     * @param pageNum
     * @param pageSize
     * @throws Exception
     */
	PageInfo queryPageInfoByExample(Example example, int pageNum, int pageSize) throws Exception;

	/**
     * 根据入参获取数据集合(分页)
     * @param parmMap
     * @param pageNum
     * @param pageSize
     * @return
     * @throws Exception
     */
    ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception;
    
    

}
