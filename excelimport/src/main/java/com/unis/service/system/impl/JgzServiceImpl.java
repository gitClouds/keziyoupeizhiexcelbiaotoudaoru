package com.unis.service.system.impl;

import java.util.List;
import java.util.Map;

import com.unis.common.secure.authc.UserInfo;
import com.unis.common.util.TemplateUtil;
import com.unis.dto.ResultDto;
import com.unis.pojo.Attachment;
import com.unis.service.system.AttachmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.system.JgzMapper;
import com.unis.model.system.Jgz;
import com.unis.service.system.JgzService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see JgzService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-01-31
 */
@Service("jgzService")
public class JgzServiceImpl extends BaseServiceImpl implements JgzService {
	private static final Logger logger = LoggerFactory.getLogger(JgzServiceImpl.class);
    @Autowired
    private JgzMapper jgzMapper;
	@Autowired
	private AttachmentService attachmentService;
    /**
     * @see JgzService#insert(Jgz jgz)
     */
    @Override
    public int insert(Jgz jgz) throws Exception {
    	if (jgz!=null){
	        jgz.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
			UserInfo user = this.getUserInfo();
			jgz.setLrr(user.getXm());
			jgz.setLrdwmc(user.getJgmc());
			jgz.setLrdwdm(user.getJgdm());
			jgz.setLrrjh(user.getJh());
			/*jgz.setUserpk(user.getPk());
			jgz.setUsername(user.getXm());*/
	        return jgzMapper.insertSelective(jgz);
    	}else{
    		logger.error("JgzServiceImpl.insert时jgz数据为空。");
    		throw new AppRuntimeException("JgzServiceImpl.insert时jgz数据为空。");
    	}        
    }

    /**
     * @see JgzService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("JgzServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("JgzServiceImpl.delete时pk为空。");
    	}else{
			Jgz jgz = this.queryJgzByPrimaryKey(pk);
			this.deleteMongo(jgz.getJbrmongo());
			this.deleteMongo(jgz.getXcrmongo());
    		return jgzMapper.deleteByPrimaryKey(pk);
    	}
    }

	private void deleteMongo(String mongoPk){
		String[] mongo = mongoPk.split("/");
		Attachment attachment = new Attachment();
		attachment.setPk(mongo[2]);
		attachment.setCatalog(mongo[1]);
		attachment.setAttachmentDb(mongo[0]);
		attachmentService.remove(attachment);
	}

    /**
     * @see JgzService#updateByPrimaryKey(Jgz jgz)
     */
    @Override
    public int updateByPrimaryKey(Jgz jgz) throws Exception {
        if (jgz!=null){
        	if(StringUtils.isBlank(jgz.getPk())){
        		logger.error("JgzServiceImpl.updateByPrimaryKey时jgz.Pk为空。");
        		throw new AppRuntimeException("JgzServiceImpl.updateByPrimaryKey时jgz.Pk为空。");
        	}
	        return jgzMapper.updateByPrimaryKeySelective(jgz);
    	}else{
    		logger.error("JgzServiceImpl.updateByPrimaryKey时jgz数据为空。");
    		throw new AppRuntimeException("JgzServiceImpl.updateByPrimaryKey时jgz数据为空。");
    	}
    }
    /**
     * @see JgzService#queryJgzByPrimaryKey(String pk)
     */
    @Override
    public Jgz queryJgzByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("JgzServiceImpl.queryJgzByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("JgzServiceImpl.queryJgzByPrimaryKey时pk为空。");
    	}else{
    		return jgzMapper.selectByPrimaryKey(pk);
    	}
    }
	/**
	 * @see JgzService#queryJgzByUserPkAndDwdm(String,String)
	 */
	@Override
	public Jgz queryJgzByUserPkAndDwdm(String userPk, String dwdm) throws Exception {
		Jgz jgz = null;
		if (StringUtils.isNotBlank(userPk) || StringUtils.isNotBlank(dwdm)){
			List<Jgz> list = jgzMapper.queryJgzByUserPkAndDwdm(userPk, dwdm);
			if (list!=null && !list.isEmpty() && list.size()>0){
				jgz = list.get(0);
			}
		}

		return jgz;
	}


	/**
     * @see JgzService#queryAsObject(Jgz jgz)
     */
    @Override
    public Jgz queryAsObject(Jgz jgz) throws Exception {
        if (jgz!=null){
	        return jgzMapper.selectOne(jgz);
    	}else{
    		logger.error("JgzServiceImpl.queryAsObject时jgz数据为空。");
    		throw new AppRuntimeException("JgzServiceImpl.queryAsObject时jgz数据为空。");
    	}
    }
    
    /**
     * @see JgzService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return jgzMapper.selectCountByExample(example);
    	}else{
    		logger.error("JgzServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("JgzServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see JgzService#queryListByExample(Example example)
     */
    @Override
    public List<Jgz> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return jgzMapper.selectByExample(example);
    	}else{
    		logger.error("JgzServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("JgzServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see JgzService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(jgzMapper.selectByExample(example));
    	}else{
    		logger.error("JgzServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("JgzServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see JgzService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(Jgz.class);
    		example.setOrderByClause("rksj DESC,pk asc");//设置排序
    		Example.Criteria criteria = example.createCriteria();
			criteria.andEqualTo("yxx",1);//设置yxx=1
			Example.Criteria criteria1 = example.createCriteria();
			criteria1.andEqualTo("yxx",1);//设置yxx=1
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’

    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("userpk")!=null && StringUtils.isNotBlank(parmMap.get("userpk").toString())){
				criteria.andEqualTo("userpk",parmMap.get("userpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("username")!=null && StringUtils.isNotBlank(parmMap.get("username").toString())){
				criteria.andEqualTo("username",parmMap.get("username").toString().trim());
				flag = true;
			}
    		if(parmMap.get("dwdm")!=null && StringUtils.isNotBlank(parmMap.get("dwdm").toString())){
				criteria.andEqualTo("dwdm",parmMap.get("dwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("dwmc")!=null && StringUtils.isNotBlank(parmMap.get("dwmc").toString())){
				criteria.andEqualTo("dwmc",parmMap.get("dwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jbrzjhm")!=null && StringUtils.isNotBlank(parmMap.get("jbrzjhm").toString())){
				criteria.andEqualTo("jbrzjhm",parmMap.get("jbrzjhm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jbrxm")!=null && StringUtils.isNotBlank(parmMap.get("jbrxm").toString())){
				criteria.andEqualTo("jbrxm",parmMap.get("jbrxm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jbrdh")!=null && StringUtils.isNotBlank(parmMap.get("jbrdh").toString())){
				criteria.andEqualTo("jbrdh",parmMap.get("jbrdh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jbrmongo")!=null && StringUtils.isNotBlank(parmMap.get("jbrmongo").toString())){
				criteria.andEqualTo("jbrmongo",parmMap.get("jbrmongo").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xcrzjhm")!=null && StringUtils.isNotBlank(parmMap.get("xcrzjhm").toString())){
				criteria.andEqualTo("xcrzjhm",parmMap.get("xcrzjhm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xcrxm")!=null && StringUtils.isNotBlank(parmMap.get("xcrxm").toString())){
				criteria.andEqualTo("xcrxm",parmMap.get("xcrxm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xcrdh")!=null && StringUtils.isNotBlank(parmMap.get("xcrdh").toString())){
				criteria.andEqualTo("xcrdh",parmMap.get("xcrdh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xcrmongo")!=null && StringUtils.isNotBlank(parmMap.get("xcrmongo").toString())){
				criteria.andEqualTo("xcrmongo",parmMap.get("xcrmongo").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrr")!=null && StringUtils.isNotBlank(parmMap.get("lrr").toString())){
				criteria.andEqualTo("lrr",parmMap.get("lrr").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrrjh")!=null && StringUtils.isNotBlank(parmMap.get("lrrjh").toString())){
				criteria.andEqualTo("lrrjh",parmMap.get("lrrjh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrdwdm")!=null && StringUtils.isNotBlank(parmMap.get("lrdwdm").toString())){
				criteria.andEqualTo("lrdwdm",parmMap.get("lrdwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrdwmc")!=null && StringUtils.isNotBlank(parmMap.get("lrdwmc").toString())){
				criteria.andEqualTo("lrdwmc",parmMap.get("lrdwmc").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    			UserInfo user = this.getUserInfo();
				criteria.andEqualTo("userpk",user.getPk());
				criteria1.andEqualTo("dwdm",user.getJgdm());
				example.or(criteria1);
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("JgzServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("JgzServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
