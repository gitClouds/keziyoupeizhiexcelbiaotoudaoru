package com.unis.service.system.impl;

import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.system.DicService;

/**
 * <pre>
 * @see DicService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2018-12-29
 */
@Service("dicService")
public class DicServiceImpl implements DicService {
	private static final Logger logger = LoggerFactory.getLogger(DicServiceImpl.class);

    
}
