package com.unis.service.system;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import com.unis.model.system.Department;
import com.unis.service.BaseService;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;

/**
 * <pre>
 * TODO
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2018-12-27
 */
public interface DepartmentService extends BaseService {

    /**
     * 新增Department实例
     * 
     * @param department
     * @throws Exception
     */
    int insert(Department department) throws Exception;

    /**
     * 删除Department实例
     * 
     * @param pk
     * @throws Exception
     */
    int delete(String pk) throws Exception;

    /**
     * 更新Department实例
     * 
     * @param department
     * @throws Exception
     */
    int updateByPrimaryKey(Department department) throws Exception;
    /**
     * 根据pk主键获取对象
     * 
     * @param pk
     * @throws Exception
     */
    Department queryDepartmentByPrimaryKey(String pk) throws Exception;

    /**
     * 查询Department实例
     * 
     * @param department
     * @throws Exception
     */
    Department queryAsObject(Department department) throws Exception;
    
    	
	/**
     * 根据条件获取条数
     * 
     * @param example
     * @throws Exception
     */
	int queryCountByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合
     * 
     * @param example
     * @throws Exception
     */
	List<Department> queryListByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合(分页),需在example 设定排序
     * 
     * @param example
     * @param pageNum
     * @param pageSize
     * @throws Exception
     */
	PageInfo queryPageInfoByExample(Example example, int pageNum, int pageSize) throws Exception;

    List<Map<String,Object>> selectTreeList(String sjdm,String filterSql,int deptLevel);
    /**
     * 根据入参获取数据集合(分页)
     * @param parmMap
     * @param pageNum
     * @param pageSize
     * @return
     * @throws Exception
     */
    ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception;
}
