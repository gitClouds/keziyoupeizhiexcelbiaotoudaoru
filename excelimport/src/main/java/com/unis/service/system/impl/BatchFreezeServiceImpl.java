package com.unis.service.system.impl;

import java.util.List;
import java.util.Map;

import com.unis.common.util.TemplateUtil;
import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.system.BatchFreezeMapper;
import com.unis.model.system.BatchFreeze;
import com.unis.service.system.BatchFreezeService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see BatchFreezeService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-12-20
 */
@Service("batchFreezeService")
public class BatchFreezeServiceImpl extends BaseServiceImpl implements BatchFreezeService {
	private static final Logger logger = LoggerFactory.getLogger(BatchFreezeServiceImpl.class);
    @Autowired
    private BatchFreezeMapper batchFreezeMapper;

    /**
     * @see BatchFreezeService#insert(BatchFreeze batchFreeze)
     */
    @Override
    public int insert(BatchFreeze batchFreeze) throws Exception {
    	if (batchFreeze!=null){
	        batchFreeze.setId(TemplateUtil.genUUID());
	        return batchFreezeMapper.insertSelective(batchFreeze);
    	}else{
    		logger.error("BatchFreezeServiceImpl.insert时batchFreeze数据为空。");
    		throw new AppRuntimeException("BatchFreezeServiceImpl.insert时batchFreeze数据为空。");
    	}        
    }

    /**
     * @see BatchFreezeService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("BatchFreezeServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("BatchFreezeServiceImpl.delete时pk为空。");
    	}else{
    		return batchFreezeMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see BatchFreezeService#updateByPrimaryKey(BatchFreeze batchFreeze)
     */
    @Override
    public int updateByPrimaryKey(BatchFreeze batchFreeze) throws Exception {
        if (batchFreeze!=null){
        	if(StringUtils.isBlank(batchFreeze.getId())){
        		logger.error("BatchFreezeServiceImpl.updateByPrimaryKey时batchFreeze.Id为空。");
        		throw new AppRuntimeException("BatchFreezeServiceImpl.updateByPrimaryKey时batchFreeze.Id为空。");
        	}
	        return batchFreezeMapper.updateByPrimaryKeySelective(batchFreeze);
    	}else{
    		logger.error("BatchFreezeServiceImpl.updateByPrimaryKey时batchFreeze数据为空。");
    		throw new AppRuntimeException("BatchFreezeServiceImpl.updateByPrimaryKey时batchFreeze数据为空。");
    	}
    }
    /**
     * @see BatchFreezeService#queryBatchFreezeByPrimaryKey(String pk)
     */
    @Override
    public BatchFreeze queryBatchFreezeByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("BatchFreezeServiceImpl.queryBatchFreezeByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("BatchFreezeServiceImpl.queryBatchFreezeByPrimaryKey时pk为空。");
    	}else{
    		return batchFreezeMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see BatchFreezeService#queryAsObject(BatchFreeze batchFreeze)
     */
    @Override
    public BatchFreeze queryAsObject(BatchFreeze batchFreeze) throws Exception {
        if (batchFreeze!=null){
	        return batchFreezeMapper.selectOne(batchFreeze);
    	}else{
    		logger.error("BatchFreezeServiceImpl.queryAsObject时batchFreeze数据为空。");
    		throw new AppRuntimeException("BatchFreezeServiceImpl.queryAsObject时batchFreeze数据为空。");
    	}
    }
    
    /**
     * @see BatchFreezeService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return batchFreezeMapper.selectCountByExample(example);
    	}else{
    		logger.error("BatchFreezeServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("BatchFreezeServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see BatchFreezeService#queryListByExample(Example example)
     */
    @Override
    public List<BatchFreeze> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return batchFreezeMapper.selectByExample(example);
    	}else{
    		logger.error("BatchFreezeServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("BatchFreezeServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see BatchFreezeService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(batchFreezeMapper.selectByExample(example));
    	}else{
    		logger.error("BatchFreezeServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("BatchFreezeServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see BatchFreezeService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(BatchFreeze.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("id")!=null && StringUtils.isNotBlank(parmMap.get("id").toString())){
				criteria.andEqualTo("id",parmMap.get("id").toString().trim());
				flag = true;
			}
    		if(parmMap.get("type")!=null && StringUtils.isNotBlank(parmMap.get("type").toString())){
				criteria.andEqualTo("type",parmMap.get("type").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yhsf")!=null && StringUtils.isNotBlank(parmMap.get("yhsf").toString())){
				criteria.andEqualTo("yhsf",parmMap.get("yhsf").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yhxm")!=null && StringUtils.isNotBlank(parmMap.get("yhxm").toString())){
				criteria.andEqualTo("yhxm",parmMap.get("yhxm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("dwdm")!=null && StringUtils.isNotBlank(parmMap.get("dwdm").toString())){
				criteria.andEqualTo("dwdm",parmMap.get("dwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("dwmc")!=null && StringUtils.isNotBlank(parmMap.get("dwmc").toString())){
				criteria.andEqualTo("dwmc",parmMap.get("dwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfzh")!=null && StringUtils.isNotBlank(parmMap.get("sfzh").toString())){
				criteria.andEqualTo("sfzh",parmMap.get("sfzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("BatchFreezeServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("BatchFreezeServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
