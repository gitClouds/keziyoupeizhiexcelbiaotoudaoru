package com.unis.service.system;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import com.unis.model.system.Permission;
import com.unis.service.BaseService;
import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;

/**
 * <pre>
 * TODO
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2018-12-28
 */
public interface PermissionService extends BaseService {

    /**
     * 新增Permission实例
     *
     * @param permission
     * @throws Exception
     */
    int insert(Permission permission) throws Exception;

    /**
     * 删除Permission实例
     *
     * @param pk
     * @throws Exception
     */
    int delete(String pk) throws Exception;

    /**
     * 更新Permission实例
     *
     * @param permission
     * @throws Exception
     */
    int updateByPrimaryKey(Permission permission) throws Exception;
    /**
     * 根据pk主键获取对象
     *
     * @param pk
     * @throws Exception
     */
    Permission queryPermissionByPrimaryKey(String pk) throws Exception;

    /**
     * 查询Permission实例
     *
     * @param permission
     * @throws Exception
     */
    Permission queryAsObject(Permission permission) throws Exception;


	/**
     * 根据条件获取条数
     *
     * @param example
     * @throws Exception
     */
	int queryCountByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合
     *
     * @param example
     * @throws Exception
     */
	List<Permission> queryListByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合(分页),需在example 设定排序
     *
     * @param example
     * @param pageNum
     * @param pageSize
     * @throws Exception
     */
	PageInfo queryPageInfoByExample(Example example, int pageNum, int pageSize) throws Exception;
    /**
     * 根据userId获取用户权限
     *
     * @param userId
     * @throws Exception
     */
    List<Permission> queryCurrentUserPermit(@Param("userId")String userId) throws Exception;
    /**
     * 根据入参获取数据集合(分页)
     * @param parmMap
     * @param pageNum
     * @param pageSize
     * @return
     * @throws Exception
     */
    ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception;


    /**
     * 获取所有权限，对应类别和外键(角色/单位/用户/菜单)已有权限的标记 checked为true
     * @param cType
     * @param foreingKey
     * @param permType
     * @return
     * @throws Exception
     */
    List<Map<String,String>> queryListByType(String cType,String foreingKey,String permType) throws Exception;

    /**
     * 授权
     * @param cType
     * @param foreingKey
     * @param relatCheckedCode
     * @return
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int relatPerm(String cType,String foreingKey,String relatCheckedCode,String permType) throws Exception;

}
