package com.unis.service.system.impl;

import java.util.List;
import java.util.Map;

import com.unis.common.secure.authc.UserInfo;
import com.unis.common.util.TemplateUtil;
import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.system.SysLogMapper;
import com.unis.model.system.SysLog;
import com.unis.service.system.SysLogService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see SysLogService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-05-22
 */
@Service("sysLogService")
public class SysLogServiceImpl extends BaseServiceImpl implements SysLogService {
	private static final Logger logger = LoggerFactory.getLogger(SysLogServiceImpl.class);
    @Autowired
    private SysLogMapper sysLogMapper;

    /**
     * @see SysLogService#insert(SysLog sysLog)
     */
    @Override
    public int insert(SysLog sysLog) throws Exception {
    	if (sysLog!=null){
	        sysLog.setPk(TemplateUtil.genUUID());
			UserInfo user = this.getUserInfo();
			if (user!=null){
                user.getPermissionCodeSet().iterator()
                        .forEachRemaining(System.out::println);
//                logger.info("用户角色;"+
//                        user.getPermissionCodeSet().iterator()
//                                .forEachRemaining(System.out::println));
				sysLog.setUsername(user.getUsername());
				sysLog.setUserpk(user.getPk());
				sysLog.setIdno(user.getIdno());
				sysLog.setJgdm(user.getJgdm());
				sysLog.setJgmc(user.getJgmc());
				sysLog.setJh(user.getJh());
			}
	        return sysLogMapper.insertSelective(sysLog);
    	}else{
    		logger.error("SysLogServiceImpl.insert时sysLog数据为空。");
    		throw new AppRuntimeException("SysLogServiceImpl.insert时sysLog数据为空。");
    	}        
    }

    /**
     * @see SysLogService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("SysLogServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("SysLogServiceImpl.delete时pk为空。");
    	}else{
    		return sysLogMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see SysLogService#updateByPrimaryKey(SysLog sysLog)
     */
    @Override
    public int updateByPrimaryKey(SysLog sysLog) throws Exception {
        if (sysLog!=null){
        	if(StringUtils.isBlank(sysLog.getPk())){
        		logger.error("SysLogServiceImpl.updateByPrimaryKey时sysLog.Pk为空。");
        		throw new AppRuntimeException("SysLogServiceImpl.updateByPrimaryKey时sysLog.Pk为空。");
        	}
	        return sysLogMapper.updateByPrimaryKeySelective(sysLog);
    	}else{
    		logger.error("SysLogServiceImpl.updateByPrimaryKey时sysLog数据为空。");
    		throw new AppRuntimeException("SysLogServiceImpl.updateByPrimaryKey时sysLog数据为空。");
    	}
    }
    /**
     * @see SysLogService#querySysLogByPrimaryKey(String pk)
     */
    @Override
    public SysLog querySysLogByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("SysLogServiceImpl.querySysLogByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("SysLogServiceImpl.querySysLogByPrimaryKey时pk为空。");
    	}else{
    		return sysLogMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see SysLogService#queryAsObject(SysLog sysLog)
     */
    @Override
    public SysLog queryAsObject(SysLog sysLog) throws Exception {
        if (sysLog!=null){
	        return sysLogMapper.selectOne(sysLog);
    	}else{
    		logger.error("SysLogServiceImpl.queryAsObject时sysLog数据为空。");
    		throw new AppRuntimeException("SysLogServiceImpl.queryAsObject时sysLog数据为空。");
    	}
    }
    
    /**
     * @see SysLogService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return sysLogMapper.selectCountByExample(example);
    	}else{
    		logger.error("SysLogServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("SysLogServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see SysLogService#queryListByExample(Example example)
     */
    @Override
    public List<SysLog> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return sysLogMapper.selectByExample(example);
    	}else{
    		logger.error("SysLogServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("SysLogServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see SysLogService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(sysLogMapper.selectByExample(example));
    	}else{
    		logger.error("SysLogServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("SysLogServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see SysLogService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(SysLog.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("czbm")!=null && StringUtils.isNotBlank(parmMap.get("czbm").toString())){
				criteria.andEqualTo("czbm",parmMap.get("czbm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("flag")!=null && StringUtils.isNotBlank(parmMap.get("flag").toString())){
				criteria.andEqualTo("flag",parmMap.get("flag").toString().trim());
				flag = true;
			}
    		if(parmMap.get("czdata")!=null && StringUtils.isNotBlank(parmMap.get("czdata").toString())){
				criteria.andEqualTo("czdata",parmMap.get("czdata").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jgdm")!=null && StringUtils.isNotBlank(parmMap.get("jgdm").toString())){
				criteria.andEqualTo("jgdm",parmMap.get("jgdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jgmc")!=null && StringUtils.isNotBlank(parmMap.get("jgmc").toString())){
				criteria.andEqualTo("jgmc",parmMap.get("jgmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("userpk")!=null && StringUtils.isNotBlank(parmMap.get("userpk").toString())){
				criteria.andEqualTo("userpk",parmMap.get("userpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("username")!=null && StringUtils.isNotBlank(parmMap.get("username").toString())){
				criteria.andEqualTo("username",parmMap.get("username").toString().trim());
				flag = true;
			}
    		if(parmMap.get("idno")!=null && StringUtils.isNotBlank(parmMap.get("idno").toString())){
				criteria.andEqualTo("idno",parmMap.get("idno").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jh")!=null && StringUtils.isNotBlank(parmMap.get("jh").toString())){
				criteria.andEqualTo("jh",parmMap.get("jh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("czip")!=null && StringUtils.isNotBlank(parmMap.get("czip").toString())){
				criteria.andEqualTo("czip",parmMap.get("czip").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("SysLogServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("SysLogServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
