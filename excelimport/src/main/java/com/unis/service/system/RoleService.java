package com.unis.service.system;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import com.unis.model.system.Role;
import com.unis.service.BaseService;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;

/**
 * <pre>
 * TODO
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2018-12-27
 */
public interface RoleService extends BaseService {

    /**
     * 新增Role实例
     * 
     * @param role
     * @throws Exception
     */
    int insert(Role role) throws Exception;

    /**
     * 删除Role实例
     * 
     * @param pk
     * @throws Exception
     */
    int delete(String pk) throws Exception;

    /**
     * 更新Role实例
     * 
     * @param role
     * @throws Exception
     */
    int updateByPrimaryKey(Role role) throws Exception;
    /**
     * 根据pk主键获取对象
     * 
     * @param pk
     * @throws Exception
     */
    Role queryRoleByPrimaryKey(String pk) throws Exception;

    /**
     * 查询Role实例
     * 
     * @param role
     * @throws Exception
     */
    Role queryAsObject(Role role) throws Exception;
    
    	
	/**
     * 根据条件获取条数
     * 
     * @param example
     * @throws Exception
     */
	int queryCountByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合
     * 
     * @param example
     * @throws Exception
     */
	List<Role> queryListByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合(分页),需在example 设定排序
     * 
     * @param example
     * @param pageNum
     * @param pageSize
     * @throws Exception
     */
	PageInfo queryPageInfoByExample(Example example, int pageNum, int pageSize) throws Exception;

    /**
     * 根据用户pk 获取权限
     * @param userid
     * @return
     * @throws Exception
     */
    List<Role> queryCurrentUserRole(@Param("userid")String userid) throws Exception;
    /**
     * 根据入参获取数据集合(分页)
     * @param parmMap
     * @param pageNum
     * @param pageSize
     * @return
     * @throws Exception
     */
    ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception;

}
