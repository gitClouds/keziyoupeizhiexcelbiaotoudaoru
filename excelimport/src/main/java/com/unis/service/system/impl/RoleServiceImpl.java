package com.unis.service.system.impl;

import java.util.List;
import java.util.Map;

import com.unis.common.util.TemplateUtil;
import com.unis.dto.ResultDto;
import com.unis.service.impl.BaseServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;

import com.unis.mapper.system.RoleMapper;
import com.unis.model.system.Role;
import com.unis.service.system.RoleService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see RoleService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2018-12-27
 */
@Service("roleService")
public class RoleServiceImpl extends BaseServiceImpl implements RoleService {

	private static final Logger logger = LoggerFactory.getLogger(RoleServiceImpl.class);

    @Autowired
    private RoleMapper roleMapper;

    /**
     * @see RoleService#insert(Role role)
     */
    @Override
    public int insert(Role role) throws Exception {
    	if (role!=null){
	        role.setPk(TemplateUtil.genUUID());
	        return roleMapper.insertSelective(role);
    	}else{
			logger.error("RoleServiceImpl.insert时role数据为空。");
    		throw new AppRuntimeException("RoleServiceImpl.insert时role数据为空。");
    	}        
    }

    /**
     * @see RoleService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
			logger.error("RoleServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("RoleServiceImpl.delete时pk为空。");
    	}else{
    		return roleMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see RoleService#updateByPrimaryKey(Role role)
     */
    @Override
    public int updateByPrimaryKey(Role role) throws Exception {
        if (role!=null){
        	if(StringUtils.isBlank(role.getPk())){
				logger.error("RoleServiceImpl.updateByPrimaryKey时role.Pk为空。");
        		throw new AppRuntimeException("RoleServiceImpl.updateByPrimaryKey时role.Pk为空。");
        	}
	        return roleMapper.updateByPrimaryKeySelective(role);
    	}else{
			logger.error("RoleServiceImpl.updateByPrimaryKey时role数据为空。");
    		throw new AppRuntimeException("RoleServiceImpl.updateByPrimaryKey时role数据为空。");
    	}
    }
    /**
     * @see RoleService#queryRoleByPrimaryKey(String pk)
     */
    @Override
    public Role queryRoleByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
			logger.error("RoleServiceImpl.queryRoleByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("RoleServiceImpl.queryRoleByPrimaryKey时pk为空。");
    	}else{
    		return roleMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see RoleService#queryAsObject(Role role)
     */
    @Override
    public Role queryAsObject(Role role) throws Exception {
        if (role!=null){
	        return roleMapper.selectOne(role);
    	}else{
			logger.error("RoleServiceImpl.queryAsObject时role数据为空。");
    		throw new AppRuntimeException("RoleServiceImpl.queryAsObject时role数据为空。");
    	}
    }
    
    /**
     * @see RoleService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return roleMapper.selectCountByExample(example);
    	}else{
			logger.error("RoleServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("RoleServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see RoleService#queryListByExample(Example example)
     */
    @Override
    public List<Role> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return roleMapper.selectByExample(example);
    	}else{
			logger.error("RoleServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("RoleServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see RoleService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(roleMapper.selectByExample(example));
    	}else{
			logger.error("RoleServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("RoleServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
	 * @see RoleService#queryCurrentUserRole(String userid)
	 */
	@Override
	public List<Role> queryCurrentUserRole(String userid) throws Exception {
		if(StringUtils.isNotBlank(userid)){
			return roleMapper.queryCurrentUserRole(userid);
		}else{
			logger.error("RoleServiceImpl.queryCurrentUserRole时userid数据为空。");
			throw new AppRuntimeException("RoleServiceImpl.queryCurrentUserRole时userid数据为空。");
		}
	}
	/**
	 * @see RoleService#queryListByPage(Map parmMap,int pageNum,int pageSize)
	 */
	@Override
	public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws Exception {

		if (parmMap!=null){
			Example example = new Example(Role.class);
			example.setOrderByClause("code asc");//设置排序
			Example.Criteria criteria = example.createCriteria();
			//criteria设置
			// = --> andEqualTo(field,value)
			// like --> andLike(field,likeValue)；likeValue 包含‘%’
			// is null --> andIsNull(field)
			// is not null --> andIsNotNull(field)
			// <> --> andNotEqualTo(field)
			// > --> andGreaterThan(field,value)
			// >= --> andGreaterThanOrEqualTo(field,value)
			// < --> andLessThan(field,value)
			// <= --> andLessThanOrEqualTo(field,value)
			// in --> andIn(field,Iterable value)
			// not in --> andNotIn(field,Iterable value)
			// between --> andBetween(field,beginValue,endValue)
			// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’

			// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)

			//criteria.andEqualTo("yxx",1);设置yxx=1
			/**
			 *此方法请根据需要修改下方条件
			 */
			//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
			//生成条件中均为 and field = value
			//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
			if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
			}
			if(parmMap.get("code")!=null && StringUtils.isNotBlank(parmMap.get("code").toString())){
				criteria.andLike("code","%"+parmMap.get("code").toString().trim()+"%");
			}
			if(parmMap.get("name")!=null && StringUtils.isNotBlank(parmMap.get("name").toString())){
				criteria.andLike("name","%"+parmMap.get("name").toString().trim()+"%");
			}
			PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			result.setTotal((int)pageInfo.getTotal());
			return result;
		}else{
			logger.error("RoleServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("RoleServiceImpl.queryListByPage时parmMap数据为空。");
		}
	}


}
