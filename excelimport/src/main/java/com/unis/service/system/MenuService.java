package com.unis.service.system;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import com.unis.model.system.Menu;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.unis.service.BaseService;

/**
 * <pre>
 * TODO
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-01-07
 */
public interface MenuService extends BaseService {

    /**
     * 新增Menu实例
     * 
     * @param menu
     * @throws Exception
     */
    int insert(Menu menu) throws Exception;

    /**
     * 删除Menu实例
     * 
     * @param pk
     * @throws Exception
     */
    int delete(String pk) throws Exception;

    /**
     * 更新Menu实例
     * 
     * @param menu
     * @throws Exception
     */
    int updateByPrimaryKey(Menu menu) throws Exception;
    /**
     * 根据pk主键获取对象
     * 
     * @param pk
     * @throws Exception
     */
    Menu queryMenuByPrimaryKey(String pk) throws Exception;

    /**
     * 查询Menu实例
     * 
     * @param menu
     * @throws Exception
     */
    Menu queryAsObject(Menu menu) throws Exception;
    
    	
	/**
     * 根据条件获取条数
     * 
     * @param example
     * @throws Exception
     */
	int queryCountByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合
     * 
     * @param example
     * @throws Exception
     */
	List<Menu> queryListByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合(分页),需在example 设定排序
     * 
     * @param example
     * @param pageNum
     * @param pageSize
     * @throws Exception
     */
	PageInfo queryPageInfoByExample(Example example, int pageNum, int pageSize) throws Exception;

    /**
     * 获取所有菜单
     * @return
     * @throws Exception
     */
	List<Map<String,Object>> queryMenuAll() throws Exception;

    /**
     * 获取对应等级菜单
     * @param userId
     * @param codeLevel
     * @param parentCode
     * @return
     */
    List<Map<String, Object>> queryMenuByParm(String userId,int codeLevel,String parentCode);
    /**
     * 根据入参获取数据集合(分页)
     * @param parmMap
     * @param pageNum
     * @param pageSize
     * @return
     * @throws Exception
     */
    ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception;
}
