package com.unis.service.system.impl;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import com.unis.model.znzf.TbDxzpYhkCompanyZh;
import com.unis.service.znzf.TbDxzpYhkCompanyZhService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.system.BankCodeMapper;
import com.unis.model.system.BankCode;
import com.unis.service.system.BankCodeService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see BankCodeService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-02-14
 */
@Service("bankCodeService")
public class BankCodeServiceImpl extends BaseServiceImpl implements BankCodeService {
	private static final Logger logger = LoggerFactory.getLogger(BankCodeServiceImpl.class);
    @Autowired
    private BankCodeMapper bankCodeMapper;
    @Autowired
	private TbDxzpYhkCompanyZhService tbDxzpYhkCompanyZhService;
    /**
     * @see BankCodeService#insert(BankCode bankCode)
     */
    @Override
    public int insert(BankCode bankCode) throws Exception {
    	if (bankCode!=null){
	        //bankCode.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return bankCodeMapper.insertSelective(bankCode);
    	}else{
    		logger.error("BankCodeServiceImpl.insert时bankCode数据为空。");
    		throw new AppRuntimeException("BankCodeServiceImpl.insert时bankCode数据为空。");
    	}        
    }

    /**
     * @see BankCodeService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("BankCodeServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("BankCodeServiceImpl.delete时pk为空。");
    	}else{
    		return bankCodeMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see BankCodeService#updateByPrimaryKey(BankCode bankCode)
     */
    @Override
    public int updateByPrimaryKey(BankCode bankCode) throws Exception {
        if (bankCode!=null){
        	if(StringUtils.isBlank(bankCode.getPk())){
        		logger.error("BankCodeServiceImpl.updateByPrimaryKey时bankCode.Pk为空。");
        		throw new AppRuntimeException("BankCodeServiceImpl.updateByPrimaryKey时bankCode.Pk为空。");
        	}
	        return bankCodeMapper.updateByPrimaryKeySelective(bankCode);
    	}else{
    		logger.error("BankCodeServiceImpl.updateByPrimaryKey时bankCode数据为空。");
    		throw new AppRuntimeException("BankCodeServiceImpl.updateByPrimaryKey时bankCode数据为空。");
    	}
    }
    /**
     * @see BankCodeService#queryBankCodeByPrimaryKey(String pk)
     */
    @Override
    public BankCode queryBankCodeByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("BankCodeServiceImpl.queryBankCodeByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("BankCodeServiceImpl.queryBankCodeByPrimaryKey时pk为空。");
    	}else{
    		return bankCodeMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see BankCodeService#queryAsObject(BankCode bankCode)
     */
    @Override
    public BankCode queryAsObject(BankCode bankCode) throws Exception {
        if (bankCode!=null){
	        return bankCodeMapper.selectOne(bankCode);
    	}else{
    		logger.error("BankCodeServiceImpl.queryAsObject时bankCode数据为空。");
    		throw new AppRuntimeException("BankCodeServiceImpl.queryAsObject时bankCode数据为空。");
    	}
    }
    
    /**
     * @see BankCodeService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return bankCodeMapper.selectCountByExample(example);
    	}else{
    		logger.error("BankCodeServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("BankCodeServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see BankCodeService#queryListByExample(Example example)
     */
    @Override
    public List<BankCode> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return bankCodeMapper.selectByExample(example);
    	}else{
    		logger.error("BankCodeServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("BankCodeServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see BankCodeService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(bankCodeMapper.selectByExample(example));
    	}else{
    		logger.error("BankCodeServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("BankCodeServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see BankCodeService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(BankCode.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("startaccount")!=null && StringUtils.isNotBlank(parmMap.get("startaccount").toString())){
				criteria.andEqualTo("startaccount",parmMap.get("startaccount").toString().trim());
				flag = true;
			}
    		if(parmMap.get("accountlength")!=null && StringUtils.isNotBlank(parmMap.get("accountlength").toString())){
				criteria.andEqualTo("accountlength",parmMap.get("accountlength").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bankcode")!=null && StringUtils.isNotBlank(parmMap.get("bankcode").toString())){
				criteria.andEqualTo("bankcode",parmMap.get("bankcode").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bankname")!=null && StringUtils.isNotBlank(parmMap.get("bankname").toString())){
				criteria.andEqualTo("bankname",parmMap.get("bankname").toString().trim());
				flag = true;
			}
    		if(parmMap.get("isok")!=null && StringUtils.isNotBlank(parmMap.get("isok").toString())){
				criteria.andEqualTo("isok",parmMap.get("isok").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("BankCodeServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("BankCodeServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }

	@Override
	public BankCode queryBankByAccount(String account) throws Exception {
		if (StringUtils.isNotBlank(account)){
			BankCode bankCode = null;

			Example example = new Example(TbDxzpYhkCompanyZh.class);
			example.setOrderByClause("id DESC");
			Example.Criteria criteria = example.createCriteria();
			criteria.andEqualTo("zh",account);
			List<TbDxzpYhkCompanyZh> lists = tbDxzpYhkCompanyZhService.queryListByExample(example);
			if (lists!=null && !lists.isEmpty() && lists.size()>0){
				TbDxzpYhkCompanyZh zh = lists.stream().filter(s-> StringUtils.isNotBlank(s.getZhjgdm())).findFirst().orElse(null);
				if (zh!=null){
					bankCode = new BankCode();
					bankCode.setBankcode(zh.getZhjgdm());
					bankCode.setBankname(zh.getZhjgmc());
					bankCode.setSubjecttype((short)2);
				}
			}

			if (bankCode==null){
				List<BankCode> list = bankCodeMapper.queryBankListByAccount(account);
				if (list!=null && !list.isEmpty() && list.size()>0){
					bankCode = list.parallelStream().filter(bean -> bean.getAccountlength()==StringUtils.length(StringUtils.trim(account))).findFirst().orElse(null);
					if (bankCode!=null){
						return bankCode;
					}else{
						return list.get(0);
					}
				}
			}
			return bankCode;
		}else{
			logger.error("BankCodeServiceImpl.queryBankByAccount时account为空。");
			throw new AppRuntimeException("BankCodeServiceImpl.queryBankByAccount时account为空。");
		}
	}
}
