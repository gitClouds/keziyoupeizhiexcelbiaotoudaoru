package com.unis.service.system;


import com.unis.model.system.Code;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <pre>
 * TODO
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2018-12-29
 */
public interface CodeService  {

    Map<String, Map<String, String>> queryDicWithSpecCode(String... codes) throws Exception;

    List<Code> queryDictMap(@Param("zddm") String zddm);

    List<Map<String,Object>> selectTreeListByZddm(String zddm,String sjdm,int codeLevel);
    List<Map<String,Object>> queryListByZddm(String zddm,String dm,String filterSql);

    List<Object> translateList(List<Object> list,Class<?> clazz,String... fieldAndCodes);

    Object translateObject(Object obj,Class<?> clazz,String... fieldAndCodes);

    Map translateMap(Map map,String... fieldAndCodes);
    List<Map> translateListMap(List<Map> list, String... fieldAndCodes);

    List<Code> queryCodeList(Code code);
}
