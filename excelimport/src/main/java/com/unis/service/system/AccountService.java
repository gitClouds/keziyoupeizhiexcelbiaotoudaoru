package com.unis.service.system;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import com.unis.model.system.Account;
import com.unis.service.BaseService;
import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;

/**
 * <pre>
 * TODO
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2018-12-27
 */
public interface AccountService extends BaseService{

    /**
     * 新增Account实例
     * 
     * @param account
     * @throws Exception
     */
    int insert(Account account) throws Exception;

    /**
     * 删除Account实例
     * 
     * @param pk
     * @throws Exception
     */
    int delete(String pk) throws Exception;

    /**
     * 更新Account实例
     * 
     * @param account
     * @throws Exception
     */
    int updateByPrimaryKey(Account account) throws Exception;

    /**
     * 修改密码
     * @param old_password 旧密码
     * @param password 新密码
     * @param conf_password 确认密码
     * @return
     * @throws Exception
     */
    String changePwd(String old_password,String password,String conf_password) throws Exception;
    /**
     * 重置密码
     * @return
     * @throws Exception
     */
    int resetPwdByInfo() throws Exception;
    /**
     * 根据pk主键获取对象
     * 
     * @param pk
     * @throws Exception
     */
    Account queryAccountByPrimaryKey(String pk) throws Exception;

    /**
     * 查询Account实例
     * 
     * @param account
     * @throws Exception
     */
    Account queryAsObject(Account account) throws Exception;
    
    	
	/**
     * 根据条件获取条数
     * 
     * @param example
     * @throws Exception
     */
	int queryCountByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合
     * 
     * @param example
     * @throws Exception
     */
	List<Account> queryListByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合(分页),需在example 设定排序
     * 
     * @param example
     * @param pageNum
     * @param pageSize
     * @throws Exception
     */
	PageInfo queryPageInfoByExample(Example example, int pageNum, int pageSize) throws Exception;

    /**
     * 根据入参获取数据集合(分页)
     * @param parmMap
     * @param pageNum
     * @param pageSize
     * @return
     * @throws Exception
     */
    ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception;

    /**
     *
     * @param pk
     * @param username
     * @param idno
     * @param jh
     * @return
     * @throws Exception
     */
    Account queryAccountByParm(@Param("pk")String pk, @Param("username")String username, @Param("idno")String idno, @Param("jh")String jh) throws Exception;
    /**
     * 根据用户PK获取全部角色数据集合（已有角色标记为"TRUE"）
     * @param userPk
     * @return
     * @throws Exception
     */
    List<Map<String,String>> queryRoleListByPage(String userPk) throws  Exception;

    /**
     * 修改用户角色
     * @param userPk
     * @param checkedRoles
     * @return
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int updateGrantRole(String userPk,String checkedRoles) throws Exception;
}
