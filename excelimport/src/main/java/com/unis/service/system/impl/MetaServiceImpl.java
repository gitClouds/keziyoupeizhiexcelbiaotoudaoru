package com.unis.service.system.impl;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.system.MetaMapper;
import com.unis.model.system.Meta;
import com.unis.service.system.MetaService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see MetaService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-03-13
 */
@Service("metaService")
public class MetaServiceImpl extends BaseServiceImpl implements MetaService {
	private static final Logger logger = LoggerFactory.getLogger(MetaServiceImpl.class);
    @Autowired
    private MetaMapper metaMapper;

    /**
     * @see MetaService#insert(Meta meta)
     */
    @Override
    public int insert(Meta meta) throws Exception {
    	if (meta!=null){
	        //meta.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return metaMapper.insertSelective(meta);
    	}else{
    		logger.error("MetaServiceImpl.insert时meta数据为空。");
    		throw new AppRuntimeException("MetaServiceImpl.insert时meta数据为空。");
    	}        
    }

    /**
     * @see MetaService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("MetaServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("MetaServiceImpl.delete时pk为空。");
    	}else{
    		return metaMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see MetaService#updateByPrimaryKey(Meta meta)
     */
    @Override
    public int updateByPrimaryKey(Meta meta) throws Exception {
        if (meta!=null){
        	if(StringUtils.isBlank(meta.getPk())){
        		logger.error("MetaServiceImpl.updateByPrimaryKey时meta.Pk为空。");
        		throw new AppRuntimeException("MetaServiceImpl.updateByPrimaryKey时meta.Pk为空。");
        	}
	        return metaMapper.updateByPrimaryKeySelective(meta);
    	}else{
    		logger.error("MetaServiceImpl.updateByPrimaryKey时meta数据为空。");
    		throw new AppRuntimeException("MetaServiceImpl.updateByPrimaryKey时meta数据为空。");
    	}
    }
    /**
     * @see MetaService#queryMetaByPrimaryKey(String pk)
     */
    @Override
    public Meta queryMetaByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("MetaServiceImpl.queryMetaByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("MetaServiceImpl.queryMetaByPrimaryKey时pk为空。");
    	}else{
    		return metaMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see MetaService#queryAsObject(Meta meta)
     */
    @Override
    public Meta queryAsObject(Meta meta) throws Exception {
        if (meta!=null){
	        return metaMapper.selectOne(meta);
    	}else{
    		logger.error("MetaServiceImpl.queryAsObject时meta数据为空。");
    		throw new AppRuntimeException("MetaServiceImpl.queryAsObject时meta数据为空。");
    	}
    }
    
    /**
     * @see MetaService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return metaMapper.selectCountByExample(example);
    	}else{
    		logger.error("MetaServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("MetaServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see MetaService#queryListByExample(Example example)
     */
    @Override
    public List<Meta> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return metaMapper.selectByExample(example);
    	}else{
    		logger.error("MetaServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("MetaServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see MetaService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(metaMapper.selectByExample(example));
    	}else{
    		logger.error("MetaServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("MetaServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see MetaService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(Meta.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("nlevel")!=null && StringUtils.isNotBlank(parmMap.get("nlevel").toString())){
				criteria.andEqualTo("nlevel",parmMap.get("nlevel").toString().trim());
				flag = true;
			}
    		if(parmMap.get("minje")!=null && StringUtils.isNotBlank(parmMap.get("minje").toString())){
				criteria.andEqualTo("minje",parmMap.get("minje").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ts")!=null && StringUtils.isNotBlank(parmMap.get("ts").toString())){
				criteria.andEqualTo("ts",parmMap.get("ts").toString().trim());
				flag = true;
			}
    		if(parmMap.get("maxhour")!=null && StringUtils.isNotBlank(parmMap.get("maxhour").toString())){
				criteria.andEqualTo("maxhour",parmMap.get("maxhour").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jdgx")!=null && StringUtils.isNotBlank(parmMap.get("jdgx").toString())){
				criteria.andEqualTo("jdgx",parmMap.get("jdgx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xgsj")!=null && StringUtils.isNotBlank(parmMap.get("xgsj").toString())){
				criteria.andEqualTo("xgsj",parmMap.get("xgsj").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("MetaServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("MetaServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
