package com.unis.service.es;

import com.alibaba.fastjson.JSONObject;
import org.elasticsearch.action.search.SearchResponse;

public interface EsService {

    SearchResponse getEsData(String tab, String gjz, int page);
}
