package com.unis.service.analysis.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.unis.dto.ResultDto;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.analysis.SfMxJgListMapper;
import com.unis.model.analysis.SfMxJgList;
import com.unis.service.analysis.SfMxJgListService;
import com.unis.common.exception.app.AppRuntimeException;
import com.unis.common.util.TimeUtil;

/**
 * <pre>
 * @see SfMxJgListService
 * </pre>
 *
 * @author zcf
 * @version 1.0
 * @since 2020-03-26
 */
@Service("sfMxJgListService")
public class SfMxJgListServiceImpl extends BaseServiceImpl implements SfMxJgListService {
	private static final Logger logger = LoggerFactory.getLogger(SfMxJgListServiceImpl.class);
    @Autowired
    private SfMxJgListMapper sfMxJgListMapper;

    /**
     * @see SfMxJgListService#insert(SfMxJgList sfMxJgList)
     */
    @Override
    public int insert(SfMxJgList sfMxJgList) throws Exception {
    	if (sfMxJgList!=null){
	        //sfMxJgList.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return sfMxJgListMapper.insertSelective(sfMxJgList);
    	}else{
    		logger.error("SfMxJgListServiceImpl.insert时sfMxJgList数据为空。");
    		throw new AppRuntimeException("SfMxJgListServiceImpl.insert时sfMxJgList数据为空。");
    	}        
    }

    /**
     * @see SfMxJgListService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("SfMxJgListServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("SfMxJgListServiceImpl.delete时pk为空。");
    	}else{
    		return sfMxJgListMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see SfMxJgListService#updateByPrimaryKey(SfMxJgList sfMxJgList)
     */
    @Override
    public int updateByPrimaryKey(SfMxJgList sfMxJgList) throws Exception {
        if (sfMxJgList!=null){
        	if(StringUtils.isBlank(sfMxJgList.getPk())){
        		logger.error("SfMxJgListServiceImpl.updateByPrimaryKey时sfMxJgList.Pk为空。");
        		throw new AppRuntimeException("SfMxJgListServiceImpl.updateByPrimaryKey时sfMxJgList.Pk为空。");
        	}
	        return sfMxJgListMapper.updateByPrimaryKeySelective(sfMxJgList);
    	}else{
    		logger.error("SfMxJgListServiceImpl.updateByPrimaryKey时sfMxJgList数据为空。");
    		throw new AppRuntimeException("SfMxJgListServiceImpl.updateByPrimaryKey时sfMxJgList数据为空。");
    	}
    }
    /**
     * @see SfMxJgListService#querySfMxJgListByPrimaryKey(String pk)
     */
    @Override
    public SfMxJgList querySfMxJgListByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("SfMxJgListServiceImpl.querySfMxJgListByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("SfMxJgListServiceImpl.querySfMxJgListByPrimaryKey时pk为空。");
    	}else{
    		return sfMxJgListMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see SfMxJgListService#queryAsObject(SfMxJgList sfMxJgList)
     */
    @Override
    public SfMxJgList queryAsObject(SfMxJgList sfMxJgList) throws Exception {
        if (sfMxJgList!=null){
	        return sfMxJgListMapper.selectOne(sfMxJgList);
    	}else{
    		logger.error("SfMxJgListServiceImpl.queryAsObject时sfMxJgList数据为空。");
    		throw new AppRuntimeException("SfMxJgListServiceImpl.queryAsObject时sfMxJgList数据为空。");
    	}
    }
    
    /**
     * @see SfMxJgListService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return sfMxJgListMapper.selectCountByExample(example);
    	}else{
    		logger.error("SfMxJgListServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("SfMxJgListServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see SfMxJgListService#queryListByExample(Example example)
     */
    @Override
    public List<SfMxJgList> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return sfMxJgListMapper.selectByExample(example);
    	}else{
    		logger.error("SfMxJgListServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("SfMxJgListServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see SfMxJgListService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(sfMxJgListMapper.selectByExample(example));
    	}else{
    		logger.error("SfMxJgListServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("SfMxJgListServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see SfMxJgListService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(SfMxJgList.class);
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jjdpk")!=null && StringUtils.isNotBlank(parmMap.get("jjdpk").toString())){
				criteria.andEqualTo("jjdpk",parmMap.get("jjdpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("applicationid")!=null && StringUtils.isNotBlank(parmMap.get("applicationid").toString())){
				criteria.andEqualTo("applicationid",parmMap.get("applicationid").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zfddh")!=null && StringUtils.isNotBlank(parmMap.get("zfddh").toString())){
				criteria.andEqualTo("zfddh",parmMap.get("zfddh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jylx")!=null && StringUtils.isNotBlank(parmMap.get("jylx").toString())){
				criteria.andEqualTo("jylx",parmMap.get("jylx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zflx")!=null && StringUtils.isNotBlank(parmMap.get("zflx").toString())){
				criteria.andEqualTo("zflx",parmMap.get("zflx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jdbz")!=null && StringUtils.isNotBlank(parmMap.get("jdbz").toString())){
				criteria.andEqualTo("jdbz",parmMap.get("jdbz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jysj")!=null && StringUtils.isNotBlank(parmMap.get("jysj").toString())){
				criteria.andEqualTo("jysj",parmMap.get("jysj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rmbzl")!=null && StringUtils.isNotBlank(parmMap.get("rmbzl").toString())){
				criteria.andEqualTo("rmbzl",parmMap.get("rmbzl").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jyje")!=null && StringUtils.isNotBlank(parmMap.get("jyje").toString())){
				criteria.andEqualTo("jyje",parmMap.get("jyje").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jylsh")!=null && StringUtils.isNotBlank(parmMap.get("jylsh").toString())){
				criteria.andEqualTo("jylsh",parmMap.get("jylsh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ye")!=null && StringUtils.isNotBlank(parmMap.get("ye").toString())){
				criteria.andEqualTo("ye",parmMap.get("ye").toString().trim());
				flag = true;
			}
    		if(parmMap.get("skyhjgdm")!=null && StringUtils.isNotBlank(parmMap.get("skyhjgdm").toString())){
				criteria.andEqualTo("skyhjgdm",parmMap.get("skyhjgdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("skyhjgmc")!=null && StringUtils.isNotBlank(parmMap.get("skyhjgmc").toString())){
				criteria.andEqualTo("skyhjgmc",parmMap.get("skyhjgmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("skyhzh")!=null && StringUtils.isNotBlank(parmMap.get("skyhzh").toString())){
				criteria.andEqualTo("skyhzh",parmMap.get("skyhzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("skzfzh")!=null && StringUtils.isNotBlank(parmMap.get("skzfzh").toString())){
				criteria.andEqualTo("skzfzh",parmMap.get("skzfzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("possbh")!=null && StringUtils.isNotBlank(parmMap.get("possbh").toString())){
				criteria.andEqualTo("possbh",parmMap.get("possbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("skshm")!=null && StringUtils.isNotBlank(parmMap.get("skshm").toString())){
				criteria.andEqualTo("skshm",parmMap.get("skshm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fkyhjgdm")!=null && StringUtils.isNotBlank(parmMap.get("fkyhjgdm").toString())){
				criteria.andEqualTo("fkyhjgdm",parmMap.get("fkyhjgdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fkyhjgmc")!=null && StringUtils.isNotBlank(parmMap.get("fkyhjgmc").toString())){
				criteria.andEqualTo("fkyhjgmc",parmMap.get("fkyhjgmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fkyhzh")!=null && StringUtils.isNotBlank(parmMap.get("fkyhzh").toString())){
				criteria.andEqualTo("fkyhzh",parmMap.get("fkyhzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fkzfzh")!=null && StringUtils.isNotBlank(parmMap.get("fkzfzh").toString())){
				criteria.andEqualTo("fkzfzh",parmMap.get("fkzfzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jysblx")!=null && StringUtils.isNotBlank(parmMap.get("jysblx").toString())){
				criteria.andEqualTo("jysblx",parmMap.get("jysblx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zfsbip")!=null && StringUtils.isNotBlank(parmMap.get("zfsbip").toString())){
				criteria.andEqualTo("zfsbip",parmMap.get("zfsbip").toString().trim());
				flag = true;
			}
    		if(parmMap.get("macdz")!=null && StringUtils.isNotBlank(parmMap.get("macdz").toString())){
				criteria.andEqualTo("macdz",parmMap.get("macdz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jyddjd")!=null && StringUtils.isNotBlank(parmMap.get("jyddjd").toString())){
				criteria.andEqualTo("jyddjd",parmMap.get("jyddjd").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jyddwd")!=null && StringUtils.isNotBlank(parmMap.get("jyddwd").toString())){
				criteria.andEqualTo("jyddwd",parmMap.get("jyddwd").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jysbh")!=null && StringUtils.isNotBlank(parmMap.get("jysbh").toString())){
				criteria.andEqualTo("jysbh",parmMap.get("jysbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yhwblsh")!=null && StringUtils.isNotBlank(parmMap.get("yhwblsh").toString())){
				criteria.andEqualTo("yhwblsh",parmMap.get("yhwblsh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bz")!=null && StringUtils.isNotBlank(parmMap.get("bz").toString())){
				criteria.andEqualTo("bz",parmMap.get("bz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ipxz")!=null && StringUtils.isNotBlank(parmMap.get("ipxz").toString())){
				criteria.andEqualTo("ipxz",parmMap.get("ipxz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("skfshmc")!=null && StringUtils.isNotBlank(parmMap.get("skfshmc").toString())){
				criteria.andEqualTo("skfshmc",parmMap.get("skfshmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qqpk")!=null && StringUtils.isNotBlank(parmMap.get("qqpk").toString())){
				criteria.andEqualTo("qqpk",parmMap.get("qqpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jyjebs")!=null && StringUtils.isNotBlank(parmMap.get("jyjebs").toString())){
    			criteria.andCondition("MOD(jyje,"+parmMap.get("jyjebs").toString().trim()+") = 0");
    			flag = true;
    		}
    		if(parmMap.get("jysjBegin")!=null && StringUtils.isNotBlank(parmMap.get("jysjBegin").toString())){
    			criteria.andGreaterThanOrEqualTo("jysj", parmMap.get("jysjBegin").toString().trim());
    			flag = true;
    		}
    		if(parmMap.get("jysjEnd")!=null && StringUtils.isNotBlank(parmMap.get("jysjEnd").toString())){
    			criteria.andLessThanOrEqualTo("jysj", parmMap.get("jysjEnd").toString().trim());
    			flag = true;
    		}
    		if(parmMap.get("jysjb")!=null && StringUtils.isNotBlank(parmMap.get("jysjb").toString())&&
    				parmMap.get("jysje")!=null && StringUtils.isNotBlank(parmMap.get("jysje").toString())){
    			criteria.andCondition("to_number(substr(jysj,9,2)) >='"+parmMap.get("jysjb").toString()+"' and to_number(substr(jysj,9,2))<="+parmMap.get("jysje").toString());
    			flag = true;
    		}
    		if(parmMap.get("isxdsd")!=null && StringUtils.isNotBlank(parmMap.get("isxdsd").toString())){
    			if(parmMap.get("isxdsd").toString().equals("1")) {
    				criteria.andCondition("(length(xdrypks) > 1 or length(sdrypks) > 1)");
    			}else if(parmMap.get("isxdsd").toString().equals("0")) {
    				criteria.andEqualTo("xdrypks","1");
    				criteria.andEqualTo("sdrypks","1");
    			}else if(parmMap.get("isxdsd").toString().equals("2")) {
    				criteria.andIsNull("xdrypks");
    				criteria.andIsNull("sdrypks");
    			}
    			flag = true;
    		}
    		criteria.andEqualTo("jdbz","0");
    		criteria.andIn("jylx", Arrays.asList("2","3"));
    		if(parmMap.get("analysis_pk")!=null && StringUtils.isNotBlank(parmMap.get("analysis_pk").toString())){
    			criteria.andCondition("pk in (select mxpk from TB_ZNZF_SCREEN_DATA where type ='2' and analysis_pk = '"+parmMap.get("analysis_pk").toString()+"')");
    			flag = true;
    		}
    		if(parmMap.get("cxzh")!=null && StringUtils.isNotBlank(parmMap.get("cxzh").toString())){
    			criteria.andCondition("applicationId in (select applicationId from tb_znzf_sf_mx_qq where   ACCNUMBER = '"+parmMap.get("cxzh").toString()+"')");
    			flag = true;
    		}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("SfMxJgListServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("SfMxJgListServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }

	@Override
	public void updateDisabByPrimaryKey(String pk) throws Exception {
		sfMxJgListMapper.updateDisabled(pk);
		
	}
}
