package com.unis.service.analysis.impl;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.unis.dto.ResultDto;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.analysis.ScreenDataMapper;
import com.unis.model.analysis.ScreenData;
import com.unis.service.analysis.ScreenDataService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see ScreenDataService
 * </pre>
 *
 * @author zcf
 * @version 1.0
 * @since 2020-03-27
 */
@Service("screenDataService")
public class ScreenDataServiceImpl extends BaseServiceImpl implements ScreenDataService {
	private static final Logger logger = LoggerFactory.getLogger(ScreenDataServiceImpl.class);
    @Autowired
    private ScreenDataMapper screenDataMapper;

    /**
     * @see ScreenDataService#insert(ScreenData screenData)
     */
    @Override
    public int insert(ScreenData screenData) throws Exception {
    	if (screenData!=null){
	        //screenData.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return screenDataMapper.insertSelective(screenData);
    	}else{
    		logger.error("ScreenDataServiceImpl.insert时screenData数据为空。");
    		throw new AppRuntimeException("ScreenDataServiceImpl.insert时screenData数据为空。");
    	}        
    }

    /**
     * @see ScreenDataService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("ScreenDataServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("ScreenDataServiceImpl.delete时pk为空。");
    	}else{
    		return screenDataMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see ScreenDataService#updateByPrimaryKey(ScreenData screenData)
     */
    @Override
    public int updateByPrimaryKey(ScreenData screenData) throws Exception {
        if (screenData!=null){
        	if(StringUtils.isBlank(screenData.getPk())){
        		logger.error("ScreenDataServiceImpl.updateByPrimaryKey时screenData.Pk为空。");
        		throw new AppRuntimeException("ScreenDataServiceImpl.updateByPrimaryKey时screenData.Pk为空。");
        	}
	        return screenDataMapper.updateByPrimaryKeySelective(screenData);
    	}else{
    		logger.error("ScreenDataServiceImpl.updateByPrimaryKey时screenData数据为空。");
    		throw new AppRuntimeException("ScreenDataServiceImpl.updateByPrimaryKey时screenData数据为空。");
    	}
    }
    /**
     * @see ScreenDataService#queryScreenDataByPrimaryKey(String pk)
     */
    @Override
    public ScreenData queryScreenDataByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("ScreenDataServiceImpl.queryScreenDataByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("ScreenDataServiceImpl.queryScreenDataByPrimaryKey时pk为空。");
    	}else{
    		return screenDataMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see ScreenDataService#queryAsObject(ScreenData screenData)
     */
    @Override
    public ScreenData queryAsObject(ScreenData screenData) throws Exception {
        if (screenData!=null){
	        return screenDataMapper.selectOne(screenData);
    	}else{
    		logger.error("ScreenDataServiceImpl.queryAsObject时screenData数据为空。");
    		throw new AppRuntimeException("ScreenDataServiceImpl.queryAsObject时screenData数据为空。");
    	}
    }
    
    /**
     * @see ScreenDataService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return screenDataMapper.selectCountByExample(example);
    	}else{
    		logger.error("ScreenDataServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("ScreenDataServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see ScreenDataService#queryListByExample(Example example)
     */
    @Override
    public List<ScreenData> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return screenDataMapper.selectByExample(example);
    	}else{
    		logger.error("ScreenDataServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("ScreenDataServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see ScreenDataService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(screenDataMapper.selectByExample(example));
    	}else{
    		logger.error("ScreenDataServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("ScreenDataServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see ScreenDataService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(ScreenData.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("type")!=null && StringUtils.isNotBlank(parmMap.get("type").toString())){
				criteria.andEqualTo("type",parmMap.get("type").toString().trim());
				flag = true;
			}
    		if(parmMap.get("mxpk")!=null && StringUtils.isNotBlank(parmMap.get("mxpk").toString())){
				criteria.andEqualTo("mxpk",parmMap.get("mxpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("analysis_pk")!=null && StringUtils.isNotBlank(parmMap.get("analysis_pk").toString())){
				criteria.andEqualTo("analysis_pk",parmMap.get("analysis_pk").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("ScreenDataServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("ScreenDataServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }

    @Override
	public List<Map> querySfRyAccountNumber(Map map) throws Exception{
    	return screenDataMapper.querySfRyAccountNumber(map);
	}

	@Override
	public List<Map> queryYhkRyAccountNumber(Map map) throws Exception{
		return screenDataMapper.queryYhkRyAccountNumber(map);
	}
}
