package com.unis.service.analysis.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.unis.dto.ResultDto;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.analysis.YhkMxJgListMapper;
import com.unis.model.analysis.YhkMxJgList;
import com.unis.service.analysis.YhkMxJgListService;
import com.unis.common.exception.app.AppRuntimeException;
import com.unis.common.util.TimeUtil;

/**
 * <pre>
 * @see YhkMxJgListService
 * </pre>
 *
 * @author zcf
 * @version 1.0
 * @since 2020-03-26
 */
@Service("yhkMxJgListService")
public class YhkMxJgListServiceImpl extends BaseServiceImpl implements YhkMxJgListService {
	private static final Logger logger = LoggerFactory.getLogger(YhkMxJgListServiceImpl.class);
    @Autowired
    private YhkMxJgListMapper yhkMxJgListMapper;

    /**
     * @see YhkMxJgListService#insert(YhkMxJgList yhkMxJgList)
     */
    @Override
    public int insert(YhkMxJgList yhkMxJgList) throws Exception {
    	if (yhkMxJgList!=null){
	        //yhkMxJgList.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return yhkMxJgListMapper.insertSelective(yhkMxJgList);
    	}else{
    		logger.error("YhkMxJgListServiceImpl.insert时yhkMxJgList数据为空。");
    		throw new AppRuntimeException("YhkMxJgListServiceImpl.insert时yhkMxJgList数据为空。");
    	}        
    }

    /**
     * @see YhkMxJgListService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("YhkMxJgListServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("YhkMxJgListServiceImpl.delete时pk为空。");
    	}else{
    		return yhkMxJgListMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see YhkMxJgListService#updateByPrimaryKey(YhkMxJgList yhkMxJgList)
     */
    @Override
    public int updateByPrimaryKey(YhkMxJgList yhkMxJgList) throws Exception {
        if (yhkMxJgList!=null){
        	if(StringUtils.isBlank(yhkMxJgList.getPk())){
        		logger.error("YhkMxJgListServiceImpl.updateByPrimaryKey时yhkMxJgList.Pk为空。");
        		throw new AppRuntimeException("YhkMxJgListServiceImpl.updateByPrimaryKey时yhkMxJgList.Pk为空。");
        	}
	        return yhkMxJgListMapper.updateByPrimaryKeySelective(yhkMxJgList);
    	}else{
    		logger.error("YhkMxJgListServiceImpl.updateByPrimaryKey时yhkMxJgList数据为空。");
    		throw new AppRuntimeException("YhkMxJgListServiceImpl.updateByPrimaryKey时yhkMxJgList数据为空。");
    	}
    }
    /**
     * @see YhkMxJgListService#queryYhkMxJgListByPrimaryKey(String pk)
     */
    @Override
    public YhkMxJgList queryYhkMxJgListByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("YhkMxJgListServiceImpl.queryYhkMxJgListByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("YhkMxJgListServiceImpl.queryYhkMxJgListByPrimaryKey时pk为空。");
    	}else{
    		return yhkMxJgListMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see YhkMxJgListService#queryAsObject(YhkMxJgList yhkMxJgList)
     */
    @Override
    public YhkMxJgList queryAsObject(YhkMxJgList yhkMxJgList) throws Exception {
        if (yhkMxJgList!=null){
	        return yhkMxJgListMapper.selectOne(yhkMxJgList);
    	}else{
    		logger.error("YhkMxJgListServiceImpl.queryAsObject时yhkMxJgList数据为空。");
    		throw new AppRuntimeException("YhkMxJgListServiceImpl.queryAsObject时yhkMxJgList数据为空。");
    	}
    }
    
    /**
     * @see YhkMxJgListService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return yhkMxJgListMapper.selectCountByExample(example);
    	}else{
    		logger.error("YhkMxJgListServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("YhkMxJgListServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see YhkMxJgListService#queryListByExample(Example example)
     */
    @Override
    public List<YhkMxJgList> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return yhkMxJgListMapper.selectByExample(example);
    	}else{
    		logger.error("YhkMxJgListServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("YhkMxJgListServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see YhkMxJgListService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize)  throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(yhkMxJgListMapper.selectByExample(example));
    	}else{
    		logger.error("YhkMxJgListServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("YhkMxJgListServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see YhkMxJgListService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws Exception {
    	if (parmMap!=null){
    		Example example = new Example(YhkMxJgList.class);
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jjdpk")!=null && StringUtils.isNotBlank(parmMap.get("jjdpk").toString())){
				criteria.andEqualTo("jjdpk",parmMap.get("jjdpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("applicationid")!=null && StringUtils.isNotBlank(parmMap.get("applicationid").toString())){
				criteria.andEqualTo("applicationid",parmMap.get("applicationid").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jylx")!=null && StringUtils.isNotBlank(parmMap.get("jylx").toString())){
				criteria.andEqualTo("jylx",parmMap.get("jylx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jdbz")!=null && StringUtils.isNotBlank(parmMap.get("jdbz").toString())){
				criteria.andEqualTo("jdbz",parmMap.get("jdbz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jyje")!=null && StringUtils.isNotBlank(parmMap.get("jyje").toString())){
				criteria.andEqualTo("jyje",parmMap.get("jyje").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jyye")!=null && StringUtils.isNotBlank(parmMap.get("jyye").toString())){
				criteria.andEqualTo("jyye",parmMap.get("jyye").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jysj")!=null && StringUtils.isNotBlank(parmMap.get("jysj").toString())){
				criteria.andEqualTo("jysj",parmMap.get("jysj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jylsh")!=null && StringUtils.isNotBlank(parmMap.get("jylsh").toString())){
				criteria.andEqualTo("jylsh",parmMap.get("jylsh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jydfmc")!=null && StringUtils.isNotBlank(parmMap.get("jydfmc").toString())){
				criteria.andEqualTo("jydfmc",parmMap.get("jydfmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jydfzh")!=null && StringUtils.isNotBlank(parmMap.get("jydfzh").toString())){
				criteria.andEqualTo("jydfzh",parmMap.get("jydfzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jydfzjhm")!=null && StringUtils.isNotBlank(parmMap.get("jydfzjhm").toString())){
				criteria.andEqualTo("jydfzjhm",parmMap.get("jydfzjhm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jydfkhh")!=null && StringUtils.isNotBlank(parmMap.get("jydfkhh").toString())){
				criteria.andEqualTo("jydfkhh",parmMap.get("jydfkhh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jysfcg")!=null && StringUtils.isNotBlank(parmMap.get("jysfcg").toString())){
				criteria.andEqualTo("jysfcg",parmMap.get("jysfcg").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xjbz")!=null && StringUtils.isNotBlank(parmMap.get("xjbz").toString())){
				criteria.andEqualTo("xjbz",parmMap.get("xjbz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jywdmc")!=null && StringUtils.isNotBlank(parmMap.get("jywdmc").toString())){
				criteria.andEqualTo("jywdmc",parmMap.get("jywdmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jywddm")!=null && StringUtils.isNotBlank(parmMap.get("jywddm").toString())){
				criteria.andEqualTo("jywddm",parmMap.get("jywddm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jyzy")!=null && StringUtils.isNotBlank(parmMap.get("jyzy").toString())){
				criteria.andEqualTo("jyzy",parmMap.get("jyzy").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ipdz")!=null && StringUtils.isNotBlank(parmMap.get("ipdz").toString())){
				criteria.andEqualTo("ipdz",parmMap.get("ipdz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("macdz")!=null && StringUtils.isNotBlank(parmMap.get("macdz").toString())){
				criteria.andEqualTo("macdz",parmMap.get("macdz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rzh")!=null && StringUtils.isNotBlank(parmMap.get("rzh").toString())){
				criteria.andEqualTo("rzh",parmMap.get("rzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cph")!=null && StringUtils.isNotBlank(parmMap.get("cph").toString())){
				criteria.andEqualTo("cph",parmMap.get("cph").toString().trim());
				flag = true;
			}
    		if(parmMap.get("pzh")!=null && StringUtils.isNotBlank(parmMap.get("pzh").toString())){
				criteria.andEqualTo("pzh",parmMap.get("pzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zdh")!=null && StringUtils.isNotBlank(parmMap.get("zdh").toString())){
				criteria.andEqualTo("zdh",parmMap.get("zdh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jygyh")!=null && StringUtils.isNotBlank(parmMap.get("jygyh").toString())){
				criteria.andEqualTo("jygyh",parmMap.get("jygyh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("shmc")!=null && StringUtils.isNotBlank(parmMap.get("shmc").toString())){
				criteria.andEqualTo("shmc",parmMap.get("shmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("shh")!=null && StringUtils.isNotBlank(parmMap.get("shh").toString())){
				criteria.andEqualTo("shh",parmMap.get("shh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rmbzl")!=null && StringUtils.isNotBlank(parmMap.get("rmbzl").toString())){
				criteria.andEqualTo("rmbzl",parmMap.get("rmbzl").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bz")!=null && StringUtils.isNotBlank(parmMap.get("bz").toString())){
				criteria.andEqualTo("bz",parmMap.get("bz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("pzzl")!=null && StringUtils.isNotBlank(parmMap.get("pzzl").toString())){
				criteria.andEqualTo("pzzl",parmMap.get("pzzl").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ipxz")!=null && StringUtils.isNotBlank(parmMap.get("ipxz").toString())){
				criteria.andEqualTo("ipxz",parmMap.get("ipxz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qqpk")!=null && StringUtils.isNotBlank(parmMap.get("qqpk").toString())){
				criteria.andEqualTo("qqpk",parmMap.get("qqpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jyjebs")!=null && StringUtils.isNotBlank(parmMap.get("jyjebs").toString())){
    			criteria.andCondition("MOD(jyje,"+parmMap.get("jyjebs").toString().trim()+") = 0 ");
    			criteria.andNotEqualTo("jyje", "0");
    			flag = true;
    		}
    		if(parmMap.get("jysjBegin")!=null && StringUtils.isNotBlank(parmMap.get("jysjBegin").toString())){
    			criteria.andGreaterThanOrEqualTo("jysj", TimeUtil.parseStringToDate(parmMap.get("jysjBegin").toString().trim()));
    			flag = true;
    		}
    		if(parmMap.get("jysjEnd")!=null && StringUtils.isNotBlank(parmMap.get("jysjEnd").toString())){
    			criteria.andLessThanOrEqualTo("jysj", TimeUtil.parseStringToDate(parmMap.get("jysjEnd").toString().trim()));
    			flag = true;
    		}
    		if(parmMap.get("jysjb")!=null && StringUtils.isNotBlank(parmMap.get("jysjb").toString())&&
    				parmMap.get("jysje")!=null && StringUtils.isNotBlank(parmMap.get("jysje").toString())){
    			criteria.andCondition("to_number(to_char(jysj,'hh24')) >='"+parmMap.get("jysjb").toString()+"' and to_number(to_char(jysj,'hh24'))<="+parmMap.get("jysje").toString());
    			flag = true;
    		}
    		if(parmMap.get("isxdsd")!=null && StringUtils.isNotBlank(parmMap.get("isxdsd").toString())){
    			if(parmMap.get("isxdsd").toString().equals("1")) {
    				criteria.andCondition("(length(xdrypks) > 1 or length(sdrypks) > 1)");
    			}else if(parmMap.get("isxdsd").toString().equals("0")) {
    				criteria.andEqualTo("xdrypks","1");
    				criteria.andEqualTo("sdrypks","1");
    			}else if(parmMap.get("isxdsd").toString().equals("2")) {
    				criteria.andIsNull("xdrypks");
    				criteria.andIsNull("sdrypks");
    			}
    			flag = true;
    		}
    		criteria.andEqualTo("jdbz","0");
    		criteria.andNotEqualTo("jydfzh", "-");
    		if(parmMap.get("analysis_pk")!=null && StringUtils.isNotBlank(parmMap.get("analysis_pk").toString())){
    			criteria.andCondition("pk in (select mxpk from TB_ZNZF_SCREEN_DATA where type ='1' and analysis_pk = '"+parmMap.get("analysis_pk").toString()+"')");
    			flag = true;
    		}
            if(parmMap.get("cxzh")!=null && StringUtils.isNotBlank(parmMap.get("cxzh").toString())){
                criteria.andCondition("applicationId in (select applicationId from tb_znzf_yhk_mx_qq where   CARDNUMBER = '"+parmMap.get("cxzh").toString()+"')");
                flag = true;
            }
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("YhkMxJgListServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("YhkMxJgListServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }

	@Override
	public void updateDisabByPrimaryKey(String pk) throws Exception {
		yhkMxJgListMapper.updateDisabled(pk);
	}
}
