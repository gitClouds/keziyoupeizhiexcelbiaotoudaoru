package com.unis.service.analysis;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import com.unis.model.analysis.AnalysisConditionModel;
import com.unis.model.analysis.ManulAnalysis;
import com.unis.model.analysis.SfMxJgList;
import com.unis.model.analysis.YhkMxJgList;

//import sun.plugin.javascript.navig.Array;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.unis.service.BaseService;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;

/**
 * <pre>
 * TODO
 * </pre>
 *
 * @author zcf
 * @version 1.0
 * @since 2020-03-26
 */
public interface ManulAnalysisService extends BaseService {

    /**
     * 新增ManulAnalysis实例
     *
     * @param manulAnalysis
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int insert(ManulAnalysis manulAnalysis) throws Exception;

    /**
     * 删除ManulAnalysis实例
     *
     * @param pk
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int delete(String pk) throws Exception;

    /**
     * 更新ManulAnalysis实例
     *
     * @param manulAnalysis
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int updateByPrimaryKey(ManulAnalysis manulAnalysis) throws Exception;

    /**
     * 根据pk主键获取对象
     *
     * @param pk
     * @throws Exception
     */
    ManulAnalysis queryManulAnalysisByPrimaryKey(String pk) throws Exception;

    /**
     * 查询ManulAnalysis实例
     *
     * @param manulAnalysis
     * @throws Exception
     */
    ManulAnalysis queryAsObject(ManulAnalysis manulAnalysis) throws Exception;


    /**
     * 根据条件获取条数
     *
     * @param example
     * @throws Exception
     */
    int queryCountByExample(Example example) throws Exception;

    /**
     * 根据条件获取数据集合
     *
     * @param example
     * @throws Exception
     */
    List<ManulAnalysis> queryListByExample(Example example) throws Exception;

    /**
     * 根据条件获取数据集合(分页),需在example 设定排序
     *
     * @param example
     * @param pageNum
     * @param pageSize
     * @throws Exception
     */
    PageInfo queryPageInfoByExample(Example example, int pageNum, int pageSize) throws Exception;

    /**
     * 根据入参获取数据集合(分页)
     *
     * @param parmMap
     * @param pageNum
     * @param pageSize
     * @return
     * @throws Exception
     */
    ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws Exception;

    void fxData(List<YhkMxJgList> yhkMxJgListList, List<SfMxJgList> sfMxJgListList, AnalysisConditionModel analysisConditionModel) throws Exception;

    List<Map> querySfSdListByExample(Map map) throws Exception;
    List<Map> querySfXdListByExample(Map map) throws Exception;
    List<Map> queryYhkSdListByExample(Map map) throws Exception;
    List<Map> queryYhkXdListByExample(Map map) throws Exception;

}
