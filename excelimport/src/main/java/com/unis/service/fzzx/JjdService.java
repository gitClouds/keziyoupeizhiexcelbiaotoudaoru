package com.unis.service.fzzx;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import com.unis.model.fzzx.Jjd;
import com.unis.pojo.fzzx.JjdModel;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.unis.service.BaseService;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;

/**
 * <pre>
 * TODO
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-01-21
 */
public interface JjdService extends BaseService {

    /**
     * 新增Jjd实例
     * 
     * @param jjd
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int insert(Jjd jjd) throws Exception;

    /**
     * 新增接警单（包含受害人、嫌疑人、商户等信息）
     * @param jjdModel
     * @return
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int insertJjd(JjdModel jjdModel) throws Exception;

    /**
     * 修改接警单（包含受害人、嫌疑人、商户等信息）
     * @param jjdModel
     * @return
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int updateJjd(JjdModel jjdModel) throws Exception;
    /**
     * 删除Jjd实例
     * 
     * @param pk
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int delete(String pk) throws Exception;
    /**
     * 删除Jjd及相关信息
     *
     * @param pk
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int deleteJjd(String pk) throws Exception;

    /**
     * 更新Jjd实例
     * 
     * @param jjd
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int updateByPrimaryKey(Jjd jjd) throws Exception;
    /**
     * 根据pk主键获取对象
     * 
     * @param pk
     * @throws Exception
     */
    Jjd queryJjdByPrimaryKey(String pk) throws Exception;

    /**
     * 根据pk获取接警单及相关子表信息
     * @param pk
     * @return
     * @throws Exception
     */
    Map<String,Object> queryJjdByPk(String pk) throws Exception;

    /**
     * 查询Jjd实例
     * 
     * @param jjd
     * @throws Exception
     */
    Jjd queryAsObject(Jjd jjd) throws Exception;
    
    	
	/**
     * 根据条件获取条数
     * 
     * @param example
     * @throws Exception
     */
	int queryCountByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合
     * 
     * @param example
     * @throws Exception
     */
	List<Jjd> queryListByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合(分页),需在example 设定排序
     * 
     * @param example
     * @param pageNum
     * @param pageSize
     * @throws Exception
     */
	PageInfo queryPageInfoByExample(Example example, int pageNum, int pageSize) throws Exception;

    /**
     * 根据map获取数据集合(分页)
     * @param parmMap
     * @param pageNum
     * @param pageSize
     * @return
     * @throws Exception
     */
    PageInfo queryPageInfoByMap(Map parmMap,int pageNum, int pageSize) throws Exception;

	/**
     * 根据入参获取数据集合(分页)
     * @param parmMap
     * @param pageNum
     * @param pageSize
     * @return
     * @throws Exception
     */
    ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception;

    /**
     * 根据jjdpk查询所有银行卡明细查询，有结果的请求，插入MAC序列表
     * @param jjdpk
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    void insertGabJjdQueue(String jjdpk) throws Exception;

    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int updateAccuratetype(String pk) throws Exception;

}
