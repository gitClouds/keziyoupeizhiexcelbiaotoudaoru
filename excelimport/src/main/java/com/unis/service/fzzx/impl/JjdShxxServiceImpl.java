package com.unis.service.fzzx.impl;

import java.util.List;
import java.util.Map;

import com.unis.common.secure.authc.UserInfo;
import com.unis.common.util.TemplateUtil;
import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.fzzx.JjdShxxMapper;
import com.unis.model.fzzx.JjdShxx;
import com.unis.service.fzzx.JjdShxxService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see JjdShxxService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-01-23
 */
@Service("jjdShxxService")
public class JjdShxxServiceImpl extends BaseServiceImpl implements JjdShxxService {
	private static final Logger logger = LoggerFactory.getLogger(JjdShxxServiceImpl.class);
    @Autowired
    private JjdShxxMapper jjdShxxMapper;

    /**
     * @see JjdShxxService#insert(JjdShxx jjdShxx)
     */
    @Override
    public int insert(JjdShxx jjdShxx) throws Exception {
    	if (jjdShxx!=null){
			jjdShxx.setPk(TemplateUtil.genUUID());
			UserInfo user = this.getUserInfo();
			jjdShxx.setLrdwdm(user.getJgdm());
			jjdShxx.setLrdwmc(user.getJgmc());
			jjdShxx.setLrrjh(user.getJh());
			jjdShxx.setLrrxm(user.getXm());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return jjdShxxMapper.insertSelective(jjdShxx);
    	}else{
    		logger.error("JjdShxxServiceImpl.insert时jjdShxx数据为空。");
    		throw new AppRuntimeException("JjdShxxServiceImpl.insert时jjdShxx数据为空。");
    	}        
    }

    /**
     * @see JjdShxxService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("JjdShxxServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("JjdShxxServiceImpl.delete时pk为空。");
    	}else{
    		return jjdShxxMapper.deleteByPrimaryKey(pk);
    	}
    }
	/**
	 * @see JjdShxxService#deleteByJjd(String)
	 */
	@Override
	public int deleteByJjd(String jjdPk) throws Exception {
		return jjdShxxMapper.deleteByJjd(jjdPk);
	}

	/**
     * @see JjdShxxService#updateByPrimaryKey(JjdShxx jjdShxx)
     */
    @Override
    public int updateByPrimaryKey(JjdShxx jjdShxx) throws Exception {
        if (jjdShxx!=null){
        	if(StringUtils.isBlank(jjdShxx.getPk())){
        		logger.error("JjdShxxServiceImpl.updateByPrimaryKey时jjdShxx.Pk为空。");
        		throw new AppRuntimeException("JjdShxxServiceImpl.updateByPrimaryKey时jjdShxx.Pk为空。");
        	}
	        return jjdShxxMapper.updateByPrimaryKeySelective(jjdShxx);
    	}else{
    		logger.error("JjdShxxServiceImpl.updateByPrimaryKey时jjdShxx数据为空。");
    		throw new AppRuntimeException("JjdShxxServiceImpl.updateByPrimaryKey时jjdShxx数据为空。");
    	}
    }
    /**
     * @see JjdShxxService#queryJjdShxxByPrimaryKey(String pk)
     */
    @Override
    public JjdShxx queryJjdShxxByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("JjdShxxServiceImpl.queryJjdShxxByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("JjdShxxServiceImpl.queryJjdShxxByPrimaryKey时pk为空。");
    	}else{
    		return jjdShxxMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see JjdShxxService#queryAsObject(JjdShxx jjdShxx)
     */
    @Override
    public JjdShxx queryAsObject(JjdShxx jjdShxx) throws Exception {
        if (jjdShxx!=null){
	        return jjdShxxMapper.selectOne(jjdShxx);
    	}else{
    		logger.error("JjdShxxServiceImpl.queryAsObject时jjdShxx数据为空。");
    		throw new AppRuntimeException("JjdShxxServiceImpl.queryAsObject时jjdShxx数据为空。");
    	}
    }
    
    /**
     * @see JjdShxxService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return jjdShxxMapper.selectCountByExample(example);
    	}else{
    		logger.error("JjdShxxServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("JjdShxxServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see JjdShxxService#queryListByExample(Example example)
     */
    @Override
    public List<JjdShxx> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return jjdShxxMapper.selectByExample(example);
    	}else{
    		logger.error("JjdShxxServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("JjdShxxServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see JjdShxxService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(jjdShxxMapper.selectByExample(example));
    	}else{
    		logger.error("JjdShxxServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("JjdShxxServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see JjdShxxService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(JjdShxx.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
			}
    		if(parmMap.get("jjdpk")!=null && StringUtils.isNotBlank(parmMap.get("jjdpk").toString())){
				criteria.andEqualTo("jjdpk",parmMap.get("jjdpk").toString().trim());
			}
    		if(parmMap.get("shmc")!=null && StringUtils.isNotBlank(parmMap.get("shmc").toString())){
				criteria.andEqualTo("shmc",parmMap.get("shmc").toString().trim());
			}
    		if(parmMap.get("lxfs")!=null && StringUtils.isNotBlank(parmMap.get("lxfs").toString())){
				criteria.andEqualTo("lxfs",parmMap.get("lxfs").toString().trim());
			}
    		if(parmMap.get("shh")!=null && StringUtils.isNotBlank(parmMap.get("shh").toString())){
				criteria.andEqualTo("shh",parmMap.get("shh").toString().trim());
			}
    		if(parmMap.get("jydh")!=null && StringUtils.isNotBlank(parmMap.get("jydh").toString())){
				criteria.andEqualTo("jydh",parmMap.get("jydh").toString().trim());
			}
    		if(parmMap.get("cjsj")!=null && StringUtils.isNotBlank(parmMap.get("cjsj").toString())){
				criteria.andEqualTo("cjsj",parmMap.get("cjsj").toString().trim());
			}
    		if(parmMap.get("xgsj")!=null && StringUtils.isNotBlank(parmMap.get("xgsj").toString())){
				criteria.andEqualTo("xgsj",parmMap.get("xgsj").toString().trim());
			}
    		if(parmMap.get("lrdwdm")!=null && StringUtils.isNotBlank(parmMap.get("lrdwdm").toString())){
				criteria.andEqualTo("lrdwdm",parmMap.get("lrdwdm").toString().trim());
			}
    		if(parmMap.get("lrdwmc")!=null && StringUtils.isNotBlank(parmMap.get("lrdwmc").toString())){
				criteria.andEqualTo("lrdwmc",parmMap.get("lrdwmc").toString().trim());
			}
    		if(parmMap.get("lrrxm")!=null && StringUtils.isNotBlank(parmMap.get("lrrxm").toString())){
				criteria.andEqualTo("lrrxm",parmMap.get("lrrxm").toString().trim());
			}
    		if(parmMap.get("lrrjh")!=null && StringUtils.isNotBlank(parmMap.get("lrrjh").toString())){
				criteria.andEqualTo("lrrjh",parmMap.get("lrrjh").toString().trim());
			}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("JjdShxxServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("JjdShxxServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
