package com.unis.service.fzzx.impl;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.fzzx.AjxxMapper;
import com.unis.model.fzzx.Ajxx;
import com.unis.service.fzzx.AjxxService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see AjxxService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-04-04
 */
@Service("ajxxService")
public class AjxxServiceImpl extends BaseServiceImpl implements AjxxService {
	private static final Logger logger = LoggerFactory.getLogger(AjxxServiceImpl.class);
    @Autowired
    private AjxxMapper ajxxMapper;

    /**
     * @see AjxxService#insert(Ajxx ajxx)
     */
    @Override
    public int insert(Ajxx ajxx) throws Exception {
    	if (ajxx!=null){
	        //ajxx.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return ajxxMapper.insertSelective(ajxx);
    	}else{
    		logger.error("AjxxServiceImpl.insert时ajxx数据为空。");
    		throw new AppRuntimeException("AjxxServiceImpl.insert时ajxx数据为空。");
    	}        
    }

    /**
     * @see AjxxService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("AjxxServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("AjxxServiceImpl.delete时pk为空。");
    	}else{
    		return ajxxMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see AjxxService#updateByPrimaryKey(Ajxx ajxx)
     */
    @Override
    public int updateByPrimaryKey(Ajxx ajxx) throws Exception {
        if (ajxx!=null){
        	if(StringUtils.isBlank(ajxx.getPk())){
        		logger.error("AjxxServiceImpl.updateByPrimaryKey时ajxx.Pk为空。");
        		throw new AppRuntimeException("AjxxServiceImpl.updateByPrimaryKey时ajxx.Pk为空。");
        	}
	        return ajxxMapper.updateByPrimaryKeySelective(ajxx);
    	}else{
    		logger.error("AjxxServiceImpl.updateByPrimaryKey时ajxx数据为空。");
    		throw new AppRuntimeException("AjxxServiceImpl.updateByPrimaryKey时ajxx数据为空。");
    	}
    }
    /**
     * @see AjxxService#queryAjxxByPrimaryKey(String pk)
     */
    @Override
    public Ajxx queryAjxxByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("AjxxServiceImpl.queryAjxxByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("AjxxServiceImpl.queryAjxxByPrimaryKey时pk为空。");
    	}else{
    		return ajxxMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see AjxxService#queryAsObject(Ajxx ajxx)
     */
    @Override
    public Ajxx queryAsObject(Ajxx ajxx) throws Exception {
        if (ajxx!=null){
	        return ajxxMapper.selectOne(ajxx);
    	}else{
    		logger.error("AjxxServiceImpl.queryAsObject时ajxx数据为空。");
    		throw new AppRuntimeException("AjxxServiceImpl.queryAsObject时ajxx数据为空。");
    	}
    }
    
    /**
     * @see AjxxService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return ajxxMapper.selectCountByExample(example);
    	}else{
    		logger.error("AjxxServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("AjxxServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see AjxxService#queryListByExample(Example example)
     */
    @Override
    public List<Ajxx> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return ajxxMapper.selectByExample(example);
    	}else{
    		logger.error("AjxxServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("AjxxServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see AjxxService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(ajxxMapper.selectByExample(example));
    	}else{
    		logger.error("AjxxServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("AjxxServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see AjxxService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(Ajxx.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jjdhm")!=null && StringUtils.isNotBlank(parmMap.get("jjdhm").toString())){
				criteria.andEqualTo("jjdhm",parmMap.get("jjdhm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ajbh")!=null && StringUtils.isNotBlank(parmMap.get("ajbh").toString())){
				criteria.andEqualTo("ajbh",parmMap.get("ajbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ajid")!=null && StringUtils.isNotBlank(parmMap.get("ajid").toString())){
				criteria.andEqualTo("ajid",parmMap.get("ajid").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zfje")!=null && StringUtils.isNotBlank(parmMap.get("zfje").toString())){
				criteria.andEqualTo("zfje",parmMap.get("zfje").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jdxz")!=null && StringUtils.isNotBlank(parmMap.get("jdxz").toString())){
				criteria.andEqualTo("jdxz",parmMap.get("jdxz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sldw")!=null && StringUtils.isNotBlank(parmMap.get("sldw").toString())){
				criteria.andEqualTo("sldw",parmMap.get("sldw").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sldwmc")!=null && StringUtils.isNotBlank(parmMap.get("sldwmc").toString())){
				criteria.andEqualTo("sldwmc",parmMap.get("sldwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jjsj")!=null && StringUtils.isNotBlank(parmMap.get("jjsj").toString())){
				criteria.andEqualTo("jjsj",parmMap.get("jjsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("barxm")!=null && StringUtils.isNotBlank(parmMap.get("barxm").toString())){
				criteria.andEqualTo("barxm",parmMap.get("barxm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("barsfzh")!=null && StringUtils.isNotBlank(parmMap.get("barsfzh").toString())){
				criteria.andEqualTo("barsfzh",parmMap.get("barsfzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("barlxdh")!=null && StringUtils.isNotBlank(parmMap.get("barlxdh").toString())){
				criteria.andEqualTo("barlxdh",parmMap.get("barlxdh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jjfs")!=null && StringUtils.isNotBlank(parmMap.get("jjfs").toString())){
				criteria.andEqualTo("jjfs",parmMap.get("jjfs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ajlx")!=null && StringUtils.isNotBlank(parmMap.get("ajlx").toString())){
				criteria.andEqualTo("ajlx",parmMap.get("ajlx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ajlb")!=null && StringUtils.isNotBlank(parmMap.get("ajlb").toString())){
				criteria.andEqualTo("ajlb",parmMap.get("ajlb").toString().trim());
				flag = true;
			}
    		if(parmMap.get("afsj")!=null && StringUtils.isNotBlank(parmMap.get("afsj").toString())){
				criteria.andEqualTo("afsj",parmMap.get("afsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("saje")!=null && StringUtils.isNotBlank(parmMap.get("saje").toString())){
				criteria.andEqualTo("saje",parmMap.get("saje").toString().trim());
				flag = true;
			}
    		if(parmMap.get("afdd")!=null && StringUtils.isNotBlank(parmMap.get("afdd").toString())){
				criteria.andEqualTo("afdd",parmMap.get("afdd").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jyaq")!=null && StringUtils.isNotBlank(parmMap.get("jyaq").toString())){
				criteria.andEqualTo("jyaq",parmMap.get("jyaq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sawz")!=null && StringUtils.isNotBlank(parmMap.get("sawz").toString())){
				criteria.andEqualTo("sawz",parmMap.get("sawz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bz")!=null && StringUtils.isNotBlank(parmMap.get("bz").toString())){
				criteria.andEqualTo("bz",parmMap.get("bz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zt")!=null && StringUtils.isNotBlank(parmMap.get("zt").toString())){
				criteria.andEqualTo("zt",parmMap.get("zt").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cjsj")!=null && StringUtils.isNotBlank(parmMap.get("cjsj").toString())){
				criteria.andEqualTo("cjsj",parmMap.get("cjsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xgsj")!=null && StringUtils.isNotBlank(parmMap.get("xgsj").toString())){
				criteria.andEqualTo("xgsj",parmMap.get("xgsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("area")!=null && StringUtils.isNotBlank(parmMap.get("area").toString())){
				criteria.andEqualTo("area",parmMap.get("area").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrdwdm")!=null && StringUtils.isNotBlank(parmMap.get("lrdwdm").toString())){
				criteria.andEqualTo("lrdwdm",parmMap.get("lrdwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrdwmc")!=null && StringUtils.isNotBlank(parmMap.get("lrdwmc").toString())){
				criteria.andEqualTo("lrdwmc",parmMap.get("lrdwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrrxm")!=null && StringUtils.isNotBlank(parmMap.get("lrrxm").toString())){
				criteria.andEqualTo("lrrxm",parmMap.get("lrrxm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrrjh")!=null && StringUtils.isNotBlank(parmMap.get("lrrjh").toString())){
				criteria.andEqualTo("lrrjh",parmMap.get("lrrjh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ladwdm")!=null && StringUtils.isNotBlank(parmMap.get("ladwdm").toString())){
				criteria.andEqualTo("ladwdm",parmMap.get("ladwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ladwmc")!=null && StringUtils.isNotBlank(parmMap.get("ladwmc").toString())){
				criteria.andEqualTo("ladwmc",parmMap.get("ladwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lasj")!=null && StringUtils.isNotBlank(parmMap.get("lasj").toString())){
				criteria.andEqualTo("lasj",parmMap.get("lasj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lxr")!=null && StringUtils.isNotBlank(parmMap.get("lxr").toString())){
				criteria.andEqualTo("lxr",parmMap.get("lxr").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lxdh")!=null && StringUtils.isNotBlank(parmMap.get("lxdh").toString())){
				criteria.andEqualTo("lxdh",parmMap.get("lxdh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lxrsj")!=null && StringUtils.isNotBlank(parmMap.get("lxrsj").toString())){
				criteria.andEqualTo("lxrsj",parmMap.get("lxrsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ajlbzl")!=null && StringUtils.isNotBlank(parmMap.get("ajlbzl").toString())){
				criteria.andEqualTo("ajlbzl",parmMap.get("ajlbzl").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("AjxxServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("AjxxServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
