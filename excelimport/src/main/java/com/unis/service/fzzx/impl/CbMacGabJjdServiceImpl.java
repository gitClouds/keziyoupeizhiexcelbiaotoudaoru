package com.unis.service.fzzx.impl;

import java.util.List;
import java.util.Map;

import com.unis.common.util.TemplateUtil;
import com.unis.dto.ResultDto;
import com.unis.service.fzzx.CbMacGabJjdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.CbMacGabJjdMapper;
import com.unis.model.znzf.CbMacGabJjd;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see CbMacGabJjdService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-05-27
 */
@Service("cbMacGabJjdService")
public class CbMacGabJjdServiceImpl extends BaseServiceImpl implements CbMacGabJjdService {
	private static final Logger logger = LoggerFactory.getLogger(CbMacGabJjdServiceImpl.class);
    @Autowired
    private CbMacGabJjdMapper cbMacGabJjdMapper;

    @Override
	public int checkCbMacGabByJjdpk(String jjdpk) throws Exception{
		if(StringUtils.isNotBlank(jjdpk)){
			return cbMacGabJjdMapper.selectCountbByJjdpk(jjdpk);
		}else{
			logger.error("JjdServiceImpl.checkCbMacGabByJjdpk时jjdpk为空。");
			throw new AppRuntimeException("JjdServiceImpl.checkCbMacGabByJjdpk时jjdpk为空。");
		}
	}
    /**
     * @see CbMacGabJjdService#insert(CbMacGabJjd cbMacGabJjd)
     */
    @Override
    public int insert(CbMacGabJjd cbMacGabJjd) throws Exception {
    	if (cbMacGabJjd!=null){
	        cbMacGabJjd.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return cbMacGabJjdMapper.insertSelective(cbMacGabJjd);
    	}else{
    		logger.error("CbMacGabJjdServiceImpl.insert时cbMacGabJjd数据为空。");
    		throw new AppRuntimeException("CbMacGabJjdServiceImpl.insert时cbMacGabJjd数据为空。");
    	}        
    }

    /**
     * @see CbMacGabJjdService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("CbMacGabJjdServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("CbMacGabJjdServiceImpl.delete时pk为空。");
    	}else{
    		return cbMacGabJjdMapper.deleteByPrimaryKey(pk);
    	}
    }
	@Override
	public int deleteByJjdpk(String jjdpk) throws Exception {
		if(StringUtils.isBlank(jjdpk)){
			logger.error("CbMacGabJjdServiceImpl.deleteByJjdpk时jjdpk为空。");
			throw new AppRuntimeException("CbMacGabJjdServiceImpl.deleteByJjdpk时jjdpk为空。");
		}else{
			return cbMacGabJjdMapper.deleteByJjdpk(jjdpk);
		}
	}
    /**
     * @see CbMacGabJjdService#updateByPrimaryKey(CbMacGabJjd cbMacGabJjd)
     */
    @Override
    public int updateByPrimaryKey(CbMacGabJjd cbMacGabJjd) throws Exception {
        if (cbMacGabJjd!=null){
        	if(StringUtils.isBlank(cbMacGabJjd.getPk())){
        		logger.error("CbMacGabJjdServiceImpl.updateByPrimaryKey时cbMacGabJjd.Pk为空。");
        		throw new AppRuntimeException("CbMacGabJjdServiceImpl.updateByPrimaryKey时cbMacGabJjd.Pk为空。");
        	}
	        return cbMacGabJjdMapper.updateByPrimaryKeySelective(cbMacGabJjd);
    	}else{
    		logger.error("CbMacGabJjdServiceImpl.updateByPrimaryKey时cbMacGabJjd数据为空。");
    		throw new AppRuntimeException("CbMacGabJjdServiceImpl.updateByPrimaryKey时cbMacGabJjd数据为空。");
    	}
    }
    /**
     * @see CbMacGabJjdService#queryCbMacGabJjdByPrimaryKey(String pk)
     */
    @Override
    public CbMacGabJjd queryCbMacGabJjdByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("CbMacGabJjdServiceImpl.queryCbMacGabJjdByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("CbMacGabJjdServiceImpl.queryCbMacGabJjdByPrimaryKey时pk为空。");
    	}else{
    		return cbMacGabJjdMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see CbMacGabJjdService#queryAsObject(CbMacGabJjd cbMacGabJjd)
     */
    @Override
    public CbMacGabJjd queryAsObject(CbMacGabJjd cbMacGabJjd) throws Exception {
        if (cbMacGabJjd!=null){
	        return cbMacGabJjdMapper.selectOne(cbMacGabJjd);
    	}else{
    		logger.error("CbMacGabJjdServiceImpl.queryAsObject时cbMacGabJjd数据为空。");
    		throw new AppRuntimeException("CbMacGabJjdServiceImpl.queryAsObject时cbMacGabJjd数据为空。");
    	}
    }
    
    /**
     * @see CbMacGabJjdService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return cbMacGabJjdMapper.selectCountByExample(example);
    	}else{
    		logger.error("CbMacGabJjdServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("CbMacGabJjdServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see CbMacGabJjdService#queryListByExample(Example example)
     */
    @Override
    public List<CbMacGabJjd> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return cbMacGabJjdMapper.selectByExample(example);
    	}else{
    		logger.error("CbMacGabJjdServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("CbMacGabJjdServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see CbMacGabJjdService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(cbMacGabJjdMapper.selectByExample(example));
    	}else{
    		logger.error("CbMacGabJjdServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("CbMacGabJjdServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see CbMacGabJjdService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(CbMacGabJjd.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sertype")!=null && StringUtils.isNotBlank(parmMap.get("sertype").toString())){
				criteria.andEqualTo("sertype",parmMap.get("sertype").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jjdpk")!=null && StringUtils.isNotBlank(parmMap.get("jjdpk").toString())){
				criteria.andEqualTo("jjdpk",parmMap.get("jjdpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zt")!=null && StringUtils.isNotBlank(parmMap.get("zt").toString())){
				criteria.andEqualTo("zt",parmMap.get("zt").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("CbMacGabJjdServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("CbMacGabJjdServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
