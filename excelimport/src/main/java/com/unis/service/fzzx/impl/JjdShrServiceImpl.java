package com.unis.service.fzzx.impl;

import java.util.List;
import java.util.Map;

import com.unis.common.secure.authc.UserInfo;
import com.unis.common.util.TemplateUtil;
import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.fzzx.JjdShrMapper;
import com.unis.model.fzzx.JjdShr;
import com.unis.service.fzzx.JjdShrService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see JjdShrService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-01-23
 */
@Service("jjdShrService")
public class JjdShrServiceImpl extends BaseServiceImpl implements JjdShrService {
	private static final Logger logger = LoggerFactory.getLogger(JjdShrServiceImpl.class);
    @Autowired
    private JjdShrMapper jjdShrMapper;

    /**
     * @see JjdShrService#insert(JjdShr jjdShr)
     */
    @Override
    public int insert(JjdShr jjdShr) throws Exception {
    	if (jjdShr!=null){
	        //menu.setPk(getPk("seqName","jgdm","A"));
	        return jjdShrMapper.insertSelective(jjdShr);
    	}else{
    		logger.error("JjdShrServiceImpl.insert时jjdShr数据为空。");
    		throw new AppRuntimeException("JjdShrServiceImpl.insert时jjdShr数据为空。");
    	}        
    }

    /**
     * @see JjdShrService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("JjdShrServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("JjdShrServiceImpl.delete时pk为空。");
    	}else{
    		return jjdShrMapper.deleteByPrimaryKey(pk);
    	}
    }

	/**
	 * @see JjdShrService#deleteByJjd(String)
	 */
    @Override
    public int deleteByJjd(String jjdPk) throws Exception{
    	return jjdShrMapper.deleteByJjd(jjdPk);
	}
	/**
	 * @see JjdShrService#deleteNotExistsByJjdPk(String, String)
	 */
	@Override
	public int deleteNotExistsByJjdPk(String jjdPk, String shrPks) throws Exception {
		return 0;
	}

	/**
     * @see JjdShrService#updateByPrimaryKey(JjdShr jjdShr)
     */
    @Override
    public int updateByPrimaryKey(JjdShr jjdShr) throws Exception {
        if (jjdShr!=null){
        	if(StringUtils.isBlank(jjdShr.getPk())){
        		logger.error("JjdShrServiceImpl.updateByPrimaryKey时jjdShr.Pk为空。");
        		throw new AppRuntimeException("JjdShrServiceImpl.updateByPrimaryKey时jjdShr.Pk为空。");
        	}
	        return jjdShrMapper.updateByPrimaryKeySelective(jjdShr);
    	}else{
    		logger.error("JjdShrServiceImpl.updateByPrimaryKey时jjdShr数据为空。");
    		throw new AppRuntimeException("JjdShrServiceImpl.updateByPrimaryKey时jjdShr数据为空。");
    	}
    }
    /**
     * @see JjdShrService#queryJjdShrByPrimaryKey(String pk)
     */
    @Override
    public JjdShr queryJjdShrByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("JjdShrServiceImpl.queryJjdShrByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("JjdShrServiceImpl.queryJjdShrByPrimaryKey时pk为空。");
    	}else{
    		return jjdShrMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see JjdShrService#queryAsObject(JjdShr jjdShr)
     */
    @Override
    public JjdShr queryAsObject(JjdShr jjdShr) throws Exception {
        if (jjdShr!=null){
	        return jjdShrMapper.selectOne(jjdShr);
    	}else{
    		logger.error("JjdShrServiceImpl.queryAsObject时jjdShr数据为空。");
    		throw new AppRuntimeException("JjdShrServiceImpl.queryAsObject时jjdShr数据为空。");
    	}
    }
    
    /**
     * @see JjdShrService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return jjdShrMapper.selectCountByExample(example);
    	}else{
    		logger.error("JjdShrServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("JjdShrServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see JjdShrService#queryListByExample(Example example)
     */
    @Override
    public List<JjdShr> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return jjdShrMapper.selectByExample(example);
    	}else{
    		logger.error("JjdShrServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("JjdShrServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see JjdShrService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(jjdShrMapper.selectByExample(example));
    	}else{
    		logger.error("JjdShrServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("JjdShrServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see JjdShrService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(JjdShr.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
			}
    		if(parmMap.get("jjdpk")!=null && StringUtils.isNotBlank(parmMap.get("jjdpk").toString())){
				criteria.andEqualTo("jjdpk",parmMap.get("jjdpk").toString().trim());
			}
    		if(parmMap.get("sfzh")!=null && StringUtils.isNotBlank(parmMap.get("sfzh").toString())){
				criteria.andEqualTo("sfzh",parmMap.get("sfzh").toString().trim());
			}
    		if(parmMap.get("dh")!=null && StringUtils.isNotBlank(parmMap.get("dh").toString())){
				criteria.andEqualTo("dh",parmMap.get("dh").toString().trim());
			}
    		if(parmMap.get("yhk")!=null && StringUtils.isNotBlank(parmMap.get("yhk").toString())){
				criteria.andEqualTo("yhk",parmMap.get("yhk").toString().trim());
			}
    		if(parmMap.get("ckr")!=null && StringUtils.isNotBlank(parmMap.get("ckr").toString())){
				criteria.andEqualTo("ckr",parmMap.get("ckr").toString().trim());
			}
    		if(parmMap.get("zfb")!=null && StringUtils.isNotBlank(parmMap.get("zfb").toString())){
				criteria.andEqualTo("zfb",parmMap.get("zfb").toString().trim());
			}
    		if(parmMap.get("wx")!=null && StringUtils.isNotBlank(parmMap.get("wx").toString())){
				criteria.andEqualTo("wx",parmMap.get("wx").toString().trim());
			}
    		if(parmMap.get("jydh")!=null && StringUtils.isNotBlank(parmMap.get("jydh").toString())){
				criteria.andEqualTo("jydh",parmMap.get("jydh").toString().trim());
			}
    		if(parmMap.get("cft")!=null && StringUtils.isNotBlank(parmMap.get("cft").toString())){
				criteria.andEqualTo("cft",parmMap.get("cft").toString().trim());
			}
    		if(parmMap.get("qq")!=null && StringUtils.isNotBlank(parmMap.get("qq").toString())){
				criteria.andEqualTo("qq",parmMap.get("qq").toString().trim());
			}
    		if(parmMap.get("zzfs")!=null && StringUtils.isNotBlank(parmMap.get("zzfs").toString())){
				criteria.andEqualTo("zzfs",parmMap.get("zzfs").toString().trim());
			}
    		if(parmMap.get("zzje")!=null && StringUtils.isNotBlank(parmMap.get("zzje").toString())){
				criteria.andEqualTo("zzje",parmMap.get("zzje").toString().trim());
			}
    		if(parmMap.get("zzsj")!=null && StringUtils.isNotBlank(parmMap.get("zzsj").toString())){
				criteria.andEqualTo("zzsj",parmMap.get("zzsj").toString().trim());
			}
    		if(parmMap.get("bz")!=null && StringUtils.isNotBlank(parmMap.get("bz").toString())){
				criteria.andEqualTo("bz",parmMap.get("bz").toString().trim());
			}
    		if(parmMap.get("cjsj")!=null && StringUtils.isNotBlank(parmMap.get("cjsj").toString())){
				criteria.andEqualTo("cjsj",parmMap.get("cjsj").toString().trim());
			}
    		if(parmMap.get("xgsj")!=null && StringUtils.isNotBlank(parmMap.get("xgsj").toString())){
				criteria.andEqualTo("xgsj",parmMap.get("xgsj").toString().trim());
			}
    		if(parmMap.get("lrdwdm")!=null && StringUtils.isNotBlank(parmMap.get("lrdwdm").toString())){
				criteria.andEqualTo("lrdwdm",parmMap.get("lrdwdm").toString().trim());
			}
    		if(parmMap.get("lrdwmc")!=null && StringUtils.isNotBlank(parmMap.get("lrdwmc").toString())){
				criteria.andEqualTo("lrdwmc",parmMap.get("lrdwmc").toString().trim());
			}
    		if(parmMap.get("lrrxm")!=null && StringUtils.isNotBlank(parmMap.get("lrrxm").toString())){
				criteria.andEqualTo("lrrxm",parmMap.get("lrrxm").toString().trim());
			}
    		if(parmMap.get("lrrjh")!=null && StringUtils.isNotBlank(parmMap.get("lrrjh").toString())){
				criteria.andEqualTo("lrrjh",parmMap.get("lrrjh").toString().trim());
			}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("JjdShrServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("JjdShrServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
