package com.unis.service.fzzx.impl;

import java.util.List;
import java.util.Map;

import com.unis.common.secure.authc.UserInfo;
import com.unis.common.util.TemplateUtil;
import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.fzzx.UserFocusMapper;
import com.unis.model.fzzx.UserFocus;
import com.unis.service.fzzx.UserFocusService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see UserFocusService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-05-22
 */
@Service("userFocusService")
public class UserFocusServiceImpl extends BaseServiceImpl implements UserFocusService {
	private static final Logger logger = LoggerFactory.getLogger(UserFocusServiceImpl.class);
    @Autowired
    private UserFocusMapper userFocusMapper;

    /**
     * @see UserFocusService#insert(UserFocus userFocus)
     */
    @Override
    public int insert(UserFocus userFocus) throws Exception {
    	if (userFocus!=null){
	        userFocus.setPk(TemplateUtil.genUUID());

	        return userFocusMapper.insertSelective(userFocus);
    	}else{
    		logger.error("UserFocusServiceImpl.insert时userFocus数据为空。");
    		throw new AppRuntimeException("UserFocusServiceImpl.insert时userFocus数据为空。");
    	}        
    }

	@Override
	public int insert(String jjdpk) throws Exception {
		UserInfo userInfo = this.getUserInfo();
		UserFocus userFocus = new UserFocus();
		userFocus.setJjdpk(jjdpk);
		userFocus.setPk(TemplateUtil.genUUID());
		userFocus.setUserpk(userInfo.getPk());
		return userFocusMapper.insertSelective(userFocus);
	}

	/**
     * @see UserFocusService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("UserFocusServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("UserFocusServiceImpl.delete时pk为空。");
    	}else{
    		return userFocusMapper.deleteByPrimaryKey(pk);
    	}
    }

	@Override
	public int deleteByJjdpk(String jjdpk) throws Exception {
		UserInfo userInfo = this.getUserInfo();
		UserFocus userFocus = new UserFocus();
		userFocus.setJjdpk(jjdpk);
		userFocus.setUserpk(userInfo.getPk());
		return userFocusMapper.delete(userFocus);
	}

	/**
     * @see UserFocusService#updateByPrimaryKey(UserFocus userFocus)
     */
    @Override
    public int updateByPrimaryKey(UserFocus userFocus) throws Exception {
        if (userFocus!=null){
        	if(StringUtils.isBlank(userFocus.getPk())){
        		logger.error("UserFocusServiceImpl.updateByPrimaryKey时userFocus.Pk为空。");
        		throw new AppRuntimeException("UserFocusServiceImpl.updateByPrimaryKey时userFocus.Pk为空。");
        	}
	        return userFocusMapper.updateByPrimaryKeySelective(userFocus);
    	}else{
    		logger.error("UserFocusServiceImpl.updateByPrimaryKey时userFocus数据为空。");
    		throw new AppRuntimeException("UserFocusServiceImpl.updateByPrimaryKey时userFocus数据为空。");
    	}
    }
    /**
     * @see UserFocusService#queryUserFocusByPrimaryKey(String pk)
     */
    @Override
    public UserFocus queryUserFocusByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("UserFocusServiceImpl.queryUserFocusByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("UserFocusServiceImpl.queryUserFocusByPrimaryKey时pk为空。");
    	}else{
    		return userFocusMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see UserFocusService#queryAsObject(UserFocus userFocus)
     */
    @Override
    public UserFocus queryAsObject(UserFocus userFocus) throws Exception {
        if (userFocus!=null){
	        return userFocusMapper.selectOne(userFocus);
    	}else{
    		logger.error("UserFocusServiceImpl.queryAsObject时userFocus数据为空。");
    		throw new AppRuntimeException("UserFocusServiceImpl.queryAsObject时userFocus数据为空。");
    	}
    }
    
    /**
     * @see UserFocusService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return userFocusMapper.selectCountByExample(example);
    	}else{
    		logger.error("UserFocusServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("UserFocusServiceImpl.queryCountByExample时example数据为空。");
    	}
    }

	@Override
	public int queryCountByJjdpk(String jjdpk) {
		UserInfo userInfo = this.getUserInfo();
		Example example = new Example(UserFocus.class);
		Example.Criteria criteria = example.createCriteria();
		criteria.andEqualTo("jjdpk",jjdpk);
		criteria.andEqualTo("userpk",userInfo.getPk());

		return userFocusMapper.selectCountByExample(example);
	}

	/**
     * @see UserFocusService#queryListByExample(Example example)
     */
    @Override
    public List<UserFocus> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return userFocusMapper.selectByExample(example);
    	}else{
    		logger.error("UserFocusServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("UserFocusServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see UserFocusService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(userFocusMapper.selectByExample(example));
    	}else{
    		logger.error("UserFocusServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("UserFocusServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see UserFocusService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(UserFocus.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jjdpk")!=null && StringUtils.isNotBlank(parmMap.get("jjdpk").toString())){
				criteria.andEqualTo("jjdpk",parmMap.get("jjdpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("userpk")!=null && StringUtils.isNotBlank(parmMap.get("userpk").toString())){
				criteria.andEqualTo("userpk",parmMap.get("userpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("UserFocusServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("UserFocusServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
