package com.unis.service.fzzx.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.unis.common.secure.authc.UserInfo;
import com.unis.common.util.StrUtil;
import com.unis.common.util.TemplateUtil;
import com.unis.common.util.TimeUtil;
import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.fzzx.JjdBmdMapper;
import com.unis.model.fzzx.JjdBmd;
import com.unis.service.fzzx.JjdBmdService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see JjdBmdService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-01-29
 */
@Service("jjdBmdService")
public class JjdBmdServiceImpl extends BaseServiceImpl implements JjdBmdService {
	private static final Logger logger = LoggerFactory.getLogger(JjdBmdServiceImpl.class);
    @Autowired
    private JjdBmdMapper jjdBmdMapper;

    /**
     * @see JjdBmdService#insert(JjdBmd jjdBmd)
     */
    @Override
    public int insert(JjdBmd jjdBmd) throws Exception {
    	if (jjdBmd!=null){
	        jjdBmd.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
			UserInfo user = this.getUserInfo();
			jjdBmd.setLrdwdm(user.getJgdm());
			jjdBmd.setLrdwmc(user.getJgmc());
			jjdBmd.setLrrxm(user.getXm());
			jjdBmd.setLrrjh(user.getJh());
	        return jjdBmdMapper.insertSelective(jjdBmd);
    	}else{
    		logger.error("JjdBmdServiceImpl.insert时jjdBmd数据为空。");
    		throw new AppRuntimeException("JjdBmdServiceImpl.insert时jjdBmd数据为空。");
    	}        
    }

    /**
     * @see JjdBmdService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("JjdBmdServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("JjdBmdServiceImpl.delete时pk为空。");
    	}else{
    		return jjdBmdMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see JjdBmdService#updateByPrimaryKey(JjdBmd jjdBmd)
     */
    @Override
    public int updateByPrimaryKey(JjdBmd jjdBmd) throws Exception {
        if (jjdBmd!=null){
        	if(StringUtils.isBlank(jjdBmd.getPk())){
        		logger.error("JjdBmdServiceImpl.updateByPrimaryKey时jjdBmd.Pk为空。");
        		throw new AppRuntimeException("JjdBmdServiceImpl.updateByPrimaryKey时jjdBmd.Pk为空。");
        	}
        	UserInfo user = this.getUserInfo();
        	jjdBmd.setXgsj(this.getDbDate());
        	jjdBmd.setXgdwdm(user.getJgdm());
        	jjdBmd.setXgdwmc(user.getJgmc());
        	jjdBmd.setXgr(user.getXm());
        	jjdBmd.setXgrjh(user.getJh());
	        return jjdBmdMapper.updateByPrimaryKeySelective(jjdBmd);
    	}else{
    		logger.error("JjdBmdServiceImpl.updateByPrimaryKey时jjdBmd数据为空。");
    		throw new AppRuntimeException("JjdBmdServiceImpl.updateByPrimaryKey时jjdBmd数据为空。");
    	}
    }
    /**
     * @see JjdBmdService#queryJjdBmdByPrimaryKey(String pk)
     */
    @Override
    public JjdBmd queryJjdBmdByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("JjdBmdServiceImpl.queryJjdBmdByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("JjdBmdServiceImpl.queryJjdBmdByPrimaryKey时pk为空。");
    	}else{
    		return jjdBmdMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see JjdBmdService#queryAsObject(JjdBmd jjdBmd)
     */
    @Override
    public JjdBmd queryAsObject(JjdBmd jjdBmd) throws Exception {
        if (jjdBmd!=null){
	        return jjdBmdMapper.selectOne(jjdBmd);
    	}else{
    		logger.error("JjdBmdServiceImpl.queryAsObject时jjdBmd数据为空。");
    		throw new AppRuntimeException("JjdBmdServiceImpl.queryAsObject时jjdBmd数据为空。");
    	}
    }
    
    /**
     * @see JjdBmdService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return jjdBmdMapper.selectCountByExample(example);
    	}else{
    		logger.error("JjdBmdServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("JjdBmdServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see JjdBmdService#queryListByExample(Example example)
     */
    @Override
    public List<JjdBmd> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return jjdBmdMapper.selectByExample(example);
    	}else{
    		logger.error("JjdBmdServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("JjdBmdServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see JjdBmdService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(jjdBmdMapper.selectByExample(example));
    	}else{
    		logger.error("JjdBmdServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("JjdBmdServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see JjdBmdService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(JjdBmd.class);
    		example.setOrderByClause("cjsj DESC,pk asc");//设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
			criteria.andEqualTo("yxx",1);
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zh")!=null && StringUtils.isNotBlank(parmMap.get("zh").toString())){
				criteria.andEqualTo("zh",parmMap.get("zh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zhmc")!=null && StringUtils.isNotBlank(parmMap.get("zhmc").toString())){
				criteria.andEqualTo("zhmc",parmMap.get("zhmc").toString().trim());
				flag = true;
			}

    		if(parmMap.get("lrdwdm")!=null && StringUtils.isNotBlank(parmMap.get("lrdwdm").toString())){
				//criteria.andEqualTo("lrdwdm",parmMap.get("lrdwdm").toString().trim());
				if (!parmMap.get("lrdwdm").toString().trim().startsWith("01")){
					String jgdmLike = StrUtil.removeDmZeor(parmMap.get("lrdwdm").toString().trim())+"%";
					criteria.andLike("lrdwdm",jgdmLike);
				}
				flag = true;
			}
			if(parmMap.get("cjsj_begin")!=null && StringUtils.isNotBlank(parmMap.get("cjsj_begin").toString())){
				criteria.andGreaterThanOrEqualTo("cjsj", TimeUtil.strToDate(parmMap.get("cjsj_begin").toString().trim(),null));
				flag = true;
			}
			if(parmMap.get("cjsj_end")!=null && StringUtils.isNotBlank(parmMap.get("cjsj_end").toString())){
				criteria.andLessThanOrEqualTo("cjsj",TimeUtil.strToDate(parmMap.get("cjsj_end").toString().trim(),null));
				flag = true;
			}

    		/*if(parmMap.get("lrrjh")!=null && StringUtils.isNotBlank(parmMap.get("lrrjh").toString())){
				criteria.andEqualTo("lrrjh",parmMap.get("lrrjh").toString().trim());
				flag = true;
			}
			if(parmMap.get("bz")!=null && StringUtils.isNotBlank(parmMap.get("bz").toString())){
				criteria.andEqualTo("bz",parmMap.get("bz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cjsj")!=null && StringUtils.isNotBlank(parmMap.get("cjsj").toString())){
				criteria.andEqualTo("cjsj",parmMap.get("cjsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xgsj")!=null && StringUtils.isNotBlank(parmMap.get("xgsj").toString())){
				criteria.andEqualTo("xgsj",parmMap.get("xgsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xgdwdm")!=null && StringUtils.isNotBlank(parmMap.get("xgdwdm").toString())){
				criteria.andEqualTo("xgdwdm",parmMap.get("xgdwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xgrjh")!=null && StringUtils.isNotBlank(parmMap.get("xgrjh").toString())){
				criteria.andEqualTo("xgrjh",parmMap.get("xgrjh").toString().trim());
				flag = true;
			}*/
    		//一个条件都没有的时候给一个默认条件
    		/*if(!flag){
				Date defaultDate = TimeUtil.strToDate(TimeUtil.getNow("yyyy-MM-dd")+" 00:00:00",null);
				criteria.andGreaterThanOrEqualTo("cjsj",defaultDate);
    		}*/
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("JjdBmdServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("JjdBmdServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
