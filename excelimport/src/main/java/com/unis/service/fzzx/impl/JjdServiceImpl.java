package com.unis.service.fzzx.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.unis.common.secure.authc.UserInfo;
import com.unis.common.util.JsonUtil;
import com.unis.common.util.StrUtil;
import com.unis.common.util.TemplateUtil;
import com.unis.common.util.TimeUtil;
import com.unis.dto.ResultDto;
import com.unis.mapper.fzzx.JjdMapper;
import com.unis.model.znzf.CbMacGabJjd;
import com.unis.pojo.fzzx.JjdModel;
import com.unis.model.fzzx.JjdShr;
import com.unis.model.fzzx.JjdShxx;
import com.unis.model.fzzx.JjdXyr;
import com.unis.service.fzzx.*;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.model.fzzx.Jjd;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see JjdService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-01-21
 */
@Service("jjdService")
public class JjdServiceImpl extends BaseServiceImpl implements JjdService {
	private static final Logger logger = LoggerFactory.getLogger(JjdServiceImpl.class);
    @Autowired
    private JjdMapper jjdMapper;
    @Autowired
    private JjdShrService jjdShrService;
    @Autowired
    private JjdXyrService jjdXyrService;
    @Autowired
    private JjdShxxService jjdShxxService;
    @Autowired
	private CbMacGabJjdService cbMacGabJjdService;

	public void insertGabJjdQueue(String jjdpk) throws Exception{
		CbMacGabJjd cbMacGabJjd = new CbMacGabJjd();
		cbMacGabJjd.setSertype((short)1);
		cbMacGabJjd.setJjdpk(jjdpk);
		cbMacGabJjdService.insert(cbMacGabJjd);
		//将该接警单下所有明细查询加入序列表，做串并分析
		jjdMapper.insertQueueMac(jjdpk);
	}

	@Override
	public int updateAccuratetype(String pk) throws Exception {
		return jjdMapper.updateAccuratetype(pk);
	}

	/**
     * @see JjdService#insert(Jjd jjd)
     */
    @Override
    public int insert(Jjd jjd) throws Exception {
    	if (jjd!=null){
	        jjd.setPk(TemplateUtil.genUUID());
			UserInfo user = this.getUserInfo();
	        jjd.setLrdwdm(user.getJgdm());
	        jjd.setLrdwmc(user.getJgmc());
	        jjd.setLrrjh(user.getJh());
	        jjd.setLrrxm(user.getXm());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	        return jjdMapper.insertSelective(jjd);
    	}else{
    		logger.error("JjdServiceImpl.insert时jjd数据为空。");
    		throw new AppRuntimeException("JjdServiceImpl.insert时jjd数据为空。");
    	}        
    }

	/**
	 * @see JjdService#insertJjd(JjdModel)
	 */
    @Override
    public int insertJjd(JjdModel jjdModel) throws Exception{
    	if (jjdModel!=null){
			jjdModel.setPk(TemplateUtil.genUUID());
			UserInfo user = this.getUserInfo();
			Jjd jjd = JSON.parseObject(JSON.toJSONString(jjdModel), Jjd.class);
			jjd.setLrdwdm(user.getJgdm());
			jjd.setLrdwmc(user.getJgmc());
			jjd.setLrrjh(user.getJh());
			jjd.setLrrxm(user.getXm());
			Example example = new Example(Jjd.class);
			Example.Criteria criteria = example.createCriteria();
			criteria.andEqualTo("jjdhm",jjd.getJjdhm());
			int existsNum = this.queryCountByExample(example);
			if(existsNum>0){
				return 2;
			}
			jjdMapper.insertSelective(jjd);
			//this.insert(jjdModel.getJjd());
			if (jjdModel.getShr()!=null && jjdModel.getShr().size()>0){
				List shrList = jjdModel.getShr();
				for (int i=0;i<shrList.size();i++){
					if (shrList.get(i)!=null){
						JjdShr shr = JSON.parseObject(JSON.toJSONString(shrList.get(i)), JjdShr.class);
						if (shr!=null && shr.getZzsj()!=null && shr.getZzfs()!=null){
							shr.setJjdpk(jjdModel.getPk());
							shr.setPk(TemplateUtil.genUUID());
							shr.setLrdwdm(user.getJgdm());
							shr.setLrdwmc(user.getJgmc());
							shr.setLrrjh(user.getJh());
							shr.setLrrxm(user.getXm());
							jjdShrService.insert(shr);
						}
					}
				}
			}
			if (jjdModel.getXyr()!=null && jjdModel.getXyr().size()>0){
				List<JjdXyr> xyrList = jjdModel.getXyr();
				for (int i=0;i<xyrList.size();i++){
					if (xyrList.get(i)!=null) {
						JjdXyr xyr = JSON.parseObject(JSON.toJSONString(xyrList.get(i)), JjdXyr.class);
						if (xyr!=null && xyr.getZzsj()!=null && xyr.getZzfs()!=null) {
							xyr.setJjdpk(jjdModel.getPk());
							jjdXyrService.insert(xyr);
						}
					}
				}
			}
			if(jjdModel.getShxx()!=null && jjdModel.getShxx().size()>0){
				List<JjdShxx> shxxList = jjdModel.getShxx();
				for (int i=0;i<shxxList.size();i++){
					if(shxxList.get(i)!=null) {
						JjdShxx shxx = JSON.parseObject(JSON.toJSONString(shxxList.get(i)), JjdShxx.class);
						if (shxx!=null && (StringUtils.isNotBlank(shxx.getShmc()) || StringUtils.isNotBlank(shxx.getShh()) || StringUtils.isNotBlank(shxx.getLxfs()) || StringUtils.isNotBlank(shxx.getJydh()))) {
							shxx.setJjdpk(jjdModel.getPk());
							jjdShxxService.insert(shxx);
						}
					}
				}
			}
			return 1;
		}else{
			logger.error("JjdServiceImpl.insertJjd时jjd数据为空。");
			throw new AppRuntimeException("JjdServiceImpl.insertJjd时jjd数据为空。");
		}
	}
	/**
	 * @see JjdService#updateJjd(JjdModel)
	 */
	@Override
	public int updateJjd(JjdModel jjdModel) throws Exception {
		if (jjdModel!=null && StringUtils.isNotBlank(jjdModel.getPk())){
			Date xgsj = this.getDbDate();
			Jjd newJjd = JSON.parseObject(JSON.toJSONString(jjdModel), Jjd.class);

			//验证hash是否一致，一致不需要进行更新操作
			Jjd nowJjd = this.queryJjdByPrimaryKey(jjdModel.getPk());
			if (nowJjd.hashCode()!=newJjd.hashCode()){
				newJjd.setXgsj(xgsj);
				this.updateByPrimaryKey(newJjd);
			}
			//修改接警单信息
			UserInfo user = this.getUserInfo();
			//循环遍历，对接警单下受害人进行对应操作
			if (jjdModel.getShr()!=null && jjdModel.getShr().size()>0){
				String shrPks=null;
				StringBuilder sb = new StringBuilder();
				List shrList = jjdModel.getShr();
				for (int i=0;i<shrList.size();i++){
					if (shrList.get(i)!=null) {
						JjdShr shr = JSON.parseObject(JSON.toJSONString(shrList.get(i)), JjdShr.class);
						if (shr!=null  && shr.getZzsj()!=null && shr.getZzfs()!=null) {
							//pk 不为空的修改
							if (StringUtils.isNotBlank(shr.getPk())) {
								sb.append("'").append(shr.getPk()).append("',");
								shr.setXgsj(xgsj);
								jjdShrService.updateByPrimaryKey(shr);
							} else {
								//pk 为空的insert
								String shrPk = TemplateUtil.genUUID();
								shr.setPk(shrPk);
								shr.setLrdwdm(user.getJgdm());
								shr.setLrdwmc(user.getJgmc());
								shr.setLrrjh(user.getJh());
								shr.setLrrxm(user.getXm());

								sb.append("'").append(shr.getPk()).append("',");
								shr.setJjdpk(jjdModel.getPk());
								jjdShrService.insert(shr);
							}
						}
					}
				}

				//删除循环中不存在的pk对应数据
				//暂时屏蔽删除，需确认需求
				/*shrPks = sb.toString().substring(1,sb.toString().length()-1);
				jjdShrService.deleteNotExistsByJjdPk(jjdModel.getPk(),shrPks);*/
			}
			//循环遍历，对接警单下嫌疑人进行对应操作
			if (jjdModel.getXyr()!=null && jjdModel.getXyr().size()>0){
				String xyrPks=null;
				StringBuilder sb = new StringBuilder();
				List xyrList = jjdModel.getXyr();
				for (int i=0;i<xyrList.size();i++){
					if (xyrList.get(i)!=null) {
						JjdXyr xyr = JSON.parseObject(JSON.toJSONString(xyrList.get(i)), JjdXyr.class);
						if (xyr!=null && xyr.getZzsj()!=null && xyr.getZzfs()!=null ) {
							//pk 不为空的修改
							if (StringUtils.isNotBlank(xyr.getPk())) {
								sb.append("'").append(xyr.getPk()).append("',");
								xyr.setXgsj(xgsj);
								jjdXyrService.updateByPrimaryKey(xyr);
							} else {
								//pk 为空的insert
								String xyrPk = TemplateUtil.genUUID();
								xyr.setPk(xyrPk);
								xyr.setLrdwdm(user.getJgdm());
								xyr.setLrdwmc(user.getJgmc());
								xyr.setLrrjh(user.getJh());
								xyr.setLrrxm(user.getXm());

								sb.append("'").append(xyr.getPk()).append("',");
								xyr.setJjdpk(jjdModel.getPk());
								jjdXyrService.insert(xyr);
							}
						}
					}
				}

				//删除循环中不存在的pk对应数据
				//暂时屏蔽删除，需确认需求
				/*xyrPks = sb.toString().substring(1,sb.toString().length()-1);
				jjdXyrService.deleteNotExistsByJjdPk(jjdModel.getPk(),xyrPks);*/
			}

			if(jjdModel.getShxx()!=null && jjdModel.getShxx().size()>0){
				String shxxPks=null;
				StringBuilder sb = new StringBuilder();
				List shxxList = jjdModel.getShxx();
				for (int i=0;i<shxxList.size();i++){
					if(shxxList.get(i)!=null) {
						JjdShxx shxx = JSON.parseObject(JSON.toJSONString(shxxList.get(i)), JjdShxx.class);
						if(shxx!=null && (StringUtils.isNotBlank(shxx.getShmc()) || StringUtils.isNotBlank(shxx.getShh()) || StringUtils.isNotBlank(shxx.getLxfs()) || StringUtils.isNotBlank(shxx.getJydh()))) {
							if (StringUtils.isNotBlank(shxx.getPk())) {
								sb.append("'").append(shxx.getPk()).append("',");
								shxx.setXgsj(xgsj);
								jjdShxxService.updateByPrimaryKey(shxx);
							} else {
								//pk 为空的insert
								String xyrPk = TemplateUtil.genUUID();
								shxx.setPk(xyrPk);
								shxx.setLrdwdm(user.getJgdm());
								shxx.setLrdwmc(user.getJgmc());
								shxx.setLrrjh(user.getJh());
								shxx.setLrrxm(user.getXm());

								sb.append("'").append(shxx.getPk()).append("',");
								shxx.setJjdpk(jjdModel.getPk());
								jjdShxxService.insert(shxx);
							}
						}
					}
				}
				//删除循环中不存在的pk对应数据
				//暂时屏蔽删除，需确认需求
				/*shxxPks = sb.toString().substring(1,sb.toString().length()-1);
				jjdXyrService.deleteNotExistsByJjdPk(jjdModel.getPk(),shxxPks);*/
			}
			return 1;
		}else{
			logger.error("JjdServiceImpl.updateJjd时jjd数据为空。");
			throw new AppRuntimeException("JjdServiceImpl.updateJjd时jjd数据为空。");
		}
	}

	/**
     * @see JjdService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("JjdServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("JjdServiceImpl.delete时pk为空。");
    	}else{
    		return jjdMapper.deleteByPrimaryKey(pk);
    	}
    }

	/**
	 * @see JjdService#deleteJjd(String)
	 */
    @Override
    public int deleteJjd(String pk) throws Exception{
    	if(StringUtils.isNotBlank(pk)){
			jjdMapper.deleteByPrimaryKey(pk);
			jjdShrService.deleteByJjd(pk);
			jjdXyrService.deleteByJjd(pk);
			jjdShxxService.deleteByJjd(pk);
			return 1;
		}else{
			logger.error("JjdServiceImpl.deleteJjd时pk为空。");
			throw new AppRuntimeException("JjdServiceImpl.deleteJjd时pk为空。");
		}
	}
    /**
     * @see JjdService#updateByPrimaryKey(Jjd jjd)
     */
    @Override
    public int updateByPrimaryKey(Jjd jjd) throws Exception {
        if (jjd!=null){
        	if(StringUtils.isBlank(jjd.getPk())){
        		logger.error("JjdServiceImpl.updateByPrimaryKey时jjd.Pk为空。");
        		throw new AppRuntimeException("JjdServiceImpl.updateByPrimaryKey时jjd.Pk为空。");
        	}
	        return jjdMapper.updateByPrimaryKeySelective(jjd);
    	}else{
    		logger.error("JjdServiceImpl.updateByPrimaryKey时jjd数据为空。");
    		throw new AppRuntimeException("JjdServiceImpl.updateByPrimaryKey时jjd数据为空。");
    	}
    }
    /**
     * @see JjdService#queryJjdByPrimaryKey(String pk)
     */
    @Override
    public Jjd queryJjdByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("JjdServiceImpl.queryJjdByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("JjdServiceImpl.queryJjdByPrimaryKey时pk为空。");
    	}else{
    		return jjdMapper.selectByPrimaryKey(pk);
    	}
    }

	/**
	 *@see JjdService#queryJjdByPk(String)
	 */
    @Override
    public Map<String,Object> queryJjdByPk(String pk) throws Exception{
    	Map<String,Object> map = null;
    	if(StringUtils.isNotBlank(pk)){
    		Jjd jjd = this.queryJjdByPrimaryKey(pk);
    		if (jjd!=null){
				//JSON.parseObject(JSON.toJSONString(xyrList.get(i)), JjdXyr.class)
				map = JsonUtil.objectToMap(jjd);
			}
		}
    	return map;
	}
    
    
    /**
     * @see JjdService#queryAsObject(Jjd jjd)
     */
    @Override
    public Jjd queryAsObject(Jjd jjd) throws Exception {
        if (jjd!=null){
	        return jjdMapper.selectOne(jjd);
    	}else{
    		logger.error("JjdServiceImpl.queryAsObject时jjd数据为空。");
    		throw new AppRuntimeException("JjdServiceImpl.queryAsObject时jjd数据为空。");
    	}
    }
    
    /**
     * @see JjdService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return jjdMapper.selectCountByExample(example);
    	}else{
    		logger.error("JjdServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("JjdServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see JjdService#queryListByExample(Example example)
     */
    @Override
    public List<Jjd> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return jjdMapper.selectByExample(example);
    	}else{
    		logger.error("JjdServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("JjdServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see JjdService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(jjdMapper.selectByExample(example));
    	}else{
    		logger.error("JjdServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("JjdServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }

	@Override
	public PageInfo queryPageInfoByMap(Map parmMap, int pageNum, int pageSize) throws Exception {
		if (parmMap!=null && !parmMap.isEmpty() && parmMap.size()>0){
			PageHelper.startPage(pageNum,pageSize);
			return new PageInfo(jjdMapper.queryListByMap(parmMap));
		}else {
			logger.error("JjdServiceImpl.queryPageInfoByMap时map数据为空。");
			throw new AppRuntimeException("JjdServiceImpl.queryPageInfoByMap时map数据为空。");
		}
	}

	/**
     * @see JjdService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(Jjd.class);
    		example.setOrderByClause("CJSJ DESC,pk asc");//设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改

			//增加权限控制，对于非系统管理员用户，增加lrdwdm权限，只能查本单位的
			Subject currentUser = SecurityUtils.getSubject();
			if (!currentUser.hasRole("_SYSTEMADMIN")&&!currentUser.hasRole("ADMIN")){
				UserInfo userInfo = (UserInfo) currentUser.getPrincipal();
				criteria.andLike("lrdwdm", StrUtil.removeDmZeor(userInfo.getJgdm())+"%");
			}

			if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
			}
			if(parmMap.get("jjdhm")!=null && StringUtils.isNotBlank(parmMap.get("jjdhm").toString())){
				criteria.andEqualTo("jjdhm",parmMap.get("jjdhm").toString().trim());
			}
			if(parmMap.get("barxm")!=null && StringUtils.isNotBlank(parmMap.get("barxm").toString())){
				criteria.andLike("barxm","%"+parmMap.get("barxm").toString().trim()+"%");
			}
			if(parmMap.get("ajbh")!=null && StringUtils.isNotBlank(parmMap.get("ajbh").toString())){
				criteria.andEqualTo("ajbh",parmMap.get("ajbh").toString().trim());
			}
			if(parmMap.get("ajid")!=null && StringUtils.isNotBlank(parmMap.get("ajid").toString())){
				criteria.andEqualTo("ajid",parmMap.get("ajid").toString().trim());
			}
			if(parmMap.get("jjfs")!=null && StringUtils.isNotBlank(parmMap.get("jjfs").toString())){
				criteria.andEqualTo("jjfs",parmMap.get("jjfs").toString().trim());
			}
			if(parmMap.get("ajlx")!=null && StringUtils.isNotBlank(parmMap.get("ajlx").toString())){
				criteria.andEqualTo("ajlx",parmMap.get("ajlx").toString().trim());
			}
			if(parmMap.get("ajlb")!=null && StringUtils.isNotBlank(parmMap.get("ajlb").toString())){
				criteria.andEqualTo("ajlb",parmMap.get("ajlb").toString().trim());
			}
			/*if(parmMap.get("lrdwdm")!=null && StringUtils.isNotBlank(parmMap.get("lrdwdm").toString())){
				criteria.andEqualTo("lrdwdm",parmMap.get("lrdwdm").toString().trim());
			}*/
			if(parmMap.get("sldw")!=null && StringUtils.isNotBlank(parmMap.get("sldw").toString())){
				criteria.andLike("sldw", StrUtil.removeDmZeor(parmMap.get("sldw").toString().trim())+"%");
			}
			if(parmMap.get("afsj_begin")!=null && StringUtils.isNotBlank(parmMap.get("afsj_begin").toString())){
				criteria.andGreaterThanOrEqualTo("afsj",TimeUtil.strToDate(parmMap.get("afsj_begin").toString().trim(),null));
			}
			if(parmMap.get("afsj_end")!=null && StringUtils.isNotBlank(parmMap.get("afsj_end").toString())){
				criteria.andLessThanOrEqualTo("afsj",TimeUtil.strToDate(parmMap.get("afsj_end").toString().trim(),null));
			}
			if (parmMap.get("saje_begin")!=null && StringUtils.isNotBlank(parmMap.get("saje_begin").toString()) && NumberUtils.isNumber(parmMap.get("saje_begin").toString())){
				criteria.andGreaterThanOrEqualTo("saje", NumberUtils.toLong(parmMap.get("saje_begin").toString(),0));
			}
			if (parmMap.get("saje_end")!=null && StringUtils.isNotBlank(parmMap.get("saje_end").toString()) && NumberUtils.isNumber(parmMap.get("saje_end").toString())){
				criteria.andLessThanOrEqualTo("saje", NumberUtils.toLong(parmMap.get("saje_end").toString(),999999999999L));
			}
			if(parmMap.get("jjsj_begin")!=null && StringUtils.isNotBlank(parmMap.get("jjsj_begin").toString())){
				criteria.andGreaterThanOrEqualTo("jjsj",TimeUtil.strToDate(parmMap.get("jjsj_begin").toString().trim(),null));
			}
			if(parmMap.get("jjsj_end")!=null && StringUtils.isNotBlank(parmMap.get("jjsj_end").toString())){
				criteria.andLessThanOrEqualTo("jjsj",TimeUtil.strToDate(parmMap.get("jjsj_end").toString().trim(),null));
			}

			//一个条件都没有时给个默认条件
			/*if(!flag){
				Date defaultDate = TimeUtil.strToDate(TimeUtil.getNow("yyyy-MM-dd")+" 00:00:00",null);
				criteria.andGreaterThanOrEqualTo("afsj",defaultDate);
			}*/

			/*if(parmMap.get("afsj")!=null && StringUtils.isNotBlank(parmMap.get("afsj").toString())){
				criteria.andEqualTo("afsj",TimeUtil.strToDate(parmMap.get("afsj").toString().trim(),null));
			}
			if(parmMap.get("zfje")!=null && StringUtils.isNotBlank(parmMap.get("zfje").toString())){
				criteria.andEqualTo("zfje",parmMap.get("zfje").toString().trim());
			}
			if(parmMap.get("jdxz")!=null && StringUtils.isNotBlank(parmMap.get("jdxz").toString())){
				criteria.andEqualTo("jdxz",parmMap.get("jdxz").toString().trim());
			}
			if(parmMap.get("jjsj")!=null && StringUtils.isNotBlank(parmMap.get("jjsj").toString())){
				criteria.andEqualTo("jjsj", TimeUtil.strToDate(parmMap.get("jjsj").toString().trim(),null));
			}
			if(parmMap.get("barxm")!=null && StringUtils.isNotBlank(parmMap.get("barxm").toString())){
				criteria.andEqualTo("barxm",parmMap.get("barxm").toString().trim());
			}
			if(parmMap.get("barsfzh")!=null && StringUtils.isNotBlank(parmMap.get("barsfzh").toString())){
				criteria.andEqualTo("barsfzh",parmMap.get("barsfzh").toString().trim());
			}
			if(parmMap.get("barlxdh")!=null && StringUtils.isNotBlank(parmMap.get("barlxdh").toString())){
				criteria.andEqualTo("barlxdh",parmMap.get("barlxdh").toString().trim());
			}
			if(parmMap.get("saje")!=null && StringUtils.isNotBlank(parmMap.get("saje").toString())){
				criteria.andEqualTo("saje",parmMap.get("saje").toString().trim());
			}
			if(parmMap.get("afdd")!=null && StringUtils.isNotBlank(parmMap.get("afdd").toString())){
				criteria.andEqualTo("afdd",parmMap.get("afdd").toString().trim());
			}
			if(parmMap.get("jyaq")!=null && StringUtils.isNotBlank(parmMap.get("jyaq").toString())){
				criteria.andEqualTo("jyaq",parmMap.get("jyaq").toString().trim());
			}
			if(parmMap.get("sawz")!=null && StringUtils.isNotBlank(parmMap.get("sawz").toString())){
				criteria.andEqualTo("sawz",parmMap.get("sawz").toString().trim());
			}
			if(parmMap.get("bz")!=null && StringUtils.isNotBlank(parmMap.get("bz").toString())){
				criteria.andEqualTo("bz",parmMap.get("bz").toString().trim());
			}
			if(parmMap.get("zt")!=null && StringUtils.isNotBlank(parmMap.get("zt").toString())){
				criteria.andEqualTo("zt",parmMap.get("zt").toString().trim());
			}
			if(parmMap.get("cjsj")!=null && StringUtils.isNotBlank(parmMap.get("cjsj").toString())){
				criteria.andEqualTo("cjsj", TimeUtil.strToDate(parmMap.get("cjsj").toString().trim(),null));
			}
			if(parmMap.get("xgsj")!=null && StringUtils.isNotBlank(parmMap.get("xgsj").toString())){
				criteria.andEqualTo("xgsj",TimeUtil.strToDate(parmMap.get("xgsj").toString().trim(),null));
			}
			if(parmMap.get("area")!=null && StringUtils.isNotBlank(parmMap.get("area").toString())){
				criteria.andEqualTo("area",parmMap.get("area").toString().trim());
			}
			if(parmMap.get("lrdwmc")!=null && StringUtils.isNotBlank(parmMap.get("lrdwmc").toString())){
				criteria.andEqualTo("lrdwmc",parmMap.get("lrdwmc").toString().trim());
			}
			if(parmMap.get("lrrxm")!=null && StringUtils.isNotBlank(parmMap.get("lrrxm").toString())){
				criteria.andEqualTo("lrrxm",parmMap.get("lrrxm").toString().trim());
			}
			if(parmMap.get("lrrjh")!=null && StringUtils.isNotBlank(parmMap.get("lrrjh").toString())){
				criteria.andEqualTo("lrrjh",parmMap.get("lrrjh").toString().trim());
			}*/
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("JjdServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("JjdServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }

}
