package com.unis.service;

import com.unis.common.secure.authc.UserInfo;
import org.apache.ibatis.annotations.Param;

import java.text.ParseException;
import java.util.Date;

/**
 * Created by Administrator on 2018/12/29/029.
 */
public interface BaseService {
    /**
     * 获取当前登录用户信息
     * @return
     */
    UserInfo getUserInfo();

    /**
     * 获取指定格式的数据库当前时间字符串
     * 如果为null,则默认为"yyyy-MM-dd HH:mm:ss"
     * @param pattern 此格式为java的格式化
     * @return
     * @throws ParseException
     */
    String getStrDbDate(@Param("pattern")String pattern)throws ParseException;

    /**
     * 获取格式为"yyyy-MM-dd HH:mm:ss"的数据库当前时间字符串
     * @return
     */
    String getStrDbDate();

    /**
     * 获取数据库当前时间
     * @return
     * @throws ParseException
     */
    Date getDbDate()throws ParseException;
    /**
     * 主键生成（32位）
     * prefix+dwdm+ yyyyMMdd + (sequence.nextval.substr(length-8))
     * 长度pkLength=32 = prefix.length + dwdm.length(12) + yyyyMMdd(8) +  sequence.substr(length-8) (8) + Random(32-28-prefix.length)(minlenght=1;maxlength=3)
     * @param seqName sequence名
     * @param dwdm 单位
     * @param prefix 前缀 the prefix length  between 1 and 3
     * @return pk
     * @throws Exception
     */
    String getPk(@Param("seqName")String seqName, @Param("dwdm")String dwdm, @Param("prefix")String prefix)throws Exception;
}
