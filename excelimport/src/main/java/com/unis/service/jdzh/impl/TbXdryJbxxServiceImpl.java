package com.unis.service.jdzh.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.unis.common.util.TemplateUtil;
import com.unis.dto.ResultDto;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.hd.TbHdInfoService;
import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.jdzh.TbXdryJbxxMapper;
import com.unis.model.hd.TbHdInfo;
import com.unis.model.jdzh.TbXdryChxx;
import com.unis.model.jdzh.TbXdryJbxx;
import com.unis.service.jdzh.TbXdryChxxService;
import com.unis.service.jdzh.TbXdryJbxxService;
import com.unis.wsClient.bean.WSTbXdryChxx;
import com.unis.wsClient.bean.WSTbXdryJbxx;
import com.unis.wsClient.bean.WSxdryxx;
import com.unis.common.config.DS;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see TbXdryJbxxService
 * </pre>
 *
 * @author gongjl
 * @version 1.0
 * @since 2020-03-05
 */
@Service("tbXdryJbxxService")
public class TbXdryJbxxServiceImpl extends BaseServiceImpl implements TbXdryJbxxService {
	private static final Logger logger = LoggerFactory.getLogger(TbXdryJbxxServiceImpl.class);
    @Autowired
    private TbXdryJbxxMapper tbXdryJbxxMapper;
    @Autowired
    private TbHdInfoService tbHdInfoService;
    @Autowired
    private TbXdryChxxService tbXdryChxxService;

    /**
     * @see TbXdryJbxxService#insert(TbXdryJbxx tbXdryJbxx)
     */
    @Override
	@DS("datasource2")
    public int insert(TbXdryJbxx tbXdryJbxx) throws Exception {
    	if (tbXdryJbxx!=null){
    		tbXdryJbxx.setRyxh(TemplateUtil.genUUID());
	        return tbXdryJbxxMapper.insertSelective(tbXdryJbxx);
    	}else{
    		logger.error("TbXdryJbxxServiceImpl.insert时tbXdryJbxx数据为空。");
    		throw new AppRuntimeException("TbXdryJbxxServiceImpl.insert时tbXdryJbxx数据为空。");
    	}        
    }

    /**
     * @see TbXdryJbxxService#delete(String pk)
     */
    @Override
	@DS("datasource2")
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("TbXdryJbxxServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("TbXdryJbxxServiceImpl.delete时pk为空。");
    	}else{
    		return tbXdryJbxxMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see TbXdryJbxxService#updateByPrimaryKey(TbXdryJbxx tbXdryJbxx)
     */
    @Override
	@DS("datasource2")
    public int updateByPrimaryKey(TbXdryJbxx tbXdryJbxx) throws Exception {
        if (tbXdryJbxx!=null){
	        return tbXdryJbxxMapper.updateByPrimaryKeySelective(tbXdryJbxx);
    	}else{
    		logger.error("TbXdryJbxxServiceImpl.updateByPrimaryKey时tbXdryJbxx数据为空。");
    		throw new AppRuntimeException("TbXdryJbxxServiceImpl.updateByPrimaryKey时tbXdryJbxx数据为空。");
    	}
    }
    /**
     * @see TbXdryJbxxService#queryTbXdryJbxxByPrimaryKey(String pk)
     */
    @Override
	@DS("datasource2")
    public TbXdryJbxx queryTbXdryJbxxByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("TbXdryJbxxServiceImpl.queryTbXdryJbxxByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("TbXdryJbxxServiceImpl.queryTbXdryJbxxByPrimaryKey时pk为空。");
    	}else{
    		return tbXdryJbxxMapper.selectByPrimaryKey(pk);
    	}
    }

	@Override
	@DS("datasource2")
	public ResultDto queryXdry(Map parmMap, int pageNum, int pageSize) throws Exception {
		PageHelper.startPage(pageNum,pageSize);
		PageInfo pageInfo = new PageInfo(tbXdryJbxxMapper.queryXdry(parmMap));
		ResultDto result = new ResultDto();
		result.setRows(pageInfo.getList());
		result.setPage(pageInfo.getPageNum());
		//result.setTotal(pageInfo.getPages());
		result.setTotal((int)pageInfo.getTotal());
		return result;
	}
    
    /**
     * @see TbXdryJbxxService#queryAsObject(TbXdryJbxx tbXdryJbxx)
     */
    @Override
	@DS("datasource2")
    public TbXdryJbxx queryAsObject(TbXdryJbxx tbXdryJbxx) throws Exception {
        if (tbXdryJbxx!=null){
	        return tbXdryJbxxMapper.selectOne(tbXdryJbxx);
    	}else{
    		logger.error("TbXdryJbxxServiceImpl.queryAsObject时tbXdryJbxx数据为空。");
    		throw new AppRuntimeException("TbXdryJbxxServiceImpl.queryAsObject时tbXdryJbxx数据为空。");
    	}
    }
    
    /**
     * @see TbXdryJbxxService#queryCountByExample(Example example)
     */
    @Override
	@DS("datasource2")
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return tbXdryJbxxMapper.selectCountByExample(example);
    	}else{
    		logger.error("TbXdryJbxxServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("TbXdryJbxxServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see TbXdryJbxxService#queryListByExample(Example example)
     */
    @Override
    @DS("datasource2")
    public List<TbXdryJbxx> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return tbXdryJbxxMapper.selectByExample(example);
    	}else{
    		logger.error("TbXdryJbxxServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("TbXdryJbxxServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see TbXdryJbxxService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
	@DS("datasource2")
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(tbXdryJbxxMapper.selectByExample(example));
    	}else{
    		logger.error("TbXdryJbxxServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("TbXdryJbxxServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see TbXdryJbxxService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
	@DS("datasource2")
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(TbXdryJbxx.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("ryxh")!=null && StringUtils.isNotBlank(parmMap.get("ryxh").toString())){
				criteria.andEqualTo("ryxh",parmMap.get("ryxh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xdryly")!=null && StringUtils.isNotBlank(parmMap.get("xdryly").toString())){
				criteria.andEqualTo("xdryly",parmMap.get("xdryly").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ywlx")!=null && StringUtils.isNotBlank(parmMap.get("ywlx").toString())){
				criteria.andEqualTo("ywlx",parmMap.get("ywlx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ywxh")!=null && StringUtils.isNotBlank(parmMap.get("ywxh").toString())){
				criteria.andEqualTo("ywxh",parmMap.get("ywxh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xyrbh")!=null && StringUtils.isNotBlank(parmMap.get("xyrbh").toString())){
				criteria.andEqualTo("xyrbh",parmMap.get("xyrbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xm")!=null && StringUtils.isNotBlank(parmMap.get("xm").toString())){
				criteria.andEqualTo("xm",parmMap.get("xm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xdrydjlydm")!=null && StringUtils.isNotBlank(parmMap.get("xdrydjlydm").toString())){
				criteria.andEqualTo("xdrydjlydm",parmMap.get("xdrydjlydm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cyzjdm")!=null && StringUtils.isNotBlank(parmMap.get("cyzjdm").toString())){
				criteria.andEqualTo("cyzjdm",parmMap.get("cyzjdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfzhm18")!=null && StringUtils.isNotBlank(parmMap.get("sfzhm18").toString())){
				criteria.andEqualTo("sfzhm18",parmMap.get("sfzhm18").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gmsfhm")!=null && StringUtils.isNotBlank(parmMap.get("gmsfhm").toString())){
				criteria.andEqualTo("gmsfhm",parmMap.get("gmsfhm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zjhm")!=null && StringUtils.isNotBlank(parmMap.get("zjhm").toString())){
				criteria.andEqualTo("zjhm",parmMap.get("zjhm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bmch")!=null && StringUtils.isNotBlank(parmMap.get("bmch").toString())){
				criteria.andEqualTo("bmch",parmMap.get("bmch").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xbdm")!=null && StringUtils.isNotBlank(parmMap.get("xbdm").toString())){
				criteria.andEqualTo("xbdm",parmMap.get("xbdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("csrq")!=null && StringUtils.isNotBlank(parmMap.get("csrq").toString())){
				criteria.andEqualTo("csrq",parmMap.get("csrq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("mzdm")!=null && StringUtils.isNotBlank(parmMap.get("mzdm").toString())){
				criteria.andEqualTo("mzdm",parmMap.get("mzdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sg")!=null && StringUtils.isNotBlank(parmMap.get("sg").toString())){
				criteria.andEqualTo("sg",parmMap.get("sg").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jggjdqdm")!=null && StringUtils.isNotBlank(parmMap.get("jggjdqdm").toString())){
				criteria.andEqualTo("jggjdqdm",parmMap.get("jggjdqdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xldm")!=null && StringUtils.isNotBlank(parmMap.get("xldm").toString())){
				criteria.andEqualTo("xldm",parmMap.get("xldm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("grsfdm")!=null && StringUtils.isNotBlank(parmMap.get("grsfdm").toString())){
				criteria.andEqualTo("grsfdm",parmMap.get("grsfdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("hyzkdm")!=null && StringUtils.isNotBlank(parmMap.get("hyzkdm").toString())){
				criteria.andEqualTo("hyzkdm",parmMap.get("hyzkdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jyqk")!=null && StringUtils.isNotBlank(parmMap.get("jyqk").toString())){
				criteria.andEqualTo("jyqk",parmMap.get("jyqk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gzdw_dwmc")!=null && StringUtils.isNotBlank(parmMap.get("gzdw_dwmc").toString())){
				criteria.andEqualTo("gzdw_dwmc",parmMap.get("gzdw_dwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("szzwbh")!=null && StringUtils.isNotBlank(parmMap.get("szzwbh").toString())){
				criteria.andEqualTo("szzwbh",parmMap.get("szzwbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rydnabh")!=null && StringUtils.isNotBlank(parmMap.get("rydnabh").toString())){
				criteria.andEqualTo("rydnabh",parmMap.get("rydnabh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("hjdz_xzqhmc")!=null && StringUtils.isNotBlank(parmMap.get("hjdz_xzqhmc").toString())){
				criteria.andEqualTo("hjdz_xzqhmc",parmMap.get("hjdz_xzqhmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("hjdz_dzmc")!=null && StringUtils.isNotBlank(parmMap.get("hjdz_dzmc").toString())){
				criteria.andEqualTo("hjdz_dzmc",parmMap.get("hjdz_dzmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("hjdz_gajgmc")!=null && StringUtils.isNotBlank(parmMap.get("hjdz_gajgmc").toString())){
				criteria.andEqualTo("hjdz_gajgmc",parmMap.get("hjdz_gajgmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sjjzd_xzqhmc")!=null && StringUtils.isNotBlank(parmMap.get("sjjzd_xzqhmc").toString())){
				criteria.andEqualTo("sjjzd_xzqhmc",parmMap.get("sjjzd_xzqhmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sjjzd_dzmc")!=null && StringUtils.isNotBlank(parmMap.get("sjjzd_dzmc").toString())){
				criteria.andEqualTo("sjjzd_dzmc",parmMap.get("sjjzd_dzmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sjjzd_gajgmc")!=null && StringUtils.isNotBlank(parmMap.get("sjjzd_gajgmc").toString())){
				criteria.andEqualTo("sjjzd_gajgmc",parmMap.get("sjjzd_gajgmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrr_xm")!=null && StringUtils.isNotBlank(parmMap.get("lrr_xm").toString())){
				criteria.andEqualTo("lrr_xm",parmMap.get("lrr_xm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrdw_dwmc")!=null && StringUtils.isNotBlank(parmMap.get("lrdw_dwmc").toString())){
				criteria.andEqualTo("lrdw_dwmc",parmMap.get("lrdw_dwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrsj")!=null && StringUtils.isNotBlank(parmMap.get("lrsj").toString())){
				criteria.andEqualTo("lrsj",parmMap.get("lrsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sf828k")!=null && StringUtils.isNotBlank(parmMap.get("sf828k").toString())){
				criteria.andEqualTo("sf828k",parmMap.get("sf828k").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sdrybh")!=null && StringUtils.isNotBlank(parmMap.get("sdrybh").toString())){
				criteria.andEqualTo("sdrybh",parmMap.get("sdrybh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxh")!=null && StringUtils.isNotBlank(parmMap.get("yxh").toString())){
				criteria.andEqualTo("yxh",parmMap.get("yxh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("inoutbz")!=null && StringUtils.isNotBlank(parmMap.get("inoutbz").toString())){
				criteria.andEqualTo("inoutbz",parmMap.get("inoutbz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gsdw")!=null && StringUtils.isNotBlank(parmMap.get("gsdw").toString())){
				criteria.andEqualTo("gsdw",parmMap.get("gsdw").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cyzk")!=null && StringUtils.isNotBlank(parmMap.get("cyzk").toString())){
				criteria.andEqualTo("cyzk",parmMap.get("cyzk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rybs")!=null && StringUtils.isNotBlank(parmMap.get("rybs").toString())){
				criteria.andEqualTo("rybs",parmMap.get("rybs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("nccbz")!=null && StringUtils.isNotBlank(parmMap.get("nccbz").toString())){
				criteria.andEqualTo("nccbz",parmMap.get("nccbz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("czbs")!=null && StringUtils.isNotBlank(parmMap.get("czbs").toString())){
				criteria.andEqualTo("czbs",parmMap.get("czbs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gxsjc")!=null && StringUtils.isNotBlank(parmMap.get("gxsjc").toString())){
				criteria.andEqualTo("gxsjc",parmMap.get("gxsjc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xgbsdw")!=null && StringUtils.isNotBlank(parmMap.get("xgbsdw").toString())){
				criteria.andEqualTo("xgbsdw",parmMap.get("xgbsdw").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ccbz")!=null && StringUtils.isNotBlank(parmMap.get("ccbz").toString())){
				criteria.andEqualTo("ccbz",parmMap.get("ccbz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gxsj")!=null && StringUtils.isNotBlank(parmMap.get("gxsj").toString())){
				criteria.andEqualTo("gxsj",parmMap.get("gxsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrdw_dwdm")!=null && StringUtils.isNotBlank(parmMap.get("lrdw_dwdm").toString())){
				criteria.andEqualTo("lrdw_dwdm",parmMap.get("lrdw_dwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("hjdz_xzqhdm")!=null && StringUtils.isNotBlank(parmMap.get("hjdz_xzqhdm").toString())){
				criteria.andEqualTo("hjdz_xzqhdm",parmMap.get("hjdz_xzqhdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("hjdz_gajgdm")!=null && StringUtils.isNotBlank(parmMap.get("hjdz_gajgdm").toString())){
				criteria.andEqualTo("hjdz_gajgdm",parmMap.get("hjdz_gajgdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sjjzd_xzqhdm")!=null && StringUtils.isNotBlank(parmMap.get("sjjzd_xzqhdm").toString())){
				criteria.andEqualTo("sjjzd_xzqhdm",parmMap.get("sjjzd_xzqhdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sjjzd_gajgdm")!=null && StringUtils.isNotBlank(parmMap.get("sjjzd_gajgdm").toString())){
				criteria.andEqualTo("sjjzd_gajgdm",parmMap.get("sjjzd_gajgdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xp")!=null && StringUtils.isNotBlank(parmMap.get("xp").toString())){
				criteria.andEqualTo("xp",parmMap.get("xp").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fjxh")!=null && StringUtils.isNotBlank(parmMap.get("fjxh").toString())){
				criteria.andEqualTo("fjxh",parmMap.get("fjxh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xxmc")!=null && StringUtils.isNotBlank(parmMap.get("xxmc").toString())){
				criteria.andEqualTo("xxmc",parmMap.get("xxmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ryxh")!=null && StringUtils.isNotBlank(parmMap.get("ryxh").toString())){
				criteria.andEqualTo("ryxh",parmMap.get("ryxh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xdryly")!=null && StringUtils.isNotBlank(parmMap.get("xdryly").toString())){
				criteria.andEqualTo("xdryly",parmMap.get("xdryly").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ywlx")!=null && StringUtils.isNotBlank(parmMap.get("ywlx").toString())){
				criteria.andEqualTo("ywlx",parmMap.get("ywlx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ywxh")!=null && StringUtils.isNotBlank(parmMap.get("ywxh").toString())){
				criteria.andEqualTo("ywxh",parmMap.get("ywxh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xyrbh")!=null && StringUtils.isNotBlank(parmMap.get("xyrbh").toString())){
				criteria.andEqualTo("xyrbh",parmMap.get("xyrbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xm")!=null && StringUtils.isNotBlank(parmMap.get("xm").toString())){
				criteria.andEqualTo("xm",parmMap.get("xm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xdrydjlydm")!=null && StringUtils.isNotBlank(parmMap.get("xdrydjlydm").toString())){
				criteria.andEqualTo("xdrydjlydm",parmMap.get("xdrydjlydm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cyzjdm")!=null && StringUtils.isNotBlank(parmMap.get("cyzjdm").toString())){
				criteria.andEqualTo("cyzjdm",parmMap.get("cyzjdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfzhm18")!=null && StringUtils.isNotBlank(parmMap.get("sfzhm18").toString())){
				criteria.andEqualTo("sfzhm18",parmMap.get("sfzhm18").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gmsfhm")!=null && StringUtils.isNotBlank(parmMap.get("gmsfhm").toString())){
				criteria.andEqualTo("gmsfhm",parmMap.get("gmsfhm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zjhm")!=null && StringUtils.isNotBlank(parmMap.get("zjhm").toString())){
				criteria.andEqualTo("zjhm",parmMap.get("zjhm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bmch")!=null && StringUtils.isNotBlank(parmMap.get("bmch").toString())){
				criteria.andEqualTo("bmch",parmMap.get("bmch").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xbdm")!=null && StringUtils.isNotBlank(parmMap.get("xbdm").toString())){
				criteria.andEqualTo("xbdm",parmMap.get("xbdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("csrq")!=null && StringUtils.isNotBlank(parmMap.get("csrq").toString())){
				criteria.andEqualTo("csrq",parmMap.get("csrq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("mzdm")!=null && StringUtils.isNotBlank(parmMap.get("mzdm").toString())){
				criteria.andEqualTo("mzdm",parmMap.get("mzdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sg")!=null && StringUtils.isNotBlank(parmMap.get("sg").toString())){
				criteria.andEqualTo("sg",parmMap.get("sg").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jggjdqdm")!=null && StringUtils.isNotBlank(parmMap.get("jggjdqdm").toString())){
				criteria.andEqualTo("jggjdqdm",parmMap.get("jggjdqdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xldm")!=null && StringUtils.isNotBlank(parmMap.get("xldm").toString())){
				criteria.andEqualTo("xldm",parmMap.get("xldm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("grsfdm")!=null && StringUtils.isNotBlank(parmMap.get("grsfdm").toString())){
				criteria.andEqualTo("grsfdm",parmMap.get("grsfdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("hyzkdm")!=null && StringUtils.isNotBlank(parmMap.get("hyzkdm").toString())){
				criteria.andEqualTo("hyzkdm",parmMap.get("hyzkdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jyqk")!=null && StringUtils.isNotBlank(parmMap.get("jyqk").toString())){
				criteria.andEqualTo("jyqk",parmMap.get("jyqk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gzdw_dwmc")!=null && StringUtils.isNotBlank(parmMap.get("gzdw_dwmc").toString())){
				criteria.andEqualTo("gzdw_dwmc",parmMap.get("gzdw_dwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("szzwbh")!=null && StringUtils.isNotBlank(parmMap.get("szzwbh").toString())){
				criteria.andEqualTo("szzwbh",parmMap.get("szzwbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rydnabh")!=null && StringUtils.isNotBlank(parmMap.get("rydnabh").toString())){
				criteria.andEqualTo("rydnabh",parmMap.get("rydnabh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("hjdz_xzqhmc")!=null && StringUtils.isNotBlank(parmMap.get("hjdz_xzqhmc").toString())){
				criteria.andEqualTo("hjdz_xzqhmc",parmMap.get("hjdz_xzqhmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("hjdz_dzmc")!=null && StringUtils.isNotBlank(parmMap.get("hjdz_dzmc").toString())){
				criteria.andEqualTo("hjdz_dzmc",parmMap.get("hjdz_dzmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("hjdz_gajgmc")!=null && StringUtils.isNotBlank(parmMap.get("hjdz_gajgmc").toString())){
				criteria.andEqualTo("hjdz_gajgmc",parmMap.get("hjdz_gajgmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sjjzd_xzqhmc")!=null && StringUtils.isNotBlank(parmMap.get("sjjzd_xzqhmc").toString())){
				criteria.andEqualTo("sjjzd_xzqhmc",parmMap.get("sjjzd_xzqhmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sjjzd_dzmc")!=null && StringUtils.isNotBlank(parmMap.get("sjjzd_dzmc").toString())){
				criteria.andEqualTo("sjjzd_dzmc",parmMap.get("sjjzd_dzmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sjjzd_gajgmc")!=null && StringUtils.isNotBlank(parmMap.get("sjjzd_gajgmc").toString())){
				criteria.andEqualTo("sjjzd_gajgmc",parmMap.get("sjjzd_gajgmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrr_xm")!=null && StringUtils.isNotBlank(parmMap.get("lrr_xm").toString())){
				criteria.andEqualTo("lrr_xm",parmMap.get("lrr_xm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrdw_dwmc")!=null && StringUtils.isNotBlank(parmMap.get("lrdw_dwmc").toString())){
				criteria.andEqualTo("lrdw_dwmc",parmMap.get("lrdw_dwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrsj")!=null && StringUtils.isNotBlank(parmMap.get("lrsj").toString())){
				criteria.andEqualTo("lrsj",parmMap.get("lrsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sf828k")!=null && StringUtils.isNotBlank(parmMap.get("sf828k").toString())){
				criteria.andEqualTo("sf828k",parmMap.get("sf828k").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sdrybh")!=null && StringUtils.isNotBlank(parmMap.get("sdrybh").toString())){
				criteria.andEqualTo("sdrybh",parmMap.get("sdrybh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxh")!=null && StringUtils.isNotBlank(parmMap.get("yxh").toString())){
				criteria.andEqualTo("yxh",parmMap.get("yxh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("inoutbz")!=null && StringUtils.isNotBlank(parmMap.get("inoutbz").toString())){
				criteria.andEqualTo("inoutbz",parmMap.get("inoutbz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gsdw")!=null && StringUtils.isNotBlank(parmMap.get("gsdw").toString())){
				criteria.andEqualTo("gsdw",parmMap.get("gsdw").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cyzk")!=null && StringUtils.isNotBlank(parmMap.get("cyzk").toString())){
				criteria.andEqualTo("cyzk",parmMap.get("cyzk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rybs")!=null && StringUtils.isNotBlank(parmMap.get("rybs").toString())){
				criteria.andEqualTo("rybs",parmMap.get("rybs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("nccbz")!=null && StringUtils.isNotBlank(parmMap.get("nccbz").toString())){
				criteria.andEqualTo("nccbz",parmMap.get("nccbz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("czbs")!=null && StringUtils.isNotBlank(parmMap.get("czbs").toString())){
				criteria.andEqualTo("czbs",parmMap.get("czbs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gxsjc")!=null && StringUtils.isNotBlank(parmMap.get("gxsjc").toString())){
				criteria.andEqualTo("gxsjc",parmMap.get("gxsjc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xgbsdw")!=null && StringUtils.isNotBlank(parmMap.get("xgbsdw").toString())){
				criteria.andEqualTo("xgbsdw",parmMap.get("xgbsdw").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ccbz")!=null && StringUtils.isNotBlank(parmMap.get("ccbz").toString())){
				criteria.andEqualTo("ccbz",parmMap.get("ccbz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gxsj")!=null && StringUtils.isNotBlank(parmMap.get("gxsj").toString())){
				criteria.andEqualTo("gxsj",parmMap.get("gxsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrdw_dwdm")!=null && StringUtils.isNotBlank(parmMap.get("lrdw_dwdm").toString())){
				criteria.andEqualTo("lrdw_dwdm",parmMap.get("lrdw_dwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("hjdz_xzqhdm")!=null && StringUtils.isNotBlank(parmMap.get("hjdz_xzqhdm").toString())){
				criteria.andEqualTo("hjdz_xzqhdm",parmMap.get("hjdz_xzqhdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("hjdz_gajgdm")!=null && StringUtils.isNotBlank(parmMap.get("hjdz_gajgdm").toString())){
				criteria.andEqualTo("hjdz_gajgdm",parmMap.get("hjdz_gajgdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sjjzd_xzqhdm")!=null && StringUtils.isNotBlank(parmMap.get("sjjzd_xzqhdm").toString())){
				criteria.andEqualTo("sjjzd_xzqhdm",parmMap.get("sjjzd_xzqhdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sjjzd_gajgdm")!=null && StringUtils.isNotBlank(parmMap.get("sjjzd_gajgdm").toString())){
				criteria.andEqualTo("sjjzd_gajgdm",parmMap.get("sjjzd_gajgdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xp")!=null && StringUtils.isNotBlank(parmMap.get("xp").toString())){
				criteria.andEqualTo("xp",parmMap.get("xp").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fjxh")!=null && StringUtils.isNotBlank(parmMap.get("fjxh").toString())){
				criteria.andEqualTo("fjxh",parmMap.get("fjxh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xxmc")!=null && StringUtils.isNotBlank(parmMap.get("xxmc").toString())){
				criteria.andEqualTo("xxmc",parmMap.get("xxmc").toString().trim());
				flag = true;
			}
			if(parmMap.get("ly")!=null && StringUtils.isNotBlank(parmMap.get("ly").toString())){
				criteria.andEqualTo("ly",parmMap.get("ly").toString().trim());
				flag = true;
			}
			if(parmMap.get("isfx")!=null && StringUtils.isNotBlank(parmMap.get("isfx").toString())){
				criteria.andEqualTo("isfx",parmMap.get("isfx").toString().trim());
				flag = true;
			}
			if(parmMap.get("zhpk")!=null && StringUtils.isNotBlank(parmMap.get("zhpk").toString())){
				criteria.andEqualTo("zhpk",parmMap.get("zhpk").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("TbXdryJbxxServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("TbXdryJbxxServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
    
    @Override
	@DS("datasource2")
    public List<String> xdryTask(WSxdryxx xds,String zhpk,Short cj,String zh) throws Exception{
        logger.info("吸毒人员开始插入");
        List<String> xdpk = new ArrayList<String>();
        TbXdryJbxx tbXdryJbxx;
        TbXdryChxx tbXdryChxx;
        String msg = xds.getMsg();
        List<WSTbXdryJbxx> wsTbXdryJbxxList = xds.getWsTbXdryJbxxs();
        List<WSTbXdryChxx> wChxxs = xds.getwChxxs();
        if (msg!=null&&!"".equals(msg)){
            throw new Exception(msg);
        }
        if (wsTbXdryJbxxList!=null||wChxxs!=null){
            if (wsTbXdryJbxxList!=null&&wsTbXdryJbxxList.size()>0){
                logger.info("TbXdryJbxx插入开始");
                for (WSTbXdryJbxx wstbXdryJbxx:wsTbXdryJbxxList) {
                    String bz = wstbXdryJbxx.getCcbz();
                    String xh = wstbXdryJbxx.getRyxh();
					if ("1111111".equals(bz)){
						xdpk.add(wstbXdryJbxx.getRyxh());
					}
                    TbXdryJbxx param = new TbXdryJbxx();
                    param.setRyxh(xh);
                    TbXdryJbxx oldxd = tbXdryJbxxMapper.selectOne(param);
                    if (oldxd!=null){
						tbXdryJbxx = new TbXdryJbxx();
						BeanUtils.copyProperties(wstbXdryJbxx,tbXdryJbxx);
						tbXdryJbxxMapper.updateByPrimaryKeySelective(tbXdryJbxx);
						logger.info("TbXdryJbxx已有数据："+xh);
                    }else {
                        tbXdryJbxx = new TbXdryJbxx();
                        BeanUtils.copyProperties(wstbXdryJbxx,tbXdryJbxx);
                        tbXdryJbxx.setLy((short) 1);
                        if (zhpk!=null&&!"".equals(zhpk)){
                            tbXdryJbxx.setZhpk(zhpk);
                            tbXdryJbxx.setSszh(zh);
                            tbXdryJbxx.setSscj(cj);
                        }
                        tbXdryJbxxMapper.insertSelective(tbXdryJbxx);
                        //插入号段信息
                        TbHdInfo tbHdInfo = new TbHdInfo();
                        String zjhm = tbXdryJbxx.getSfzhm18();
                        if (zjhm!=null&&!"".equals(zjhm)){
                            Example example = new Example(TbHdInfo.class);
                            Example.Criteria criteria = example.createCriteria();
                            criteria.andEqualTo("hd_hm",zjhm);
                            List<TbHdInfo> hds = tbHdInfoService.queryListByExample(example);
                            if (hds.size()>0){
                                continue;
                            }else {
                                logger.info("TbHdInfo插入开始");
                                tbHdInfo.setHd_id(TemplateUtil.genUUID());
                                tbHdInfo.setHd_hm(tbXdryJbxx.getSfzhm18()!=null?tbXdryJbxx.getSfzhm18():"");
                                tbHdInfo.setHd_hz(tbXdryJbxx.getXm() != null ? tbXdryJbxx.getXm() : "");
                                tbHdInfo.setHd_ly(2);
                                tbHdInfo.setHd_lrr_xm(tbXdryJbxx.getLrr_xm() != null ? tbXdryJbxx.getLrr_xm() : "");
                                tbHdInfo.setHd_lrr_dw_mc(tbXdryJbxx.getLrdw_dwmc() != null ? tbXdryJbxx.getLrdw_dwmc() : "");
                                tbHdInfo.setHd_yxx(1);
                                tbHdInfo.setHd_lrsj(new Date());
                                tbHdInfoService.insert(tbHdInfo);
                                logger.info("TbHdInfo插入结束");
                            }
                        }
                    }
                }
                logger.info("TbXdryJbxx插入结束");
            }
            if (wChxxs!=null&&wChxxs.size()>0){
                logger.info("TbXdryChxx插入开始");
                for (WSTbXdryChxx wsTbXdryChxx:wChxxs) {
                    String xh = wsTbXdryChxx.getChxh();
                    TbXdryChxx param = new TbXdryChxx();
                    param.setChxh(xh);
                    TbXdryChxx oldxd = tbXdryChxxService.queryAsObject(param);
                    if (oldxd!=null){
						tbXdryChxx = new TbXdryChxx();
						BeanUtils.copyProperties(wsTbXdryChxx,tbXdryChxx);
						tbXdryChxxService.updateByPrimaryKey(tbXdryChxx);
                        logger.info("TbXdryChxx已有数据："+xh);
                        continue;
                    }else {
                        tbXdryChxx = new TbXdryChxx();
                        BeanUtils.copyProperties(wsTbXdryChxx,tbXdryChxx);
                        tbXdryChxx.setLy(1);
                        tbXdryChxxService.insert(tbXdryChxx);
                    }
                }
                logger.info("TbXdryChxx插入结束");
            }
        }else{
            logger.info("吸毒人员结果为空");
        }
        logger.info("吸毒人员插入结束");
        return xdpk;
    }
}
