package com.unis.service.jdzh.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.unis.common.util.TemplateUtil;
import com.unis.dto.ResultDto;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.hd.TbHdInfoService;
import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.jdzh.TbDpajSdryJbxxMapper;
import com.unis.model.hd.TbHdInfo;
import com.unis.model.jdzh.TbDpajJbxx;
import com.unis.model.jdzh.TbDpajSdryChxx;
import com.unis.model.jdzh.TbDpajSdryJbxx;
import com.unis.service.jdzh.TbDpajJbxxService;
import com.unis.service.jdzh.TbDpajSdryChxxService;
import com.unis.service.jdzh.TbDpajSdryJbxxService;
import com.unis.wsClient.bean.WSTbDpajJbxx;
import com.unis.wsClient.bean.WSTbDpajSdryChxx;
import com.unis.wsClient.bean.WSTbDpajSdryJbxx;
import com.unis.wsClient.bean.WSsdryxx;
import com.unis.common.config.DS;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see TbDpajSdryJbxxService
 * </pre>
 *
 * @author gongjl
 * @version 1.0
 * @since 2020-03-05
 */
@Service("tbDpajSdryJbxxService")
public class TbDpajSdryJbxxServiceImpl extends BaseServiceImpl implements TbDpajSdryJbxxService {
	private static final Logger logger = LoggerFactory.getLogger(TbDpajSdryJbxxServiceImpl.class);
    @Autowired
    private TbDpajSdryJbxxMapper tbDpajSdryJbxxMapper;
    @Autowired
    private TbHdInfoService tbHdInfoService;
    @Autowired
    private TbDpajSdryChxxService tbDpajSdryChxxService;
    @Autowired
    private TbDpajJbxxService tbDpajJbxxService;

    /**
     * @see TbDpajSdryJbxxService#insert(TbDpajSdryJbxx tbDpajSdryJbxx)
     */
    @Override
	@DS("datasource2")
    public int insert(TbDpajSdryJbxx tbDpajSdryJbxx) throws Exception {
    	if (tbDpajSdryJbxx!=null){
	        tbDpajSdryJbxx.setJlbh(TemplateUtil.genUUID());
	        //menu.setJlbh(getPk("seqName","jgdm","A"));
	                
	        return tbDpajSdryJbxxMapper.insertSelective(tbDpajSdryJbxx);
    	}else{
    		logger.error("TbDpajSdryJbxxServiceImpl.insert时tbDpajSdryJbxx数据为空。");
    		throw new AppRuntimeException("TbDpajSdryJbxxServiceImpl.insert时tbDpajSdryJbxx数据为空。");
    	}        
    }

    /**
     * @see TbDpajSdryJbxxService#delete(String pk)
     */
    @Override
	@DS("datasource2")
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("TbDpajSdryJbxxServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("TbDpajSdryJbxxServiceImpl.delete时pk为空。");
    	}else{
    		return tbDpajSdryJbxxMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see TbDpajSdryJbxxService#updateByPrimaryKey(TbDpajSdryJbxx tbDpajSdryJbxx)
     */
    @Override
	@DS("datasource2")
    public int updateByPrimaryKey(TbDpajSdryJbxx tbDpajSdryJbxx) throws Exception {
        if (tbDpajSdryJbxx!=null){
        	if(StringUtils.isBlank(tbDpajSdryJbxx.getJlbh())){
        		logger.error("TbDpajSdryJbxxServiceImpl.updateByPrimaryKey时tbDpajSdryJbxx.Jlbh为空。");
        		throw new AppRuntimeException("TbDpajSdryJbxxServiceImpl.updateByPrimaryKey时tbDpajSdryJbxx.Jlbh为空。");
        	}
	        return tbDpajSdryJbxxMapper.updateByPrimaryKeySelective(tbDpajSdryJbxx);
    	}else{
    		logger.error("TbDpajSdryJbxxServiceImpl.updateByPrimaryKey时tbDpajSdryJbxx数据为空。");
    		throw new AppRuntimeException("TbDpajSdryJbxxServiceImpl.updateByPrimaryKey时tbDpajSdryJbxx数据为空。");
    	}
    }
    /**
     * @see TbDpajSdryJbxxService#queryTbDpajSdryJbxxByPrimaryKey(String pk)
     */
    @Override
	@DS("datasource2")
    public TbDpajSdryJbxx queryTbDpajSdryJbxxByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("TbDpajSdryJbxxServiceImpl.queryTbDpajSdryJbxxByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("TbDpajSdryJbxxServiceImpl.queryTbDpajSdryJbxxByPrimaryKey时pk为空。");
    	}else{
    		return tbDpajSdryJbxxMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see TbDpajSdryJbxxService#queryAsObject(TbDpajSdryJbxx tbDpajSdryJbxx)
     */
    @Override
	@DS("datasource2")
    public TbDpajSdryJbxx queryAsObject(TbDpajSdryJbxx tbDpajSdryJbxx) throws Exception {
        if (tbDpajSdryJbxx!=null){
	        return tbDpajSdryJbxxMapper.selectOne(tbDpajSdryJbxx);
    	}else{
    		logger.error("TbDpajSdryJbxxServiceImpl.queryAsObject时tbDpajSdryJbxx数据为空。");
    		throw new AppRuntimeException("TbDpajSdryJbxxServiceImpl.queryAsObject时tbDpajSdryJbxx数据为空。");
    	}
    }
    
    /**
     * @see TbDpajSdryJbxxService#queryCountByExample(Example example)
     */
    @Override
	@DS("datasource2")
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return tbDpajSdryJbxxMapper.selectCountByExample(example);
    	}else{
    		logger.error("TbDpajSdryJbxxServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("TbDpajSdryJbxxServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see TbDpajSdryJbxxService#queryListByExample(Example example)
     */
    @Override
    @DS("datasource2")
    public List<TbDpajSdryJbxx> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return tbDpajSdryJbxxMapper.selectByExample(example);
    	}else{
    		logger.error("TbDpajSdryJbxxServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("TbDpajSdryJbxxServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see TbDpajSdryJbxxService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
	@DS("datasource2")
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(tbDpajSdryJbxxMapper.selectByExample(example));
    	}else{
    		logger.error("TbDpajSdryJbxxServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("TbDpajSdryJbxxServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see TbDpajSdryJbxxService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
	@DS("datasource2")
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(TbDpajSdryJbxx.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("jlbh")!=null && StringUtils.isNotBlank(parmMap.get("jlbh").toString())){
				criteria.andEqualTo("jlbh",parmMap.get("jlbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xyrbh")!=null && StringUtils.isNotBlank(parmMap.get("xyrbh").toString())){
				criteria.andEqualTo("xyrbh",parmMap.get("xyrbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("tbdw_dwmc")!=null && StringUtils.isNotBlank(parmMap.get("tbdw_dwmc").toString())){
				criteria.andEqualTo("tbdw_dwmc",parmMap.get("tbdw_dwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("djr_xm")!=null && StringUtils.isNotBlank(parmMap.get("djr_xm").toString())){
				criteria.andEqualTo("djr_xm",parmMap.get("djr_xm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xm")!=null && StringUtils.isNotBlank(parmMap.get("xm").toString())){
				criteria.andEqualTo("xm",parmMap.get("xm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cym")!=null && StringUtils.isNotBlank(parmMap.get("cym").toString())){
				criteria.andEqualTo("cym",parmMap.get("cym").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cyzjdm")!=null && StringUtils.isNotBlank(parmMap.get("cyzjdm").toString())){
				criteria.andEqualTo("cyzjdm",parmMap.get("cyzjdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zjhm")!=null && StringUtils.isNotBlank(parmMap.get("zjhm").toString())){
				criteria.andEqualTo("zjhm",parmMap.get("zjhm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qtzjhm")!=null && StringUtils.isNotBlank(parmMap.get("qtzjhm").toString())){
				criteria.andEqualTo("qtzjhm",parmMap.get("qtzjhm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qq")!=null && StringUtils.isNotBlank(parmMap.get("qq").toString())){
				criteria.andEqualTo("qq",parmMap.get("qq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("hjd_xzqh")!=null && StringUtils.isNotBlank(parmMap.get("hjd_xzqh").toString())){
				criteria.andEqualTo("hjd_xzqh",parmMap.get("hjd_xzqh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("hjdxz")!=null && StringUtils.isNotBlank(parmMap.get("hjdxz").toString())){
				criteria.andEqualTo("hjdxz",parmMap.get("hjdxz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("hjd_dwdm")!=null && StringUtils.isNotBlank(parmMap.get("hjd_dwdm").toString())){
				criteria.andEqualTo("hjd_dwdm",parmMap.get("hjd_dwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("hjd_dwmc")!=null && StringUtils.isNotBlank(parmMap.get("hjd_dwmc").toString())){
				criteria.andEqualTo("hjd_dwmc",parmMap.get("hjd_dwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sjjzd_xzqh")!=null && StringUtils.isNotBlank(parmMap.get("sjjzd_xzqh").toString())){
				criteria.andEqualTo("sjjzd_xzqh",parmMap.get("sjjzd_xzqh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sjjzdxz")!=null && StringUtils.isNotBlank(parmMap.get("sjjzdxz").toString())){
				criteria.andEqualTo("sjjzdxz",parmMap.get("sjjzdxz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sjjzd_dwmc")!=null && StringUtils.isNotBlank(parmMap.get("sjjzd_dwmc").toString())){
				criteria.andEqualTo("sjjzd_dwmc",parmMap.get("sjjzd_dwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sjjzd_dwdm")!=null && StringUtils.isNotBlank(parmMap.get("sjjzd_dwdm").toString())){
				criteria.andEqualTo("sjjzd_dwdm",parmMap.get("sjjzd_dwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qtdz_xzqh")!=null && StringUtils.isNotBlank(parmMap.get("qtdz_xzqh").toString())){
				criteria.andEqualTo("qtdz_xzqh",parmMap.get("qtdz_xzqh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qtzzxz")!=null && StringUtils.isNotBlank(parmMap.get("qtzzxz").toString())){
				criteria.andEqualTo("qtzzxz",parmMap.get("qtzzxz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bmch")!=null && StringUtils.isNotBlank(parmMap.get("bmch").toString())){
				criteria.andEqualTo("bmch",parmMap.get("bmch").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xbdm")!=null && StringUtils.isNotBlank(parmMap.get("xbdm").toString())){
				criteria.andEqualTo("xbdm",parmMap.get("xbdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("csrq")!=null && StringUtils.isNotBlank(parmMap.get("csrq").toString())){
				criteria.andEqualTo("csrq",parmMap.get("csrq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("csdssxdm")!=null && StringUtils.isNotBlank(parmMap.get("csdssxdm").toString())){
				criteria.andEqualTo("csdssxdm",parmMap.get("csdssxdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("grsfdm")!=null && StringUtils.isNotBlank(parmMap.get("grsfdm").toString())){
				criteria.andEqualTo("grsfdm",parmMap.get("grsfdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xldm")!=null && StringUtils.isNotBlank(parmMap.get("xldm").toString())){
				criteria.andEqualTo("xldm",parmMap.get("xldm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("mzdm")!=null && StringUtils.isNotBlank(parmMap.get("mzdm").toString())){
				criteria.andEqualTo("mzdm",parmMap.get("mzdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fwcs")!=null && StringUtils.isNotBlank(parmMap.get("fwcs").toString())){
				criteria.andEqualTo("fwcs",parmMap.get("fwcs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sg")!=null && StringUtils.isNotBlank(parmMap.get("sg").toString())){
				criteria.andEqualTo("sg",parmMap.get("sg").toString().trim());
				flag = true;
			}
    		if(parmMap.get("tmtzms")!=null && StringUtils.isNotBlank(parmMap.get("tmtzms").toString())){
				criteria.andEqualTo("tmtzms",parmMap.get("tmtzms").toString().trim());
				flag = true;
			}
    		if(parmMap.get("br_lxdh")!=null && StringUtils.isNotBlank(parmMap.get("br_lxdh").toString())){
				criteria.andEqualTo("br_lxdh",parmMap.get("br_lxdh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lxrxx")!=null && StringUtils.isNotBlank(parmMap.get("lxrxx").toString())){
				criteria.andEqualTo("lxrxx",parmMap.get("lxrxx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gjdm")!=null && StringUtils.isNotBlank(parmMap.get("gjdm").toString())){
				criteria.andEqualTo("gjdm",parmMap.get("gjdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("hyzkdm")!=null && StringUtils.isNotBlank(parmMap.get("hyzkdm").toString())){
				criteria.andEqualTo("hyzkdm",parmMap.get("hyzkdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ryxz")!=null && StringUtils.isNotBlank(parmMap.get("ryxz").toString())){
				criteria.andEqualTo("ryxz",parmMap.get("ryxz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xzdjrq")!=null && StringUtils.isNotBlank(parmMap.get("xzdjrq").toString())){
				criteria.andEqualTo("xzdjrq",parmMap.get("xzdjrq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ccxdrq")!=null && StringUtils.isNotBlank(parmMap.get("ccxdrq").toString())){
				criteria.andEqualTo("ccxdrq",parmMap.get("ccxdrq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("dpfzxyrlx")!=null && StringUtils.isNotBlank(parmMap.get("dpfzxyrlx").toString())){
				criteria.andEqualTo("dpfzxyrlx",parmMap.get("dpfzxyrlx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bz")!=null && StringUtils.isNotBlank(parmMap.get("bz").toString())){
				criteria.andEqualTo("bz",parmMap.get("bz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("djrq")!=null && StringUtils.isNotBlank(parmMap.get("djrq").toString())){
				criteria.andEqualTo("djrq",parmMap.get("djrq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sdrylx")!=null && StringUtils.isNotBlank(parmMap.get("sdrylx").toString())){
				criteria.andEqualTo("sdrylx",parmMap.get("sdrylx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cxdw_dwmc")!=null && StringUtils.isNotBlank(parmMap.get("cxdw_dwmc").toString())){
				criteria.andEqualTo("cxdw_dwmc",parmMap.get("cxdw_dwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xmhypy")!=null && StringUtils.isNotBlank(parmMap.get("xmhypy").toString())){
				criteria.andEqualTo("xmhypy",parmMap.get("xmhypy").toString().trim());
				flag = true;
			}
    		if(parmMap.get("tbdq_xzqh")!=null && StringUtils.isNotBlank(parmMap.get("tbdq_xzqh").toString())){
				criteria.andEqualTo("tbdq_xzqh",parmMap.get("tbdq_xzqh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ztrylxdm")!=null && StringUtils.isNotBlank(parmMap.get("ztrylxdm").toString())){
				criteria.andEqualTo("ztrylxdm",parmMap.get("ztrylxdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("wsbh")!=null && StringUtils.isNotBlank(parmMap.get("wsbh").toString())){
				criteria.andEqualTo("wsbh",parmMap.get("wsbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yzdxxxyrlrly")!=null && StringUtils.isNotBlank(parmMap.get("yzdxxxyrlrly").toString())){
				criteria.andEqualTo("yzdxxxyrlrly",parmMap.get("yzdxxxyrlrly").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yqtxqx")!=null && StringUtils.isNotBlank(parmMap.get("yqtxqx").toString())){
				criteria.andEqualTo("yqtxqx",parmMap.get("yqtxqx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qtzt")!=null && StringUtils.isNotBlank(parmMap.get("qtzt").toString())){
				criteria.andEqualTo("qtzt",parmMap.get("qtzt").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rylb")!=null && StringUtils.isNotBlank(parmMap.get("rylb").toString())){
				criteria.andEqualTo("rylb",parmMap.get("rylb").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ryjsdm")!=null && StringUtils.isNotBlank(parmMap.get("ryjsdm").toString())){
				criteria.andEqualTo("ryjsdm",parmMap.get("ryjsdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qtjsdm")!=null && StringUtils.isNotBlank(parmMap.get("qtjsdm").toString())){
				criteria.andEqualTo("qtjsdm",parmMap.get("qtjsdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("nl")!=null && StringUtils.isNotBlank(parmMap.get("nl").toString())){
				criteria.andEqualTo("nl",parmMap.get("nl").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jgssxdm")!=null && StringUtils.isNotBlank(parmMap.get("jgssxdm").toString())){
				criteria.andEqualTo("jgssxdm",parmMap.get("jgssxdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jl")!=null && StringUtils.isNotBlank(parmMap.get("jl").toString())){
				criteria.andEqualTo("jl",parmMap.get("jl").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jwdz")!=null && StringUtils.isNotBlank(parmMap.get("jwdz").toString())){
				criteria.andEqualTo("jwdz",parmMap.get("jwdz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("txdz")!=null && StringUtils.isNotBlank(parmMap.get("txdz").toString())){
				criteria.andEqualTo("txdz",parmMap.get("txdz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("wwm")!=null && StringUtils.isNotBlank(parmMap.get("wwm").toString())){
				criteria.andEqualTo("wwm",parmMap.get("wwm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("wwx")!=null && StringUtils.isNotBlank(parmMap.get("wwx").toString())){
				criteria.andEqualTo("wwx",parmMap.get("wwx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xxdm")!=null && StringUtils.isNotBlank(parmMap.get("xxdm").toString())){
				criteria.andEqualTo("xxdm",parmMap.get("xxdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zjxydm")!=null && StringUtils.isNotBlank(parmMap.get("zjxydm").toString())){
				criteria.andEqualTo("zjxydm",parmMap.get("zjxydm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zy")!=null && StringUtils.isNotBlank(parmMap.get("zy").toString())){
				criteria.andEqualTo("zy",parmMap.get("zy").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zylbdm")!=null && StringUtils.isNotBlank(parmMap.get("zylbdm").toString())){
				criteria.andEqualTo("zylbdm",parmMap.get("zylbdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zzmmdm")!=null && StringUtils.isNotBlank(parmMap.get("zzmmdm").toString())){
				criteria.andEqualTo("zzmmdm",parmMap.get("zzmmdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("hykydm")!=null && StringUtils.isNotBlank(parmMap.get("hykydm").toString())){
				criteria.andEqualTo("hykydm",parmMap.get("hykydm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfsw")!=null && StringUtils.isNotBlank(parmMap.get("sfsw").toString())){
				criteria.andEqualTo("sfsw",parmMap.get("sfsw").toString().trim());
				flag = true;
			}
    		if(parmMap.get("swsj")!=null && StringUtils.isNotBlank(parmMap.get("swsj").toString())){
				criteria.andEqualTo("swsj",parmMap.get("swsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xdjlhcjgdm")!=null && StringUtils.isNotBlank(parmMap.get("xdjlhcjgdm").toString())){
				criteria.andEqualTo("xdjlhcjgdm",parmMap.get("xdjlhcjgdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yz")!=null && StringUtils.isNotBlank(parmMap.get("yz").toString())){
				criteria.andEqualTo("yz",parmMap.get("yz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("saryzcdm")!=null && StringUtils.isNotBlank(parmMap.get("saryzcdm").toString())){
				criteria.andEqualTo("saryzcdm",parmMap.get("saryzcdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fzzlx")!=null && StringUtils.isNotBlank(parmMap.get("fzzlx").toString())){
				criteria.andEqualTo("fzzlx",parmMap.get("fzzlx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qyfwdm")!=null && StringUtils.isNotBlank(parmMap.get("qyfwdm").toString())){
				criteria.andEqualTo("qyfwdm",parmMap.get("qyfwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bkzt")!=null && StringUtils.isNotBlank(parmMap.get("bkzt").toString())){
				criteria.andEqualTo("bkzt",parmMap.get("bkzt").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fzztlxdm")!=null && StringUtils.isNotBlank(parmMap.get("fzztlxdm").toString())){
				criteria.andEqualTo("fzztlxdm",parmMap.get("fzztlxdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("tbdw_dwdm")!=null && StringUtils.isNotBlank(parmMap.get("tbdw_dwdm").toString())){
				criteria.andEqualTo("tbdw_dwdm",parmMap.get("tbdw_dwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("tbdq")!=null && StringUtils.isNotBlank(parmMap.get("tbdq").toString())){
				criteria.andEqualTo("tbdq",parmMap.get("tbdq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jkzk")!=null && StringUtils.isNotBlank(parmMap.get("jkzk").toString())){
				criteria.andEqualTo("jkzk",parmMap.get("jkzk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("czbz")!=null && StringUtils.isNotBlank(parmMap.get("czbz").toString())){
				criteria.andEqualTo("czbz",parmMap.get("czbz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("wlsfxx")!=null && StringUtils.isNotBlank(parmMap.get("wlsfxx").toString())){
				criteria.andEqualTo("wlsfxx",parmMap.get("wlsfxx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gjsj")!=null && StringUtils.isNotBlank(parmMap.get("gjsj").toString())){
				criteria.andEqualTo("gjsj",parmMap.get("gjsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gxsj")!=null && StringUtils.isNotBlank(parmMap.get("gxsj").toString())){
				criteria.andEqualTo("gxsj",parmMap.get("gxsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gxsjc")!=null && StringUtils.isNotBlank(parmMap.get("gxsjc").toString())){
				criteria.andEqualTo("gxsjc",parmMap.get("gxsjc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xgbsdw")!=null && StringUtils.isNotBlank(parmMap.get("xgbsdw").toString())){
				criteria.andEqualTo("xgbsdw",parmMap.get("xgbsdw").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sjch")!=null && StringUtils.isNotBlank(parmMap.get("sjch").toString())){
				criteria.andEqualTo("sjch",parmMap.get("sjch").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sjxh")!=null && StringUtils.isNotBlank(parmMap.get("sjxh").toString())){
				criteria.andEqualTo("sjxh",parmMap.get("sjxh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gh")!=null && StringUtils.isNotBlank(parmMap.get("gh").toString())){
				criteria.andEqualTo("gh",parmMap.get("gh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("email")!=null && StringUtils.isNotBlank(parmMap.get("email").toString())){
				criteria.andEqualTo("email",parmMap.get("email").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xnsf")!=null && StringUtils.isNotBlank(parmMap.get("xnsf").toString())){
				criteria.andEqualTo("xnsf",parmMap.get("xnsf").toString().trim());
				flag = true;
			}
    		if(parmMap.get("wx")!=null && StringUtils.isNotBlank(parmMap.get("wx").toString())){
				criteria.andEqualTo("wx",parmMap.get("wx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ry_dwmc")!=null && StringUtils.isNotBlank(parmMap.get("ry_dwmc").toString())){
				criteria.andEqualTo("ry_dwmc",parmMap.get("ry_dwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("tsbj")!=null && StringUtils.isNotBlank(parmMap.get("tsbj").toString())){
				criteria.andEqualTo("tsbj",parmMap.get("tsbj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sczt")!=null && StringUtils.isNotBlank(parmMap.get("sczt").toString())){
				criteria.andEqualTo("sczt",parmMap.get("sczt").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sjhgsd")!=null && StringUtils.isNotBlank(parmMap.get("sjhgsd").toString())){
				criteria.andEqualTo("sjhgsd",parmMap.get("sjhgsd").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ryzp")!=null && StringUtils.isNotBlank(parmMap.get("ryzp").toString())){
				criteria.andEqualTo("ryzp",parmMap.get("ryzp").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fzlx")!=null && StringUtils.isNotBlank(parmMap.get("fzlx").toString())){
				criteria.andEqualTo("fzlx",parmMap.get("fzlx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ryly")!=null && StringUtils.isNotBlank(parmMap.get("ryly").toString())){
				criteria.andEqualTo("ryly",parmMap.get("ryly").toString().trim());
				flag = true;
			}
    		if(parmMap.get("czbs")!=null && StringUtils.isNotBlank(parmMap.get("czbs").toString())){
				criteria.andEqualTo("czbs",parmMap.get("czbs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cszt")!=null && StringUtils.isNotBlank(parmMap.get("cszt").toString())){
				criteria.andEqualTo("cszt",parmMap.get("cszt").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yqtxqxbf")!=null && StringUtils.isNotBlank(parmMap.get("yqtxqxbf").toString())){
				criteria.andEqualTo("yqtxqxbf",parmMap.get("yqtxqxbf").toString().trim());
				flag = true;
			}
    		if(parmMap.get("csdssxdmbf")!=null && StringUtils.isNotBlank(parmMap.get("csdssxdmbf").toString())){
				criteria.andEqualTo("csdssxdmbf",parmMap.get("csdssxdmbf").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jgssxdmbf")!=null && StringUtils.isNotBlank(parmMap.get("jgssxdmbf").toString())){
				criteria.andEqualTo("jgssxdmbf",parmMap.get("jgssxdmbf").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sgbf")!=null && StringUtils.isNotBlank(parmMap.get("sgbf").toString())){
				criteria.andEqualTo("sgbf",parmMap.get("sgbf").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bhsbh")!=null && StringUtils.isNotBlank(parmMap.get("bhsbh").toString())){
				criteria.andEqualTo("bhsbh",parmMap.get("bhsbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xp")!=null && StringUtils.isNotBlank(parmMap.get("xp").toString())){
				criteria.andEqualTo("xp",parmMap.get("xp").toString().trim());
				flag = true;
			}
    		if(parmMap.get("n_yxh")!=null && StringUtils.isNotBlank(parmMap.get("n_yxh").toString())){
				criteria.andEqualTo("n_yxh",parmMap.get("n_yxh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lxrlxfs")!=null && StringUtils.isNotBlank(parmMap.get("lxrlxfs").toString())){
				criteria.andEqualTo("lxrlxfs",parmMap.get("lxrlxfs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lxrgx")!=null && StringUtils.isNotBlank(parmMap.get("lxrgx").toString())){
				criteria.andEqualTo("lxrgx",parmMap.get("lxrgx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sjly")!=null && StringUtils.isNotBlank(parmMap.get("sjly").toString())){
				criteria.andEqualTo("sjly",parmMap.get("sjly").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jlbh")!=null && StringUtils.isNotBlank(parmMap.get("jlbh").toString())){
				criteria.andEqualTo("jlbh",parmMap.get("jlbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xyrbh")!=null && StringUtils.isNotBlank(parmMap.get("xyrbh").toString())){
				criteria.andEqualTo("xyrbh",parmMap.get("xyrbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("tbdw_dwmc")!=null && StringUtils.isNotBlank(parmMap.get("tbdw_dwmc").toString())){
				criteria.andEqualTo("tbdw_dwmc",parmMap.get("tbdw_dwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("djr_xm")!=null && StringUtils.isNotBlank(parmMap.get("djr_xm").toString())){
				criteria.andEqualTo("djr_xm",parmMap.get("djr_xm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xm")!=null && StringUtils.isNotBlank(parmMap.get("xm").toString())){
				criteria.andEqualTo("xm",parmMap.get("xm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cym")!=null && StringUtils.isNotBlank(parmMap.get("cym").toString())){
				criteria.andEqualTo("cym",parmMap.get("cym").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cyzjdm")!=null && StringUtils.isNotBlank(parmMap.get("cyzjdm").toString())){
				criteria.andEqualTo("cyzjdm",parmMap.get("cyzjdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zjhm")!=null && StringUtils.isNotBlank(parmMap.get("zjhm").toString())){
				criteria.andEqualTo("zjhm",parmMap.get("zjhm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qtzjhm")!=null && StringUtils.isNotBlank(parmMap.get("qtzjhm").toString())){
				criteria.andEqualTo("qtzjhm",parmMap.get("qtzjhm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qq")!=null && StringUtils.isNotBlank(parmMap.get("qq").toString())){
				criteria.andEqualTo("qq",parmMap.get("qq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("hjd_xzqh")!=null && StringUtils.isNotBlank(parmMap.get("hjd_xzqh").toString())){
				criteria.andEqualTo("hjd_xzqh",parmMap.get("hjd_xzqh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("hjdxz")!=null && StringUtils.isNotBlank(parmMap.get("hjdxz").toString())){
				criteria.andEqualTo("hjdxz",parmMap.get("hjdxz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("hjd_dwdm")!=null && StringUtils.isNotBlank(parmMap.get("hjd_dwdm").toString())){
				criteria.andEqualTo("hjd_dwdm",parmMap.get("hjd_dwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("hjd_dwmc")!=null && StringUtils.isNotBlank(parmMap.get("hjd_dwmc").toString())){
				criteria.andEqualTo("hjd_dwmc",parmMap.get("hjd_dwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sjjzd_xzqh")!=null && StringUtils.isNotBlank(parmMap.get("sjjzd_xzqh").toString())){
				criteria.andEqualTo("sjjzd_xzqh",parmMap.get("sjjzd_xzqh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sjjzdxz")!=null && StringUtils.isNotBlank(parmMap.get("sjjzdxz").toString())){
				criteria.andEqualTo("sjjzdxz",parmMap.get("sjjzdxz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sjjzd_dwmc")!=null && StringUtils.isNotBlank(parmMap.get("sjjzd_dwmc").toString())){
				criteria.andEqualTo("sjjzd_dwmc",parmMap.get("sjjzd_dwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sjjzd_dwdm")!=null && StringUtils.isNotBlank(parmMap.get("sjjzd_dwdm").toString())){
				criteria.andEqualTo("sjjzd_dwdm",parmMap.get("sjjzd_dwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qtdz_xzqh")!=null && StringUtils.isNotBlank(parmMap.get("qtdz_xzqh").toString())){
				criteria.andEqualTo("qtdz_xzqh",parmMap.get("qtdz_xzqh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qtzzxz")!=null && StringUtils.isNotBlank(parmMap.get("qtzzxz").toString())){
				criteria.andEqualTo("qtzzxz",parmMap.get("qtzzxz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bmch")!=null && StringUtils.isNotBlank(parmMap.get("bmch").toString())){
				criteria.andEqualTo("bmch",parmMap.get("bmch").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xbdm")!=null && StringUtils.isNotBlank(parmMap.get("xbdm").toString())){
				criteria.andEqualTo("xbdm",parmMap.get("xbdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("csrq")!=null && StringUtils.isNotBlank(parmMap.get("csrq").toString())){
				criteria.andEqualTo("csrq",parmMap.get("csrq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("csdssxdm")!=null && StringUtils.isNotBlank(parmMap.get("csdssxdm").toString())){
				criteria.andEqualTo("csdssxdm",parmMap.get("csdssxdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("grsfdm")!=null && StringUtils.isNotBlank(parmMap.get("grsfdm").toString())){
				criteria.andEqualTo("grsfdm",parmMap.get("grsfdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xldm")!=null && StringUtils.isNotBlank(parmMap.get("xldm").toString())){
				criteria.andEqualTo("xldm",parmMap.get("xldm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("mzdm")!=null && StringUtils.isNotBlank(parmMap.get("mzdm").toString())){
				criteria.andEqualTo("mzdm",parmMap.get("mzdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fwcs")!=null && StringUtils.isNotBlank(parmMap.get("fwcs").toString())){
				criteria.andEqualTo("fwcs",parmMap.get("fwcs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sg")!=null && StringUtils.isNotBlank(parmMap.get("sg").toString())){
				criteria.andEqualTo("sg",parmMap.get("sg").toString().trim());
				flag = true;
			}
    		if(parmMap.get("tmtzms")!=null && StringUtils.isNotBlank(parmMap.get("tmtzms").toString())){
				criteria.andEqualTo("tmtzms",parmMap.get("tmtzms").toString().trim());
				flag = true;
			}
    		if(parmMap.get("br_lxdh")!=null && StringUtils.isNotBlank(parmMap.get("br_lxdh").toString())){
				criteria.andEqualTo("br_lxdh",parmMap.get("br_lxdh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lxrxx")!=null && StringUtils.isNotBlank(parmMap.get("lxrxx").toString())){
				criteria.andEqualTo("lxrxx",parmMap.get("lxrxx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gjdm")!=null && StringUtils.isNotBlank(parmMap.get("gjdm").toString())){
				criteria.andEqualTo("gjdm",parmMap.get("gjdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("hyzkdm")!=null && StringUtils.isNotBlank(parmMap.get("hyzkdm").toString())){
				criteria.andEqualTo("hyzkdm",parmMap.get("hyzkdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ryxz")!=null && StringUtils.isNotBlank(parmMap.get("ryxz").toString())){
				criteria.andEqualTo("ryxz",parmMap.get("ryxz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xzdjrq")!=null && StringUtils.isNotBlank(parmMap.get("xzdjrq").toString())){
				criteria.andEqualTo("xzdjrq",parmMap.get("xzdjrq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ccxdrq")!=null && StringUtils.isNotBlank(parmMap.get("ccxdrq").toString())){
				criteria.andEqualTo("ccxdrq",parmMap.get("ccxdrq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("dpfzxyrlx")!=null && StringUtils.isNotBlank(parmMap.get("dpfzxyrlx").toString())){
				criteria.andEqualTo("dpfzxyrlx",parmMap.get("dpfzxyrlx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bz")!=null && StringUtils.isNotBlank(parmMap.get("bz").toString())){
				criteria.andEqualTo("bz",parmMap.get("bz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("djrq")!=null && StringUtils.isNotBlank(parmMap.get("djrq").toString())){
				criteria.andEqualTo("djrq",parmMap.get("djrq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sdrylx")!=null && StringUtils.isNotBlank(parmMap.get("sdrylx").toString())){
				criteria.andEqualTo("sdrylx",parmMap.get("sdrylx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cxdw_dwmc")!=null && StringUtils.isNotBlank(parmMap.get("cxdw_dwmc").toString())){
				criteria.andEqualTo("cxdw_dwmc",parmMap.get("cxdw_dwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xmhypy")!=null && StringUtils.isNotBlank(parmMap.get("xmhypy").toString())){
				criteria.andEqualTo("xmhypy",parmMap.get("xmhypy").toString().trim());
				flag = true;
			}
    		if(parmMap.get("tbdq_xzqh")!=null && StringUtils.isNotBlank(parmMap.get("tbdq_xzqh").toString())){
				criteria.andEqualTo("tbdq_xzqh",parmMap.get("tbdq_xzqh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ztrylxdm")!=null && StringUtils.isNotBlank(parmMap.get("ztrylxdm").toString())){
				criteria.andEqualTo("ztrylxdm",parmMap.get("ztrylxdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("wsbh")!=null && StringUtils.isNotBlank(parmMap.get("wsbh").toString())){
				criteria.andEqualTo("wsbh",parmMap.get("wsbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yzdxxxyrlrly")!=null && StringUtils.isNotBlank(parmMap.get("yzdxxxyrlrly").toString())){
				criteria.andEqualTo("yzdxxxyrlrly",parmMap.get("yzdxxxyrlrly").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yqtxqx")!=null && StringUtils.isNotBlank(parmMap.get("yqtxqx").toString())){
				criteria.andEqualTo("yqtxqx",parmMap.get("yqtxqx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qtzt")!=null && StringUtils.isNotBlank(parmMap.get("qtzt").toString())){
				criteria.andEqualTo("qtzt",parmMap.get("qtzt").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rylb")!=null && StringUtils.isNotBlank(parmMap.get("rylb").toString())){
				criteria.andEqualTo("rylb",parmMap.get("rylb").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ryjsdm")!=null && StringUtils.isNotBlank(parmMap.get("ryjsdm").toString())){
				criteria.andEqualTo("ryjsdm",parmMap.get("ryjsdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qtjsdm")!=null && StringUtils.isNotBlank(parmMap.get("qtjsdm").toString())){
				criteria.andEqualTo("qtjsdm",parmMap.get("qtjsdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("nl")!=null && StringUtils.isNotBlank(parmMap.get("nl").toString())){
				criteria.andEqualTo("nl",parmMap.get("nl").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jgssxdm")!=null && StringUtils.isNotBlank(parmMap.get("jgssxdm").toString())){
				criteria.andEqualTo("jgssxdm",parmMap.get("jgssxdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jl")!=null && StringUtils.isNotBlank(parmMap.get("jl").toString())){
				criteria.andEqualTo("jl",parmMap.get("jl").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jwdz")!=null && StringUtils.isNotBlank(parmMap.get("jwdz").toString())){
				criteria.andEqualTo("jwdz",parmMap.get("jwdz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("txdz")!=null && StringUtils.isNotBlank(parmMap.get("txdz").toString())){
				criteria.andEqualTo("txdz",parmMap.get("txdz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("wwm")!=null && StringUtils.isNotBlank(parmMap.get("wwm").toString())){
				criteria.andEqualTo("wwm",parmMap.get("wwm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("wwx")!=null && StringUtils.isNotBlank(parmMap.get("wwx").toString())){
				criteria.andEqualTo("wwx",parmMap.get("wwx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xxdm")!=null && StringUtils.isNotBlank(parmMap.get("xxdm").toString())){
				criteria.andEqualTo("xxdm",parmMap.get("xxdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zjxydm")!=null && StringUtils.isNotBlank(parmMap.get("zjxydm").toString())){
				criteria.andEqualTo("zjxydm",parmMap.get("zjxydm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zy")!=null && StringUtils.isNotBlank(parmMap.get("zy").toString())){
				criteria.andEqualTo("zy",parmMap.get("zy").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zylbdm")!=null && StringUtils.isNotBlank(parmMap.get("zylbdm").toString())){
				criteria.andEqualTo("zylbdm",parmMap.get("zylbdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zzmmdm")!=null && StringUtils.isNotBlank(parmMap.get("zzmmdm").toString())){
				criteria.andEqualTo("zzmmdm",parmMap.get("zzmmdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("hykydm")!=null && StringUtils.isNotBlank(parmMap.get("hykydm").toString())){
				criteria.andEqualTo("hykydm",parmMap.get("hykydm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfsw")!=null && StringUtils.isNotBlank(parmMap.get("sfsw").toString())){
				criteria.andEqualTo("sfsw",parmMap.get("sfsw").toString().trim());
				flag = true;
			}
    		if(parmMap.get("swsj")!=null && StringUtils.isNotBlank(parmMap.get("swsj").toString())){
				criteria.andEqualTo("swsj",parmMap.get("swsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xdjlhcjgdm")!=null && StringUtils.isNotBlank(parmMap.get("xdjlhcjgdm").toString())){
				criteria.andEqualTo("xdjlhcjgdm",parmMap.get("xdjlhcjgdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yz")!=null && StringUtils.isNotBlank(parmMap.get("yz").toString())){
				criteria.andEqualTo("yz",parmMap.get("yz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("saryzcdm")!=null && StringUtils.isNotBlank(parmMap.get("saryzcdm").toString())){
				criteria.andEqualTo("saryzcdm",parmMap.get("saryzcdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fzzlx")!=null && StringUtils.isNotBlank(parmMap.get("fzzlx").toString())){
				criteria.andEqualTo("fzzlx",parmMap.get("fzzlx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qyfwdm")!=null && StringUtils.isNotBlank(parmMap.get("qyfwdm").toString())){
				criteria.andEqualTo("qyfwdm",parmMap.get("qyfwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bkzt")!=null && StringUtils.isNotBlank(parmMap.get("bkzt").toString())){
				criteria.andEqualTo("bkzt",parmMap.get("bkzt").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fzztlxdm")!=null && StringUtils.isNotBlank(parmMap.get("fzztlxdm").toString())){
				criteria.andEqualTo("fzztlxdm",parmMap.get("fzztlxdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("tbdw_dwdm")!=null && StringUtils.isNotBlank(parmMap.get("tbdw_dwdm").toString())){
				criteria.andEqualTo("tbdw_dwdm",parmMap.get("tbdw_dwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("tbdq")!=null && StringUtils.isNotBlank(parmMap.get("tbdq").toString())){
				criteria.andEqualTo("tbdq",parmMap.get("tbdq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jkzk")!=null && StringUtils.isNotBlank(parmMap.get("jkzk").toString())){
				criteria.andEqualTo("jkzk",parmMap.get("jkzk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("czbz")!=null && StringUtils.isNotBlank(parmMap.get("czbz").toString())){
				criteria.andEqualTo("czbz",parmMap.get("czbz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("wlsfxx")!=null && StringUtils.isNotBlank(parmMap.get("wlsfxx").toString())){
				criteria.andEqualTo("wlsfxx",parmMap.get("wlsfxx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gjsj")!=null && StringUtils.isNotBlank(parmMap.get("gjsj").toString())){
				criteria.andEqualTo("gjsj",parmMap.get("gjsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gxsj")!=null && StringUtils.isNotBlank(parmMap.get("gxsj").toString())){
				criteria.andEqualTo("gxsj",parmMap.get("gxsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gxsjc")!=null && StringUtils.isNotBlank(parmMap.get("gxsjc").toString())){
				criteria.andEqualTo("gxsjc",parmMap.get("gxsjc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xgbsdw")!=null && StringUtils.isNotBlank(parmMap.get("xgbsdw").toString())){
				criteria.andEqualTo("xgbsdw",parmMap.get("xgbsdw").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sjch")!=null && StringUtils.isNotBlank(parmMap.get("sjch").toString())){
				criteria.andEqualTo("sjch",parmMap.get("sjch").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sjxh")!=null && StringUtils.isNotBlank(parmMap.get("sjxh").toString())){
				criteria.andEqualTo("sjxh",parmMap.get("sjxh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gh")!=null && StringUtils.isNotBlank(parmMap.get("gh").toString())){
				criteria.andEqualTo("gh",parmMap.get("gh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("email")!=null && StringUtils.isNotBlank(parmMap.get("email").toString())){
				criteria.andEqualTo("email",parmMap.get("email").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xnsf")!=null && StringUtils.isNotBlank(parmMap.get("xnsf").toString())){
				criteria.andEqualTo("xnsf",parmMap.get("xnsf").toString().trim());
				flag = true;
			}
    		if(parmMap.get("wx")!=null && StringUtils.isNotBlank(parmMap.get("wx").toString())){
				criteria.andEqualTo("wx",parmMap.get("wx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ry_dwmc")!=null && StringUtils.isNotBlank(parmMap.get("ry_dwmc").toString())){
				criteria.andEqualTo("ry_dwmc",parmMap.get("ry_dwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("tsbj")!=null && StringUtils.isNotBlank(parmMap.get("tsbj").toString())){
				criteria.andEqualTo("tsbj",parmMap.get("tsbj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sczt")!=null && StringUtils.isNotBlank(parmMap.get("sczt").toString())){
				criteria.andEqualTo("sczt",parmMap.get("sczt").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sjhgsd")!=null && StringUtils.isNotBlank(parmMap.get("sjhgsd").toString())){
				criteria.andEqualTo("sjhgsd",parmMap.get("sjhgsd").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ryzp")!=null && StringUtils.isNotBlank(parmMap.get("ryzp").toString())){
				criteria.andEqualTo("ryzp",parmMap.get("ryzp").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fzlx")!=null && StringUtils.isNotBlank(parmMap.get("fzlx").toString())){
				criteria.andEqualTo("fzlx",parmMap.get("fzlx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ryly")!=null && StringUtils.isNotBlank(parmMap.get("ryly").toString())){
				criteria.andEqualTo("ryly",parmMap.get("ryly").toString().trim());
				flag = true;
			}
    		if(parmMap.get("czbs")!=null && StringUtils.isNotBlank(parmMap.get("czbs").toString())){
				criteria.andEqualTo("czbs",parmMap.get("czbs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cszt")!=null && StringUtils.isNotBlank(parmMap.get("cszt").toString())){
				criteria.andEqualTo("cszt",parmMap.get("cszt").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yqtxqxbf")!=null && StringUtils.isNotBlank(parmMap.get("yqtxqxbf").toString())){
				criteria.andEqualTo("yqtxqxbf",parmMap.get("yqtxqxbf").toString().trim());
				flag = true;
			}
    		if(parmMap.get("csdssxdmbf")!=null && StringUtils.isNotBlank(parmMap.get("csdssxdmbf").toString())){
				criteria.andEqualTo("csdssxdmbf",parmMap.get("csdssxdmbf").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jgssxdmbf")!=null && StringUtils.isNotBlank(parmMap.get("jgssxdmbf").toString())){
				criteria.andEqualTo("jgssxdmbf",parmMap.get("jgssxdmbf").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sgbf")!=null && StringUtils.isNotBlank(parmMap.get("sgbf").toString())){
				criteria.andEqualTo("sgbf",parmMap.get("sgbf").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bhsbh")!=null && StringUtils.isNotBlank(parmMap.get("bhsbh").toString())){
				criteria.andEqualTo("bhsbh",parmMap.get("bhsbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xp")!=null && StringUtils.isNotBlank(parmMap.get("xp").toString())){
				criteria.andEqualTo("xp",parmMap.get("xp").toString().trim());
				flag = true;
			}
    		if(parmMap.get("n_yxh")!=null && StringUtils.isNotBlank(parmMap.get("n_yxh").toString())){
				criteria.andEqualTo("n_yxh",parmMap.get("n_yxh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lxrlxfs")!=null && StringUtils.isNotBlank(parmMap.get("lxrlxfs").toString())){
				criteria.andEqualTo("lxrlxfs",parmMap.get("lxrlxfs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lxrgx")!=null && StringUtils.isNotBlank(parmMap.get("lxrgx").toString())){
				criteria.andEqualTo("lxrgx",parmMap.get("lxrgx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sjly")!=null && StringUtils.isNotBlank(parmMap.get("sjly").toString())){
				criteria.andEqualTo("sjly",parmMap.get("sjly").toString().trim());
				flag = true;
			}
			if(parmMap.get("ly")!=null && StringUtils.isNotBlank(parmMap.get("ly").toString())){
				criteria.andEqualTo("ly",parmMap.get("ly").toString().trim());
				flag = true;
			}if(parmMap.get("zhpk")!=null && StringUtils.isNotBlank(parmMap.get("zhpk").toString())){
				criteria.andEqualTo("zhpk",parmMap.get("zhpk").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("TbDpajSdryJbxxServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("TbDpajSdryJbxxServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }

	@Override
	@DS("datasource2")
	public ResultDto querySdry(Map parmMap, int pageNum, int pageSize) throws Exception {
		PageHelper.startPage(pageNum,pageSize);
		PageInfo pageInfo = new PageInfo(tbDpajSdryJbxxMapper.querySdry(parmMap));
		ResultDto result = new ResultDto();
		result.setRows(pageInfo.getList());
		result.setPage(pageInfo.getPageNum());
		//result.setTotal(pageInfo.getPages());
		result.setTotal((int)pageInfo.getTotal());
		return result;
	}

	@Override
	@DS("datasource2")
	public TbDpajSdryJbxx queryByPk(String pk) throws Exception {
		if(pk!=null&&!"".equals(pk)){
			return tbDpajSdryJbxxMapper.queryByPk(pk);
		}else{
			logger.error("TbDpajSdryJbxxServiceImpl.queryByPk时pk数据为空。");
			throw new AppRuntimeException("TbDpajSdryJbxxServiceImpl.queryByPk时pk数据为空。");
		}
	}
	
 	@Override
	@DS("datasource2")
    public List<String> sdryTask(WSsdryxx sds,String zhpk,Short cj,String zh) throws Exception{
        logger.info("涉毒人员开始插入");
        List<String> sdpk = new ArrayList<String>();
        TbDpajSdryJbxx tbDpajSdryJbxx;
        TbDpajSdryChxx tbDpajSdryChxx;
        TbDpajJbxx tbDpajJbxx;
        String msg = sds.getMsg();
        List<WSTbDpajSdryJbxx> wsTbDpajSdryJbxxes = sds.getwSdryJbxxs();
        List<WSTbDpajSdryChxx> wSdryChxxs = sds.getwSdryChxxs();
        List<WSTbDpajJbxx> wsTbDpajJbxxs = sds.getWsTbDpajJbxxs();

        if (msg!=null&&!"".equals(msg)){
            throw new Exception(msg);
        }
        if (wsTbDpajSdryJbxxes!=null||wSdryChxxs!=null||wsTbDpajJbxxs!=null){
            if (wsTbDpajSdryJbxxes!=null&&wsTbDpajSdryJbxxes.size()>0) {
                logger.info("TbDpajSdryJbxx插入开始");
                for (WSTbDpajSdryJbxx wstbDpajSdryJbxx : wsTbDpajSdryJbxxes) {
                    String bz= wstbDpajSdryJbxx.getCzbz();
                    String xh = wstbDpajSdryJbxx.getJlbh();
					if ("1111111".equals(bz)){
						sdpk.add(wstbDpajSdryJbxx.getJlbh());
					}
                    TbDpajSdryJbxx param = new TbDpajSdryJbxx();
                    param.setJlbh(xh);
                    TbDpajSdryJbxx oldxd = tbDpajSdryJbxxMapper.selectOne(param);
                    if (oldxd!=null){
						tbDpajSdryJbxx = new TbDpajSdryJbxx();
						BeanUtils.copyProperties(wstbDpajSdryJbxx, tbDpajSdryJbxx);
						tbDpajSdryJbxxMapper.updateByPrimaryKeySelective(tbDpajSdryJbxx);
						logger.info("TbDpajSdryJbxx已有数据："+xh);
                    }else {
                        tbDpajSdryJbxx = new TbDpajSdryJbxx();
                        BeanUtils.copyProperties(wstbDpajSdryJbxx, tbDpajSdryJbxx);
                        tbDpajSdryJbxx.setLy((short) 1);
                        if (zhpk != null && !"".equals(zhpk)) {
                            tbDpajSdryJbxx.setZhpk(zhpk);
                            tbDpajSdryJbxx.setSszh(zh);
                            tbDpajSdryJbxx.setSscj(cj);
                        }
                        tbDpajSdryJbxxMapper.insertSelective(tbDpajSdryJbxx);
                        //插入号段信息
                        TbHdInfo tbHdInfo = new TbHdInfo();
                        String zjhm = tbDpajSdryJbxx.getZjhm();
                        String dh = tbDpajSdryJbxx.getBr_lxdh();
                        if ((zjhm != null && !"".equals(zjhm)) || (dh != null && !"".equals(dh))) {
                            Example example = new Example(TbHdInfo.class);
                            Example.Criteria criteria = example.createCriteria();
                            if (dh != null && !"".equals(dh)) {
                                criteria.andEqualTo("hd_hm", dh);
                                tbHdInfo.setHd_hm(dh);
                            } else {
                                criteria.andEqualTo("hd_hm", zjhm);
                                tbHdInfo.setHd_hm(zjhm);
                            }
                            List<TbHdInfo> hds = tbHdInfoService.queryListByExample(example);
                            if (hds.size() > 0) {
                                continue;
                            } else {
                                logger.info("TbHdInfo插入开始");
                                tbHdInfo.setHd_id(TemplateUtil.genUUID());
                                tbHdInfo.setHd_hz(tbDpajSdryJbxx.getXm() != null ? tbDpajSdryJbxx.getXm() : "");
                                tbHdInfo.setHd_ly(2);
                                tbHdInfo.setHd_yxx(1);
                                tbHdInfo.setHd_lrsj(new Date());
                                tbHdInfoService.insert(tbHdInfo);
                                logger.info("TbHdInfo插入结束");
                            }
                        }
                    }
                }
                logger.info("TbDpajSdryJbxx插入结束");
            }
            if (wSdryChxxs!=null&&wSdryChxxs.size()>0){
                logger.info("TbDpajSdryChxx插入开始");
                for (WSTbDpajSdryChxx wsTbDpajSdryChxx:wSdryChxxs) {
                    String xh = wsTbDpajSdryChxx.getJlbh();
                    TbDpajSdryChxx param = new TbDpajSdryChxx();
                    param.setJlbh(xh);
                    TbDpajSdryChxx oldxd = tbDpajSdryChxxService.queryAsObject(param);
                    if (oldxd!=null){
						tbDpajSdryChxx = new TbDpajSdryChxx();
						BeanUtils.copyProperties(wsTbDpajSdryChxx, tbDpajSdryChxx);
						tbDpajSdryChxxService.updateByPrimaryKey(tbDpajSdryChxx);
                        logger.info("TbDpajSdryChxx已有数据："+xh);
                        continue;
                    }else {
                        tbDpajSdryChxx = new TbDpajSdryChxx();
                        BeanUtils.copyProperties(wsTbDpajSdryChxx, tbDpajSdryChxx);
                        tbDpajSdryChxx.setLy((short) 1);
                        tbDpajSdryChxxService.insert(tbDpajSdryChxx);
                    }
                }
                logger.info("TbDpajSdryChxx插入结束");
            }
            if (wsTbDpajJbxxs!=null&&wsTbDpajJbxxs.size()>0){
                logger.info("TbDpajJbxx插入开始");
                for (WSTbDpajJbxx wsTbDpajJbxx:wsTbDpajJbxxs) {
                    String xh = wsTbDpajJbxx.getJlbh();
                    TbDpajJbxx param = new TbDpajJbxx();
                    param.setJlbh(xh);
                    TbDpajJbxx oldxd = tbDpajJbxxService.queryAsObject(param);
                    if (oldxd!=null){
						tbDpajJbxx = new TbDpajJbxx();
						BeanUtils.copyProperties(wsTbDpajJbxx, tbDpajJbxx);
						tbDpajJbxxService.updateByPrimaryKey(tbDpajJbxx);
                        logger.info("TbDpajJbxx已有数据："+xh);
                        continue;
                    }else {
                        tbDpajJbxx = new TbDpajJbxx();
                        BeanUtils.copyProperties(wsTbDpajJbxx, tbDpajJbxx);
                        tbDpajJbxx.setLy((short) 1);
                        tbDpajJbxxService.insert(tbDpajJbxx);
                    }
                }
                logger.info("TbDpajJbxx插入结束");
            }
        }else{
            logger.info("涉毒人员结果为空");
        }
        logger.info("涉毒人员插入结束");
        return sdpk;
    }

}
