package com.unis.service.jdzh.impl;

import java.util.List;
import java.util.Map;

import com.unis.common.config.DS;
import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.jdzh.TbDpajJbxxMapper;
import com.unis.model.jdzh.TbDpajJbxx;
import com.unis.service.jdzh.TbDpajJbxxService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see TbDpajJbxxService
 * </pre>
 *
 * @author gongjl
 * @version 1.0
 * @since 2020-03-10
 */
@Service("tbDpajJbxxService")
public class TbDpajJbxxServiceImpl extends BaseServiceImpl implements TbDpajJbxxService {
	private static final Logger logger = LoggerFactory.getLogger(TbDpajJbxxServiceImpl.class);
    @Autowired
    private TbDpajJbxxMapper tbDpajJbxxMapper;

    /**
     * @see TbDpajJbxxService#insert(TbDpajJbxx tbDpajJbxx)
     */
    @Override
	@DS("datasource2")
    public int insert(TbDpajJbxx tbDpajJbxx) throws Exception {
    	if (tbDpajJbxx!=null){
	        //tbDpajJbxx.setJlbh(TemplateUtil.genUUID());
	        //menu.setJlbh(getPk("seqName","jgdm","A"));
	                
	        return tbDpajJbxxMapper.insertSelective(tbDpajJbxx);
    	}else{
    		logger.error("TbDpajJbxxServiceImpl.insert时tbDpajJbxx数据为空。");
    		throw new AppRuntimeException("TbDpajJbxxServiceImpl.insert时tbDpajJbxx数据为空。");
    	}        
    }

    /**
     * @see TbDpajJbxxService#delete(String pk)
     */
    @Override
	@DS("datasource2")
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("TbDpajJbxxServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("TbDpajJbxxServiceImpl.delete时pk为空。");
    	}else{
    		return tbDpajJbxxMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see TbDpajJbxxService#updateByPrimaryKey(TbDpajJbxx tbDpajJbxx)
     */
    @Override
	@DS("datasource2")
    public int updateByPrimaryKey(TbDpajJbxx tbDpajJbxx) throws Exception {
        if (tbDpajJbxx!=null){
        	if(StringUtils.isBlank(tbDpajJbxx.getJlbh())){
        		logger.error("TbDpajJbxxServiceImpl.updateByPrimaryKey时tbDpajJbxx.Jlbh为空。");
        		throw new AppRuntimeException("TbDpajJbxxServiceImpl.updateByPrimaryKey时tbDpajJbxx.Jlbh为空。");
        	}
	        return tbDpajJbxxMapper.updateByPrimaryKeySelective(tbDpajJbxx);
    	}else{
    		logger.error("TbDpajJbxxServiceImpl.updateByPrimaryKey时tbDpajJbxx数据为空。");
    		throw new AppRuntimeException("TbDpajJbxxServiceImpl.updateByPrimaryKey时tbDpajJbxx数据为空。");
    	}
    }
    /**
     * @see TbDpajJbxxService#queryTbDpajJbxxByPrimaryKey(String pk)
     */
    @Override
	@DS("datasource2")
    public TbDpajJbxx queryTbDpajJbxxByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("TbDpajJbxxServiceImpl.queryTbDpajJbxxByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("TbDpajJbxxServiceImpl.queryTbDpajJbxxByPrimaryKey时pk为空。");
    	}else{
    		return tbDpajJbxxMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see TbDpajJbxxService#queryAsObject(TbDpajJbxx tbDpajJbxx)
     */
    @Override
	@DS("datasource2")
    public TbDpajJbxx queryAsObject(TbDpajJbxx tbDpajJbxx) throws Exception {
        if (tbDpajJbxx!=null){
	        return tbDpajJbxxMapper.selectOne(tbDpajJbxx);
    	}else{
    		logger.error("TbDpajJbxxServiceImpl.queryAsObject时tbDpajJbxx数据为空。");
    		throw new AppRuntimeException("TbDpajJbxxServiceImpl.queryAsObject时tbDpajJbxx数据为空。");
    	}
    }
    
    /**
     * @see TbDpajJbxxService#queryCountByExample(Example example)
     */
    @Override
	@DS("datasource2")
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return tbDpajJbxxMapper.selectCountByExample(example);
    	}else{
    		logger.error("TbDpajJbxxServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("TbDpajJbxxServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see TbDpajJbxxService#queryListByExample(Example example)
     */
    @Override
	@DS("datasource2")
    public List<TbDpajJbxx> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return tbDpajJbxxMapper.selectByExample(example);
    	}else{
    		logger.error("TbDpajJbxxServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("TbDpajJbxxServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see TbDpajJbxxService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
	@DS("datasource2")
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(tbDpajJbxxMapper.selectByExample(example));
    	}else{
    		logger.error("TbDpajJbxxServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("TbDpajJbxxServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see TbDpajJbxxService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
	@DS("datasource2")
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(TbDpajJbxx.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("jlbh")!=null && StringUtils.isNotBlank(parmMap.get("jlbh").toString())){
				criteria.andEqualTo("jlbh",parmMap.get("jlbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ajbh")!=null && StringUtils.isNotBlank(parmMap.get("ajbh").toString())){
				criteria.andEqualTo("ajbh",parmMap.get("ajbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ajmc")!=null && StringUtils.isNotBlank(parmMap.get("ajmc").toString())){
				criteria.andEqualTo("ajmc",parmMap.get("ajmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ajlbdm")!=null && StringUtils.isNotBlank(parmMap.get("ajlbdm").toString())){
				criteria.andEqualTo("ajlbdm",parmMap.get("ajlbdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fxsj")!=null && StringUtils.isNotBlank(parmMap.get("fxsj").toString())){
				criteria.andEqualTo("fxsj",parmMap.get("fxsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("dzmc")!=null && StringUtils.isNotBlank(parmMap.get("dzmc").toString())){
				criteria.andEqualTo("dzmc",parmMap.get("dzmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fazdxz")!=null && StringUtils.isNotBlank(parmMap.get("fazdxz").toString())){
				criteria.andEqualTo("fazdxz",parmMap.get("fazdxz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("padq")!=null && StringUtils.isNotBlank(parmMap.get("padq").toString())){
				criteria.andEqualTo("padq",parmMap.get("padq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ladw_dwdm")!=null && StringUtils.isNotBlank(parmMap.get("ladw_dwdm").toString())){
				criteria.andEqualTo("ladw_dwdm",parmMap.get("ladw_dwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ladw_dwmc")!=null && StringUtils.isNotBlank(parmMap.get("ladw_dwmc").toString())){
				criteria.andEqualTo("ladw_dwmc",parmMap.get("ladw_dwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cbrxm")!=null && StringUtils.isNotBlank(parmMap.get("cbrxm").toString())){
				criteria.andEqualTo("cbrxm",parmMap.get("cbrxm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cbr_lxdh")!=null && StringUtils.isNotBlank(parmMap.get("cbr_lxdh").toString())){
				criteria.andEqualTo("cbr_lxdh",parmMap.get("cbr_lxdh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("larq")!=null && StringUtils.isNotBlank(parmMap.get("larq").toString())){
				criteria.andEqualTo("larq",parmMap.get("larq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("padw_dwdm")!=null && StringUtils.isNotBlank(parmMap.get("padw_dwdm").toString())){
				criteria.andEqualTo("padw_dwdm",parmMap.get("padw_dwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("padw_dwmc")!=null && StringUtils.isNotBlank(parmMap.get("padw_dwmc").toString())){
				criteria.andEqualTo("padw_dwmc",parmMap.get("padw_dwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("badwlxdm")!=null && StringUtils.isNotBlank(parmMap.get("badwlxdm").toString())){
				criteria.andEqualTo("badwlxdm",parmMap.get("badwlxdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("parq")!=null && StringUtils.isNotBlank(parmMap.get("parq").toString())){
				criteria.andEqualTo("parq",parmMap.get("parq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cdfsdm")!=null && StringUtils.isNotBlank(parmMap.get("cdfsdm").toString())){
				criteria.andEqualTo("cdfsdm",parmMap.get("cdfsdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fyfsdm")!=null && StringUtils.isNotBlank(parmMap.get("fyfsdm").toString())){
				criteria.andEqualTo("fyfsdm",parmMap.get("fyfsdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfjt")!=null && StringUtils.isNotBlank(parmMap.get("sfjt").toString())){
				criteria.andEqualTo("sfjt",parmMap.get("sfjt").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fzthxzdm")!=null && StringUtils.isNotBlank(parmMap.get("fzthxzdm").toString())){
				criteria.andEqualTo("fzthxzdm",parmMap.get("fzthxzdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fzthmc")!=null && StringUtils.isNotBlank(parmMap.get("fzthmc").toString())){
				criteria.andEqualTo("fzthmc",parmMap.get("fzthmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jyaq")!=null && StringUtils.isNotBlank(parmMap.get("jyaq").toString())){
				criteria.andEqualTo("jyaq",parmMap.get("jyaq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jzrtgqk")!=null && StringUtils.isNotBlank(parmMap.get("jzrtgqk").toString())){
				criteria.andEqualTo("jzrtgqk",parmMap.get("jzrtgqk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zarqkfx")!=null && StringUtils.isNotBlank(parmMap.get("zarqkfx").toString())){
				criteria.andEqualTo("zarqkfx",parmMap.get("zarqkfx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("asjzcxwlbdm")!=null && StringUtils.isNotBlank(parmMap.get("asjzcxwlbdm").toString())){
				criteria.andEqualTo("asjzcxwlbdm",parmMap.get("asjzcxwlbdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("pafsdm")!=null && StringUtils.isNotBlank(parmMap.get("pafsdm").toString())){
				criteria.andEqualTo("pafsdm",parmMap.get("pafsdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cxajyydm")!=null && StringUtils.isNotBlank(parmMap.get("cxajyydm").toString())){
				criteria.andEqualTo("cxajyydm",parmMap.get("cxajyydm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cxajrq")!=null && StringUtils.isNotBlank(parmMap.get("cxajrq").toString())){
				criteria.andEqualTo("cxajrq",parmMap.get("cxajrq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cjqk")!=null && StringUtils.isNotBlank(parmMap.get("cjqk").toString())){
				criteria.andEqualTo("cjqk",parmMap.get("cjqk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cjrq")!=null && StringUtils.isNotBlank(parmMap.get("cjrq").toString())){
				criteria.andEqualTo("cjrq",parmMap.get("cjrq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gkfs")!=null && StringUtils.isNotBlank(parmMap.get("gkfs").toString())){
				criteria.andEqualTo("gkfs",parmMap.get("gkfs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("wqgkrq")!=null && StringUtils.isNotBlank(parmMap.get("wqgkrq").toString())){
				criteria.andEqualTo("wqgkrq",parmMap.get("wqgkrq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("syztdm")!=null && StringUtils.isNotBlank(parmMap.get("syztdm").toString())){
				criteria.andEqualTo("syztdm",parmMap.get("syztdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cxdw_dwmc")!=null && StringUtils.isNotBlank(parmMap.get("cxdw_dwmc").toString())){
				criteria.andEqualTo("cxdw_dwmc",parmMap.get("cxdw_dwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cxdw_dwdm")!=null && StringUtils.isNotBlank(parmMap.get("cxdw_dwdm").toString())){
				criteria.andEqualTo("cxdw_dwdm",parmMap.get("cxdw_dwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zcjddm")!=null && StringUtils.isNotBlank(parmMap.get("zcjddm").toString())){
				criteria.andEqualTo("zcjddm",parmMap.get("zcjddm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("tbdw_dwdm")!=null && StringUtils.isNotBlank(parmMap.get("tbdw_dwdm").toString())){
				criteria.andEqualTo("tbdw_dwdm",parmMap.get("tbdw_dwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("tbdw_dwmc")!=null && StringUtils.isNotBlank(parmMap.get("tbdw_dwmc").toString())){
				criteria.andEqualTo("tbdw_dwmc",parmMap.get("tbdw_dwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("djr_xm")!=null && StringUtils.isNotBlank(parmMap.get("djr_xm").toString())){
				criteria.andEqualTo("djr_xm",parmMap.get("djr_xm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("djrq")!=null && StringUtils.isNotBlank(parmMap.get("djrq").toString())){
				criteria.andEqualTo("djrq",parmMap.get("djrq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bz")!=null && StringUtils.isNotBlank(parmMap.get("bz").toString())){
				criteria.andEqualTo("bz",parmMap.get("bz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("shdw_dwmc")!=null && StringUtils.isNotBlank(parmMap.get("shdw_dwmc").toString())){
				criteria.andEqualTo("shdw_dwmc",parmMap.get("shdw_dwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("shdw_dwdm")!=null && StringUtils.isNotBlank(parmMap.get("shdw_dwdm").toString())){
				criteria.andEqualTo("shdw_dwdm",parmMap.get("shdw_dwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jhdp")!=null && StringUtils.isNotBlank(parmMap.get("jhdp").toString())){
				criteria.andEqualTo("jhdp",parmMap.get("jhdp").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qbtgdw_dwmc")!=null && StringUtils.isNotBlank(parmMap.get("qbtgdw_dwmc").toString())){
				criteria.andEqualTo("qbtgdw_dwmc",parmMap.get("qbtgdw_dwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qbtgdw_dwdm")!=null && StringUtils.isNotBlank(parmMap.get("qbtgdw_dwdm").toString())){
				criteria.andEqualTo("qbtgdw_dwdm",parmMap.get("qbtgdw_dwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fyqdsm")!=null && StringUtils.isNotBlank(parmMap.get("fyqdsm").toString())){
				criteria.andEqualTo("fyqdsm",parmMap.get("fyqdsm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("pdbz")!=null && StringUtils.isNotBlank(parmMap.get("pdbz").toString())){
				criteria.andEqualTo("pdbz",parmMap.get("pdbz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ajdh")!=null && StringUtils.isNotBlank(parmMap.get("ajdh").toString())){
				criteria.andEqualTo("ajdh",parmMap.get("ajdh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ajxz")!=null && StringUtils.isNotBlank(parmMap.get("ajxz").toString())){
				criteria.andEqualTo("ajxz",parmMap.get("ajxz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zzdw_dwmc")!=null && StringUtils.isNotBlank(parmMap.get("zzdw_dwmc").toString())){
				criteria.andEqualTo("zzdw_dwmc",parmMap.get("zzdw_dwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zzdw_dwdm")!=null && StringUtils.isNotBlank(parmMap.get("zzdw_dwdm").toString())){
				criteria.andEqualTo("zzdw_dwdm",parmMap.get("zzdw_dwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zddw_dwmc")!=null && StringUtils.isNotBlank(parmMap.get("zddw_dwmc").toString())){
				criteria.andEqualTo("zddw_dwmc",parmMap.get("zddw_dwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zddw_dwdm")!=null && StringUtils.isNotBlank(parmMap.get("zddw_dwdm").toString())){
				criteria.andEqualTo("zddw_dwdm",parmMap.get("zddw_dwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gjhdqdm")!=null && StringUtils.isNotBlank(parmMap.get("gjhdqdm").toString())){
				criteria.andEqualTo("gjhdqdm",parmMap.get("gjhdqdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sadq_xzqh")!=null && StringUtils.isNotBlank(parmMap.get("sadq_xzqh").toString())){
				criteria.andEqualTo("sadq_xzqh",parmMap.get("sadq_xzqh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fzrxm")!=null && StringUtils.isNotBlank(parmMap.get("fzrxm").toString())){
				criteria.andEqualTo("fzrxm",parmMap.get("fzrxm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fzr_lxdh")!=null && StringUtils.isNotBlank(parmMap.get("fzr_lxdh").toString())){
				criteria.andEqualTo("fzr_lxdh",parmMap.get("fzr_lxdh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xsly")!=null && StringUtils.isNotBlank(parmMap.get("xsly").toString())){
				criteria.andEqualTo("xsly",parmMap.get("xsly").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xsly_dwdm")!=null && StringUtils.isNotBlank(parmMap.get("xsly_dwdm").toString())){
				criteria.andEqualTo("xsly_dwdm",parmMap.get("xsly_dwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xsly_dwmc")!=null && StringUtils.isNotBlank(parmMap.get("xsly_dwmc").toString())){
				criteria.andEqualTo("xsly_dwmc",parmMap.get("xsly_dwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qyxsly")!=null && StringUtils.isNotBlank(parmMap.get("qyxsly").toString())){
				criteria.andEqualTo("qyxsly",parmMap.get("qyxsly").toString().trim());
				flag = true;
			}
    		if(parmMap.get("mqzcgzjzqk")!=null && StringUtils.isNotBlank(parmMap.get("mqzcgzjzqk").toString())){
				criteria.andEqualTo("mqzcgzjzqk",parmMap.get("mqzcgzjzqk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zcgzzwt")!=null && StringUtils.isNotBlank(parmMap.get("zcgzzwt").toString())){
				criteria.andEqualTo("zcgzzwt",parmMap.get("zcgzzwt").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xybgzjy")!=null && StringUtils.isNotBlank(parmMap.get("xybgzjy").toString())){
				criteria.andEqualTo("xybgzjy",parmMap.get("xybgzjy").toString().trim());
				flag = true;
			}
    		if(parmMap.get("inforcontent")!=null && StringUtils.isNotBlank(parmMap.get("inforcontent").toString())){
				criteria.andEqualTo("inforcontent",parmMap.get("inforcontent").toString().trim());
				flag = true;
			}
    		if(parmMap.get("asjdbjbdm")!=null && StringUtils.isNotBlank(parmMap.get("asjdbjbdm").toString())){
				criteria.andEqualTo("asjdbjbdm",parmMap.get("asjdbjbdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("pazt")!=null && StringUtils.isNotBlank(parmMap.get("pazt").toString())){
				criteria.andEqualTo("pazt",parmMap.get("pazt").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ywlb")!=null && StringUtils.isNotBlank(parmMap.get("ywlb").toString())){
				criteria.andEqualTo("ywlb",parmMap.get("ywlb").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qlsj")!=null && StringUtils.isNotBlank(parmMap.get("qlsj").toString())){
				criteria.andEqualTo("qlsj",parmMap.get("qlsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zcsbqs")!=null && StringUtils.isNotBlank(parmMap.get("zcsbqs").toString())){
				criteria.andEqualTo("zcsbqs",parmMap.get("zcsbqs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sqlhqs")!=null && StringUtils.isNotBlank(parmMap.get("sqlhqs").toString())){
				criteria.andEqualTo("sqlhqs",parmMap.get("sqlhqs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sqlhspbs")!=null && StringUtils.isNotBlank(parmMap.get("sqlhspbs").toString())){
				criteria.andEqualTo("sqlhspbs",parmMap.get("sqlhspbs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lhba")!=null && StringUtils.isNotBlank(parmMap.get("lhba").toString())){
				criteria.andEqualTo("lhba",parmMap.get("lhba").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lhzc_gajgjgdm")!=null && StringUtils.isNotBlank(parmMap.get("lhzc_gajgjgdm").toString())){
				criteria.andEqualTo("lhzc_gajgjgdm",parmMap.get("lhzc_gajgjgdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ajpasqb")!=null && StringUtils.isNotBlank(parmMap.get("ajpasqb").toString())){
				criteria.andEqualTo("ajpasqb",parmMap.get("ajpasqb").toString().trim());
				flag = true;
			}
    		if(parmMap.get("paqlsj")!=null && StringUtils.isNotBlank(parmMap.get("paqlsj").toString())){
				criteria.andEqualTo("paqlsj",parmMap.get("paqlsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cxqs")!=null && StringUtils.isNotBlank(parmMap.get("cxqs").toString())){
				criteria.andEqualTo("cxqs",parmMap.get("cxqs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lacbdq")!=null && StringUtils.isNotBlank(parmMap.get("lacbdq").toString())){
				criteria.andEqualTo("lacbdq",parmMap.get("lacbdq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("dqshjg")!=null && StringUtils.isNotBlank(parmMap.get("dqshjg").toString())){
				criteria.andEqualTo("dqshjg",parmMap.get("dqshjg").toString().trim());
				flag = true;
			}
    		if(parmMap.get("shbz")!=null && StringUtils.isNotBlank(parmMap.get("shbz").toString())){
				criteria.andEqualTo("shbz",parmMap.get("shbz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gxsj")!=null && StringUtils.isNotBlank(parmMap.get("gxsj").toString())){
				criteria.andEqualTo("gxsj",parmMap.get("gxsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gxsjc")!=null && StringUtils.isNotBlank(parmMap.get("gxsjc").toString())){
				criteria.andEqualTo("gxsjc",parmMap.get("gxsjc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xgbsdw")!=null && StringUtils.isNotBlank(parmMap.get("xgbsdw").toString())){
				criteria.andEqualTo("xgbsdw",parmMap.get("xgbsdw").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfhlwaj")!=null && StringUtils.isNotBlank(parmMap.get("sfhlwaj").toString())){
				criteria.andEqualTo("sfhlwaj",parmMap.get("sfhlwaj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("hlwajlx")!=null && StringUtils.isNotBlank(parmMap.get("hlwajlx").toString())){
				criteria.andEqualTo("hlwajlx",parmMap.get("hlwajlx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qtsm")!=null && StringUtils.isNotBlank(parmMap.get("qtsm").toString())){
				criteria.andEqualTo("qtsm",parmMap.get("qtsm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfsxxq")!=null && StringUtils.isNotBlank(parmMap.get("sfsxxq").toString())){
				criteria.andEqualTo("sfsxxq",parmMap.get("sfsxxq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sdxqfs")!=null && StringUtils.isNotBlank(parmMap.get("sdxqfs").toString())){
				criteria.andEqualTo("sdxqfs",parmMap.get("sdxqfs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xqqtsm")!=null && StringUtils.isNotBlank(parmMap.get("xqqtsm").toString())){
				criteria.andEqualTo("xqqtsm",parmMap.get("xqqtsm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfhsh")!=null && StringUtils.isNotBlank(parmMap.get("sfhsh").toString())){
				criteria.andEqualTo("sfhsh",parmMap.get("sfhsh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfwzfd")!=null && StringUtils.isNotBlank(parmMap.get("sfwzfd").toString())){
				criteria.andEqualTo("sfwzfd",parmMap.get("sfwzfd").toString().trim());
				flag = true;
			}
    		if(parmMap.get("mbajzt")!=null && StringUtils.isNotBlank(parmMap.get("mbajzt").toString())){
				criteria.andEqualTo("mbajzt",parmMap.get("mbajzt").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ajdj")!=null && StringUtils.isNotBlank(parmMap.get("ajdj").toString())){
				criteria.andEqualTo("ajdj",parmMap.get("ajdj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ajlx")!=null && StringUtils.isNotBlank(parmMap.get("ajlx").toString())){
				criteria.andEqualTo("ajlx",parmMap.get("ajlx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("b_m_jlbh")!=null && StringUtils.isNotBlank(parmMap.get("b_m_jlbh").toString())){
				criteria.andEqualTo("b_m_jlbh",parmMap.get("b_m_jlbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("s_m_jlbh")!=null && StringUtils.isNotBlank(parmMap.get("s_m_jlbh").toString())){
				criteria.andEqualTo("s_m_jlbh",parmMap.get("s_m_jlbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("c_m_jlbh")!=null && StringUtils.isNotBlank(parmMap.get("c_m_jlbh").toString())){
				criteria.andEqualTo("c_m_jlbh",parmMap.get("c_m_jlbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("mbajsbdj")!=null && StringUtils.isNotBlank(parmMap.get("mbajsbdj").toString())){
				criteria.andEqualTo("mbajsbdj",parmMap.get("mbajsbdj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xcxs")!=null && StringUtils.isNotBlank(parmMap.get("xcxs").toString())){
				criteria.andEqualTo("xcxs",parmMap.get("xcxs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfxc")!=null && StringUtils.isNotBlank(parmMap.get("sfxc").toString())){
				criteria.andEqualTo("sfxc",parmMap.get("sfxc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfqzjb")!=null && StringUtils.isNotBlank(parmMap.get("sfqzjb").toString())){
				criteria.andEqualTo("sfqzjb",parmMap.get("sfqzjb").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jjsqxx")!=null && StringUtils.isNotBlank(parmMap.get("jjsqxx").toString())){
				criteria.andEqualTo("jjsqxx",parmMap.get("jjsqxx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sbdw_dwmc")!=null && StringUtils.isNotBlank(parmMap.get("sbdw_dwmc").toString())){
				criteria.andEqualTo("sbdw_dwmc",parmMap.get("sbdw_dwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sbdw_dwdm")!=null && StringUtils.isNotBlank(parmMap.get("sbdw_dwdm").toString())){
				criteria.andEqualTo("sbdw_dwdm",parmMap.get("sbdw_dwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lxr")!=null && StringUtils.isNotBlank(parmMap.get("lxr").toString())){
				criteria.andEqualTo("lxr",parmMap.get("lxr").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lxr_dh")!=null && StringUtils.isNotBlank(parmMap.get("lxr_dh").toString())){
				criteria.andEqualTo("lxr_dh",parmMap.get("lxr_dh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("b_inforcontent")!=null && StringUtils.isNotBlank(parmMap.get("b_inforcontent").toString())){
				criteria.andEqualTo("b_inforcontent",parmMap.get("b_inforcontent").toString().trim());
				flag = true;
			}
    		if(parmMap.get("s_inforcontent")!=null && StringUtils.isNotBlank(parmMap.get("s_inforcontent").toString())){
				criteria.andEqualTo("s_inforcontent",parmMap.get("s_inforcontent").toString().trim());
				flag = true;
			}
    		if(parmMap.get("c_inforcontent")!=null && StringUtils.isNotBlank(parmMap.get("c_inforcontent").toString())){
				criteria.andEqualTo("c_inforcontent",parmMap.get("c_inforcontent").toString().trim());
				flag = true;
			}
    		if(parmMap.get("x_inforcontent")!=null && StringUtils.isNotBlank(parmMap.get("x_inforcontent").toString())){
				criteria.andEqualTo("x_inforcontent",parmMap.get("x_inforcontent").toString().trim());
				flag = true;
			}
    		if(parmMap.get("czbs")!=null && StringUtils.isNotBlank(parmMap.get("czbs").toString())){
				criteria.andEqualTo("czbs",parmMap.get("czbs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zyqxdzfs")!=null && StringUtils.isNotBlank(parmMap.get("zyqxdzfs").toString())){
				criteria.andEqualTo("zyqxdzfs",parmMap.get("zyqxdzfs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cjzdbh")!=null && StringUtils.isNotBlank(parmMap.get("cjzdbh").toString())){
				criteria.andEqualTo("cjzdbh",parmMap.get("cjzdbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("tqrybh")!=null && StringUtils.isNotBlank(parmMap.get("tqrybh").toString())){
				criteria.andEqualTo("tqrybh",parmMap.get("tqrybh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qbcpbh")!=null && StringUtils.isNotBlank(parmMap.get("qbcpbh").toString())){
				criteria.andEqualTo("qbcpbh",parmMap.get("qbcpbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zsjjsqxx")!=null && StringUtils.isNotBlank(parmMap.get("zsjjsqxx").toString())){
				criteria.andEqualTo("zsjjsqxx",parmMap.get("zsjjsqxx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("b_inforcontent_sqsj")!=null && StringUtils.isNotBlank(parmMap.get("b_inforcontent_sqsj").toString())){
				criteria.andEqualTo("b_inforcontent_sqsj",parmMap.get("b_inforcontent_sqsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("s_inforcontent_sqsj")!=null && StringUtils.isNotBlank(parmMap.get("s_inforcontent_sqsj").toString())){
				criteria.andEqualTo("s_inforcontent_sqsj",parmMap.get("s_inforcontent_sqsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("c_inforcontent_sqsj")!=null && StringUtils.isNotBlank(parmMap.get("c_inforcontent_sqsj").toString())){
				criteria.andEqualTo("c_inforcontent_sqsj",parmMap.get("c_inforcontent_sqsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("x_inforcontent_sqsj")!=null && StringUtils.isNotBlank(parmMap.get("x_inforcontent_sqsj").toString())){
				criteria.andEqualTo("x_inforcontent_sqsj",parmMap.get("x_inforcontent_sqsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bsxm_lx")!=null && StringUtils.isNotBlank(parmMap.get("bsxm_lx").toString())){
				criteria.andEqualTo("bsxm_lx",parmMap.get("bsxm_lx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bm_qlsj")!=null && StringUtils.isNotBlank(parmMap.get("bm_qlsj").toString())){
				criteria.andEqualTo("bm_qlsj",parmMap.get("bm_qlsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sm_qlsj")!=null && StringUtils.isNotBlank(parmMap.get("sm_qlsj").toString())){
				criteria.andEqualTo("sm_qlsj",parmMap.get("sm_qlsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cm_qlsj")!=null && StringUtils.isNotBlank(parmMap.get("cm_qlsj").toString())){
				criteria.andEqualTo("cm_qlsj",parmMap.get("cm_qlsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cszt")!=null && StringUtils.isNotBlank(parmMap.get("cszt").toString())){
				criteria.andEqualTo("cszt",parmMap.get("cszt").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cbrxm2")!=null && StringUtils.isNotBlank(parmMap.get("cbrxm2").toString())){
				criteria.andEqualTo("cbrxm2",parmMap.get("cbrxm2").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cbr_lxdh2")!=null && StringUtils.isNotBlank(parmMap.get("cbr_lxdh2").toString())){
				criteria.andEqualTo("cbr_lxdh2",parmMap.get("cbr_lxdh2").toString().trim());
				flag = true;
			}
    		if(parmMap.get("pafsdm_old")!=null && StringUtils.isNotBlank(parmMap.get("pafsdm_old").toString())){
				criteria.andEqualTo("pafsdm_old",parmMap.get("pafsdm_old").toString().trim());
				flag = true;
			}
    		if(parmMap.get("n_yxh")!=null && StringUtils.isNotBlank(parmMap.get("n_yxh").toString())){
				criteria.andEqualTo("n_yxh",parmMap.get("n_yxh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sjly")!=null && StringUtils.isNotBlank(parmMap.get("sjly").toString())){
				criteria.andEqualTo("sjly",parmMap.get("sjly").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qqczcs")!=null && StringUtils.isNotBlank(parmMap.get("qqczcs").toString())){
				criteria.andEqualTo("qqczcs",parmMap.get("qqczcs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zzly")!=null && StringUtils.isNotBlank(parmMap.get("zzly").toString())){
				criteria.andEqualTo("zzly",parmMap.get("zzly").toString().trim());
				flag = true;
			}
    		if(parmMap.get("mbajlb")!=null && StringUtils.isNotBlank(parmMap.get("mbajlb").toString())){
				criteria.andEqualTo("mbajlb",parmMap.get("mbajlb").toString().trim());
				flag = true;
			}
    		if(parmMap.get("mbajxh")!=null && StringUtils.isNotBlank(parmMap.get("mbajxh").toString())){
				criteria.andEqualTo("mbajxh",parmMap.get("mbajxh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ajly")!=null && StringUtils.isNotBlank(parmMap.get("ajly").toString())){
				criteria.andEqualTo("ajly",parmMap.get("ajly").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jzcd_ajlx")!=null && StringUtils.isNotBlank(parmMap.get("jzcd_ajlx").toString())){
				criteria.andEqualTo("jzcd_ajlx",parmMap.get("jzcd_ajlx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ly")!=null && StringUtils.isNotBlank(parmMap.get("ly").toString())){
				criteria.andEqualTo("ly",parmMap.get("ly").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("TbDpajJbxxServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("TbDpajJbxxServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
