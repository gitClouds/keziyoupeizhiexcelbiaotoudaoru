package com.unis.service.jdzh.impl;

import java.util.List;
import java.util.Map;

import com.unis.common.config.DS;
import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.jdzh.TbDpajSdryChxxMapper;
import com.unis.model.jdzh.TbDpajSdryChxx;
import com.unis.service.jdzh.TbDpajSdryChxxService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see TbDpajSdryChxxService
 * </pre>
 *
 * @author gongjl
 * @version 1.0
 * @since 2020-03-10
 */
@Service("tbDpajSdryChxxService")
public class TbDpajSdryChxxServiceImpl extends BaseServiceImpl implements TbDpajSdryChxxService {
	private static final Logger logger = LoggerFactory.getLogger(TbDpajSdryChxxServiceImpl.class);
    @Autowired
    private TbDpajSdryChxxMapper tbDpajSdryChxxMapper;

    /**
     * @see TbDpajSdryChxxService#insert(TbDpajSdryChxx tbDpajSdryChxx)
     */
    @Override
	@DS("datasource2")
    public int insert(TbDpajSdryChxx tbDpajSdryChxx) throws Exception {
    	if (tbDpajSdryChxx!=null){
	        //tbDpajSdryChxx.setJlbh(TemplateUtil.genUUID());
	        //menu.setJlbh(getPk("seqName","jgdm","A"));
	                
	        return tbDpajSdryChxxMapper.insertSelective(tbDpajSdryChxx);
    	}else{
    		logger.error("TbDpajSdryChxxServiceImpl.insert时tbDpajSdryChxx数据为空。");
    		throw new AppRuntimeException("TbDpajSdryChxxServiceImpl.insert时tbDpajSdryChxx数据为空。");
    	}        
    }

    /**
     * @see TbDpajSdryChxxService#delete(String pk)
     */
    @Override
	@DS("datasource2")
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("TbDpajSdryChxxServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("TbDpajSdryChxxServiceImpl.delete时pk为空。");
    	}else{
    		return tbDpajSdryChxxMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see TbDpajSdryChxxService#updateByPrimaryKey(TbDpajSdryChxx tbDpajSdryChxx)
     */
    @Override
	@DS("datasource2")
    public int updateByPrimaryKey(TbDpajSdryChxx tbDpajSdryChxx) throws Exception {
        if (tbDpajSdryChxx!=null){
        	if(StringUtils.isBlank(tbDpajSdryChxx.getJlbh())){
        		logger.error("TbDpajSdryChxxServiceImpl.updateByPrimaryKey时tbDpajSdryChxx.Jlbh为空。");
        		throw new AppRuntimeException("TbDpajSdryChxxServiceImpl.updateByPrimaryKey时tbDpajSdryChxx.Jlbh为空。");
        	}
	        return tbDpajSdryChxxMapper.updateByPrimaryKeySelective(tbDpajSdryChxx);
    	}else{
    		logger.error("TbDpajSdryChxxServiceImpl.updateByPrimaryKey时tbDpajSdryChxx数据为空。");
    		throw new AppRuntimeException("TbDpajSdryChxxServiceImpl.updateByPrimaryKey时tbDpajSdryChxx数据为空。");
    	}
    }
    /**
     * @see TbDpajSdryChxxService#queryTbDpajSdryChxxByPrimaryKey(String pk)
     */
    @Override
	@DS("datasource2")
    public TbDpajSdryChxx queryTbDpajSdryChxxByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("TbDpajSdryChxxServiceImpl.queryTbDpajSdryChxxByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("TbDpajSdryChxxServiceImpl.queryTbDpajSdryChxxByPrimaryKey时pk为空。");
    	}else{
    		return tbDpajSdryChxxMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see TbDpajSdryChxxService#queryAsObject(TbDpajSdryChxx tbDpajSdryChxx)
     */
    @Override
	@DS("datasource2")
    public TbDpajSdryChxx queryAsObject(TbDpajSdryChxx tbDpajSdryChxx) throws Exception {
        if (tbDpajSdryChxx!=null){
	        return tbDpajSdryChxxMapper.selectOne(tbDpajSdryChxx);
    	}else{
    		logger.error("TbDpajSdryChxxServiceImpl.queryAsObject时tbDpajSdryChxx数据为空。");
    		throw new AppRuntimeException("TbDpajSdryChxxServiceImpl.queryAsObject时tbDpajSdryChxx数据为空。");
    	}
    }
    
    /**
     * @see TbDpajSdryChxxService#queryCountByExample(Example example)
     */
    @Override
	@DS("datasource2")
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return tbDpajSdryChxxMapper.selectCountByExample(example);
    	}else{
    		logger.error("TbDpajSdryChxxServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("TbDpajSdryChxxServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see TbDpajSdryChxxService#queryListByExample(Example example)
     */
    @Override
	@DS("datasource2")
    public List<TbDpajSdryChxx> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return tbDpajSdryChxxMapper.selectByExample(example);
    	}else{
    		logger.error("TbDpajSdryChxxServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("TbDpajSdryChxxServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see TbDpajSdryChxxService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
	@DS("datasource2")
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(tbDpajSdryChxxMapper.selectByExample(example));
    	}else{
    		logger.error("TbDpajSdryChxxServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("TbDpajSdryChxxServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see TbDpajSdryChxxService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
	@DS("datasource2")
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(TbDpajSdryChxx.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("jlbh")!=null && StringUtils.isNotBlank(parmMap.get("jlbh").toString())){
				criteria.andEqualTo("jlbh",parmMap.get("jlbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ajbh")!=null && StringUtils.isNotBlank(parmMap.get("ajbh").toString())){
				criteria.andEqualTo("ajbh",parmMap.get("ajbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xyrbh")!=null && StringUtils.isNotBlank(parmMap.get("xyrbh").toString())){
				criteria.andEqualTo("xyrbh",parmMap.get("xyrbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("tbdw_dwmc")!=null && StringUtils.isNotBlank(parmMap.get("tbdw_dwmc").toString())){
				criteria.andEqualTo("tbdw_dwmc",parmMap.get("tbdw_dwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("djr_xm")!=null && StringUtils.isNotBlank(parmMap.get("djr_xm").toString())){
				criteria.andEqualTo("djr_xm",parmMap.get("djr_xm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("djrq")!=null && StringUtils.isNotBlank(parmMap.get("djrq").toString())){
				criteria.andEqualTo("djrq",parmMap.get("djrq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sdlx")!=null && StringUtils.isNotBlank(parmMap.get("sdlx").toString())){
				criteria.andEqualTo("sdlx",parmMap.get("sdlx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("chrq")!=null && StringUtils.isNotBlank(parmMap.get("chrq").toString())){
				criteria.andEqualTo("chrq",parmMap.get("chrq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ddmc")!=null && StringUtils.isNotBlank(parmMap.get("ddmc").toString())){
				criteria.andEqualTo("ddmc",parmMap.get("ddmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("hdqh_xzqh")!=null && StringUtils.isNotBlank(parmMap.get("hdqh_xzqh").toString())){
				criteria.andEqualTo("hdqh_xzqh",parmMap.get("hdqh_xzqh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("chdw_dwmc")!=null && StringUtils.isNotBlank(parmMap.get("chdw_dwmc").toString())){
				criteria.andEqualTo("chdw_dwmc",parmMap.get("chdw_dwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("czqk")!=null && StringUtils.isNotBlank(parmMap.get("czqk").toString())){
				criteria.andEqualTo("czqk",parmMap.get("czqk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("chd_xzqh")!=null && StringUtils.isNotBlank(parmMap.get("chd_xzqh").toString())){
				criteria.andEqualTo("chd_xzqh",parmMap.get("chd_xzqh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("czrq")!=null && StringUtils.isNotBlank(parmMap.get("czrq").toString())){
				criteria.andEqualTo("czrq",parmMap.get("czrq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jyaq")!=null && StringUtils.isNotBlank(parmMap.get("jyaq").toString())){
				criteria.andEqualTo("jyaq",parmMap.get("jyaq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("khyhmc")!=null && StringUtils.isNotBlank(parmMap.get("khyhmc").toString())){
				criteria.andEqualTo("khyhmc",parmMap.get("khyhmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xykh")!=null && StringUtils.isNotBlank(parmMap.get("xykh").toString())){
				criteria.andEqualTo("xykh",parmMap.get("xykh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yhzh")!=null && StringUtils.isNotBlank(parmMap.get("yhzh").toString())){
				criteria.andEqualTo("yhzh",parmMap.get("yhzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jdchphm")!=null && StringUtils.isNotBlank(parmMap.get("jdchphm").toString())){
				criteria.andEqualTo("jdchphm",parmMap.get("jdchphm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xdjlhcjgdm")!=null && StringUtils.isNotBlank(parmMap.get("xdjlhcjgdm").toString())){
				criteria.andEqualTo("xdjlhcjgdm",parmMap.get("xdjlhcjgdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ztrybh")!=null && StringUtils.isNotBlank(parmMap.get("ztrybh").toString())){
				criteria.andEqualTo("ztrybh",parmMap.get("ztrybh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lad_xzqh")!=null && StringUtils.isNotBlank(parmMap.get("lad_xzqh").toString())){
				criteria.andEqualTo("lad_xzqh",parmMap.get("lad_xzqh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("swzjrq")!=null && StringUtils.isNotBlank(parmMap.get("swzjrq").toString())){
				criteria.andEqualTo("swzjrq",parmMap.get("swzjrq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zhd_xzqh")!=null && StringUtils.isNotBlank(parmMap.get("zhd_xzqh").toString())){
				criteria.andEqualTo("zhd_xzqh",parmMap.get("zhd_xzqh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("tjjbdm")!=null && StringUtils.isNotBlank(parmMap.get("tjjbdm").toString())){
				criteria.andEqualTo("tjjbdm",parmMap.get("tjjbdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zhfsdm")!=null && StringUtils.isNotBlank(parmMap.get("zhfsdm").toString())){
				criteria.andEqualTo("zhfsdm",parmMap.get("zhfsdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xstgfsdm")!=null && StringUtils.isNotBlank(parmMap.get("xstgfsdm").toString())){
				criteria.andEqualTo("xstgfsdm",parmMap.get("xstgfsdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zcxwnrms")!=null && StringUtils.isNotBlank(parmMap.get("zcxwnrms").toString())){
				criteria.andEqualTo("zcxwnrms",parmMap.get("zcxwnrms").toString().trim());
				flag = true;
			}
    		if(parmMap.get("hjd_xzqh")!=null && StringUtils.isNotBlank(parmMap.get("hjd_xzqh").toString())){
				criteria.andEqualTo("hjd_xzqh",parmMap.get("hjd_xzqh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sftp")!=null && StringUtils.isNotBlank(parmMap.get("sftp").toString())){
				criteria.andEqualTo("sftp",parmMap.get("sftp").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sphzxqk")!=null && StringUtils.isNotBlank(parmMap.get("sphzxqk").toString())){
				criteria.andEqualTo("sphzxqk",parmMap.get("sphzxqk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yjd_xzqh")!=null && StringUtils.isNotBlank(parmMap.get("yjd_xzqh").toString())){
				criteria.andEqualTo("yjd_xzqh",parmMap.get("yjd_xzqh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jhzazzy")!=null && StringUtils.isNotBlank(parmMap.get("jhzazzy").toString())){
				criteria.andEqualTo("jhzazzy",parmMap.get("jhzazzy").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sftsrq")!=null && StringUtils.isNotBlank(parmMap.get("sftsrq").toString())){
				criteria.andEqualTo("sftsrq",parmMap.get("sftsrq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfgwyjry")!=null && StringUtils.isNotBlank(parmMap.get("sfgwyjry").toString())){
				criteria.andEqualTo("sfgwyjry",parmMap.get("sfgwyjry").toString().trim());
				flag = true;
			}
    		if(parmMap.get("dbzh")!=null && StringUtils.isNotBlank(parmMap.get("dbzh").toString())){
				criteria.andEqualTo("dbzh",parmMap.get("dbzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("chdwdm")!=null && StringUtils.isNotBlank(parmMap.get("chdwdm").toString())){
				criteria.andEqualTo("chdwdm",parmMap.get("chdwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("tbdw_dwdm")!=null && StringUtils.isNotBlank(parmMap.get("tbdw_dwdm").toString())){
				criteria.andEqualTo("tbdw_dwdm",parmMap.get("tbdw_dwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xqn")!=null && StringUtils.isNotBlank(parmMap.get("xqn").toString())){
				criteria.andEqualTo("xqn",parmMap.get("xqn").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xqy")!=null && StringUtils.isNotBlank(parmMap.get("xqy").toString())){
				criteria.andEqualTo("xqy",parmMap.get("xqy").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zxqzrq")!=null && StringUtils.isNotBlank(parmMap.get("zxqzrq").toString())){
				criteria.andEqualTo("zxqzrq",parmMap.get("zxqzrq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gxsj")!=null && StringUtils.isNotBlank(parmMap.get("gxsj").toString())){
				criteria.andEqualTo("gxsj",parmMap.get("gxsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gxsjc")!=null && StringUtils.isNotBlank(parmMap.get("gxsjc").toString())){
				criteria.andEqualTo("gxsjc",parmMap.get("gxsjc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xgbsdw")!=null && StringUtils.isNotBlank(parmMap.get("xgbsdw").toString())){
				criteria.andEqualTo("xgbsdw",parmMap.get("xgbsdw").toString().trim());
				flag = true;
			}
    		if(parmMap.get("flwsbh")!=null && StringUtils.isNotBlank(parmMap.get("flwsbh").toString())){
				criteria.andEqualTo("flwsbh",parmMap.get("flwsbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfzf")!=null && StringUtils.isNotBlank(parmMap.get("sfzf").toString())){
				criteria.andEqualTo("sfzf",parmMap.get("sfzf").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfzt")!=null && StringUtils.isNotBlank(parmMap.get("sfzt").toString())){
				criteria.andEqualTo("sfzt",parmMap.get("sfzt").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sftsrqlx")!=null && StringUtils.isNotBlank(parmMap.get("sftsrqlx").toString())){
				criteria.andEqualTo("sftsrqlx",parmMap.get("sftsrqlx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfjhgk")!=null && StringUtils.isNotBlank(parmMap.get("sfjhgk").toString())){
				criteria.andEqualTo("sfjhgk",parmMap.get("sfjhgk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zhsj")!=null && StringUtils.isNotBlank(parmMap.get("zhsj").toString())){
				criteria.andEqualTo("zhsj",parmMap.get("zhsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zhd")!=null && StringUtils.isNotBlank(parmMap.get("zhd").toString())){
				criteria.andEqualTo("zhd",parmMap.get("zhd").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zhd_xxdq")!=null && StringUtils.isNotBlank(parmMap.get("zhd_xxdq").toString())){
				criteria.andEqualTo("zhd_xxdq",parmMap.get("zhd_xxdq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zhd_xxdz")!=null && StringUtils.isNotBlank(parmMap.get("zhd_xxdz").toString())){
				criteria.andEqualTo("zhd_xxdz",parmMap.get("zhd_xxdz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fzqk")!=null && StringUtils.isNotBlank(parmMap.get("fzqk").toString())){
				criteria.andEqualTo("fzqk",parmMap.get("fzqk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fzlx")!=null && StringUtils.isNotBlank(parmMap.get("fzlx").toString())){
				criteria.andEqualTo("fzlx",parmMap.get("fzlx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("czbs")!=null && StringUtils.isNotBlank(parmMap.get("czbs").toString())){
				criteria.andEqualTo("czbs",parmMap.get("czbs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ryly")!=null && StringUtils.isNotBlank(parmMap.get("ryly").toString())){
				criteria.andEqualTo("ryly",parmMap.get("ryly").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cszt")!=null && StringUtils.isNotBlank(parmMap.get("cszt").toString())){
				criteria.andEqualTo("cszt",parmMap.get("cszt").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gjdm")!=null && StringUtils.isNotBlank(parmMap.get("gjdm").toString())){
				criteria.andEqualTo("gjdm",parmMap.get("gjdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sdryjlbh")!=null && StringUtils.isNotBlank(parmMap.get("sdryjlbh").toString())){
				criteria.andEqualTo("sdryjlbh",parmMap.get("sdryjlbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("chdwxc")!=null && StringUtils.isNotBlank(parmMap.get("chdwxc").toString())){
				criteria.andEqualTo("chdwxc",parmMap.get("chdwxc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sftsrq_old")!=null && StringUtils.isNotBlank(parmMap.get("sftsrq_old").toString())){
				criteria.andEqualTo("sftsrq_old",parmMap.get("sftsrq_old").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sjly")!=null && StringUtils.isNotBlank(parmMap.get("sjly").toString())){
				criteria.andEqualTo("sjly",parmMap.get("sjly").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ly")!=null && StringUtils.isNotBlank(parmMap.get("ly").toString())){
				criteria.andEqualTo("ly",parmMap.get("ly").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("TbDpajSdryChxxServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("TbDpajSdryChxxServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
