package com.unis.service.jdzh.impl;

import java.util.List;
import java.util.Map;

import com.unis.common.config.DS;
import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.jdzh.TbXdryChxxMapper;
import com.unis.model.jdzh.TbXdryChxx;
import com.unis.service.jdzh.TbXdryChxxService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see TbXdryChxxService
 * </pre>
 *
 * @author gongjl
 * @version 1.0
 * @since 2020-03-10
 */
@Service("tbXdryChxxService")
public class TbXdryChxxServiceImpl extends BaseServiceImpl implements TbXdryChxxService {
	private static final Logger logger = LoggerFactory.getLogger(TbXdryChxxServiceImpl.class);
    @Autowired
    private TbXdryChxxMapper tbXdryChxxMapper;

    /**
     * @see TbXdryChxxService#insert(TbXdryChxx tbXdryChxx)
     */
    @Override
	@DS("datasource2")
    public int insert(TbXdryChxx tbXdryChxx) throws Exception {
    	if (tbXdryChxx!=null){
	        //tbXdryChxx.setChxh(TemplateUtil.genUUID());
	        //menu.setChxh(getPk("seqName","jgdm","A"));
	                
	        return tbXdryChxxMapper.insertSelective(tbXdryChxx);
    	}else{
    		logger.error("TbXdryChxxServiceImpl.insert时tbXdryChxx数据为空。");
    		throw new AppRuntimeException("TbXdryChxxServiceImpl.insert时tbXdryChxx数据为空。");
    	}        
    }

    /**
     * @see TbXdryChxxService#delete(String pk)
     */
    @Override
	@DS("datasource2")
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("TbXdryChxxServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("TbXdryChxxServiceImpl.delete时pk为空。");
    	}else{
    		return tbXdryChxxMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see TbXdryChxxService#updateByPrimaryKey(TbXdryChxx tbXdryChxx)
     */
    @Override
	@DS("datasource2")
    public int updateByPrimaryKey(TbXdryChxx tbXdryChxx) throws Exception {
        if (tbXdryChxx!=null){
        	if(StringUtils.isBlank(tbXdryChxx.getChxh())){
        		logger.error("TbXdryChxxServiceImpl.updateByPrimaryKey时tbXdryChxx.Chxh为空。");
        		throw new AppRuntimeException("TbXdryChxxServiceImpl.updateByPrimaryKey时tbXdryChxx.Chxh为空。");
        	}
	        return tbXdryChxxMapper.updateByPrimaryKeySelective(tbXdryChxx);
    	}else{
    		logger.error("TbXdryChxxServiceImpl.updateByPrimaryKey时tbXdryChxx数据为空。");
    		throw new AppRuntimeException("TbXdryChxxServiceImpl.updateByPrimaryKey时tbXdryChxx数据为空。");
    	}
    }
    /**
     * @see TbXdryChxxService#queryTbXdryChxxByPrimaryKey(String pk)
     */
    @Override
	@DS("datasource2")
    public TbXdryChxx queryTbXdryChxxByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("TbXdryChxxServiceImpl.queryTbXdryChxxByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("TbXdryChxxServiceImpl.queryTbXdryChxxByPrimaryKey时pk为空。");
    	}else{
    		return tbXdryChxxMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see TbXdryChxxService#queryAsObject(TbXdryChxx tbXdryChxx)
     */
    @Override
	@DS("datasource2")
    public TbXdryChxx queryAsObject(TbXdryChxx tbXdryChxx) throws Exception {
        if (tbXdryChxx!=null){
	        return tbXdryChxxMapper.selectOne(tbXdryChxx);
    	}else{
    		logger.error("TbXdryChxxServiceImpl.queryAsObject时tbXdryChxx数据为空。");
    		throw new AppRuntimeException("TbXdryChxxServiceImpl.queryAsObject时tbXdryChxx数据为空。");
    	}
    }
    
    /**
     * @see TbXdryChxxService#queryCountByExample(Example example)
     */
    @Override
	@DS("datasource2")
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return tbXdryChxxMapper.selectCountByExample(example);
    	}else{
    		logger.error("TbXdryChxxServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("TbXdryChxxServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see TbXdryChxxService#queryListByExample(Example example)
     */
    @Override
	@DS("datasource2")
    public List<TbXdryChxx> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return tbXdryChxxMapper.selectByExample(example);
    	}else{
    		logger.error("TbXdryChxxServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("TbXdryChxxServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see TbXdryChxxService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
	@DS("datasource2")
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(tbXdryChxxMapper.selectByExample(example));
    	}else{
    		logger.error("TbXdryChxxServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("TbXdryChxxServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see TbXdryChxxService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
	@DS("datasource2")
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(TbXdryChxx.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("chxh")!=null && StringUtils.isNotBlank(parmMap.get("chxh").toString())){
				criteria.andEqualTo("chxh",parmMap.get("chxh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xyrbh")!=null && StringUtils.isNotBlank(parmMap.get("xyrbh").toString())){
				criteria.andEqualTo("xyrbh",parmMap.get("xyrbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("chrq")!=null && StringUtils.isNotBlank(parmMap.get("chrq").toString())){
				criteria.andEqualTo("chrq",parmMap.get("chrq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("chdw_xzqhdm")!=null && StringUtils.isNotBlank(parmMap.get("chdw_xzqhdm").toString())){
				criteria.andEqualTo("chdw_xzqhdm",parmMap.get("chdw_xzqhdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("chdw_qhnxxdz")!=null && StringUtils.isNotBlank(parmMap.get("chdw_qhnxxdz").toString())){
				criteria.andEqualTo("chdw_qhnxxdz",parmMap.get("chdw_qhnxxdz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("chdw_dwmc")!=null && StringUtils.isNotBlank(parmMap.get("chdw_dwmc").toString())){
				criteria.andEqualTo("chdw_dwmc",parmMap.get("chdw_dwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xdcszldm")!=null && StringUtils.isNotBlank(parmMap.get("xdcszldm").toString())){
				criteria.andEqualTo("xdcszldm",parmMap.get("xdcszldm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bclydpzl")!=null && StringUtils.isNotBlank(parmMap.get("bclydpzl").toString())){
				criteria.andEqualTo("bclydpzl",parmMap.get("bclydpzl").toString().trim());
				flag = true;
			}
    		if(parmMap.get("dplydm")!=null && StringUtils.isNotBlank(parmMap.get("dplydm").toString())){
				criteria.andEqualTo("dplydm",parmMap.get("dplydm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("wfss_jyaq")!=null && StringUtils.isNotBlank(parmMap.get("wfss_jyaq").toString())){
				criteria.andEqualTo("wfss_jyaq",parmMap.get("wfss_jyaq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xdrychlxdm")!=null && StringUtils.isNotBlank(parmMap.get("xdrychlxdm").toString())){
				criteria.andEqualTo("xdrychlxdm",parmMap.get("xdrychlxdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xdrychlydm")!=null && StringUtils.isNotBlank(parmMap.get("xdrychlydm").toString())){
				criteria.andEqualTo("xdrychlydm",parmMap.get("xdrychlydm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("njjgdm")!=null && StringUtils.isNotBlank(parmMap.get("njjgdm").toString())){
				criteria.andEqualTo("njjgdm",parmMap.get("njjgdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xdfsdm")!=null && StringUtils.isNotBlank(parmMap.get("xdfsdm").toString())){
				criteria.andEqualTo("xdfsdm",parmMap.get("xdfsdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xfcs")!=null && StringUtils.isNotBlank(parmMap.get("xfcs").toString())){
				criteria.andEqualTo("xfcs",parmMap.get("xfcs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ylcschxh")!=null && StringUtils.isNotBlank(parmMap.get("ylcschxh").toString())){
				criteria.andEqualTo("ylcschxh",parmMap.get("ylcschxh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ylcsmc")!=null && StringUtils.isNotBlank(parmMap.get("ylcsmc").toString())){
				criteria.andEqualTo("ylcsmc",parmMap.get("ylcsmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sftsrq")!=null && StringUtils.isNotBlank(parmMap.get("sftsrq").toString())){
				criteria.andEqualTo("sftsrq",parmMap.get("sftsrq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("tsrqzl")!=null && StringUtils.isNotBlank(parmMap.get("tsrqzl").toString())){
				criteria.andEqualTo("tsrqzl",parmMap.get("tsrqzl").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxjg")!=null && StringUtils.isNotBlank(parmMap.get("yxjg").toString())){
				criteria.andEqualTo("yxjg",parmMap.get("yxjg").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfjb")!=null && StringUtils.isNotBlank(parmMap.get("sfjb").toString())){
				criteria.andEqualTo("sfjb",parmMap.get("sfjb").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jblx")!=null && StringUtils.isNotBlank(parmMap.get("jblx").toString())){
				criteria.andEqualTo("jblx",parmMap.get("jblx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfmcch")!=null && StringUtils.isNotBlank(parmMap.get("sfmcch").toString())){
				criteria.andEqualTo("sfmcch",parmMap.get("sfmcch").toString().trim());
				flag = true;
			}
    		if(parmMap.get("chcs")!=null && StringUtils.isNotBlank(parmMap.get("chcs").toString())){
				criteria.andEqualTo("chcs",parmMap.get("chcs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("tbr_xm")!=null && StringUtils.isNotBlank(parmMap.get("tbr_xm").toString())){
				criteria.andEqualTo("tbr_xm",parmMap.get("tbr_xm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("tbr_lxdh")!=null && StringUtils.isNotBlank(parmMap.get("tbr_lxdh").toString())){
				criteria.andEqualTo("tbr_lxdh",parmMap.get("tbr_lxdh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("shr_xm")!=null && StringUtils.isNotBlank(parmMap.get("shr_xm").toString())){
				criteria.andEqualTo("shr_xm",parmMap.get("shr_xm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("tbdw_dwmc")!=null && StringUtils.isNotBlank(parmMap.get("tbdw_dwmc").toString())){
				criteria.andEqualTo("tbdw_dwmc",parmMap.get("tbdw_dwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("tbrq")!=null && StringUtils.isNotBlank(parmMap.get("tbrq").toString())){
				criteria.andEqualTo("tbrq",parmMap.get("tbrq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrr_xm")!=null && StringUtils.isNotBlank(parmMap.get("lrr_xm").toString())){
				criteria.andEqualTo("lrr_xm",parmMap.get("lrr_xm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrdw_dwmc")!=null && StringUtils.isNotBlank(parmMap.get("lrdw_dwmc").toString())){
				criteria.andEqualTo("lrdw_dwmc",parmMap.get("lrdw_dwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrsj")!=null && StringUtils.isNotBlank(parmMap.get("lrsj").toString())){
				criteria.andEqualTo("lrsj",parmMap.get("lrsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bz")!=null && StringUtils.isNotBlank(parmMap.get("bz").toString())){
				criteria.andEqualTo("bz",parmMap.get("bz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rybs")!=null && StringUtils.isNotBlank(parmMap.get("rybs").toString())){
				criteria.andEqualTo("rybs",parmMap.get("rybs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xgbsdw")!=null && StringUtils.isNotBlank(parmMap.get("xgbsdw").toString())){
				criteria.andEqualTo("xgbsdw",parmMap.get("xgbsdw").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gsdw")!=null && StringUtils.isNotBlank(parmMap.get("gsdw").toString())){
				criteria.andEqualTo("gsdw",parmMap.get("gsdw").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gxsjc")!=null && StringUtils.isNotBlank(parmMap.get("gxsjc").toString())){
				criteria.andEqualTo("gxsjc",parmMap.get("gxsjc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("czbs")!=null && StringUtils.isNotBlank(parmMap.get("czbs").toString())){
				criteria.andEqualTo("czbs",parmMap.get("czbs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("inoutbz")!=null && StringUtils.isNotBlank(parmMap.get("inoutbz").toString())){
				criteria.andEqualTo("inoutbz",parmMap.get("inoutbz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gxsj")!=null && StringUtils.isNotBlank(parmMap.get("gxsj").toString())){
				criteria.andEqualTo("gxsj",parmMap.get("gxsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("chdw_dwdm")!=null && StringUtils.isNotBlank(parmMap.get("chdw_dwdm").toString())){
				criteria.andEqualTo("chdw_dwdm",parmMap.get("chdw_dwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrdw_dwdm")!=null && StringUtils.isNotBlank(parmMap.get("lrdw_dwdm").toString())){
				criteria.andEqualTo("lrdw_dwdm",parmMap.get("lrdw_dwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("tbdw_dwdm")!=null && StringUtils.isNotBlank(parmMap.get("tbdw_dwdm").toString())){
				criteria.andEqualTo("tbdw_dwdm",parmMap.get("tbdw_dwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xyrlb")!=null && StringUtils.isNotBlank(parmMap.get("xyrlb").toString())){
				criteria.andEqualTo("xyrlb",parmMap.get("xyrlb").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fjmc")!=null && StringUtils.isNotBlank(parmMap.get("fjmc").toString())){
				criteria.andEqualTo("fjmc",parmMap.get("fjmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("wlgjfjxh")!=null && StringUtils.isNotBlank(parmMap.get("wlgjfjxh").toString())){
				criteria.andEqualTo("wlgjfjxh",parmMap.get("wlgjfjxh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("chjclx")!=null && StringUtils.isNotBlank(parmMap.get("chjclx").toString())){
				criteria.andEqualTo("chjclx",parmMap.get("chjclx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("chjc_dwdm")!=null && StringUtils.isNotBlank(parmMap.get("chjc_dwdm").toString())){
				criteria.andEqualTo("chjc_dwdm",parmMap.get("chjc_dwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("chjc_dwmc")!=null && StringUtils.isNotBlank(parmMap.get("chjc_dwmc").toString())){
				criteria.andEqualTo("chjc_dwmc",parmMap.get("chjc_dwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("chsnl")!=null && StringUtils.isNotBlank(parmMap.get("chsnl").toString())){
				criteria.andEqualTo("chsnl",parmMap.get("chsnl").toString().trim());
				flag = true;
			}
    		if(parmMap.get("tsywms")!=null && StringUtils.isNotBlank(parmMap.get("tsywms").toString())){
				criteria.andEqualTo("tsywms",parmMap.get("tsywms").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sftsyw")!=null && StringUtils.isNotBlank(parmMap.get("sftsyw").toString())){
				criteria.andEqualTo("sftsyw",parmMap.get("sftsyw").toString().trim());
				flag = true;
			}
    		if(parmMap.get("czqk")!=null && StringUtils.isNotBlank(parmMap.get("czqk").toString())){
				criteria.andEqualTo("czqk",parmMap.get("czqk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("czqkmc")!=null && StringUtils.isNotBlank(parmMap.get("czqkmc").toString())){
				criteria.andEqualTo("czqkmc",parmMap.get("czqkmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bczhzqgkxzdm")!=null && StringUtils.isNotBlank(parmMap.get("bczhzqgkxzdm").toString())){
				criteria.andEqualTo("bczhzqgkxzdm",parmMap.get("bczhzqgkxzdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ly")!=null && StringUtils.isNotBlank(parmMap.get("ly").toString())){
				criteria.andEqualTo("ly",parmMap.get("ly").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("TbXdryChxxServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("TbXdryChxxServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
