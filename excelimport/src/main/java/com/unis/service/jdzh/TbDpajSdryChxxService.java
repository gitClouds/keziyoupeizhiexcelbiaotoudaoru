package com.unis.service.jdzh;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import com.unis.model.jdzh.TbDpajSdryChxx;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.unis.service.BaseService;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;

/**
 * <pre>
 * TODO
 * </pre>
 *
 * @author gongjl
 * @version 1.0
 * @since 2020-03-10
 */
public interface TbDpajSdryChxxService extends BaseService {

    /**
     * 新增TbDpajSdryChxx实例
     * 
     * @param tbDpajSdryChxx
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int insert(TbDpajSdryChxx tbDpajSdryChxx) throws Exception;

    /**
     * 删除TbDpajSdryChxx实例
     * 
     * @param pk
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int delete(String pk) throws Exception;

    /**
     * 更新TbDpajSdryChxx实例
     * 
     * @param tbDpajSdryChxx
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int updateByPrimaryKey(TbDpajSdryChxx tbDpajSdryChxx) throws Exception;
    /**
     * 根据pk主键获取对象
     * 
     * @param pk
     * @throws Exception
     */
    TbDpajSdryChxx queryTbDpajSdryChxxByPrimaryKey(String pk) throws Exception;

    /**
     * 查询TbDpajSdryChxx实例
     * 
     * @param tbDpajSdryChxx
     * @throws Exception
     */
    TbDpajSdryChxx queryAsObject(TbDpajSdryChxx tbDpajSdryChxx) throws Exception;
    
    	
	/**
     * 根据条件获取条数
     * 
     * @param example
     * @throws Exception
     */
	int queryCountByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合
     * 
     * @param example
     * @throws Exception
     */
	List<TbDpajSdryChxx> queryListByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合(分页),需在example 设定排序
     * 
     * @param example
     * @param pageNum
     * @param pageSize
     * @throws Exception
     */
	PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception;

	/**
     * 根据入参获取数据集合(分页)
     * @param parmMap
     * @param pageNum
     * @param pageSize
     * @return
     * @throws Exception
     */
    ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception;
    
    

}
