package com.unis.service.jdzh;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import com.unis.model.jdzh.TbDpajSdryJbxx;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.unis.service.BaseService;
import com.unis.wsClient.bean.WSsdryxx;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;

/**
 * <pre>
 * TODO
 * </pre>
 *
 * @author gongjl
 * @version 1.0
 * @since 2020-03-05
 */
public interface TbDpajSdryJbxxService extends BaseService {

    /**
     * 新增TbDpajSdryJbxx实例
     * 
     * @param tbDpajSdryJbxx
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int insert(TbDpajSdryJbxx tbDpajSdryJbxx) throws Exception;

    /**
     * 删除TbDpajSdryJbxx实例
     * 
     * @param pk
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int delete(String pk) throws Exception;

    /**
     * 更新TbDpajSdryJbxx实例
     * 
     * @param tbDpajSdryJbxx
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int updateByPrimaryKey(TbDpajSdryJbxx tbDpajSdryJbxx) throws Exception;
    /**
     * 根据pk主键获取对象
     * 
     * @param pk
     * @throws Exception
     */
    TbDpajSdryJbxx queryTbDpajSdryJbxxByPrimaryKey(String pk) throws Exception;

    /**
     * 查询TbDpajSdryJbxx实例
     * 
     * @param tbDpajSdryJbxx
     * @throws Exception
     */
    TbDpajSdryJbxx queryAsObject(TbDpajSdryJbxx tbDpajSdryJbxx) throws Exception;
    
    	
	/**
     * 根据条件获取条数
     * 
     * @param example
     * @throws Exception
     */
	int queryCountByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合
     * 
     * @param example
     * @throws Exception
     */
	List<TbDpajSdryJbxx> queryListByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合(分页),需在example 设定排序
     * 
     * @param example
     * @param pageNum
     * @param pageSize
     * @throws Exception
     */
	PageInfo queryPageInfoByExample(Example example, int pageNum, int pageSize) throws Exception;

	/**
     * 根据入参获取数据集合(分页)
     * @param parmMap
     * @param pageNum
     * @param pageSize
     * @return
     * @throws Exception
     */
    ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception;

    ResultDto querySdry(Map parmMap, int pageNum, int pageSize) throws  Exception;

    /**
     * 除去lx字段查找
     * @param pk
     * @return
     * @throws Exception
     */
    TbDpajSdryJbxx queryByPk(String pk) throws  Exception;

	List<String> sdryTask(WSsdryxx wSsdryxx,String zhpk,Short cj,String zh)throws Exception;
}
