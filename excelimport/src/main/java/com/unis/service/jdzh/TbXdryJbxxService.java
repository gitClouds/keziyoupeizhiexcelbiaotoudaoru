package com.unis.service.jdzh;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import com.unis.model.jdzh.TbXdryJbxx;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.unis.service.BaseService;
import com.unis.wsClient.bean.WSxdryxx;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;

/**
 * <pre>
 * TODO
 * </pre>
 *
 * @author gongjl
 * @version 1.0
 * @since 2020-03-05
 */
public interface TbXdryJbxxService extends BaseService {

    /**
     * 新增TbXdryJbxx实例
     * 
     * @param tbXdryJbxx
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int insert(TbXdryJbxx tbXdryJbxx) throws Exception;

    /**
     * 删除TbXdryJbxx实例
     * 
     * @param pk
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int delete(String pk) throws Exception;

    /**
     * 更新TbXdryJbxx实例
     * 
     * @param tbXdryJbxx
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int updateByPrimaryKey(TbXdryJbxx tbXdryJbxx) throws Exception;
    /**
     * 根据pk主键获取对象
     * 
     * @param pk
     * @throws Exception
     */
    TbXdryJbxx queryTbXdryJbxxByPrimaryKey(String pk) throws Exception;

    /**
     * 查询TbXdryJbxx实例
     * 
     * @param tbXdryJbxx
     * @throws Exception
     */
    TbXdryJbxx queryAsObject(TbXdryJbxx tbXdryJbxx) throws Exception;
    
    	
	/**
     * 根据条件获取条数
     * 
     * @param example
     * @throws Exception
     */
	int queryCountByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合
     * 
     * @param example
     * @throws Exception
     */
	List<TbXdryJbxx> queryListByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合(分页),需在example 设定排序
     * 
     * @param example
     * @param pageNum
     * @param pageSize
     * @throws Exception
     */
	PageInfo queryPageInfoByExample(Example example, int pageNum, int pageSize) throws Exception;

	/**
     * 根据入参获取数据集合(分页)
     * @param parmMap
     * @param pageNum
     * @param pageSize
     * @return
     * @throws Exception
     */
    ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception;

    ResultDto queryXdry(Map parmMap, int pageNum, int pageSize) throws  Exception;


    public List<String> xdryTask(WSxdryxx xds,String zhpk,Short cj,String zh) throws Exception;
    
    

}
