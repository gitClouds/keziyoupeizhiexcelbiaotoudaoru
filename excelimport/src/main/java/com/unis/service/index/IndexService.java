package com.unis.service.index;

import com.unis.service.BaseService;

import java.util.List;
import java.util.Map;

/**
 * Created by xuk
 * on 2019/7/17.
 */
public interface IndexService extends BaseService {

    Map<String,Object> statisticsFrozen();

    Map<String,Object> statisticsQuery();

    Map<String,Object> statisticsJjd();

    List<Map<String, String>> statisticsJjdByAjlb();

    List<Map<String,String>> statisticsJjd(String rksj);
}
