package com.unis.service.index.impl;

import com.unis.common.secure.authc.UserInfo;
import com.unis.common.util.TimeUtil;
import com.unis.mapper.fzzx.JjdMapper;
import com.unis.mapper.znzf.SfMxQqMapper;
import com.unis.mapper.znzf.SfZfQqMapper;
import com.unis.mapper.znzf.YhkMxQqMapper;
import com.unis.mapper.znzf.YhkZfQqMapper;
import com.unis.service.impl.BaseServiceImpl;
import com.unis.service.index.IndexService;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by xuk
 * on 2019/7/17.
 */
@Service("indexService")
public class IndexServiceImpl extends BaseServiceImpl implements IndexService {
    private static final Logger logger = LoggerFactory.getLogger(IndexServiceImpl.class);

    @Autowired
    private SfZfQqMapper sfZfQqMapper;
    @Autowired
    private YhkZfQqMapper yhkZfQqMapper;
    @Autowired
    private YhkMxQqMapper yhkMxQqMapper;
    @Autowired
    private SfMxQqMapper sfMxQqMapper;
    @Autowired
    private JjdMapper jjdMapper;
    @Override
    public Map<String, Object> statisticsFrozen() {
        Map<String,Object> resultMap = new HashMap<>();
        Subject currentUser = SecurityUtils.getSubject();
        List<Map<String,String>> todayList = new ArrayList<>();
        List<Map<String,String>> weekList = new ArrayList<>();
        List<Map<String,String>> monthList = new ArrayList<>();

        List<Map<String,BigDecimal>> sfZfList = null;
        List<Map<String,BigDecimal>> yhkZfList = null;
        if (currentUser.hasRole("_SYSTEMADMIN")){
            yhkZfList = yhkZfQqMapper.indexStatistics("");
            sfZfList = sfZfQqMapper.indexStatistics("");
        }else{
            UserInfo user = (UserInfo) currentUser.getPrincipal();
            yhkZfList = yhkZfQqMapper.indexStatistics(user.getJgdm());
            sfZfList = sfZfQqMapper.indexStatistics(user.getJgdm());
        }
        statisticsFrozenListAdd("银行卡",todayList,weekList,monthList,yhkZfList);
        statisticsFrozenListAdd("第三方",todayList,weekList,monthList,sfZfList);

        resultMap.put("today",todayList);
        resultMap.put("week",weekList);
        resultMap.put("month",monthList);
        return resultMap;
    }

    @Override
    public Map<String, Object> statisticsQuery() {
        Map<String,Object> resultMap = new HashMap<>();
        Subject currentUser = SecurityUtils.getSubject();
        List<Map<String,String>> todayList = new ArrayList<>();
        List<Map<String,String>> weekList = new ArrayList<>();
        List<Map<String,String>> monthList = new ArrayList<>();

        List<Map<String,BigDecimal>> sfQqList = null;
        List<Map<String,BigDecimal>> yhkMxList = null;
        if (currentUser.hasRole("_SYSTEMADMIN")){
            yhkMxList = yhkMxQqMapper.indexStatistics("");
            sfQqList = sfMxQqMapper.indexStatistics("");
        }else{
            UserInfo user = (UserInfo) currentUser.getPrincipal();
            yhkMxList = yhkMxQqMapper.indexStatistics(user.getJgdm());
            sfQqList = sfMxQqMapper.indexStatistics(user.getJgdm());
        }
        statisticsFrozenListAdd("银行卡",todayList,weekList,monthList,yhkMxList);
        statisticsFrozenListAdd("第三方",todayList,weekList,monthList,sfQqList);
        resultMap.put("today",todayList);
        resultMap.put("week",weekList);
        resultMap.put("month",monthList);
        return resultMap;
    }

    @Override
    public Map<String, Object> statisticsJjd() {
        Map<String,Object> resultMap = new HashMap<>();
        Subject currentUser = SecurityUtils.getSubject();
        List<Map<String,String>> list = null;
        List<Map<String,String>> weekList = new ArrayList<>();
        List<Map<String,String>> monthList = new ArrayList<>();

        if (currentUser.hasRole("_SYSTEMADMIN")){
            list = jjdMapper.indexStatistics("");
        }else{
            UserInfo user = (UserInfo) currentUser.getPrincipal();
            list = jjdMapper.indexStatistics(user.getJgdm());
        }
        Date nowDate = new Date();
        for (int i=29;i>=0;i--){
            String thisDateStr = TimeUtil.fmtDate(TimeUtil.addDay(nowDate,-i),"MM-dd");
            Map<String,String> map = new HashMap<>();
            map.put("name",thisDateStr);
            map.put("value",0+"");
            map.put("noinit",0+"");
            map.put("issucc",0+"");
            map.put("saje",0+"");
            if (list!=null && !list.isEmpty()&& list.size()>0){
                Map<String,String> dataMap = list.stream().filter(stringStringMap -> StringUtils.equalsIgnoreCase(thisDateStr,stringStringMap.get("CJSJ"))).findFirst().orElse(null);
                if (dataMap!=null && !dataMap.isEmpty() && dataMap.size()>0){
//                    map.put("value",dataMap.get("ZS"));
                    map.put("noinit",dataMap.get("NOINIT"));
                    map.put("issucc",dataMap.get("ISSUCC"));
                    map.put("saje",dataMap.get("SAJE"));
                }
            }
            monthList.add(map);
            if (i<7){
                weekList.add(map);
            }
        }
        resultMap.put("week",weekList);
        resultMap.put("month",monthList);
        return resultMap;
    }

    @Override
    public List<Map<String, String>> statisticsJjdByAjlb() {
        Subject currentUser = SecurityUtils.getSubject();
        List<Map<String,String>> list = null;

        if (currentUser.hasRole("_SYSTEMADMIN")){
            list = jjdMapper.indexStatisticsByAjlb("");
        }else{
            UserInfo user = (UserInfo) currentUser.getPrincipal();
            list = jjdMapper.indexStatisticsByAjlb(user.getJgdm());
        }
        return list;
    }

    @Override
    public List<Map<String, String>> statisticsJjd(String rksj) {
        return jjdMapper.indexStatisticsByRksj(rksj);
    }

    private Map<String,BigDecimal> checkStatisticsFrozenMap(Map<String,BigDecimal> map){
        if(map==null || map.isEmpty() || map.size()<1){
            map = new HashMap<>();
            map.put("ZS",new BigDecimal(0));
            map.put("WFK",new BigDecimal(0));
            map.put("SUCC",new BigDecimal(0));
            map.put("NSUCC",new BigDecimal(0));
        }
        return map;
    }
    private void statisticsFrozenListAdd(String name,List<Map<String,String>> todayList,List<Map<String,String>> weekList,List<Map<String,String>> monthList, List<Map<String,BigDecimal>> list){
        Map<String,BigDecimal> todayMap = null;
        Map<String,BigDecimal> weekMap = null;
        Map<String,BigDecimal> monthMap = null;

        for (Map<String,BigDecimal> m : list){
            if (m.get("DTYPE").intValue() == 1){
                todayMap = m;
            }else if (m.get("DTYPE").intValue() == 2){
                weekMap = m;
            }else if (m.get("DTYPE").intValue() == 3){
                monthMap = m;
            }
        }
        todayMap = checkStatisticsFrozenMap(todayMap);
        weekMap = checkStatisticsFrozenMap(weekMap);
        monthMap = checkStatisticsFrozenMap(monthMap);

        Map<String,String> tMap = new HashMap<>();
        tMap.put("name",name);
        tMap.put("value",todayMap.get("ZS").intValue()+"");
        tMap.put("noresp",todayMap.get("WFK").intValue()+"");
        tMap.put("succ",todayMap.get("SUCC").intValue()+"");
        tMap.put("nsucc",todayMap.get("NSUCC").intValue()+"");
        todayList.add(tMap);

        Map<String,String> wMap = new HashMap<>();
        wMap.put("name",name);
        wMap.put("value",(todayMap.get("ZS").intValue()+weekMap.get("ZS").intValue())+"");
        wMap.put("noresp",(todayMap.get("WFK").intValue()+weekMap.get("WFK").intValue())+"");
        wMap.put("succ",(todayMap.get("SUCC").intValue()+weekMap.get("SUCC").intValue())+"");
        wMap.put("nsucc",(todayMap.get("NSUCC").intValue()+weekMap.get("NSUCC").intValue())+"");
        weekList.add(wMap);

        Map<String,String> mMap = new HashMap<>();
        mMap.put("name",name);
        mMap.put("value",(todayMap.get("ZS").intValue()+weekMap.get("ZS").intValue()+monthMap.get("ZS").intValue())+"");
        mMap.put("noresp",(todayMap.get("WFK").intValue()+weekMap.get("WFK").intValue()+monthMap.get("WFK").intValue())+"");
        mMap.put("succ",(todayMap.get("SUCC").intValue()+weekMap.get("SUCC").intValue()+monthMap.get("SUCC").intValue())+"");
        mMap.put("nsucc",(todayMap.get("NSUCC").intValue()+weekMap.get("NSUCC").intValue()+monthMap.get("NSUCC").intValue())+"");
        monthList.add(mMap);
    }

}
