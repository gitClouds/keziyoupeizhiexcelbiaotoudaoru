package com.unis.service.wxjyyjmx;

import java.util.Map;

import com.unis.dto.ResultDto;
import com.unis.model.wxjyyjmx.VZhDfzfzhsjtjx;
import com.unis.model.wxjyyjmx.VZhWfXjjh;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.unis.service.BaseService;

/**
 * <pre>
 * TODO
 * </pre>
 *
 * @author gongjl
 * @version 1.0
 * @since 2020-05-06
 */
public interface VZhDfzfzhsjtjxService extends BaseService {

	/**
     * 根据条件获取数据集合(分页),需在example 设定排序
     * 
     * @param example
     * @param pageNum
     * @param pageSize
     * @throws Exception
     */
	PageInfo queryPageInfoByExample(Example example, int pageNum, int pageSize) throws Exception;

	/**
     * 根据入参获取数据集合(分页)
     * @param parmMap
     * @param pageNum
     * @param pageSize
     * @return
     * @throws Exception
     */
    ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception;

    VZhDfzfzhsjtjx queryClob(String zh) throws Exception;


}
