package com.unis.service.wxjyyjmx.impl;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.unis.dto.ResultDto;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.wxjyyjmx.VZhDfzfzhZjtjMapper;
import com.unis.model.wxjyyjmx.VZhDfzfzhZjtj;
import com.unis.service.wxjyyjmx.VZhDfzfzhZjtjService;
import com.unis.common.config.DS;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see VZhDfzfzhZjtjService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2020-05-06
 */
@Service("vZhDfzfzhZjtjService")
public class VZhDfzfzhZjtjServiceImpl extends BaseServiceImpl implements VZhDfzfzhZjtjService {
	private static final Logger logger = LoggerFactory.getLogger(VZhDfzfzhZjtjServiceImpl.class);
    @Autowired
    private VZhDfzfzhZjtjMapper vZhDfzfzhZjtjMapper;

    /**
     * @see VZhDfzfzhZjtjService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(vZhDfzfzhZjtjMapper.selectByExample(example));
    	}else{
    		logger.error("VZhDfzfzhZjtjServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("VZhDfzfzhZjtjServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see VZhDfzfzhZjtjService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    @DS("datasource2")
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(VZhDfzfzhZjtj.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("dfzfzh")!=null && StringUtils.isNotBlank(parmMap.get("dfzfzh").toString())){
				criteria.andEqualTo("dfzfzh",parmMap.get("dfzfzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("df_wxh")!=null && StringUtils.isNotBlank(parmMap.get("df_wxh").toString())){
				criteria.andEqualTo("df_wxh",parmMap.get("df_wxh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("df_khxm_2cc")!=null && StringUtils.isNotBlank(parmMap.get("df_khxm_2cc").toString())){
				criteria.andEqualTo("df_khxm_2cc",parmMap.get("df_khxm_2cc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("df_khzj_2cc")!=null && StringUtils.isNotBlank(parmMap.get("df_khzj_2cc").toString())){
				criteria.andEqualTo("df_khzj_2cc",parmMap.get("df_khzj_2cc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("df_bdsjh_2cc")!=null && StringUtils.isNotBlank(parmMap.get("df_bdsjh_2cc").toString())){
				criteria.andEqualTo("df_bdsjh_2cc",parmMap.get("df_bdsjh_2cc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("df_gwhj")!=null && StringUtils.isNotBlank(parmMap.get("df_gwhj").toString())){
				criteria.andEqualTo("df_gwhj",parmMap.get("df_gwhj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("czcs")!=null && StringUtils.isNotBlank(parmMap.get("czcs").toString())){
				criteria.andEqualTo("czcs",parmMap.get("czcs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("czje")!=null && StringUtils.isNotBlank(parmMap.get("czje").toString())){
				criteria.andEqualTo("czje",parmMap.get("czje").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rzcs")!=null && StringUtils.isNotBlank(parmMap.get("rzcs").toString())){
				criteria.andEqualTo("rzcs",parmMap.get("rzcs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rzje")!=null && StringUtils.isNotBlank(parmMap.get("rzje").toString())){
				criteria.andEqualTo("rzje",parmMap.get("rzje").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zjgxrs")!=null && StringUtils.isNotBlank(parmMap.get("zjgxrs").toString())){
				criteria.andEqualTo("zjgxrs",parmMap.get("zjgxrs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sdzjgxrs")!=null && StringUtils.isNotBlank(parmMap.get("sdzjgxrs").toString())){
				criteria.andEqualTo("sdzjgxrs",parmMap.get("sdzjgxrs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("dfzhrzrs")!=null && StringUtils.isNotBlank(parmMap.get("dfzhrzrs").toString())){
				criteria.andEqualTo("dfzhrzrs",parmMap.get("dfzhrzrs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("dfzhczrs")!=null && StringUtils.isNotBlank(parmMap.get("dfzhczrs").toString())){
				criteria.andEqualTo("dfzhczrs",parmMap.get("dfzhczrs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("dfrzsdrs")!=null && StringUtils.isNotBlank(parmMap.get("dfrzsdrs").toString())){
				criteria.andEqualTo("dfrzsdrs",parmMap.get("dfrzsdrs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("dfczsdrs")!=null && StringUtils.isNotBlank(parmMap.get("dfczsdrs").toString())){
				criteria.andEqualTo("dfczsdrs",parmMap.get("dfczsdrs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("mgsdl")!=null && StringUtils.isNotBlank(parmMap.get("mgsdl").toString())){
				criteria.andEqualTo("mgsdl",parmMap.get("mgsdl").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zsjyl")!=null && StringUtils.isNotBlank(parmMap.get("zsjyl").toString())){
				criteria.andEqualTo("zsjyl",parmMap.get("zsjyl").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("VZhDfzfzhZjtjServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("VZhDfzfzhZjtjServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
