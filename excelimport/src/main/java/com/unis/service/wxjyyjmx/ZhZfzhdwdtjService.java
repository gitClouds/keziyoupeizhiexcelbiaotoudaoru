package com.unis.service.wxjyyjmx;

import com.github.pagehelper.PageInfo;
import com.unis.dto.ResultDto;
import com.unis.service.BaseService;
import tk.mybatis.mapper.entity.Example;

import java.util.Map;

/**
 * <pre>
 * TODO
 * </pre>
 *
 * @author Qx
 * @version 1.0
 * @since 2020-05-09
 */
public interface ZhZfzhdwdtjService extends BaseService {

    /**
     * 根据条件获取数据集合(分页),需在example 设定排序
     *
     * @param example
     * @param pageNum
     * @param pageSize
     * @throws Exception
     */
    PageInfo queryPageInfoByExample(Example example, int pageNum, int pageSize) throws Exception;

    /**
     * 根据入参获取数据集合(分页)
     *
     * @param parmMap
     * @param pageNum
     * @param pageSize
     * @return
     * @throws Exception
     */
    ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws Exception;


    ResultDto queryListByPageDr(Map<?, ?> paramMap, int pageNumber, int pageSize) throws Exception;
}
