package com.unis.service.wxjyyjmx.impl;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.unis.dto.ResultDto;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.wxjyyjmx.VZhWfCrzjhTjMapper;
import com.unis.model.wxjyyjmx.VZhWfCrzjhTj;
import com.unis.service.wxjyyjmx.VZhWfCrzjhTjService;
import com.unis.common.config.DS;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see VZhWfCrzjhTjService
 * </pre>
 *
 * @author Qx
 * @version 1.0
 * @since 2020-05-06
 */
@Service("vZhWfCrzjhTjService")
public class VZhWfCrzjhTjServiceImpl extends BaseServiceImpl implements VZhWfCrzjhTjService {
	private static final Logger logger = LoggerFactory.getLogger(VZhWfCrzjhTjServiceImpl.class);
    @Autowired
    private VZhWfCrzjhTjMapper vZhWfCrzjhTjMapper;

    /**
     * @see VZhWfCrzjhTjService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(vZhWfCrzjhTjMapper.selectByExample(example));
    	}else{
    		logger.error("VZhWfCrzjhTjServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("VZhWfCrzjhTjServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see VZhWfCrzjhTjService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    @DS("datasource2")
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(VZhWfCrzjhTj.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("wfzfzh")!=null && StringUtils.isNotBlank(parmMap.get("wfzfzh").toString())){
				criteria.andEqualTo("wfzfzh",parmMap.get("wfzfzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rz_wf_wxh")!=null && StringUtils.isNotBlank(parmMap.get("rz_wf_wxh").toString())){
				criteria.andEqualTo("rz_wf_wxh",parmMap.get("rz_wf_wxh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rz_wf_wxzh_x1")!=null && StringUtils.isNotBlank(parmMap.get("rz_wf_wxzh_x1").toString())){
				criteria.andEqualTo("rz_wf_wxzh_x1",parmMap.get("rz_wf_wxzh_x1").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rz_wf_gwhj")!=null && StringUtils.isNotBlank(parmMap.get("rz_wf_gwhj").toString())){
				criteria.andEqualTo("rz_wf_gwhj",parmMap.get("rz_wf_gwhj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rz_jydss")!=null && StringUtils.isNotBlank(parmMap.get("rz_jydss").toString())){
				criteria.andEqualTo("rz_jydss",parmMap.get("rz_jydss").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rz_jycs")!=null && StringUtils.isNotBlank(parmMap.get("rz_jycs").toString())){
				criteria.andEqualTo("rz_jycs",parmMap.get("rz_jycs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rz_jyje")!=null && StringUtils.isNotBlank(parmMap.get("rz_jyje").toString())){
				criteria.andEqualTo("rz_jyje",parmMap.get("rz_jyje").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rz_sdqkje")!=null && StringUtils.isNotBlank(parmMap.get("rz_sdqkje").toString())){
				criteria.andEqualTo("rz_sdqkje",parmMap.get("rz_sdqkje").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rz_sdqkje1")!=null && StringUtils.isNotBlank(parmMap.get("rz_sdqkje1").toString())){
				criteria.andEqualTo("rz_sdqkje1",parmMap.get("rz_sdqkje1").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rz_yd_jydss")!=null && StringUtils.isNotBlank(parmMap.get("rz_yd_jydss").toString())){
				criteria.andEqualTo("rz_yd_jydss",parmMap.get("rz_yd_jydss").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rz_df_sdzjhm_ryb")!=null && StringUtils.isNotBlank(parmMap.get("rz_df_sdzjhm_ryb").toString())){
				criteria.andEqualTo("rz_df_sdzjhm_ryb",parmMap.get("rz_df_sdzjhm_ryb").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rz_mgsjl")!=null && StringUtils.isNotBlank(parmMap.get("rz_mgsjl").toString())){
				criteria.andEqualTo("rz_mgsjl",parmMap.get("rz_mgsjl").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rz_zsjyl")!=null && StringUtils.isNotBlank(parmMap.get("rz_zsjyl").toString())){
				criteria.andEqualTo("rz_zsjyl",parmMap.get("rz_zsjyl").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rz_sdrs")!=null && StringUtils.isNotBlank(parmMap.get("rz_sdrs").toString())){
				criteria.andEqualTo("rz_sdrs",parmMap.get("rz_sdrs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rz_df_zjhm_wxglr")!=null && StringUtils.isNotBlank(parmMap.get("rz_df_zjhm_wxglr").toString())){
				criteria.andEqualTo("rz_df_zjhm_wxglr",parmMap.get("rz_df_zjhm_wxglr").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cz_wf_wxzh_x1")!=null && StringUtils.isNotBlank(parmMap.get("cz_wf_wxzh_x1").toString())){
				criteria.andEqualTo("cz_wf_wxzh_x1",parmMap.get("cz_wf_wxzh_x1").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cz_wf_gwhj")!=null && StringUtils.isNotBlank(parmMap.get("cz_wf_gwhj").toString())){
				criteria.andEqualTo("cz_wf_gwhj",parmMap.get("cz_wf_gwhj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cz_jydss")!=null && StringUtils.isNotBlank(parmMap.get("cz_jydss").toString())){
				criteria.andEqualTo("cz_jydss",parmMap.get("cz_jydss").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cz_jycs")!=null && StringUtils.isNotBlank(parmMap.get("cz_jycs").toString())){
				criteria.andEqualTo("cz_jycs",parmMap.get("cz_jycs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cz_jyje")!=null && StringUtils.isNotBlank(parmMap.get("cz_jyje").toString())){
				criteria.andEqualTo("cz_jyje",parmMap.get("cz_jyje").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cz_sdqkje")!=null && StringUtils.isNotBlank(parmMap.get("cz_sdqkje").toString())){
				criteria.andEqualTo("cz_sdqkje",parmMap.get("cz_sdqkje").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cz_sdqkje1")!=null && StringUtils.isNotBlank(parmMap.get("cz_sdqkje1").toString())){
				criteria.andEqualTo("cz_sdqkje1",parmMap.get("cz_sdqkje1").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cz_yd_jydss")!=null && StringUtils.isNotBlank(parmMap.get("cz_yd_jydss").toString())){
				criteria.andEqualTo("cz_yd_jydss",parmMap.get("cz_yd_jydss").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cz_df_sdzjhm_ryb")!=null && StringUtils.isNotBlank(parmMap.get("cz_df_sdzjhm_ryb").toString())){
				criteria.andEqualTo("cz_df_sdzjhm_ryb",parmMap.get("cz_df_sdzjhm_ryb").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cz_mgsjl")!=null && StringUtils.isNotBlank(parmMap.get("cz_mgsjl").toString())){
				criteria.andEqualTo("cz_mgsjl",parmMap.get("cz_mgsjl").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cz_zsjyl")!=null && StringUtils.isNotBlank(parmMap.get("cz_zsjyl").toString())){
				criteria.andEqualTo("cz_zsjyl",parmMap.get("cz_zsjyl").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cz_sdrs")!=null && StringUtils.isNotBlank(parmMap.get("cz_sdrs").toString())){
				criteria.andEqualTo("cz_sdrs",parmMap.get("cz_sdrs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cz_df_zjhm_wxglr")!=null && StringUtils.isNotBlank(parmMap.get("cz_df_zjhm_wxglr").toString())){
				criteria.andEqualTo("cz_df_zjhm_wxglr",parmMap.get("cz_df_zjhm_wxglr").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zjycs")!=null && StringUtils.isNotBlank(parmMap.get("zjycs").toString())){
				criteria.andEqualTo("zjycs",parmMap.get("zjycs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zjyds")!=null && StringUtils.isNotBlank(parmMap.get("zjyds").toString())){
				criteria.andEqualTo("zjyds",parmMap.get("zjyds").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zjyje")!=null && StringUtils.isNotBlank(parmMap.get("zjyje").toString())){
				criteria.andEqualTo("zjyje",parmMap.get("zjyje").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ydjyds")!=null && StringUtils.isNotBlank(parmMap.get("ydjyds").toString())){
				criteria.andEqualTo("ydjyds",parmMap.get("ydjyds").toString().trim());
				flag = true;
			}
    		if(parmMap.get("df_sdzjhm_ryb")!=null && StringUtils.isNotBlank(parmMap.get("df_sdzjhm_ryb").toString())){
				criteria.andEqualTo("df_sdzjhm_ryb",parmMap.get("df_sdzjhm_ryb").toString().trim());
				flag = true;
			}
    		if(parmMap.get("df_zjhm_wxglr")!=null && StringUtils.isNotBlank(parmMap.get("df_zjhm_wxglr").toString())){
				criteria.andEqualTo("df_zjhm_wxglr",parmMap.get("df_zjhm_wxglr").toString().trim());
				flag = true;
			}
    		if(parmMap.get("wf_wxzh_x1")!=null && StringUtils.isNotBlank(parmMap.get("wf_wxzh_x1").toString())){
				criteria.andEqualTo("wf_wxzh_x1",parmMap.get("wf_wxzh_x1").toString().trim());
				flag = true;
			}
    		if(parmMap.get("wf_gwhj")!=null && StringUtils.isNotBlank(parmMap.get("wf_gwhj").toString())){
				criteria.andEqualTo("wf_gwhj",parmMap.get("wf_gwhj").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("VZhWfCrzjhTjServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("VZhWfCrzjhTjServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
