package com.unis.service.wxjyyjmx.impl;

import java.util.List;
import java.util.Map;

import com.unis.model.wxjyyjmx.*;
import com.unis.service.wxjyyjmx.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.unis.dto.ResultDto;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.wxjyyjmx.ZhZfzhdwdtjMapper;
import com.unis.common.config.DS;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see ZhZfzhdwdtjService
 * </pre>
 *
 * @author Qx
 * @version 1.0
 * @since 2020-05-09
 */
@Service("zhZfzhdwdtjService")
public class ZhZfzhdwdtjServiceImpl extends BaseServiceImpl implements ZhZfzhdwdtjService {
    private static final Logger logger = LoggerFactory.getLogger(ZhZfzhdwdtjServiceImpl.class);
    @Autowired
    private ZhZfzhdwdtjMapper zhZfzhdwdtjMapper;
    @Autowired
    private VZhWfSjjhService vZhWfSjjhService;
    @Autowired
    private VZhWfXjjhService vZhWfXjjhService;
    @Autowired
    private VZhDfzfzhsjtjxService vZhDfzfzhsjtjxService;
    @Autowired
    private VZhDfzfzhxjtjxService vZhDfzfzhxjtjxService;
    @Autowired
    private VZhSdqkjhService vZhSdqkjhService;
    @Autowired
    private ZhMxdrdataService zhMxdrdataService;


    /**
     * @see ZhZfzhdwdtjService#queryPageInfoByExample(Example example, int pageNum, int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example, int pageNum, int pageSize) throws Exception {
        if (example != null) {
            PageHelper.startPage(pageNum, pageSize);
            return new PageInfo(zhZfzhdwdtjMapper.selectByExample(example));
        } else {
            logger.error("ZhZfzhdwdtjServiceImpl.queryListByExample时example数据为空。");
            throw new AppRuntimeException("ZhZfzhdwdtjServiceImpl.queryListByExample时example数据为空。");
        }
    }

    /**
     * @see ZhZfzhdwdtjService#queryListByPage(Map parmMap, int pageNum, int pageSize)
     */
    @Override
    @DS("datasource2")
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws Exception {
        if (parmMap != null) {
            Example example = new Example(ZhZfzhdwdtj.class);
            //example.setOrderByClause("rksj DESC,pk asc");设置排序
            Example.Criteria criteria = example.createCriteria();
            //criteria设置
            // = --> andEqualTo(field,value)
            // like --> andLike(field,likeValue)；likeValue 包含‘%’
            // is null --> andIsNull(field)
            // is not null --> andIsNotNull(field)
            // <> --> andNotEqualTo(field)
            // > --> andGreaterThan(field,value)
            // >= --> andGreaterThanOrEqualTo(field,value)
            // < --> andLessThan(field,value)
            // <= --> andLessThanOrEqualTo(field,value)
            // in --> andIn(field,Iterable value)
            // not in --> andNotIn(field,Iterable value)
            // between --> andBetween(field,beginValue,endValue)
            // not like --> andNotLike(field,likeValue)；likeValue 包含‘%’

            // or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)

            //criteria.andEqualTo("yxx",1);设置yxx=1
            /**
             *此方法请根据需要修改下方条件
             */
            //此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
            //生成条件中均为 and field = value
            //生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改

            boolean flag = false;
            if (parmMap.get("zhjysjrqx") != null && StringUtils.isNotBlank(parmMap.get("zhjysjrqx").toString())) {
                criteria.andEqualTo("zhjysjrqx", parmMap.get("zhjysjrqx").toString().trim());
                flag = true;
            }
            if (parmMap.get("ydmx") != null && StringUtils.isNotBlank(parmMap.get("ydmx").toString())) {
                criteria.andEqualTo("ydmx", parmMap.get("ydmx").toString().trim());
                flag = true;
            }
            if (parmMap.get("kmdgcs_zjgl") != null && StringUtils.isNotBlank(parmMap.get("kmdgcs_zjgl").toString())) {
                criteria.andEqualTo("kmdgcs_zjgl", parmMap.get("kmdgcs_zjgl").toString().trim());
                flag = true;
            }
            if (parmMap.get("dfzfzhjycs") != null && StringUtils.isNotBlank(parmMap.get("dfzfzhjycs").toString())) {
                criteria.andEqualTo("dfzfzhjycs", parmMap.get("dfzfzhjycs").toString().trim());
                flag = true;
            }
            if (parmMap.get("dfzhjysj") != null && StringUtils.isNotBlank(parmMap.get("dfzhjysj").toString())) {
                criteria.andEqualTo("dfzhjysj", parmMap.get("dfzhjysj").toString().trim());
                flag = true;
            }
            if (parmMap.get("dfzhjyqk") != null && StringUtils.isNotBlank(parmMap.get("dfzhjyqk").toString())) {
                criteria.andEqualTo("dfzhjyqk", parmMap.get("dfzhjyqk").toString().trim());
                flag = true;
            }
            if (parmMap.get("dfzzjysj") != null && StringUtils.isNotBlank(parmMap.get("dfzzjysj").toString())) {
                criteria.andEqualTo("dfzzjysj", parmMap.get("dfzzjysj").toString().trim());
                flag = true;
            }
            if (parmMap.get("dfzzjyqk") != null && StringUtils.isNotBlank(parmMap.get("dfzzjyqk").toString())) {
                criteria.andEqualTo("dfzzjyqk", parmMap.get("dfzzjyqk").toString().trim());
                flag = true;
            }
            if (parmMap.get("wfzhjyye") != null && StringUtils.isNotBlank(parmMap.get("wfzhjyye").toString())) {
                criteria.andEqualTo("wfzhjyye", parmMap.get("wfzhjyye").toString().trim());
                flag = true;
            }
            if (parmMap.get("wfzhlyaj") != null && StringUtils.isNotBlank(parmMap.get("wfzhlyaj").toString())) {
                criteria.andEqualTo("wfzhlyaj", parmMap.get("wfzhlyaj").toString().trim());
                flag = true;
            }
            if (parmMap.get("wfzhjysj1") != null && StringUtils.isNotBlank(parmMap.get("wfzhjysj1").toString())) {
                criteria.andEqualTo("wfzhjysj1", parmMap.get("wfzhjysj1").toString().trim());
                flag = true;
            }
            if (parmMap.get("wfzhjyqk1") != null && StringUtils.isNotBlank(parmMap.get("wfzhjyqk1").toString())) {
                criteria.andEqualTo("wfzhjyqk1", parmMap.get("wfzhjyqk1").toString().trim());
                flag = true;
            }
            if (parmMap.get("qbqkxx") != null && StringUtils.isNotBlank(parmMap.get("qbqkxx").toString())) {
                criteria.andEqualTo("qbqkxx", parmMap.get("qbqkxx").toString().trim());
                flag = true;
            }
            if (parmMap.get("zjzhsj") != null && StringUtils.isNotBlank(parmMap.get("zjzhsj").toString())) {
                criteria.andEqualTo("zjzhsj", parmMap.get("zjzhsj").toString().trim());
                flag = true;
            }
            if (parmMap.get("zjzhsjly") != null && StringUtils.isNotBlank(parmMap.get("zjzhsjly").toString())) {
                criteria.andEqualTo("zjzhsjly", parmMap.get("zjzhsjly").toString().trim());
                flag = true;
            }
            if (parmMap.get("qkryhjsd") != null && StringUtils.isNotBlank(parmMap.get("qkryhjsd").toString())) {
                criteria.andEqualTo("qkryhjsd", parmMap.get("qkryhjsd").toString().trim());
                flag = true;
            }
            if (parmMap.get("zjzhrylbzw") != null && StringUtils.isNotBlank(parmMap.get("zjzhrylbzw").toString())) {
                criteria.andEqualTo("zjzhrylbzw", parmMap.get("zjzhrylbzw").toString().trim());
                flag = true;
            }
            if (parmMap.get("qksldw") != null && StringUtils.isNotBlank(parmMap.get("qksldw").toString())) {
                criteria.andEqualTo("qksldw", parmMap.get("qksldw").toString().trim());
                flag = true;
            }
            if (parmMap.get("zjzhqk") != null && StringUtils.isNotBlank(parmMap.get("zjzhqk").toString())) {
                criteria.andEqualTo("zjzhqk", parmMap.get("zjzhqk").toString().trim());
                flag = true;
            }
            if (parmMap.get("bdyhk_wxzhgl") != null && StringUtils.isNotBlank(parmMap.get("bdyhk_wxzhgl").toString())) {
                criteria.andEqualTo("bdyhk_wxzhgl", parmMap.get("bdyhk_wxzhgl").toString().trim());
                flag = true;
            }
            if (parmMap.get("khztzjhm_wxzhgl") != null && StringUtils.isNotBlank(parmMap.get("khztzjhm_wxzhgl").toString())) {
                criteria.andEqualTo("khztzjhm_wxzhgl", parmMap.get("khztzjhm_wxzhgl").toString().trim());
                flag = true;
            }
            if (parmMap.get("khztxm_wxzhgl") != null && StringUtils.isNotBlank(parmMap.get("khztxm_wxzhgl").toString())) {
                criteria.andEqualTo("khztxm_wxzhgl", parmMap.get("khztxm_wxzhgl").toString().trim());
                flag = true;
            }
            if (parmMap.get("bdsjh_wxzhgl") != null && StringUtils.isNotBlank(parmMap.get("bdsjh_wxzhgl").toString())) {
                criteria.andEqualTo("bdsjh_wxzhgl", parmMap.get("bdsjh_wxzhgl").toString().trim());
                flag = true;
            }
            if (parmMap.get("kmdgcs_wxgl") != null && StringUtils.isNotBlank(parmMap.get("kmdgcs_wxgl").toString())) {
                criteria.andEqualTo("kmdgcs_wxgl", parmMap.get("kmdgcs_wxgl").toString().trim());
                flag = true;
            }
            if (parmMap.get("dfwxh") != null && StringUtils.isNotBlank(parmMap.get("dfwxh").toString())) {
                criteria.andEqualTo("dfwxh", parmMap.get("dfwxh").toString().trim());
                flag = true;
            }
            if (parmMap.get("ydmx2") != null && StringUtils.isNotBlank(parmMap.get("ydmx2").toString())) {
                criteria.andEqualTo("ydmx2", parmMap.get("ydmx2").toString().trim());
                flag = true;
            }
            if (parmMap.get("wfzhjysj") != null && StringUtils.isNotBlank(parmMap.get("wfzhjysj").toString())) {
                criteria.andEqualTo("wfzhjysj", parmMap.get("wfzhjysj").toString().trim());
                flag = true;
            }
            if (parmMap.get("wfzhjyqk") != null && StringUtils.isNotBlank(parmMap.get("wfzhjyqk").toString())) {
                criteria.andEqualTo("wfzhjyqk", parmMap.get("wfzhjyqk").toString().trim());
                flag = true;
            }
            if (parmMap.get("wfzfzhjycs") != null && StringUtils.isNotBlank(parmMap.get("wfzfzhjycs").toString())) {
                criteria.andEqualTo("wfzfzhjycs", parmMap.get("wfzfzhjycs").toString().trim());
                flag = true;
            }
            if (parmMap.get("wfzzjysj") != null && StringUtils.isNotBlank(parmMap.get("wfzzjysj").toString())) {
                criteria.andEqualTo("wfzzjysj", parmMap.get("wfzzjysj").toString().trim());
                flag = true;
            }
            if (parmMap.get("wfzzjyqk") != null && StringUtils.isNotBlank(parmMap.get("wfzzjyqk").toString())) {
                criteria.andEqualTo("wfzzjyqk", parmMap.get("wfzzjyqk").toString().trim());
                flag = true;
            }
            if (parmMap.get("bdyhk_wfzf") != null && StringUtils.isNotBlank(parmMap.get("bdyhk_wfzf").toString())) {
                criteria.andEqualTo("bdyhk_wfzf", parmMap.get("bdyhk_wfzf").toString().trim());
                flag = true;
            }
            if (parmMap.get("zjh_wfzf") != null && StringUtils.isNotBlank(parmMap.get("zjh_wfzf").toString())) {
                criteria.andEqualTo("zjh_wfzf", parmMap.get("zjh_wfzf").toString().trim());
                flag = true;
            }
            if (parmMap.get("khxm_wfzf") != null && StringUtils.isNotBlank(parmMap.get("khxm_wfzf").toString())) {
                criteria.andEqualTo("khxm_wfzf", parmMap.get("khxm_wfzf").toString().trim());
                flag = true;
            }
            if (parmMap.get("bdsjh_wfzf") != null && StringUtils.isNotBlank(parmMap.get("bdsjh_wfzf").toString())) {
                criteria.andEqualTo("bdsjh_wfzf", parmMap.get("bdsjh_wfzf").toString().trim());
                flag = true;
            }
            if (parmMap.get("gwhj_wfzf") != null && StringUtils.isNotBlank(parmMap.get("gwhj_wfzf").toString())) {
                criteria.andEqualTo("gwhj_wfzf", parmMap.get("gwhj_wfzf").toString().trim());
                flag = true;
            }
            if (parmMap.get("wxzh_x_wfzf") != null && StringUtils.isNotBlank(parmMap.get("wxzh_x_wfzf").toString())) {
                criteria.andEqualTo("wxzh_x_wfzf", parmMap.get("wxzh_x_wfzf").toString().trim());
                flag = true;
            }
            if (parmMap.get("kmdgcs_wfzj") != null && StringUtils.isNotBlank(parmMap.get("kmdgcs_wfzj").toString())) {
                criteria.andEqualTo("kmdgcs_wfzj", parmMap.get("kmdgcs_wfzj").toString().trim());
                flag = true;
            }
            if (parmMap.get("cftzh") != null && StringUtils.isNotBlank(parmMap.get("cftzh").toString())) {
                criteria.andEqualTo("cftzh", parmMap.get("cftzh").toString().trim());
                flag = true;
            }
            if (parmMap.get("wxh") != null && StringUtils.isNotBlank(parmMap.get("wxh").toString())) {
                criteria.andEqualTo("wxh", parmMap.get("wxh").toString().trim());
                flag = true;
            }
            if (parmMap.get("cftkhr") != null && StringUtils.isNotBlank(parmMap.get("cftkhr").toString())) {
                criteria.andEqualTo("cftkhr", parmMap.get("cftkhr").toString().trim());
                flag = true;
            }
            if (parmMap.get("cftkhzj") != null && StringUtils.isNotBlank(parmMap.get("cftkhzj").toString())) {
                criteria.andEqualTo("cftkhzj", parmMap.get("cftkhzj").toString().trim());
                flag = true;
            }
            if (parmMap.get("cftbdsjh") != null && StringUtils.isNotBlank(parmMap.get("cftbdsjh").toString())) {
                criteria.andEqualTo("cftbdsjh", parmMap.get("cftbdsjh").toString().trim());
                flag = true;
            }
            if (parmMap.get("df_gwhj") != null && StringUtils.isNotBlank(parmMap.get("df_gwhj").toString())) {
                criteria.andEqualTo("df_gwhj", parmMap.get("df_gwhj").toString().trim());
                flag = true;
            }
            if (parmMap.get("czcs") != null && StringUtils.isNotBlank(parmMap.get("czcs").toString())) {
                criteria.andEqualTo("czcs", parmMap.get("czcs").toString().trim());
                flag = true;
            }
            if (parmMap.get("czje") != null && StringUtils.isNotBlank(parmMap.get("czje").toString())) {
                criteria.andEqualTo("czje", parmMap.get("czje").toString().trim());
                flag = true;
            }
            if (parmMap.get("rzcs") != null && StringUtils.isNotBlank(parmMap.get("rzcs").toString())) {
                criteria.andEqualTo("rzcs", parmMap.get("rzcs").toString().trim());
                flag = true;
            }
            if (parmMap.get("rzje") != null && StringUtils.isNotBlank(parmMap.get("rzje").toString())) {
                criteria.andEqualTo("rzje", parmMap.get("rzje").toString().trim());
                flag = true;
            }
            if (parmMap.get("zjgxrs") != null && StringUtils.isNotBlank(parmMap.get("zjgxrs").toString())) {
                criteria.andEqualTo("zjgxrs", parmMap.get("zjgxrs").toString().trim());
                flag = true;
            }
            if (parmMap.get("sdzjgxrs") != null && StringUtils.isNotBlank(parmMap.get("sdzjgxrs").toString())) {
                criteria.andEqualTo("sdzjgxrs", parmMap.get("sdzjgxrs").toString().trim());
                flag = true;
            }
            if (parmMap.get("dfzhrzrs") != null && StringUtils.isNotBlank(parmMap.get("dfzhrzrs").toString())) {
                criteria.andEqualTo("dfzhrzrs", parmMap.get("dfzhrzrs").toString().trim());
                flag = true;
            }
            if (parmMap.get("dfzhczrs") != null && StringUtils.isNotBlank(parmMap.get("dfzhczrs").toString())) {
                criteria.andEqualTo("dfzhczrs", parmMap.get("dfzhczrs").toString().trim());
                flag = true;
            }
            if (parmMap.get("dfrzsdrs") != null && StringUtils.isNotBlank(parmMap.get("dfrzsdrs").toString())) {
                criteria.andEqualTo("dfrzsdrs", parmMap.get("dfrzsdrs").toString().trim());
                flag = true;
            }
            if (parmMap.get("dfczsdrs") != null && StringUtils.isNotBlank(parmMap.get("dfczsdrs").toString())) {
                criteria.andEqualTo("dfczsdrs", parmMap.get("dfczsdrs").toString().trim());
                flag = true;
            }
            if (parmMap.get("mfsdzb") != null && StringUtils.isNotBlank(parmMap.get("mfsdzb").toString())) {
                criteria.andEqualTo("mfsdzb", parmMap.get("mfsdzb").toString().trim());
                flag = true;
            }
            if (parmMap.get("zsjyzb") != null && StringUtils.isNotBlank(parmMap.get("zsjyzb").toString())) {
                criteria.andEqualTo("zsjyzb", parmMap.get("zsjyzb").toString().trim());
                flag = true;
            }
            if (parmMap.get("sfbdhjryds") != null && StringUtils.isNotBlank(parmMap.get("sfbdhjryds").toString())) {
                criteria.andEqualTo("sfbdhjryds", parmMap.get("sfbdhjryds").toString().trim());
                flag = true;
            }
            if (parmMap.get("zhjysj") != null && StringUtils.isNotBlank(parmMap.get("zhjysj").toString())) {
                criteria.andEqualTo("zhjysj", parmMap.get("zhjysj").toString().trim());
                flag = true;
            }
            if (parmMap.get("zhjyqk") != null && StringUtils.isNotBlank(parmMap.get("zhjyqk").toString())) {
                criteria.andEqualTo("zhjyqk", parmMap.get("zhjyqk").toString().trim());
                flag = true;
            }
            if (parmMap.get("rz_wf_wxzh_x1") != null && StringUtils.isNotBlank(parmMap.get("rz_wf_wxzh_x1").toString())) {
                criteria.andEqualTo("rz_wf_wxzh_x1", parmMap.get("rz_wf_wxzh_x1").toString().trim());
                flag = true;
            }
            if (parmMap.get("rz_jydss") != null && StringUtils.isNotBlank(parmMap.get("rz_jydss").toString())) {
                criteria.andEqualTo("rz_jydss", parmMap.get("rz_jydss").toString().trim());
                flag = true;
            }
            if (parmMap.get("rz_jycs") != null && StringUtils.isNotBlank(parmMap.get("rz_jycs").toString())) {
                criteria.andEqualTo("rz_jycs", parmMap.get("rz_jycs").toString().trim());
                flag = true;
            }
            if (parmMap.get("rz_jyje") != null && StringUtils.isNotBlank(parmMap.get("rz_jyje").toString())) {
                criteria.andEqualTo("rz_jyje", parmMap.get("rz_jyje").toString().trim());
                flag = true;
            }
            if (parmMap.get("rz_yd_jydss") != null && StringUtils.isNotBlank(parmMap.get("rz_yd_jydss").toString())) {
                criteria.andEqualTo("rz_yd_jydss", parmMap.get("rz_yd_jydss").toString().trim());
                flag = true;
            }
            if (parmMap.get("rz_df_sdzjhm_ryb") != null && StringUtils.isNotBlank(parmMap.get("rz_df_sdzjhm_ryb").toString())) {
                criteria.andEqualTo("rz_df_sdzjhm_ryb", parmMap.get("rz_df_sdzjhm_ryb").toString().trim());
                flag = true;
            }
            if (parmMap.get("rz_sdrs") != null && StringUtils.isNotBlank(parmMap.get("rz_sdrs").toString())) {
                criteria.andEqualTo("rz_sdrs", parmMap.get("rz_sdrs").toString().trim());
                flag = true;
            }
            if (parmMap.get("rz_df_zjhm_wxglr") != null && StringUtils.isNotBlank(parmMap.get("rz_df_zjhm_wxglr").toString())) {
                criteria.andEqualTo("rz_df_zjhm_wxglr", parmMap.get("rz_df_zjhm_wxglr").toString().trim());
                flag = true;
            }
            if (parmMap.get("rz_mgsjl") != null && StringUtils.isNotBlank(parmMap.get("rz_mgsjl").toString())) {
                criteria.andEqualTo("rz_mgsjl", parmMap.get("rz_mgsjl").toString().trim());
                flag = true;
            }
            if (parmMap.get("rz_zsjyl") != null && StringUtils.isNotBlank(parmMap.get("rz_zsjyl").toString())) {
                criteria.andEqualTo("rz_zsjyl", parmMap.get("rz_zsjyl").toString().trim());
                flag = true;
            }
            if (parmMap.get("rz_sdqkje") != null && StringUtils.isNotBlank(parmMap.get("rz_sdqkje").toString())) {
                criteria.andEqualTo("rz_sdqkje", parmMap.get("rz_sdqkje").toString().trim());
                flag = true;
            }
            if (parmMap.get("rz_sdqkje1") != null && StringUtils.isNotBlank(parmMap.get("rz_sdqkje1").toString())) {
                criteria.andEqualTo("rz_sdqkje1", parmMap.get("rz_sdqkje1").toString().trim());
                flag = true;
            }
            if (parmMap.get("cz_jydss") != null && StringUtils.isNotBlank(parmMap.get("cz_jydss").toString())) {
                criteria.andEqualTo("cz_jydss", parmMap.get("cz_jydss").toString().trim());
                flag = true;
            }
            if (parmMap.get("cz_jycs") != null && StringUtils.isNotBlank(parmMap.get("cz_jycs").toString())) {
                criteria.andEqualTo("cz_jycs", parmMap.get("cz_jycs").toString().trim());
                flag = true;
            }
            if (parmMap.get("cz_jyje") != null && StringUtils.isNotBlank(parmMap.get("cz_jyje").toString())) {
                criteria.andEqualTo("cz_jyje", parmMap.get("cz_jyje").toString().trim());
                flag = true;
            }
            if (parmMap.get("cz_yd_jydss") != null && StringUtils.isNotBlank(parmMap.get("cz_yd_jydss").toString())) {
                criteria.andEqualTo("cz_yd_jydss", parmMap.get("cz_yd_jydss").toString().trim());
                flag = true;
            }
            if (parmMap.get("cz_df_sdzjhm_ryb") != null && StringUtils.isNotBlank(parmMap.get("cz_df_sdzjhm_ryb").toString())) {
                criteria.andEqualTo("cz_df_sdzjhm_ryb", parmMap.get("cz_df_sdzjhm_ryb").toString().trim());
                flag = true;
            }
            if (parmMap.get("cz_sdrs") != null && StringUtils.isNotBlank(parmMap.get("cz_sdrs").toString())) {
                criteria.andEqualTo("cz_sdrs", parmMap.get("cz_sdrs").toString().trim());
                flag = true;
            }
            if (parmMap.get("cz_df_zjhm_wxglr") != null && StringUtils.isNotBlank(parmMap.get("cz_df_zjhm_wxglr").toString())) {
                criteria.andEqualTo("cz_df_zjhm_wxglr", parmMap.get("cz_df_zjhm_wxglr").toString().trim());
                flag = true;
            }
            if (parmMap.get("cz_mgsjl") != null && StringUtils.isNotBlank(parmMap.get("cz_mgsjl").toString())) {
                criteria.andEqualTo("cz_mgsjl", parmMap.get("cz_mgsjl").toString().trim());
                flag = true;
            }
            if (parmMap.get("cz_zsjyl") != null && StringUtils.isNotBlank(parmMap.get("cz_zsjyl").toString())) {
                criteria.andEqualTo("cz_zsjyl", parmMap.get("cz_zsjyl").toString().trim());
                flag = true;
            }
            if (parmMap.get("cz_sdqkje") != null && StringUtils.isNotBlank(parmMap.get("cz_sdqkje").toString())) {
                criteria.andEqualTo("cz_sdqkje", parmMap.get("cz_sdqkje").toString().trim());
                flag = true;
            }
            if (parmMap.get("cz_sdqkje1") != null && StringUtils.isNotBlank(parmMap.get("cz_sdqkje1").toString())) {
                criteria.andEqualTo("cz_sdqkje1", parmMap.get("cz_sdqkje1").toString().trim());
                flag = true;
            }
            if (parmMap.get("zjycs") != null && StringUtils.isNotBlank(parmMap.get("zjycs").toString())) {
                criteria.andEqualTo("zjycs", parmMap.get("zjycs").toString().trim());
                flag = true;
            }
            if (parmMap.get("zjyds") != null && StringUtils.isNotBlank(parmMap.get("zjyds").toString())) {
                criteria.andEqualTo("zjyds", parmMap.get("zjyds").toString().trim());
                flag = true;
            }
            if (parmMap.get("zjyje") != null && StringUtils.isNotBlank(parmMap.get("zjyje").toString())) {
                criteria.andEqualTo("zjyje", parmMap.get("zjyje").toString().trim());
                flag = true;
            }
            if (parmMap.get("ydjyds") != null && StringUtils.isNotBlank(parmMap.get("ydjyds").toString())) {
                criteria.andEqualTo("ydjyds", parmMap.get("ydjyds").toString().trim());
                flag = true;
            }
            if (parmMap.get("df_sdzjhm_ryb") != null && StringUtils.isNotBlank(parmMap.get("df_sdzjhm_ryb").toString())) {
                criteria.andEqualTo("df_sdzjhm_ryb", parmMap.get("df_sdzjhm_ryb").toString().trim());
                flag = true;
            }
            if (parmMap.get("df_zjhm_wxglr") != null && StringUtils.isNotBlank(parmMap.get("df_zjhm_wxglr").toString())) {
                criteria.andEqualTo("df_zjhm_wxglr", parmMap.get("df_zjhm_wxglr").toString().trim());
                flag = true;
            }
            if (parmMap.get("wf_wxzh_x1") != null && StringUtils.isNotBlank(parmMap.get("wf_wxzh_x1").toString())) {
                criteria.andEqualTo("wf_wxzh_x1", parmMap.get("wf_wxzh_x1").toString().trim());
                flag = true;
            }
            if (parmMap.get("wf_gwhj") != null && StringUtils.isNotBlank(parmMap.get("wf_gwhj").toString())) {
                criteria.andEqualTo("wf_gwhj", parmMap.get("wf_gwhj").toString().trim());
                flag = true;
            }
            if (parmMap.get("sj") != null && StringUtils.isNotBlank(parmMap.get("sj").toString())) {
                criteria.andEqualTo("sj", parmMap.get("sj").toString().trim());
                flag = true;
            }
            if (parmMap.get("xj") != null && StringUtils.isNotBlank(parmMap.get("xj").toString())) {
                criteria.andEqualTo("xj", parmMap.get("xj").toString().trim());
                flag = true;
            }
            if (parmMap.get("wfsj") != null && StringUtils.isNotBlank(parmMap.get("wfsj").toString())) {
                criteria.andEqualTo("wfsj", parmMap.get("wfsj").toString().trim());
                flag = true;
            }
            if (parmMap.get("wfxj") != null && StringUtils.isNotBlank(parmMap.get("wfxj").toString())) {
                criteria.andEqualTo("wfxj", parmMap.get("wfxj").toString().trim());
                flag = true;
            }
            if (parmMap.get("mgsdlx") != null && StringUtils.isNotBlank(parmMap.get("mgsdlx").toString())) {
                criteria.andEqualTo("mgsdlx", parmMap.get("mgsdlx").toString().trim());
                flag = true;
            }
            if (parmMap.get("zsjylx") != null && StringUtils.isNotBlank(parmMap.get("zsjylx").toString())) {
                criteria.andEqualTo("zsjylx", parmMap.get("zsjylx").toString().trim());
                flag = true;
            }
            if (parmMap.get("zzjysjx") != null && StringUtils.isNotBlank(parmMap.get("zzjysjx").toString())) {
                criteria.andEqualTo("zzjysjx", parmMap.get("zzjysjx").toString().trim());
                flag = true;
            }
            if (parmMap.get("zhjyqkx") != null && StringUtils.isNotBlank(parmMap.get("zhjyqkx").toString())) {
                criteria.andEqualTo("zhjyqkx", parmMap.get("zhjyqkx").toString().trim());
                flag = true;
            }
            if (parmMap.get("kmdgcsx") != null && StringUtils.isNotBlank(parmMap.get("kmdgcsx").toString())) {
                criteria.andEqualTo("kmdgcsx", parmMap.get("kmdgcsx").toString().trim());
                flag = true;
            }
            if (parmMap.get("sfdqmxa") != null && StringUtils.isNotBlank(parmMap.get("sfdqmxa").toString())) {
                criteria.andEqualTo("sfdqmxa", parmMap.get("sfdqmxa").toString().trim());
                flag = true;
            }
            if (parmMap.get("bdyhkxa") != null && StringUtils.isNotBlank(parmMap.get("bdyhkxa").toString())) {
                criteria.andEqualTo("bdyhkxa", parmMap.get("bdyhkxa").toString().trim());
                flag = true;
            }
            if (parmMap.get("zhjysjx") != null && StringUtils.isNotBlank(parmMap.get("zhjysjx").toString())) {
                criteria.andEqualTo("zhjysjx", parmMap.get("zhjysjx").toString().trim());
                flag = true;
            }
            if (parmMap.get("bdsjhmx") != null && StringUtils.isNotBlank(parmMap.get("bdsjhmx").toString())) {
                criteria.andEqualTo("bdsjhmx", parmMap.get("bdsjhmx").toString().trim());
                flag = true;
            }
            if (parmMap.get("cftkhzjx") != null && StringUtils.isNotBlank(parmMap.get("cftkhzjx").toString())) {
                criteria.andEqualTo("cftkhzjx", parmMap.get("cftkhzjx").toString().trim());
                flag = true;
            }
            if (parmMap.get("cftkhrx") != null && StringUtils.isNotBlank(parmMap.get("cftkhrx").toString())) {
                criteria.andEqualTo("cftkhrx", parmMap.get("cftkhrx").toString().trim());
                flag = true;
            }
            if (parmMap.get("czsdrhzb") != null && StringUtils.isNotBlank(parmMap.get("czsdrhzb").toString())) {
                criteria.andEqualTo("czsdrhzb", parmMap.get("czsdrhzb").toString().trim());
                flag = true;
            }
            if (parmMap.get("rzsdrszb") != null && StringUtils.isNotBlank(parmMap.get("rzsdrszb").toString())) {
                criteria.andEqualTo("rzsdrszb", parmMap.get("rzsdrszb").toString().trim());
                flag = true;
            }
            if (parmMap.get("zhjysjrqgs") != null && StringUtils.isNotBlank(parmMap.get("zhjysjrqgs").toString())) {
                criteria.andEqualTo("zhjysjrqgs", parmMap.get("zhjysjrqgs").toString().trim());
                flag = true;
            }
            if (parmMap.get("gwhjx") != null && StringUtils.isNotBlank(parmMap.get("gwhjx").toString())) {
                criteria.andEqualTo("gwhjx", parmMap.get("gwhjx").toString().trim());
                flag = true;
            }
            if (parmMap.get("sdzigxrsa") != null && StringUtils.isNotBlank(parmMap.get("sdzigxrsa").toString())) {
                criteria.andEqualTo("sdzigxrsa", parmMap.get("sdzigxrsa").toString().trim());
                flag = true;
            }
            if (parmMap.get("rzrsxa") != null && StringUtils.isNotBlank(parmMap.get("rzrsxa").toString())) {
                criteria.andEqualTo("rzrsxa", parmMap.get("rzrsxa").toString().trim());
                flag = true;
            }
            if (parmMap.get("rzsdrsxa") != null && StringUtils.isNotBlank(parmMap.get("rzsdrsxa").toString())) {
                criteria.andEqualTo("rzsdrsxa", parmMap.get("rzsdrsxa").toString().trim());
                flag = true;
            }
            if (parmMap.get("rzjexa") != null && StringUtils.isNotBlank(parmMap.get("rzjexa").toString())) {
                criteria.andEqualTo("rzjexa", parmMap.get("rzjexa").toString().trim());
                flag = true;
            }
            if (parmMap.get("rzcsxa") != null && StringUtils.isNotBlank(parmMap.get("rzcsxa").toString())) {
                criteria.andEqualTo("rzcsxa", parmMap.get("rzcsxa").toString().trim());
                flag = true;
            }
            if (parmMap.get("czrsxa") != null && StringUtils.isNotBlank(parmMap.get("czrsxa").toString())) {
                criteria.andEqualTo("czrsxa", parmMap.get("czrsxa").toString().trim());
                flag = true;
            }
            if (parmMap.get("czsdrsxa") != null && StringUtils.isNotBlank(parmMap.get("czsdrsxa").toString())) {
                criteria.andEqualTo("czsdrsxa", parmMap.get("czsdrsxa").toString().trim());
                flag = true;
            }
            if (parmMap.get("czjexa") != null && StringUtils.isNotBlank(parmMap.get("czjexa").toString())) {
                criteria.andEqualTo("czjexa", parmMap.get("czjexa").toString().trim());
                flag = true;
            }
            if (parmMap.get("ydzjgxrs") != null && StringUtils.isNotBlank(parmMap.get("ydzjgxrs").toString())) {
                criteria.andEqualTo("ydzjgxrs", parmMap.get("ydzjgxrs").toString().trim());
                flag = true;
            }
            if (parmMap.get("wxhx") != null && StringUtils.isNotBlank(parmMap.get("wxhx").toString())) {
                criteria.andEqualTo("wxhx", parmMap.get("wxhx").toString().trim());
                flag = true;
            }
            if (parmMap.get("wfmgsdl") != null && StringUtils.isNotBlank(parmMap.get("wfmgsdl").toString())) {
                criteria.andEqualTo("wfmgsdl", parmMap.get("wfmgsdl").toString().trim());
                flag = true;
            }
            if (parmMap.get("wfzsjyl") != null && StringUtils.isNotBlank(parmMap.get("wfzsjyl").toString())) {
                criteria.andEqualTo("wfzsjyl", parmMap.get("wfzsjyl").toString().trim());
                flag = true;
            }
            if (parmMap.get("khzl") != null && StringUtils.isNotBlank(parmMap.get("khzl").toString())) {
                criteria.andEqualTo("khzl", parmMap.get("khzl").toString().trim());
                flag = true;
            }
            if (parmMap.get("jxsdfmsxa") != null && StringUtils.isNotBlank(parmMap.get("jxsdfmsxa").toString())) {
                criteria.andEqualTo("jxsdfmsxa", parmMap.get("jxsdfmsxa").toString().trim());
                flag = true;
            }
            if (parmMap.get("jxsdfxa") != null && StringUtils.isNotBlank(parmMap.get("jxsdfxa").toString())) {
                criteria.andEqualTo("jxsdfxa", parmMap.get("jxsdfxa").toString().trim());
                flag = true;
            }
            if (parmMap.get("ypbg") != null && StringUtils.isNotBlank(parmMap.get("ypbg").toString())) {
                criteria.andEqualTo("ypbg", parmMap.get("ypbg").toString().trim());
                flag = true;
            }
            if (parmMap.get("sdhy") != null && StringUtils.isNotBlank(parmMap.get("sdhy").toString())) {
                criteria.andEqualTo("sdhy", parmMap.get("sdhy").toString().trim());
                flag = true;
            }
            if (parmMap.get("zjhm_wxlt_x") != null && StringUtils.isNotBlank(parmMap.get("zjhm_wxlt_x").toString())) {
                criteria.andEqualTo("zjhm_wxlt_x", parmMap.get("zjhm_wxlt_x").toString().trim());
                flag = true;
            }
            if (parmMap.get("xm_wxlt_x") != null && StringUtils.isNotBlank(parmMap.get("xm_wxlt_x").toString())) {
                criteria.andEqualTo("xm_wxlt_x", parmMap.get("xm_wxlt_x").toString().trim());
                flag = true;
            }
            if (parmMap.get("wxtldfmsx") != null && StringUtils.isNotBlank(parmMap.get("wxtldfmsx").toString())) {
                criteria.andEqualTo("wxtldfmsx", parmMap.get("wxtldfmsx").toString().trim());
                flag = true;
            }
            if (parmMap.get("wxtldfx") != null && StringUtils.isNotBlank(parmMap.get("wxtldfx").toString())) {
                criteria.andEqualTo("wxtldfx", parmMap.get("wxtldfx").toString().trim());
                flag = true;
            }
            if (parmMap.get("wxncx") != null && StringUtils.isNotBlank(parmMap.get("wxncx").toString())) {
                criteria.andEqualTo("wxncx", parmMap.get("wxncx").toString().trim());
                flag = true;
            }
            if (parmMap.get("xdzdfms") != null && StringUtils.isNotBlank(parmMap.get("xdzdfms").toString())) {
                criteria.andEqualTo("xdzdfms", parmMap.get("xdzdfms").toString().trim());
                flag = true;
            }
            if (parmMap.get("df_xdz") != null && StringUtils.isNotBlank(parmMap.get("df_xdz").toString())) {
                criteria.andEqualTo("df_xdz", parmMap.get("df_xdz").toString().trim());
                flag = true;
            }
            if (parmMap.get("df_fxs_j") != null && StringUtils.isNotBlank(parmMap.get("df_fxs_j").toString())) {
                criteria.andEqualTo("df_fxs_j", parmMap.get("df_fxs_j").toString().trim());
                flag = true;
            }
            if (parmMap.get("jxsdfms") != null && StringUtils.isNotBlank(parmMap.get("jxsdfms").toString())) {
                criteria.andEqualTo("jxsdfms", parmMap.get("jxsdfms").toString().trim());
                flag = true;
            }
            if (parmMap.get("sjx") != null && StringUtils.isNotBlank(parmMap.get("sjx").toString())) {
                criteria.andEqualTo("sjx", parmMap.get("sjx").toString().trim());
                flag = true;
            }
            if (parmMap.get("xjx") != null && StringUtils.isNotBlank(parmMap.get("xjx").toString())) {
                criteria.andEqualTo("xjx", parmMap.get("xjx").toString().trim());
                flag = true;
            }
            //一个条件都没有的时候给一个默认条件
            if (!flag) {

            }

            boolean jsdrData = false;
            List<String> sljyList = null;
            int drdf = 0;
            PageInfo pageInfo = this.queryPageInfoByExample(example, pageNum, pageSize);
            List<ZhZfzhdwdtj> list = pageInfo.getList();
            VZhWfSjjh vZhWfSjjh;
            VZhWfXjjh vZhWfXjjh;
            VZhDfzfzhsjtjx vZhDfzfzhsjtjx;
            VZhDfzfzhxjtjx vZhDfzfzhxjtjx;
            VZhSdqkjh vZhSdqkjh;
            for (ZhZfzhdwdtj zhZfzhdwdtj : list) {

                vZhWfSjjh = vZhWfSjjhService.queryClob(zhZfzhdwdtj.getCftzh());
                vZhWfXjjh = vZhWfXjjhService.queryClob(zhZfzhdwdtj.getCftzh());
                vZhDfzfzhsjtjx = vZhDfzfzhsjtjxService.queryClob(zhZfzhdwdtj.getCftzh());
                vZhDfzfzhxjtjx = vZhDfzfzhxjtjxService.queryClob(zhZfzhdwdtj.getCftzh());
                vZhSdqkjh = vZhSdqkjhService.queryClob(zhZfzhdwdtj.getCftkhr());
                if (vZhWfSjjh != null) {
                    zhZfzhdwdtj.setWfsj(vZhWfSjjh.getWfsj());
                }
                if (vZhWfXjjh != null) {
                    zhZfzhdwdtj.setWfxj(vZhWfXjjh.getWfxj());
                }
                if (vZhDfzfzhsjtjx != null) {
                    zhZfzhdwdtj.setSj(vZhDfzfzhsjtjx.getDfsj());
                }
                if (vZhDfzfzhxjtjx != null) {
                    zhZfzhdwdtj.setXj(vZhDfzfzhxjtjx.getDfxj());
                }
                if (vZhSdqkjh != null) {
                    zhZfzhdwdtj.setQbqkxx(vZhSdqkjh.getQkjh());
                }
                zhZfzhdwdtj.setXjx(zhZfzhdwdtj.getXj() + zhZfzhdwdtj.getWfxj());
                zhZfzhdwdtj.setSjx(zhZfzhdwdtj.getSj() + zhZfzhdwdtj.getWfsj());
            }


            ResultDto result = new ResultDto();
            result.setRows(list);
            result.setPage(pageInfo.getPageNum());
            //result.setTotal(pageInfo.getPages());
            result.setTotal((int) pageInfo.getTotal());
            return result;
        } else {
            logger.error("ZhZfzhdwdtjServiceImpl.queryListByPage时parmMap数据为空。");
            throw new AppRuntimeException("ZhZfzhdwdtjServiceImpl.queryListByPage时parmMap数据为空。");
        }
    }

    @Override
    @DS("datasource2")
    public ResultDto queryListByPageDr(Map parmMap, int pageNum, int pageSize) throws Exception {
        ResultDto resultDto = this.queryListByPage(parmMap, pageNum, pageSize);
        if (resultDto != null) {
            List<String> sljyList = zhMxdrdataService.querySjly();
            int drdf;
            for (Object o : resultDto.getRows()) {
                ZhZfzhdwdtj zhZfzhdwdtj = (ZhZfzhdwdtj) o;
                drdf = 0;
                ZhMxdrdata zhMxdrdata = new ZhMxdrdata();
                zhMxdrdata.setCxzh(zhZfzhdwdtj.getCftzh());
                for (String sjly : sljyList) {
                    zhMxdrdata.setSjly(sjly);
                    zhMxdrdata = zhMxdrdataService.querySjlyAndCxzh(zhMxdrdata);
                    drdf += Integer.parseInt(zhMxdrdata.getDf());
                }
                zhZfzhdwdtj.setJxsdfxa(String.valueOf((Integer.parseInt(zhZfzhdwdtj.getJxsdfxa()) + drdf) / sljyList.size()));
            }
        }
        return resultDto;
    }
}
