package com.unis.service.wxjyyjmx.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.unis.common.exception.app.AppRuntimeException;
import com.unis.dto.ResultDto;
import com.unis.mapper.wxjyyjmx.VZhSdryzbqcMapper;
import com.unis.model.wxjyyjmx.VZhSdryzbqc;
import com.unis.service.impl.BaseServiceImpl;
import com.unis.service.wxjyyjmx.VZhSdryzbqcService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Map;

/**
 * <pre>
 * @see VZhSdryzbqcService
 * </pre>
 *
 * @author Qx
 * @version 1.0
 * @since 2020-05-12
 */
@Service("vZhSdryzbqcService")
public class VZhSdryzbqcServiceImpl extends BaseServiceImpl implements VZhSdryzbqcService {
	private static final Logger logger = LoggerFactory.getLogger(VZhSdryzbqcServiceImpl.class);
    @Autowired
    private VZhSdryzbqcMapper vZhSdryzbqcMapper;

    /**
     * @see VZhSdryzbqcService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(vZhSdryzbqcMapper.selectByExample(example));
    	}else{
    		logger.error("VZhSdryzbqcServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("VZhSdryzbqcServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see VZhSdryzbqcService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(VZhSdryzbqc.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("xm")!=null && StringUtils.isNotBlank(parmMap.get("xm").toString())){
				criteria.andEqualTo("xm",parmMap.get("xm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zjhmyh")!=null && StringUtils.isNotBlank(parmMap.get("zjhmyh").toString())){
				criteria.andEqualTo("zjhmyh",parmMap.get("zjhmyh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xyrdh")!=null && StringUtils.isNotBlank(parmMap.get("xyrdh").toString())){
				criteria.andEqualTo("xyrdh",parmMap.get("xyrdh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("spsj")!=null && StringUtils.isNotBlank(parmMap.get("spsj").toString())){
				criteria.andEqualTo("spsj",parmMap.get("spsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ryhjsd")!=null && StringUtils.isNotBlank(parmMap.get("ryhjsd").toString())){
				criteria.andEqualTo("ryhjsd",parmMap.get("ryhjsd").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rydz")!=null && StringUtils.isNotBlank(parmMap.get("rydz").toString())){
				criteria.andEqualTo("rydz",parmMap.get("rydz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sldw")!=null && StringUtils.isNotBlank(parmMap.get("sldw").toString())){
				criteria.andEqualTo("sldw",parmMap.get("sldw").toString().trim());
				flag = true;
			}
    		if(parmMap.get("aq")!=null && StringUtils.isNotBlank(parmMap.get("aq").toString())){
				criteria.andEqualTo("aq",parmMap.get("aq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ajbhzjhm")!=null && StringUtils.isNotBlank(parmMap.get("ajbhzjhm").toString())){
				criteria.andEqualTo("ajbhzjhm",parmMap.get("ajbhzjhm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sjly")!=null && StringUtils.isNotBlank(parmMap.get("sjly").toString())){
				criteria.andEqualTo("sjly",parmMap.get("sjly").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ll")!=null && StringUtils.isNotBlank(parmMap.get("ll").toString())){
				criteria.andEqualTo("ll",parmMap.get("ll").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfgtry")!=null && StringUtils.isNotBlank(parmMap.get("sfgtry").toString())){
				criteria.andEqualTo("sfgtry",parmMap.get("sfgtry").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ch")!=null && StringUtils.isNotBlank(parmMap.get("ch").toString())){
				criteria.andEqualTo("ch",parmMap.get("ch").toString().trim());
				flag = true;
			}
    		if(parmMap.get("csrq")!=null && StringUtils.isNotBlank(parmMap.get("csrq").toString())){
				criteria.andEqualTo("csrq",parmMap.get("csrq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qq")!=null && StringUtils.isNotBlank(parmMap.get("qq").toString())){
				criteria.andEqualTo("qq",parmMap.get("qq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gj")!=null && StringUtils.isNotBlank(parmMap.get("gj").toString())){
				criteria.andEqualTo("gj",parmMap.get("gj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sftw")!=null && StringUtils.isNotBlank(parmMap.get("sftw").toString())){
				criteria.andEqualTo("sftw",parmMap.get("sftw").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zjzl")!=null && StringUtils.isNotBlank(parmMap.get("zjzl").toString())){
				criteria.andEqualTo("zjzl",parmMap.get("zjzl").toString().trim());
				flag = true;
			}
    		if(parmMap.get("hjdxz")!=null && StringUtils.isNotBlank(parmMap.get("hjdxz").toString())){
				criteria.andEqualTo("hjdxz",parmMap.get("hjdxz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("wx")!=null && StringUtils.isNotBlank(parmMap.get("wx").toString())){
				criteria.andEqualTo("wx",parmMap.get("wx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qtzjhm")!=null && StringUtils.isNotBlank(parmMap.get("qtzjhm").toString())){
				criteria.andEqualTo("qtzjhm",parmMap.get("qtzjhm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xmbc")!=null && StringUtils.isNotBlank(parmMap.get("xmbc").toString())){
				criteria.andEqualTo("xmbc",parmMap.get("xmbc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rylbzw")!=null && StringUtils.isNotBlank(parmMap.get("rylbzw").toString())){
				criteria.andEqualTo("rylbzw",parmMap.get("rylbzw").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lxfsdh")!=null && StringUtils.isNotBlank(parmMap.get("lxfsdh").toString())){
				criteria.andEqualTo("lxfsdh",parmMap.get("lxfsdh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfam")!=null && StringUtils.isNotBlank(parmMap.get("sfam").toString())){
				criteria.andEqualTo("sfam",parmMap.get("sfam").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfxg")!=null && StringUtils.isNotBlank(parmMap.get("sfxg").toString())){
				criteria.andEqualTo("sfxg",parmMap.get("sfxg").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zxghqc")!=null && StringUtils.isNotBlank(parmMap.get("zxghqc").toString())){
				criteria.andEqualTo("zxghqc",parmMap.get("zxghqc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfjsn")!=null && StringUtils.isNotBlank(parmMap.get("sfjsn").toString())){
				criteria.andEqualTo("sfjsn",parmMap.get("sfjsn").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfbd")!=null && StringUtils.isNotBlank(parmMap.get("sfbd").toString())){
				criteria.andEqualTo("sfbd",parmMap.get("sfbd").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ryhjgsx")!=null && StringUtils.isNotBlank(parmMap.get("ryhjgsx").toString())){
				criteria.andEqualTo("ryhjgsx",parmMap.get("ryhjgsx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("hjdxxdzx")!=null && StringUtils.isNotBlank(parmMap.get("hjdxxdzx").toString())){
				criteria.andEqualTo("hjdxxdzx",parmMap.get("hjdxxdzx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gathqtrybq")!=null && StringUtils.isNotBlank(parmMap.get("gathqtrybq").toString())){
				criteria.andEqualTo("gathqtrybq",parmMap.get("gathqtrybq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zjzhqk")!=null && StringUtils.isNotBlank(parmMap.get("zjzhqk").toString())){
				criteria.andEqualTo("zjzhqk",parmMap.get("zjzhqk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfxs")!=null && StringUtils.isNotBlank(parmMap.get("sfxs").toString())){
				criteria.andEqualTo("sfxs",parmMap.get("sfxs").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("VZhSdryzbqcServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("VZhSdryzbqcServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
