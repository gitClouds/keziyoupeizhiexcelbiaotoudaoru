package com.unis.service.wxjyyjmx.impl;

import java.util.List;
import java.util.Map;

import com.unis.common.config.DS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.unis.dto.ResultDto;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.wxjyyjmx.ZhMxdrdataMapper;
import com.unis.model.wxjyyjmx.ZhMxdrdata;
import com.unis.service.wxjyyjmx.ZhMxdrdataService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see ZhMxdrdataService
 * </pre>
 *
 * @author Qx
 * @version 1.0
 * @since 2020-05-29
 */
@Service("zhMxdrdataService")
public class ZhMxdrdataServiceImpl extends BaseServiceImpl implements ZhMxdrdataService {
    private static final Logger logger = LoggerFactory.getLogger(ZhMxdrdataServiceImpl.class);
    @Autowired
    private ZhMxdrdataMapper zhMxdrdataMapper;

    /**
     * @see ZhMxdrdataService#queryPageInfoByExample(Example example, int pageNum, int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example, int pageNum, int pageSize) throws Exception {
        if (example != null) {
            PageHelper.startPage(pageNum, pageSize);
            return new PageInfo(zhMxdrdataMapper.selectByExample(example));
        } else {
            logger.error("ZhMxdrdataServiceImpl.queryListByExample时example数据为空。");
            throw new AppRuntimeException("ZhMxdrdataServiceImpl.queryListByExample时example数据为空。");
        }
    }

    /**
     * @see ZhMxdrdataService#queryListByPage(Map parmMap, int pageNum, int pageSize)
     */
    @Override
    @DS("datasource2")
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws Exception {
        if (parmMap != null) {
            Example example = new Example(ZhMxdrdata.class);
            //example.setOrderByClause("rksj DESC,pk asc");设置排序
            Example.Criteria criteria = example.createCriteria();
            //criteria设置
            // = --> andEqualTo(field,value)
            // like --> andLike(field,likeValue)；likeValue 包含‘%’
            // is null --> andIsNull(field)
            // is not null --> andIsNotNull(field)
            // <> --> andNotEqualTo(field)
            // > --> andGreaterThan(field,value)
            // >= --> andGreaterThanOrEqualTo(field,value)
            // < --> andLessThan(field,value)
            // <= --> andLessThanOrEqualTo(field,value)
            // in --> andIn(field,Iterable value)
            // not in --> andNotIn(field,Iterable value)
            // between --> andBetween(field,beginValue,endValue)
            // not like --> andNotLike(field,likeValue)；likeValue 包含‘%’

            // or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)

            //criteria.andEqualTo("yxx",1);设置yxx=1
            /**
             *此方法请根据需要修改下方条件
             */
            //此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
            //生成条件中均为 and field = value
            //生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改

            boolean flag = false;
            if (parmMap.get("id") != null && StringUtils.isNotBlank(parmMap.get("id").toString())) {
                criteria.andEqualTo("id", parmMap.get("id").toString().trim());
                flag = true;
            }
            if (parmMap.get("cxzh") != null && StringUtils.isNotBlank(parmMap.get("cxzh").toString())) {
                criteria.andEqualTo("cxzh", parmMap.get("cxzh").toString().trim());
                flag = true;
            }
            if (parmMap.get("df") != null && StringUtils.isNotBlank(parmMap.get("df").toString())) {
                criteria.andEqualTo("df", parmMap.get("df").toString().trim());
                flag = true;
            }
            if (parmMap.get("sjly") != null && StringUtils.isNotBlank(parmMap.get("sjly").toString())) {
                criteria.andEqualTo("sjly", parmMap.get("sjly").toString().trim());
                flag = true;
            }
            if (parmMap.get("lrsj") != null && StringUtils.isNotBlank(parmMap.get("lrsj").toString())) {
                criteria.andEqualTo("lrsj", parmMap.get("lrsj").toString().trim());
                flag = true;
            }
            if (parmMap.get("lrrjh") != null && StringUtils.isNotBlank(parmMap.get("lrrjh").toString())) {
                criteria.andEqualTo("lrrjh", parmMap.get("lrrjh").toString().trim());
                flag = true;
            }
            if (parmMap.get("lrrxm") != null && StringUtils.isNotBlank(parmMap.get("lrrxm").toString())) {
                criteria.andEqualTo("lrrxm", parmMap.get("lrrxm").toString().trim());
                flag = true;
            }
            if (parmMap.get("lrrdwdm") != null && StringUtils.isNotBlank(parmMap.get("lrrdwdm").toString())) {
                criteria.andEqualTo("lrrdwdm", parmMap.get("lrrdwdm").toString().trim());
                flag = true;
            }
            if (parmMap.get("lrrdwmc") != null && StringUtils.isNotBlank(parmMap.get("lrrdwmc").toString())) {
                criteria.andEqualTo("lrrdwmc", parmMap.get("lrrdwmc").toString().trim());
                flag = true;
            }
            //一个条件都没有的时候给一个默认条件
            if (!flag) {

            }
            PageInfo pageInfo = this.queryPageInfoByExample(example, pageNum, pageSize);
            ResultDto result = new ResultDto();
            result.setRows(pageInfo.getList());
            result.setPage(pageInfo.getPageNum());
            //result.setTotal(pageInfo.getPages());
            result.setTotal((int) pageInfo.getTotal());
            return result;
        } else {
            logger.error("ZhMxdrdataServiceImpl.queryListByPage时parmMap数据为空。");
            throw new AppRuntimeException("ZhMxdrdataServiceImpl.queryListByPage时parmMap数据为空。");
        }
    }

    @Override
    public List<String> querySjly() throws Exception {
        return zhMxdrdataMapper.querySjly();
    }

    @Override
    public  ZhMxdrdata  querySjlyAndCxzh(ZhMxdrdata zhMxdrdata) throws Exception {
        return zhMxdrdataMapper.querySjlyAndCxzh(zhMxdrdata);
    }
}
