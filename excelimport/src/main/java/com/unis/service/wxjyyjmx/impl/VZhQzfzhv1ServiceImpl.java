package com.unis.service.wxjyyjmx.impl;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.wxjyyjmx.VZhQzfzhv1Mapper;
import com.unis.model.wxjyyjmx.VZhQzfzhv1;
import com.unis.service.wxjyyjmx.VZhQzfzhv1Service;
import com.unis.common.config.DS;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see VZhQzfzhv1Service
 * </pre>
 *
 * @author gongjl
 * @version 1.0
 * @since 2020-05-06
 */
@Service("vZhQzfzhv1Service")
public class VZhQzfzhv1ServiceImpl extends BaseServiceImpl implements VZhQzfzhv1Service {
	private static final Logger logger = LoggerFactory.getLogger(VZhQzfzhv1ServiceImpl.class);
    @Autowired
    private VZhQzfzhv1Mapper vZhQzfzhv1Mapper;

    /**
     * @see VZhQzfzhv1Service#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(vZhQzfzhv1Mapper.selectByExample(example));
    	}else{
    		logger.error("VZhQzfzhv1ServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("VZhQzfzhv1ServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see VZhQzfzhv1Service#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    @DS("datasource2")
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(VZhQzfzhv1.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("dlh")!=null && StringUtils.isNotBlank(parmMap.get("dlh").toString())){
				criteria.andEqualTo("dlh",parmMap.get("dlh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xm_qymc")!=null && StringUtils.isNotBlank(parmMap.get("xm_qymc").toString())){
				criteria.andEqualTo("xm_qymc",parmMap.get("xm_qymc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yhkrzzt")!=null && StringUtils.isNotBlank(parmMap.get("yhkrzzt").toString())){
				criteria.andEqualTo("yhkrzzt",parmMap.get("yhkrzzt").toString().trim());
				flag = true;
			}
    		if(parmMap.get("wxh")!=null && StringUtils.isNotBlank(parmMap.get("wxh").toString())){
				criteria.andEqualTo("wxh",parmMap.get("wxh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zjlx")!=null && StringUtils.isNotBlank(parmMap.get("zjlx").toString())){
				criteria.andEqualTo("zjlx",parmMap.get("zjlx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bdsjh")!=null && StringUtils.isNotBlank(parmMap.get("bdsjh").toString())){
				criteria.andEqualTo("bdsjh",parmMap.get("bdsjh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cxzh")!=null && StringUtils.isNotBlank(parmMap.get("cxzh").toString())){
				criteria.andEqualTo("cxzh",parmMap.get("cxzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zfzh")!=null && StringUtils.isNotBlank(parmMap.get("zfzh").toString())){
				criteria.andEqualTo("zfzh",parmMap.get("zfzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zjh")!=null && StringUtils.isNotBlank(parmMap.get("zjh").toString())){
				criteria.andEqualTo("zjh",parmMap.get("zjh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zhlb")!=null && StringUtils.isNotBlank(parmMap.get("zhlb").toString())){
				criteria.andEqualTo("zhlb",parmMap.get("zhlb").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yhklx")!=null && StringUtils.isNotBlank(parmMap.get("yhklx").toString())){
				criteria.andEqualTo("yhklx",parmMap.get("yhklx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zzyxq")!=null && StringUtils.isNotBlank(parmMap.get("zzyxq").toString())){
				criteria.andEqualTo("zzyxq",parmMap.get("zzyxq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qczd")!=null && StringUtils.isNotBlank(parmMap.get("qczd").toString())){
				criteria.andEqualTo("qczd",parmMap.get("qczd").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yhkssyhmc")!=null && StringUtils.isNotBlank(parmMap.get("yhkssyhmc").toString())){
				criteria.andEqualTo("yhkssyhmc",parmMap.get("yhkssyhmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yhkyxq")!=null && StringUtils.isNotBlank(parmMap.get("yhkyxq").toString())){
				criteria.andEqualTo("yhkyxq",parmMap.get("yhkyxq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sj")!=null && StringUtils.isNotBlank(parmMap.get("sj").toString())){
				criteria.andEqualTo("sj",parmMap.get("sj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yhkqtxx")!=null && StringUtils.isNotBlank(parmMap.get("yhkqtxx").toString())){
				criteria.andEqualTo("yhkqtxx",parmMap.get("yhkqtxx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yhkkh")!=null && StringUtils.isNotBlank(parmMap.get("yhkkh").toString())){
				criteria.andEqualTo("yhkkh",parmMap.get("yhkkh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qqh")!=null && StringUtils.isNotBlank(parmMap.get("qqh").toString())){
				criteria.andEqualTo("qqh",parmMap.get("qqh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yhkssyhjgbm")!=null && StringUtils.isNotBlank(parmMap.get("yhkssyhjgbm").toString())){
				criteria.andEqualTo("yhkssyhjgbm",parmMap.get("yhkssyhjgbm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bdsjh_wxzt")!=null && StringUtils.isNotBlank(parmMap.get("bdsjh_wxzt").toString())){
				criteria.andEqualTo("bdsjh_wxzt",parmMap.get("bdsjh_wxzt").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bdyhk_wxzt")!=null && StringUtils.isNotBlank(parmMap.get("bdyhk_wxzt").toString())){
				criteria.andEqualTo("bdyhk_wxzt",parmMap.get("bdyhk_wxzt").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("VZhQzfzhv1ServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("VZhQzfzhv1ServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
