package com.unis.service.wxjyyjmx.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.unis.common.config.DS;
import com.unis.common.exception.app.AppRuntimeException;
import com.unis.dto.ResultDto;
import com.unis.mapper.wxjyyjmx.VZhDfzfZhjycsMapper;
import com.unis.model.wxjyyjmx.VZhDfzfZhjycs;
import com.unis.service.impl.BaseServiceImpl;
import com.unis.service.wxjyyjmx.VZhDfzfZhjycsService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Map;

/**
 * <pre>
 * @see VZhDfzfZhjycsService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2020-05-07
 */

@Service("vZhDfzfZhjycsService")
public class VZhDfzfZhjycsServiceImpl extends BaseServiceImpl implements VZhDfzfZhjycsService {
    private static final Logger logger = LoggerFactory.getLogger(VZhDfzfZhjycsServiceImpl.class);
    @Autowired
    private VZhDfzfZhjycsMapper vZhDfzfZhjycsMapper;

    /**
     * @see VZhDfzfZhjycsService#queryPageInfoByExample(Example example, int pageNum, int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example, int pageNum, int pageSize) throws Exception {
        if (example != null) {
            PageHelper.startPage(pageNum, pageSize);
            return new PageInfo(vZhDfzfZhjycsMapper.selectByExample(example));
        } else {
            logger.error("VZhDfzfZhjycsServiceImpl.queryListByExample时example数据为空。");
            throw new AppRuntimeException("VZhDfzfZhjycsServiceImpl.queryListByExample时example数据为空。");
        }
    }

    /**
     * @see VZhDfzfZhjycsService#queryListByPage(Map parmMap, int pageNum, int pageSize)
     */
    @Override
    @DS("datasource2")
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws Exception {
        if (parmMap != null) {
            Example example = new Example(VZhDfzfZhjycs.class);
            //example.setOrderByClause("rksj DESC,pk asc");设置排序
            Example.Criteria criteria = example.createCriteria();
            //criteria设置
            // = --> andEqualTo(field,value)
            // like --> andLike(field,likeValue)；likeValue 包含‘%’
            // is null --> andIsNull(field)
            // is not null --> andIsNotNull(field)
            // <> --> andNotEqualTo(field)
            // > --> andGreaterThan(field,value)
            // >= --> andGreaterThanOrEqualTo(field,value)
            // < --> andLessThan(field,value)
            // <= --> andLessThanOrEqualTo(field,value)
            // in --> andIn(field,Iterable value)
            // not in --> andNotIn(field,Iterable value)
            // between --> andBetween(field,beginValue,endValue)
            // not like --> andNotLike(field,likeValue)；likeValue 包含‘%’

            // or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)

            //criteria.andEqualTo("yxx",1);设置yxx=1
            /**
             *此方法请根据需要修改下方条件
             */
            //此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
            //生成条件中均为 and field = value
            //生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改

            boolean flag = false;
            if (parmMap.get("dfzfzh") != null && StringUtils.isNotBlank(parmMap.get("dfzfzh").toString())) {
                criteria.andEqualTo("dfzfzh", parmMap.get("dfzfzh").toString().trim());
                flag = true;
            }
            if (parmMap.get("jycs") != null && StringUtils.isNotBlank(parmMap.get("jycs").toString())) {
                criteria.andEqualTo("jycs", parmMap.get("jycs").toString().trim());
                flag = true;
            }
            //一个条件都没有的时候给一个默认条件
            if (!flag) {

            }
            PageInfo pageInfo = this.queryPageInfoByExample(example, pageNum, pageSize);
            ResultDto result = new ResultDto();
            result.setRows(pageInfo.getList());
            result.setPage(pageInfo.getPageNum());
            //result.setTotal(pageInfo.getPages());
            result.setTotal((int) pageInfo.getTotal());
            return result;
        } else {
            logger.error("VZhDfzfZhjycsServiceImpl.queryListByPage时parmMap数据为空。");
            throw new AppRuntimeException("VZhDfzfZhjycsServiceImpl.queryListByPage时parmMap数据为空。");
        }
    }
}
