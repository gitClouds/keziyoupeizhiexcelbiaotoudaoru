package com.unis.service.wxjyyjmx.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.unis.common.config.DS;
import com.unis.common.exception.app.AppRuntimeException;
import com.unis.dto.ResultDto;
import com.unis.mapper.wxjyyjmx.VZhZfzhdwdtj1Mapper;
import com.unis.model.wxjyyjmx.*;
import com.unis.service.impl.BaseServiceImpl;
import com.unis.service.wxjyyjmx.*;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * <pre>
 * @see VZhZfzhdwdtj1Service
 * </pre>
 *
 * @author Qx
 * @version 1.0
 * @since 2020-06-02
 */
@Service("vZhZfzhdwdtj1Service")
public class VZhZfzhdwdtj1ServiceImpl extends BaseServiceImpl implements VZhZfzhdwdtj1Service {
    private static final Logger logger = LoggerFactory.getLogger(VZhZfzhdwdtj1ServiceImpl.class);
    @Autowired
    private VZhZfzhdwdtj1Mapper vZhZfzhdwdtj1Mapper;
    @Autowired
    private VZhWfSjjhService vZhWfSjjhService;
    @Autowired
    private VZhWfXjjhService vZhWfXjjhService;
    @Autowired
    private VZhDfzfzhsjtjxService vZhDfzfzhsjtjxService;
    @Autowired
    private VZhDfzfzhxjtjxService vZhDfzfzhxjtjxService;

    /**
     * @see VZhZfzhdwdtj1Service#queryPageInfoByExample(Example example, int pageNum, int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example, int pageNum, int pageSize) throws Exception {
        if (example != null) {
            PageHelper.startPage(pageNum, pageSize);
            return new PageInfo(vZhZfzhdwdtj1Mapper.selectByExample(example));
        } else {
            logger.error("VZhZfzhdwdtj1ServiceImpl.queryListByExample时example数据为空。");
            throw new AppRuntimeException("VZhZfzhdwdtj1ServiceImpl.queryListByExample时example数据为空。");
        }
    }

    /**
     * @see VZhZfzhdwdtj1Service#queryListByPage(Map parmMap, int pageNum, int pageSize)
     */
    @Override
    @DS("datasource2")
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws Exception {
        if (parmMap != null) {
            Example example = new Example(VZhZfzhdwdtj1.class);
            //example.setOrderByClause("rksj DESC,pk asc");设置排序
            Example.Criteria criteria = example.createCriteria();
            //criteria设置
            // = --> andEqualTo(field,value)
            // like --> andLike(field,likeValue)；likeValue 包含‘%’
            // is null --> andIsNull(field)
            // is not null --> andIsNotNull(field)
            // <> --> andNotEqualTo(field)
            // > --> andGreaterThan(field,value)
            // >= --> andGreaterThanOrEqualTo(field,value)
            // < --> andLessThan(field,value)
            // <= --> andLessThanOrEqualTo(field,value)
            // in --> andIn(field,Iterable value)
            // not in --> andNotIn(field,Iterable value)
            // between --> andBetween(field,beginValue,endValue)
            // not like --> andNotLike(field,likeValue)；likeValue 包含‘%’

            // or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)

            //criteria.andEqualTo("yxx",1);设置yxx=1
            /**
             *此方法请根据需要修改下方条件
             */
            //此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
            //生成条件中均为 and field = value
            //生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改

            boolean flag = false;
            if (parmMap.get("cftzh") != null && StringUtils.isNotBlank(parmMap.get("cftzh").toString())) {
                criteria.andEqualTo("cftzh", parmMap.get("cftzh").toString().trim());
                flag = true;
            }
            if (parmMap.get("wf_rz_sdrs") != null && StringUtils.isNotBlank(parmMap.get("wf_rz_sdrs").toString())) {
                criteria.andEqualTo("wf_rz_sdrs", parmMap.get("wf_rz_sdrs").toString().trim());
                flag = true;
            }
            if (parmMap.get("zhjysj") != null && StringUtils.isNotBlank(parmMap.get("zhjysj").toString())) {
                criteria.andEqualTo("zhjysj", parmMap.get("zhjysj").toString().trim());
                flag = true;
            }
            if (parmMap.get("zhjyqk") != null && StringUtils.isNotBlank(parmMap.get("zhjyqk").toString())) {
                criteria.andEqualTo("zhjyqk", parmMap.get("zhjyqk").toString().trim());
                flag = true;
            }
            if (parmMap.get("mx") != null && StringUtils.isNotBlank(parmMap.get("mx").toString())) {
                criteria.andEqualTo("mx", parmMap.get("mx").toString().trim());
                flag = true;
            }
            if (parmMap.get("bdsjhmx") != null && StringUtils.isNotBlank(parmMap.get("bdsjhmx").toString())) {
                criteria.andEqualTo("bdsjhmx", parmMap.get("bdsjhmx").toString().trim());
                flag = true;
            }
            if (parmMap.get("khzj") != null && StringUtils.isNotBlank(parmMap.get("khzj").toString())) {
                criteria.andEqualTo("khzj", parmMap.get("khzj").toString().trim());
                flag = true;
            }
            if (parmMap.get("khr") != null && StringUtils.isNotBlank(parmMap.get("khr").toString())) {
                criteria.andEqualTo("khr", parmMap.get("khr").toString().trim());
                flag = true;
            }
            if (parmMap.get("sdzjgxrs") != null && StringUtils.isNotBlank(parmMap.get("sdzjgxrs").toString())) {
                criteria.andEqualTo("sdzjgxrs", parmMap.get("sdzjgxrs").toString().trim());
                flag = true;
            }
            if (parmMap.get("rz_rs") != null && StringUtils.isNotBlank(parmMap.get("rz_rs").toString())) {
                criteria.andEqualTo("rz_rs", parmMap.get("rz_rs").toString().trim());
                flag = true;
            }
            if (parmMap.get("rz_sdrs") != null && StringUtils.isNotBlank(parmMap.get("rz_sdrs").toString())) {
                criteria.andEqualTo("rz_sdrs", parmMap.get("rz_sdrs").toString().trim());
                flag = true;
            }
            if (parmMap.get("rz_je") != null && StringUtils.isNotBlank(parmMap.get("rz_je").toString())) {
                criteria.andEqualTo("rz_je", parmMap.get("rz_je").toString().trim());
                flag = true;
            }
            if (parmMap.get("rz_cs") != null && StringUtils.isNotBlank(parmMap.get("rz_cs").toString())) {
                criteria.andEqualTo("rz_cs", parmMap.get("rz_cs").toString().trim());
                flag = true;
            }
            if (parmMap.get("cz_cs") != null && StringUtils.isNotBlank(parmMap.get("cz_cs").toString())) {
                criteria.andEqualTo("cz_cs", parmMap.get("cz_cs").toString().trim());
                flag = true;
            }
            if (parmMap.get("cz_sdrs") != null && StringUtils.isNotBlank(parmMap.get("cz_sdrs").toString())) {
                criteria.andEqualTo("cz_sdrs", parmMap.get("cz_sdrs").toString().trim());
                flag = true;
            }
            if (parmMap.get("cz_je") != null && StringUtils.isNotBlank(parmMap.get("cz_je").toString())) {
                criteria.andEqualTo("cz_je", parmMap.get("cz_je").toString().trim());
                flag = true;
            }
            if (parmMap.get("ydzjgxrs") != null && StringUtils.isNotBlank(parmMap.get("ydzjgxrs").toString())) {
                criteria.andEqualTo("ydzjgxrs", parmMap.get("ydzjgxrs").toString().trim());
                flag = true;
            }
            if (parmMap.get("wxh") != null && StringUtils.isNotBlank(parmMap.get("wxh").toString())) {
                criteria.andEqualTo("wxh", parmMap.get("wxh").toString().trim());
                flag = true;
            }
            if (parmMap.get("xjx") != null && StringUtils.isNotBlank(parmMap.get("xjx").toString())) {
                criteria.andEqualTo("xjx", parmMap.get("xjx").toString().trim());
                flag = true;
            }
            if (parmMap.get("sjx") != null && StringUtils.isNotBlank(parmMap.get("sjx").toString())) {
                criteria.andEqualTo("sjx", parmMap.get("sjx").toString().trim());
                flag = true;
            }
            if (parmMap.get("sdhy") != null && StringUtils.isNotBlank(parmMap.get("sdhy").toString())) {
                criteria.andEqualTo("sdhy", parmMap.get("sdhy").toString().trim());
                flag = true;
            }
            if (parmMap.get("wxtljfmx") != null && StringUtils.isNotBlank(parmMap.get("wxtljfmx").toString())) {
                criteria.andEqualTo("wxtljfmx", parmMap.get("wxtljfmx").toString().trim());
                flag = true;
            }
            if (parmMap.get("wxtljf") != null && StringUtils.isNotBlank(parmMap.get("wxtljf").toString())) {
                criteria.andEqualTo("wxtljf", parmMap.get("wxtljf").toString().trim());
                flag = true;
            }
            if (parmMap.get("wxnc") != null && StringUtils.isNotBlank(parmMap.get("wxnc").toString())) {
                criteria.andEqualTo("wxnc", parmMap.get("wxnc").toString().trim());
                flag = true;
            }
            if (parmMap.get("khzl") != null && StringUtils.isNotBlank(parmMap.get("khzl").toString())) {
                criteria.andEqualTo("khzl", parmMap.get("khzl").toString().trim());
                flag = true;
            }
            if (parmMap.get("ypbg") != null && StringUtils.isNotBlank(parmMap.get("ypbg").toString())) {
                criteria.andEqualTo("ypbg", parmMap.get("ypbg").toString().trim());
                flag = true;
            }
            if (parmMap.get("jxsjfms") != null && StringUtils.isNotBlank(parmMap.get("jxsjfms").toString())) {
                criteria.andEqualTo("jxsjfms", parmMap.get("jxsjfms").toString().trim());
                flag = true;
            }
            if (parmMap.get("jxsjf") != null && StringUtils.isNotBlank(parmMap.get("jxsjf").toString())) {
                criteria.andEqualTo("jxsjf", parmMap.get("jxsjf").toString().trim());
                flag = true;
            }
            if (parmMap.get("lyaj") != null && StringUtils.isNotBlank(parmMap.get("lyaj").toString())) {
                criteria.andEqualTo("lyaj", parmMap.get("lyaj").toString().trim());
                flag = true;
            }
            if (parmMap.get("smsj") != null && StringUtils.isNotBlank(parmMap.get("smsj").toString())) {
                criteria.andEqualTo("smsj", parmMap.get("smsj").toString().trim());
                flag = true;
            }
            if (parmMap.get("zxckwz") != null && StringUtils.isNotBlank(parmMap.get("zxckwz").toString())) {
                criteria.andEqualTo("zxckwz", parmMap.get("zxckwz").toString().trim());
                flag = true;
            }
            if (parmMap.get("yjckwz") != null && StringUtils.isNotBlank(parmMap.get("yjckwz").toString())) {
                criteria.andEqualTo("yjckwz", parmMap.get("yjckwz").toString().trim());
                flag = true;
            }
            if (parmMap.get("sjdz") != null && StringUtils.isNotBlank(parmMap.get("sjdz").toString())) {
                criteria.andEqualTo("sjdz", parmMap.get("sjdz").toString().trim());
                flag = true;
            }
            if (parmMap.get("albqs") != null && StringUtils.isNotBlank(parmMap.get("albqs").toString())) {
                criteria.andEqualTo("albqs", parmMap.get("albqs").toString().trim());
                flag = true;
            }
            if (parmMap.get("skgljf_wf") != null && StringUtils.isNotBlank(parmMap.get("skgljf_wf").toString())) {
                criteria.andEqualTo("skgljf_wf", parmMap.get("skgljf_wf").toString().trim());
                flag = true;
            }
            if (parmMap.get("skgljf_df") != null && StringUtils.isNotBlank(parmMap.get("skgljf_df").toString())) {
                criteria.andEqualTo("skgljf_df", parmMap.get("skgljf_df").toString().trim());
                flag = true;
            }
            if (parmMap.get("skgljf_max") != null && StringUtils.isNotBlank(parmMap.get("skgljf_max").toString())) {
                criteria.andEqualTo("skgljf_max", parmMap.get("skgljf_max").toString().trim());
                flag = true;
            }
            if (parmMap.get("jfmsx") != null && StringUtils.isNotBlank(parmMap.get("jfmsx").toString())) {
                criteria.andEqualTo("jfmsx", parmMap.get("jfmsx").toString().trim());
                flag = true;
            }
            if (parmMap.get("jfx") != null && StringUtils.isNotBlank(parmMap.get("jfx").toString())) {
                criteria.andEqualTo("jfx", parmMap.get("jfx").toString().trim());
                flag = true;
            }
            if (parmMap.get("ypbg1") != null && StringUtils.isNotBlank(parmMap.get("ypbg1").toString())) {
                criteria.andEqualTo("ypbg1", parmMap.get("ypbg1").toString().trim());
                flag = true;
            }
            //一个条件都没有的时候给一个默认条件
            if (!flag) {

            }
            PageInfo pageInfo = this.queryPageInfoByExample(example, pageNum, pageSize);

            List<VZhZfzhdwdtj1> list = pageInfo.getList();
            VZhWfSjjh vZhWfSjjh;
            VZhWfXjjh vZhWfXjjh;
            VZhDfzfzhsjtjx vZhDfzfzhsjtjx;
            VZhDfzfzhxjtjx vZhDfzfzhxjtjx;
            for (VZhZfzhdwdtj1 vZhZfzhdwdtj1 : list) {
                vZhWfSjjh = vZhWfSjjhService.queryClob(vZhZfzhdwdtj1.getCftzh());
                vZhWfXjjh = vZhWfXjjhService.queryClob(vZhZfzhdwdtj1.getCftzh());
                vZhDfzfzhsjtjx = vZhDfzfzhsjtjxService.queryClob(vZhZfzhdwdtj1.getCftzh());
                vZhDfzfzhxjtjx = vZhDfzfzhxjtjxService.queryClob(vZhZfzhdwdtj1.getCftzh());
                vZhWfSjjh = vZhWfSjjh == null ? new VZhWfSjjh() : vZhWfSjjh;
                vZhWfXjjh = vZhWfXjjh == null ? new VZhWfXjjh() : vZhWfXjjh;
                vZhDfzfzhsjtjx = vZhDfzfzhsjtjx == null ? new VZhDfzfzhsjtjx() : vZhDfzfzhsjtjx;
                vZhDfzfzhxjtjx = vZhDfzfzhxjtjx == null ? new VZhDfzfzhxjtjx() : vZhDfzfzhxjtjx;
                vZhZfzhdwdtj1.setXjx(vZhWfXjjh.getWfxj() + vZhDfzfzhxjtjx.getDfxj());
                vZhZfzhdwdtj1.setSjx(vZhWfSjjh.getWfsj() + vZhDfzfzhsjtjx.getDfsj());
            }

            ResultDto result = new ResultDto();
            result.setRows(pageInfo.getList());
            result.setPage(pageInfo.getPageNum());
            //result.setTotal(pageInfo.getPages());
            result.setTotal((int) pageInfo.getTotal());
            return result;
        } else {
            logger.error("VZhZfzhdwdtj1ServiceImpl.queryListByPage时parmMap数据为空。");
            throw new AppRuntimeException("VZhZfzhdwdtj1ServiceImpl.queryListByPage时parmMap数据为空。");
        }
    }
}
