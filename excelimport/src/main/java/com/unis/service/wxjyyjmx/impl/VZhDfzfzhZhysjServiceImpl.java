package com.unis.service.wxjyyjmx.impl;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.unis.dto.ResultDto;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.wxjyyjmx.VZhDfzfzhZhysjMapper;
import com.unis.model.wxjyyjmx.VZhDfzfzhZhysj;
import com.unis.service.wxjyyjmx.VZhDfzfzhZhysjService;
import com.unis.common.config.DS;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see VZhDfzfzhZhysjService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2020-05-06
 */
@Service("vZhDfzfzhZhysjService")
public class VZhDfzfzhZhysjServiceImpl extends BaseServiceImpl implements VZhDfzfzhZhysjService {
	private static final Logger logger = LoggerFactory.getLogger(VZhDfzfzhZhysjServiceImpl.class);
    @Autowired
    private VZhDfzfzhZhysjMapper vZhDfzfzhZhysjMapper;

    /**
     * @see VZhDfzfzhZhysjService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(vZhDfzfzhZhysjMapper.selectByExample(example));
    	}else{
    		logger.error("VZhDfzfzhZhysjServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("VZhDfzfzhZhysjServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see VZhDfzfzhZhysjService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    @DS("datasource2")
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(VZhDfzfzhZhysj.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("dfzfzh")!=null && StringUtils.isNotBlank(parmMap.get("dfzfzh").toString())){
				criteria.andEqualTo("dfzfzh",parmMap.get("dfzfzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jysj")!=null && StringUtils.isNotBlank(parmMap.get("jysj").toString())){
				criteria.andEqualTo("jysj",parmMap.get("jysj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("dfzfzhzhhysj")!=null && StringUtils.isNotBlank(parmMap.get("dfzfzhzhhysj").toString())){
				criteria.andEqualTo("dfzfzhzhhysj",parmMap.get("dfzfzhzhhysj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("wf_khzj")!=null && StringUtils.isNotBlank(parmMap.get("wf_khzj").toString())){
				criteria.andEqualTo("wf_khzj",parmMap.get("wf_khzj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jylx")!=null && StringUtils.isNotBlank(parmMap.get("jylx").toString())){
				criteria.andEqualTo("jylx",parmMap.get("jylx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("wf_ly")!=null && StringUtils.isNotBlank(parmMap.get("wf_ly").toString())){
				criteria.andEqualTo("wf_ly",parmMap.get("wf_ly").toString().trim());
				flag = true;
			}
    		if(parmMap.get("wf_khxm")!=null && StringUtils.isNotBlank(parmMap.get("wf_khxm").toString())){
				criteria.andEqualTo("wf_khxm",parmMap.get("wf_khxm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jyje")!=null && StringUtils.isNotBlank(parmMap.get("jyje").toString())){
				criteria.andEqualTo("jyje",parmMap.get("jyje").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zhhyqk")!=null && StringUtils.isNotBlank(parmMap.get("zhhyqk").toString())){
				criteria.andEqualTo("zhhyqk",parmMap.get("zhhyqk").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("VZhDfzfzhZhysjServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("VZhDfzfzhZhysjServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
