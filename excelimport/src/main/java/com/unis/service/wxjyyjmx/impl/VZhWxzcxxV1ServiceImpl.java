package com.unis.service.wxjyyjmx.impl;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.unis.dto.ResultDto;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.wxjyyjmx.VZhWxzcxxV1Mapper;
import com.unis.model.wxjyyjmx.VZhWxzcxxV1;
import com.unis.service.wxjyyjmx.VZhWxzcxxV1Service;
import com.unis.common.config.DS;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see VZhWxzcxxV1Service
 * </pre>
 *
 * @author Qx
 * @version 1.0
 * @since 2020-05-06
 */
@Service("vZhWxzcxxV1Service")
public class VZhWxzcxxV1ServiceImpl extends BaseServiceImpl implements VZhWxzcxxV1Service {
	private static final Logger logger = LoggerFactory.getLogger(VZhWxzcxxV1ServiceImpl.class);
    @Autowired
    private VZhWxzcxxV1Mapper vZhWxzcxxV1Mapper;

    /**
     * @see VZhWxzcxxV1Service#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(vZhWxzcxxV1Mapper.selectByExample(example));
    	}else{
    		logger.error("VZhWxzcxxV1ServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("VZhWxzcxxV1ServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see VZhWxzcxxV1Service#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    @DS("datasource2")
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(VZhWxzcxxV1.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("bdyhk")!=null && StringUtils.isNotBlank(parmMap.get("bdyhk").toString())){
				criteria.andEqualTo("bdyhk",parmMap.get("bdyhk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bz")!=null && StringUtils.isNotBlank(parmMap.get("bz").toString())){
				criteria.andEqualTo("bz",parmMap.get("bz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("khztzjhm")!=null && StringUtils.isNotBlank(parmMap.get("khztzjhm").toString())){
				criteria.andEqualTo("khztzjhm",parmMap.get("khztzjhm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zjyxq")!=null && StringUtils.isNotBlank(parmMap.get("zjyxq").toString())){
				criteria.andEqualTo("zjyxq",parmMap.get("zjyxq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cxfksm")!=null && StringUtils.isNotBlank(parmMap.get("cxfksm").toString())){
				criteria.andEqualTo("cxfksm",parmMap.get("cxfksm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fkjgmc")!=null && StringUtils.isNotBlank(parmMap.get("fkjgmc").toString())){
				criteria.andEqualTo("fkjgmc",parmMap.get("fkjgmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cxzh")!=null && StringUtils.isNotBlank(parmMap.get("cxzh").toString())){
				criteria.andEqualTo("cxzh",parmMap.get("cxzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("wxh")!=null && StringUtils.isNotBlank(parmMap.get("wxh").toString())){
				criteria.andEqualTo("wxh",parmMap.get("wxh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("shhm")!=null && StringUtils.isNotBlank(parmMap.get("shhm").toString())){
				criteria.andEqualTo("shhm",parmMap.get("shhm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ly")!=null && StringUtils.isNotBlank(parmMap.get("ly").toString())){
				criteria.andEqualTo("ly",parmMap.get("ly").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lyfj")!=null && StringUtils.isNotBlank(parmMap.get("lyfj").toString())){
				criteria.andEqualTo("lyfj",parmMap.get("lyfj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("khztxm")!=null && StringUtils.isNotBlank(parmMap.get("khztxm").toString())){
				criteria.andEqualTo("khztxm",parmMap.get("khztxm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("shmc")!=null && StringUtils.isNotBlank(parmMap.get("shmc").toString())){
				criteria.andEqualTo("shmc",parmMap.get("shmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("khztzjlx")!=null && StringUtils.isNotBlank(parmMap.get("khztzjlx").toString())){
				criteria.andEqualTo("khztzjlx",parmMap.get("khztzjlx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qqh")!=null && StringUtils.isNotBlank(parmMap.get("qqh").toString())){
				criteria.andEqualTo("qqh",parmMap.get("qqh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bdsjh")!=null && StringUtils.isNotBlank(parmMap.get("bdsjh").toString())){
				criteria.andEqualTo("bdsjh",parmMap.get("bdsjh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sj")!=null && StringUtils.isNotBlank(parmMap.get("sj").toString())){
				criteria.andEqualTo("sj",parmMap.get("sj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zcxxlrsj")!=null && StringUtils.isNotBlank(parmMap.get("zcxxlrsj").toString())){
				criteria.andEqualTo("zcxxlrsj",parmMap.get("zcxxlrsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cxzh_sfssfz")!=null && StringUtils.isNotBlank(parmMap.get("cxzh_sfssfz").toString())){
				criteria.andEqualTo("cxzh_sfssfz",parmMap.get("cxzh_sfssfz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("wxzh_x")!=null && StringUtils.isNotBlank(parmMap.get("wxzh_x").toString())){
				criteria.andEqualTo("wxzh_x",parmMap.get("wxzh_x").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qczd_x")!=null && StringUtils.isNotBlank(parmMap.get("qczd_x").toString())){
				criteria.andEqualTo("qczd_x",parmMap.get("qczd_x").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfzj6")!=null && StringUtils.isNotBlank(parmMap.get("sfzj6").toString())){
				criteria.andEqualTo("sfzj6",parmMap.get("sfzj6").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gwhj")!=null && StringUtils.isNotBlank(parmMap.get("gwhj").toString())){
				criteria.andEqualTo("gwhj",parmMap.get("gwhj").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("VZhWxzcxxV1ServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("VZhWxzcxxV1ServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
