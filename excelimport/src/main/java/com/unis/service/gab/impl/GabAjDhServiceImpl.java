package com.unis.service.gab.impl;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.gab.GabAjDhMapper;
import com.unis.model.gab.GabAjDh;
import com.unis.service.gab.GabAjDhService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see GabAjDhService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-06-10
 */
@Service("gabAjDhService")
public class GabAjDhServiceImpl extends BaseServiceImpl implements GabAjDhService {
	private static final Logger logger = LoggerFactory.getLogger(GabAjDhServiceImpl.class);
    @Autowired
    private GabAjDhMapper gabAjDhMapper;


    /**
     * @see GabAjDhService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return gabAjDhMapper.selectCountByExample(example);
    	}else{
    		logger.error("GabAjDhServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("GabAjDhServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see GabAjDhService#queryListByExample(Example example)
     */
    @Override
    public List<GabAjDh> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return gabAjDhMapper.selectByExample(example);
    	}else{
    		logger.error("GabAjDhServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("GabAjDhServiceImpl.queryListByExample时example数据为空。");
    	}
    }

	@Override
	public List<GabAjDh> queryListByAjid(String ajid) throws Exception {
		if(StringUtils.isBlank(ajid)){
			logger.error("GabAjDhServiceImpl.queryListByAjid时ajid为空。");
			throw new AppRuntimeException("GabAjDhServiceImpl.queryListByAjid时ajid为空。");
		}else {
			Example example = new Example(GabAjDh.class);
			example.setOrderByClause("xh");
			Example.Criteria criteria = example.createCriteria();
			criteria.andEqualTo("ajid",ajid);
			return queryListByExample(example);
		}
	}
}
