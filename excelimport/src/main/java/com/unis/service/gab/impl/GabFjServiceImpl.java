package com.unis.service.gab.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.gab.GabFjMapper;
import com.unis.model.gab.GabFj;
import com.unis.service.gab.GabFjService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see GabFjService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-06-10
 */
@Service("gabFjService")
public class GabFjServiceImpl extends BaseServiceImpl implements GabFjService {
	private static final Logger logger = LoggerFactory.getLogger(GabFjServiceImpl.class);
    @Autowired
    private GabFjMapper gabFjMapper;
    /**
     * @see GabFjService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return gabFjMapper.selectCountByExample(example);
    	}else{
    		logger.error("GabFjServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("GabFjServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see GabFjService#queryListByExample(Example example)
     */
    @Override
    public List<GabFj> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return gabFjMapper.selectByExample(example);
    	}else{
    		logger.error("GabFjServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("GabFjServiceImpl.queryListByExample时example数据为空。");
    	}
    }

	@Override
	public List<GabFj> queryListByYwid(String ywid) throws Exception {
		if(StringUtils.isBlank(ywid)){
			logger.error("GabFjServiceImpl.queryListByYwid时ywid为空。");
			throw new AppRuntimeException("GabFjServiceImpl.queryListByYwid时ywid为空。");
		}else{
			Example example = new Example(GabFj.class);
			example.setOrderByClause("fjid");
			Example.Criteria criteria = example.createCriteria();
			criteria.andEqualTo("ywid",ywid);

			return queryListByExample(example);
		}
	}
}
