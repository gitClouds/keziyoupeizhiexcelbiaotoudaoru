package com.unis.service.gab.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.gab.GabAjSayhkMapper;
import com.unis.model.gab.GabAjSayhk;
import com.unis.service.gab.GabAjSayhkService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see GabAjSayhkService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-06-10
 */
@Service("gabAjSayhkService")
public class GabAjSayhkServiceImpl extends BaseServiceImpl implements GabAjSayhkService {
	private static final Logger logger = LoggerFactory.getLogger(GabAjSayhkServiceImpl.class);
    @Autowired
    private GabAjSayhkMapper gabAjSayhkMapper;

    /**
     * @see GabAjSayhkService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return gabAjSayhkMapper.selectCountByExample(example);
    	}else{
    		logger.error("GabAjSayhkServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("GabAjSayhkServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see GabAjSayhkService#queryListByExample(Example example)
     */
    @Override
    public List<GabAjSayhk> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return gabAjSayhkMapper.selectByExample(example);
    	}else{
    		logger.error("GabAjSayhkServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("GabAjSayhkServiceImpl.queryListByExample时example数据为空。");
    	}
    }

	@Override
	public List<GabAjSayhk> queryListByAjid(String ajid) throws Exception {
		if(StringUtils.isBlank(ajid)){
			logger.error("GabAjSayhkServiceImpl.queryListByAjid时ajid为空。");
			throw new AppRuntimeException("GabAjSayhkServiceImpl.queryListByAjid时ajid为空。");
		}else{
			Example example = new Example(GabAjSayhk.class);
			example.setOrderByClause("xh");
			Example.Criteria criteria = example.createCriteria();
			criteria.andEqualTo("ajid",ajid);
			return queryListByExample(example);
		}
	}
}
