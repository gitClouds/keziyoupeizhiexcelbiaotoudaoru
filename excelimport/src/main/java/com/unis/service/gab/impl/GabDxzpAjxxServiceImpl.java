package com.unis.service.gab.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.gab.GabDxzpAjxxMapper;
import com.unis.model.gab.GabDxzpAjxx;
import com.unis.service.gab.GabDxzpAjxxService;
import com.unis.common.exception.app.AppRuntimeException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * <pre>
 * @see GabDxzpAjxxService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-06-10
 */
@Service("gabDxzpAjxxService")
public class GabDxzpAjxxServiceImpl extends BaseServiceImpl implements GabDxzpAjxxService {
	private static final Logger logger = LoggerFactory.getLogger(GabDxzpAjxxServiceImpl.class);
    @Autowired
    private GabDxzpAjxxMapper gabDxzpAjxxMapper;
    /**
     * @see GabDxzpAjxxService#queryGabDxzpAjxxByPrimaryKey(String pk)
     */
    @Override
    public GabDxzpAjxx queryGabDxzpAjxxByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("GabDxzpAjxxServiceImpl.queryGabDxzpAjxxByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("GabDxzpAjxxServiceImpl.queryGabDxzpAjxxByPrimaryKey时pk为空。");
    	}else{
    		return gabDxzpAjxxMapper.selectByPrimaryKey(pk);
    	}
    }

	@Override
	public List<Map<String, String>> queryMacCbAjxxByMacs(String[] macs) {
    	if (macs!=null && macs.length>0){
			return gabDxzpAjxxMapper.queryMacCbAjxxByMacs(macs);
		}
		return null;
	}

}
