package com.unis.service.gab;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import com.unis.model.gab.GabAjWl;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.unis.service.BaseService;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;

/**
 * <pre>
 * TODO
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-06-10
 */
public interface GabAjWlService extends BaseService {
	/**
     * 根据条件获取条数
     * 
     * @param example
     * @throws Exception
     */
	int queryCountByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合
     * 
     * @param example
     * @throws Exception
     */
	List<GabAjWl> queryListByExample(Example example) throws Exception;
    List<GabAjWl> queryListByAjid(String ajid) throws Exception;
}
