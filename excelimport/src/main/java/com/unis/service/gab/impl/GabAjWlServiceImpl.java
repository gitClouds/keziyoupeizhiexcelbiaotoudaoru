package com.unis.service.gab.impl;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.gab.GabAjWlMapper;
import com.unis.model.gab.GabAjWl;
import com.unis.service.gab.GabAjWlService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see GabAjWlService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-06-10
 */
@Service("gabAjWlService")
public class GabAjWlServiceImpl extends BaseServiceImpl implements GabAjWlService {
	private static final Logger logger = LoggerFactory.getLogger(GabAjWlServiceImpl.class);
    @Autowired
    private GabAjWlMapper gabAjWlMapper;


    /**
     * @see GabAjWlService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return gabAjWlMapper.selectCountByExample(example);
    	}else{
    		logger.error("GabAjWlServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("GabAjWlServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see GabAjWlService#queryListByExample(Example example)
     */
    @Override
    public List<GabAjWl> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return gabAjWlMapper.selectByExample(example);
    	}else{
    		logger.error("GabAjWlServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("GabAjWlServiceImpl.queryListByExample时example数据为空。");
    	}
    }

	@Override
	public List<GabAjWl> queryListByAjid(String ajid) throws Exception {
		if(StringUtils.isBlank(ajid)){
			logger.error("GabAjWlServiceImpl.queryListByAjid时ajid为空。");
			throw new AppRuntimeException("GabAjWlServiceImpl.queryListByAjid时ajid为空。");
		}else{
			Example example = new Example(GabAjWl.class);
			example.setOrderByClause("xh");
			Example.Criteria criteria = example.createCriteria();
			criteria.andEqualTo("ajid",ajid);
			return queryListByExample(example);
		}
	}
}
