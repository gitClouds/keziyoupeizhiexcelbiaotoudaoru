package com.unis.service.gab;

import java.util.List;
import com.unis.model.gab.GabAjDh;
import tk.mybatis.mapper.entity.Example;
import com.unis.service.BaseService;

/**
 * <pre>
 * TODO
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-06-10
 */
public interface GabAjDhService extends BaseService {
	/**
     * 根据条件获取条数
     * 
     * @param example
     * @throws Exception
     */
	int queryCountByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合
     * 
     * @param example
     * @throws Exception
     */
	List<GabAjDh> queryListByExample(Example example) throws Exception;

    List<GabAjDh> queryListByAjid(String ajid) throws Exception;
}
