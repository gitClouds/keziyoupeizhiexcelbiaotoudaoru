package com.unis.service.gab.impl;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.gab.GabAjQqMapper;
import com.unis.model.gab.GabAjQq;
import com.unis.service.gab.GabAjQqService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see GabAjQqService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-06-10
 */
@Service("gabAjQqService")
public class GabAjQqServiceImpl extends BaseServiceImpl implements GabAjQqService {
	private static final Logger logger = LoggerFactory.getLogger(GabAjQqServiceImpl.class);
    @Autowired
    private GabAjQqMapper gabAjQqMapper;

    /**
     * @see GabAjQqService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return gabAjQqMapper.selectCountByExample(example);
    	}else{
    		logger.error("GabAjQqServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("GabAjQqServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see GabAjQqService#queryListByExample(Example example)
     */
    @Override
    public List<GabAjQq> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return gabAjQqMapper.selectByExample(example);
    	}else{
    		logger.error("GabAjQqServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("GabAjQqServiceImpl.queryListByExample时example数据为空。");
    	}
    }

	@Override
	public List<GabAjQq> queryListByAjid(String ajid) throws Exception {
		if(StringUtils.isBlank(ajid)){
			logger.error("GabAjQqServiceImpl.queryListByAjid时ajid为空。");
			throw new AppRuntimeException("GabAjQqServiceImpl.queryListByAjid时ajid为空。");
		}else {
			Example example = new Example(GabAjQq.class);
			example.setOrderByClause("xh");
			Example.Criteria criteria = example.createCriteria();
			criteria.andEqualTo("ajid",ajid);
			return queryListByExample(example);
		}
	}
}
