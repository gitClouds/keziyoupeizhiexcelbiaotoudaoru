package com.unis.service.gab.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.unis.common.exception.app.AppRuntimeException;
import com.unis.dto.ResultDto;
import com.unis.mapper.gab.GabDyzyYhkQqMapper;
import com.unis.model.gab.GabDyzyYhkQq;
import com.unis.service.gab.GabDyzyYhkQqService;
import com.unis.service.impl.BaseServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2019/6/5.
 */
@Service("gabDyzyYhkQqService")
public class GabDyzyYhkQqServiceImpl extends BaseServiceImpl implements GabDyzyYhkQqService {
    private static final Logger logger = LoggerFactory.getLogger(GabDyzyYhkQqServiceImpl.class);
    @Autowired
    private GabDyzyYhkQqMapper gabDyzyYhkQqMapper;

    @Override
    public List<GabDyzyYhkQq> queryListByMac(String macdz) throws Exception {
        if (StringUtils.isNotBlank(macdz)){
            return gabDyzyYhkQqMapper.selectByMacdz(macdz);
        }else {
            logger.error("GabDyzyYhkQqServiceImpl.queryListByMac时macdz数据为空。");
            throw new AppRuntimeException("GabDyzyYhkQqServiceImpl.queryListByMac时macdz数据为空。");
        }
    }

    @Override
    public ResultDto queryListMacByPage(String macdz, int pageNum, int pageSize) throws Exception {
        ResultDto result = null;
        if (StringUtils.isNotBlank(macdz)){
            macdz = StringUtils.upperCase(StringUtils.trim(StringUtils.replace(StringUtils.replace(StringUtils.replace(macdz,":","")," ",""),"-","")));
            PageHelper.startPage(pageNum,pageSize);
            PageInfo pageInfo = new PageInfo(gabDyzyYhkQqMapper.selectByMacdz(macdz));

            result = new ResultDto();
            result.setRows(pageInfo.getList());
            result.setPage(pageInfo.getPageNum());
            //result.setTotal(pageInfo.getPages());
            result.setTotal((int)pageInfo.getTotal());
        }
        return result;
    }
}
