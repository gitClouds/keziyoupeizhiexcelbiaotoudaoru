package com.unis.service.gab;

import com.unis.model.gab.GabDxzpAjxx;
import com.unis.service.BaseService;

import java.util.List;
import java.util.Map;

/**
 * <pre>
 * TODO
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-06-10
 */
public interface GabDxzpAjxxService extends BaseService {
    /**
     * 根据pk主键获取对象
     * 
     * @param pk
     * @throws Exception
     */
    GabDxzpAjxx queryGabDxzpAjxxByPrimaryKey(String pk) throws Exception;

    List<Map<String,String>> queryMacCbAjxxByMacs(String[] macs);
}
