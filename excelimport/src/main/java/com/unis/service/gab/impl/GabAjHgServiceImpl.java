package com.unis.service.gab.impl;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.gab.GabAjHgMapper;
import com.unis.model.gab.GabAjHg;
import com.unis.service.gab.GabAjHgService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see GabAjHgService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-06-11
 */
@Service("gabAjHgService")
public class GabAjHgServiceImpl extends BaseServiceImpl implements GabAjHgService {
	private static final Logger logger = LoggerFactory.getLogger(GabAjHgServiceImpl.class);
    @Autowired
    private GabAjHgMapper gabAjHgMapper;
    /**
     * @see GabAjHgService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return gabAjHgMapper.selectCountByExample(example);
    	}else{
    		logger.error("GabAjHgServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("GabAjHgServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see GabAjHgService#queryListByExample(Example example)
     */
    @Override
    public List<GabAjHg> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return gabAjHgMapper.selectByExample(example);
    	}else{
    		logger.error("GabAjHgServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("GabAjHgServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see GabAjHgService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(gabAjHgMapper.selectByExample(example));
    	}else{
    		logger.error("GabAjHgServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("GabAjHgServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see GabAjHgService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(GabAjHg.class);
    		example.setOrderByClause("rksj DESC");
    		Example.Criteria criteria = example.createCriteria();
    		criteria.andEqualTo("yxx",1);
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("xh")!=null && StringUtils.isNotBlank(parmMap.get("xh").toString())){
				criteria.andEqualTo("xh",parmMap.get("xh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ajid")!=null && StringUtils.isNotBlank(parmMap.get("ajid").toString())){
				criteria.andEqualTo("ajid",parmMap.get("ajid").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fadwdm")!=null && StringUtils.isNotBlank(parmMap.get("fadwdm").toString())){
				criteria.andEqualTo("fadwdm",parmMap.get("fadwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fadwmc")!=null && StringUtils.isNotBlank(parmMap.get("fadwmc").toString())){
				criteria.andEqualTo("fadwmc",parmMap.get("fadwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jdms")!=null && StringUtils.isNotBlank(parmMap.get("jdms").toString())){
				criteria.andEqualTo("jdms",parmMap.get("jdms").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jzlb")!=null && StringUtils.isNotBlank(parmMap.get("jzlb").toString())){
				criteria.andEqualTo("jzlb",parmMap.get("jzlb").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ywxh")!=null && StringUtils.isNotBlank(parmMap.get("ywxh").toString())){
				criteria.andEqualTo("ywxh",parmMap.get("ywxh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jzczsj")!=null && StringUtils.isNotBlank(parmMap.get("jzczsj").toString())){
				criteria.andEqualTo("jzczsj",parmMap.get("jzczsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jzczr")!=null && StringUtils.isNotBlank(parmMap.get("jzczr").toString())){
				criteria.andEqualTo("jzczr",parmMap.get("jzczr").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jzczdwdm")!=null && StringUtils.isNotBlank(parmMap.get("jzczdwdm").toString())){
				criteria.andEqualTo("jzczdwdm",parmMap.get("jzczdwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jzczdwmc")!=null && StringUtils.isNotBlank(parmMap.get("jzczdwmc").toString())){
				criteria.andEqualTo("jzczdwmc",parmMap.get("jzczdwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfzx")!=null && StringUtils.isNotBlank(parmMap.get("sfzx").toString())){
				criteria.andEqualTo("sfzx",parmMap.get("sfzx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zxsj")!=null && StringUtils.isNotBlank(parmMap.get("zxsj").toString())){
				criteria.andEqualTo("zxsj",parmMap.get("zxsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zxr")!=null && StringUtils.isNotBlank(parmMap.get("zxr").toString())){
				criteria.andEqualTo("zxr",parmMap.get("zxr").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zxdwdm")!=null && StringUtils.isNotBlank(parmMap.get("zxdwdm").toString())){
				criteria.andEqualTo("zxdwdm",parmMap.get("zxdwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zxdwmc")!=null && StringUtils.isNotBlank(parmMap.get("zxdwmc").toString())){
				criteria.andEqualTo("zxdwmc",parmMap.get("zxdwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("GabAjHgServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("GabAjHgServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
