package com.unis.service.gab;

import com.unis.dto.ResultDto;
import com.unis.model.gab.GabDyzyYhkQq;
import com.unis.service.BaseService;

import java.util.List;

/**
 * <pre>
 * TODO
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-04-01
 */
public interface GabDyzyYhkQqService extends BaseService {

	List<GabDyzyYhkQq> queryListByMac(String macdz) throws Exception;
	ResultDto queryListMacByPage(String macdz, int pageNum, int pageSize) throws  Exception;
}
