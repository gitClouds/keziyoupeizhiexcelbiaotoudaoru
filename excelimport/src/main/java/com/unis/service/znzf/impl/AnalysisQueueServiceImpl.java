package com.unis.service.znzf.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.unis.common.exception.app.AppRuntimeException;
import com.unis.common.util.TemplateUtil;
import com.unis.mapper.znzf.AnalysisQueueMapper;
import com.unis.model.znzf.AnalysisQueue;
import com.unis.service.impl.BaseServiceImpl;
import com.unis.service.znzf.AnalysisQueueService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * <pre>
 * @see AnalysisQueueService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-04-17
 */
@Service("analysisQueueService")
public class AnalysisQueueServiceImpl extends BaseServiceImpl implements AnalysisQueueService {
	private static final Logger logger = LoggerFactory.getLogger(AnalysisQueueServiceImpl.class);
    @Autowired
    private AnalysisQueueMapper analysisQueueMapper;

    /**
     * @see AnalysisQueueService#insert(AnalysisQueue analysisQueue)
     */
    @Override
    public int insert(AnalysisQueue analysisQueue) throws Exception {
    	if (analysisQueue!=null){
	        analysisQueue.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return analysisQueueMapper.insertSelective(analysisQueue);
    	}else{
    		logger.error("AnalysisQueueServiceImpl.insert时analysisQueue数据为空。");
    		throw new AppRuntimeException("AnalysisQueueServiceImpl.insert时analysisQueue数据为空。");
    	}        
    }
	/**
     * @see AnalysisQueueService#insert(List< AnalysisQueue >)
     */
	@Override
	public int insert(List<AnalysisQueue> list) throws Exception {
		if (list!=null && !list.isEmpty() && list.size()>0){
			return analysisQueueMapper.insertBatch(list);
		}
		return 0;
	}

    /**
     * @see AnalysisQueueService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("AnalysisQueueServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("AnalysisQueueServiceImpl.delete时pk为空。");
    	}else{
    		return analysisQueueMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see AnalysisQueueService#updateByPrimaryKey(AnalysisQueue analysisQueue)
     */
    @Override
    public int updateByPrimaryKey(AnalysisQueue analysisQueue) throws Exception {
        if (analysisQueue!=null){
        	if(StringUtils.isBlank(analysisQueue.getPk())){
        		logger.error("AnalysisQueueServiceImpl.updateByPrimaryKey时analysisQueue.Pk为空。");
        		throw new AppRuntimeException("AnalysisQueueServiceImpl.updateByPrimaryKey时analysisQueue.Pk为空。");
        	}
	        return analysisQueueMapper.updateByPrimaryKeySelective(analysisQueue);
    	}else{
    		logger.error("AnalysisQueueServiceImpl.updateByPrimaryKey时analysisQueue数据为空。");
    		throw new AppRuntimeException("AnalysisQueueServiceImpl.updateByPrimaryKey时analysisQueue数据为空。");
    	}
    }
    /**
     * @see AnalysisQueueService#queryAnalysisQueueByPrimaryKey(String pk)
     */
    @Override
    public AnalysisQueue queryAnalysisQueueByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("AnalysisQueueServiceImpl.queryAnalysisQueueByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("AnalysisQueueServiceImpl.queryAnalysisQueueByPrimaryKey时pk为空。");
    	}else{
    		return analysisQueueMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see AnalysisQueueService#queryAsObject(AnalysisQueue analysisQueue)
     */
    @Override
    public AnalysisQueue queryAsObject(AnalysisQueue analysisQueue) throws Exception {
        if (analysisQueue!=null){
	        return analysisQueueMapper.selectOne(analysisQueue);
    	}else{
    		logger.error("AnalysisQueueServiceImpl.queryAsObject时analysisQueue数据为空。");
    		throw new AppRuntimeException("AnalysisQueueServiceImpl.queryAsObject时analysisQueue数据为空。");
    	}
    }
    
    /**
     * @see AnalysisQueueService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return analysisQueueMapper.selectCountByExample(example);
    	}else{
    		logger.error("AnalysisQueueServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("AnalysisQueueServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see AnalysisQueueService#queryListByExample(Example example)
     */
    @Override
    public List<AnalysisQueue> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return analysisQueueMapper.selectByExample(example);
    	}else{
    		logger.error("AnalysisQueueServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("AnalysisQueueServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see AnalysisQueueService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example, int pageNum, int pageSize) throws Exception {
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(analysisQueueMapper.selectByExample(example));
    	}else{
    		logger.error("AnalysisQueueServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("AnalysisQueueServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
}
