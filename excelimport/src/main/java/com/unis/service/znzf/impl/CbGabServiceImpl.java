package com.unis.service.znzf.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.unis.common.util.TimeUtil;
import com.unis.dto.ResultDto;
import com.unis.model.znzf.Cb;
import com.unis.service.system.CodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.CbGabMapper;
import com.unis.model.znzf.CbGab;
import com.unis.service.znzf.CbGabService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see CbGabService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-04-22
 */
@Service("cbGabService")
public class CbGabServiceImpl extends BaseServiceImpl implements CbGabService {
	private static final Logger logger = LoggerFactory.getLogger(CbGabServiceImpl.class);
    @Autowired
    private CbGabMapper cbGabMapper;
	@Autowired
	private CodeService codeService;
    /**
     * @see CbGabService#insert(CbGab cbGab)
     */
    @Override
    public int insert(CbGab cbGab) throws Exception {
    	if (cbGab!=null){
	        //cbGab.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return cbGabMapper.insertSelective(cbGab);
    	}else{
    		logger.error("CbGabServiceImpl.insert时cbGab数据为空。");
    		throw new AppRuntimeException("CbGabServiceImpl.insert时cbGab数据为空。");
    	}        
    }

    /**
     * @see CbGabService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("CbGabServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("CbGabServiceImpl.delete时pk为空。");
    	}else{
    		return cbGabMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see CbGabService#updateByPrimaryKey(CbGab cbGab)
     */
    @Override
    public int updateByPrimaryKey(CbGab cbGab) throws Exception {
        if (cbGab!=null){
        	if(StringUtils.isBlank(cbGab.getPk())){
        		logger.error("CbGabServiceImpl.updateByPrimaryKey时cbGab.Pk为空。");
        		throw new AppRuntimeException("CbGabServiceImpl.updateByPrimaryKey时cbGab.Pk为空。");
        	}
	        return cbGabMapper.updateByPrimaryKeySelective(cbGab);
    	}else{
    		logger.error("CbGabServiceImpl.updateByPrimaryKey时cbGab数据为空。");
    		throw new AppRuntimeException("CbGabServiceImpl.updateByPrimaryKey时cbGab数据为空。");
    	}
    }
    /**
     * @see CbGabService#queryCbGabByPrimaryKey(String pk)
     */
    @Override
    public CbGab queryCbGabByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("CbGabServiceImpl.queryCbGabByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("CbGabServiceImpl.queryCbGabByPrimaryKey时pk为空。");
    	}else{
    		return cbGabMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see CbGabService#queryAsObject(CbGab cbGab)
     */
    @Override
    public CbGab queryAsObject(CbGab cbGab) throws Exception {
        if (cbGab!=null){
	        return cbGabMapper.selectOne(cbGab);
    	}else{
    		logger.error("CbGabServiceImpl.queryAsObject时cbGab数据为空。");
    		throw new AppRuntimeException("CbGabServiceImpl.queryAsObject时cbGab数据为空。");
    	}
    }
    
    /**
     * @see CbGabService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return cbGabMapper.selectCountByExample(example);
    	}else{
    		logger.error("CbGabServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("CbGabServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see CbGabService#queryListByExample(Example example)
     */
    @Override
    public List<CbGab> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return cbGabMapper.selectByExample(example);
    	}else{
    		logger.error("CbGabServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("CbGabServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see CbGabService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(cbGabMapper.selectByExample(example));
    	}else{
    		logger.error("CbGabServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("CbGabServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
	 * @see CbGabService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
	 */
	@Override
	public PageInfo queryPageInfoByJjdpk(String jjdpk,int pageNum,int pageSize) throws Exception {
		if(StringUtils.isNotBlank(jjdpk)){
			PageHelper.startPage(pageNum,pageSize);
			return new PageInfo(cbGabMapper.queryCbGabByJjdpk(jjdpk));
		}else{
			logger.error("CbGabServiceImpl.queryListByExample时example数据为空。");
			throw new AppRuntimeException("CbGabServiceImpl.queryListByExample时example数据为空。");
		}
	}
	/**
	 * @see CbGabService#queryListByPage(Map parmMap,int pageNum,int pageSize)
	 */
	@Override
	public ResultDto queryListByJjdpk(String jjdpk, int pageNum, int pageSize) throws  Exception{
		if (StringUtils.isNotBlank(jjdpk)){
			PageInfo pageInfo = this.queryPageInfoByJjdpk(jjdpk,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
		}else{
			logger.error("CbGabServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("CbGabServiceImpl.queryListByPage时parmMap数据为空。");
		}
	}
	/**
     * @see CbGabService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(CbGab.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改

    		boolean flag = false;
    		/*if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}*/
    		if(parmMap.get("sertype")!=null && StringUtils.isNotBlank(parmMap.get("sertype").toString())){
				criteria.andEqualTo("sertype",parmMap.get("sertype").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jjdpk")!=null && StringUtils.isNotBlank(parmMap.get("jjdpk").toString())){
				criteria.andEqualTo("jjdpk",parmMap.get("jjdpk").toString().trim());
				flag = true;
			}
    		/*if(parmMap.get("ywsqbh")!=null && StringUtils.isNotBlank(parmMap.get("ywsqbh").toString())){
				criteria.andEqualTo("ywsqbh",parmMap.get("ywsqbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bankcode")!=null && StringUtils.isNotBlank(parmMap.get("bankcode").toString())){
				criteria.andEqualTo("bankcode",parmMap.get("bankcode").toString().trim());
				flag = true;
			}*/
    		if(parmMap.get("cardnumber")!=null && StringUtils.isNotBlank(parmMap.get("cardnumber").toString())){
				criteria.andEqualTo("cardnumber",parmMap.get("cardnumber").toString().trim());
				flag = true;
			}
    		/*if(parmMap.get("cardname")!=null && StringUtils.isNotBlank(parmMap.get("cardname").toString())){
				criteria.andEqualTo("cardname",parmMap.get("cardname").toString().trim());
				flag = true;
			}*/
    		if(parmMap.get("lrdwdm")!=null && StringUtils.isNotBlank(parmMap.get("lrdwdm").toString())){
				criteria.andEqualTo("lrdwdm",parmMap.get("lrdwdm").toString().trim());
				flag = true;
			}
    		/*if(parmMap.get("lrsj")!=null && StringUtils.isNotBlank(parmMap.get("lrsj").toString())){
				criteria.andEqualTo("lrsj",parmMap.get("lrsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}*/
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			List list = pageInfo.getList();
			//翻译：539-银行；542-三方；DWDM-单位代码
			list = codeService.translateList(list,CbGab.class,"lrdwdm:DWDM","bankcode:539","bankcode:542");
			result.setRows(list);
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("CbGabServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("CbGabServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
