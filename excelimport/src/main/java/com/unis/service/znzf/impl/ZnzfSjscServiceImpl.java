package com.unis.service.znzf.impl;

import java.util.List;
import java.util.Map;

import com.unis.common.secure.authc.UserInfo;
import com.unis.common.util.TemplateUtil;
import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.ZnzfSjscMapper;
import com.unis.model.znzf.ZnzfSjsc;
import com.unis.service.znzf.ZnzfSjscService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see ZnzfSjscService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-04-22
 */
@Service("znzfSjscService")
public class ZnzfSjscServiceImpl extends BaseServiceImpl implements ZnzfSjscService {
	private static final Logger logger = LoggerFactory.getLogger(ZnzfSjscServiceImpl.class);
    @Autowired
    private ZnzfSjscMapper znzfSjscMapper;

    /**
     * @see ZnzfSjscService#insert(ZnzfSjsc znzfSjsc)
     */
    @Override
    public int insert(ZnzfSjsc znzfSjsc) throws Exception {
    	if (znzfSjsc!=null){
	        znzfSjsc.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
			UserInfo info = this.getUserInfo();
			znzfSjsc.setLrdwdm(info.getJgdm());
			znzfSjsc.setLrdwmc(info.getJgmc());
			znzfSjsc.setLrrxm(info.getXm());
			znzfSjsc.setLrrjh(info.getJh());
	        return znzfSjscMapper.insertSelective(znzfSjsc);
    	}else{
    		logger.error("ZnzfSjscServiceImpl.insert时znzfSjsc数据为空。");
    		throw new AppRuntimeException("ZnzfSjscServiceImpl.insert时znzfSjsc数据为空。");
    	}        
    }

    /**
     * @see ZnzfSjscService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("ZnzfSjscServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("ZnzfSjscServiceImpl.delete时pk为空。");
    	}else{
    		return znzfSjscMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see ZnzfSjscService#updateByPrimaryKey(ZnzfSjsc znzfSjsc)
     */
    @Override
    public int updateByPrimaryKey(ZnzfSjsc znzfSjsc) throws Exception {
        if (znzfSjsc!=null){
        	if(StringUtils.isBlank(znzfSjsc.getPk())){
        		logger.error("ZnzfSjscServiceImpl.updateByPrimaryKey时znzfSjsc.Pk为空。");
        		throw new AppRuntimeException("ZnzfSjscServiceImpl.updateByPrimaryKey时znzfSjsc.Pk为空。");
        	}
	        return znzfSjscMapper.updateByPrimaryKeySelective(znzfSjsc);
    	}else{
    		logger.error("ZnzfSjscServiceImpl.updateByPrimaryKey时znzfSjsc数据为空。");
    		throw new AppRuntimeException("ZnzfSjscServiceImpl.updateByPrimaryKey时znzfSjsc数据为空。");
    	}
    }
    /**
     * @see ZnzfSjscService#queryZnzfSjscByPrimaryKey(String pk)
     */
    @Override
    public ZnzfSjsc queryZnzfSjscByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("ZnzfSjscServiceImpl.queryZnzfSjscByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("ZnzfSjscServiceImpl.queryZnzfSjscByPrimaryKey时pk为空。");
    	}else{
    		return znzfSjscMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see ZnzfSjscService#queryAsObject(ZnzfSjsc znzfSjsc)
     */
    @Override
    public ZnzfSjsc queryAsObject(ZnzfSjsc znzfSjsc) throws Exception {
        if (znzfSjsc!=null){
	        return znzfSjscMapper.selectOne(znzfSjsc);
    	}else{
    		logger.error("ZnzfSjscServiceImpl.queryAsObject时znzfSjsc数据为空。");
    		throw new AppRuntimeException("ZnzfSjscServiceImpl.queryAsObject时znzfSjsc数据为空。");
    	}
    }
    
    /**
     * @see ZnzfSjscService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return znzfSjscMapper.selectCountByExample(example);
    	}else{
    		logger.error("ZnzfSjscServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("ZnzfSjscServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see ZnzfSjscService#queryListByExample(Example example)
     */
    @Override
    public List<ZnzfSjsc> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return znzfSjscMapper.selectByExample(example);
    	}else{
    		logger.error("ZnzfSjscServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("ZnzfSjscServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see ZnzfSjscService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(znzfSjscMapper.selectByExample(example));
    	}else{
    		logger.error("ZnzfSjscServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("ZnzfSjscServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see ZnzfSjscService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(ZnzfSjsc.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jjdpk")!=null && StringUtils.isNotBlank(parmMap.get("jjdpk").toString())){
				criteria.andEqualTo("jjdpk",parmMap.get("jjdpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("datatype")!=null && StringUtils.isNotBlank(parmMap.get("datatype").toString())){
				criteria.andEqualTo("datatype",parmMap.get("datatype").toString().trim());
				flag = true;
			}
    		if(parmMap.get("mongopk")!=null && StringUtils.isNotBlank(parmMap.get("mongopk").toString())){
				criteria.andEqualTo("mongopk",parmMap.get("mongopk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("filename")!=null && StringUtils.isNotBlank(parmMap.get("filename").toString())){
				criteria.andEqualTo("filename",parmMap.get("filename").toString().trim());
				flag = true;
			}
    		if(parmMap.get("filesize")!=null && StringUtils.isNotBlank(parmMap.get("filesize").toString())){
				criteria.andEqualTo("filesize",parmMap.get("filesize").toString().trim());
				flag = true;
			}
    		if(parmMap.get("filetype")!=null && StringUtils.isNotBlank(parmMap.get("filetype").toString())){
				criteria.andEqualTo("filetype",parmMap.get("filetype").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrdwdm")!=null && StringUtils.isNotBlank(parmMap.get("lrdwdm").toString())){
				criteria.andEqualTo("lrdwdm",parmMap.get("lrdwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrdwmc")!=null && StringUtils.isNotBlank(parmMap.get("lrdwmc").toString())){
				criteria.andEqualTo("lrdwmc",parmMap.get("lrdwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrrxm")!=null && StringUtils.isNotBlank(parmMap.get("lrrxm").toString())){
				criteria.andEqualTo("lrrxm",parmMap.get("lrrxm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrrjh")!=null && StringUtils.isNotBlank(parmMap.get("lrrjh").toString())){
				criteria.andEqualTo("lrrjh",parmMap.get("lrrjh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("ZnzfSjscServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("ZnzfSjscServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
