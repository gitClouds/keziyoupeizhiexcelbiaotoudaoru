package com.unis.service.znzf.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.unis.common.enums.QqlxEnum;
import com.unis.common.secure.authc.UserInfo;
import com.unis.common.util.TemplateUtil;
import com.unis.common.util.TimeUtil;
import com.unis.dto.ResultDto;
import com.unis.model.fzzx.Jjd;
import com.unis.model.znzf.Jlt;
import com.unis.model.znzf.ZnzfQueue;
import com.unis.pojo.znzf.ReqModel;
import com.unis.service.fzzx.JjdService;
import com.unis.service.system.CodeService;
import com.unis.service.znzf.JltService;
import com.unis.service.znzf.ZnzfQueueService;
import com.unis.service.znzf.ZnzfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.YhkZfQqMapper;
import com.unis.model.znzf.YhkZfQq;
import com.unis.service.znzf.YhkZfQqService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see YhkZfQqService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-02-20
 */
@Service("yhkZfQqService")
public class YhkZfQqServiceImpl extends BaseServiceImpl implements YhkZfQqService {
	private static final Logger logger = LoggerFactory.getLogger(YhkZfQqServiceImpl.class);
    @Autowired
    private YhkZfQqMapper yhkZfQqMapper;
	@Autowired
	private ZnzfQueueService znzfQueueService;
	@Autowired
	private ZnzfService znzfService;
	@Autowired
	private CodeService codeService;
	@Autowired
	private JltService jltService;
	@Autowired
	private JjdService jjdService;
    /**
     * @see YhkZfQqService#insert(YhkZfQq yhkZfQq)
     */
    @Override
    public int insert(YhkZfQq yhkZfQq) throws Exception {
    	if (yhkZfQq!=null){
	        //yhkZfQq.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return yhkZfQqMapper.insertSelective(yhkZfQq);
    	}else{
    		logger.error("YhkZfQqServiceImpl.insert时yhkZfQq数据为空。");
    		throw new AppRuntimeException("YhkZfQqServiceImpl.insert时yhkZfQq数据为空。");
    	}        
    }

	@Override
	public String insertYhkZf(Jlt jlt) throws Exception {
		String result = null;
		//String mongoPk = znzfService.insertMongo("yhk","zf",bean);
		UserInfo user = this.getUserInfo();
		Date nowDate = this.getDbDate();

		Jjd jjd = jjdService.queryJjdByPrimaryKey(jlt.getJjdpk());

		Map bean = new HashMap();
		YhkZfQq qq = new YhkZfQq();

		qq.setPk(TemplateUtil.genUUID());
		qq.setLrdwdm(user.getJgdm());
		qq.setLrdwmc(user.getJgmc());
		qq.setLrrxm(user.getXm());
		qq.setLrrjh(user.getJh());
		qq.setJjdpk(jlt.getJjdpk());
		qq.setReason("电信诈骗涉案账号");
		qq.setSubjecttype((short) 1);
		qq.setBankcode(jlt.getBankcode());
		qq.setBankname(jlt.getBankname());
		qq.setAccountname(jlt.getAccountname());
		qq.setCardnumber(jlt.getCardnumber());
		qq.setStarttime(nowDate);
		qq.setExpiretime(TimeUtil.addDay(nowDate,2));

		qq.setTransferamount((long) 0);
		qq.setTransfertime(nowDate);

		bean.put("zhlx","银行卡");
		bean.put("zzje","0");
		bean.put("zzsj",TimeUtil.fmtDate(nowDate,null));
		bean.put("ajlb","电信诈骗");
		bean.put("zzfs","银行卡");
		bean.put("bankname",jlt.getBankname());
		bean.put("accountname",jlt.getAccountname());
		bean.put("cardnumber",jlt.getCardnumber());
		bean.put("shr",jjd!=null&&StringUtils.isNotBlank(jjd.getBarxm())?jjd.getBarxm():jlt.getAccountname());
		bean.put("zfCheckType","1");
		String mongoPk = znzfService.insertMongo("yhk","zf",bean);

		qq.setFlws(mongoPk);
		this.insert(qq);

		//增加序列
		ZnzfQueue queue = new ZnzfQueue();
		queue.setSerpk(qq.getPk());
		queue.setSertype(QqlxEnum.yhkQqlxZhifu.getKey());
		queue.setYxj((short) 2);
		queue.setJjdpk(jlt.getJjdpk());
		queue.setCardnumber(jlt.getCardnumber());
		queue.setLrdwdm(user.getJgdm());
		queue.setLrdwmc(user.getJgmc());
		queue.setLrrxm(user.getXm());
		queue.setLrrjh(user.getJh());
		queue.setBankcode(jlt.getBankcode());
		queue.setBankname(jlt.getBankname());
		znzfQueueService.insert(queue);

		result = qq.getPk();
		return result;
	}
	@Override
	public int insertYhkZf(ReqModel reqModel) throws Exception {
		int resultNumber = 0;
		Map bean =  null;
		if(reqModel!=null && reqModel.getBean()!=null && !reqModel.getBean().isEmpty() && reqModel.getBean().size()>0){
			List<Map> list = new ArrayList<>();
			UserInfo user = this.getUserInfo();
			Map<String,String> mongoMap = new HashMap();
			Map<String, Map<String, String>> dicMap = codeService.queryDicWithSpecCode("010107","010104");
			Date nowDate = this.getDbDate();

			Jjd jjd = jjdService.queryJjdByPrimaryKey(reqModel.getJjdPk());

			for (Map<String,String> map : reqModel.getBean()){
				bean = new HashMap();

				bean.put("shr",jjd!=null&&StringUtils.isNotBlank(jjd.getBarxm())?jjd.getBarxm():reqModel.getShr());
				bean.put("pk", TemplateUtil.genUUID());
				bean.put("lrdwdm",user.getJgdm());
				bean.put("lrdwmc",user.getJgmc());
				bean.put("lrrxm",user.getXm());
				bean.put("lrrjh",user.getJh());
				bean.put("jjdpk",reqModel.getJjdPk());
				bean.put("reason",reqModel.getSy());
				bean.put("subjecttype",Short.parseShort(map.get("zhlb")));
				bean.put("bankcode",map.get("zhjgdm"));
				bean.put("bankname",map.get("zhjgmc"));
				bean.put("accountname",map.get("zhxm"));//reqModel.getShr()
				String zh = StringUtils.isNotBlank(map.get("zh"))?map.get("zh"):"";
				bean.put("cardnumber",zh);
				bean.put("zhlx",map.get("zhlx"));

				bean.put("transferamount",Integer.parseInt(map.get("zcje")));
				bean.put("transfertime", TimeUtil.strToDate(map.get("zcsj"),null));

				bean.put("starttime",nowDate);
				bean.put("expiretime",TimeUtil.addDay(nowDate,2));

				bean.put("zzje",map.get("zcje"));
				bean.put("zzsj", map.get("zcsj"));

				bean.put("ajlb", dicMap.get("010107").get(reqModel.getAjlb()));
				bean.put("zzfs", dicMap.get("010104").get(map.get("zzfs")));
				bean.put("zfCheckType","1");
				String mongoPk = "";
				if (mongoMap!=null && !mongoMap.isEmpty() && mongoMap.size()>0 && StringUtils.isNotBlank(mongoMap.get(zh))){
					mongoPk = mongoMap.get(zh);
				}else{
					mongoPk = znzfService.insertMongo("yhk","zf",bean);
					mongoMap.put(zh,mongoPk);
				}
				bean.put("flws",mongoPk);
				//添加金流图，止付数据从金流图获取，此步忽略
				/*bean.put("nlevel",reqModel.getNlevel());
				bean.put("sertype",new Short("1"));
				bean.put("parentpk",reqModel.getParentpk());
				bean.put("parentzh",reqModel.getParentzh());*/
				//jltService.checkJltAndInsert(bean);

				list.add(bean);
			}

			if (list!=null && !list.isEmpty() && list.size()>0){

				resultNumber = yhkZfQqMapper.insertBatch(list);
				znzfQueueService.insertBatch(list, QqlxEnum.yhkQqlxZhifu.getKey(),(short)1);
				for (Map map : list){
					//修改资金流中zfpk
					Jlt updJlt = new Jlt();
					updJlt.setPk(reqModel.getJltpk());
					updJlt.setZfpk((String)map.get("pk"));
					jltService.updateByPrimaryKey(updJlt);
				}
			}
		}
		return resultNumber;
	}

	/**
     * @see YhkZfQqService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("YhkZfQqServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("YhkZfQqServiceImpl.delete时pk为空。");
    	}else{
    		return yhkZfQqMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see YhkZfQqService#updateByPrimaryKey(YhkZfQq yhkZfQq)
     */
    @Override
    public int updateByPrimaryKey(YhkZfQq yhkZfQq) throws Exception {
        if (yhkZfQq!=null){
        	if(StringUtils.isBlank(yhkZfQq.getPk())){
        		logger.error("YhkZfQqServiceImpl.updateByPrimaryKey时yhkZfQq.Pk为空。");
        		throw new AppRuntimeException("YhkZfQqServiceImpl.updateByPrimaryKey时yhkZfQq.Pk为空。");
        	}
	        return yhkZfQqMapper.updateByPrimaryKeySelective(yhkZfQq);
    	}else{
    		logger.error("YhkZfQqServiceImpl.updateByPrimaryKey时yhkZfQq数据为空。");
    		throw new AppRuntimeException("YhkZfQqServiceImpl.updateByPrimaryKey时yhkZfQq数据为空。");
    	}
    }
    /**
     * @see YhkZfQqService#queryYhkZfQqByPrimaryKey(String pk)
     */
    @Override
    public YhkZfQq queryYhkZfQqByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("YhkZfQqServiceImpl.queryYhkZfQqByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("YhkZfQqServiceImpl.queryYhkZfQqByPrimaryKey时pk为空。");
    	}else{
    		return yhkZfQqMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see YhkZfQqService#queryAsObject(YhkZfQq yhkZfQq)
     */
    @Override
    public YhkZfQq queryAsObject(YhkZfQq yhkZfQq) throws Exception {
        if (yhkZfQq!=null){
	        return yhkZfQqMapper.selectOne(yhkZfQq);
    	}else{
    		logger.error("YhkZfQqServiceImpl.queryAsObject时yhkZfQq数据为空。");
    		throw new AppRuntimeException("YhkZfQqServiceImpl.queryAsObject时yhkZfQq数据为空。");
    	}
    }
    
    /**
     * @see YhkZfQqService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return yhkZfQqMapper.selectCountByExample(example);
    	}else{
    		logger.error("YhkZfQqServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("YhkZfQqServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see YhkZfQqService#queryListByExample(Example example)
     */
    @Override
    public List<YhkZfQq> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return yhkZfQqMapper.selectByExample(example);
    	}else{
    		logger.error("YhkZfQqServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("YhkZfQqServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see YhkZfQqService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(yhkZfQqMapper.selectByExample(example));
    	}else{
    		logger.error("YhkZfQqServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("YhkZfQqServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see YhkZfQqService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(YhkZfQq.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jjdpk")!=null && StringUtils.isNotBlank(parmMap.get("jjdpk").toString())){
				criteria.andEqualTo("jjdpk",parmMap.get("jjdpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("applicationid")!=null && StringUtils.isNotBlank(parmMap.get("applicationid").toString())){
				criteria.andEqualTo("applicationid",parmMap.get("applicationid").toString().trim());
				flag = true;
			}
    		if(parmMap.get("subjecttype")!=null && StringUtils.isNotBlank(parmMap.get("subjecttype").toString())){
				criteria.andEqualTo("subjecttype",parmMap.get("subjecttype").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bankcode")!=null && StringUtils.isNotBlank(parmMap.get("bankcode").toString())){
				criteria.andEqualTo("bankcode",parmMap.get("bankcode").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bankname")!=null && StringUtils.isNotBlank(parmMap.get("bankname").toString())){
				criteria.andEqualTo("bankname",parmMap.get("bankname").toString().trim());
				flag = true;
			}
    		if(parmMap.get("accountname")!=null && StringUtils.isNotBlank(parmMap.get("accountname").toString())){
				criteria.andEqualTo("accountname",parmMap.get("accountname").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cardnumber")!=null && StringUtils.isNotBlank(parmMap.get("cardnumber").toString())){
				criteria.andEqualTo("cardnumber",parmMap.get("cardnumber").toString().trim());
				flag = true;
			}
    		if(parmMap.get("transfertime")!=null && StringUtils.isNotBlank(parmMap.get("transfertime").toString())){
				criteria.andEqualTo("transfertime",parmMap.get("transfertime").toString().trim());
				flag = true;
			}
    		if(parmMap.get("transferamount")!=null && StringUtils.isNotBlank(parmMap.get("transferamount").toString())){
				criteria.andEqualTo("transferamount",parmMap.get("transferamount").toString().trim());
				flag = true;
			}
    		if(parmMap.get("reason")!=null && StringUtils.isNotBlank(parmMap.get("reason").toString())){
				criteria.andEqualTo("reason",parmMap.get("reason").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrdwdm")!=null && StringUtils.isNotBlank(parmMap.get("lrdwdm").toString())){
				criteria.andEqualTo("lrdwdm",parmMap.get("lrdwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrdwmc")!=null && StringUtils.isNotBlank(parmMap.get("lrdwmc").toString())){
				criteria.andEqualTo("lrdwmc",parmMap.get("lrdwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrrxm")!=null && StringUtils.isNotBlank(parmMap.get("lrrxm").toString())){
				criteria.andEqualTo("lrrxm",parmMap.get("lrrxm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrrjh")!=null && StringUtils.isNotBlank(parmMap.get("lrrjh").toString())){
				criteria.andEqualTo("lrrjh",parmMap.get("lrrjh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("starttime")!=null && StringUtils.isNotBlank(parmMap.get("starttime").toString())){
				criteria.andEqualTo("starttime",parmMap.get("starttime").toString().trim());
				flag = true;
			}
    		if(parmMap.get("expiretime")!=null && StringUtils.isNotBlank(parmMap.get("expiretime").toString())){
				criteria.andEqualTo("expiretime",parmMap.get("expiretime").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("reqflag")!=null && StringUtils.isNotBlank(parmMap.get("reqflag").toString())){
				criteria.andEqualTo("reqflag",parmMap.get("reqflag").toString().trim());
				flag = true;
			}
    		if(parmMap.get("reqdate")!=null && StringUtils.isNotBlank(parmMap.get("reqdate").toString())){
				criteria.andEqualTo("reqdate",parmMap.get("reqdate").toString().trim());
				flag = true;
			}
    		if(parmMap.get("reqcount")!=null && StringUtils.isNotBlank(parmMap.get("reqcount").toString())){
				criteria.andEqualTo("reqcount",parmMap.get("reqcount").toString().trim());
				flag = true;
			}
    		if(parmMap.get("resflag")!=null && StringUtils.isNotBlank(parmMap.get("resflag").toString())){
				criteria.andEqualTo("resflag",parmMap.get("resflag").toString().trim());
				flag = true;
			}
    		if(parmMap.get("resdate")!=null && StringUtils.isNotBlank(parmMap.get("resdate").toString())){
				criteria.andEqualTo("resdate",parmMap.get("resdate").toString().trim());
				flag = true;
			}
    		if(parmMap.get("resultcode")!=null && StringUtils.isNotBlank(parmMap.get("resultcode").toString())){
				criteria.andEqualTo("resultcode",parmMap.get("resultcode").toString().trim());
				flag = true;
			}
    		if(parmMap.get("feedbackremark")!=null && StringUtils.isNotBlank(parmMap.get("feedbackremark").toString())){
				criteria.andEqualTo("feedbackremark",parmMap.get("feedbackremark").toString().trim());
				flag = true;
			}
    		if(parmMap.get("flws")!=null && StringUtils.isNotBlank(parmMap.get("flws").toString())){
				criteria.andEqualTo("flws",parmMap.get("flws").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("YhkZfQqServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("YhkZfQqServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }

}
