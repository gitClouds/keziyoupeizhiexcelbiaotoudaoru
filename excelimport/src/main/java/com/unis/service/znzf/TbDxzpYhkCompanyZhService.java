package com.unis.service.znzf;

import com.unis.model.znzf.TbDxzpYhkCompanyZh;
import com.unis.service.BaseService;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * <pre>
 * TODO
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-12-10
 */
public interface TbDxzpYhkCompanyZhService extends BaseService {

    /**
     * 查询TbDxzpYhkCompanyZh实例
     * 
     * @param tbDxzpYhkCompanyZh
     * @throws Exception
     */
    TbDxzpYhkCompanyZh queryAsObject(TbDxzpYhkCompanyZh tbDxzpYhkCompanyZh) throws Exception;
    
    	
	/**
     * 根据条件获取条数
     * 
     * @param example
     * @throws Exception
     */
	int queryCountByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合
     * 
     * @param example
     * @throws Exception
     */
	List<TbDxzpYhkCompanyZh> queryListByExample(Example example) throws Exception;

}
