package com.unis.service.znzf.impl;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.SfMxJgMapper;
import com.unis.model.znzf.SfMxJg;
import com.unis.service.znzf.SfMxJgService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see SfMxJgService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-03-09
 */
@Service("sfMxJgService")
public class SfMxJgServiceImpl extends BaseServiceImpl implements SfMxJgService {
	private static final Logger logger = LoggerFactory.getLogger(SfMxJgServiceImpl.class);
    @Autowired
    private SfMxJgMapper sfMxJgMapper;

    /**
     * @see SfMxJgService#insert(SfMxJg sfMxJg)
     */
    @Override
    public int insert(SfMxJg sfMxJg) throws Exception {
    	if (sfMxJg!=null){
	        //sfMxJg.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return sfMxJgMapper.insertSelective(sfMxJg);
    	}else{
    		logger.error("SfMxJgServiceImpl.insert时sfMxJg数据为空。");
    		throw new AppRuntimeException("SfMxJgServiceImpl.insert时sfMxJg数据为空。");
    	}        
    }

    /**
     * @see SfMxJgService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("SfMxJgServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("SfMxJgServiceImpl.delete时pk为空。");
    	}else{
    		return sfMxJgMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see SfMxJgService#updateByPrimaryKey(SfMxJg sfMxJg)
     */
    @Override
    public int updateByPrimaryKey(SfMxJg sfMxJg) throws Exception {
        if (sfMxJg!=null){
        	if(StringUtils.isBlank(sfMxJg.getPk())){
        		logger.error("SfMxJgServiceImpl.updateByPrimaryKey时sfMxJg.Pk为空。");
        		throw new AppRuntimeException("SfMxJgServiceImpl.updateByPrimaryKey时sfMxJg.Pk为空。");
        	}
	        return sfMxJgMapper.updateByPrimaryKeySelective(sfMxJg);
    	}else{
    		logger.error("SfMxJgServiceImpl.updateByPrimaryKey时sfMxJg数据为空。");
    		throw new AppRuntimeException("SfMxJgServiceImpl.updateByPrimaryKey时sfMxJg数据为空。");
    	}
    }
    /**
     * @see SfMxJgService#querySfMxJgByPrimaryKey(String pk)
     */
    @Override
    public SfMxJg querySfMxJgByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("SfMxJgServiceImpl.querySfMxJgByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("SfMxJgServiceImpl.querySfMxJgByPrimaryKey时pk为空。");
    	}else{
    		return sfMxJgMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see SfMxJgService#queryAsObject(SfMxJg sfMxJg)
     */
    @Override
    public SfMxJg queryAsObject(SfMxJg sfMxJg) throws Exception {
        if (sfMxJg!=null){
	        return sfMxJgMapper.selectOne(sfMxJg);
    	}else{
    		logger.error("SfMxJgServiceImpl.queryAsObject时sfMxJg数据为空。");
    		throw new AppRuntimeException("SfMxJgServiceImpl.queryAsObject时sfMxJg数据为空。");
    	}
    }
    
    /**
     * @see SfMxJgService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return sfMxJgMapper.selectCountByExample(example);
    	}else{
    		logger.error("SfMxJgServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("SfMxJgServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see SfMxJgService#queryListByExample(Example example)
     */
    @Override
    public List<SfMxJg> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return sfMxJgMapper.selectByExample(example);
    	}else{
    		logger.error("SfMxJgServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("SfMxJgServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see SfMxJgService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(sfMxJgMapper.selectByExample(example));
    	}else{
    		logger.error("SfMxJgServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("SfMxJgServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see SfMxJgService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(SfMxJg.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qqpk")!=null && StringUtils.isNotBlank(parmMap.get("qqpk").toString())){
				criteria.andEqualTo("qqpk",parmMap.get("qqpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jjdpk")!=null && StringUtils.isNotBlank(parmMap.get("jjdpk").toString())){
				criteria.andEqualTo("jjdpk",parmMap.get("jjdpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("applicationid")!=null && StringUtils.isNotBlank(parmMap.get("applicationid").toString())){
				criteria.andEqualTo("applicationid",parmMap.get("applicationid").toString().trim());
				flag = true;
			}
    		if(parmMap.get("resultcode")!=null && StringUtils.isNotBlank(parmMap.get("resultcode").toString())){
				criteria.andEqualTo("resultcode",parmMap.get("resultcode").toString().trim());
				flag = true;
			}
    		if(parmMap.get("subjecttype")!=null && StringUtils.isNotBlank(parmMap.get("subjecttype").toString())){
				criteria.andEqualTo("subjecttype",parmMap.get("subjecttype").toString().trim());
				flag = true;
			}
    		if(parmMap.get("accountnumber")!=null && StringUtils.isNotBlank(parmMap.get("accountnumber").toString())){
				criteria.andEqualTo("accountnumber",parmMap.get("accountnumber").toString().trim());
				flag = true;
			}
    		if(parmMap.get("accounttype")!=null && StringUtils.isNotBlank(parmMap.get("accounttype").toString())){
				criteria.andEqualTo("accounttype",parmMap.get("accounttype").toString().trim());
				flag = true;
			}
    		if(parmMap.get("accountstatus")!=null && StringUtils.isNotBlank(parmMap.get("accountstatus").toString())){
				criteria.andEqualTo("accountstatus",parmMap.get("accountstatus").toString().trim());
				flag = true;
			}
    		if(parmMap.get("currency")!=null && StringUtils.isNotBlank(parmMap.get("currency").toString())){
				criteria.andEqualTo("currency",parmMap.get("currency").toString().trim());
				flag = true;
			}
    		if(parmMap.get("accountbalance")!=null && StringUtils.isNotBlank(parmMap.get("accountbalance").toString())){
				criteria.andEqualTo("accountbalance",parmMap.get("accountbalance").toString().trim());
				flag = true;
			}
    		if(parmMap.get("accountinfo")!=null && StringUtils.isNotBlank(parmMap.get("accountinfo").toString())){
				criteria.andEqualTo("accountinfo",parmMap.get("accountinfo").toString().trim());
				flag = true;
			}
    		if(parmMap.get("accountopentime")!=null && StringUtils.isNotBlank(parmMap.get("accountopentime").toString())){
				criteria.andEqualTo("accountopentime",parmMap.get("accountopentime").toString().trim());
				flag = true;
			}
    		if(parmMap.get("accountopenip")!=null && StringUtils.isNotBlank(parmMap.get("accountopenip").toString())){
				criteria.andEqualTo("accountopenip",parmMap.get("accountopenip").toString().trim());
				flag = true;
			}
    		if(parmMap.get("accountopenmac")!=null && StringUtils.isNotBlank(parmMap.get("accountopenmac").toString())){
				criteria.andEqualTo("accountopenmac",parmMap.get("accountopenmac").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lasttransactiontime")!=null && StringUtils.isNotBlank(parmMap.get("lasttransactiontime").toString())){
				criteria.andEqualTo("lasttransactiontime",parmMap.get("lasttransactiontime").toString().trim());
				flag = true;
			}
    		if(parmMap.get("feedbackremark")!=null && StringUtils.isNotBlank(parmMap.get("feedbackremark").toString())){
				criteria.andEqualTo("feedbackremark",parmMap.get("feedbackremark").toString().trim());
				flag = true;
			}
    		if(parmMap.get("feedbackorgname")!=null && StringUtils.isNotBlank(parmMap.get("feedbackorgname").toString())){
				criteria.andEqualTo("feedbackorgname",parmMap.get("feedbackorgname").toString().trim());
				flag = true;
			}
    		if(parmMap.get("operatorname")!=null && StringUtils.isNotBlank(parmMap.get("operatorname").toString())){
				criteria.andEqualTo("operatorname",parmMap.get("operatorname").toString().trim());
				flag = true;
			}
    		if(parmMap.get("operatorphonenumber")!=null && StringUtils.isNotBlank(parmMap.get("operatorphonenumber").toString())){
				criteria.andEqualTo("operatorphonenumber",parmMap.get("operatorphonenumber").toString().trim());
				flag = true;
			}
    		if(parmMap.get("remark")!=null && StringUtils.isNotBlank(parmMap.get("remark").toString())){
				criteria.andEqualTo("remark",parmMap.get("remark").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fksj")!=null && StringUtils.isNotBlank(parmMap.get("fksj").toString())){
				criteria.andEqualTo("fksj",parmMap.get("fksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("SfMxJgServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("SfMxJgServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
