package com.unis.service.znzf;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import com.unis.model.znzf.CbMacGl;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.unis.service.BaseService;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;

/**
 * <pre>
 * TODO
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-05-24
 */
public interface CbMacGlService extends BaseService {

    /**
     * 新增CbMacGl实例
     * 
     * @param cbMacGl
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int insert(CbMacGl cbMacGl) throws Exception;

    /**
     * 删除CbMacGl实例
     * 
     * @param pk
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int delete(String pk) throws Exception;

    /**
     * 更新CbMacGl实例
     * 
     * @param cbMacGl
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int updateByPrimaryKey(CbMacGl cbMacGl) throws Exception;
    /**
     * 根据pk主键获取对象
     * 
     * @param pk
     * @throws Exception
     */
    CbMacGl queryCbMacGlByPrimaryKey(String pk) throws Exception;

    /**
     * 查询CbMacGl实例
     * 
     * @param cbMacGl
     * @throws Exception
     */
    CbMacGl queryAsObject(CbMacGl cbMacGl) throws Exception;
    
    	
	/**
     * 根据条件获取条数
     * 
     * @param example
     * @throws Exception
     */
	int queryCountByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合
     * 
     * @param example
     * @throws Exception
     */
	List<CbMacGl> queryListByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合(分页),需在example 设定排序
     * 
     * @param example
     * @param pageNum
     * @param pageSize
     * @throws Exception
     */
	PageInfo queryPageInfoByExample(Example example, int pageNum, int pageSize) throws Exception;

	/**
     * 根据入参获取数据集合(分页)
     * @param parmMap
     * @param pageNum
     * @param pageSize
     * @return
     * @throws Exception
     */
    ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception;

    PageInfo queryCbMacGlListByMap(Map parmMap, int pageNum, int pageSize) throws  Exception;

    ResultDto queryListByMap(Map parmMap, int pageNum, int pageSize) throws  Exception;

    ResultDto queryListByQqpk(Map parmMap, int pageNum, int pageSize) throws  Exception;

    List<Map> queryQqzhByJjdpkAndMacdz(String jjdpk,String macdz) throws Exception;
}
