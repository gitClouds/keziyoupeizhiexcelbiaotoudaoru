package com.unis.service.znzf.impl;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import com.unis.service.fzzx.JjdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.CbMacMapper;
import com.unis.model.znzf.CbMac;
import com.unis.service.znzf.CbMacService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see CbMacService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-05-16
 */
@Service("cbMacService")
public class CbMacServiceImpl extends BaseServiceImpl implements CbMacService {
	private static final Logger logger = LoggerFactory.getLogger(CbMacServiceImpl.class);
    @Autowired
    private CbMacMapper cbMacMapper;
	@Autowired
	private JjdService jjdService;
    /**
     * @see CbMacService#insert(CbMac cbMac)
     */
    @Override
    public int insert(CbMac cbMac) throws Exception {
    	if (cbMac!=null){
	        //cbMac.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return cbMacMapper.insertSelective(cbMac);
    	}else{
    		logger.error("CbMacServiceImpl.insert时cbMac数据为空。");
    		throw new AppRuntimeException("CbMacServiceImpl.insert时cbMac数据为空。");
    	}        
    }

    /**
     * @see CbMacService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("CbMacServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("CbMacServiceImpl.delete时pk为空。");
    	}else{
    		return cbMacMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see CbMacService#updateByPrimaryKey(CbMac cbMac)
     */
    @Override
    public int updateByPrimaryKey(CbMac cbMac) throws Exception {
        if (cbMac!=null){
        	if(StringUtils.isBlank(cbMac.getPk())){
        		logger.error("CbMacServiceImpl.updateByPrimaryKey时cbMac.Pk为空。");
        		throw new AppRuntimeException("CbMacServiceImpl.updateByPrimaryKey时cbMac.Pk为空。");
        	}
	        return cbMacMapper.updateByPrimaryKeySelective(cbMac);
    	}else{
    		logger.error("CbMacServiceImpl.updateByPrimaryKey时cbMac数据为空。");
    		throw new AppRuntimeException("CbMacServiceImpl.updateByPrimaryKey时cbMac数据为空。");
    	}
    }
    /**
     * @see CbMacService#queryCbMacByPrimaryKey(String pk)
     */
    @Override
    public CbMac queryCbMacByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("CbMacServiceImpl.queryCbMacByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("CbMacServiceImpl.queryCbMacByPrimaryKey时pk为空。");
    	}else{
    		return cbMacMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see CbMacService#queryAsObject(CbMac cbMac)
     */
    @Override
    public CbMac queryAsObject(CbMac cbMac) throws Exception {
        if (cbMac!=null){
	        return cbMacMapper.selectOne(cbMac);
    	}else{
    		logger.error("CbMacServiceImpl.queryAsObject时cbMac数据为空。");
    		throw new AppRuntimeException("CbMacServiceImpl.queryAsObject时cbMac数据为空。");
    	}
    }
    
    /**
     * @see CbMacService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return cbMacMapper.selectCountByExample(example);
    	}else{
    		logger.error("CbMacServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("CbMacServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see CbMacService#queryListByExample(Example example)
     */
    @Override
    public List<CbMac> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return cbMacMapper.selectByExample(example);
    	}else{
    		logger.error("CbMacServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("CbMacServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see CbMacService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(cbMacMapper.selectByExample(example));
    	}else{
    		logger.error("CbMacServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("CbMacServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see CbMacService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(CbMac.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cbbh")!=null && StringUtils.isNotBlank(parmMap.get("cbbh").toString())){
				criteria.andEqualTo("cbbh",parmMap.get("cbbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sertype")!=null && StringUtils.isNotBlank(parmMap.get("sertype").toString())){
				criteria.andEqualTo("sertype",parmMap.get("sertype").toString().trim());
				flag = true;
			}
    		if(parmMap.get("mazdz")!=null && StringUtils.isNotBlank(parmMap.get("mazdz").toString())){
				criteria.andEqualTo("mazdz",parmMap.get("mazdz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("CbMacServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("CbMacServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
