package com.unis.service.znzf.impl;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.SfZtJgMapper;
import com.unis.model.znzf.SfZtJg;
import com.unis.service.znzf.SfZtJgService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see SfZtJgService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-03-11
 */
@Service("sfZtJgService")
public class SfZtJgServiceImpl extends BaseServiceImpl implements SfZtJgService {
	private static final Logger logger = LoggerFactory.getLogger(SfZtJgServiceImpl.class);
    @Autowired
    private SfZtJgMapper sfZtJgMapper;

    /**
     * @see SfZtJgService#insert(SfZtJg sfZtJg)
     */
    @Override
    public int insert(SfZtJg sfZtJg) throws Exception {
    	if (sfZtJg!=null){
	        //sfZtJg.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return sfZtJgMapper.insertSelective(sfZtJg);
    	}else{
    		logger.error("SfZtJgServiceImpl.insert时sfZtJg数据为空。");
    		throw new AppRuntimeException("SfZtJgServiceImpl.insert时sfZtJg数据为空。");
    	}        
    }

    /**
     * @see SfZtJgService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("SfZtJgServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("SfZtJgServiceImpl.delete时pk为空。");
    	}else{
    		return sfZtJgMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see SfZtJgService#updateByPrimaryKey(SfZtJg sfZtJg)
     */
    @Override
    public int updateByPrimaryKey(SfZtJg sfZtJg) throws Exception {
        if (sfZtJg!=null){
        	if(StringUtils.isBlank(sfZtJg.getPk())){
        		logger.error("SfZtJgServiceImpl.updateByPrimaryKey时sfZtJg.Pk为空。");
        		throw new AppRuntimeException("SfZtJgServiceImpl.updateByPrimaryKey时sfZtJg.Pk为空。");
        	}
	        return sfZtJgMapper.updateByPrimaryKeySelective(sfZtJg);
    	}else{
    		logger.error("SfZtJgServiceImpl.updateByPrimaryKey时sfZtJg数据为空。");
    		throw new AppRuntimeException("SfZtJgServiceImpl.updateByPrimaryKey时sfZtJg数据为空。");
    	}
    }
    /**
     * @see SfZtJgService#querySfZtJgByPrimaryKey(String pk)
     */
    @Override
    public SfZtJg querySfZtJgByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("SfZtJgServiceImpl.querySfZtJgByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("SfZtJgServiceImpl.querySfZtJgByPrimaryKey时pk为空。");
    	}else{
    		return sfZtJgMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see SfZtJgService#queryAsObject(SfZtJg sfZtJg)
     */
    @Override
    public SfZtJg queryAsObject(SfZtJg sfZtJg) throws Exception {
        if (sfZtJg!=null){
	        return sfZtJgMapper.selectOne(sfZtJg);
    	}else{
    		logger.error("SfZtJgServiceImpl.queryAsObject时sfZtJg数据为空。");
    		throw new AppRuntimeException("SfZtJgServiceImpl.queryAsObject时sfZtJg数据为空。");
    	}
    }
    
    /**
     * @see SfZtJgService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return sfZtJgMapper.selectCountByExample(example);
    	}else{
    		logger.error("SfZtJgServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("SfZtJgServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see SfZtJgService#queryListByExample(Example example)
     */
    @Override
    public List<SfZtJg> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return sfZtJgMapper.selectByExample(example);
    	}else{
    		logger.error("SfZtJgServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("SfZtJgServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see SfZtJgService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(sfZtJgMapper.selectByExample(example));
    	}else{
    		logger.error("SfZtJgServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("SfZtJgServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see SfZtJgService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(SfZtJg.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qqpk")!=null && StringUtils.isNotBlank(parmMap.get("qqpk").toString())){
				criteria.andEqualTo("qqpk",parmMap.get("qqpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jjdpk")!=null && StringUtils.isNotBlank(parmMap.get("jjdpk").toString())){
				criteria.andEqualTo("jjdpk",parmMap.get("jjdpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("applicationid")!=null && StringUtils.isNotBlank(parmMap.get("applicationid").toString())){
				criteria.andEqualTo("applicationid",parmMap.get("applicationid").toString().trim());
				flag = true;
			}
    		if(parmMap.get("resultcode")!=null && StringUtils.isNotBlank(parmMap.get("resultcode").toString())){
				criteria.andEqualTo("resultcode",parmMap.get("resultcode").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jbrxm")!=null && StringUtils.isNotBlank(parmMap.get("jbrxm").toString())){
				criteria.andEqualTo("jbrxm",parmMap.get("jbrxm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jbrdh")!=null && StringUtils.isNotBlank(parmMap.get("jbrdh").toString())){
				criteria.andEqualTo("jbrdh",parmMap.get("jbrdh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cxcslx")!=null && StringUtils.isNotBlank(parmMap.get("cxcslx").toString())){
				criteria.andEqualTo("cxcslx",parmMap.get("cxcslx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cs")!=null && StringUtils.isNotBlank(parmMap.get("cs").toString())){
				criteria.andEqualTo("cs",parmMap.get("cs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zhlb")!=null && StringUtils.isNotBlank(parmMap.get("zhlb").toString())){
				criteria.andEqualTo("zhlb",parmMap.get("zhlb").toString().trim());
				flag = true;
			}
    		if(parmMap.get("khztxm")!=null && StringUtils.isNotBlank(parmMap.get("khztxm").toString())){
				criteria.andEqualTo("khztxm",parmMap.get("khztxm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("khztzjlx")!=null && StringUtils.isNotBlank(parmMap.get("khztzjlx").toString())){
				criteria.andEqualTo("khztzjlx",parmMap.get("khztzjlx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("khztzjhm")!=null && StringUtils.isNotBlank(parmMap.get("khztzjhm").toString())){
				criteria.andEqualTo("khztzjhm",parmMap.get("khztzjhm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zjyxq")!=null && StringUtils.isNotBlank(parmMap.get("zjyxq").toString())){
				criteria.andEqualTo("zjyxq",parmMap.get("zjyxq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("khztbdsjh")!=null && StringUtils.isNotBlank(parmMap.get("khztbdsjh").toString())){
				criteria.andEqualTo("khztbdsjh",parmMap.get("khztbdsjh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("shmc")!=null && StringUtils.isNotBlank(parmMap.get("shmc").toString())){
				criteria.andEqualTo("shmc",parmMap.get("shmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("shhm")!=null && StringUtils.isNotBlank(parmMap.get("shhm").toString())){
				criteria.andEqualTo("shhm",parmMap.get("shhm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fkjgmc")!=null && StringUtils.isNotBlank(parmMap.get("fkjgmc").toString())){
				criteria.andEqualTo("fkjgmc",parmMap.get("fkjgmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cxfksm")!=null && StringUtils.isNotBlank(parmMap.get("cxfksm").toString())){
				criteria.andEqualTo("cxfksm",parmMap.get("cxfksm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bz")!=null && StringUtils.isNotBlank(parmMap.get("bz").toString())){
				criteria.andEqualTo("bz",parmMap.get("bz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fksj")!=null && StringUtils.isNotBlank(parmMap.get("fksj").toString())){
				criteria.andEqualTo("fksj",parmMap.get("fksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("SfZtJgServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("SfZtJgServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
