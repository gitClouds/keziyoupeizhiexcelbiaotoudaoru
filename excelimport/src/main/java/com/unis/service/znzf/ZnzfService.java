package com.unis.service.znzf;

import com.unis.dto.ResultDto;
import com.unis.service.BaseService;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * Created by Administrator on 2019/2/14/014.
 */
public interface ZnzfService extends BaseService {
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    byte[] createFlws(String type, Map<String,String> map) throws Exception;
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    String insertMongo(String type,String czType, Map<String,String> map) throws Exception;

    /**
     * 根据入参获取数据集合(分页)
     * @param jjdpk
     * @param cType
     * @param pageNum
     * @param pageSize
     * @return
     * @throws Exception
     */
    ResultDto queryListByPage(String jjdpk,String cType, int pageNum, int pageSize) throws  Exception;

}
