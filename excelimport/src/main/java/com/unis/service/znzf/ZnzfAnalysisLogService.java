package com.unis.service.znzf;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import com.unis.model.znzf.ZnzfAnalysisLog;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.unis.service.BaseService;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;

/**
 * <pre>
 * TODO
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-03-13
 */
public interface ZnzfAnalysisLogService extends BaseService {

    /**
     * 新增ZnzfAnalysisLog实例
     * 
     * @param znzfAnalysisLog
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int insert(ZnzfAnalysisLog znzfAnalysisLog) throws Exception;

    /**
     * 删除ZnzfAnalysisLog实例
     * 
     * @param pk
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int delete(String pk) throws Exception;

    /**
     * 更新ZnzfAnalysisLog实例
     * 
     * @param znzfAnalysisLog
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int updateByPrimaryKey(ZnzfAnalysisLog znzfAnalysisLog) throws Exception;
    /**
     * 根据pk主键获取对象
     * 
     * @param pk
     * @throws Exception
     */
    ZnzfAnalysisLog queryZnzfAnalysisLogByPrimaryKey(String pk) throws Exception;

    /**
     * 查询ZnzfAnalysisLog实例
     * 
     * @param znzfAnalysisLog
     * @throws Exception
     */
    ZnzfAnalysisLog queryAsObject(ZnzfAnalysisLog znzfAnalysisLog) throws Exception;
    
    	
	/**
     * 根据条件获取条数
     * 
     * @param example
     * @throws Exception
     */
	int queryCountByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合
     * 
     * @param example
     * @throws Exception
     */
	List<ZnzfAnalysisLog> queryListByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合(分页),需在example 设定排序
     * 
     * @param example
     * @param pageNum
     * @param pageSize
     * @throws Exception
     */
	PageInfo queryPageInfoByExample(Example example, int pageNum, int pageSize) throws Exception;

	/**
     * 根据入参获取数据集合(分页)
     * @param parmMap
     * @param pageNum
     * @param pageSize
     * @return
     * @throws Exception
     */
    ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception;
    
    

}
