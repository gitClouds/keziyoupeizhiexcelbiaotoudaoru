package com.unis.service.znzf.impl;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.SfZfJgMapper;
import com.unis.model.znzf.SfZfJg;
import com.unis.service.znzf.SfZfJgService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see SfZfJgService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-03-08
 */
@Service("sfZfJgService")
public class SfZfJgServiceImpl extends BaseServiceImpl implements SfZfJgService {
	private static final Logger logger = LoggerFactory.getLogger(SfZfJgServiceImpl.class);
    @Autowired
    private SfZfJgMapper sfZfJgMapper;

    /**
     * @see SfZfJgService#insert(SfZfJg sfZfJg)
     */
    @Override
    public int insert(SfZfJg sfZfJg) throws Exception {
    	if (sfZfJg!=null){
	        //sfZfJg.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return sfZfJgMapper.insertSelective(sfZfJg);
    	}else{
    		logger.error("SfZfJgServiceImpl.insert时sfZfJg数据为空。");
    		throw new AppRuntimeException("SfZfJgServiceImpl.insert时sfZfJg数据为空。");
    	}        
    }

    /**
     * @see SfZfJgService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("SfZfJgServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("SfZfJgServiceImpl.delete时pk为空。");
    	}else{
    		return sfZfJgMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see SfZfJgService#updateByPrimaryKey(SfZfJg sfZfJg)
     */
    @Override
    public int updateByPrimaryKey(SfZfJg sfZfJg) throws Exception {
        if (sfZfJg!=null){
        	if(StringUtils.isBlank(sfZfJg.getPk())){
        		logger.error("SfZfJgServiceImpl.updateByPrimaryKey时sfZfJg.Pk为空。");
        		throw new AppRuntimeException("SfZfJgServiceImpl.updateByPrimaryKey时sfZfJg.Pk为空。");
        	}
	        return sfZfJgMapper.updateByPrimaryKeySelective(sfZfJg);
    	}else{
    		logger.error("SfZfJgServiceImpl.updateByPrimaryKey时sfZfJg数据为空。");
    		throw new AppRuntimeException("SfZfJgServiceImpl.updateByPrimaryKey时sfZfJg数据为空。");
    	}
    }
    /**
     * @see SfZfJgService#querySfZfJgByPrimaryKey(String pk)
     */
    @Override
    public SfZfJg querySfZfJgByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("SfZfJgServiceImpl.querySfZfJgByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("SfZfJgServiceImpl.querySfZfJgByPrimaryKey时pk为空。");
    	}else{
    		return sfZfJgMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see SfZfJgService#queryAsObject(SfZfJg sfZfJg)
     */
    @Override
    public SfZfJg queryAsObject(SfZfJg sfZfJg) throws Exception {
        if (sfZfJg!=null){
	        return sfZfJgMapper.selectOne(sfZfJg);
    	}else{
    		logger.error("SfZfJgServiceImpl.queryAsObject时sfZfJg数据为空。");
    		throw new AppRuntimeException("SfZfJgServiceImpl.queryAsObject时sfZfJg数据为空。");
    	}
    }
    
    /**
     * @see SfZfJgService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return sfZfJgMapper.selectCountByExample(example);
    	}else{
    		logger.error("SfZfJgServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("SfZfJgServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see SfZfJgService#queryListByExample(Example example)
     */
    @Override
    public List<SfZfJg> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return sfZfJgMapper.selectByExample(example);
    	}else{
    		logger.error("SfZfJgServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("SfZfJgServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see SfZfJgService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(sfZfJgMapper.selectByExample(example));
    	}else{
    		logger.error("SfZfJgServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("SfZfJgServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see SfZfJgService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(SfZfJg.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qqpk")!=null && StringUtils.isNotBlank(parmMap.get("qqpk").toString())){
				criteria.andEqualTo("qqpk",parmMap.get("qqpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jjdpk")!=null && StringUtils.isNotBlank(parmMap.get("jjdpk").toString())){
				criteria.andEqualTo("jjdpk",parmMap.get("jjdpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("applicationid")!=null && StringUtils.isNotBlank(parmMap.get("applicationid").toString())){
				criteria.andEqualTo("applicationid",parmMap.get("applicationid").toString().trim());
				flag = true;
			}
    		if(parmMap.get("resultcode")!=null && StringUtils.isNotBlank(parmMap.get("resultcode").toString())){
				criteria.andEqualTo("resultcode",parmMap.get("resultcode").toString().trim());
				flag = true;
			}
    		if(parmMap.get("subjecttype")!=null && StringUtils.isNotBlank(parmMap.get("subjecttype").toString())){
				criteria.andEqualTo("subjecttype",parmMap.get("subjecttype").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zhtype")!=null && StringUtils.isNotBlank(parmMap.get("zhtype").toString())){
				criteria.andEqualTo("zhtype",parmMap.get("zhtype").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zh")!=null && StringUtils.isNotBlank(parmMap.get("zh").toString())){
				criteria.andEqualTo("zh",parmMap.get("zh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("accountname")!=null && StringUtils.isNotBlank(parmMap.get("accountname").toString())){
				criteria.andEqualTo("accountname",parmMap.get("accountname").toString().trim());
				flag = true;
			}
    		if(parmMap.get("idtype")!=null && StringUtils.isNotBlank(parmMap.get("idtype").toString())){
				criteria.andEqualTo("idtype",parmMap.get("idtype").toString().trim());
				flag = true;
			}
    		if(parmMap.get("idnumber")!=null && StringUtils.isNotBlank(parmMap.get("idnumber").toString())){
				criteria.andEqualTo("idnumber",parmMap.get("idnumber").toString().trim());
				flag = true;
			}
    		if(parmMap.get("accountbalance")!=null && StringUtils.isNotBlank(parmMap.get("accountbalance").toString())){
				criteria.andEqualTo("accountbalance",parmMap.get("accountbalance").toString().trim());
				flag = true;
			}
    		if(parmMap.get("suspensionamount")!=null && StringUtils.isNotBlank(parmMap.get("suspensionamount").toString())){
				criteria.andEqualTo("suspensionamount",parmMap.get("suspensionamount").toString().trim());
				flag = true;
			}
    		if(parmMap.get("starttime")!=null && StringUtils.isNotBlank(parmMap.get("starttime").toString())){
				criteria.andEqualTo("starttime",parmMap.get("starttime").toString().trim());
				flag = true;
			}
    		if(parmMap.get("expiretime")!=null && StringUtils.isNotBlank(parmMap.get("expiretime").toString())){
				criteria.andEqualTo("expiretime",parmMap.get("expiretime").toString().trim());
				flag = true;
			}
    		if(parmMap.get("failurecause")!=null && StringUtils.isNotBlank(parmMap.get("failurecause").toString())){
				criteria.andEqualTo("failurecause",parmMap.get("failurecause").toString().trim());
				flag = true;
			}
    		if(parmMap.get("feedbackremark")!=null && StringUtils.isNotBlank(parmMap.get("feedbackremark").toString())){
				criteria.andEqualTo("feedbackremark",parmMap.get("feedbackremark").toString().trim());
				flag = true;
			}
    		if(parmMap.get("feedbackorgname")!=null && StringUtils.isNotBlank(parmMap.get("feedbackorgname").toString())){
				criteria.andEqualTo("feedbackorgname",parmMap.get("feedbackorgname").toString().trim());
				flag = true;
			}
    		if(parmMap.get("operatorname")!=null && StringUtils.isNotBlank(parmMap.get("operatorname").toString())){
				criteria.andEqualTo("operatorname",parmMap.get("operatorname").toString().trim());
				flag = true;
			}
    		if(parmMap.get("operatorphonenumber")!=null && StringUtils.isNotBlank(parmMap.get("operatorphonenumber").toString())){
				criteria.andEqualTo("operatorphonenumber",parmMap.get("operatorphonenumber").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fksj")!=null && StringUtils.isNotBlank(parmMap.get("fksj").toString())){
				criteria.andEqualTo("fksj",parmMap.get("fksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("SfZfJgServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("SfZfJgServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
