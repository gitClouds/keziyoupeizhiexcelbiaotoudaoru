package com.unis.service.znzf.impl;

import java.util.List;
import java.util.Map;

import com.unis.common.util.TemplateUtil;
import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.MacCbjgMapper;
import com.unis.model.znzf.MacCbjg;
import com.unis.service.znzf.MacCbjgService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see MacCbjgService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-05-17
 */
@Service("macCbjgService")
public class MacCbjgServiceImpl extends BaseServiceImpl implements MacCbjgService {
	private static final Logger logger = LoggerFactory.getLogger(MacCbjgServiceImpl.class);
    @Autowired
    private MacCbjgMapper macCbjgMapper;

    /**
     * @see MacCbjgService#insert(MacCbjg macCbjg)
     */
    @Override
    public int insert(MacCbjg macCbjg) throws Exception {
    	if (macCbjg!=null){
	        macCbjg.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return macCbjgMapper.insertSelective(macCbjg);
    	}else{
    		logger.error("MacCbjgServiceImpl.insert时macCbjg数据为空。");
    		throw new AppRuntimeException("MacCbjgServiceImpl.insert时macCbjg数据为空。");
    	}        
    }

    /**
     * @see MacCbjgService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("MacCbjgServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("MacCbjgServiceImpl.delete时pk为空。");
    	}else{
    		return macCbjgMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see MacCbjgService#updateByPrimaryKey(MacCbjg macCbjg)
     */
    @Override
    public int updateByPrimaryKey(MacCbjg macCbjg) throws Exception {
        if (macCbjg!=null){
        	if(StringUtils.isBlank(macCbjg.getPk())){
        		logger.error("MacCbjgServiceImpl.updateByPrimaryKey时macCbjg.Pk为空。");
        		throw new AppRuntimeException("MacCbjgServiceImpl.updateByPrimaryKey时macCbjg.Pk为空。");
        	}
	        return macCbjgMapper.updateByPrimaryKeySelective(macCbjg);
    	}else{
    		logger.error("MacCbjgServiceImpl.updateByPrimaryKey时macCbjg数据为空。");
    		throw new AppRuntimeException("MacCbjgServiceImpl.updateByPrimaryKey时macCbjg数据为空。");
    	}
    }
    /**
     * @see MacCbjgService#queryMacCbjgByPrimaryKey(String pk)
     */
    @Override
    public MacCbjg queryMacCbjgByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("MacCbjgServiceImpl.queryMacCbjgByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("MacCbjgServiceImpl.queryMacCbjgByPrimaryKey时pk为空。");
    	}else{
    		return macCbjgMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see MacCbjgService#queryAsObject(MacCbjg macCbjg)
     */
    @Override
    public MacCbjg queryAsObject(MacCbjg macCbjg) throws Exception {
        if (macCbjg!=null){
	        return macCbjgMapper.selectOne(macCbjg);
    	}else{
    		logger.error("MacCbjgServiceImpl.queryAsObject时macCbjg数据为空。");
    		throw new AppRuntimeException("MacCbjgServiceImpl.queryAsObject时macCbjg数据为空。");
    	}
    }
    
    /**
     * @see MacCbjgService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return macCbjgMapper.selectCountByExample(example);
    	}else{
    		logger.error("MacCbjgServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("MacCbjgServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see MacCbjgService#queryListByExample(Example example)
     */
    @Override
    public List<MacCbjg> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return macCbjgMapper.selectByExample(example);
    	}else{
    		logger.error("MacCbjgServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("MacCbjgServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see MacCbjgService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(macCbjgMapper.selectByExample(example));
    	}else{
    		logger.error("MacCbjgServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("MacCbjgServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see MacCbjgService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(MacCbjg.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sertype")!=null && StringUtils.isNotBlank(parmMap.get("sertype").toString())){
				criteria.andEqualTo("sertype",parmMap.get("sertype").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zhjgdm")!=null && StringUtils.isNotBlank(parmMap.get("zhjgdm").toString())){
				criteria.andEqualTo("zhjgdm",parmMap.get("zhjgdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zhjgmc")!=null && StringUtils.isNotBlank(parmMap.get("zhjgmc").toString())){
				criteria.andEqualTo("zhjgmc",parmMap.get("zhjgmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("thzf")!=null && StringUtils.isNotBlank(parmMap.get("thzf").toString())){
				criteria.andEqualTo("thzf",parmMap.get("thzf").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("MacCbjgServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("MacCbjgServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
