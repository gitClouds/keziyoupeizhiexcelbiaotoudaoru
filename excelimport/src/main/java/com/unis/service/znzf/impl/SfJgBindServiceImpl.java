package com.unis.service.znzf.impl;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.SfJgBindMapper;
import com.unis.model.znzf.SfJgBind;
import com.unis.service.znzf.SfJgBindService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see SfJgBindService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-03-09
 */
@Service("sfJgBindService")
public class SfJgBindServiceImpl extends BaseServiceImpl implements SfJgBindService {
	private static final Logger logger = LoggerFactory.getLogger(SfJgBindServiceImpl.class);
    @Autowired
    private SfJgBindMapper sfJgBindMapper;

    /**
     * @see SfJgBindService#insert(SfJgBind sfJgBind)
     */
    @Override
    public int insert(SfJgBind sfJgBind) throws Exception {
    	if (sfJgBind!=null){
	        //sfJgBind.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return sfJgBindMapper.insertSelective(sfJgBind);
    	}else{
    		logger.error("SfJgBindServiceImpl.insert时sfJgBind数据为空。");
    		throw new AppRuntimeException("SfJgBindServiceImpl.insert时sfJgBind数据为空。");
    	}        
    }

    /**
     * @see SfJgBindService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("SfJgBindServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("SfJgBindServiceImpl.delete时pk为空。");
    	}else{
    		return sfJgBindMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see SfJgBindService#updateByPrimaryKey(SfJgBind sfJgBind)
     */
    @Override
    public int updateByPrimaryKey(SfJgBind sfJgBind) throws Exception {
        if (sfJgBind!=null){
        	if(StringUtils.isBlank(sfJgBind.getPk())){
        		logger.error("SfJgBindServiceImpl.updateByPrimaryKey时sfJgBind.Pk为空。");
        		throw new AppRuntimeException("SfJgBindServiceImpl.updateByPrimaryKey时sfJgBind.Pk为空。");
        	}
	        return sfJgBindMapper.updateByPrimaryKeySelective(sfJgBind);
    	}else{
    		logger.error("SfJgBindServiceImpl.updateByPrimaryKey时sfJgBind数据为空。");
    		throw new AppRuntimeException("SfJgBindServiceImpl.updateByPrimaryKey时sfJgBind数据为空。");
    	}
    }
    /**
     * @see SfJgBindService#querySfJgBindByPrimaryKey(String pk)
     */
    @Override
    public SfJgBind querySfJgBindByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("SfJgBindServiceImpl.querySfJgBindByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("SfJgBindServiceImpl.querySfJgBindByPrimaryKey时pk为空。");
    	}else{
    		return sfJgBindMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see SfJgBindService#queryAsObject(SfJgBind sfJgBind)
     */
    @Override
    public SfJgBind queryAsObject(SfJgBind sfJgBind) throws Exception {
        if (sfJgBind!=null){
	        return sfJgBindMapper.selectOne(sfJgBind);
    	}else{
    		logger.error("SfJgBindServiceImpl.queryAsObject时sfJgBind数据为空。");
    		throw new AppRuntimeException("SfJgBindServiceImpl.queryAsObject时sfJgBind数据为空。");
    	}
    }
    
    /**
     * @see SfJgBindService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return sfJgBindMapper.selectCountByExample(example);
    	}else{
    		logger.error("SfJgBindServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("SfJgBindServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see SfJgBindService#queryListByExample(Example example)
     */
    @Override
    public List<SfJgBind> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return sfJgBindMapper.selectByExample(example);
    	}else{
    		logger.error("SfJgBindServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("SfJgBindServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see SfJgBindService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(sfJgBindMapper.selectByExample(example));
    	}else{
    		logger.error("SfJgBindServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("SfJgBindServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see SfJgBindService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(SfJgBind.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("applicationid")!=null && StringUtils.isNotBlank(parmMap.get("applicationid").toString())){
				criteria.andEqualTo("applicationid",parmMap.get("applicationid").toString().trim());
				flag = true;
			}
    		if(parmMap.get("datatype")!=null && StringUtils.isNotBlank(parmMap.get("datatype").toString())){
				criteria.andEqualTo("datatype",parmMap.get("datatype").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bindingdata")!=null && StringUtils.isNotBlank(parmMap.get("bindingdata").toString())){
				criteria.andEqualTo("bindingdata",parmMap.get("bindingdata").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fksj")!=null && StringUtils.isNotBlank(parmMap.get("fksj").toString())){
				criteria.andEqualTo("fksj",parmMap.get("fksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
				return  new ResultDto();
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("SfJgBindServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("SfJgBindServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
