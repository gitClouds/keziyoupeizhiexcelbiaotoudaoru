package com.unis.service.znzf.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.unis.common.exception.app.AppRuntimeException;
import com.unis.common.util.StrUtil;
import com.unis.dto.ResultDto;
import com.unis.mapper.znzf.CbGlMapper;
import com.unis.model.znzf.CbGl;
import com.unis.service.impl.BaseServiceImpl;
import com.unis.service.znzf.CbGlService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.sql.Clob;
import java.util.List;
import java.util.Map;

/**
 * <pre>
 * @see CbGlService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-04-22
 */
@Service("cbGlService")
public class CbGlServiceImpl extends BaseServiceImpl implements CbGlService {
	private static final Logger logger = LoggerFactory.getLogger(CbGlServiceImpl.class);
    @Autowired
    private CbGlMapper cbGlMapper;

    /**
     * @see CbGlService#insert(CbGl cbGl)
     */
    @Override
    public int insert(CbGl cbGl) throws Exception {
    	if (cbGl!=null){
	        //cbGl.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return cbGlMapper.insertSelective(cbGl);
    	}else{
    		logger.error("CbGlServiceImpl.insert时cbGl数据为空。");
    		throw new AppRuntimeException("CbGlServiceImpl.insert时cbGl数据为空。");
    	}        
    }

    /**
     * @see CbGlService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("CbGlServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("CbGlServiceImpl.delete时pk为空。");
    	}else{
    		return cbGlMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see CbGlService#updateByPrimaryKey(CbGl cbGl)
     */
    @Override
    public int updateByPrimaryKey(CbGl cbGl) throws Exception {
        if (cbGl!=null){
        	if(StringUtils.isBlank(cbGl.getPk())){
        		logger.error("CbGlServiceImpl.updateByPrimaryKey时cbGl.Pk为空。");
        		throw new AppRuntimeException("CbGlServiceImpl.updateByPrimaryKey时cbGl.Pk为空。");
        	}
	        return cbGlMapper.updateByPrimaryKeySelective(cbGl);
    	}else{
    		logger.error("CbGlServiceImpl.updateByPrimaryKey时cbGl数据为空。");
    		throw new AppRuntimeException("CbGlServiceImpl.updateByPrimaryKey时cbGl数据为空。");
    	}
    }
    /**
     * @see CbGlService#queryCbGlByPrimaryKey(String pk)
     */
    @Override
    public CbGl queryCbGlByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("CbGlServiceImpl.queryCbGlByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("CbGlServiceImpl.queryCbGlByPrimaryKey时pk为空。");
    	}else{
    		return cbGlMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see CbGlService#queryAsObject(CbGl cbGl)
     */
    @Override
    public CbGl queryAsObject(CbGl cbGl) throws Exception {
        if (cbGl!=null){
	        return cbGlMapper.selectOne(cbGl);
    	}else{
    		logger.error("CbGlServiceImpl.queryAsObject时cbGl数据为空。");
    		throw new AppRuntimeException("CbGlServiceImpl.queryAsObject时cbGl数据为空。");
    	}
    }
    
    /**
     * @see CbGlService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return cbGlMapper.selectCountByExample(example);
    	}else{
    		logger.error("CbGlServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("CbGlServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see CbGlService#queryListByExample(Example example)
     */
    @Override
    public List<CbGl> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return cbGlMapper.selectByExample(example);
    	}else{
    		logger.error("CbGlServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("CbGlServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see CbGlService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(cbGlMapper.selectByExample(example));
    	}else{
    		logger.error("CbGlServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("CbGlServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	@Override
	public PageInfo queryPageInfoByJjdpk(String jjdpk,int pageNum,int pageSize) throws Exception {
		if(StringUtils.isNotBlank(jjdpk)){
			PageHelper.startPage(pageNum,pageSize);
			List<Map> list = cbGlMapper.selectByJjdpk(jjdpk);
			if(list!=null &&!list.isEmpty() && list.size()>0){
				if(list.get(0).get("CBYJ") instanceof String){

				}else{
					for (Map map:list ){
						map.put("CBYJ", StrUtil.ClobtoString((Clob) map.get("CBYJ")));
					}
				}
			}
			return new PageInfo(list);
		}else{
			logger.error("CbGlServiceImpl.queryListByExample时example数据为空。");
			throw new AppRuntimeException("CbGlServiceImpl.queryListByExample时example数据为空。");
		}
	}
	/**
     * @see CbGlService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(CbGl.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cbbh")!=null && StringUtils.isNotBlank(parmMap.get("cbbh").toString())){
				criteria.andEqualTo("cbbh",parmMap.get("cbbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sertype")!=null && StringUtils.isNotBlank(parmMap.get("sertype").toString())){
				criteria.andEqualTo("sertype",parmMap.get("sertype").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jjdpk")!=null && StringUtils.isNotBlank(parmMap.get("jjdpk").toString())){
				criteria.andEqualTo("jjdpk",parmMap.get("jjdpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bankcode")!=null && StringUtils.isNotBlank(parmMap.get("bankcode").toString())){
				criteria.andEqualTo("bankcode",parmMap.get("bankcode").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cardnumber")!=null && StringUtils.isNotBlank(parmMap.get("cardnumber").toString())){
				criteria.andEqualTo("cardnumber",parmMap.get("cardnumber").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrdwdm")!=null && StringUtils.isNotBlank(parmMap.get("lrdwdm").toString())){
				criteria.andEqualTo("lrdwdm",parmMap.get("lrdwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrdwmc")!=null && StringUtils.isNotBlank(parmMap.get("lrdwmc").toString())){
				criteria.andEqualTo("lrdwmc",parmMap.get("lrdwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrrxm")!=null && StringUtils.isNotBlank(parmMap.get("lrrxm").toString())){
				criteria.andEqualTo("lrrxm",parmMap.get("lrrxm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrrjh")!=null && StringUtils.isNotBlank(parmMap.get("lrrjh").toString())){
				criteria.andEqualTo("lrrjh",parmMap.get("lrrjh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrsj")!=null && StringUtils.isNotBlank(parmMap.get("lrsj").toString())){
				criteria.andEqualTo("lrsj",parmMap.get("lrsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jltpk")!=null && StringUtils.isNotBlank(parmMap.get("jltpk").toString())){
				criteria.andEqualTo("jltpk",parmMap.get("jltpk").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("CbGlServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("CbGlServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
	public ResultDto queryListByPageCb(String jjdpk, int pageNum, int pageSize) throws  Exception{
		if (StringUtils.isNotBlank(jjdpk)){
			PageInfo pageInfo = this.queryPageInfoByJjdpk(jjdpk,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
		}else{
			logger.error("CbGlServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("CbGlServiceImpl.queryListByPage时parmMap数据为空。");
		}
	}
}
