package com.unis.service.znzf.impl;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.SfJgActMapper;
import com.unis.model.znzf.SfJgAct;
import com.unis.service.znzf.SfJgActService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see SfJgActService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-03-11
 */
@Service("sfJgActService")
public class SfJgActServiceImpl extends BaseServiceImpl implements SfJgActService {
	private static final Logger logger = LoggerFactory.getLogger(SfJgActServiceImpl.class);
    @Autowired
    private SfJgActMapper sfJgActMapper;

    /**
     * @see SfJgActService#insert(SfJgAct sfJgAct)
     */
    @Override
    public int insert(SfJgAct sfJgAct) throws Exception {
    	if (sfJgAct!=null){
	        //sfJgAct.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return sfJgActMapper.insertSelective(sfJgAct);
    	}else{
    		logger.error("SfJgActServiceImpl.insert时sfJgAct数据为空。");
    		throw new AppRuntimeException("SfJgActServiceImpl.insert时sfJgAct数据为空。");
    	}        
    }

    /**
     * @see SfJgActService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("SfJgActServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("SfJgActServiceImpl.delete时pk为空。");
    	}else{
    		return sfJgActMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see SfJgActService#updateByPrimaryKey(SfJgAct sfJgAct)
     */
    @Override
    public int updateByPrimaryKey(SfJgAct sfJgAct) throws Exception {
        if (sfJgAct!=null){
        	if(StringUtils.isBlank(sfJgAct.getPk())){
        		logger.error("SfJgActServiceImpl.updateByPrimaryKey时sfJgAct.Pk为空。");
        		throw new AppRuntimeException("SfJgActServiceImpl.updateByPrimaryKey时sfJgAct.Pk为空。");
        	}
	        return sfJgActMapper.updateByPrimaryKeySelective(sfJgAct);
    	}else{
    		logger.error("SfJgActServiceImpl.updateByPrimaryKey时sfJgAct数据为空。");
    		throw new AppRuntimeException("SfJgActServiceImpl.updateByPrimaryKey时sfJgAct数据为空。");
    	}
    }
    /**
     * @see SfJgActService#querySfJgActByPrimaryKey(String pk)
     */
    @Override
    public SfJgAct querySfJgActByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("SfJgActServiceImpl.querySfJgActByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("SfJgActServiceImpl.querySfJgActByPrimaryKey时pk为空。");
    	}else{
    		return sfJgActMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see SfJgActService#queryAsObject(SfJgAct sfJgAct)
     */
    @Override
    public SfJgAct queryAsObject(SfJgAct sfJgAct) throws Exception {
        if (sfJgAct!=null){
	        return sfJgActMapper.selectOne(sfJgAct);
    	}else{
    		logger.error("SfJgActServiceImpl.queryAsObject时sfJgAct数据为空。");
    		throw new AppRuntimeException("SfJgActServiceImpl.queryAsObject时sfJgAct数据为空。");
    	}
    }
    
    /**
     * @see SfJgActService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return sfJgActMapper.selectCountByExample(example);
    	}else{
    		logger.error("SfJgActServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("SfJgActServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see SfJgActService#queryListByExample(Example example)
     */
    @Override
    public List<SfJgAct> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return sfJgActMapper.selectByExample(example);
    	}else{
    		logger.error("SfJgActServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("SfJgActServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see SfJgActService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(sfJgActMapper.selectByExample(example));
    	}else{
    		logger.error("SfJgActServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("SfJgActServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see SfJgActService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(SfJgAct.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qqpk")!=null && StringUtils.isNotBlank(parmMap.get("qqpk").toString())){
				criteria.andEqualTo("qqpk",parmMap.get("qqpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jjdpk")!=null && StringUtils.isNotBlank(parmMap.get("jjdpk").toString())){
				criteria.andEqualTo("jjdpk",parmMap.get("jjdpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("applicationid")!=null && StringUtils.isNotBlank(parmMap.get("applicationid").toString())){
				criteria.andEqualTo("applicationid",parmMap.get("applicationid").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zfzh")!=null && StringUtils.isNotBlank(parmMap.get("zfzh").toString())){
				criteria.andEqualTo("zfzh",parmMap.get("zfzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zhlb")!=null && StringUtils.isNotBlank(parmMap.get("zhlb").toString())){
				criteria.andEqualTo("zhlb",parmMap.get("zhlb").toString().trim());
				flag = true;
			}
    		if(parmMap.get("khxm")!=null && StringUtils.isNotBlank(parmMap.get("khxm").toString())){
				criteria.andEqualTo("khxm",parmMap.get("khxm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("khzjlx")!=null && StringUtils.isNotBlank(parmMap.get("khzjlx").toString())){
				criteria.andEqualTo("khzjlx",parmMap.get("khzjlx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("khzjhm")!=null && StringUtils.isNotBlank(parmMap.get("khzjhm").toString())){
				criteria.andEqualTo("khzjhm",parmMap.get("khzjhm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zjyxq")!=null && StringUtils.isNotBlank(parmMap.get("zjyxq").toString())){
				criteria.andEqualTo("zjyxq",parmMap.get("zjyxq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bdsjh")!=null && StringUtils.isNotBlank(parmMap.get("bdsjh").toString())){
				criteria.andEqualTo("bdsjh",parmMap.get("bdsjh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("dlhm")!=null && StringUtils.isNotBlank(parmMap.get("dlhm").toString())){
				criteria.andEqualTo("dlhm",parmMap.get("dlhm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("wx")!=null && StringUtils.isNotBlank(parmMap.get("wx").toString())){
				criteria.andEqualTo("wx",parmMap.get("wx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qq")!=null && StringUtils.isNotBlank(parmMap.get("qq").toString())){
				criteria.andEqualTo("qq",parmMap.get("qq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ip")!=null && StringUtils.isNotBlank(parmMap.get("ip").toString())){
				criteria.andEqualTo("ip",parmMap.get("ip").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sbhm")!=null && StringUtils.isNotBlank(parmMap.get("sbhm").toString())){
				criteria.andEqualTo("sbhm",parmMap.get("sbhm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("shmc")!=null && StringUtils.isNotBlank(parmMap.get("shmc").toString())){
				criteria.andEqualTo("shmc",parmMap.get("shmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("shhm")!=null && StringUtils.isNotBlank(parmMap.get("shhm").toString())){
				criteria.andEqualTo("shhm",parmMap.get("shhm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("posbh")!=null && StringUtils.isNotBlank(parmMap.get("posbh").toString())){
				criteria.andEqualTo("posbh",parmMap.get("posbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("posdz")!=null && StringUtils.isNotBlank(parmMap.get("posdz").toString())){
				criteria.andEqualTo("posdz",parmMap.get("posdz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("posdwdz")!=null && StringUtils.isNotBlank(parmMap.get("posdwdz").toString())){
				criteria.andEqualTo("posdwdz",parmMap.get("posdwdz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("SfJgActServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("SfJgActServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
