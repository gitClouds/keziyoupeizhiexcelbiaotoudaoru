package com.unis.service.znzf.impl;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.SfJgMeaMapper;
import com.unis.model.znzf.SfJgMea;
import com.unis.service.znzf.SfJgMeaService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see SfJgMeaService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-03-11
 */
@Service("sfJgMeaService")
public class SfJgMeaServiceImpl extends BaseServiceImpl implements SfJgMeaService {
	private static final Logger logger = LoggerFactory.getLogger(SfJgMeaServiceImpl.class);
    @Autowired
    private SfJgMeaMapper sfJgMeaMapper;

    /**
     * @see SfJgMeaService#insert(SfJgMea sfJgMea)
     */
    @Override
    public int insert(SfJgMea sfJgMea) throws Exception {
    	if (sfJgMea!=null){
	        //sfJgMea.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return sfJgMeaMapper.insertSelective(sfJgMea);
    	}else{
    		logger.error("SfJgMeaServiceImpl.insert时sfJgMea数据为空。");
    		throw new AppRuntimeException("SfJgMeaServiceImpl.insert时sfJgMea数据为空。");
    	}        
    }

    /**
     * @see SfJgMeaService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("SfJgMeaServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("SfJgMeaServiceImpl.delete时pk为空。");
    	}else{
    		return sfJgMeaMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see SfJgMeaService#updateByPrimaryKey(SfJgMea sfJgMea)
     */
    @Override
    public int updateByPrimaryKey(SfJgMea sfJgMea) throws Exception {
        if (sfJgMea!=null){
        	if(StringUtils.isBlank(sfJgMea.getPk())){
        		logger.error("SfJgMeaServiceImpl.updateByPrimaryKey时sfJgMea.Pk为空。");
        		throw new AppRuntimeException("SfJgMeaServiceImpl.updateByPrimaryKey时sfJgMea.Pk为空。");
        	}
	        return sfJgMeaMapper.updateByPrimaryKeySelective(sfJgMea);
    	}else{
    		logger.error("SfJgMeaServiceImpl.updateByPrimaryKey时sfJgMea数据为空。");
    		throw new AppRuntimeException("SfJgMeaServiceImpl.updateByPrimaryKey时sfJgMea数据为空。");
    	}
    }
    /**
     * @see SfJgMeaService#querySfJgMeaByPrimaryKey(String pk)
     */
    @Override
    public SfJgMea querySfJgMeaByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("SfJgMeaServiceImpl.querySfJgMeaByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("SfJgMeaServiceImpl.querySfJgMeaByPrimaryKey时pk为空。");
    	}else{
    		return sfJgMeaMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see SfJgMeaService#queryAsObject(SfJgMea sfJgMea)
     */
    @Override
    public SfJgMea queryAsObject(SfJgMea sfJgMea) throws Exception {
        if (sfJgMea!=null){
	        return sfJgMeaMapper.selectOne(sfJgMea);
    	}else{
    		logger.error("SfJgMeaServiceImpl.queryAsObject时sfJgMea数据为空。");
    		throw new AppRuntimeException("SfJgMeaServiceImpl.queryAsObject时sfJgMea数据为空。");
    	}
    }
    
    /**
     * @see SfJgMeaService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return sfJgMeaMapper.selectCountByExample(example);
    	}else{
    		logger.error("SfJgMeaServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("SfJgMeaServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see SfJgMeaService#queryListByExample(Example example)
     */
    @Override
    public List<SfJgMea> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return sfJgMeaMapper.selectByExample(example);
    	}else{
    		logger.error("SfJgMeaServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("SfJgMeaServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see SfJgMeaService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(sfJgMeaMapper.selectByExample(example));
    	}else{
    		logger.error("SfJgMeaServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("SfJgMeaServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see SfJgMeaService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(SfJgMea.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("actpk")!=null && StringUtils.isNotBlank(parmMap.get("actpk").toString())){
				criteria.andEqualTo("actpk",parmMap.get("actpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("applicationid")!=null && StringUtils.isNotBlank(parmMap.get("applicationid").toString())){
				criteria.andEqualTo("applicationid",parmMap.get("applicationid").toString().trim());
				flag = true;
			}
    		if(parmMap.get("csxh")!=null && StringUtils.isNotBlank(parmMap.get("csxh").toString())){
				criteria.andEqualTo("csxh",parmMap.get("csxh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zfzh")!=null && StringUtils.isNotBlank(parmMap.get("zfzh").toString())){
				criteria.andEqualTo("zfzh",parmMap.get("zfzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cslx")!=null && StringUtils.isNotBlank(parmMap.get("cslx").toString())){
				criteria.andEqualTo("cslx",parmMap.get("cslx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("kssj")!=null && StringUtils.isNotBlank(parmMap.get("kssj").toString())){
				criteria.andEqualTo("kssj",parmMap.get("kssj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jzsj")!=null && StringUtils.isNotBlank(parmMap.get("jzsj").toString())){
				criteria.andEqualTo("jzsj",parmMap.get("jzsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jgmc")!=null && StringUtils.isNotBlank(parmMap.get("jgmc").toString())){
				criteria.andEqualTo("jgmc",parmMap.get("jgmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rmbzl")!=null && StringUtils.isNotBlank(parmMap.get("rmbzl").toString())){
				criteria.andEqualTo("rmbzl",parmMap.get("rmbzl").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zxje")!=null && StringUtils.isNotBlank(parmMap.get("zxje").toString())){
				criteria.andEqualTo("zxje",parmMap.get("zxje").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bz")!=null && StringUtils.isNotBlank(parmMap.get("bz").toString())){
				criteria.andEqualTo("bz",parmMap.get("bz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("SfJgMeaServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("SfJgMeaServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
