package com.unis.service.znzf.impl;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.YhkMxJgLstMapper;
import com.unis.model.znzf.YhkMxJgLst;
import com.unis.service.znzf.YhkMxJgLstService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see YhkMxJgLstService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-02-27
 */
@Service("yhkMxJgLstService")
public class YhkMxJgLstServiceImpl extends BaseServiceImpl implements YhkMxJgLstService {
	private static final Logger logger = LoggerFactory.getLogger(YhkMxJgLstServiceImpl.class);
    @Autowired
    private YhkMxJgLstMapper yhkMxJgLstMapper;

    /**
     * @see YhkMxJgLstService#insert(YhkMxJgLst yhkMxJgLst)
     */
    @Override
    public int insert(YhkMxJgLst yhkMxJgLst) throws Exception {
    	if (yhkMxJgLst!=null){
	        //yhkMxJgLst.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return yhkMxJgLstMapper.insertSelective(yhkMxJgLst);
    	}else{
    		logger.error("YhkMxJgLstServiceImpl.insert时yhkMxJgLst数据为空。");
    		throw new AppRuntimeException("YhkMxJgLstServiceImpl.insert时yhkMxJgLst数据为空。");
    	}        
    }

    /**
     * @see YhkMxJgLstService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("YhkMxJgLstServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("YhkMxJgLstServiceImpl.delete时pk为空。");
    	}else{
    		return yhkMxJgLstMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see YhkMxJgLstService#updateByPrimaryKey(YhkMxJgLst yhkMxJgLst)
     */
    @Override
    public int updateByPrimaryKey(YhkMxJgLst yhkMxJgLst) throws Exception {
        if (yhkMxJgLst!=null){
        	if(StringUtils.isBlank(yhkMxJgLst.getPk())){
        		logger.error("YhkMxJgLstServiceImpl.updateByPrimaryKey时yhkMxJgLst.Pk为空。");
        		throw new AppRuntimeException("YhkMxJgLstServiceImpl.updateByPrimaryKey时yhkMxJgLst.Pk为空。");
        	}
	        return yhkMxJgLstMapper.updateByPrimaryKeySelective(yhkMxJgLst);
    	}else{
    		logger.error("YhkMxJgLstServiceImpl.updateByPrimaryKey时yhkMxJgLst数据为空。");
    		throw new AppRuntimeException("YhkMxJgLstServiceImpl.updateByPrimaryKey时yhkMxJgLst数据为空。");
    	}
    }
    /**
     * @see YhkMxJgLstService#queryYhkMxJgLstByPrimaryKey(String pk)
     */
    @Override
    public YhkMxJgLst queryYhkMxJgLstByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("YhkMxJgLstServiceImpl.queryYhkMxJgLstByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("YhkMxJgLstServiceImpl.queryYhkMxJgLstByPrimaryKey时pk为空。");
    	}else{
    		return yhkMxJgLstMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see YhkMxJgLstService#queryAsObject(YhkMxJgLst yhkMxJgLst)
     */
    @Override
    public YhkMxJgLst queryAsObject(YhkMxJgLst yhkMxJgLst) throws Exception {
        if (yhkMxJgLst!=null){
	        return yhkMxJgLstMapper.selectOne(yhkMxJgLst);
    	}else{
    		logger.error("YhkMxJgLstServiceImpl.queryAsObject时yhkMxJgLst数据为空。");
    		throw new AppRuntimeException("YhkMxJgLstServiceImpl.queryAsObject时yhkMxJgLst数据为空。");
    	}
    }
    
    /**
     * @see YhkMxJgLstService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return yhkMxJgLstMapper.selectCountByExample(example);
    	}else{
    		logger.error("YhkMxJgLstServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("YhkMxJgLstServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see YhkMxJgLstService#queryListByExample(Example example)
     */
    @Override
    public List<YhkMxJgLst> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return yhkMxJgLstMapper.selectByExample(example);
    	}else{
    		logger.error("YhkMxJgLstServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("YhkMxJgLstServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see YhkMxJgLstService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(yhkMxJgLstMapper.selectByExample(example));
    	}else{
    		logger.error("YhkMxJgLstServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("YhkMxJgLstServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see YhkMxJgLstService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(YhkMxJgLst.class);
    		example.setOrderByClause("JYSJ DESC,rzh desc");//设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qqpk")!=null && StringUtils.isNotBlank(parmMap.get("qqpk").toString())){
				criteria.andEqualTo("qqpk",parmMap.get("qqpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jjdpk")!=null && StringUtils.isNotBlank(parmMap.get("jjdpk").toString())){
				criteria.andEqualTo("jjdpk",parmMap.get("jjdpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("applicationid")!=null && StringUtils.isNotBlank(parmMap.get("applicationid").toString())){
				criteria.andEqualTo("applicationid",parmMap.get("applicationid").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jylx")!=null && StringUtils.isNotBlank(parmMap.get("jylx").toString())){
				criteria.andEqualTo("jylx",parmMap.get("jylx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jdbz")!=null && StringUtils.isNotBlank(parmMap.get("jdbz").toString())){
				criteria.andEqualTo("jdbz",parmMap.get("jdbz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jyje")!=null && StringUtils.isNotBlank(parmMap.get("jyje").toString())){
				criteria.andEqualTo("jyje",parmMap.get("jyje").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jyye")!=null && StringUtils.isNotBlank(parmMap.get("jyye").toString())){
				criteria.andEqualTo("jyye",parmMap.get("jyye").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jysj")!=null && StringUtils.isNotBlank(parmMap.get("jysj").toString())){
				criteria.andEqualTo("jysj",parmMap.get("jysj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jylsh")!=null && StringUtils.isNotBlank(parmMap.get("jylsh").toString())){
				criteria.andEqualTo("jylsh",parmMap.get("jylsh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jydfmc")!=null && StringUtils.isNotBlank(parmMap.get("jydfmc").toString())){
				criteria.andEqualTo("jydfmc",parmMap.get("jydfmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jydfzh")!=null && StringUtils.isNotBlank(parmMap.get("jydfzh").toString())){
				criteria.andEqualTo("jydfzh",parmMap.get("jydfzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jydfzjhm")!=null && StringUtils.isNotBlank(parmMap.get("jydfzjhm").toString())){
				criteria.andEqualTo("jydfzjhm",parmMap.get("jydfzjhm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jydfkhh")!=null && StringUtils.isNotBlank(parmMap.get("jydfkhh").toString())){
				criteria.andEqualTo("jydfkhh",parmMap.get("jydfkhh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jysfcg")!=null && StringUtils.isNotBlank(parmMap.get("jysfcg").toString())){
				criteria.andEqualTo("jysfcg",parmMap.get("jysfcg").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xjbz")!=null && StringUtils.isNotBlank(parmMap.get("xjbz").toString())){
				criteria.andEqualTo("xjbz",parmMap.get("xjbz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jywdmc")!=null && StringUtils.isNotBlank(parmMap.get("jywdmc").toString())){
				criteria.andEqualTo("jywdmc",parmMap.get("jywdmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jywddm")!=null && StringUtils.isNotBlank(parmMap.get("jywddm").toString())){
				criteria.andEqualTo("jywddm",parmMap.get("jywddm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jyzy")!=null && StringUtils.isNotBlank(parmMap.get("jyzy").toString())){
				criteria.andEqualTo("jyzy",parmMap.get("jyzy").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ipdz")!=null && StringUtils.isNotBlank(parmMap.get("ipdz").toString())){
				criteria.andEqualTo("ipdz",parmMap.get("ipdz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("macdz")!=null && StringUtils.isNotBlank(parmMap.get("macdz").toString())){
				criteria.andEqualTo("macdz",parmMap.get("macdz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rzh")!=null && StringUtils.isNotBlank(parmMap.get("rzh").toString())){
				criteria.andEqualTo("rzh",parmMap.get("rzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cph")!=null && StringUtils.isNotBlank(parmMap.get("cph").toString())){
				criteria.andEqualTo("cph",parmMap.get("cph").toString().trim());
				flag = true;
			}
    		if(parmMap.get("pzh")!=null && StringUtils.isNotBlank(parmMap.get("pzh").toString())){
				criteria.andEqualTo("pzh",parmMap.get("pzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zdh")!=null && StringUtils.isNotBlank(parmMap.get("zdh").toString())){
				criteria.andEqualTo("zdh",parmMap.get("zdh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jygyh")!=null && StringUtils.isNotBlank(parmMap.get("jygyh").toString())){
				criteria.andEqualTo("jygyh",parmMap.get("jygyh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("shmc")!=null && StringUtils.isNotBlank(parmMap.get("shmc").toString())){
				criteria.andEqualTo("shmc",parmMap.get("shmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("shh")!=null && StringUtils.isNotBlank(parmMap.get("shh").toString())){
				criteria.andEqualTo("shh",parmMap.get("shh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rmbzl")!=null && StringUtils.isNotBlank(parmMap.get("rmbzl").toString())){
				criteria.andEqualTo("rmbzl",parmMap.get("rmbzl").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bz")!=null && StringUtils.isNotBlank(parmMap.get("bz").toString())){
				criteria.andEqualTo("bz",parmMap.get("bz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("pzzl")!=null && StringUtils.isNotBlank(parmMap.get("pzzl").toString())){
				criteria.andEqualTo("pzzl",parmMap.get("pzzl").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("YhkMxJgLstServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("YhkMxJgLstServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }

	@Override
	public List<String> queryApplicationByQqpk(String qqpk) throws Exception {
    	if (StringUtils.isNotBlank(qqpk)){
    		return yhkMxJgLstMapper.queryApplicationByQqpk(qqpk);
		}else{
			logger.error("YhkMxJgLstServiceImpl.queryApplicationByQqpk时qqpk数据为空。");
			throw new AppRuntimeException("YhkMxJgLstServiceImpl.queryApplicationByQqpk时qqpk数据为空。");
		}
	}

	@Override
	public List<Map<String, String>> queryMxListByJjd(String jjdpk, int nlevel) {
    	if (StringUtils.isNotBlank(jjdpk)){
			return yhkMxJgLstMapper.queryMxListByJjd(jjdpk,nlevel);
		}
		return null;
	}

	@Override
	public List<YhkMxJgLst> queryJe(String ywsqbh, String zh) throws Exception {
		if (StringUtils.isNotBlank(ywsqbh)&&StringUtils.isNotBlank(zh)){
			return yhkMxJgLstMapper.queryJe(ywsqbh,zh);
		}else {
			throw new Exception("银行卡queryJe参数为空：ywsqbh-"+ywsqbh+",zh-"+zh);
		}
	}
}
