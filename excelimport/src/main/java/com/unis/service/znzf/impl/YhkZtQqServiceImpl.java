package com.unis.service.znzf.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.unis.common.enums.QqlxEnum;
import com.unis.common.secure.authc.UserInfo;
import com.unis.common.util.TemplateUtil;
import com.unis.dto.ResultDto;
import com.unis.pojo.znzf.ReqModel;
import com.unis.service.znzf.JltService;
import com.unis.service.znzf.ZnzfQueueService;
import com.unis.service.znzf.ZnzfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.YhkZtQqMapper;
import com.unis.model.znzf.YhkZtQq;
import com.unis.service.znzf.YhkZtQqService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see YhkZtQqService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-02-20
 */
@Service("yhkZtQqService")
public class YhkZtQqServiceImpl extends BaseServiceImpl implements YhkZtQqService {
	private static final Logger logger = LoggerFactory.getLogger(YhkZtQqServiceImpl.class);
    @Autowired
    private YhkZtQqMapper yhkZtQqMapper;
	@Autowired
	private ZnzfQueueService znzfQueueService;
	@Autowired
	private ZnzfService znzfService;
	@Autowired
	private JltService jltService;
    /**
     * @see YhkZtQqService#insert(YhkZtQq yhkZtQq)
     */
    @Override
    public int insert(YhkZtQq yhkZtQq) throws Exception {
    	if (yhkZtQq!=null){
	        yhkZtQq.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return yhkZtQqMapper.insertSelective(yhkZtQq);
    	}else{
    		logger.error("YhkZtQqServiceImpl.insert时yhkZtQq数据为空。");
    		throw new AppRuntimeException("YhkZtQqServiceImpl.insert时yhkZtQq数据为空。");
    	}        
    }

	@Override
	public int insertYhkZt(ReqModel reqModel) throws Exception {
		int resultNumber = 0;
		Map bean =  null;
		if(reqModel!=null && reqModel.getBean()!=null && !reqModel.getBean().isEmpty() && reqModel.getBean().size()>0){
			List<Map> list = new ArrayList<>();
			UserInfo user = this.getUserInfo();
			Map<String,String> mongoMap = new HashMap();
			for (Map<String,String> map : reqModel.getBean()){
				if (map==null || map.isEmpty() || map.size()<1 ){
					continue;
				}
				bean = new HashMap();

				bean.put("pk", TemplateUtil.genUUID());
				bean.put("lrdwdm",user.getJgdm());
				bean.put("lrdwmc",user.getJgmc());
				bean.put("lrrxm",user.getXm());
				bean.put("lrrjh",user.getJh());
				bean.put("jjdpk",reqModel.getJjdPk());
				bean.put("reason",reqModel.getSy());
				bean.put("subjecttype",Short.parseShort(map.get("zhlb")));
				bean.put("bankcode",map.get("zhjgdm"));
				bean.put("bankname",map.get("zhjgmc"));
				bean.put("accountname",map.get("zhxm"));
				String zh = StringUtils.isNotBlank(map.get("zh"))?map.get("zh"):"";
				bean.put("cardnumber",zh);
				bean.put("zhlx",map.get("zhlx"));
				String mongoPk = "";
				if (mongoMap!=null && !mongoMap.isEmpty() && mongoMap.size()>0 && StringUtils.isNotBlank(mongoMap.get(zh))){
					mongoPk = mongoMap.get(zh);
				}else{
					mongoPk = znzfService.insertMongo("yhk","cx",bean);
					mongoMap.put(zh,mongoPk);
				}
				bean.put("flws",mongoPk);

				bean.put("nlevel",reqModel.getNlevel());
				bean.put("sertype",new Short("1"));
				bean.put("parentpk",reqModel.getParentpk());
				bean.put("parentzh",reqModel.getParentzh());
				//jltService.checkJltAndInsert(bean);
				bean.put("iszcygzs",reqModel.getIszcygzs());
				list.add(bean);
			}

			if (list!=null && !list.isEmpty() && list.size()>0){
				for (Map map : list){
					jltService.checkJltAndInsert(map);
				}
				resultNumber = yhkZtQqMapper.insertBatch(list);
				znzfQueueService.insertBatch(list, QqlxEnum.yhkQqlxZtcx.getKey(),(short)1);
			}
		}
		return resultNumber;
	}

	/**
     * @see YhkZtQqService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("YhkZtQqServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("YhkZtQqServiceImpl.delete时pk为空。");
    	}else{
    		return yhkZtQqMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see YhkZtQqService#updateByPrimaryKey(YhkZtQq yhkZtQq)
     */
    @Override
    public int updateByPrimaryKey(YhkZtQq yhkZtQq) throws Exception {
        if (yhkZtQq!=null){
        	if(StringUtils.isBlank(yhkZtQq.getPk())){
        		logger.error("YhkZtQqServiceImpl.updateByPrimaryKey时yhkZtQq.Pk为空。");
        		throw new AppRuntimeException("YhkZtQqServiceImpl.updateByPrimaryKey时yhkZtQq.Pk为空。");
        	}
	        return yhkZtQqMapper.updateByPrimaryKeySelective(yhkZtQq);
    	}else{
    		logger.error("YhkZtQqServiceImpl.updateByPrimaryKey时yhkZtQq数据为空。");
    		throw new AppRuntimeException("YhkZtQqServiceImpl.updateByPrimaryKey时yhkZtQq数据为空。");
    	}
    }

	@Override
	public int updateLevelByJlt(String jjdpk, String cardnumber, int nlevel, int newNlevel, int thisNlevel) throws Exception {
		return yhkZtQqMapper.updateNlevel(jjdpk,cardnumber,nlevel,newNlevel,thisNlevel);
	}

	/**
     * @see YhkZtQqService#queryYhkZtQqByPrimaryKey(String pk)
     */
    @Override
    public YhkZtQq queryYhkZtQqByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("YhkZtQqServiceImpl.queryYhkZtQqByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("YhkZtQqServiceImpl.queryYhkZtQqByPrimaryKey时pk为空。");
    	}else{
    		return yhkZtQqMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see YhkZtQqService#queryAsObject(YhkZtQq yhkZtQq)
     */
    @Override
    public YhkZtQq queryAsObject(YhkZtQq yhkZtQq) throws Exception {
        if (yhkZtQq!=null){
	        return yhkZtQqMapper.selectOne(yhkZtQq);
    	}else{
    		logger.error("YhkZtQqServiceImpl.queryAsObject时yhkZtQq数据为空。");
    		throw new AppRuntimeException("YhkZtQqServiceImpl.queryAsObject时yhkZtQq数据为空。");
    	}
    }
    
    /**
     * @see YhkZtQqService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return yhkZtQqMapper.selectCountByExample(example);
    	}else{
    		logger.error("YhkZtQqServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("YhkZtQqServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see YhkZtQqService#queryListByExample(Example example)
     */
    @Override
    public List<YhkZtQq> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return yhkZtQqMapper.selectByExample(example);
    	}else{
    		logger.error("YhkZtQqServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("YhkZtQqServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see YhkZtQqService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(yhkZtQqMapper.selectByExample(example));
    	}else{
    		logger.error("YhkZtQqServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("YhkZtQqServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see YhkZtQqService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(YhkZtQq.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jjdpk")!=null && StringUtils.isNotBlank(parmMap.get("jjdpk").toString())){
				criteria.andEqualTo("jjdpk",parmMap.get("jjdpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("applicationid")!=null && StringUtils.isNotBlank(parmMap.get("applicationid").toString())){
				criteria.andEqualTo("applicationid",parmMap.get("applicationid").toString().trim());
				flag = true;
			}
    		if(parmMap.get("subjecttype")!=null && StringUtils.isNotBlank(parmMap.get("subjecttype").toString())){
				criteria.andEqualTo("subjecttype",parmMap.get("subjecttype").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bankcode")!=null && StringUtils.isNotBlank(parmMap.get("bankcode").toString())){
				criteria.andEqualTo("bankcode",parmMap.get("bankcode").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bankname")!=null && StringUtils.isNotBlank(parmMap.get("bankname").toString())){
				criteria.andEqualTo("bankname",parmMap.get("bankname").toString().trim());
				flag = true;
			}
    		if(parmMap.get("accountname")!=null && StringUtils.isNotBlank(parmMap.get("accountname").toString())){
				criteria.andEqualTo("accountname",parmMap.get("accountname").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cardnumber")!=null && StringUtils.isNotBlank(parmMap.get("cardnumber").toString())){
				criteria.andEqualTo("cardnumber",parmMap.get("cardnumber").toString().trim());
				flag = true;
			}
    		if(parmMap.get("reason")!=null && StringUtils.isNotBlank(parmMap.get("reason").toString())){
				criteria.andEqualTo("reason",parmMap.get("reason").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrdwdm")!=null && StringUtils.isNotBlank(parmMap.get("lrdwdm").toString())){
				criteria.andEqualTo("lrdwdm",parmMap.get("lrdwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrdwmc")!=null && StringUtils.isNotBlank(parmMap.get("lrdwmc").toString())){
				criteria.andEqualTo("lrdwmc",parmMap.get("lrdwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrrxm")!=null && StringUtils.isNotBlank(parmMap.get("lrrxm").toString())){
				criteria.andEqualTo("lrrxm",parmMap.get("lrrxm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrrjh")!=null && StringUtils.isNotBlank(parmMap.get("lrrjh").toString())){
				criteria.andEqualTo("lrrjh",parmMap.get("lrrjh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("reqflag")!=null && StringUtils.isNotBlank(parmMap.get("reqflag").toString())){
				criteria.andEqualTo("reqflag",parmMap.get("reqflag").toString().trim());
				flag = true;
			}
    		if(parmMap.get("reqdate")!=null && StringUtils.isNotBlank(parmMap.get("reqdate").toString())){
				criteria.andEqualTo("reqdate",parmMap.get("reqdate").toString().trim());
				flag = true;
			}
    		if(parmMap.get("reqcount")!=null && StringUtils.isNotBlank(parmMap.get("reqcount").toString())){
				criteria.andEqualTo("reqcount",parmMap.get("reqcount").toString().trim());
				flag = true;
			}
    		if(parmMap.get("resflag")!=null && StringUtils.isNotBlank(parmMap.get("resflag").toString())){
				criteria.andEqualTo("resflag",parmMap.get("resflag").toString().trim());
				flag = true;
			}
    		if(parmMap.get("resdate")!=null && StringUtils.isNotBlank(parmMap.get("resdate").toString())){
				criteria.andEqualTo("resdate",parmMap.get("resdate").toString().trim());
				flag = true;
			}
    		if(parmMap.get("resultcode")!=null && StringUtils.isNotBlank(parmMap.get("resultcode").toString())){
				criteria.andEqualTo("resultcode",parmMap.get("resultcode").toString().trim());
				flag = true;
			}
    		if(parmMap.get("feedbackremark")!=null && StringUtils.isNotBlank(parmMap.get("feedbackremark").toString())){
				criteria.andEqualTo("feedbackremark",parmMap.get("feedbackremark").toString().trim());
				flag = true;
			}
    		if(parmMap.get("flws")!=null && StringUtils.isNotBlank(parmMap.get("flws").toString())){
				criteria.andEqualTo("flws",parmMap.get("flws").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("YhkZtQqServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("YhkZtQqServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
