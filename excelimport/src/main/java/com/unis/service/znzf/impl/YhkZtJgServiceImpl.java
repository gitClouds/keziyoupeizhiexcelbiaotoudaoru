package com.unis.service.znzf.impl;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.YhkZtJgMapper;
import com.unis.model.znzf.YhkZtJg;
import com.unis.service.znzf.YhkZtJgService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see YhkZtJgService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-02-26
 */
@Service("yhkZtJgService")
public class YhkZtJgServiceImpl extends BaseServiceImpl implements YhkZtJgService {
	private static final Logger logger = LoggerFactory.getLogger(YhkZtJgServiceImpl.class);
    @Autowired
    private YhkZtJgMapper yhkZtJgMapper;

    /**
     * @see YhkZtJgService#insert(YhkZtJg yhkZtJg)
     */
    @Override
    public int insert(YhkZtJg yhkZtJg) throws Exception {
    	if (yhkZtJg!=null){
	        //yhkZtJg.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return yhkZtJgMapper.insertSelective(yhkZtJg);
    	}else{
    		logger.error("YhkZtJgServiceImpl.insert时yhkZtJg数据为空。");
    		throw new AppRuntimeException("YhkZtJgServiceImpl.insert时yhkZtJg数据为空。");
    	}        
    }

    /**
     * @see YhkZtJgService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("YhkZtJgServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("YhkZtJgServiceImpl.delete时pk为空。");
    	}else{
    		return yhkZtJgMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see YhkZtJgService#updateByPrimaryKey(YhkZtJg yhkZtJg)
     */
    @Override
    public int updateByPrimaryKey(YhkZtJg yhkZtJg) throws Exception {
        if (yhkZtJg!=null){
        	if(StringUtils.isBlank(yhkZtJg.getPk())){
        		logger.error("YhkZtJgServiceImpl.updateByPrimaryKey时yhkZtJg.Pk为空。");
        		throw new AppRuntimeException("YhkZtJgServiceImpl.updateByPrimaryKey时yhkZtJg.Pk为空。");
        	}
	        return yhkZtJgMapper.updateByPrimaryKeySelective(yhkZtJg);
    	}else{
    		logger.error("YhkZtJgServiceImpl.updateByPrimaryKey时yhkZtJg数据为空。");
    		throw new AppRuntimeException("YhkZtJgServiceImpl.updateByPrimaryKey时yhkZtJg数据为空。");
    	}
    }
    /**
     * @see YhkZtJgService#queryYhkZtJgByPrimaryKey(String pk)
     */
    @Override
    public YhkZtJg queryYhkZtJgByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("YhkZtJgServiceImpl.queryYhkZtJgByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("YhkZtJgServiceImpl.queryYhkZtJgByPrimaryKey时pk为空。");
    	}else{
    		return yhkZtJgMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see YhkZtJgService#queryAsObject(YhkZtJg yhkZtJg)
     */
    @Override
    public YhkZtJg queryAsObject(YhkZtJg yhkZtJg) throws Exception {
        if (yhkZtJg!=null){
	        return yhkZtJgMapper.selectOne(yhkZtJg);
    	}else{
    		logger.error("YhkZtJgServiceImpl.queryAsObject时yhkZtJg数据为空。");
    		throw new AppRuntimeException("YhkZtJgServiceImpl.queryAsObject时yhkZtJg数据为空。");
    	}
    }
    
    /**
     * @see YhkZtJgService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return yhkZtJgMapper.selectCountByExample(example);
    	}else{
    		logger.error("YhkZtJgServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("YhkZtJgServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see YhkZtJgService#queryListByExample(Example example)
     */
    @Override
    public List<YhkZtJg> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return yhkZtJgMapper.selectByExample(example);
    	}else{
    		logger.error("YhkZtJgServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("YhkZtJgServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see YhkZtJgService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(yhkZtJgMapper.selectByExample(example));
    	}else{
    		logger.error("YhkZtJgServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("YhkZtJgServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see YhkZtJgService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(YhkZtJg.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qqpk")!=null && StringUtils.isNotBlank(parmMap.get("qqpk").toString())){
				criteria.andEqualTo("qqpk",parmMap.get("qqpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jjdpk")!=null && StringUtils.isNotBlank(parmMap.get("jjdpk").toString())){
				criteria.andEqualTo("jjdpk",parmMap.get("jjdpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("applicationid")!=null && StringUtils.isNotBlank(parmMap.get("applicationid").toString())){
				criteria.andEqualTo("applicationid",parmMap.get("applicationid").toString().trim());
				flag = true;
			}
    		if(parmMap.get("resultcode")!=null && StringUtils.isNotBlank(parmMap.get("resultcode").toString())){
				criteria.andEqualTo("resultcode",parmMap.get("resultcode").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jbrxm")!=null && StringUtils.isNotBlank(parmMap.get("jbrxm").toString())){
				criteria.andEqualTo("jbrxm",parmMap.get("jbrxm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jbrdh")!=null && StringUtils.isNotBlank(parmMap.get("jbrdh").toString())){
				criteria.andEqualTo("jbrdh",parmMap.get("jbrdh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fkjgmc")!=null && StringUtils.isNotBlank(parmMap.get("fkjgmc").toString())){
				criteria.andEqualTo("fkjgmc",parmMap.get("fkjgmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fksm")!=null && StringUtils.isNotBlank(parmMap.get("fksm").toString())){
				criteria.andEqualTo("fksm",parmMap.get("fksm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zjlx")!=null && StringUtils.isNotBlank(parmMap.get("zjlx").toString())){
				criteria.andEqualTo("zjlx",parmMap.get("zjlx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zjlxhm")!=null && StringUtils.isNotBlank(parmMap.get("zjlxhm").toString())){
				criteria.andEqualTo("zjlxhm",parmMap.get("zjlxhm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cxztmc")!=null && StringUtils.isNotBlank(parmMap.get("cxztmc").toString())){
				criteria.andEqualTo("cxztmc",parmMap.get("cxztmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cxsj")!=null && StringUtils.isNotBlank(parmMap.get("cxsj").toString())){
				criteria.andEqualTo("cxsj",parmMap.get("cxsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("dbrxm")!=null && StringUtils.isNotBlank(parmMap.get("dbrxm").toString())){
				criteria.andEqualTo("dbrxm",parmMap.get("dbrxm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("dbrzjlx")!=null && StringUtils.isNotBlank(parmMap.get("dbrzjlx").toString())){
				criteria.andEqualTo("dbrzjlx",parmMap.get("dbrzjlx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("dbrzjlxhm")!=null && StringUtils.isNotBlank(parmMap.get("dbrzjlxhm").toString())){
				criteria.andEqualTo("dbrzjlxhm",parmMap.get("dbrzjlxhm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zzdz")!=null && StringUtils.isNotBlank(parmMap.get("zzdz").toString())){
				criteria.andEqualTo("zzdz",parmMap.get("zzdz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zzdh")!=null && StringUtils.isNotBlank(parmMap.get("zzdh").toString())){
				criteria.andEqualTo("zzdh",parmMap.get("zzdh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gzdw")!=null && StringUtils.isNotBlank(parmMap.get("gzdw").toString())){
				criteria.andEqualTo("gzdw",parmMap.get("gzdw").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gzdwdz")!=null && StringUtils.isNotBlank(parmMap.get("gzdwdz").toString())){
				criteria.andEqualTo("gzdwdz",parmMap.get("gzdwdz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gzdwdh")!=null && StringUtils.isNotBlank(parmMap.get("gzdwdh").toString())){
				criteria.andEqualTo("gzdwdh",parmMap.get("gzdwdh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxdz")!=null && StringUtils.isNotBlank(parmMap.get("yxdz").toString())){
				criteria.andEqualTo("yxdz",parmMap.get("yxdz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("frdb")!=null && StringUtils.isNotBlank(parmMap.get("frdb").toString())){
				criteria.andEqualTo("frdb",parmMap.get("frdb").toString().trim());
				flag = true;
			}
    		if(parmMap.get("frdbzjlx")!=null && StringUtils.isNotBlank(parmMap.get("frdbzjlx").toString())){
				criteria.andEqualTo("frdbzjlx",parmMap.get("frdbzjlx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("frdbzjlxhm")!=null && StringUtils.isNotBlank(parmMap.get("frdbzjlxhm").toString())){
				criteria.andEqualTo("frdbzjlxhm",parmMap.get("frdbzjlxhm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("khygszchm")!=null && StringUtils.isNotBlank(parmMap.get("khygszchm").toString())){
				criteria.andEqualTo("khygszchm",parmMap.get("khygszchm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("gsnsh")!=null && StringUtils.isNotBlank(parmMap.get("gsnsh").toString())){
				criteria.andEqualTo("gsnsh",parmMap.get("gsnsh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("dsnsh")!=null && StringUtils.isNotBlank(parmMap.get("dsnsh").toString())){
				criteria.andEqualTo("dsnsh",parmMap.get("dsnsh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zjshrq")!=null && StringUtils.isNotBlank(parmMap.get("zjshrq").toString())){
				criteria.andEqualTo("zjshrq",parmMap.get("zjshrq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bz")!=null && StringUtils.isNotBlank(parmMap.get("bz").toString())){
				criteria.andEqualTo("bz",parmMap.get("bz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fksj")!=null && StringUtils.isNotBlank(parmMap.get("fksj").toString())){
				criteria.andEqualTo("fksj",parmMap.get("fksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("YhkZtJgServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("YhkZtJgServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
