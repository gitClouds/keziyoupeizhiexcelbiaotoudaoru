package com.unis.service.znzf.impl;

import java.util.List;
import java.util.Map;

import com.unis.common.secure.authc.UserInfo;
import com.unis.common.util.TemplateUtil;
import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.ZnzfYpslMapper;
import com.unis.model.znzf.ZnzfYpsl;
import com.unis.service.znzf.ZnzfYpslService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see ZnzfYpslService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-04-15
 */
@Service("znzfYpslService")
public class ZnzfYpslServiceImpl extends BaseServiceImpl implements ZnzfYpslService {
	private static final Logger logger = LoggerFactory.getLogger(ZnzfYpslServiceImpl.class);
    @Autowired
    private ZnzfYpslMapper znzfYpslMapper;

    /**
     * @see ZnzfYpslService#insert(ZnzfYpsl znzfYpsl)
     */
    @Override
    public int insert(ZnzfYpsl znzfYpsl) throws Exception {
    	if (znzfYpsl!=null){
	        znzfYpsl.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
			UserInfo  info = this.getUserInfo();
			znzfYpsl.setLrdwdm(info.getJgdm());
			znzfYpsl.setLrdwmc(info.getJgmc());
			znzfYpsl.setLrrxm(info.getXm());
			znzfYpsl.setLrrjh(info.getJh());
	        return znzfYpslMapper.insertSelective(znzfYpsl);
    	}else{
    		logger.error("ZnzfYpslServiceImpl.insert时znzfYpsl数据为空。");
    		throw new AppRuntimeException("ZnzfYpslServiceImpl.insert时znzfYpsl数据为空。");
    	}        
    }

    /**
     * @see ZnzfYpslService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("ZnzfYpslServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("ZnzfYpslServiceImpl.delete时pk为空。");
    	}else{
    		return znzfYpslMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see ZnzfYpslService#updateByPrimaryKey(ZnzfYpsl znzfYpsl)
     */
    @Override
    public int updateByPrimaryKey(ZnzfYpsl znzfYpsl) throws Exception {
        if (znzfYpsl!=null){
        	if(StringUtils.isBlank(znzfYpsl.getPk())){
        		logger.error("ZnzfYpslServiceImpl.updateByPrimaryKey时znzfYpsl.Pk为空。");
        		throw new AppRuntimeException("ZnzfYpslServiceImpl.updateByPrimaryKey时znzfYpsl.Pk为空。");
        	}
	        return znzfYpslMapper.updateByPrimaryKeySelective(znzfYpsl);
    	}else{
    		logger.error("ZnzfYpslServiceImpl.updateByPrimaryKey时znzfYpsl数据为空。");
    		throw new AppRuntimeException("ZnzfYpslServiceImpl.updateByPrimaryKey时znzfYpsl数据为空。");
    	}
    }
    /**
     * @see ZnzfYpslService#queryZnzfYpslByPrimaryKey(String pk)
     */
    @Override
    public ZnzfYpsl queryZnzfYpslByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("ZnzfYpslServiceImpl.queryZnzfYpslByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("ZnzfYpslServiceImpl.queryZnzfYpslByPrimaryKey时pk为空。");
    	}else{
    		return znzfYpslMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see ZnzfYpslService#queryAsObject(ZnzfYpsl znzfYpsl)
     */
    @Override
    public ZnzfYpsl queryAsObject(ZnzfYpsl znzfYpsl) throws Exception {
        if (znzfYpsl!=null){
	        return znzfYpslMapper.selectOne(znzfYpsl);
    	}else{
    		logger.error("ZnzfYpslServiceImpl.queryAsObject时znzfYpsl数据为空。");
    		throw new AppRuntimeException("ZnzfYpslServiceImpl.queryAsObject时znzfYpsl数据为空。");
    	}
    }
    
    /**
     * @see ZnzfYpslService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return znzfYpslMapper.selectCountByExample(example);
    	}else{
    		logger.error("ZnzfYpslServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("ZnzfYpslServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see ZnzfYpslService#queryListByExample(Example example)
     */
    @Override
    public List<ZnzfYpsl> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return znzfYpslMapper.selectByExample(example);
    	}else{
    		logger.error("ZnzfYpslServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("ZnzfYpslServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see ZnzfYpslService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(znzfYpslMapper.selectByExample(example));
    	}else{
    		logger.error("ZnzfYpslServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("ZnzfYpslServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see ZnzfYpslService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(ZnzfYpsl.class);
    		example.setOrderByClause("rksj DESC,pk asc");
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		criteria.andEqualTo("yxx",1);
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
			if(parmMap.get("jjdpk")!=null && StringUtils.isNotBlank(parmMap.get("jjdpk").toString())){
				criteria.andEqualTo("jjdpk",parmMap.get("jjdpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("nr")!=null && StringUtils.isNotBlank(parmMap.get("nr").toString())){
				criteria.andEqualTo("nr",parmMap.get("nr").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrdwdm")!=null && StringUtils.isNotBlank(parmMap.get("lrdwdm").toString())){
				criteria.andEqualTo("lrdwdm",parmMap.get("lrdwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrdwmc")!=null && StringUtils.isNotBlank(parmMap.get("lrdwmc").toString())){
				criteria.andEqualTo("lrdwmc",parmMap.get("lrdwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrrxm")!=null && StringUtils.isNotBlank(parmMap.get("lrrxm").toString())){
				criteria.andEqualTo("lrrxm",parmMap.get("lrrxm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrrjh")!=null && StringUtils.isNotBlank(parmMap.get("lrrjh").toString())){
				criteria.andEqualTo("lrrjh",parmMap.get("lrrjh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("ZnzfYpslServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("ZnzfYpslServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
