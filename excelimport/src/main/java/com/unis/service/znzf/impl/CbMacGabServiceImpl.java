package com.unis.service.znzf.impl;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.CbMacGabMapper;
import com.unis.model.znzf.CbMacGab;
import com.unis.service.znzf.CbMacGabService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see CbMacGabService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-05-28
 */
@Service("cbMacGabService")
public class CbMacGabServiceImpl extends BaseServiceImpl implements CbMacGabService {
	private static final Logger logger = LoggerFactory.getLogger(CbMacGabServiceImpl.class);
    @Autowired
    private CbMacGabMapper cbMacGabMapper;

    /**
     * @see CbMacGabService#insert(CbMacGab cbMacGab)
     */
    @Override
    public int insert(CbMacGab cbMacGab) throws Exception {
    	if (cbMacGab!=null){
	        //cbMacGab.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return cbMacGabMapper.insertSelective(cbMacGab);
    	}else{
    		logger.error("CbMacGabServiceImpl.insert时cbMacGab数据为空。");
    		throw new AppRuntimeException("CbMacGabServiceImpl.insert时cbMacGab数据为空。");
    	}        
    }

    /**
     * @see CbMacGabService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("CbMacGabServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("CbMacGabServiceImpl.delete时pk为空。");
    	}else{
    		return cbMacGabMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see CbMacGabService#updateByPrimaryKey(CbMacGab cbMacGab)
     */
    @Override
    public int updateByPrimaryKey(CbMacGab cbMacGab) throws Exception {
        if (cbMacGab!=null){
        	if(StringUtils.isBlank(cbMacGab.getPk())){
        		logger.error("CbMacGabServiceImpl.updateByPrimaryKey时cbMacGab.Pk为空。");
        		throw new AppRuntimeException("CbMacGabServiceImpl.updateByPrimaryKey时cbMacGab.Pk为空。");
        	}
	        return cbMacGabMapper.updateByPrimaryKeySelective(cbMacGab);
    	}else{
    		logger.error("CbMacGabServiceImpl.updateByPrimaryKey时cbMacGab数据为空。");
    		throw new AppRuntimeException("CbMacGabServiceImpl.updateByPrimaryKey时cbMacGab数据为空。");
    	}
    }
    /**
     * @see CbMacGabService#queryCbMacGabByPrimaryKey(String pk)
     */
    @Override
    public CbMacGab queryCbMacGabByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("CbMacGabServiceImpl.queryCbMacGabByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("CbMacGabServiceImpl.queryCbMacGabByPrimaryKey时pk为空。");
    	}else{
    		return cbMacGabMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see CbMacGabService#queryAsObject(CbMacGab cbMacGab)
     */
    @Override
    public CbMacGab queryAsObject(CbMacGab cbMacGab) throws Exception {
        if (cbMacGab!=null){
	        return cbMacGabMapper.selectOne(cbMacGab);
    	}else{
    		logger.error("CbMacGabServiceImpl.queryAsObject时cbMacGab数据为空。");
    		throw new AppRuntimeException("CbMacGabServiceImpl.queryAsObject时cbMacGab数据为空。");
    	}
    }
    
    /**
     * @see CbMacGabService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return cbMacGabMapper.selectCountByExample(example);
    	}else{
    		logger.error("CbMacGabServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("CbMacGabServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see CbMacGabService#queryListByExample(Example example)
     */
    @Override
    public List<CbMacGab> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return cbMacGabMapper.selectByExample(example);
    	}else{
    		logger.error("CbMacGabServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("CbMacGabServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see CbMacGabService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(cbMacGabMapper.selectByExample(example));
    	}else{
    		logger.error("CbMacGabServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("CbMacGabServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see CbMacGabService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(CbMacGab.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		/*if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}*/
    		/*if(parmMap.get("sertype")!=null && StringUtils.isNotBlank(parmMap.get("sertype").toString())){
				criteria.andEqualTo("sertype",parmMap.get("sertype").toString().trim());
				flag = true;
			}*/
    		/*if(parmMap.get("qqpk")!=null && StringUtils.isNotBlank(parmMap.get("qqpk").toString())){
				criteria.andEqualTo("qqpk",parmMap.get("qqpk").toString().trim());
				flag = true;
			}*/
    		if(parmMap.get("jjdpk")!=null && StringUtils.isNotBlank(parmMap.get("jjdpk").toString())){
				criteria.andEqualTo("jjdpk",parmMap.get("jjdpk").toString().trim());
				flag = true;
			}
    		/*if(parmMap.get("ywsqbh")!=null && StringUtils.isNotBlank(parmMap.get("ywsqbh").toString())){
				criteria.andEqualTo("ywsqbh",parmMap.get("ywsqbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("macdz")!=null && StringUtils.isNotBlank(parmMap.get("macdz").toString())){
				criteria.andEqualTo("macdz",parmMap.get("macdz").toString().trim());
				flag = true;
			}*/
    		if(parmMap.get("qqzh")!=null && StringUtils.isNotBlank(parmMap.get("qqzh").toString())){
				criteria.andEqualTo("qqzh",parmMap.get("qqzh").toString().trim());
				flag = true;
			}
    		/*if(parmMap.get("qqzhxm")!=null && StringUtils.isNotBlank(parmMap.get("qqzhxm").toString())){
				criteria.andEqualTo("qqzhxm",parmMap.get("qqzhxm").toString().trim());
				flag = true;
			}*/
    		/*if(parmMap.get("qqzhjgdm")!=null && StringUtils.isNotBlank(parmMap.get("qqzhjgdm").toString())){
				criteria.andEqualTo("qqzhjgdm",parmMap.get("qqzhjgdm").toString().trim());
				flag = true;
			}*/
    		/*if(parmMap.get("qqzhjgmc")!=null && StringUtils.isNotBlank(parmMap.get("qqzhjgmc").toString())){
				criteria.andEqualTo("qqzhjgmc",parmMap.get("qqzhjgmc").toString().trim());
				flag = true;
			}*/
    		if(parmMap.get("qqlrdwdm")!=null && StringUtils.isNotBlank(parmMap.get("qqlrdwdm").toString())){
				criteria.andEqualTo("qqlrdwdm",parmMap.get("qqlrdwdm").toString().trim());
				flag = true;
			}
    		/*if(parmMap.get("qqlrdwmc")!=null && StringUtils.isNotBlank(parmMap.get("qqlrdwmc").toString())){
				criteria.andEqualTo("qqlrdwmc",parmMap.get("qqlrdwmc").toString().trim());
				flag = true;
			}*/
    		/*if(parmMap.get("lrsj")!=null && StringUtils.isNotBlank(parmMap.get("lrsj").toString())){
				criteria.andEqualTo("lrsj",parmMap.get("lrsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sy")!=null && StringUtils.isNotBlank(parmMap.get("sy").toString())){
				criteria.andEqualTo("sy",parmMap.get("sy").toString().trim());
				flag = true;
			}
    		if(parmMap.get("count")!=null && StringUtils.isNotBlank(parmMap.get("count").toString())){
				criteria.andEqualTo("count",parmMap.get("count").toString().trim());
				flag = true;
			}*/
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("CbMacGabServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("CbMacGabServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
	/**
	 * @see CbMacGabService#queryListByPage(Map parmMap,int pageNum,int pageSize)
	 */
	@Override
	public ResultDto queryListByJjdpk(String jjdpk, int pageNum, int pageSize) throws  Exception{
		if (StringUtils.isNotBlank(jjdpk)){
			PageInfo pageInfo = this.queryPageInfoByJjdpk(jjdpk,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
		}else{
			logger.error("CbGabServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("CbGabServiceImpl.queryListByPage时parmMap数据为空。");
		}
	}
	@Override
	public PageInfo queryPageInfoByJjdpk(String jjdpk,int pageNum,int pageSize) throws Exception {
		if(StringUtils.isNotBlank(jjdpk)){
			PageHelper.startPage(pageNum,pageSize);
			return new PageInfo(cbMacGabMapper.queryCbMacGabByJjdpk(jjdpk));
		}else{
			logger.error("CbGabServiceImpl.queryListByExample时example数据为空。");
			throw new AppRuntimeException("CbGabServiceImpl.queryListByExample时example数据为空。");
		}
	}
}
