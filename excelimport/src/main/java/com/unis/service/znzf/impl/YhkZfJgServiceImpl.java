package com.unis.service.znzf.impl;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.YhkZfJgMapper;
import com.unis.model.znzf.YhkZfJg;
import com.unis.service.znzf.YhkZfJgService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see YhkZfJgService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-02-25
 */
@Service("yhkZfJgService")
public class YhkZfJgServiceImpl extends BaseServiceImpl implements YhkZfJgService {
	private static final Logger logger = LoggerFactory.getLogger(YhkZfJgServiceImpl.class);
    @Autowired
    private YhkZfJgMapper yhkZfJgMapper;

    /**
     * @see YhkZfJgService#insert(YhkZfJg yhkZfJg)
     */
    @Override
    public int insert(YhkZfJg yhkZfJg) throws Exception {
    	if (yhkZfJg!=null){
	        //yhkZfJg.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return yhkZfJgMapper.insertSelective(yhkZfJg);
    	}else{
    		logger.error("YhkZfJgServiceImpl.insert时yhkZfJg数据为空。");
    		throw new AppRuntimeException("YhkZfJgServiceImpl.insert时yhkZfJg数据为空。");
    	}        
    }

    /**
     * @see YhkZfJgService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("YhkZfJgServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("YhkZfJgServiceImpl.delete时pk为空。");
    	}else{
    		return yhkZfJgMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see YhkZfJgService#updateByPrimaryKey(YhkZfJg yhkZfJg)
     */
    @Override
    public int updateByPrimaryKey(YhkZfJg yhkZfJg) throws Exception {
        if (yhkZfJg!=null){
        	if(StringUtils.isBlank(yhkZfJg.getPk())){
        		logger.error("YhkZfJgServiceImpl.updateByPrimaryKey时yhkZfJg.Pk为空。");
        		throw new AppRuntimeException("YhkZfJgServiceImpl.updateByPrimaryKey时yhkZfJg.Pk为空。");
        	}
	        return yhkZfJgMapper.updateByPrimaryKeySelective(yhkZfJg);
    	}else{
    		logger.error("YhkZfJgServiceImpl.updateByPrimaryKey时yhkZfJg数据为空。");
    		throw new AppRuntimeException("YhkZfJgServiceImpl.updateByPrimaryKey时yhkZfJg数据为空。");
    	}
    }
    /**
     * @see YhkZfJgService#queryYhkZfJgByPrimaryKey(String pk)
     */
    @Override
    public YhkZfJg queryYhkZfJgByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("YhkZfJgServiceImpl.queryYhkZfJgByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("YhkZfJgServiceImpl.queryYhkZfJgByPrimaryKey时pk为空。");
    	}else{
    		return yhkZfJgMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see YhkZfJgService#queryAsObject(YhkZfJg yhkZfJg)
     */
    @Override
    public YhkZfJg queryAsObject(YhkZfJg yhkZfJg) throws Exception {
        if (yhkZfJg!=null){
	        return yhkZfJgMapper.selectOne(yhkZfJg);
    	}else{
    		logger.error("YhkZfJgServiceImpl.queryAsObject时yhkZfJg数据为空。");
    		throw new AppRuntimeException("YhkZfJgServiceImpl.queryAsObject时yhkZfJg数据为空。");
    	}
    }
    
    /**
     * @see YhkZfJgService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return yhkZfJgMapper.selectCountByExample(example);
    	}else{
    		logger.error("YhkZfJgServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("YhkZfJgServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see YhkZfJgService#queryListByExample(Example example)
     */
    @Override
    public List<YhkZfJg> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return yhkZfJgMapper.selectByExample(example);
    	}else{
    		logger.error("YhkZfJgServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("YhkZfJgServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see YhkZfJgService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(yhkZfJgMapper.selectByExample(example));
    	}else{
    		logger.error("YhkZfJgServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("YhkZfJgServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see YhkZfJgService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(YhkZfJg.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qqpk")!=null && StringUtils.isNotBlank(parmMap.get("qqpk").toString())){
				criteria.andEqualTo("qqpk",parmMap.get("qqpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jjdpk")!=null && StringUtils.isNotBlank(parmMap.get("jjdpk").toString())){
				criteria.andEqualTo("jjdpk",parmMap.get("jjdpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("applicationid")!=null && StringUtils.isNotBlank(parmMap.get("applicationid").toString())){
				criteria.andEqualTo("applicationid",parmMap.get("applicationid").toString().trim());
				flag = true;
			}
    		if(parmMap.get("resultcode")!=null && StringUtils.isNotBlank(parmMap.get("resultcode").toString())){
				criteria.andEqualTo("resultcode",parmMap.get("resultcode").toString().trim());
				flag = true;
			}
    		if(parmMap.get("accounttype")!=null && StringUtils.isNotBlank(parmMap.get("accounttype").toString())){
				criteria.andEqualTo("accounttype",parmMap.get("accounttype").toString().trim());
				flag = true;
			}
    		if(parmMap.get("accountnumber")!=null && StringUtils.isNotBlank(parmMap.get("accountnumber").toString())){
				criteria.andEqualTo("accountnumber",parmMap.get("accountnumber").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cardnumber")!=null && StringUtils.isNotBlank(parmMap.get("cardnumber").toString())){
				criteria.andEqualTo("cardnumber",parmMap.get("cardnumber").toString().trim());
				flag = true;
			}
    		if(parmMap.get("accountbalance")!=null && StringUtils.isNotBlank(parmMap.get("accountbalance").toString())){
				criteria.andEqualTo("accountbalance",parmMap.get("accountbalance").toString().trim());
				flag = true;
			}
    		if(parmMap.get("starttime")!=null && StringUtils.isNotBlank(parmMap.get("starttime").toString())){
				criteria.andEqualTo("starttime",parmMap.get("starttime").toString().trim());
				flag = true;
			}
    		if(parmMap.get("expiretime")!=null && StringUtils.isNotBlank(parmMap.get("expiretime").toString())){
				criteria.andEqualTo("expiretime",parmMap.get("expiretime").toString().trim());
				flag = true;
			}
    		if(parmMap.get("failurecause")!=null && StringUtils.isNotBlank(parmMap.get("failurecause").toString())){
				criteria.andEqualTo("failurecause",parmMap.get("failurecause").toString().trim());
				flag = true;
			}
    		if(parmMap.get("feedbackremark")!=null && StringUtils.isNotBlank(parmMap.get("feedbackremark").toString())){
				criteria.andEqualTo("feedbackremark",parmMap.get("feedbackremark").toString().trim());
				flag = true;
			}
    		if(parmMap.get("feedbackorgname")!=null && StringUtils.isNotBlank(parmMap.get("feedbackorgname").toString())){
				criteria.andEqualTo("feedbackorgname",parmMap.get("feedbackorgname").toString().trim());
				flag = true;
			}
    		if(parmMap.get("operatorname")!=null && StringUtils.isNotBlank(parmMap.get("operatorname").toString())){
				criteria.andEqualTo("operatorname",parmMap.get("operatorname").toString().trim());
				flag = true;
			}
    		if(parmMap.get("operatorphonenumber")!=null && StringUtils.isNotBlank(parmMap.get("operatorphonenumber").toString())){
				criteria.andEqualTo("operatorphonenumber",parmMap.get("operatorphonenumber").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fksj")!=null && StringUtils.isNotBlank(parmMap.get("fksj").toString())){
				criteria.andEqualTo("fksj",parmMap.get("fksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("YhkZfJgServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("YhkZfJgServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
