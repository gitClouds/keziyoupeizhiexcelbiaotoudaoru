package com.unis.service.znzf;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import com.unis.model.znzf.Jlt;
import com.unis.model.znzf.SfZfQq;
import com.unis.pojo.znzf.ReqModel;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.unis.service.BaseService;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;

/**
 * <pre>
 * TODO
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-02-28
 */
public interface SfZfQqService extends BaseService {

    /**
     * 新增SfZfQq实例
     * 
     * @param sfZfQq
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int insert(SfZfQq sfZfQq) throws Exception;
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int insertSfZf(ReqModel reqModel) throws Exception;
    /**
     * 删除SfZfQq实例
     * 
     * @param pk
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int delete(String pk) throws Exception;

    /**
     * 更新SfZfQq实例
     * 
     * @param sfZfQq
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int updateByPrimaryKey(SfZfQq sfZfQq) throws Exception;
    /**
     * 根据pk主键获取对象
     * 
     * @param pk
     * @throws Exception
     */
    SfZfQq querySfZfQqByPrimaryKey(String pk) throws Exception;

    /**
     * 查询SfZfQq实例
     * 
     * @param sfZfQq
     * @throws Exception
     */
    SfZfQq queryAsObject(SfZfQq sfZfQq) throws Exception;
    
    	
	/**
     * 根据条件获取条数
     * 
     * @param example
     * @throws Exception
     */
	int queryCountByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合
     * 
     * @param example
     * @throws Exception
     */
	List<SfZfQq> queryListByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合(分页),需在example 设定排序
     * 
     * @param example
     * @param pageNum
     * @param pageSize
     * @throws Exception
     */
	PageInfo queryPageInfoByExample(Example example, int pageNum, int pageSize) throws Exception;

	/**
     * 根据入参获取数据集合(分页)
     * @param parmMap
     * @param pageNum
     * @param pageSize
     * @return
     * @throws Exception
     */
    ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception;

    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    String insertSfZf(Jlt jlt) throws Exception;

}
