package com.unis.service.znzf.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.unis.common.util.TemplateUtil;
import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.ZnzfQueueMapper;
import com.unis.model.znzf.ZnzfQueue;
import com.unis.service.znzf.ZnzfQueueService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see ZnzfQueueService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-02-14
 */
@Service("znzfQueueService")
public class ZnzfQueueServiceImpl extends BaseServiceImpl implements ZnzfQueueService {
	private static final Logger logger = LoggerFactory.getLogger(ZnzfQueueServiceImpl.class);
    @Autowired
    private ZnzfQueueMapper znzfQueueMapper;

    /**
     * @see ZnzfQueueService#insert(ZnzfQueue znzfQueue)
     */
    @Override
    public int insert(ZnzfQueue znzfQueue) throws Exception {
    	if (znzfQueue!=null){
	        znzfQueue.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	        return znzfQueueMapper.insertSelective(znzfQueue);
    	}else{
    		logger.error("ZnzfQueueServiceImpl.insert时znzfQueue数据为空。");
    		throw new AppRuntimeException("ZnzfQueueServiceImpl.insert时znzfQueue数据为空。");
    	}        
    }

	@Override
	public int insertBatchZt(List<Map> dataList,String serType,short yxj) throws Exception {
		if (dataList!=null && !dataList.isEmpty() && dataList.size()>0){
			List<Map> list = new ArrayList<>();
			for (Map map : dataList){
				map.put("serpk",map.get("ztpk"));
				map.put("pk",TemplateUtil.genUUID());
				map.put("sertype",serType);
				map.put("yxj",yxj);
				list.add(map);
			}
			if (list!=null && !list.isEmpty() && list.size()>0){
				return znzfQueueMapper.insertBatch(list);
			}
		}
		return 0;
	}
	@Override
	public int insertBatchMx(List<Map> dataList,String serType,short yxj) throws Exception {
		if (dataList!=null && !dataList.isEmpty() && dataList.size()>0){
			List<Map> list = new ArrayList<>();
			for (Map map : dataList){
				map.put("serpk",map.get("glpk"));
				map.put("pk",TemplateUtil.genUUID());
				map.put("sertype",serType);
				if("0".equals(map.get("subjecttype").toString())) {
					map.put("yxj",(short)-1);
				}else {
					map.put("yxj",yxj);
				}
				list.add(map);
			}
			if (list!=null && !list.isEmpty() && list.size()>0){
				return znzfQueueMapper.insertBatch(list);
			}
		}
		return 0;
	}
	@Override
	public int insertBatch(List<Map> dataList,String serType,short yxj) throws Exception {
		if (dataList!=null && !dataList.isEmpty() && dataList.size()>0){
			List<Map> list = new ArrayList<>();
			for (Map map : dataList){
				map.put("serpk",map.get("pk"));
				map.put("pk",TemplateUtil.genUUID());
				map.put("sertype",serType);
				map.put("yxj",yxj);
				list.add(map);
			}
			if (list!=null && !list.isEmpty() && list.size()>0){
				return znzfQueueMapper.insertBatch(list);
			}
		}
		return 0;
	}

	/**
     * @see ZnzfQueueService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("ZnzfQueueServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("ZnzfQueueServiceImpl.delete时pk为空。");
    	}else{
    		return znzfQueueMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see ZnzfQueueService#updateByPrimaryKey(ZnzfQueue znzfQueue)
     */
    @Override
    public int updateByPrimaryKey(ZnzfQueue znzfQueue) throws Exception {
        if (znzfQueue!=null){
        	if(StringUtils.isBlank(znzfQueue.getPk())){
        		logger.error("ZnzfQueueServiceImpl.updateByPrimaryKey时znzfQueue.Pk为空。");
        		throw new AppRuntimeException("ZnzfQueueServiceImpl.updateByPrimaryKey时znzfQueue.Pk为空。");
        	}
	        return znzfQueueMapper.updateByPrimaryKeySelective(znzfQueue);
    	}else{
    		logger.error("ZnzfQueueServiceImpl.updateByPrimaryKey时znzfQueue数据为空。");
    		throw new AppRuntimeException("ZnzfQueueServiceImpl.updateByPrimaryKey时znzfQueue数据为空。");
    	}
    }
    /**
     * @see ZnzfQueueService#queryZnzfQueueByPrimaryKey(String pk)
     */
    @Override
    public ZnzfQueue queryZnzfQueueByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("ZnzfQueueServiceImpl.queryZnzfQueueByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("ZnzfQueueServiceImpl.queryZnzfQueueByPrimaryKey时pk为空。");
    	}else{
    		return znzfQueueMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see ZnzfQueueService#queryAsObject(ZnzfQueue znzfQueue)
     */
    @Override
    public ZnzfQueue queryAsObject(ZnzfQueue znzfQueue) throws Exception {
        if (znzfQueue!=null){
	        return znzfQueueMapper.selectOne(znzfQueue);
    	}else{
    		logger.error("ZnzfQueueServiceImpl.queryAsObject时znzfQueue数据为空。");
    		throw new AppRuntimeException("ZnzfQueueServiceImpl.queryAsObject时znzfQueue数据为空。");
    	}
    }
    
    /**
     * @see ZnzfQueueService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return znzfQueueMapper.selectCountByExample(example);
    	}else{
    		logger.error("ZnzfQueueServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("ZnzfQueueServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see ZnzfQueueService#queryListByExample(Example example)
     */
    @Override
    public List<ZnzfQueue> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return znzfQueueMapper.selectByExample(example);
    	}else{
    		logger.error("ZnzfQueueServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("ZnzfQueueServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see ZnzfQueueService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(znzfQueueMapper.selectByExample(example));
    	}else{
    		logger.error("ZnzfQueueServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("ZnzfQueueServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see ZnzfQueueService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(ZnzfQueue.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jjdpk")!=null && StringUtils.isNotBlank(parmMap.get("jjdpk").toString())){
				criteria.andEqualTo("jjdpk",parmMap.get("jjdpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("serpk")!=null && StringUtils.isNotBlank(parmMap.get("serpk").toString())){
				criteria.andEqualTo("serpk",parmMap.get("serpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sertype")!=null && StringUtils.isNotBlank(parmMap.get("sertype").toString())){
				criteria.andEqualTo("sertype",parmMap.get("sertype").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bankcode")!=null && StringUtils.isNotBlank(parmMap.get("bankcode").toString())){
				criteria.andEqualTo("bankcode",parmMap.get("bankcode").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bankname")!=null && StringUtils.isNotBlank(parmMap.get("bankname").toString())){
				criteria.andEqualTo("bankname",parmMap.get("bankname").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cardnumber")!=null && StringUtils.isNotBlank(parmMap.get("cardnumber").toString())){
				criteria.andEqualTo("cardnumber",parmMap.get("cardnumber").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrdwdm")!=null && StringUtils.isNotBlank(parmMap.get("lrdwdm").toString())){
				criteria.andEqualTo("lrdwdm",parmMap.get("lrdwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrdwmc")!=null && StringUtils.isNotBlank(parmMap.get("lrdwmc").toString())){
				criteria.andEqualTo("lrdwmc",parmMap.get("lrdwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrrxm")!=null && StringUtils.isNotBlank(parmMap.get("lrrxm").toString())){
				criteria.andEqualTo("lrrxm",parmMap.get("lrrxm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrrjh")!=null && StringUtils.isNotBlank(parmMap.get("lrrjh").toString())){
				criteria.andEqualTo("lrrjh",parmMap.get("lrrjh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxj")!=null && StringUtils.isNotBlank(parmMap.get("yxj").toString())){
				criteria.andEqualTo("yxj",parmMap.get("yxj").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("ZnzfQueueServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("ZnzfQueueServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
