package com.unis.service.znzf.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.unis.common.config.DS;
import com.unis.common.enums.QqlxEnum;
import com.unis.common.util.TemplateUtil;
import com.unis.dto.ResultDto;
import com.unis.model.znzf.*;
import com.unis.service.znzf.*;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.JltMapper;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see JltService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-03-13
 */
@Service("jltService")
public class JltServiceImpl extends BaseServiceImpl implements JltService {
	private static final Logger logger = LoggerFactory.getLogger(JltServiceImpl.class);
    @Autowired
    private JltMapper jltMapper;
    @Autowired
    private YhkZfQqService yhkZfQqService;
    @Autowired
    private SfZfQqService sfZfQqService;
	@Autowired
    private YhkMxQqService yhkMxQqService;
	@Autowired
    private YhkZtQqService yhkZtQqService;
	@Autowired
    private SfZtQqService sfZtQqService;
    @Autowired
    private SfMxQqService sfMxQqService;
    @Autowired
    private SfQzhQqService sfQzhQqService;
    @Autowired
    private SfLsQqService sfLsQqService;
    @Autowired
    private AnalysisQueueService analysisQueueService;
    @Autowired
    private YhkMxJgLstService yhkMxJgLstService;
    @Autowired
    private SfMxJgLstService sfMxJgLstService;
    /**
     * @see JltService#insert(Jlt jlt)
     */
    @Override
    public int insert(Jlt jlt) throws Exception {
    	if (jlt!=null){
	        //jlt.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return jltMapper.insertSelective(jlt);
    	}else{
    		logger.error("JltServiceImpl.insert时jlt数据为空。");
    		throw new AppRuntimeException("JltServiceImpl.insert时jlt数据为空。");
    	}        
    }

    /**
     * @see JltService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("JltServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("JltServiceImpl.delete时pk为空。");
    	}else{
    		return jltMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see JltService#updateByPrimaryKey(Jlt jlt)
     */
    @Override
    public int updateByPrimaryKey(Jlt jlt) throws Exception {
        if (jlt!=null){
        	if(StringUtils.isBlank(jlt.getPk())){
        		logger.error("JltServiceImpl.updateByPrimaryKey时jlt.Pk为空。");
        		throw new AppRuntimeException("JltServiceImpl.updateByPrimaryKey时jlt.Pk为空。");
        	}
	        return jltMapper.updateByPrimaryKeySelective(jlt);
    	}else{
    		logger.error("JltServiceImpl.updateByPrimaryKey时jlt数据为空。");
    		throw new AppRuntimeException("JltServiceImpl.updateByPrimaryKey时jlt数据为空。");
    	}
    }
    /**
     * @see JltService#queryJltByPrimaryKey(String pk)
     */
    @Override
    public Jlt queryJltByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("JltServiceImpl.queryJltByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("JltServiceImpl.queryJltByPrimaryKey时pk为空。");
    	}else{
    		return jltMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see JltService#queryAsObject(Jlt jlt)
     */
    @Override
    public Jlt queryAsObject(Jlt jlt) throws Exception {
        if (jlt!=null){
	        return jltMapper.selectOne(jlt);
    	}else{
    		logger.error("JltServiceImpl.queryAsObject时jlt数据为空。");
    		throw new AppRuntimeException("JltServiceImpl.queryAsObject时jlt数据为空。");
    	}
    }
    
    /**
     * @see JltService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return jltMapper.selectCountByExample(example);
    	}else{
    		logger.error("JltServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("JltServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see JltService#queryListByExample(Example example)
     */
    @Override
    public List<Jlt> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return jltMapper.selectByExample(example);
    	}else{
    		logger.error("JltServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("JltServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see JltService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(jltMapper.selectByExample(example));
    	}else{
    		logger.error("JltServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("JltServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see JltService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(Jlt.class);
    		example.setOrderByClause("accountname,pk");//设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
			//是否是侦查员工作室
			if(parmMap.get("iszcygzs")!=null && StringUtils.isNotBlank(parmMap.get("iszcygzs").toString()) && NumberUtils.isNumber(parmMap.get("iszcygzs").toString()) && NumberUtils.toInt(parmMap.get("iszcygzs").toString())>=1){
				criteria.andEqualTo("iszcygzs",NumberUtils.toInt(parmMap.get("iszcygzs").toString()));
			}else{
				criteria.andEqualTo("isshow",1);
				if(parmMap.get("nlevel")!=null && StringUtils.isNotBlank(parmMap.get("nlevel").toString())){
					criteria.andEqualTo("nlevel",parmMap.get("nlevel").toString().trim());
					flag = true;
				}
			}

    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jjdpk")!=null && StringUtils.isNotBlank(parmMap.get("jjdpk").toString())){
				criteria.andEqualTo("jjdpk",parmMap.get("jjdpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sertype")!=null && StringUtils.isNotBlank(parmMap.get("sertype").toString())){
				criteria.andEqualTo("sertype",parmMap.get("sertype").toString().trim());
				flag = true;
			}
    		if(parmMap.get("applicationid")!=null && StringUtils.isNotBlank(parmMap.get("applicationid").toString())){
				criteria.andEqualTo("applicationid",parmMap.get("applicationid").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bankcode")!=null && StringUtils.isNotBlank(parmMap.get("bankcode").toString())){
				criteria.andEqualTo("bankcode",parmMap.get("bankcode").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bankname")!=null && StringUtils.isNotBlank(parmMap.get("bankname").toString())){
				criteria.andEqualTo("bankname",parmMap.get("bankname").toString().trim());
				flag = true;
			}
    		if(parmMap.get("accountname")!=null && StringUtils.isNotBlank(parmMap.get("accountname").toString())){
				criteria.andEqualTo("accountname",parmMap.get("accountname").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cardnumber")!=null && StringUtils.isNotBlank(parmMap.get("cardnumber").toString())){
				criteria.andEqualTo("cardnumber",parmMap.get("cardnumber").toString().trim());
				flag = true;
			}

    		if(parmMap.get("jtype")!=null && StringUtils.isNotBlank(parmMap.get("jtype").toString())){
				criteria.andEqualTo("jtype",parmMap.get("jtype").toString().trim());
				flag = true;
			}
    		if(parmMap.get("parentpk")!=null && StringUtils.isNotBlank(parmMap.get("parentpk").toString())){
				criteria.andEqualTo("parentpk",parmMap.get("parentpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("parentzh")!=null && StringUtils.isNotBlank(parmMap.get("parentzh").toString())){
				criteria.andEqualTo("parentzh",parmMap.get("parentzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zfzt")!=null && StringUtils.isNotBlank(parmMap.get("zfzt").toString())){
				criteria.andEqualTo("zfzt",parmMap.get("zfzt").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zfpk")!=null && StringUtils.isNotBlank(parmMap.get("zfpk").toString())){
				criteria.andEqualTo("zfpk",parmMap.get("zfpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("djzt")!=null && StringUtils.isNotBlank(parmMap.get("djzt").toString())){
				criteria.andEqualTo("djzt",parmMap.get("djzt").toString().trim());
				flag = true;
			}
    		if(parmMap.get("djpk")!=null && StringUtils.isNotBlank(parmMap.get("djpk").toString())){
				criteria.andEqualTo("djpk",parmMap.get("djpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
			if(parmMap.get("fxbs")!=null && StringUtils.isNotBlank(parmMap.get("fxbs").toString())){
				criteria.andEqualTo("fxbs",parmMap.get("fxbs").toString().trim());
				flag = true;
			}
			if(parmMap.get("xskbs")!=null && StringUtils.isNotBlank(parmMap.get("xskbs").toString())){
				criteria.andEqualTo("xskbs",parmMap.get("xskbs").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("JltServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("JltServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
	@Override
	public ResultDto queryListByJjdpkAndCbyj(Map parmMap, int pageNum, int pageSize) throws  Exception {
		if (parmMap != null) {
			Example example = new Example(Jlt.class);
			example.setOrderByClause("parentpk,pk");//设置排序
			Example.Criteria criteria = example.createCriteria();
			boolean flag = false;
			if(parmMap.get("jjdpk")!=null && StringUtils.isNotBlank(parmMap.get("jjdpk").toString())){
				criteria.andEqualTo("jjdpk",parmMap.get("jjdpk").toString().trim());
				flag = true;
			}

			if(parmMap.get("cbyj")!=null && StringUtils.isNotBlank(parmMap.get("cbyj").toString())){
				String[] strs = parmMap.get("cbyj").toString().trim().split(",");
				List<String> list= Arrays.asList(strs);

				criteria.andIn("cardnumber",list);
				flag = true;
			}
			//一个条件都没有的时候给一个默认条件
			if(!flag){

			}
			PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
		}else{
			logger.error("JltServiceImpl.queryListByJjdpkAndCbyj时parmMap数据为空。");
			throw new AppRuntimeException("JltServiceImpl.queryListByJjdpkAndCbyj时parmMap数据为空。");
		}
	}
	@Override
	public int getJltLevel(String jjdPk,int iszcygzs) throws Exception {
		int jltLevel = -1;
		if (StringUtils.isNotBlank(jjdPk) && StringUtils.isNotBlank(jjdPk)){
			if (iszcygzs>=1){
				jltLevel = jltMapper.getZcygzsJltLevel(jjdPk);
			}else{
				jltLevel = jltMapper.getZcxxJltLevel(jjdPk);
			}
		}
		return jltLevel;
	}
	@Override
	public int checkJltAndInsert(Map map) throws Exception{
		/*Jlt jlt = new Jlt();
		jlt.setJjdpk((String) map.get("jjdpk"));
		jlt.setCardnumber((String)map.get("cardnumber"));
		jlt.setNlevel((Short)map.get("nlevel"));
		jlt = this.queryAsObject(jlt);*/
//		Example example = new Example(Jlt.class);
//		Example.Criteria criteria = example.createCriteria();
//		example.setOrderByClause("rksj desc");//设置排序
//		criteria.andEqualTo("jjdpk",map.get("jjdpk").toString().trim());
//		criteria.andEqualTo("cardnumber",map.get("cardnumber").toString().trim());
//		criteria.andEqualTo("nlevel",map.get("nlevel").toString().trim());
//
//		List<Jlt> jltList = this.queryListByExample(example);
//		//int count = this.queryCountByExample(example);
//
//		if(jltList!=null && !jltList.isEmpty() && jltList.size()>0){
//			Jlt jlt = jltList.get(0);
//			//卡号在金流图中存在，并且金流图中类别为银行卡，但是当前请求是三方，修改显示全账号查询
//			if (map.get("sertype")!=null && StringUtils.equalsIgnoreCase(map.get("sertype").toString(),"2")
//					&& jlt.getSertype()==1){
//				Jlt updJlt = new Jlt();
//				updJlt.setQzhzt((short) 1);
//				updJlt.setPk(jlt.getPk());
//				this.updateByPrimaryKey(updJlt);
//			}
//		}else{
			Jlt jlt = new Jlt();
			jlt.setPk(TemplateUtil.genUUID());
			jlt.setCardnumber((String) map.get("cardnumber"));//账号
			jlt.setAccountname(map.get("accountname")==null?"":(String) map.get("accountname"));//姓名
			jlt.setJjdpk((String) map.get("jjdpk"));
			jlt.setNlevel((short)1);//'金流图中等级(0=受害人；1=一级卡...)';
			if(map.get("sertype")!=null && map.get("sertype")!=""){//'请求类型(1=yhk;2=sf)';
				jlt.setSertype(new Short(map.get("sertype").toString()));
			}

			if(map.get("parentpk")!=null&&map.get("parentpk")!=""){
				jlt.setParentpk((String)map.get("parentpk"));
				jlt.setParentzh(map.get("parentzh")==null?"":(String)map.get("parentzh"));
			}else{
				jlt.setParentpk("0");
			}
			jlt.setBankcode(map.get("bankcode")==null?"":(String)map.get("bankcode"));
			jlt.setBankname(map.get("bankname")==null?"":(String)map.get("bankname"));
			jlt.setCardid(map.get("cardid")==null?"":(String)map.get("cardid"));
			jlt.setPhone(map.get("phone")==null?"":(String)map.get("phone"));

			if(map.get("qzhzt")!=null && StringUtils.isNotBlank((String) map.get("qzhzt")) && NumberUtils.isNumber((String) map.get("qzhzt")) && NumberUtils.toInt((String)map.get("qzhzt"))==1 ){
				jlt.setQzhzt((short) 1);
			}
			if(map.get("iszcygzs")!=null && StringUtils.isNotBlank(map.get("iszcygzs").toString()) && NumberUtils.isNumber(map.get("iszcygzs").toString()) && NumberUtils.toInt(map.get("iszcygzs").toString()) >= 1){
				jlt.setIszcygzs(NumberUtils.toShort(map.get("iszcygzs").toString()));
				jlt.setIsshow((short) 0);
			}
			jlt.setMxqqpk((String) map.get("pk"));
			jlt.setZtqqpk((String) map.get("ztpk"));
			jlt.setQzhpk((String) map.get("qzhpk"));
			return this.insert(jlt);
//		}
//		return 1;
	}

	@Override
	public int updateDisabByPrimaryKey(String pk) throws Exception {
		if(StringUtils.isBlank(pk)){
			logger.error("JltServiceImpl.updateDisabByPrimaryKey时pk为空。");
			throw new AppRuntimeException("JltServiceImpl.updateDisabByPrimaryKey时pk为空。");
		}else{
			return jltMapper.updateDisabled(pk);
		}
	}

	@Override
	public int updateNlevel(String pk, String cardnumber, String parentzh,int nlevel) throws Exception{
		if (StringUtils.isNotBlank(pk) && StringUtils.isNotBlank(cardnumber) && StringUtils.isNotBlank(parentzh) && nlevel > 0){
			//获取当前金流图数据
			Jlt thisJlt = this.queryJltByPrimaryKey(pk);
			if (thisJlt!=null){
				//验证此账号在指定等级是否存在
				Example jltExample = new Example(Jlt.class);
				Example.Criteria jltCriteria = jltExample.createCriteria();
				jltCriteria.andEqualTo("nlevel",nlevel);
				jltCriteria.andEqualTo("cardnumber",cardnumber);
				jltCriteria.andEqualTo("jjdpk",thisJlt.getJjdpk());

				List<Jlt> jltList = this.queryListByExample(jltExample);
				if (jltList!=null && !jltList.isEmpty() && jltList.size()>0){
					return -1;
				}else{
					//验证上级账号是否存在
					Jlt parentJlt = null;
					Example jltExample2 = new Example(Jlt.class);
					Example.Criteria jltCriteria2 = jltExample2.createCriteria();
					jltCriteria2.andEqualTo("nlevel",(nlevel-1));
					jltCriteria2.andEqualTo("cardnumber",parentzh.trim());
					jltCriteria2.andEqualTo("jjdpk",thisJlt.getJjdpk());

					jltList = this.queryListByExample(jltExample2);

					if (jltList!=null && !jltList.isEmpty() && jltList.size()>0){
						parentJlt = jltList.get(0);
					}

					if (parentJlt==null){
						return -2;
					}else{
						//获取所有子节点（包含本节点）
						List<Jlt> childJlt = jltMapper.queryChildJltByPk(thisJlt.getPk());
						if (childJlt!=null && !childJlt.isEmpty() && childJlt.size()>0){
							for (Jlt jltNode : childJlt){
								if (jltNode!=null && StringUtils.isNotBlank(jltNode.getJjdpk()) && StringUtils.isNotBlank(jltNode.getCardnumber()) && jltNode.getSertype()!=null && jltNode.getNlevel()!=null && jltNode.getNlevel()>0){
									if (jltNode.getSertype()==1){//银行卡
										yhkMxQqService.updateLevelByJlt(jltNode.getJjdpk(),jltNode.getCardnumber(),jltNode.getNlevel(),nlevel,thisJlt.getNlevel());
										yhkZtQqService.updateLevelByJlt(jltNode.getJjdpk(),jltNode.getCardnumber(),jltNode.getNlevel(),nlevel,thisJlt.getNlevel());
									}else if (jltNode.getSertype()==2){//三方
										sfMxQqService.updateLevelByJlt(jltNode.getJjdpk(),jltNode.getCardnumber(),jltNode.getNlevel(),nlevel,thisJlt.getNlevel());
										sfZtQqService.updateLevelByJlt(jltNode.getJjdpk(),jltNode.getCardnumber(),jltNode.getNlevel(),nlevel,thisJlt.getNlevel());
										sfQzhQqService.updateLevelByJlt(jltNode.getJjdpk(),jltNode.getCardnumber(),jltNode.getNlevel(),nlevel,thisJlt.getNlevel());
									}else if (jltNode.getSertype()==3) {//流水
										sfLsQqService.updateLevelByJlt(jltNode.getJjdpk(),jltNode.getCardnumber(),jltNode.getNlevel(),nlevel,thisJlt.getNlevel());
									}
								}
							}
							//修改金流图等级
							Jlt updJlt = new Jlt();
							updJlt.setPk(thisJlt.getPk());
							updJlt.setParentzh(parentJlt.getCardnumber());
							updJlt.setParentpk(parentJlt.getPk());
							//updJlt.setIsshow((short) 1);
							this.updateByPrimaryKey(updJlt);
							return jltMapper.updateZcygzsJltLevel(thisJlt.getPk(),nlevel,thisJlt.getNlevel());
						}else{
							return -3;
						}
					}
				}
			}else{
				return 0;
			}
		}else{
			logger.error("JltServiceImpl.updateNlevel时pk/cardnumber/parentzh/nlevel为空。");
			throw new AppRuntimeException("JltServiceImpl.updateNlevel时pk/cardnumber/parentzh/nlevel为空。");
		}
	}

	@Override
	public String launchQq(String pk, String qqType) throws Exception {
    	String result = null;
    	if (StringUtils.isNotBlank(pk) && StringUtils.isNotBlank(qqType)){
    		Jlt jlt = this.queryJltByPrimaryKey(pk);
    		if (jlt!=null){
    			if (jlt.getSertype()==1){//银行
					if (StringUtils.lastIndexOf(QqlxEnum.yhkQqlxZhifu.getKey(),qqType) >=0){//止付
						String zfpk = null;
						zfpk = yhkZfQqService.insertYhkZf(jlt);
						if (StringUtils.isNotBlank(zfpk)){
							//修改资金流中zfpk
							Jlt updJlt = new Jlt();
							updJlt.setPk(jlt.getPk());
							updJlt.setZfpk(zfpk);
							this.updateByPrimaryKey(updJlt);
							result = zfpk;
						}
					}else if (StringUtils.lastIndexOf(QqlxEnum.yhkQqlxDongjie.getKey(),qqType) >=0){//冻结

						/*Jlt updJlt = new Jlt();
						updJlt.setPk(jlt.getPk());
						updJlt.setDjpk("");
						this.updateByPrimaryKey(updJlt);*/
					}
				}else if (jlt.getSertype()==2){//三方
					if (StringUtils.lastIndexOf(QqlxEnum.sfQqlxZhifu.getKey(),qqType) >=0){//止付
						String zfpk = null;
						zfpk = sfZfQqService.insertSfZf(jlt);
						if (StringUtils.isNotBlank(zfpk)){
							//修改资金流中zfpk
							Jlt updJlt = new Jlt();
							updJlt.setPk(jlt.getPk());
							updJlt.setZfpk(zfpk);
							this.updateByPrimaryKey(updJlt);
							result = zfpk;
						}
					}else if (StringUtils.lastIndexOf(QqlxEnum.sfQqlxDongjie.getKey(),qqType) >=0){//冻结

						/*Jlt updJlt = new Jlt();
						updJlt.setPk(jlt.getPk());
						updJlt.setDjpk("");
						this.updateByPrimaryKey(updJlt);*/
					}
				}
			}
		}else{
			logger.error("JltServiceImpl.launchQq时pk/qqType数据为空。");
			throw new AppRuntimeException("JltServiceImpl.launchQq时pk/qqType数据为空。");
		}
		return result;
	}

	@Override
	public List<Map> queryJltTreeByJjd(String jjdpk) throws Exception {
    	if (StringUtils.isNotBlank(jjdpk)){
			return jltMapper.selectJltTreeByJjd(jjdpk);
		}else{
			logger.error("JltServiceImpl.queryJltTreeByJjd时jjdpk数据为空。");
			throw new AppRuntimeException("JltServiceImpl.queryJltTreeByJjd时jjdpk数据为空。");
		}
	}

	@Override
	public int keepOnAnalysis(Jlt[] jlts) throws Exception {
    	int result =0;
    	if (jlts!=null && jlts.length>0){
    		for (Jlt jlt : jlts){
    			jlt.setKeepon((short)0);
    			this.updateByPrimaryKey(jlt);
    			result++;
    		}
    	}
    		
//    	if (jlts!=null && jlts.length>0){
//    		for (Jlt jlt : jlts){
//    			switch (jlt.getSertype()){
//					case 1://银行卡相关请求
//						//只有银行卡明细需要再次分析
//						//查询相关请求表
//						Example yhkExample = new Example(YhkMxQq.class);
//						Example.Criteria yhkCriteria = yhkExample.createCriteria();
//						yhkExample.setOrderByClause("pk");
//						yhkCriteria.andEqualTo("jjdpk",jlt.getJjdpk());
//						yhkCriteria.andEqualTo("cardnumber",jlt.getCardnumber());
//						yhkCriteria.andEqualTo("nlevel",jlt.getNlevel());
//						List<YhkMxQq> yhkMxQqList = yhkMxQqService.queryListByExample(yhkExample);
//						if (yhkMxQqList!=null && !yhkMxQqList.isEmpty() && yhkMxQqList.size()>0){
//							YhkMxQq yhkMxQq = yhkMxQqList.get(0);//yhkMxQqList.stream().filter(bean->bean.getResflag()==1).findFirst().orElse(null);
//							if (yhkMxQq!=null){
//								//查询请求相关结果
//								List<String> appids = yhkMxJgLstService.queryApplicationByQqpk(yhkMxQq.getPk());
//
//								if (appids!=null && !appids.isEmpty() && appids.size()>0){
//									List<AnalysisQueue> analysisQueueList = new ArrayList<>(appids.size());
//									for (String applicationid : appids){
//										AnalysisQueue analysisQueue = new AnalysisQueue();
//										analysisQueue.setPk(TemplateUtil.genUUID());
//										analysisQueue.setApplicationid(applicationid);
//										analysisQueue.setCardnumber(jlt.getCardnumber());
//										analysisQueue.setJjdpk(jlt.getJjdpk());
//										analysisQueue.setQqpk(yhkMxQq.getPk());
//										analysisQueue.setQqtype("Y-07");
//										analysisQueueList.add(analysisQueue);
//									}
//									//插入序列
//									analysisQueueService.insert(analysisQueueList);
//									//修改请求表
//									YhkMxQq updYhkMxQq = new YhkMxQq();
//									updYhkMxQq.setPk(yhkMxQq.getPk());
//									updYhkMxQq.setFxbs((short) 1);
//									yhkMxQqService.updateByPrimaryKey(updYhkMxQq);
//									//修改金流图表
//									Jlt updYhkJlt = new Jlt();
//									updYhkJlt.setPk(jlt.getPk());
//									updYhkJlt.setKeepon((short) 1);
//									this.updateByPrimaryKey(updYhkJlt);
//									result++;
//								}
//							}
//						}
//						break;
//					case 2://三方相关请求
//						//需分析三方明细、主体、全账号
//						boolean flag = false;
//						//三方主体
//						Example sfZtExample = new Example(SfZtQq.class);
//						Example.Criteria sfZtCriteria = sfZtExample.createCriteria();
//						sfZtExample.setOrderByClause("resdate asc,pk asc");
//						sfZtCriteria.andEqualTo("jjdpk",jlt.getJjdpk());
//						sfZtCriteria.andEqualTo("accnumber",jlt.getCardnumber());
//						sfZtCriteria.andEqualTo("nlevel",jlt.getNlevel());
//						List<SfZtQq> sfZtQqList = sfZtQqService.queryListByExample(sfZtExample);
//						if (sfZtQqList!=null && !sfZtQqList.isEmpty() && sfZtQqList.size()>0){
//							//已反馈、已分析的，修改为可再次分析
//							SfZtQq qq = sfZtQqList.stream().filter(bean->bean.getResflag()==1).findFirst().orElse(null);
//							if (qq!=null){
//								SfZtQq updQq = new SfZtQq();
//								updQq.setPk(qq.getPk());
//								updQq.setFxbs((short) 1);
//								sfZtQqService.updateByPrimaryKey(updQq);
//								flag = true;
//							}
//						}
//						//全账号
//						Example sfQzhExample = new Example(SfQzhQq.class);
//						Example.Criteria sfQzhCriteria = sfQzhExample.createCriteria();
//						sfQzhExample.setOrderByClause("resdate asc,pk asc");
//						sfQzhCriteria.andEqualTo("jjdpk",jlt.getJjdpk());
//						sfQzhCriteria.andEqualTo("accnumber",jlt.getCardnumber());
//						sfQzhCriteria.andEqualTo("nlevel",jlt.getNlevel());
//						sfQzhCriteria.andEqualTo("resflag",1);
//						List<SfQzhQq> sfQzhQqList = sfQzhQqService.queryListByExample(sfQzhExample);
//						if (sfQzhQqList!=null && !sfQzhQqList.isEmpty() && sfQzhQqList.size()>0){
//							//已反馈、已分析的，修改为可再次分析
//							for (SfQzhQq qq : sfQzhQqList){
//								SfQzhQq updQq = new SfQzhQq();
//								updQq.setPk(qq.getPk());
//								updQq.setFxbs((short) 1);
//								sfQzhQqService.updateByPrimaryKey(updQq);
//								flag = true;
//							}
//						}
//						//三方明细
//						Example sfMxExample = new Example(SfMxQq.class);
//						Example.Criteria sfMxCriteria = sfMxExample.createCriteria();
//						sfMxExample.setOrderByClause("pk");
//						sfMxCriteria.andEqualTo("jjdpk",jlt.getJjdpk());
//						sfMxCriteria.andEqualTo("accnumber",jlt.getCardnumber());
//						sfMxCriteria.andEqualTo("nlevel",jlt.getNlevel());
//						List<SfMxQq> sfMxQqList = sfMxQqService.queryListByExample(sfMxExample);
//						if (sfMxQqList!=null && !sfMxQqList.isEmpty() && sfMxQqList.size()>0){
//							SfMxQq sfMxQq = sfMxQqList.get(0);//sfMxQqList.stream().filter(bean->bean.getResflag()==1).findFirst().orElse(null);
//							if (sfMxQq!=null){
//								//查询请求相关结果
//								List<String> appids = sfMxJgLstService.queryApplicationByQqpk(sfMxQq.getPk());
//
//								if (appids!=null && !appids.isEmpty() && appids.size()>0){
//									List<AnalysisQueue> analysisQueueList = new ArrayList<>(appids.size());
//									for (String applicationid : appids){
//										AnalysisQueue analysisQueue = new AnalysisQueue();
//										analysisQueue.setPk(TemplateUtil.genUUID());
//										analysisQueue.setApplicationid(applicationid);
//										analysisQueue.setCardnumber(jlt.getCardnumber());
//										analysisQueue.setJjdpk(jlt.getJjdpk());
//										analysisQueue.setQqpk(sfMxQq.getPk());
//										analysisQueue.setQqtype("S-07");
//										analysisQueueList.add(analysisQueue);
//									}
//									//插入序列
//									analysisQueueService.insert(analysisQueueList);
//									//修改请求表
//									SfMxQq updSfMxQq = new SfMxQq();
//									updSfMxQq.setPk(sfMxQq.getPk());
//									updSfMxQq.setFxbs((short) 1);
//									sfMxQqService.updateByPrimaryKey(updSfMxQq);
//									flag = true;
//								}
//							}
//						}
//						if (flag){
//							Jlt updJlt = new Jlt();
//							updJlt.setPk(jlt.getPk());
//							updJlt.setKeepon((short) 1);
//							this.updateByPrimaryKey(updJlt);
//							result++;
//						}
//						break;
//					case 3://流水请求
//						//分析流水号请求
//						Example example = new Example(SfLsQq.class);
//						Example.Criteria criteria = example.createCriteria();
//						example.setOrderByClause("resdate asc,pk asc");
//						criteria.andEqualTo("jjdpk",jlt.getJjdpk());
//						criteria.andEqualTo("accnumber",jlt.getCardnumber());
//						criteria.andEqualTo("nlevel",jlt.getNlevel());
//
//						List<SfLsQq> list = sfLsQqService.queryListByExample(example);
//						if (list!=null && !list.isEmpty() && list.size()>0){
//							//已反馈、已分析的，修改为可再次分析
//							SfLsQq qq = list.stream().filter(bean->bean.getResflag()==1).findFirst().orElse(null);
//							if (qq!=null){
//								//修改jlt.keepon=1
//								//修改lsQq.fxbs=1
//								Jlt updJlt = new Jlt();
//								updJlt.setPk(jlt.getPk());
//								updJlt.setKeepon((short) 1);
//								this.updateByPrimaryKey(updJlt);
//
//								SfLsQq updQq = new SfLsQq();
//								updQq.setPk(qq.getPk());
//								updQq.setFxbs((short) 1);
//								sfLsQqService.updateByPrimaryKey(updQq);
//								result++;
//							}
//						}
//						break;
//					default:
//						break;
//				}
//			}
//		}else{
//			logger.error("JltServiceImpl.keepOnAnalysis时jlts为空。");
//			throw new AppRuntimeException("JltServiceImpl.keepOnAnalysis时jlts为空。");
//		}
		return result;
	}

	@Override
	public List<Jlt> queryXskTree(String pk) throws Exception {
		if (pk!=null&&!"".equals(pk)){
			return jltMapper.queryXskTree(pk);
		}else{
			logger.error("JltServiceImpl.queryXskTree时pk数据为空。");
			throw new AppRuntimeException("JltServiceImpl.queryXskTree时pk数据为空。");
		}
	}

	@Override
	public List<Jlt> queryTreeFirstLevel(String pk) throws Exception {
		if (pk!=null&&!"".equals(pk)){
			return jltMapper.queryTreeFirstLevel(pk);
		}else{
			logger.error("JltServiceImpl.queryTreeFirstLevel时pk数据为空。");
			throw new AppRuntimeException("JltServiceImpl.queryTreeFirstLevel时pk数据为空。");
		}
	}

	@Override
	public List<Jlt> queryXskTreeCToP(String pk) throws Exception{
		if (pk!=null&&!"".equals(pk)){
			return jltMapper.queryXskTreeCToP(pk);
		}else{
			logger.error("JltServiceImpl.queryAsObject时jlt数据为空。");
			throw new AppRuntimeException("JltServiceImpl.queryAsObject时jlt数据为空。");
		}
	}

	@Override
	public List<Jlt> queryXskTreeAll(Jlt jlt) throws Exception{
		if (jlt != null){
			return jltMapper.queryXskTreeAll(jlt);
		}else{
			logger.error("JltServiceImpl.queryAsObject时jlt数据为空。");
			throw new AppRuntimeException("JltServiceImpl.queryAsObject时jlt数据为空。");
		}
	}

	@Override
	public List<Map<String, Object>> querySfMxJgMap(Map<String, Object> map) throws Exception{
    	return jltMapper.querySfMxJgMap(map);
	}

	@Override
	public List<Map<String, Object>> queryYhkMxJgMap(Map<String, Object> map) throws Exception{
    	return jltMapper.queryYhkMxJgMap(map);
	}

}
