package com.unis.service.znzf;

import com.github.pagehelper.PageInfo;
import com.unis.dto.ResultDto;
import com.unis.model.znzf.Jlt;
import com.unis.service.BaseService;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * <pre>
 * TODO
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-03-13
 */
public interface JltService extends BaseService {

    /**
     * 新增Jlt实例
     * 
     * @param jlt
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int insert(Jlt jlt) throws Exception;

    /**
     * 删除Jlt实例
     * 
     * @param pk
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int delete(String pk) throws Exception;

    /**
     * 更新Jlt实例
     * 
     * @param jlt
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int updateByPrimaryKey(Jlt jlt) throws Exception;
    /**
     * 根据pk主键获取对象
     * 
     * @param pk
     * @throws Exception
     */
    Jlt queryJltByPrimaryKey(String pk) throws Exception;

    /**
     * 查询Jlt实例
     * 
     * @param jlt
     * @throws Exception
     */
    Jlt queryAsObject(Jlt jlt) throws Exception;
    
    	
	/**
     * 根据条件获取条数
     * 
     * @param example
     * @throws Exception
     */
	int queryCountByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合
     * 
     * @param example
     * @throws Exception
     */
	List<Jlt> queryListByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合(分页),需在example 设定排序
     * 
     * @param example
     * @param pageNum
     * @param pageSize
     * @throws Exception
     */
	PageInfo queryPageInfoByExample(Example example, int pageNum, int pageSize) throws Exception;

	/**
     * 根据入参获取数据集合(分页)
     * @param parmMap
     * @param pageNum
     * @param pageSize
     * @return
     * @throws Exception
     */
    ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception;
    /**
     * 根据入参获取数据集合(分页)
     * @param parmMap
     * @param pageNum
     * @param pageSize
     * @return
     * @throws Exception
     */
    ResultDto queryListByJjdpkAndCbyj(Map parmMap, int pageNum, int pageSize) throws  Exception;
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    String launchQq(String pk,String qqType) throws Exception;
    int getJltLevel(String jjdPk,int iszcygzs) throws Exception;
    int checkJltAndInsert(Map map) throws Exception;

    /**
     * 更新disab
     *
     * @param pk
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int updateDisabByPrimaryKey(String pk) throws Exception;
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int updateNlevel(String pk,String cardnumber,String parentzh,int nlevel) throws Exception;

    List<Map> queryJltTreeByJjd(String jjdpk) throws Exception;

    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int keepOnAnalysis(Jlt[] jlts) throws Exception;

    List<Jlt> queryXskTree(String pk) throws Exception;
    List<Jlt> queryTreeFirstLevel(String pk) throws Exception;
    List<Jlt> queryXskTreeCToP(String pk) throws Exception;
    List<Jlt> queryXskTreeAll(Jlt jlt) throws Exception;
    List<Map<String, Object>> querySfMxJgMap(Map<String, Object> map) throws Exception;
    List<Map<String, Object>> queryYhkMxJgMap(Map<String, Object> map) throws Exception;
}
