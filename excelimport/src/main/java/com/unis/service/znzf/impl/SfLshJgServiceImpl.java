package com.unis.service.znzf.impl;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.SfLshJgMapper;
import com.unis.model.znzf.SfLshJg;
import com.unis.service.znzf.SfLshJgService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see SfLshJgService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-03-11
 */
@Service("sfLshJgService")
public class SfLshJgServiceImpl extends BaseServiceImpl implements SfLshJgService {
	private static final Logger logger = LoggerFactory.getLogger(SfLshJgServiceImpl.class);
    @Autowired
    private SfLshJgMapper sfLshJgMapper;

    /**
     * @see SfLshJgService#insert(SfLshJg sfLshJg)
     */
    @Override
    public int insert(SfLshJg sfLshJg) throws Exception {
    	if (sfLshJg!=null){
	        //sfLshJg.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return sfLshJgMapper.insertSelective(sfLshJg);
    	}else{
    		logger.error("SfLshJgServiceImpl.insert时sfLshJg数据为空。");
    		throw new AppRuntimeException("SfLshJgServiceImpl.insert时sfLshJg数据为空。");
    	}        
    }

    /**
     * @see SfLshJgService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("SfLshJgServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("SfLshJgServiceImpl.delete时pk为空。");
    	}else{
    		return sfLshJgMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see SfLshJgService#updateByPrimaryKey(SfLshJg sfLshJg)
     */
    @Override
    public int updateByPrimaryKey(SfLshJg sfLshJg) throws Exception {
        if (sfLshJg!=null){
        	if(StringUtils.isBlank(sfLshJg.getPk())){
        		logger.error("SfLshJgServiceImpl.updateByPrimaryKey时sfLshJg.Pk为空。");
        		throw new AppRuntimeException("SfLshJgServiceImpl.updateByPrimaryKey时sfLshJg.Pk为空。");
        	}
	        return sfLshJgMapper.updateByPrimaryKeySelective(sfLshJg);
    	}else{
    		logger.error("SfLshJgServiceImpl.updateByPrimaryKey时sfLshJg数据为空。");
    		throw new AppRuntimeException("SfLshJgServiceImpl.updateByPrimaryKey时sfLshJg数据为空。");
    	}
    }
    /**
     * @see SfLshJgService#querySfLshJgByPrimaryKey(String pk)
     */
    @Override
    public SfLshJg querySfLshJgByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("SfLshJgServiceImpl.querySfLshJgByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("SfLshJgServiceImpl.querySfLshJgByPrimaryKey时pk为空。");
    	}else{
    		return sfLshJgMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see SfLshJgService#queryAsObject(SfLshJg sfLshJg)
     */
    @Override
    public SfLshJg queryAsObject(SfLshJg sfLshJg) throws Exception {
        if (sfLshJg!=null){
	        return sfLshJgMapper.selectOne(sfLshJg);
    	}else{
    		logger.error("SfLshJgServiceImpl.queryAsObject时sfLshJg数据为空。");
    		throw new AppRuntimeException("SfLshJgServiceImpl.queryAsObject时sfLshJg数据为空。");
    	}
    }
    
    /**
     * @see SfLshJgService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return sfLshJgMapper.selectCountByExample(example);
    	}else{
    		logger.error("SfLshJgServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("SfLshJgServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see SfLshJgService#queryListByExample(Example example)
     */
    @Override
    public List<SfLshJg> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return sfLshJgMapper.selectByExample(example);
    	}else{
    		logger.error("SfLshJgServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("SfLshJgServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see SfLshJgService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(sfLshJgMapper.selectByExample(example));
    	}else{
    		logger.error("SfLshJgServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("SfLshJgServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see SfLshJgService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(SfLshJg.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qqpk")!=null && StringUtils.isNotBlank(parmMap.get("qqpk").toString())){
				criteria.andEqualTo("qqpk",parmMap.get("qqpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jjdpk")!=null && StringUtils.isNotBlank(parmMap.get("jjdpk").toString())){
				criteria.andEqualTo("jjdpk",parmMap.get("jjdpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("applicationid")!=null && StringUtils.isNotBlank(parmMap.get("applicationid").toString())){
				criteria.andEqualTo("applicationid",parmMap.get("applicationid").toString().trim());
				flag = true;
			}
    		if(parmMap.get("resultcode")!=null && StringUtils.isNotBlank(parmMap.get("resultcode").toString())){
				criteria.andEqualTo("resultcode",parmMap.get("resultcode").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zfjglx")!=null && StringUtils.isNotBlank(parmMap.get("zfjglx").toString())){
				criteria.andEqualTo("zfjglx",parmMap.get("zfjglx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jylx")!=null && StringUtils.isNotBlank(parmMap.get("jylx").toString())){
				criteria.andEqualTo("jylx",parmMap.get("jylx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zflx")!=null && StringUtils.isNotBlank(parmMap.get("zflx").toString())){
				criteria.andEqualTo("zflx",parmMap.get("zflx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rmbzl")!=null && StringUtils.isNotBlank(parmMap.get("rmbzl").toString())){
				criteria.andEqualTo("rmbzl",parmMap.get("rmbzl").toString().trim());
				flag = true;
			}
    		if(parmMap.get("je")!=null && StringUtils.isNotBlank(parmMap.get("je").toString())){
				criteria.andEqualTo("je",parmMap.get("je").toString().trim());
				flag = true;
			}
    		if(parmMap.get("skfyhjgdm")!=null && StringUtils.isNotBlank(parmMap.get("skfyhjgdm").toString())){
				criteria.andEqualTo("skfyhjgdm",parmMap.get("skfyhjgdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("skfyhjgmc")!=null && StringUtils.isNotBlank(parmMap.get("skfyhjgmc").toString())){
				criteria.andEqualTo("skfyhjgmc",parmMap.get("skfyhjgmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("skfzh")!=null && StringUtils.isNotBlank(parmMap.get("skfzh").toString())){
				criteria.andEqualTo("skfzh",parmMap.get("skfzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("skfzfzh")!=null && StringUtils.isNotBlank(parmMap.get("skfzfzh").toString())){
				criteria.andEqualTo("skfzfzh",parmMap.get("skfzfzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("posbh")!=null && StringUtils.isNotBlank(parmMap.get("posbh").toString())){
				criteria.andEqualTo("posbh",parmMap.get("posbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("skfshhm")!=null && StringUtils.isNotBlank(parmMap.get("skfshhm").toString())){
				criteria.andEqualTo("skfshhm",parmMap.get("skfshhm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fkfyhjgdm")!=null && StringUtils.isNotBlank(parmMap.get("fkfyhjgdm").toString())){
				criteria.andEqualTo("fkfyhjgdm",parmMap.get("fkfyhjgdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fkfyhjgmc")!=null && StringUtils.isNotBlank(parmMap.get("fkfyhjgmc").toString())){
				criteria.andEqualTo("fkfyhjgmc",parmMap.get("fkfyhjgmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fkfzh")!=null && StringUtils.isNotBlank(parmMap.get("fkfzh").toString())){
				criteria.andEqualTo("fkfzh",parmMap.get("fkfzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fkfzfzh")!=null && StringUtils.isNotBlank(parmMap.get("fkfzfzh").toString())){
				criteria.andEqualTo("fkfzfzh",parmMap.get("fkfzfzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jysblx")!=null && StringUtils.isNotBlank(parmMap.get("jysblx").toString())){
				criteria.andEqualTo("jysblx",parmMap.get("jysblx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sbip")!=null && StringUtils.isNotBlank(parmMap.get("sbip").toString())){
				criteria.andEqualTo("sbip",parmMap.get("sbip").toString().trim());
				flag = true;
			}
    		if(parmMap.get("macdz")!=null && StringUtils.isNotBlank(parmMap.get("macdz").toString())){
				criteria.andEqualTo("macdz",parmMap.get("macdz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sbhm")!=null && StringUtils.isNotBlank(parmMap.get("sbhm").toString())){
				criteria.andEqualTo("sbhm",parmMap.get("sbhm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jyddjd")!=null && StringUtils.isNotBlank(parmMap.get("jyddjd").toString())){
				criteria.andEqualTo("jyddjd",parmMap.get("jyddjd").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jyddwd")!=null && StringUtils.isNotBlank(parmMap.get("jyddwd").toString())){
				criteria.andEqualTo("jyddwd",parmMap.get("jyddwd").toString().trim());
				flag = true;
			}
    		if(parmMap.get("wbqdlsh")!=null && StringUtils.isNotBlank(parmMap.get("wbqdlsh").toString())){
				criteria.andEqualTo("wbqdlsh",parmMap.get("wbqdlsh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fksm")!=null && StringUtils.isNotBlank(parmMap.get("fksm").toString())){
				criteria.andEqualTo("fksm",parmMap.get("fksm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fkjgmc")!=null && StringUtils.isNotBlank(parmMap.get("fkjgmc").toString())){
				criteria.andEqualTo("fkjgmc",parmMap.get("fkjgmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jbrxm")!=null && StringUtils.isNotBlank(parmMap.get("jbrxm").toString())){
				criteria.andEqualTo("jbrxm",parmMap.get("jbrxm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jbrdh")!=null && StringUtils.isNotBlank(parmMap.get("jbrdh").toString())){
				criteria.andEqualTo("jbrdh",parmMap.get("jbrdh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bz")!=null && StringUtils.isNotBlank(parmMap.get("bz").toString())){
				criteria.andEqualTo("bz",parmMap.get("bz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fksj")!=null && StringUtils.isNotBlank(parmMap.get("fksj").toString())){
				criteria.andEqualTo("fksj",parmMap.get("fksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("SfLshJgServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("SfLshJgServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
