package com.unis.service.znzf.impl;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.YhkMxJgMapper;
import com.unis.model.znzf.YhkMxJg;
import com.unis.service.znzf.YhkMxJgService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see YhkMxJgService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-02-27
 */
@Service("yhkMxJgService")
public class YhkMxJgServiceImpl extends BaseServiceImpl implements YhkMxJgService {
	private static final Logger logger = LoggerFactory.getLogger(YhkMxJgServiceImpl.class);
    @Autowired
    private YhkMxJgMapper yhkMxJgMapper;

    /**
     * @see YhkMxJgService#insert(YhkMxJg yhkMxJg)
     */
    @Override
    public int insert(YhkMxJg yhkMxJg) throws Exception {
    	if (yhkMxJg!=null){
	        //yhkMxJg.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return yhkMxJgMapper.insertSelective(yhkMxJg);
    	}else{
    		logger.error("YhkMxJgServiceImpl.insert时yhkMxJg数据为空。");
    		throw new AppRuntimeException("YhkMxJgServiceImpl.insert时yhkMxJg数据为空。");
    	}        
    }

    /**
     * @see YhkMxJgService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("YhkMxJgServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("YhkMxJgServiceImpl.delete时pk为空。");
    	}else{
    		return yhkMxJgMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see YhkMxJgService#updateByPrimaryKey(YhkMxJg yhkMxJg)
     */
    @Override
    public int updateByPrimaryKey(YhkMxJg yhkMxJg) throws Exception {
        if (yhkMxJg!=null){
        	if(StringUtils.isBlank(yhkMxJg.getPk())){
        		logger.error("YhkMxJgServiceImpl.updateByPrimaryKey时yhkMxJg.Pk为空。");
        		throw new AppRuntimeException("YhkMxJgServiceImpl.updateByPrimaryKey时yhkMxJg.Pk为空。");
        	}
	        return yhkMxJgMapper.updateByPrimaryKeySelective(yhkMxJg);
    	}else{
    		logger.error("YhkMxJgServiceImpl.updateByPrimaryKey时yhkMxJg数据为空。");
    		throw new AppRuntimeException("YhkMxJgServiceImpl.updateByPrimaryKey时yhkMxJg数据为空。");
    	}
    }
    /**
     * @see YhkMxJgService#queryYhkMxJgByPrimaryKey(String pk)
     */
    @Override
    public YhkMxJg queryYhkMxJgByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("YhkMxJgServiceImpl.queryYhkMxJgByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("YhkMxJgServiceImpl.queryYhkMxJgByPrimaryKey时pk为空。");
    	}else{
    		return yhkMxJgMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see YhkMxJgService#queryAsObject(YhkMxJg yhkMxJg)
     */
    @Override
    public YhkMxJg queryAsObject(YhkMxJg yhkMxJg) throws Exception {
        if (yhkMxJg!=null){
	        return yhkMxJgMapper.selectOne(yhkMxJg);
    	}else{
    		logger.error("YhkMxJgServiceImpl.queryAsObject时yhkMxJg数据为空。");
    		throw new AppRuntimeException("YhkMxJgServiceImpl.queryAsObject时yhkMxJg数据为空。");
    	}
    }
    
    /**
     * @see YhkMxJgService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return yhkMxJgMapper.selectCountByExample(example);
    	}else{
    		logger.error("YhkMxJgServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("YhkMxJgServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see YhkMxJgService#queryListByExample(Example example)
     */
    @Override
    public List<YhkMxJg> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return yhkMxJgMapper.selectByExample(example);
    	}else{
    		logger.error("YhkMxJgServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("YhkMxJgServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see YhkMxJgService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(yhkMxJgMapper.selectByExample(example));
    	}else{
    		logger.error("YhkMxJgServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("YhkMxJgServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see YhkMxJgService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(YhkMxJg.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qqpk")!=null && StringUtils.isNotBlank(parmMap.get("qqpk").toString())){
				criteria.andEqualTo("qqpk",parmMap.get("qqpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jjdpk")!=null && StringUtils.isNotBlank(parmMap.get("jjdpk").toString())){
				criteria.andEqualTo("jjdpk",parmMap.get("jjdpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("applicationid")!=null && StringUtils.isNotBlank(parmMap.get("applicationid").toString())){
				criteria.andEqualTo("applicationid",parmMap.get("applicationid").toString().trim());
				flag = true;
			}
    		if(parmMap.get("resultcode")!=null && StringUtils.isNotBlank(parmMap.get("resultcode").toString())){
				criteria.andEqualTo("resultcode",parmMap.get("resultcode").toString().trim());
				flag = true;
			}
    		if(parmMap.get("operatorname")!=null && StringUtils.isNotBlank(parmMap.get("operatorname").toString())){
				criteria.andEqualTo("operatorname",parmMap.get("operatorname").toString().trim());
				flag = true;
			}
    		if(parmMap.get("operatorphone")!=null && StringUtils.isNotBlank(parmMap.get("operatorphone").toString())){
				criteria.andEqualTo("operatorphone",parmMap.get("operatorphone").toString().trim());
				flag = true;
			}
    		if(parmMap.get("feedbackorgname")!=null && StringUtils.isNotBlank(parmMap.get("feedbackorgname").toString())){
				criteria.andEqualTo("feedbackorgname",parmMap.get("feedbackorgname").toString().trim());
				flag = true;
			}
    		if(parmMap.get("feedbackremark")!=null && StringUtils.isNotBlank(parmMap.get("feedbackremark").toString())){
				criteria.andEqualTo("feedbackremark",parmMap.get("feedbackremark").toString().trim());
				flag = true;
			}
    		if(parmMap.get("accountname")!=null && StringUtils.isNotBlank(parmMap.get("accountname").toString())){
				criteria.andEqualTo("accountname",parmMap.get("accountname").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cardnumber")!=null && StringUtils.isNotBlank(parmMap.get("cardnumber").toString())){
				criteria.andEqualTo("cardnumber",parmMap.get("cardnumber").toString().trim());
				flag = true;
			}
    		if(parmMap.get("depbankbranch")!=null && StringUtils.isNotBlank(parmMap.get("depbankbranch").toString())){
				criteria.andEqualTo("depbankbranch",parmMap.get("depbankbranch").toString().trim());
				flag = true;
			}
    		if(parmMap.get("depbankbranchcode")!=null && StringUtils.isNotBlank(parmMap.get("depbankbranchcode").toString())){
				criteria.andEqualTo("depbankbranchcode",parmMap.get("depbankbranchcode").toString().trim());
				flag = true;
			}
    		if(parmMap.get("accopentime")!=null && StringUtils.isNotBlank(parmMap.get("accopentime").toString())){
				criteria.andEqualTo("accopentime",parmMap.get("accopentime").toString().trim());
				flag = true;
			}
    		if(parmMap.get("acccantntime")!=null && StringUtils.isNotBlank(parmMap.get("acccantntime").toString())){
				criteria.andEqualTo("acccantntime",parmMap.get("acccantntime").toString().trim());
				flag = true;
			}
    		if(parmMap.get("acccantnbranch")!=null && StringUtils.isNotBlank(parmMap.get("acccantnbranch").toString())){
				criteria.andEqualTo("acccantnbranch",parmMap.get("acccantnbranch").toString().trim());
				flag = true;
			}
    		if(parmMap.get("remark")!=null && StringUtils.isNotBlank(parmMap.get("remark").toString())){
				criteria.andEqualTo("remark",parmMap.get("remark").toString().trim());
				flag = true;
			}
    		if(parmMap.get("accnumber")!=null && StringUtils.isNotBlank(parmMap.get("accnumber").toString())){
				criteria.andEqualTo("accnumber",parmMap.get("accnumber").toString().trim());
				flag = true;
			}
    		if(parmMap.get("accserial")!=null && StringUtils.isNotBlank(parmMap.get("accserial").toString())){
				criteria.andEqualTo("accserial",parmMap.get("accserial").toString().trim());
				flag = true;
			}
    		if(parmMap.get("acctype")!=null && StringUtils.isNotBlank(parmMap.get("acctype").toString())){
				criteria.andEqualTo("acctype",parmMap.get("acctype").toString().trim());
				flag = true;
			}
    		if(parmMap.get("accstatus")!=null && StringUtils.isNotBlank(parmMap.get("accstatus").toString())){
				criteria.andEqualTo("accstatus",parmMap.get("accstatus").toString().trim());
				flag = true;
			}
    		if(parmMap.get("accbalance")!=null && StringUtils.isNotBlank(parmMap.get("accbalance").toString())){
				criteria.andEqualTo("accbalance",parmMap.get("accbalance").toString().trim());
				flag = true;
			}
    		if(parmMap.get("availablebalance")!=null && StringUtils.isNotBlank(parmMap.get("availablebalance").toString())){
				criteria.andEqualTo("availablebalance",parmMap.get("availablebalance").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lasttransactiontime")!=null && StringUtils.isNotBlank(parmMap.get("lasttransactiontime").toString())){
				criteria.andEqualTo("lasttransactiontime",parmMap.get("lasttransactiontime").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fksj")!=null && StringUtils.isNotBlank(parmMap.get("fksj").toString())){
				criteria.andEqualTo("fksj",parmMap.get("fksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("currency")!=null && StringUtils.isNotBlank(parmMap.get("currency").toString())){
				criteria.andEqualTo("currency",parmMap.get("currency").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cashremit")!=null && StringUtils.isNotBlank(parmMap.get("cashremit").toString())){
				criteria.andEqualTo("cashremit",parmMap.get("cashremit").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("YhkMxJgServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("YhkMxJgServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
