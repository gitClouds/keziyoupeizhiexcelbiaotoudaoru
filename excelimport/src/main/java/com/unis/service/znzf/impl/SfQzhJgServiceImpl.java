package com.unis.service.znzf.impl;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.SfQzhJgMapper;
import com.unis.model.znzf.SfQzhJg;
import com.unis.service.znzf.SfQzhJgService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see SfQzhJgService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-03-11
 */
@Service("sfQzhJgService")
public class SfQzhJgServiceImpl extends BaseServiceImpl implements SfQzhJgService {
	private static final Logger logger = LoggerFactory.getLogger(SfQzhJgServiceImpl.class);
    @Autowired
    private SfQzhJgMapper sfQzhJgMapper;

    /**
     * @see SfQzhJgService#insert(SfQzhJg sfQzhJg)
     */
    @Override
    public int insert(SfQzhJg sfQzhJg) throws Exception {
    	if (sfQzhJg!=null){
	        //sfQzhJg.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return sfQzhJgMapper.insertSelective(sfQzhJg);
    	}else{
    		logger.error("SfQzhJgServiceImpl.insert时sfQzhJg数据为空。");
    		throw new AppRuntimeException("SfQzhJgServiceImpl.insert时sfQzhJg数据为空。");
    	}        
    }

    /**
     * @see SfQzhJgService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("SfQzhJgServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("SfQzhJgServiceImpl.delete时pk为空。");
    	}else{
    		return sfQzhJgMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see SfQzhJgService#updateByPrimaryKey(SfQzhJg sfQzhJg)
     */
    @Override
    public int updateByPrimaryKey(SfQzhJg sfQzhJg) throws Exception {
        if (sfQzhJg!=null){
        	if(StringUtils.isBlank(sfQzhJg.getPk())){
        		logger.error("SfQzhJgServiceImpl.updateByPrimaryKey时sfQzhJg.Pk为空。");
        		throw new AppRuntimeException("SfQzhJgServiceImpl.updateByPrimaryKey时sfQzhJg.Pk为空。");
        	}
	        return sfQzhJgMapper.updateByPrimaryKeySelective(sfQzhJg);
    	}else{
    		logger.error("SfQzhJgServiceImpl.updateByPrimaryKey时sfQzhJg数据为空。");
    		throw new AppRuntimeException("SfQzhJgServiceImpl.updateByPrimaryKey时sfQzhJg数据为空。");
    	}
    }
    /**
     * @see SfQzhJgService#querySfQzhJgByPrimaryKey(String pk)
     */
    @Override
    public SfQzhJg querySfQzhJgByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("SfQzhJgServiceImpl.querySfQzhJgByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("SfQzhJgServiceImpl.querySfQzhJgByPrimaryKey时pk为空。");
    	}else{
    		return sfQzhJgMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see SfQzhJgService#queryAsObject(SfQzhJg sfQzhJg)
     */
    @Override
    public SfQzhJg queryAsObject(SfQzhJg sfQzhJg) throws Exception {
        if (sfQzhJg!=null){
	        return sfQzhJgMapper.selectOne(sfQzhJg);
    	}else{
    		logger.error("SfQzhJgServiceImpl.queryAsObject时sfQzhJg数据为空。");
    		throw new AppRuntimeException("SfQzhJgServiceImpl.queryAsObject时sfQzhJg数据为空。");
    	}
    }
    
    /**
     * @see SfQzhJgService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return sfQzhJgMapper.selectCountByExample(example);
    	}else{
    		logger.error("SfQzhJgServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("SfQzhJgServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see SfQzhJgService#queryListByExample(Example example)
     */
    @Override
    public List<SfQzhJg> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return sfQzhJgMapper.selectByExample(example);
    	}else{
    		logger.error("SfQzhJgServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("SfQzhJgServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see SfQzhJgService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(sfQzhJgMapper.selectByExample(example));
    	}else{
    		logger.error("SfQzhJgServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("SfQzhJgServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see SfQzhJgService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(SfQzhJg.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qqpk")!=null && StringUtils.isNotBlank(parmMap.get("qqpk").toString())){
				criteria.andEqualTo("qqpk",parmMap.get("qqpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jjdpk")!=null && StringUtils.isNotBlank(parmMap.get("jjdpk").toString())){
				criteria.andEqualTo("jjdpk",parmMap.get("jjdpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("applicationid")!=null && StringUtils.isNotBlank(parmMap.get("applicationid").toString())){
				criteria.andEqualTo("applicationid",parmMap.get("applicationid").toString().trim());
				flag = true;
			}
    		if(parmMap.get("resultcode")!=null && StringUtils.isNotBlank(parmMap.get("resultcode").toString())){
				criteria.andEqualTo("resultcode",parmMap.get("resultcode").toString().trim());
				flag = true;
			}
    		if(parmMap.get("feedbackremark")!=null && StringUtils.isNotBlank(parmMap.get("feedbackremark").toString())){
				criteria.andEqualTo("feedbackremark",parmMap.get("feedbackremark").toString().trim());
				flag = true;
			}
    		if(parmMap.get("feedbackorgname")!=null && StringUtils.isNotBlank(parmMap.get("feedbackorgname").toString())){
				criteria.andEqualTo("feedbackorgname",parmMap.get("feedbackorgname").toString().trim());
				flag = true;
			}
    		if(parmMap.get("operatorname")!=null && StringUtils.isNotBlank(parmMap.get("operatorname").toString())){
				criteria.andEqualTo("operatorname",parmMap.get("operatorname").toString().trim());
				flag = true;
			}
    		if(parmMap.get("operatorphonenumber")!=null && StringUtils.isNotBlank(parmMap.get("operatorphonenumber").toString())){
				criteria.andEqualTo("operatorphonenumber",parmMap.get("operatorphonenumber").toString().trim());
				flag = true;
			}
    		if(parmMap.get("remark")!=null && StringUtils.isNotBlank(parmMap.get("remark").toString())){
				criteria.andEqualTo("remark",parmMap.get("remark").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fksj")!=null && StringUtils.isNotBlank(parmMap.get("fksj").toString())){
				criteria.andEqualTo("fksj",parmMap.get("fksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("SfQzhJgServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("SfQzhJgServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
