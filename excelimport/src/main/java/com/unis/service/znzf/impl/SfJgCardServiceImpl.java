package com.unis.service.znzf.impl;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.SfJgCardMapper;
import com.unis.model.znzf.SfJgCard;
import com.unis.service.znzf.SfJgCardService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see SfJgCardService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-03-11
 */
@Service("sfJgCardService")
public class SfJgCardServiceImpl extends BaseServiceImpl implements SfJgCardService {
	private static final Logger logger = LoggerFactory.getLogger(SfJgCardServiceImpl.class);
    @Autowired
    private SfJgCardMapper sfJgCardMapper;

    /**
     * @see SfJgCardService#insert(SfJgCard sfJgCard)
     */
    @Override
    public int insert(SfJgCard sfJgCard) throws Exception {
    	if (sfJgCard!=null){
	        //sfJgCard.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return sfJgCardMapper.insertSelective(sfJgCard);
    	}else{
    		logger.error("SfJgCardServiceImpl.insert时sfJgCard数据为空。");
    		throw new AppRuntimeException("SfJgCardServiceImpl.insert时sfJgCard数据为空。");
    	}        
    }

    /**
     * @see SfJgCardService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("SfJgCardServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("SfJgCardServiceImpl.delete时pk为空。");
    	}else{
    		return sfJgCardMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see SfJgCardService#updateByPrimaryKey(SfJgCard sfJgCard)
     */
    @Override
    public int updateByPrimaryKey(SfJgCard sfJgCard) throws Exception {
        if (sfJgCard!=null){
        	if(StringUtils.isBlank(sfJgCard.getPk())){
        		logger.error("SfJgCardServiceImpl.updateByPrimaryKey时sfJgCard.Pk为空。");
        		throw new AppRuntimeException("SfJgCardServiceImpl.updateByPrimaryKey时sfJgCard.Pk为空。");
        	}
	        return sfJgCardMapper.updateByPrimaryKeySelective(sfJgCard);
    	}else{
    		logger.error("SfJgCardServiceImpl.updateByPrimaryKey时sfJgCard数据为空。");
    		throw new AppRuntimeException("SfJgCardServiceImpl.updateByPrimaryKey时sfJgCard数据为空。");
    	}
    }
    /**
     * @see SfJgCardService#querySfJgCardByPrimaryKey(String pk)
     */
    @Override
    public SfJgCard querySfJgCardByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("SfJgCardServiceImpl.querySfJgCardByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("SfJgCardServiceImpl.querySfJgCardByPrimaryKey时pk为空。");
    	}else{
    		return sfJgCardMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see SfJgCardService#queryAsObject(SfJgCard sfJgCard)
     */
    @Override
    public SfJgCard queryAsObject(SfJgCard sfJgCard) throws Exception {
        if (sfJgCard!=null){
	        return sfJgCardMapper.selectOne(sfJgCard);
    	}else{
    		logger.error("SfJgCardServiceImpl.queryAsObject时sfJgCard数据为空。");
    		throw new AppRuntimeException("SfJgCardServiceImpl.queryAsObject时sfJgCard数据为空。");
    	}
    }
    
    /**
     * @see SfJgCardService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return sfJgCardMapper.selectCountByExample(example);
    	}else{
    		logger.error("SfJgCardServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("SfJgCardServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see SfJgCardService#queryListByExample(Example example)
     */
    @Override
    public List<SfJgCard> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return sfJgCardMapper.selectByExample(example);
    	}else{
    		logger.error("SfJgCardServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("SfJgCardServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see SfJgCardService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(sfJgCardMapper.selectByExample(example));
    	}else{
    		logger.error("SfJgCardServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("SfJgCardServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see SfJgCardService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(SfJgCard.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("applicationid")!=null && StringUtils.isNotBlank(parmMap.get("applicationid").toString())){
				criteria.andEqualTo("applicationid",parmMap.get("applicationid").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yhkjgdm")!=null && StringUtils.isNotBlank(parmMap.get("yhkjgdm").toString())){
				criteria.andEqualTo("yhkjgdm",parmMap.get("yhkjgdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yhkjgmc")!=null && StringUtils.isNotBlank(parmMap.get("yhkjgmc").toString())){
				criteria.andEqualTo("yhkjgmc",parmMap.get("yhkjgmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yhkhm")!=null && StringUtils.isNotBlank(parmMap.get("yhkhm").toString())){
				criteria.andEqualTo("yhkhm",parmMap.get("yhkhm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yhklx")!=null && StringUtils.isNotBlank(parmMap.get("yhklx").toString())){
				criteria.andEqualTo("yhklx",parmMap.get("yhklx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yhkrzzt")!=null && StringUtils.isNotBlank(parmMap.get("yhkrzzt").toString())){
				criteria.andEqualTo("yhkrzzt",parmMap.get("yhkrzzt").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxq")!=null && StringUtils.isNotBlank(parmMap.get("yxq").toString())){
				criteria.andEqualTo("yxq",parmMap.get("yxq").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qtxx")!=null && StringUtils.isNotBlank(parmMap.get("qtxx").toString())){
				criteria.andEqualTo("qtxx",parmMap.get("qtxx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
			if(parmMap.get("actpk")!=null && StringUtils.isNotBlank(parmMap.get("actpk").toString())){
				criteria.andEqualTo("actpk",parmMap.get("actpk").toString().trim());
				flag = true;
			}

    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
				return  new ResultDto();
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("SfJgCardServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("SfJgCardServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
