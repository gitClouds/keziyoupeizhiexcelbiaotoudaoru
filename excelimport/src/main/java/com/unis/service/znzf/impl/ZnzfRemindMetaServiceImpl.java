package com.unis.service.znzf.impl;

import java.util.List;
import java.util.Map;

import com.unis.common.util.TemplateUtil;
import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.ZnzfRemindMetaMapper;
import com.unis.model.znzf.ZnzfRemindMeta;
import com.unis.service.znzf.ZnzfRemindMetaService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see ZnzfRemindMetaService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-05-20
 */
@Service("znzfRemindMetaService")
public class ZnzfRemindMetaServiceImpl extends BaseServiceImpl implements ZnzfRemindMetaService {
	private static final Logger logger = LoggerFactory.getLogger(ZnzfRemindMetaServiceImpl.class);
    @Autowired
    private ZnzfRemindMetaMapper znzfRemindMetaMapper;

    /**
     * @see ZnzfRemindMetaService#insert(ZnzfRemindMeta znzfRemindMeta)
     */
    @Override
    public int insert(ZnzfRemindMeta znzfRemindMeta) throws Exception {
    	if (znzfRemindMeta!=null){
	        znzfRemindMeta.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	        return znzfRemindMetaMapper.insertSelective(znzfRemindMeta);
    	}else{
    		logger.error("ZnzfRemindMetaServiceImpl.insert时znzfRemindMeta数据为空。");
    		throw new AppRuntimeException("ZnzfRemindMetaServiceImpl.insert时znzfRemindMeta数据为空。");
    	}        
    }

    /**
     * @see ZnzfRemindMetaService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("ZnzfRemindMetaServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("ZnzfRemindMetaServiceImpl.delete时pk为空。");
    	}else{
    		return znzfRemindMetaMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see ZnzfRemindMetaService#updateByPrimaryKey(ZnzfRemindMeta znzfRemindMeta)
     */
    @Override
    public int updateByPrimaryKey(ZnzfRemindMeta znzfRemindMeta) throws Exception {
        if (znzfRemindMeta!=null){
        	if(StringUtils.isBlank(znzfRemindMeta.getPk())){
        		logger.error("ZnzfRemindMetaServiceImpl.updateByPrimaryKey时znzfRemindMeta.Pk为空。");
        		throw new AppRuntimeException("ZnzfRemindMetaServiceImpl.updateByPrimaryKey时znzfRemindMeta.Pk为空。");
        	}
	        return znzfRemindMetaMapper.updateByPrimaryKeySelective(znzfRemindMeta);
    	}else{
    		logger.error("ZnzfRemindMetaServiceImpl.updateByPrimaryKey时znzfRemindMeta数据为空。");
    		throw new AppRuntimeException("ZnzfRemindMetaServiceImpl.updateByPrimaryKey时znzfRemindMeta数据为空。");
    	}
    }
    /**
     * @see ZnzfRemindMetaService#queryZnzfRemindMetaByPrimaryKey(String pk)
     */
    @Override
    public ZnzfRemindMeta queryZnzfRemindMetaByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("ZnzfRemindMetaServiceImpl.queryZnzfRemindMetaByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("ZnzfRemindMetaServiceImpl.queryZnzfRemindMetaByPrimaryKey时pk为空。");
    	}else{
    		return znzfRemindMetaMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see ZnzfRemindMetaService#queryAsObject(ZnzfRemindMeta znzfRemindMeta)
     */
    @Override
    public ZnzfRemindMeta queryAsObject(ZnzfRemindMeta znzfRemindMeta) throws Exception {
        if (znzfRemindMeta!=null){
	        return znzfRemindMetaMapper.selectOne(znzfRemindMeta);
    	}else{
    		logger.error("ZnzfRemindMetaServiceImpl.queryAsObject时znzfRemindMeta数据为空。");
    		throw new AppRuntimeException("ZnzfRemindMetaServiceImpl.queryAsObject时znzfRemindMeta数据为空。");
    	}
    }
    
    /**
     * @see ZnzfRemindMetaService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return znzfRemindMetaMapper.selectCountByExample(example);
    	}else{
    		logger.error("ZnzfRemindMetaServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("ZnzfRemindMetaServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see ZnzfRemindMetaService#queryListByExample(Example example)
     */
    @Override
    public List<ZnzfRemindMeta> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return znzfRemindMetaMapper.selectByExample(example);
    	}else{
    		logger.error("ZnzfRemindMetaServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("ZnzfRemindMetaServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see ZnzfRemindMetaService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(znzfRemindMetaMapper.selectByExample(example));
    	}else{
    		logger.error("ZnzfRemindMetaServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("ZnzfRemindMetaServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see ZnzfRemindMetaService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(ZnzfRemindMeta.class);
    		example.setOrderByClause("rksj DESC");//设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bankcode")!=null && StringUtils.isNotBlank(parmMap.get("bankcode").toString())){
				criteria.andEqualTo("bankcode",parmMap.get("bankcode").toString().trim());
				flag = true;
			}
    		if(parmMap.get("remindtype")!=null && StringUtils.isNotBlank(parmMap.get("remindtype").toString())){
				criteria.andEqualTo("remindtype",parmMap.get("remindtype").toString().trim());
				flag = true;
			}
    		if(parmMap.get("remark")!=null && StringUtils.isNotBlank(parmMap.get("remark").toString())){
				criteria.andEqualTo("remark",parmMap.get("remark").toString().trim());
				flag = true;
			}
    		if(parmMap.get("checkfield")!=null && StringUtils.isNotBlank(parmMap.get("checkfield").toString())){
				criteria.andEqualTo("checkfield",parmMap.get("checkfield").toString().trim());
				flag = true;
			}
    		if(parmMap.get("checkdata")!=null && StringUtils.isNotBlank(parmMap.get("checkdata").toString())){
				criteria.andEqualTo("checkdata",parmMap.get("checkdata").toString().trim());
				flag = true;
			}
    		if(parmMap.get("positionval")!=null && StringUtils.isNotBlank(parmMap.get("positionval").toString())){
				criteria.andEqualTo("positionval",parmMap.get("positionval").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("ZnzfRemindMetaServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("ZnzfRemindMetaServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
