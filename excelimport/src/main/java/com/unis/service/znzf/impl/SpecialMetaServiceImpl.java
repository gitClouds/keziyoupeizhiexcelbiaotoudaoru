package com.unis.service.znzf.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.unis.common.constant.Constants;
import com.unis.common.util.TemplateUtil;
import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.SpecialMetaMapper;
import com.unis.model.znzf.SpecialMeta;
import com.unis.service.znzf.SpecialMetaService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see SpecialMetaService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-04-09
 */
@Service("specialMetaService")
public class SpecialMetaServiceImpl extends BaseServiceImpl implements SpecialMetaService {
	private static final Logger logger = LoggerFactory.getLogger(SpecialMetaServiceImpl.class);
    @Autowired
    private SpecialMetaMapper specialMetaMapper;

	@Autowired
	private CacheManager cacheManager;

    /**
     * @see SpecialMetaService#insert(SpecialMeta specialMeta)
     */
    @Override
    public int insert(SpecialMeta specialMeta) throws Exception {
    	if (specialMeta!=null){
	        specialMeta.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return specialMetaMapper.insertSelective(specialMeta);
    	}else{
    		logger.error("SpecialMetaServiceImpl.insert时specialMeta数据为空。");
    		throw new AppRuntimeException("SpecialMetaServiceImpl.insert时specialMeta数据为空。");
    	}        
    }

    /**
     * @see SpecialMetaService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("SpecialMetaServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("SpecialMetaServiceImpl.delete时pk为空。");
    	}else{
    		return specialMetaMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see SpecialMetaService#updateByPrimaryKey(SpecialMeta specialMeta)
     */
    @Override
    public int updateByPrimaryKey(SpecialMeta specialMeta) throws Exception {
        if (specialMeta!=null){
        	if(StringUtils.isBlank(specialMeta.getPk())){
        		logger.error("SpecialMetaServiceImpl.updateByPrimaryKey时specialMeta.Pk为空。");
        		throw new AppRuntimeException("SpecialMetaServiceImpl.updateByPrimaryKey时specialMeta.Pk为空。");
        	}
	        return specialMetaMapper.updateByPrimaryKeySelective(specialMeta);
    	}else{
    		logger.error("SpecialMetaServiceImpl.updateByPrimaryKey时specialMeta数据为空。");
    		throw new AppRuntimeException("SpecialMetaServiceImpl.updateByPrimaryKey时specialMeta数据为空。");
    	}
    }
    /**
     * @see SpecialMetaService#querySpecialMetaByPrimaryKey(String pk)
     */
    @Override
    public SpecialMeta querySpecialMetaByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("SpecialMetaServiceImpl.querySpecialMetaByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("SpecialMetaServiceImpl.querySpecialMetaByPrimaryKey时pk为空。");
    	}else{
    		return specialMetaMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see SpecialMetaService#queryAsObject(SpecialMeta specialMeta)
     */
    @Override
    public SpecialMeta queryAsObject(SpecialMeta specialMeta) throws Exception {
        if (specialMeta!=null){
	        return specialMetaMapper.selectOne(specialMeta);
    	}else{
    		logger.error("SpecialMetaServiceImpl.queryAsObject时specialMeta数据为空。");
    		throw new AppRuntimeException("SpecialMetaServiceImpl.queryAsObject时specialMeta数据为空。");
    	}
    }
    
    /**
     * @see SpecialMetaService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return specialMetaMapper.selectCountByExample(example);
    	}else{
    		logger.error("SpecialMetaServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("SpecialMetaServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see SpecialMetaService#queryListByExample(Example example)
     */
    @Override
    public List<SpecialMeta> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return specialMetaMapper.selectByExample(example);
    	}else{
    		logger.error("SpecialMetaServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("SpecialMetaServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see SpecialMetaService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(specialMetaMapper.selectByExample(example));
    	}else{
    		logger.error("SpecialMetaServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("SpecialMetaServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see SpecialMetaService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(SpecialMeta.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bankcode")!=null && StringUtils.isNotBlank(parmMap.get("bankcode").toString())){
				criteria.andEqualTo("bankcode",parmMap.get("bankcode").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jdbz")!=null && StringUtils.isNotBlank(parmMap.get("jdbz").toString())){
				criteria.andEqualTo("jdbz",parmMap.get("jdbz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("checkfield")!=null && StringUtils.isNotBlank(parmMap.get("checkfield").toString())){
				criteria.andEqualTo("checkfield",parmMap.get("checkfield").toString().trim());
				flag = true;
			}
    		if(parmMap.get("checkvalue")!=null && StringUtils.isNotBlank(parmMap.get("checkvalue").toString())){
				criteria.andEqualTo("checkvalue",parmMap.get("checkvalue").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sertype")!=null && StringUtils.isNotBlank(parmMap.get("sertype").toString())){
				criteria.andEqualTo("sertype",parmMap.get("sertype").toString().trim());
				flag = true;
			}
    		if(parmMap.get("datafield")!=null && StringUtils.isNotBlank(parmMap.get("datafield").toString())){
				criteria.andEqualTo("datafield",parmMap.get("datafield").toString().trim());
				flag = true;
			}
    		if(parmMap.get("positionval")!=null && StringUtils.isNotBlank(parmMap.get("positionval").toString())){
				criteria.andEqualTo("positionval",parmMap.get("positionval").toString().trim());
				flag = true;
			}
    		if(parmMap.get("startpfx")!=null && StringUtils.isNotBlank(parmMap.get("startpfx").toString())){
				criteria.andEqualTo("startpfx",parmMap.get("startpfx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("endpfx")!=null && StringUtils.isNotBlank(parmMap.get("endpfx").toString())){
				criteria.andEqualTo("endpfx",parmMap.get("endpfx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("paycode")!=null && StringUtils.isNotBlank(parmMap.get("paycode").toString())){
				criteria.andEqualTo("paycode",parmMap.get("paycode").toString().trim());
				flag = true;
			}
    		if(parmMap.get("payname")!=null && StringUtils.isNotBlank(parmMap.get("payname").toString())){
				criteria.andEqualTo("payname",parmMap.get("payname").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("SpecialMetaServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("SpecialMetaServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }

	@Override
	public Map<String, List<SpecialMeta>> queryAllSpecialMeta() throws Exception {
		Map<String, List<SpecialMeta>> map = null;

		Cache cache = cacheManager.getCache(Constants.ENCACHE_POOL);
		map = cache.get("all_speciaMeta",Map.class);
		if (map!=null && !map.isEmpty() && map.size()>0){
			return map;
		}else{
			map = new HashMap<>();
			Example example = new Example(SpecialMeta.class);
			Example.Criteria criteria = example.createCriteria();
			criteria.andEqualTo("yxx",1);
			List<SpecialMeta> allList = this.queryListByExample(example);
			if (allList!=null && !allList.isEmpty() && allList.size()>0){
				List<SpecialMeta> bankList = null;
				for (SpecialMeta meta : allList){
					bankList = map.get(meta.getBankcode());
					if (bankList!=null && !bankList.isEmpty() && bankList.size()>0){
						map.get(meta.getBankcode()).add(meta);
					}else{
						bankList = new ArrayList<>();
						bankList.add(meta);
						map.put(meta.getBankcode(),bankList);
					}
				}
			}
			cache.put("all_speciaMeta",map);
		}
		return map;
	}
}
