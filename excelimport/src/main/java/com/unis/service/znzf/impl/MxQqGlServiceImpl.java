package com.unis.service.znzf.impl;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.MxQqGlMapper;
import com.unis.model.znzf.MxQqGl;
import com.unis.service.znzf.MxQqGlService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see MxQqGlService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-04-29
 */
@Service("mxQqGlService")
public class MxQqGlServiceImpl extends BaseServiceImpl implements MxQqGlService {
	private static final Logger logger = LoggerFactory.getLogger(MxQqGlServiceImpl.class);
    @Autowired
    private MxQqGlMapper mxQqGlMapper;

    /**
     * @see MxQqGlService#insert(MxQqGl mxQqGl)
     */
    @Override
    public int insert(MxQqGl mxQqGl) throws Exception {
    	if (mxQqGl!=null){
	        //mxQqGl.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return mxQqGlMapper.insertSelective(mxQqGl);
    	}else{
    		logger.error("MxQqGlServiceImpl.insert时mxQqGl数据为空。");
    		throw new AppRuntimeException("MxQqGlServiceImpl.insert时mxQqGl数据为空。");
    	}        
    }

    /**
     * @see MxQqGlService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("MxQqGlServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("MxQqGlServiceImpl.delete时pk为空。");
    	}else{
    		return mxQqGlMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see MxQqGlService#updateByPrimaryKey(MxQqGl mxQqGl)
     */
    @Override
    public int updateByPrimaryKey(MxQqGl mxQqGl) throws Exception {
        if (mxQqGl!=null){
        	if(StringUtils.isBlank(mxQqGl.getPk())){
        		logger.error("MxQqGlServiceImpl.updateByPrimaryKey时mxQqGl.Pk为空。");
        		throw new AppRuntimeException("MxQqGlServiceImpl.updateByPrimaryKey时mxQqGl.Pk为空。");
        	}
	        return mxQqGlMapper.updateByPrimaryKeySelective(mxQqGl);
    	}else{
    		logger.error("MxQqGlServiceImpl.updateByPrimaryKey时mxQqGl数据为空。");
    		throw new AppRuntimeException("MxQqGlServiceImpl.updateByPrimaryKey时mxQqGl数据为空。");
    	}
    }
    /**
     * @see MxQqGlService#queryMxQqGlByPrimaryKey(String pk)
     */
    @Override
    public MxQqGl queryMxQqGlByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("MxQqGlServiceImpl.queryMxQqGlByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("MxQqGlServiceImpl.queryMxQqGlByPrimaryKey时pk为空。");
    	}else{
    		return mxQqGlMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see MxQqGlService#queryAsObject(MxQqGl mxQqGl)
     */
    @Override
    public MxQqGl queryAsObject(MxQqGl mxQqGl) throws Exception {
        if (mxQqGl!=null){
	        return mxQqGlMapper.selectOne(mxQqGl);
    	}else{
    		logger.error("MxQqGlServiceImpl.queryAsObject时mxQqGl数据为空。");
    		throw new AppRuntimeException("MxQqGlServiceImpl.queryAsObject时mxQqGl数据为空。");
    	}
    }
    
    /**
     * @see MxQqGlService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return mxQqGlMapper.selectCountByExample(example);
    	}else{
    		logger.error("MxQqGlServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("MxQqGlServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see MxQqGlService#queryListByExample(Example example)
     */
    @Override
    public List<MxQqGl> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return mxQqGlMapper.selectByExample(example);
    	}else{
    		logger.error("MxQqGlServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("MxQqGlServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see MxQqGlService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(mxQqGlMapper.selectByExample(example));
    	}else{
    		logger.error("MxQqGlServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("MxQqGlServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see MxQqGlService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(MxQqGl.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sertype")!=null && StringUtils.isNotBlank(parmMap.get("sertype").toString())){
				criteria.andEqualTo("sertype",parmMap.get("sertype").toString().trim());
				flag = true;
			}
    		if(parmMap.get("applicationid")!=null && StringUtils.isNotBlank(parmMap.get("applicationid").toString())){
				criteria.andEqualTo("applicationid",parmMap.get("applicationid").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cardnumber")!=null && StringUtils.isNotBlank(parmMap.get("cardnumber").toString())){
				criteria.andEqualTo("cardnumber",parmMap.get("cardnumber").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfqqcg")!=null && StringUtils.isNotBlank(parmMap.get("sfqqcg").toString())){
				criteria.andEqualTo("sfqqcg",parmMap.get("sfqqcg").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("MxQqGlServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("MxQqGlServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
    @Override
	public int insertBatch(List<Map> dataList) throws Exception{
    	return mxQqGlMapper.insertBatch(dataList);
	}
}
