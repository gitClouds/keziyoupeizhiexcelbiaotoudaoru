package com.unis.service.znzf.impl;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.CbMapper;
import com.unis.model.znzf.Cb;
import com.unis.service.znzf.CbService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see CbService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-04-22
 */
@Service("cbService")
public class CbServiceImpl extends BaseServiceImpl implements CbService {
	private static final Logger logger = LoggerFactory.getLogger(CbServiceImpl.class);
    @Autowired
    private CbMapper cbMapper;

    /**
     * @see CbService#insert(Cb cb)
     */
    @Override
    public int insert(Cb cb) throws Exception {
    	if (cb!=null){
	        //cb.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return cbMapper.insertSelective(cb);
    	}else{
    		logger.error("CbServiceImpl.insert时cb数据为空。");
    		throw new AppRuntimeException("CbServiceImpl.insert时cb数据为空。");
    	}        
    }

    /**
     * @see CbService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("CbServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("CbServiceImpl.delete时pk为空。");
    	}else{
    		return cbMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see CbService#updateByPrimaryKey(Cb cb)
     */
    @Override
    public int updateByPrimaryKey(Cb cb) throws Exception {
        if (cb!=null){
        	if(StringUtils.isBlank(cb.getPk())){
        		logger.error("CbServiceImpl.updateByPrimaryKey时cb.Pk为空。");
        		throw new AppRuntimeException("CbServiceImpl.updateByPrimaryKey时cb.Pk为空。");
        	}
	        return cbMapper.updateByPrimaryKeySelective(cb);
    	}else{
    		logger.error("CbServiceImpl.updateByPrimaryKey时cb数据为空。");
    		throw new AppRuntimeException("CbServiceImpl.updateByPrimaryKey时cb数据为空。");
    	}
    }
    /**
     * @see CbService#queryCbByPrimaryKey(String pk)
     */
    @Override
    public Cb queryCbByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("CbServiceImpl.queryCbByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("CbServiceImpl.queryCbByPrimaryKey时pk为空。");
    	}else{
    		return cbMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see CbService#queryAsObject(Cb cb)
     */
    @Override
    public Cb queryAsObject(Cb cb) throws Exception {
        if (cb!=null){
	        return cbMapper.selectOne(cb);
    	}else{
    		logger.error("CbServiceImpl.queryAsObject时cb数据为空。");
    		throw new AppRuntimeException("CbServiceImpl.queryAsObject时cb数据为空。");
    	}
    }
    
    /**
     * @see CbService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return cbMapper.selectCountByExample(example);
    	}else{
    		logger.error("CbServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("CbServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see CbService#queryListByExample(Example example)
     */
    @Override
    public List<Cb> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return cbMapper.selectByExample(example);
    	}else{
    		logger.error("CbServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("CbServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see CbService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(cbMapper.selectByExample(example));
    	}else{
    		logger.error("CbServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("CbServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see CbService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(Cb.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cbbh")!=null && StringUtils.isNotBlank(parmMap.get("cbbh").toString())){
				criteria.andEqualTo("cbbh",parmMap.get("cbbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sertype")!=null && StringUtils.isNotBlank(parmMap.get("sertype").toString())){
				criteria.andEqualTo("sertype",parmMap.get("sertype").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bankcode")!=null && StringUtils.isNotBlank(parmMap.get("bankcode").toString())){
				criteria.andEqualTo("bankcode",parmMap.get("bankcode").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bankname")!=null && StringUtils.isNotBlank(parmMap.get("bankname").toString())){
				criteria.andEqualTo("bankname",parmMap.get("bankname").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cardname")!=null && StringUtils.isNotBlank(parmMap.get("cardname").toString())){
				criteria.andEqualTo("cardname",parmMap.get("cardname").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cardnumber")!=null && StringUtils.isNotBlank(parmMap.get("cardnumber").toString())){
				criteria.andEqualTo("cardnumber",parmMap.get("cardnumber").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bz")!=null && StringUtils.isNotBlank(parmMap.get("bz").toString())){
				criteria.andEqualTo("bz",parmMap.get("bz").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("CbServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("CbServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
