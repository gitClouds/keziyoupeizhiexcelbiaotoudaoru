package com.unis.service.znzf.impl;

import java.sql.Clob;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.unis.common.util.StrUtil;
import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.CbMacGlMapper;
import com.unis.model.znzf.CbMacGl;
import com.unis.service.znzf.CbMacGlService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see CbMacGlService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-05-24
 */
@Service("cbMacGlService")
public class CbMacGlServiceImpl extends BaseServiceImpl implements CbMacGlService {
	private static final Logger logger = LoggerFactory.getLogger(CbMacGlServiceImpl.class);
    @Autowired
    private CbMacGlMapper cbMacGlMapper;
	public List<Map> queryQqzhByJjdpkAndMacdz(String jjdpk,String macdz) throws Exception{
		if(StringUtils.isNotBlank(jjdpk)&&StringUtils.isNotBlank(macdz)){
			return cbMacGlMapper.queryQqzhByJjdpkAndMacdz(jjdpk,macdz);
		}else{
			logger.error("CbMacGlServiceImpl.queryListByJjdpkAndMacdz时jjdpk或macdz数据为空。");
			throw new AppRuntimeException("CbMacGlServiceImpl.queryListByJjdpkAndMacdz时jjdpk或macdz数据为空。");
		}
	}
    /**
     * @see CbMacGlService#insert(CbMacGl cbMacGl)
     */
    @Override
    public int insert(CbMacGl cbMacGl) throws Exception {
    	if (cbMacGl!=null){
	        //cbMacGl.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return cbMacGlMapper.insertSelective(cbMacGl);
    	}else{
    		logger.error("CbMacGlServiceImpl.insert时cbMacGl数据为空。");
    		throw new AppRuntimeException("CbMacGlServiceImpl.insert时cbMacGl数据为空。");
    	}        
    }

    /**
     * @see CbMacGlService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("CbMacGlServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("CbMacGlServiceImpl.delete时pk为空。");
    	}else{
    		return cbMacGlMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see CbMacGlService#updateByPrimaryKey(CbMacGl cbMacGl)
     */
    @Override
    public int updateByPrimaryKey(CbMacGl cbMacGl) throws Exception {
        if (cbMacGl!=null){
        	if(StringUtils.isBlank(cbMacGl.getPk())){
        		logger.error("CbMacGlServiceImpl.updateByPrimaryKey时cbMacGl.Pk为空。");
        		throw new AppRuntimeException("CbMacGlServiceImpl.updateByPrimaryKey时cbMacGl.Pk为空。");
        	}
	        return cbMacGlMapper.updateByPrimaryKeySelective(cbMacGl);
    	}else{
    		logger.error("CbMacGlServiceImpl.updateByPrimaryKey时cbMacGl数据为空。");
    		throw new AppRuntimeException("CbMacGlServiceImpl.updateByPrimaryKey时cbMacGl数据为空。");
    	}
    }
    /**
     * @see CbMacGlService#queryCbMacGlByPrimaryKey(String pk)
     */
    @Override
    public CbMacGl queryCbMacGlByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("CbMacGlServiceImpl.queryCbMacGlByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("CbMacGlServiceImpl.queryCbMacGlByPrimaryKey时pk为空。");
    	}else{
    		return cbMacGlMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see CbMacGlService#queryAsObject(CbMacGl cbMacGl)
     */
    @Override
    public CbMacGl queryAsObject(CbMacGl cbMacGl) throws Exception {
        if (cbMacGl!=null){
	        return cbMacGlMapper.selectOne(cbMacGl);
    	}else{
    		logger.error("CbMacGlServiceImpl.queryAsObject时cbMacGl数据为空。");
    		throw new AppRuntimeException("CbMacGlServiceImpl.queryAsObject时cbMacGl数据为空。");
    	}
    }
    
    /**
     * @see CbMacGlService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return cbMacGlMapper.selectCountByExample(example);
    	}else{
    		logger.error("CbMacGlServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("CbMacGlServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see CbMacGlService#queryListByExample(Example example)
     */
    @Override
    public List<CbMacGl> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return cbMacGlMapper.selectByExample(example);
    	}else{
    		logger.error("CbMacGlServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("CbMacGlServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see CbMacGlService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(cbMacGlMapper.selectByExample(example));
    	}else{
    		logger.error("CbMacGlServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("CbMacGlServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see CbMacGlService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(CbMacGl.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cbbh")!=null && StringUtils.isNotBlank(parmMap.get("cbbh").toString())){
				criteria.andEqualTo("cbbh",parmMap.get("cbbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sertype")!=null && StringUtils.isNotBlank(parmMap.get("sertype").toString())){
				criteria.andEqualTo("sertype",parmMap.get("sertype").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qqpk")!=null && StringUtils.isNotBlank(parmMap.get("qqpk").toString())){
				criteria.andEqualTo("qqpk",parmMap.get("qqpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jjdpk")!=null && StringUtils.isNotBlank(parmMap.get("jjdpk").toString())){
				criteria.andEqualTo("jjdpk",parmMap.get("jjdpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("macdz")!=null && StringUtils.isNotBlank(parmMap.get("macdz").toString())){
				criteria.andEqualTo("macdz",parmMap.get("macdz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qqzh")!=null && StringUtils.isNotBlank(parmMap.get("qqzh").toString())){
				criteria.andEqualTo("qqzh",parmMap.get("qqzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qqzhxm")!=null && StringUtils.isNotBlank(parmMap.get("qqzhxm").toString())){
				criteria.andEqualTo("qqzhxm",parmMap.get("qqzhxm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qqzhjgdm")!=null && StringUtils.isNotBlank(parmMap.get("qqzhjgdm").toString())){
				criteria.andEqualTo("qqzhjgdm",parmMap.get("qqzhjgdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qqzhjgmc")!=null && StringUtils.isNotBlank(parmMap.get("qqzhjgmc").toString())){
				criteria.andEqualTo("qqzhjgmc",parmMap.get("qqzhjgmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jjdhm")!=null && StringUtils.isNotBlank(parmMap.get("jjdhm").toString())){
				criteria.andEqualTo("jjdhm",parmMap.get("jjdhm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("barxm")!=null && StringUtils.isNotBlank(parmMap.get("barxm").toString())){
				criteria.andEqualTo("barxm",parmMap.get("barxm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sldwdm")!=null && StringUtils.isNotBlank(parmMap.get("sldwdm").toString())){
				criteria.andEqualTo("sldwdm",parmMap.get("sldwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sldwmc")!=null && StringUtils.isNotBlank(parmMap.get("sldwmc").toString())){
				criteria.andEqualTo("sldwmc",parmMap.get("sldwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("afsj")!=null && StringUtils.isNotBlank(parmMap.get("afsj").toString())){
				criteria.andEqualTo("afsj",parmMap.get("afsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("CbMacGlServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("CbMacGlServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }

	@Override
	public ResultDto queryListByQqpk(Map parmMap, int pageNum, int pageSize) throws  Exception {
		if (parmMap != null) {
			Example example = new Example(CbMacGl.class);
			example.setOrderByClause(" afsj,pk");//设置排序
			Example.Criteria criteria = example.createCriteria();
			boolean flag = false;
			if(parmMap.get("qqpk")!=null && StringUtils.isNotBlank(parmMap.get("qqpk").toString())){
				String[] strs = parmMap.get("qqpk").toString().trim().split(",");
				List<String> list= Arrays.asList(strs);

				criteria.andIn("pk",list);
				flag = true;
			}
			//一个条件都没有的时候给一个默认条件
			if(!flag){

			}
			PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
		}else{
			logger.error("JltServiceImpl.queryListByJjdpkAndCbyj时parmMap数据为空。");
			throw new AppRuntimeException("JltServiceImpl.queryListByJjdpkAndCbyj时parmMap数据为空。");
		}
	}
	public PageInfo queryCbMacGlListByMap(Map parmMap, int pageNum, int pageSize) throws  Exception{
		if (parmMap!=null && !parmMap.isEmpty() && parmMap.size()>0){
			PageHelper.startPage(pageNum,pageSize);

			List<Map> list = cbMacGlMapper.queryListByMap(parmMap);
			if(list!=null &&!list.isEmpty() && list.size()>0){
				if(list.get(0).get("QQPK") instanceof String){

				}else{
					for (Map map:list ){
						map.put("QQPK", StrUtil.ClobtoString((Clob) map.get("QQPK")));
						map.put("CBYJ", StrUtil.ClobtoString((Clob) map.get("CBYJ")));
					}
				}
			}
			return new PageInfo(list);
		}else {
			logger.error("CbMacGlServiceImpl.queryCbMacGlListByMap时map数据为空。");
			throw new AppRuntimeException("CbMacGlServiceImpl.queryCbMacGlListByMap时map数据为空。");
		}
	}
	@Override
	public ResultDto queryListByMap(Map parmMap, int pageNum, int pageSize) throws Exception {
		if (parmMap!=null){
			PageInfo pageInfo = this.queryCbMacGlListByMap(parmMap,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
		}else{
			logger.error("CbMacGlServiceImpl.queryListByMap时parmMap数据为空。");
			throw new AppRuntimeException("CbMacGlServiceImpl.queryListByMap时parmMap数据为空。");
		}
	}
}
