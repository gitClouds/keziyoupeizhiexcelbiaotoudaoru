package com.unis.service.znzf.impl;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.ZnzfAnalysisLogMapper;
import com.unis.model.znzf.ZnzfAnalysisLog;
import com.unis.service.znzf.ZnzfAnalysisLogService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see ZnzfAnalysisLogService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-03-13
 */
@Service("znzfAnalysisLogService")
public class ZnzfAnalysisLogServiceImpl extends BaseServiceImpl implements ZnzfAnalysisLogService {
	private static final Logger logger = LoggerFactory.getLogger(ZnzfAnalysisLogServiceImpl.class);
    @Autowired
    private ZnzfAnalysisLogMapper znzfAnalysisLogMapper;

    /**
     * @see ZnzfAnalysisLogService#insert(ZnzfAnalysisLog znzfAnalysisLog)
     */
    @Override
    public int insert(ZnzfAnalysisLog znzfAnalysisLog) throws Exception {
    	if (znzfAnalysisLog!=null){
	        //znzfAnalysisLog.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return znzfAnalysisLogMapper.insertSelective(znzfAnalysisLog);
    	}else{
    		logger.error("ZnzfAnalysisLogServiceImpl.insert时znzfAnalysisLog数据为空。");
    		throw new AppRuntimeException("ZnzfAnalysisLogServiceImpl.insert时znzfAnalysisLog数据为空。");
    	}        
    }

    /**
     * @see ZnzfAnalysisLogService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("ZnzfAnalysisLogServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("ZnzfAnalysisLogServiceImpl.delete时pk为空。");
    	}else{
    		return znzfAnalysisLogMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see ZnzfAnalysisLogService#updateByPrimaryKey(ZnzfAnalysisLog znzfAnalysisLog)
     */
    @Override
    public int updateByPrimaryKey(ZnzfAnalysisLog znzfAnalysisLog) throws Exception {
        if (znzfAnalysisLog!=null){
        	if(StringUtils.isBlank(znzfAnalysisLog.getPk())){
        		logger.error("ZnzfAnalysisLogServiceImpl.updateByPrimaryKey时znzfAnalysisLog.Pk为空。");
        		throw new AppRuntimeException("ZnzfAnalysisLogServiceImpl.updateByPrimaryKey时znzfAnalysisLog.Pk为空。");
        	}
	        return znzfAnalysisLogMapper.updateByPrimaryKeySelective(znzfAnalysisLog);
    	}else{
    		logger.error("ZnzfAnalysisLogServiceImpl.updateByPrimaryKey时znzfAnalysisLog数据为空。");
    		throw new AppRuntimeException("ZnzfAnalysisLogServiceImpl.updateByPrimaryKey时znzfAnalysisLog数据为空。");
    	}
    }
    /**
     * @see ZnzfAnalysisLogService#queryZnzfAnalysisLogByPrimaryKey(String pk)
     */
    @Override
    public ZnzfAnalysisLog queryZnzfAnalysisLogByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("ZnzfAnalysisLogServiceImpl.queryZnzfAnalysisLogByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("ZnzfAnalysisLogServiceImpl.queryZnzfAnalysisLogByPrimaryKey时pk为空。");
    	}else{
    		return znzfAnalysisLogMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see ZnzfAnalysisLogService#queryAsObject(ZnzfAnalysisLog znzfAnalysisLog)
     */
    @Override
    public ZnzfAnalysisLog queryAsObject(ZnzfAnalysisLog znzfAnalysisLog) throws Exception {
        if (znzfAnalysisLog!=null){
	        return znzfAnalysisLogMapper.selectOne(znzfAnalysisLog);
    	}else{
    		logger.error("ZnzfAnalysisLogServiceImpl.queryAsObject时znzfAnalysisLog数据为空。");
    		throw new AppRuntimeException("ZnzfAnalysisLogServiceImpl.queryAsObject时znzfAnalysisLog数据为空。");
    	}
    }
    
    /**
     * @see ZnzfAnalysisLogService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return znzfAnalysisLogMapper.selectCountByExample(example);
    	}else{
    		logger.error("ZnzfAnalysisLogServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("ZnzfAnalysisLogServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see ZnzfAnalysisLogService#queryListByExample(Example example)
     */
    @Override
    public List<ZnzfAnalysisLog> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return znzfAnalysisLogMapper.selectByExample(example);
    	}else{
    		logger.error("ZnzfAnalysisLogServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("ZnzfAnalysisLogServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see ZnzfAnalysisLogService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(znzfAnalysisLogMapper.selectByExample(example));
    	}else{
    		logger.error("ZnzfAnalysisLogServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("ZnzfAnalysisLogServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see ZnzfAnalysisLogService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(ZnzfAnalysisLog.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jjdpk")!=null && StringUtils.isNotBlank(parmMap.get("jjdpk").toString())){
				criteria.andEqualTo("jjdpk",parmMap.get("jjdpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cardnumber")!=null && StringUtils.isNotBlank(parmMap.get("cardnumber").toString())){
				criteria.andEqualTo("cardnumber",parmMap.get("cardnumber").toString().trim());
				flag = true;
			}
    		if(parmMap.get("mxjson")!=null && StringUtils.isNotBlank(parmMap.get("mxjson").toString())){
				criteria.andEqualTo("mxjson",parmMap.get("mxjson").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ztjson")!=null && StringUtils.isNotBlank(parmMap.get("ztjson").toString())){
				criteria.andEqualTo("ztjson",parmMap.get("ztjson").toString().trim());
				flag = true;
			}
    		if(parmMap.get("isok")!=null && StringUtils.isNotBlank(parmMap.get("isok").toString())){
				criteria.andEqualTo("isok",parmMap.get("isok").toString().trim());
				flag = true;
			}
    		if(parmMap.get("errmsg")!=null && StringUtils.isNotBlank(parmMap.get("errmsg").toString())){
				criteria.andEqualTo("errmsg",parmMap.get("errmsg").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("ZnzfAnalysisLogServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("ZnzfAnalysisLogServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
