package com.unis.service.znzf;

import com.github.pagehelper.PageInfo;
import com.unis.model.znzf.AnalysisQueue;
import com.unis.service.BaseService;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * <pre>
 * TODO
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-04-17
 */
public interface AnalysisQueueService extends BaseService {

    /**
     * 新增AnalysisQueue实例
     * 
     * @param analysisQueue
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int insert(AnalysisQueue analysisQueue) throws Exception;

    /**
     * 新增AnalysisQueue集合
     *
     * @param list
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int insert(List<AnalysisQueue> list) throws Exception;
    /**
     * 删除AnalysisQueue实例
     * 
     * @param pk
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int delete(String pk) throws Exception;

    /**
     * 更新AnalysisQueue实例
     * 
     * @param analysisQueue
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int updateByPrimaryKey(AnalysisQueue analysisQueue) throws Exception;
    /**
     * 根据pk主键获取对象
     * 
     * @param pk
     * @throws Exception
     */
    AnalysisQueue queryAnalysisQueueByPrimaryKey(String pk) throws Exception;

    /**
     * 查询AnalysisQueue实例
     * 
     * @param analysisQueue
     * @throws Exception
     */
    AnalysisQueue queryAsObject(AnalysisQueue analysisQueue) throws Exception;
    
    	
	/**
     * 根据条件获取条数
     * 
     * @param example
     * @throws Exception
     */
	int queryCountByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合
     * 
     * @param example
     * @throws Exception
     */
	List<AnalysisQueue> queryListByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合(分页),需在example 设定排序
     * 
     * @param example
     * @param pageNum
     * @param pageSize
     * @throws Exception
     */
	PageInfo queryPageInfoByExample(Example example, int pageNum, int pageSize) throws Exception;

}
