package com.unis.service.znzf.impl;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.SfMxJgLstMapper;
import com.unis.model.znzf.SfMxJgLst;
import com.unis.service.znzf.SfMxJgLstService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see SfMxJgLstService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-03-09
 */
@Service("sfMxJgLstService")
public class SfMxJgLstServiceImpl extends BaseServiceImpl implements SfMxJgLstService {
	private static final Logger logger = LoggerFactory.getLogger(SfMxJgLstServiceImpl.class);
    @Autowired
    private SfMxJgLstMapper sfMxJgLstMapper;

    /**
     * @see SfMxJgLstService#insert(SfMxJgLst sfMxJgLst)
     */
    @Override
    public int insert(SfMxJgLst sfMxJgLst) throws Exception {
    	if (sfMxJgLst!=null){
	        //sfMxJgLst.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return sfMxJgLstMapper.insertSelective(sfMxJgLst);
    	}else{
    		logger.error("SfMxJgLstServiceImpl.insert时sfMxJgLst数据为空。");
    		throw new AppRuntimeException("SfMxJgLstServiceImpl.insert时sfMxJgLst数据为空。");
    	}        
    }

    /**
     * @see SfMxJgLstService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("SfMxJgLstServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("SfMxJgLstServiceImpl.delete时pk为空。");
    	}else{
    		return sfMxJgLstMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see SfMxJgLstService#updateByPrimaryKey(SfMxJgLst sfMxJgLst)
     */
    @Override
    public int updateByPrimaryKey(SfMxJgLst sfMxJgLst) throws Exception {
        if (sfMxJgLst!=null){
        	if(StringUtils.isBlank(sfMxJgLst.getPk())){
        		logger.error("SfMxJgLstServiceImpl.updateByPrimaryKey时sfMxJgLst.Pk为空。");
        		throw new AppRuntimeException("SfMxJgLstServiceImpl.updateByPrimaryKey时sfMxJgLst.Pk为空。");
        	}
	        return sfMxJgLstMapper.updateByPrimaryKeySelective(sfMxJgLst);
    	}else{
    		logger.error("SfMxJgLstServiceImpl.updateByPrimaryKey时sfMxJgLst数据为空。");
    		throw new AppRuntimeException("SfMxJgLstServiceImpl.updateByPrimaryKey时sfMxJgLst数据为空。");
    	}
    }
    /**
     * @see SfMxJgLstService#querySfMxJgLstByPrimaryKey(String pk)
     */
    @Override
    public SfMxJgLst querySfMxJgLstByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("SfMxJgLstServiceImpl.querySfMxJgLstByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("SfMxJgLstServiceImpl.querySfMxJgLstByPrimaryKey时pk为空。");
    	}else{
    		return sfMxJgLstMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see SfMxJgLstService#queryAsObject(SfMxJgLst sfMxJgLst)
     */
    @Override
    public SfMxJgLst queryAsObject(SfMxJgLst sfMxJgLst) throws Exception {
        if (sfMxJgLst!=null){
	        return sfMxJgLstMapper.selectOne(sfMxJgLst);
    	}else{
    		logger.error("SfMxJgLstServiceImpl.queryAsObject时sfMxJgLst数据为空。");
    		throw new AppRuntimeException("SfMxJgLstServiceImpl.queryAsObject时sfMxJgLst数据为空。");
    	}
    }
    
    /**
     * @see SfMxJgLstService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return sfMxJgLstMapper.selectCountByExample(example);
    	}else{
    		logger.error("SfMxJgLstServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("SfMxJgLstServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see SfMxJgLstService#queryListByExample(Example example)
     */
    @Override
    public List<SfMxJgLst> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return sfMxJgLstMapper.selectByExample(example);
    	}else{
    		logger.error("SfMxJgLstServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("SfMxJgLstServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see SfMxJgLstService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(sfMxJgLstMapper.selectByExample(example));
    	}else{
    		logger.error("SfMxJgLstServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("SfMxJgLstServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see SfMxJgLstService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(SfMxJgLst.class);
    		example.setOrderByClause("jysj DESC,pk asc");//设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("qqpk")!=null && StringUtils.isNotBlank(parmMap.get("qqpk").toString())){
				criteria.andEqualTo("qqpk",parmMap.get("qqpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jjdpk")!=null && StringUtils.isNotBlank(parmMap.get("jjdpk").toString())){
				criteria.andEqualTo("jjdpk",parmMap.get("jjdpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("applicationid")!=null && StringUtils.isNotBlank(parmMap.get("applicationid").toString())){
				criteria.andEqualTo("applicationid",parmMap.get("applicationid").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zfddh")!=null && StringUtils.isNotBlank(parmMap.get("zfddh").toString())){
				criteria.andEqualTo("zfddh",parmMap.get("zfddh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jylx")!=null && StringUtils.isNotBlank(parmMap.get("jylx").toString())){
				criteria.andEqualTo("jylx",parmMap.get("jylx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zflx")!=null && StringUtils.isNotBlank(parmMap.get("zflx").toString())){
				criteria.andEqualTo("zflx",parmMap.get("zflx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jdbz")!=null && StringUtils.isNotBlank(parmMap.get("jdbz").toString())){
				criteria.andEqualTo("jdbz",parmMap.get("jdbz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jysj")!=null && StringUtils.isNotBlank(parmMap.get("jysj").toString())){
				criteria.andEqualTo("jysj",parmMap.get("jysj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rmbzl")!=null && StringUtils.isNotBlank(parmMap.get("rmbzl").toString())){
				criteria.andEqualTo("rmbzl",parmMap.get("rmbzl").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jyje")!=null && StringUtils.isNotBlank(parmMap.get("jyje").toString())){
				criteria.andEqualTo("jyje",parmMap.get("jyje").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jylsh")!=null && StringUtils.isNotBlank(parmMap.get("jylsh").toString())){
				criteria.andEqualTo("jylsh",parmMap.get("jylsh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ye")!=null && StringUtils.isNotBlank(parmMap.get("ye").toString())){
				criteria.andEqualTo("ye",parmMap.get("ye").toString().trim());
				flag = true;
			}
    		if(parmMap.get("skyhjgdm")!=null && StringUtils.isNotBlank(parmMap.get("skyhjgdm").toString())){
				criteria.andEqualTo("skyhjgdm",parmMap.get("skyhjgdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("skyhjgmc")!=null && StringUtils.isNotBlank(parmMap.get("skyhjgmc").toString())){
				criteria.andEqualTo("skyhjgmc",parmMap.get("skyhjgmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("skyhzh")!=null && StringUtils.isNotBlank(parmMap.get("skyhzh").toString())){
				criteria.andEqualTo("skyhzh",parmMap.get("skyhzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("skzfzh")!=null && StringUtils.isNotBlank(parmMap.get("skzfzh").toString())){
				criteria.andEqualTo("skzfzh",parmMap.get("skzfzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("possbh")!=null && StringUtils.isNotBlank(parmMap.get("possbh").toString())){
				criteria.andEqualTo("possbh",parmMap.get("possbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("skshm")!=null && StringUtils.isNotBlank(parmMap.get("skshm").toString())){
				criteria.andEqualTo("skshm",parmMap.get("skshm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fkyhjgdm")!=null && StringUtils.isNotBlank(parmMap.get("fkyhjgdm").toString())){
				criteria.andEqualTo("fkyhjgdm",parmMap.get("fkyhjgdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fkyhjgmc")!=null && StringUtils.isNotBlank(parmMap.get("fkyhjgmc").toString())){
				criteria.andEqualTo("fkyhjgmc",parmMap.get("fkyhjgmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fkyhzh")!=null && StringUtils.isNotBlank(parmMap.get("fkyhzh").toString())){
				criteria.andEqualTo("fkyhzh",parmMap.get("fkyhzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fkzfzh")!=null && StringUtils.isNotBlank(parmMap.get("fkzfzh").toString())){
				criteria.andEqualTo("fkzfzh",parmMap.get("fkzfzh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jysblx")!=null && StringUtils.isNotBlank(parmMap.get("jysblx").toString())){
				criteria.andEqualTo("jysblx",parmMap.get("jysblx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zfsbip")!=null && StringUtils.isNotBlank(parmMap.get("zfsbip").toString())){
				criteria.andEqualTo("zfsbip",parmMap.get("zfsbip").toString().trim());
				flag = true;
			}
    		if(parmMap.get("macdz")!=null && StringUtils.isNotBlank(parmMap.get("macdz").toString())){
				criteria.andEqualTo("macdz",parmMap.get("macdz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jyddjd")!=null && StringUtils.isNotBlank(parmMap.get("jyddjd").toString())){
				criteria.andEqualTo("jyddjd",parmMap.get("jyddjd").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jyddwd")!=null && StringUtils.isNotBlank(parmMap.get("jyddwd").toString())){
				criteria.andEqualTo("jyddwd",parmMap.get("jyddwd").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jysbh")!=null && StringUtils.isNotBlank(parmMap.get("jysbh").toString())){
				criteria.andEqualTo("jysbh",parmMap.get("jysbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yhwblsh")!=null && StringUtils.isNotBlank(parmMap.get("yhwblsh").toString())){
				criteria.andEqualTo("yhwblsh",parmMap.get("yhwblsh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bz")!=null && StringUtils.isNotBlank(parmMap.get("bz").toString())){
				criteria.andEqualTo("bz",parmMap.get("bz").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("SfMxJgLstServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("SfMxJgLstServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }

	@Override
	public List<String> queryApplicationByQqpk(String qqpk) throws Exception {
		if (StringUtils.isNotBlank(qqpk)){
			return sfMxJgLstMapper.queryApplicationByQqpk(qqpk);
		}else{
			logger.error("SfMxJgLstServiceImpl.queryApplicationByQqpk时qqpk数据为空。");
			throw new AppRuntimeException("SfMxJgLstServiceImpl.queryApplicationByQqpk时qqpk数据为空。");
		}
	}


	@Override
	public List<Map<String, String>> queryMxListByJjd(String jjdpk, String zh, int nlevel) {
		if (StringUtils.isNotBlank(jjdpk)){
			return sfMxJgLstMapper.queryMxListByJjd(jjdpk,zh,nlevel);
		}
		return null;
	}

	@Override
	public List<SfMxJgLst> queryJe(String ywsqbh, String zh,Short type) throws Exception {
		if (StringUtils.isNotBlank(ywsqbh)&&StringUtils.isNotBlank(zh)){
			return sfMxJgLstMapper.queryJe(ywsqbh,zh,type);
		}else {
			throw new Exception("三方queryJe参数为空：ywsqbh-"+ywsqbh+",zh-"+zh+",type-"+type);
		}
	}
}
