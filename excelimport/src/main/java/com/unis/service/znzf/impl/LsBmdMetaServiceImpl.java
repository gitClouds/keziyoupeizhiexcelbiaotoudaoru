package com.unis.service.znzf.impl;

import java.util.List;
import java.util.Map;

import com.unis.common.util.TemplateUtil;
import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.LsBmdMetaMapper;
import com.unis.model.znzf.LsBmdMeta;
import com.unis.service.znzf.LsBmdMetaService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see LsBmdMetaService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-05-20
 */
@Service("lsBmdMetaService")
public class LsBmdMetaServiceImpl extends BaseServiceImpl implements LsBmdMetaService {
	private static final Logger logger = LoggerFactory.getLogger(LsBmdMetaServiceImpl.class);
    @Autowired
    private LsBmdMetaMapper lsBmdMetaMapper;

    /**
     * @see LsBmdMetaService#insert(LsBmdMeta lsBmdMeta)
     */
    @Override
    public int insert(LsBmdMeta lsBmdMeta) throws Exception {
    	if (lsBmdMeta!=null){
	        lsBmdMeta.setPk(TemplateUtil.genUUID());
	        return lsBmdMetaMapper.insertSelective(lsBmdMeta);
    	}else{
    		logger.error("LsBmdMetaServiceImpl.insert时lsBmdMeta数据为空。");
    		throw new AppRuntimeException("LsBmdMetaServiceImpl.insert时lsBmdMeta数据为空。");
    	}        
    }

    /**
     * @see LsBmdMetaService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("LsBmdMetaServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("LsBmdMetaServiceImpl.delete时pk为空。");
    	}else{
    		return lsBmdMetaMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see LsBmdMetaService#updateByPrimaryKey(LsBmdMeta lsBmdMeta)
     */
    @Override
    public int updateByPrimaryKey(LsBmdMeta lsBmdMeta) throws Exception {
        if (lsBmdMeta!=null){
        	if(StringUtils.isBlank(lsBmdMeta.getPk())){
        		logger.error("LsBmdMetaServiceImpl.updateByPrimaryKey时lsBmdMeta.Pk为空。");
        		throw new AppRuntimeException("LsBmdMetaServiceImpl.updateByPrimaryKey时lsBmdMeta.Pk为空。");
        	}
	        return lsBmdMetaMapper.updateByPrimaryKeySelective(lsBmdMeta);
    	}else{
    		logger.error("LsBmdMetaServiceImpl.updateByPrimaryKey时lsBmdMeta数据为空。");
    		throw new AppRuntimeException("LsBmdMetaServiceImpl.updateByPrimaryKey时lsBmdMeta数据为空。");
    	}
    }
    /**
     * @see LsBmdMetaService#queryLsBmdMetaByPrimaryKey(String pk)
     */
    @Override
    public LsBmdMeta queryLsBmdMetaByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("LsBmdMetaServiceImpl.queryLsBmdMetaByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("LsBmdMetaServiceImpl.queryLsBmdMetaByPrimaryKey时pk为空。");
    	}else{
    		return lsBmdMetaMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see LsBmdMetaService#queryAsObject(LsBmdMeta lsBmdMeta)
     */
    @Override
    public LsBmdMeta queryAsObject(LsBmdMeta lsBmdMeta) throws Exception {
        if (lsBmdMeta!=null){
	        return lsBmdMetaMapper.selectOne(lsBmdMeta);
    	}else{
    		logger.error("LsBmdMetaServiceImpl.queryAsObject时lsBmdMeta数据为空。");
    		throw new AppRuntimeException("LsBmdMetaServiceImpl.queryAsObject时lsBmdMeta数据为空。");
    	}
    }
    
    /**
     * @see LsBmdMetaService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return lsBmdMetaMapper.selectCountByExample(example);
    	}else{
    		logger.error("LsBmdMetaServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("LsBmdMetaServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see LsBmdMetaService#queryListByExample(Example example)
     */
    @Override
    public List<LsBmdMeta> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return lsBmdMetaMapper.selectByExample(example);
    	}else{
    		logger.error("LsBmdMetaServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("LsBmdMetaServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see LsBmdMetaService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(lsBmdMetaMapper.selectByExample(example));
    	}else{
    		logger.error("LsBmdMetaServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("LsBmdMetaServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see LsBmdMetaService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(LsBmdMeta.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("bankcode")!=null && StringUtils.isNotBlank(parmMap.get("bankcode").toString())){
				criteria.andEqualTo("bankcode",parmMap.get("bankcode").toString().trim());
				flag = true;
			}
    		if(parmMap.get("paycode")!=null && StringUtils.isNotBlank(parmMap.get("paycode").toString())){
				criteria.andEqualTo("paycode",parmMap.get("paycode").toString().trim());
				flag = true;
			}
    		if(parmMap.get("checkvalue")!=null && StringUtils.isNotBlank(parmMap.get("checkvalue").toString())){
				criteria.andEqualTo("checkvalue",parmMap.get("checkvalue").toString().trim());
				flag = true;
			}
    		if(parmMap.get("positionval")!=null && StringUtils.isNotBlank(parmMap.get("positionval").toString())){
				criteria.andEqualTo("positionval",parmMap.get("positionval").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("rksj")!=null && StringUtils.isNotBlank(parmMap.get("rksj").toString())){
				criteria.andEqualTo("rksj",parmMap.get("rksj").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("LsBmdMetaServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("LsBmdMetaServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
