package com.unis.service.znzf.impl;

import com.unis.common.exception.app.AppRuntimeException;
import com.unis.mapper.znzf.TbDxzpYhkCompanyZhMapper;
import com.unis.model.znzf.TbDxzpYhkCompanyZh;
import com.unis.service.impl.BaseServiceImpl;
import com.unis.service.znzf.TbDxzpYhkCompanyZhService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * <pre>
 * @see TbDxzpYhkCompanyZhService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-12-10
 */
@Service("tbDxzpYhkCompanyZhService")
public class TbDxzpYhkCompanyZhServiceImpl extends BaseServiceImpl implements TbDxzpYhkCompanyZhService {
	private static final Logger logger = LoggerFactory.getLogger(TbDxzpYhkCompanyZhServiceImpl.class);
    @Autowired
    private TbDxzpYhkCompanyZhMapper tbDxzpYhkCompanyZhMapper;


    /**
     * @see TbDxzpYhkCompanyZhService#queryAsObject(TbDxzpYhkCompanyZh tbDxzpYhkCompanyZh)
     */
    @Override
    public TbDxzpYhkCompanyZh queryAsObject(TbDxzpYhkCompanyZh tbDxzpYhkCompanyZh) throws Exception {
        if (tbDxzpYhkCompanyZh!=null){
	        return tbDxzpYhkCompanyZhMapper.selectOne(tbDxzpYhkCompanyZh);
    	}else{
    		logger.error("TbDxzpYhkCompanyZhServiceImpl.queryAsObject时tbDxzpYhkCompanyZh数据为空。");
    		throw new AppRuntimeException("TbDxzpYhkCompanyZhServiceImpl.queryAsObject时tbDxzpYhkCompanyZh数据为空。");
    	}
    }
    
    /**
     * @see TbDxzpYhkCompanyZhService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return tbDxzpYhkCompanyZhMapper.selectCountByExample(example);
    	}else{
    		logger.error("TbDxzpYhkCompanyZhServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("TbDxzpYhkCompanyZhServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see TbDxzpYhkCompanyZhService#queryListByExample(Example example)
     */
    @Override
    public List<TbDxzpYhkCompanyZh> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return tbDxzpYhkCompanyZhMapper.selectByExample(example);
    	}else{
    		logger.error("TbDxzpYhkCompanyZhServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("TbDxzpYhkCompanyZhServiceImpl.queryListByExample时example数据为空。");
    	}
    }
}
