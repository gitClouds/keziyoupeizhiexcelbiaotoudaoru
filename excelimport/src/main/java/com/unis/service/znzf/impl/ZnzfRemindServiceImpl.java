package com.unis.service.znzf.impl;

import java.util.List;
import java.util.Map;

import com.unis.common.util.TemplateUtil;
import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.znzf.ZnzfRemindMapper;
import com.unis.model.znzf.ZnzfRemind;
import com.unis.service.znzf.ZnzfRemindService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see ZnzfRemindService
 * </pre>
 *
 * @author xuk
 * @version 1.0
 * @since 2019-03-18
 */
@Service("znzfRemindService")
public class ZnzfRemindServiceImpl extends BaseServiceImpl implements ZnzfRemindService {
	private static final Logger logger = LoggerFactory.getLogger(ZnzfRemindServiceImpl.class);
    @Autowired
    private ZnzfRemindMapper znzfRemindMapper;

    /**
     * @see ZnzfRemindService#insert(ZnzfRemind znzfRemind)
     */
    @Override
    public int insert(ZnzfRemind znzfRemind) throws Exception {
    	if (znzfRemind!=null){
	        znzfRemind.setPk(TemplateUtil.genUUID());
	        //menu.setPk(getPk("seqName","jgdm","A"));
	                
	        return znzfRemindMapper.insertSelective(znzfRemind);
    	}else{
    		logger.error("ZnzfRemindServiceImpl.insert时znzfRemind数据为空。");
    		throw new AppRuntimeException("ZnzfRemindServiceImpl.insert时znzfRemind数据为空。");
    	}        
    }

    /**
     * @see ZnzfRemindService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("ZnzfRemindServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("ZnzfRemindServiceImpl.delete时pk为空。");
    	}else{
    		return znzfRemindMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see ZnzfRemindService#updateByPrimaryKey(ZnzfRemind znzfRemind)
     */
    @Override
    public int updateByPrimaryKey(ZnzfRemind znzfRemind) throws Exception {
        if (znzfRemind!=null){
        	if(StringUtils.isBlank(znzfRemind.getPk())){
        		logger.error("ZnzfRemindServiceImpl.updateByPrimaryKey时znzfRemind.Pk为空。");
        		throw new AppRuntimeException("ZnzfRemindServiceImpl.updateByPrimaryKey时znzfRemind.Pk为空。");
        	}
	        return znzfRemindMapper.updateByPrimaryKeySelective(znzfRemind);
    	}else{
    		logger.error("ZnzfRemindServiceImpl.updateByPrimaryKey时znzfRemind数据为空。");
    		throw new AppRuntimeException("ZnzfRemindServiceImpl.updateByPrimaryKey时znzfRemind数据为空。");
    	}
    }
    /**
     * @see ZnzfRemindService#queryZnzfRemindByPrimaryKey(String pk)
     */
    @Override
    public ZnzfRemind queryZnzfRemindByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("ZnzfRemindServiceImpl.queryZnzfRemindByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("ZnzfRemindServiceImpl.queryZnzfRemindByPrimaryKey时pk为空。");
    	}else{
    		return znzfRemindMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see ZnzfRemindService#queryAsObject(ZnzfRemind znzfRemind)
     */
    @Override
    public ZnzfRemind queryAsObject(ZnzfRemind znzfRemind) throws Exception {
        if (znzfRemind!=null){
	        return znzfRemindMapper.selectOne(znzfRemind);
    	}else{
    		logger.error("ZnzfRemindServiceImpl.queryAsObject时znzfRemind数据为空。");
    		throw new AppRuntimeException("ZnzfRemindServiceImpl.queryAsObject时znzfRemind数据为空。");
    	}
    }
    
    /**
     * @see ZnzfRemindService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return znzfRemindMapper.selectCountByExample(example);
    	}else{
    		logger.error("ZnzfRemindServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("ZnzfRemindServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see ZnzfRemindService#queryListByExample(Example example)
     */
    @Override
    public List<ZnzfRemind> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return znzfRemindMapper.selectByExample(example);
    	}else{
    		logger.error("ZnzfRemindServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("ZnzfRemindServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see ZnzfRemindService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(znzfRemindMapper.selectByExample(example));
    	}else{
    		logger.error("ZnzfRemindServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("ZnzfRemindServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see ZnzfRemindService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(ZnzfRemind.class);
    		example.setOrderByClause("pk");//设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
				criteria.andEqualTo("pk",parmMap.get("pk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jjdpk")!=null && StringUtils.isNotBlank(parmMap.get("jjdpk").toString())){
				criteria.andEqualTo("jjdpk",parmMap.get("jjdpk").toString().trim());
				flag = true;
			}
			if(parmMap.get("jltpk")!=null && StringUtils.isNotBlank(parmMap.get("jltpk").toString())){
				criteria.andEqualTo("jltpk",parmMap.get("jltpk").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cardnumber")!=null && StringUtils.isNotBlank(parmMap.get("cardnumber").toString())){
				criteria.andEqualTo("cardnumber",parmMap.get("cardnumber").toString().trim());
				flag = true;
			}
    		if(parmMap.get("remindtype")!=null && StringUtils.isNotBlank(parmMap.get("remindtype").toString())){
				criteria.andEqualTo("remindtype",parmMap.get("remindtype").toString().trim());
				flag = true;
			}
    		if(parmMap.get("remark")!=null && StringUtils.isNotBlank(parmMap.get("remark").toString())){
				criteria.andEqualTo("remark",parmMap.get("remark").toString().trim());
				flag = true;
			}
    		if(parmMap.get("measurespk")!=null && StringUtils.isNotBlank(parmMap.get("measurespk").toString())){
				criteria.andEqualTo("measurespk",parmMap.get("measurespk").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("ZnzfRemindServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("ZnzfRemindServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
