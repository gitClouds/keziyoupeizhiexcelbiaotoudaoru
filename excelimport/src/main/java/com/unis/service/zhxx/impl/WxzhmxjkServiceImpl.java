package com.unis.service.zhxx.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.unis.common.constant.Constants;
import com.unis.common.enums.QqlxEnum;
import com.unis.common.secure.authc.UserInfo;
import com.unis.common.util.TemplateUtil;
import com.unis.common.util.TimeUtil;
import com.unis.mapper.znzf.SfMxQqMapper;
import com.unis.model.zhxx.TbJdzjfxSfZh;
import com.unis.model.znzf.MxQqGl;
import com.unis.model.znzf.SfMxQq;
import com.unis.model.znzf.SfQzhQq;
import com.unis.model.znzf.ZnzfQueue;
import com.unis.service.impl.BaseServiceImpl;
import com.unis.service.zhxx.TbJdzjfxSfZhService;
import com.unis.service.zhxx.WxzhmxjkService;
import com.unis.service.znzf.MxQqGlService;
import com.unis.service.znzf.SfMxQqService;
import com.unis.service.znzf.SfQzhQqService;
import com.unis.service.znzf.ZnzfQueueService;
import com.unis.service.znzf.ZnzfService;

@Service("wxzhmxjkService")
public class WxzhmxjkServiceImpl extends BaseServiceImpl implements WxzhmxjkService {
	
    @Autowired
    private ZnzfService znzfService;
    @Autowired
    private SfMxQqService sfMxQqService;
    @Autowired
    private MxQqGlService mxQqGlService;
    @Autowired
    private ZnzfQueueService znzfQueueService;
    @Autowired
    private SfQzhQqService sfQzhQqService;
    @Autowired
    private TbJdzjfxSfZhService tbJdzjfxSfZhService;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
	public void mxfx(String id) throws Exception {
		Subject currentUser = SecurityUtils.getSubject();
		UserInfo user=(UserInfo) currentUser.getPrincipal();
		TbJdzjfxSfZh tbJdzjfxSfZh=tbJdzjfxSfZhService.queryTbJdzjfxSfZhByPrimaryKey(id);
		
		if(StringUtils.isNotBlank(tbJdzjfxSfZh.getZh())) {
			//插入三方明细查询请求
			SfMxQq sfMxQq=new SfMxQq();
			sfMxQq.setJjdpk(tbJdzjfxSfZh.getId());
			sfMxQq.setSubjecttype((short)1);
			sfMxQq.setAccnumber(tbJdzjfxSfZh.getZh());
			sfMxQq.setPaycode(tbJdzjfxSfZh.getJgdm());
			sfMxQq.setPayname(tbJdzjfxSfZh.getJgmc());
			sfMxQq.setStarttime(TimeUtil.addDay(tbJdzjfxSfZh.getLrsj(), -4));
			sfMxQq.setExpiretime(TimeUtil.addDay(tbJdzjfxSfZh.getLrsj(), -1));
			sfMxQq.setReason(Constants.SY);
			sfMxQq.setLrdwdm(user.getJgdm());
			sfMxQq.setLrdwmc(user.getJgmc());
			sfMxQq.setLrrjh(user.getJh());
			sfMxQq.setLrrxm(user.getXm());
			sfMxQq.setNlevel((short)(1));
			sfMxQq.setFxbs((short)(2));
			sfMxQq.setSjly(tbJdzjfxSfZh.getSjly());
			
			Map<String,String> map = new HashMap<>();
			map.put("lrdwdm",sfMxQq.getLrdwdm());
			map.put("bankcode",sfMxQq.getPaycode());
			map.put("bankname",sfMxQq.getPayname());
			map.put("cardnumber",sfMxQq.getAccnumber());
			map.put("zhlx",sfMxQq.getSubjecttype().toString());
			String mongoPk = znzfService.insertMongo("sf","cx",map);
			sfMxQq.setFlws(mongoPk);
			sfMxQqService.insert(sfMxQq);	
			
			//插入银行卡明细查询管理
			MxQqGl mxQqGl=new MxQqGl();
			mxQqGl.setPk(TemplateUtil.genUUID());
			mxQqGl.setSertype((short)2);
			mxQqGl.setQqpk(sfMxQq.getPk());
			mxQqGl.setCardnumber(sfMxQq.getAccnumber());
			mxQqGl.setKssj(sfMxQq.getStarttime());
			mxQqGl.setJssj(sfMxQq.getExpiretime());
			mxQqGlService.insert(mxQqGl);
			
			//插入请求序列
			ZnzfQueue znzfQueueMx=new ZnzfQueue();
			znzfQueueMx.setSerpk(mxQqGl.getPk());
			znzfQueueMx.setSertype(QqlxEnum.sfQqlxMxcx.getKey());
			znzfQueueMx.setCardnumber(sfMxQq.getAccnumber());
			znzfQueueMx.setBankcode(sfMxQq.getPaycode());
			znzfQueueService.insert(znzfQueueMx);
		}else {
			//插入全账户查询请求
			SfQzhQq sfQzhQq = new SfQzhQq();
            sfQzhQq.setPk(TemplateUtil.genUUID());
            sfQzhQq.setJjdpk(tbJdzjfxSfZh.getId());
            sfQzhQq.setSubjecttype((short) 1);
            sfQzhQq.setPaycode(tbJdzjfxSfZh.getJgdm());
            sfQzhQq.setPayname(tbJdzjfxSfZh.getJgmc());
            sfQzhQq.setAcctype("02");
            sfQzhQq.setAccnumber(tbJdzjfxSfZh.getZjhm());
            sfQzhQq.setAccountname("");
            sfQzhQq.setAccountype("");
            sfQzhQq.setReason(Constants.SY);
            sfQzhQq.setRemark("");
            sfQzhQq.setLrdwdm(user.getJgdm());
            sfQzhQq.setLrdwmc(user.getJgmc());
            sfQzhQq.setLrrxm(user.getXm());
            sfQzhQq.setLrrjh(user.getJh());
            sfQzhQq.setNlevel((short) 1);
            sfQzhQq.setFxbs((short)2);
            sfQzhQq.setSjly(tbJdzjfxSfZh.getSjly());
			
            Map<String,String> map = new HashMap<>();
			map.put("lrdwdm",sfQzhQq.getLrdwdm());
			map.put("bankcode",sfQzhQq.getPaycode());
			map.put("bankname",sfQzhQq.getPayname());
			map.put("cardnumber",sfQzhQq.getAccnumber());
			map.put("zhlx",sfQzhQq.getSubjecttype().toString());
			String mongoPk = znzfService.insertMongo("sf","cx",map);
			sfQzhQq.setFlws(mongoPk);
			sfQzhQqService.insert(sfQzhQq);	
			
			//插入请求序列
			ZnzfQueue znzfQueueMx=new ZnzfQueue();
			znzfQueueMx.setSerpk(sfQzhQq.getPk());
			znzfQueueMx.setSertype(QqlxEnum.sfQqlxQzhcx.getKey());
			znzfQueueMx.setCardnumber(sfQzhQq.getAccnumber());
			znzfQueueMx.setBankcode(sfQzhQq.getPaycode());
			znzfQueueService.insert(znzfQueueMx);
		}
		tbJdzjfxSfZh.setCxkg("1");
		tbJdzjfxSfZh.setFxbs((short) 2);
		tbJdzjfxSfZhService.updateByPrimaryKey(tbJdzjfxSfZh);
	}

	@Override
	public void tzjxcx(String id, String cxkg) throws Exception {
		TbJdzjfxSfZh tbJdzjfxSfZh=tbJdzjfxSfZhService.queryTbJdzjfxSfZhByPrimaryKey(id);
		tbJdzjfxSfZh.setCxkg(cxkg);
		tbJdzjfxSfZhService.updateByPrimaryKey(tbJdzjfxSfZh);
	}

}
