package com.unis.service.zhxx.impl;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unis.service.impl.BaseServiceImpl;
import com.unis.mapper.zhxx.TbJdzjfxYhZhMapper;
import com.unis.model.zhxx.TbJdzjfxYhZh;
import com.unis.service.zhxx.TbJdzjfxYhZhService;
import com.unis.common.exception.app.AppRuntimeException;

/**
 * <pre>
 * @see TbJdzjfxYhZhService
 * </pre>
 *
 * @author fdzptwx
 * @version 1.0
 * @since 2020-02-22
 */
@Service("tbJdzjfxYhZhService")
public class TbJdzjfxYhZhServiceImpl extends BaseServiceImpl implements TbJdzjfxYhZhService {
	private static final Logger logger = LoggerFactory.getLogger(TbJdzjfxYhZhServiceImpl.class);
    @Autowired
    private TbJdzjfxYhZhMapper tbJdzjfxYhZhMapper;

    /**
     * @see TbJdzjfxYhZhService#insert(TbJdzjfxYhZh tbJdzjfxYhZh)
     */
    @Override
    public int insert(TbJdzjfxYhZh tbJdzjfxYhZh) throws Exception {
    	if (tbJdzjfxYhZh!=null){
	        //tbJdzjfxYhZh.setId(TemplateUtil.genUUID());
	        //menu.setId(getPk("seqName","jgdm","A"));
	        return tbJdzjfxYhZhMapper.insertSelective(tbJdzjfxYhZh);
    	}else{
    		logger.error("TbJdzjfxYhZhServiceImpl.insert时tbJdzjfxYhZh数据为空。");
    		throw new AppRuntimeException("TbJdzjfxYhZhServiceImpl.insert时tbJdzjfxYhZh数据为空。");
    	}        
    }

    /**
     * @see TbJdzjfxYhZhService#delete(String pk)
     */
    @Override
    public int delete(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("TbJdzjfxYhZhServiceImpl.delete时pk为空。");
    		throw new AppRuntimeException("TbJdzjfxYhZhServiceImpl.delete时pk为空。");
    	}else{
    		return tbJdzjfxYhZhMapper.deleteByPrimaryKey(pk);
    	}
    }

    /**
     * @see TbJdzjfxYhZhService#updateByPrimaryKey(TbJdzjfxYhZh tbJdzjfxYhZh)
     */
    @Override
    public int updateByPrimaryKey(TbJdzjfxYhZh tbJdzjfxYhZh) throws Exception {
        if (tbJdzjfxYhZh!=null){
        	if(StringUtils.isBlank(tbJdzjfxYhZh.getId())){
        		logger.error("TbJdzjfxYhZhServiceImpl.updateByPrimaryKey时tbJdzjfxYhZh.Id为空。");
        		throw new AppRuntimeException("TbJdzjfxYhZhServiceImpl.updateByPrimaryKey时tbJdzjfxYhZh.Id为空。");
        	}
	        return tbJdzjfxYhZhMapper.updateByPrimaryKeySelective(tbJdzjfxYhZh);
    	}else{
    		logger.error("TbJdzjfxYhZhServiceImpl.updateByPrimaryKey时tbJdzjfxYhZh数据为空。");
    		throw new AppRuntimeException("TbJdzjfxYhZhServiceImpl.updateByPrimaryKey时tbJdzjfxYhZh数据为空。");
    	}
    }
    /**
     * @see TbJdzjfxYhZhService#queryTbJdzjfxYhZhByPrimaryKey(String pk)
     */
    @Override
    public TbJdzjfxYhZh queryTbJdzjfxYhZhByPrimaryKey(String pk) throws Exception {
    	if(StringUtils.isBlank(pk)){
    		logger.error("TbJdzjfxYhZhServiceImpl.queryTbJdzjfxYhZhByPrimaryKey时pk为空。");
    		throw new AppRuntimeException("TbJdzjfxYhZhServiceImpl.queryTbJdzjfxYhZhByPrimaryKey时pk为空。");
    	}else{
    		return tbJdzjfxYhZhMapper.selectByPrimaryKey(pk);
    	}
    }
    
    
    /**
     * @see TbJdzjfxYhZhService#queryAsObject(TbJdzjfxYhZh tbJdzjfxYhZh)
     */
    @Override
    public TbJdzjfxYhZh queryAsObject(TbJdzjfxYhZh tbJdzjfxYhZh) throws Exception {
        if (tbJdzjfxYhZh!=null){
	        return tbJdzjfxYhZhMapper.selectOne(tbJdzjfxYhZh);
    	}else{
    		logger.error("TbJdzjfxYhZhServiceImpl.queryAsObject时tbJdzjfxYhZh数据为空。");
    		throw new AppRuntimeException("TbJdzjfxYhZhServiceImpl.queryAsObject时tbJdzjfxYhZh数据为空。");
    	}
    }
    
    /**
     * @see TbJdzjfxYhZhService#queryCountByExample(Example example)
     */
    @Override
    public int queryCountByExample(Example example) throws Exception {
    	if(example!=null){
    		return tbJdzjfxYhZhMapper.selectCountByExample(example);
    	}else{
    		logger.error("TbJdzjfxYhZhServiceImpl.queryCountByExample时example数据为空。");
    		throw new AppRuntimeException("TbJdzjfxYhZhServiceImpl.queryCountByExample时example数据为空。");
    	}
    }
    /**
     * @see TbJdzjfxYhZhService#queryListByExample(Example example)
     */
    @Override
    public List<TbJdzjfxYhZh> queryListByExample(Example example) throws Exception {
    	if(example!=null){
    		return tbJdzjfxYhZhMapper.selectByExample(example);
    	}else{
    		logger.error("TbJdzjfxYhZhServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("TbJdzjfxYhZhServiceImpl.queryListByExample时example数据为空。");
    	}
    }
    /**
     * @see TbJdzjfxYhZhService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(tbJdzjfxYhZhMapper.selectByExample(example));
    	}else{
    		logger.error("TbJdzjfxYhZhServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("TbJdzjfxYhZhServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }
	/**
     * @see TbJdzjfxYhZhService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(TbJdzjfxYhZh.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("id")!=null && StringUtils.isNotBlank(parmMap.get("id").toString())){
				criteria.andEqualTo("id",parmMap.get("id").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zh")!=null && StringUtils.isNotBlank(parmMap.get("zh").toString())){
				criteria.andEqualTo("zh",parmMap.get("zh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xm")!=null && StringUtils.isNotBlank(parmMap.get("xm").toString())){
				criteria.andEqualTo("xm",parmMap.get("xm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zjlx")!=null && StringUtils.isNotBlank(parmMap.get("zjlx").toString())){
				criteria.andEqualTo("zjlx",parmMap.get("zjlx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zjhm")!=null && StringUtils.isNotBlank(parmMap.get("zjhm").toString())){
				criteria.andEqualTo("zjhm",parmMap.get("zjhm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jgmc")!=null && StringUtils.isNotBlank(parmMap.get("jgmc").toString())){
				criteria.andEqualTo("jgmc",parmMap.get("jgmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jgdm")!=null && StringUtils.isNotBlank(parmMap.get("jgdm").toString())){
				criteria.andEqualTo("jgdm",parmMap.get("jgdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zzsj")!=null && StringUtils.isNotBlank(parmMap.get("zzsj").toString())){
				criteria.andEqualTo("zzsj",parmMap.get("zzsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zzje")!=null && StringUtils.isNotBlank(parmMap.get("zzje").toString())){
				criteria.andEqualTo("zzje",parmMap.get("zzje").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrr")!=null && StringUtils.isNotBlank(parmMap.get("lrr").toString())){
				criteria.andEqualTo("lrr",parmMap.get("lrr").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrsj")!=null && StringUtils.isNotBlank(parmMap.get("lrsj").toString())){
				criteria.andEqualTo("lrsj",parmMap.get("lrsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ssgx")!=null && StringUtils.isNotBlank(parmMap.get("ssgx").toString())){
				criteria.andEqualTo("ssgx",parmMap.get("ssgx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zhlb")!=null && StringUtils.isNotBlank(parmMap.get("zhlb").toString())){
				criteria.andEqualTo("zhlb",parmMap.get("zhlb").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrrxm")!=null && StringUtils.isNotBlank(parmMap.get("lrrxm").toString())){
				criteria.andEqualTo("lrrxm",parmMap.get("lrrxm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrdw")!=null && StringUtils.isNotBlank(parmMap.get("lrdw").toString())){
				criteria.andEqualTo("lrdw",parmMap.get("lrdw").toString().trim());
				flag = true;
			}
    		if(parmMap.get("lrrdwmc")!=null && StringUtils.isNotBlank(parmMap.get("lrrdwmc").toString())){
				criteria.andEqualTo("lrrdwmc",parmMap.get("lrrdwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xgrxm")!=null && StringUtils.isNotBlank(parmMap.get("xgrxm").toString())){
				criteria.andEqualTo("xgrxm",parmMap.get("xgrxm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xgr")!=null && StringUtils.isNotBlank(parmMap.get("xgr").toString())){
				criteria.andEqualTo("xgr",parmMap.get("xgr").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xgrdwdm")!=null && StringUtils.isNotBlank(parmMap.get("xgrdwdm").toString())){
				criteria.andEqualTo("xgrdwdm",parmMap.get("xgrdwdm").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xgrdwmc")!=null && StringUtils.isNotBlank(parmMap.get("xgrdwmc").toString())){
				criteria.andEqualTo("xgrdwmc",parmMap.get("xgrdwmc").toString().trim());
				flag = true;
			}
    		if(parmMap.get("xgsj")!=null && StringUtils.isNotBlank(parmMap.get("xgsj").toString())){
				criteria.andEqualTo("xgsj",parmMap.get("xgsj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yxx")!=null && StringUtils.isNotBlank(parmMap.get("yxx").toString())){
				criteria.andEqualTo("yxx",parmMap.get("yxx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zzfs")!=null && StringUtils.isNotBlank(parmMap.get("zzfs").toString())){
				criteria.andEqualTo("zzfs",parmMap.get("zzfs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfyzf")!=null && StringUtils.isNotBlank(parmMap.get("sfyzf").toString())){
				criteria.andEqualTo("sfyzf",parmMap.get("sfyzf").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfydj")!=null && StringUtils.isNotBlank(parmMap.get("sfydj").toString())){
				criteria.andEqualTo("sfydj",parmMap.get("sfydj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("ye")!=null && StringUtils.isNotBlank(parmMap.get("ye").toString())){
				criteria.andEqualTo("ye",parmMap.get("ye").toString().trim());
				flag = true;
			}
    		if(parmMap.get("cj")!=null && StringUtils.isNotBlank(parmMap.get("cj").toString())){
				criteria.andEqualTo("cj",parmMap.get("cj").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfys")!=null && StringUtils.isNotBlank(parmMap.get("sfys").toString())){
				criteria.andEqualTo("sfys",parmMap.get("sfys").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zjfh")!=null && StringUtils.isNotBlank(parmMap.get("zjfh").toString())){
				criteria.andEqualTo("zjfh",parmMap.get("zjfh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zhlx")!=null && StringUtils.isNotBlank(parmMap.get("zhlx").toString())){
				criteria.andEqualTo("zhlx",parmMap.get("zhlx").toString().trim());
				flag = true;
			}
    		if(parmMap.get("fhje")!=null && StringUtils.isNotBlank(parmMap.get("fhje").toString())){
				criteria.andEqualTo("fhje",parmMap.get("fhje").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfxf")!=null && StringUtils.isNotBlank(parmMap.get("sfxf").toString())){
				criteria.andEqualTo("sfxf",parmMap.get("sfxf").toString().trim());
				flag = true;
			}
    		if(parmMap.get("sfje")!=null && StringUtils.isNotBlank(parmMap.get("sfje").toString())){
				criteria.andEqualTo("sfje",parmMap.get("sfje").toString().trim());
				flag = true;
			}
    		if(parmMap.get("zjfhzt")!=null && StringUtils.isNotBlank(parmMap.get("zjfhzt").toString())){
				criteria.andEqualTo("zjfhzt",parmMap.get("zjfhzt").toString().trim());
				flag = true;
			}
    		if(parmMap.get("yhksqbh")!=null && StringUtils.isNotBlank(parmMap.get("yhksqbh").toString())){
				criteria.andEqualTo("yhksqbh",parmMap.get("yhksqbh").toString().trim());
				flag = true;
			}
    		if(parmMap.get("kszfbs")!=null && StringUtils.isNotBlank(parmMap.get("kszfbs").toString())){
				criteria.andEqualTo("kszfbs",parmMap.get("kszfbs").toString().trim());
				flag = true;
			}
    		if(parmMap.get("jjbh")!=null && StringUtils.isNotBlank(parmMap.get("jjbh").toString())){
				criteria.andEqualTo("jjbh",parmMap.get("jjbh").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("TbJdzjfxYhZhServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("TbJdzjfxYhZhServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
}
