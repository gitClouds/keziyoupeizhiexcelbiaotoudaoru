package com.unis.service.zhxx;

import java.util.List;
import java.util.Map;

import com.unis.dto.ResultDto;
import com.unis.model.zhxx.TbJdzjfxSfZh;
import tk.mybatis.mapper.entity.Example;
import com.github.pagehelper.PageInfo;
import com.unis.service.BaseService;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;

/**
 * <pre>
 * TODO
 * </pre>
 *
 * @author fdzptwx
 * @version 1.0
 * @since 2020-02-21
 */
public interface TbJdzjfxSfZhService extends BaseService {

    /**
     * 新增TbJdzjfxSfZh实例
     * 
     * @param tbJdzjfxSfZh
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int insert(TbJdzjfxSfZh tbJdzjfxSfZh) throws Exception;

    /**
     * 删除TbJdzjfxSfZh实例
     * 
     * @param pk
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int delete(String pk) throws Exception;

    /**
     * 更新TbJdzjfxSfZh实例
     * 
     * @param tbJdzjfxSfZh
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    int updateByPrimaryKey(TbJdzjfxSfZh tbJdzjfxSfZh) throws Exception;
    /**
     * 根据pk主键获取对象
     * 
     * @param pk
     * @throws Exception
     */
    TbJdzjfxSfZh queryTbJdzjfxSfZhByPrimaryKey(String pk) throws Exception;

    /**
     * 查询TbJdzjfxSfZh实例
     * 
     * @param tbJdzjfxSfZh
     * @throws Exception
     */
    TbJdzjfxSfZh queryAsObject(TbJdzjfxSfZh tbJdzjfxSfZh) throws Exception;
    
    	
	/**
     * 根据条件获取条数
     * 
     * @param example
     * @throws Exception
     */
	int queryCountByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合
     * 
     * @param example
     * @throws Exception
     */
	List<TbJdzjfxSfZh> queryListByExample(Example example) throws Exception;
	/**
     * 根据条件获取数据集合(分页),需在example 设定排序
     * 
     * @param example
     * @param pageNum
     * @param pageSize
     * @throws Exception
     */
	PageInfo queryPageInfoByExample(Example example, int pageNum, int pageSize) throws Exception;

	/**
     * 根据入参获取数据集合(分页)
     * @param parmMap
     * @param pageNum
     * @param pageSize
     * @return
     * @throws Exception
     */
    ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception;
    
    

}
