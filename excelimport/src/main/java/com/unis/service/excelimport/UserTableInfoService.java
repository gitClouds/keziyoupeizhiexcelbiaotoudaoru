package com.unis.service.excelimport;

import com.github.pagehelper.PageInfo;
import com.unis.dto.ResultDto;
import com.unis.model.excelimport.UserTableInfo;
import com.unis.service.BaseService;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * <pre>
 * TODO
 * </pre>
 *
 * @author fdzptwx
 * @version 1.0
 * @since 2020-05-08
 */
public interface UserTableInfoService extends BaseService {

 	/**
     * 根据条件获取数据集合(分页),需在example 设定排序
     * 
     * @param example
     * @param pageNum
     * @param pageSize
     * @throws Exception
     */
	PageInfo queryPageInfoByExample(Example example, int pageNum, int pageSize) throws Exception;

	/**
     * 根据入参获取数据集合(分页)
     * @param parmMap
     * @param pageNum
     * @param pageSize
     * @return
     * @throws Exception
     */
    ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception;
    List<UserTableInfo> queryListAll(UserTableInfo userTableInfo) throws Exception;
	List<Map<String, String>> queryTableCloAndComents(UserTableInfo userTableInfo) throws Exception;
    List<Map<String, String>> queryTableCloAndComentOrder(UserTableInfo userTableInfo) throws Exception;
	List<Map<String, Object>> queryTableDatas(Map<String, Object> map) throws Exception;
	List<UserTableInfo> queryPrimaryKey(UserTableInfo userTableInfo) throws Exception;
    List<Map<String, Object>> queryTableSpaceName(Map<String, Object> map) throws Exception;
	Map<String, Integer>queryTableDatasMax(Map<String, String> map) throws Exception;
	List<Map<String, Object>> queryTableDatasByMap(Map<String, Object> map) throws Exception;
	List<Map<String, Object>> queryAllSqlStatement(Map<String, Object> map) throws Exception;
	Integer queryTableDatasTotalCount(Map<String, Object> map) throws Exception;
	int insertExcelToData(Map<String, Object> map) throws Exception;
    int updateDataToTable(Map<String, Object> map) throws Exception;
    int deleteDataFromTable(Map<String, Object> map) throws Exception;
    int excuteSql(Map<String, Object> map) throws Exception;
	List<Map<String, Object>> excuteSqlReturnMap(Map<String, Object> map) throws Exception;
	List<Map<String, Object>> queryCreateTableInfo(Map<String, String> map) throws Exception;
	List<Map<String, Object>> querySqlTaskBatch(Map<String, String> map) throws Exception;
	List<Map<String, Object>> queryValidTaskData(Map<String, String> map) throws Exception;
	//查询存储过程的方法
	void callProcedure(Map map) throws Exception;
	//查询函数的方法
	void callFunction(Map map)throws Exception;
	int insertAllSqlStatement(Map<String, Object> map) throws Exception;
	int updateSqlTaskSchedule(Map<String, Object> map) throws Exception;
	List<Map<String, String>> querySqlBatchInfoList(Map<String, String> map) throws Exception;

}
