package com.unis.service.excelimport.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.unis.common.exception.app.AppRuntimeException;
import com.unis.dto.ResultDto;
import com.unis.mapper.excelimport.UserTableInfoMapper;
import com.unis.model.excelimport.UserTableInfo;
import com.unis.service.excelimport.UserTableInfoService;
import com.unis.service.impl.BaseServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * <pre>
 * @see UserTableInfoService
 * </pre>
 *
 * @author fdzptwx
 * @version 1.0
 * @since 2020-05-08
 */
@Service("userTableInfoService")
public class UserTableInfoServiceImpl extends BaseServiceImpl implements UserTableInfoService {
	private static final Logger logger = LoggerFactory.getLogger(UserTableInfoServiceImpl.class);
    @Autowired
    private UserTableInfoMapper userTableInfoMapper;

    /**
     * @see UserTableInfoService#queryPageInfoByExample(Example example,int pageNum,int pageSize)
     */
    @Override
    public PageInfo queryPageInfoByExample(Example example,int pageNum,int pageSize) throws Exception {    	
    	if(example!=null){
    		PageHelper.startPage(pageNum,pageSize);
    		return new PageInfo(userTableInfoMapper.selectByExample(example));
    	}else{
    		logger.error("UserTableInfoServiceImpl.queryListByExample时example数据为空。");
    		throw new AppRuntimeException("UserTableInfoServiceImpl.queryListByExample时example数据为空。");
    	}    	
    }



	@Override
	public List<UserTableInfo> queryListAll(UserTableInfo userTableInfo) throws Exception{
    	return userTableInfoMapper.select(userTableInfo);
	}
	@Override
	public List<Map<String, String>> queryTableCloAndComents(UserTableInfo userTableInfo) throws Exception{
    	return userTableInfoMapper.queryTableCloAndComents(userTableInfo);
	};
    @Override
	public List<Map<String, String>> queryTableCloAndComentOrder(UserTableInfo userTableInfo) throws Exception{
    	return userTableInfoMapper.queryTableCloAndComentOrder(userTableInfo);
	}
	@Override
	public List<Map<String, Object>> queryTableDatas(Map<String,Object> map) throws Exception{
		return userTableInfoMapper.queryTableDatas(map);
	}
	@Override
	public List<Map<String, Object>> queryTableSpaceName(Map<String, Object> map) throws Exception{
		return userTableInfoMapper.queryTableSpaceName(map);
	}
	@Override
	public List<UserTableInfo> queryPrimaryKey(UserTableInfo userTableInfo) throws Exception{
		return userTableInfoMapper.queryPrimaryKey(userTableInfo);
	}
	@Override
	public List<Map<String, Object>> queryCreateTableInfo(Map<String, String> map) throws Exception{
    	return userTableInfoMapper.queryCreateTableInfo(map);
	}
	@Override
	public List<Map<String, Object>> querySqlTaskBatch(Map<String, String> map) throws Exception{
    	return userTableInfoMapper.querySqlTaskBatch(map);
	}
    @Override
	public int insertExcelToData(Map<String, Object> map) throws Exception{
    	return userTableInfoMapper.insertExcelToData(map);
	}
	@Override
	public Map<String, Integer>queryTableDatasMax(Map<String, String> map) throws Exception{
		return userTableInfoMapper.queryTableDatasMax(map);
	};
	@Override
	public List<Map<String, Object>> queryTableDatasByMap(Map<String, Object> map) throws Exception{
		return userTableInfoMapper.queryTableDatasByMap(map);
	};
	@Override
	public List<Map<String, Object>> queryValidTaskData(Map<String, String> map) throws Exception{
		return userTableInfoMapper.queryValidTaskData(map);
	}
	@Override
	public Integer queryTableDatasTotalCount(Map<String, Object> map) throws Exception{
		return userTableInfoMapper.queryTableDatasTotalCount(map);
	}
	@Override
	public List<Map<String, Object>> queryAllSqlStatement(Map<String, Object> map) throws Exception{
		return userTableInfoMapper.queryAllSqlStatement(map);
	}
	@Override
	public int updateDataToTable(Map<String, Object> map) throws Exception{
		return updateDataToTable(map);
	}
	@Override
	public int deleteDataFromTable(Map<String, Object> map) throws Exception{
		return deleteDataFromTable(map);
	}
	@Override
	public int excuteSql(Map<String, Object> map) throws Exception{
		return excuteSql(map);
	}
	/**
     * @see UserTableInfoService#queryListByPage(Map parmMap,int pageNum,int pageSize)
     */
    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception{
    	if (parmMap!=null){
    		Example example = new Example(UserTableInfo.class);
    		//example.setOrderByClause("rksj DESC,pk asc");设置排序
    		Example.Criteria criteria = example.createCriteria();
    		//criteria设置
    		// = --> andEqualTo(field,value)
    		// like --> andLike(field,likeValue)；likeValue 包含‘%’
    		// is null --> andIsNull(field)
    		// is not null --> andIsNotNull(field)
    		// <> --> andNotEqualTo(field)
    		// > --> andGreaterThan(field,value)
    		// >= --> andGreaterThanOrEqualTo(field,value)
    		// < --> andLessThan(field,value)
    		// <= --> andLessThanOrEqualTo(field,value)
    		// in --> andIn(field,Iterable value)
    		// not in --> andNotIn(field,Iterable value)
    		// between --> andBetween(field,beginValue,endValue)
    		// not like --> andNotLike(field,likeValue)；likeValue 包含‘%’
    		
    		// or --> 上述方法的and 都有与之对应的or方法 eg: orEqualTo(field,value)
    		
    		//criteria.andEqualTo("yxx",1);设置yxx=1
    		/**
    		 *此方法请根据需要修改下方条件
    		*/
    		//此查询仅针对单表查询，若需要多表查询，尽量使用配置Mapper.xml 和Mapper.java的方式
    		//生成条件中均为 and field = value 
    		//生成代码中，若表结构中有_等字符，生成字段可能会与实体类不同，请注意修改
    		
    		boolean flag = false;
    		if(parmMap.get("id")!=null && StringUtils.isNotBlank(parmMap.get("id").toString())){
				criteria.andEqualTo("id",parmMap.get("id").toString().trim());
				flag = true;
			}
    		if(parmMap.get("table_name")!=null && StringUtils.isNotBlank(parmMap.get("table_name").toString())){
				criteria.andEqualTo("table_name",parmMap.get("table_name").toString().trim());
				flag = true;
			}
    		if(parmMap.get("table_type")!=null && StringUtils.isNotBlank(parmMap.get("table_type").toString())){
				criteria.andEqualTo("table_type",parmMap.get("table_type").toString().trim());
				flag = true;
			}
    		if(parmMap.get("comments")!=null && StringUtils.isNotBlank(parmMap.get("comments").toString())){
				criteria.andEqualTo("comments",parmMap.get("comments").toString().trim());
				flag = true;
			}
    		//一个条件都没有的时候给一个默认条件
    		if(!flag){
    		
    		}
    		PageInfo pageInfo = this.queryPageInfoByExample(example,pageNum,pageSize);
			ResultDto result = new ResultDto();
			result.setRows(pageInfo.getList());
			result.setPage(pageInfo.getPageNum());
			//result.setTotal(pageInfo.getPages());
			result.setTotal((int)pageInfo.getTotal());
			return result;
    	}else{
			logger.error("UserTableInfoServiceImpl.queryListByPage时parmMap数据为空。");
			throw new AppRuntimeException("UserTableInfoServiceImpl.queryListByPage时parmMap数据为空。");
		}
    }
    @Override
	public List<Map<String, Object>> excuteSqlReturnMap(Map<String, Object> map) throws Exception{
    	return userTableInfoMapper.excuteSqlReturnMap(map);
	}
	//查询存储过程的方法
	@Override
	public void callProcedure(Map map) throws Exception{
    	userTableInfoMapper.callProcedure(map);
	}
	//查询函数的方法
	@Override
	public void callFunction(Map map) throws Exception{
    	userTableInfoMapper.callFunction(map);
	}

	public int insertAllSqlStatement(Map<String, Object> map) throws Exception{
    	return userTableInfoMapper.insertAllSqlStatement(map);
	}

	@Override
	public int updateSqlTaskSchedule(Map<String, Object> map) throws Exception{
    	return userTableInfoMapper.updateSqlTaskSchedule(map);
	}

	@Override
	public List<Map<String, String>> querySqlBatchInfoList(Map<String, String> map) throws Exception{
    	return userTableInfoMapper.querySqlBatchInfoList(map);
	}

}
