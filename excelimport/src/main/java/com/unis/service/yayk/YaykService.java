package com.unis.service.yayk;

import com.unis.dto.ResultDto;
import com.unis.model.znzf.SfZtQq;
import com.unis.model.znzf.YhkZtQq;
import com.unis.service.BaseService;

import java.util.Map;

/**
 * Created by xuk
 * on 2019/3/20/020.
 */
public interface YaykService extends BaseService {
    /**
     * 根据入参获取数据集合(分页)
     * @param parmMap
     * @param pageNum
     * @param pageSize
     * @return
     * @throws Exception
     */
    ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws  Exception;

	void ztQqSend(YhkZtQq yhkZtQq, SfZtQq sfZtQq)  throws Exception ;


}
