package com.unis.service.yayk.impl;

import com.github.pagehelper.PageInfo;
import com.unis.common.enums.QqlxEnum;
import com.unis.common.exception.app.AppRuntimeException;
import com.unis.common.util.TimeUtil;
import com.unis.dto.ResultDto;
import com.unis.mapper.znzf.SfZtJgMapper;
import com.unis.mapper.znzf.SfZtQqMapper;
import com.unis.mapper.znzf.YhkZtJgMapper;
import com.unis.mapper.znzf.YhkZtQqMapper;
import com.unis.model.fzzx.Jjd;
import com.unis.model.znzf.SfZtQq;
import com.unis.model.znzf.YhkZtQq;
import com.unis.model.znzf.ZnzfQueue;
import com.unis.service.fzzx.JjdService;
import com.unis.service.impl.BaseServiceImpl;
import com.unis.service.yayk.YaykService;
import com.unis.service.zhxx.TbJdzjfxSfZhService;
import com.unis.service.znzf.SfZtJgService;
import com.unis.service.znzf.SfZtQqService;
import com.unis.service.znzf.YhkZtJgService;
import com.unis.service.znzf.YhkZtQqService;
import com.unis.service.znzf.ZnzfQueueService;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.Map;

/**
 * Created by Administrator on 2019/3/20/020.
 */
@Service("yaykService")
public class YaykServiceImpl extends BaseServiceImpl implements YaykService {
    private static final Logger logger = LoggerFactory.getLogger(YaykServiceImpl.class);
    @Autowired
    private TbJdzjfxSfZhService tbJdzjfxSfZhService;
    @Autowired
    private YhkZtJgMapper yhkZtJgMapper;
    @Autowired
    private SfZtJgMapper sfZtJgMapper;
    @Autowired
    private YhkZtQqMapper yhkZtQqMapper;
    @Autowired
    private SfZtQqMapper sfZtQqMapper;
    @Autowired
    private ZnzfQueueService znzfQueueService;

    @Override
    public ResultDto queryListByPage(Map parmMap, int pageNum, int pageSize) throws Exception {
        if (parmMap!=null){
        	 parmMap.put("fxbs", 2);
        	 parmMap.put("px", "FXSJ");
        	 parmMap.put("pclrr", "440400");
        	 ResultDto page = tbJdzjfxSfZhService.queryListByPage(parmMap,pageNum,pageSize);
             return page;
        }else{
            logger.error("YaykServiceImpl.queryListByPage时parmMap数据为空。");
            throw new AppRuntimeException("YaykServiceImpl.queryListByPage时parmMap数据为空。");
        }
    }

	@Override
	public void ztQqSend(YhkZtQq yhkZtQq, SfZtQq sfZtQq) throws Exception {
		if(yhkZtQq != null) {
			ZnzfQueue znzfQueue =new ZnzfQueue();
			znzfQueue.setJjdpk(yhkZtQq.getJjdpk());
			znzfQueue.setSerpk(yhkZtQq.getPk());
			znzfQueue.setSertype(QqlxEnum.yhkQqlxZtcx.getKey());
			znzfQueue.setBankcode(yhkZtQq.getBankcode());
			znzfQueue.setBankname(yhkZtQq.getBankname());
			znzfQueue.setCardnumber(yhkZtQq.getCardnumber());
			znzfQueueService.insert(znzfQueue);
			yhkZtJgMapper.deleteByApplicationid(yhkZtQq.getApplicationid());
			yhkZtQqMapper.updateBypk(yhkZtQq.getPk());
			
		}
		if(sfZtQq != null) {
			ZnzfQueue znzfQueue =new ZnzfQueue();
			znzfQueue.setJjdpk(sfZtQq.getJjdpk());
			znzfQueue.setSerpk(sfZtQq.getPk());
			znzfQueue.setSertype(QqlxEnum.sfQqlxZtcx.getKey());
			znzfQueue.setBankcode(sfZtQq.getPaycode());
			znzfQueue.setBankname(sfZtQq.getPayname());
			znzfQueue.setCardnumber(sfZtQq.getAccnumber());
			znzfQueueService.insert(znzfQueue);
			sfZtJgMapper.deleteByApplicationid(sfZtQq.getApplicationid());
			sfZtQqMapper.updateBypk(sfZtQq.getPk());
		}
	}
}
