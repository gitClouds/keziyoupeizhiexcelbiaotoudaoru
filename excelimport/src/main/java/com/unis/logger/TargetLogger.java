package com.unis.logger;

import java.lang.annotation.*;

/**
 * Created by xuk on 2019/5/22.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({
        ElementType.METHOD, ElementType.PARAMETER
})
@Documented
public @interface TargetLogger {
    String value();
}
