package com.unis.model.wxjyyjmx;

import java.util.Date;
import javax.persistence.*;
import com.unis.common.annotation.ESField;
import com.unis.common.annotation.ESTable;
/**
* 我方支付账号交易次数
*
* @author Qx
* @version 1.0
* @since 2020-05-12
*/
@Table(name = "V_ZH_WFZFZHJYCS_TB")
@ESTable(name = "vzhwfzfzhjycs",text = "我方支付账号交易次数")
public class VZhWfzfZhjycs  {

private static final long serialVersionUID = 1L;

/****************************************
* Basic fields
****************************************/
    /**
    * 我方支付账号: VARCHAR
    */
    @Column(name = "WFZFZH")
    @ESField(text = "我方支付账号")
    private String wfzfzh;

    /**
    * 交易次数: DECIMAL
    */
    @Column(name = "JYCS")
    @ESField(text = "交易次数")
    private Integer jycs;

    public void setWfzfzh(String wfzfzh) {
    this.wfzfzh = wfzfzh;
    }

    public String getWfzfzh() {
    return wfzfzh;
    }

    public void setJycs(Integer jycs) {
    this.jycs = jycs;
    }

    public Integer getJycs() {
    return jycs;
    }

}
