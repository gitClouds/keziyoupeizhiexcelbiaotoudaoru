package com.unis.model.wxjyyjmx;

import com.unis.common.annotation.ESField;
import com.unis.common.annotation.ESTable;

import javax.persistence.Column;
import javax.persistence.Table;

/**
* 对方支付账号上家统计新
*
* @author Qx
* @version 1.0
* @since 2020-05-12
*/
@ESTable(name = "vzhdfzfzhsjtjx",text = "对方支付账号上家统计新")
@Table(name = "V_ZH_Dfzfzhsjtjx_TB")
public class VZhDfzfzhsjtjx  {

private static final long serialVersionUID = 1L;

/****************************************
* Basic fields
****************************************/
    /**
    * 对方支付账号《入账》: VARCHAR
    */
    @Column(name = "DF_ZFZH_RZ")
    @ESField(text = "对方支付账号《入账》")
    private String df_zfzh_rz;

    /**
    * 对方上家: VARCHAR
    */
    @Column(name = "DFSJ")
    @ESField(text = "对方上家")
    private String dfsj;

    public void setDf_zfzh_rz(String df_zfzh_rz) {
    this.df_zfzh_rz = df_zfzh_rz;
    }

    public String getDf_zfzh_rz() {
    return df_zfzh_rz;
    }

    public void setDfsj(String dfsj) {
    this.dfsj = dfsj;
    }

    public String getDfsj() {
    return dfsj;
    }

}
