package com.unis.model.wxjyyjmx;

import com.unis.common.annotation.ESField;
import com.unis.common.annotation.ESTable;

import javax.persistence.Column;
import javax.persistence.Table;

/**
* 我方出入账聚合统计
*
* @author Qx
* @version 1.0
* @since 2020-05-12
*/
@ESTable(name = "vzhwfcrzjhtj",text = "我方出入账聚合统计")
@Table(name = "V_ZH_Wf_Crzjh_Tj_TB")
public class VZhWfCrzjhTj  {

private static final long serialVersionUID = 1L;

/****************************************
* Basic fields
****************************************/
    /**
    * 我方支付账户: VARCHAR
    */
    @Column(name = "WFZFZH")
    @ESField(text = "我方支付账户")
    private String wfzfzh;

    /**
    * 《入账》我方微信号: VARCHAR
    */
    @Column(name = "RZ_WF_WXH")
    @ESField(text = "《入账》我方微信号")
    private String rz_wf_wxh;

    /**
    * 《入账》我方微信账号新1: VARCHAR
    */
    @Column(name = "RZ_WF_WXZH_X1")
    @ESField(text = "《入账》我方微信账号新1")
    private String rz_wf_wxzh_x1;

    /**
    * 《入账》我方高危户籍: DECIMAL
    */
    @Column(name = "RZ_WF_GWHJ")
    @ESField(text = "《入账》我方高危户籍")
    private Integer rz_wf_gwhj;

    /**
    * 《入账》交易对手数: DECIMAL
    */
    @Column(name = "RZ_JYDSS")
    @ESField(text = "《入账》交易对手数")
    private Integer rz_jydss;

    /**
    * 《入账》交易次数: DECIMAL
    */
    @Column(name = "RZ_JYCS")
    @ESField(text = "《入账》交易次数")
    private Integer rz_jycs;

    /**
    * 《入账》交易金额: DECIMAL
    */
    @Column(name = "RZ_JYJE")
    @ESField(text = "《入账》交易金额")
    private Integer rz_jyje;

    /**
    * 《入账》涉毒前科金额: DECIMAL
    */
    @Column(name = "RZ_SDQKJE")
    @ESField(text = "《入账》涉毒前科金额")
    private Integer rz_sdqkje;

    /**
    * 《入账》涉赌前科金额: DECIMAL
    */
    @Column(name = "RZ_SDQKJE1")
    @ESField(text = "《入账》涉赌前科金额")
    private Integer rz_sdqkje1;

    /**
    * 《入账》异地交易对手数: DECIMAL
    */
    @Column(name = "RZ_YD_JYDSS")
    @ESField(text = "《入账》异地交易对手数")
    private Integer rz_yd_jydss;

    /**
    * 《入账》对方涉毒证件号码人员表: DECIMAL
    */
    @Column(name = "RZ_DF_SDZJHM_RYB")
    @ESField(text = "《入账》对方涉毒证件号码人员表")
    private Integer rz_df_sdzjhm_ryb;

    /**
    * 《入账》敏感时间段: VARCHAR
    */
    @Column(name = "RZ_MGSJL")
    @ESField(text = "《入账》敏感时间段")
    private String rz_mgsjl;

    /**
    * 《入账》整数交易率: VARCHAR
    */
    @Column(name = "RZ_ZSJYL")
    @ESField(text = "《入账》整数交易率")
    private String rz_zsjyl;

    /**
    * 《入账》涉毒人数: DECIMAL
    */
    @Column(name = "RZ_SDRS")
    @ESField(text = "《入账》涉毒人数")
    private Integer rz_sdrs;

    /**
    * 《入账》对方证件号码微信关联人: DECIMAL
    */
    @Column(name = "RZ_DF_ZJHM_WXGLR")
    @ESField(text = "《入账》对方证件号码微信关联人")
    private Integer rz_df_zjhm_wxglr;

    /**
    * 《出账》我方微信账号新1: VARCHAR
    */
    @Column(name = "CZ_WF_WXZH_X1")
    @ESField(text = "《出账》我方微信账号新1")
    private String cz_wf_wxzh_x1;

    /**
    * 《出账》我方高危户籍: DECIMAL
    */
    @Column(name = "CZ_WF_GWHJ")
    @ESField(text = "《出账》我方高危户籍")
    private Integer cz_wf_gwhj;

    /**
    * 《出账》交易对手数: DECIMAL
    */
    @Column(name = "CZ_JYDSS")
    @ESField(text = "《出账》交易对手数")
    private Integer cz_jydss;

    /**
    * 《出账》交易次数: DECIMAL
    */
    @Column(name = "CZ_JYCS")
    @ESField(text = "《出账》交易次数")
    private Integer cz_jycs;

    /**
    * 《出账》交易金额: DECIMAL
    */
    @Column(name = "CZ_JYJE")
    @ESField(text = "《出账》交易金额")
    private Integer cz_jyje;

    /**
    * 《出账》涉毒前科金额: DECIMAL
    */
    @Column(name = "CZ_SDQKJE")
    @ESField(text = "《出账》涉毒前科金额")
    private Integer cz_sdqkje;

    /**
    * 《出账》涉赌前科金额: DECIMAL
    */
    @Column(name = "CZ_SDQKJE1")
    @ESField(text = "《出账》涉赌前科金额")
    private Integer cz_sdqkje1;

    /**
    * 《出账》异地交易对手数: DECIMAL
    */
    @Column(name = "CZ_YD_JYDSS")
    @ESField(text = "《出账》异地交易对手数")
    private Integer cz_yd_jydss;

    /**
    * 《出账》对方涉毒证件号码人员表: DECIMAL
    */
    @Column(name = "CZ_DF_SDZJHM_RYB")
    @ESField(text = "《出账》对方涉毒证件号码人员表")
    private Integer cz_df_sdzjhm_ryb;

    /**
    * 《出账》敏感时间段: VARCHAR
    */
    @Column(name = "CZ_MGSJL")
    @ESField(text = "《出账》敏感时间段")
    private String cz_mgsjl;

    /**
    * 《出账》整数交易率: VARCHAR
    */
    @Column(name = "CZ_ZSJYL")
    @ESField(text = "《出账》整数交易率")
    private String cz_zsjyl;

    /**
    * 《出账》涉毒人数: DECIMAL
    */
    @Column(name = "CZ_SDRS")
    @ESField(text = "《出账》涉毒人数")
    private Integer cz_sdrs;

    /**
    * 《出账》对方证件号码微信关联人: DECIMAL
    */
    @Column(name = "CZ_DF_ZJHM_WXGLR")
    @ESField(text = "《出账》对方证件号码微信关联人")
    private Integer cz_df_zjhm_wxglr;

    /**
    * 总交易次数: DECIMAL
    */
    @Column(name = "ZJYCS")
    @ESField(text = "总交易次数")
    private Integer zjycs;

    /**
    * 总交易对手: DECIMAL
    */
    @Column(name = "ZJYDS")
    @ESField(text = "总交易对手")
    private Integer zjyds;

    /**
    * 总交易金额: DECIMAL
    */
    @Column(name = "ZJYJE")
    @ESField(text = "总交易金额")
    private Integer zjyje;

    /**
    * 已调交易对手: DECIMAL
    */
    @Column(name = "YDJYDS")
    @ESField(text = "已调交易对手")
    private Integer ydjyds;

    /**
    * 对方涉毒证件号码_人员表: DECIMAL
    */
    @Column(name = "DF_SDZJHM_RYB")
    @ESField(text = "对方涉毒证件号码_人员表")
    private Integer df_sdzjhm_ryb;

    /**
    * 对方证件号码_微信关联人: DECIMAL
    */
    @Column(name = "DF_ZJHM_WXGLR")
    @ESField(text = "对方证件号码_微信关联人")
    private Integer df_zjhm_wxglr;

    /**
    * 我方微信账号新1: VARCHAR
    */
    @Column(name = "WF_WXZH_X1")
    @ESField(text = "我方微信账号新1")
    private String wf_wxzh_x1;

    /**
    * 我方高危户籍: DECIMAL
    */
    @Column(name = "WF_GWHJ")
    @ESField(text = "我方高危户籍")
    private Integer wf_gwhj;

    public void setWfzfzh(String wfzfzh) {
    this.wfzfzh = wfzfzh;
    }

    public String getWfzfzh() {
    return wfzfzh;
    }

    public void setRz_wf_wxh(String rz_wf_wxh) {
    this.rz_wf_wxh = rz_wf_wxh;
    }

    public String getRz_wf_wxh() {
    return rz_wf_wxh;
    }

    public void setRz_wf_wxzh_x1(String rz_wf_wxzh_x1) {
    this.rz_wf_wxzh_x1 = rz_wf_wxzh_x1;
    }

    public String getRz_wf_wxzh_x1() {
    return rz_wf_wxzh_x1;
    }

    public void setRz_wf_gwhj(Integer rz_wf_gwhj) {
    this.rz_wf_gwhj = rz_wf_gwhj;
    }

    public Integer getRz_wf_gwhj() {
    return rz_wf_gwhj;
    }

    public void setRz_jydss(Integer rz_jydss) {
    this.rz_jydss = rz_jydss;
    }

    public Integer getRz_jydss() {
    return rz_jydss;
    }

    public void setRz_jycs(Integer rz_jycs) {
    this.rz_jycs = rz_jycs;
    }

    public Integer getRz_jycs() {
    return rz_jycs;
    }

    public void setRz_jyje(Integer rz_jyje) {
    this.rz_jyje = rz_jyje;
    }

    public Integer getRz_jyje() {
    return rz_jyje;
    }

    public void setRz_sdqkje(Integer rz_sdqkje) {
    this.rz_sdqkje = rz_sdqkje;
    }

    public Integer getRz_sdqkje() {
    return rz_sdqkje;
    }

    public void setRz_sdqkje1(Integer rz_sdqkje1) {
    this.rz_sdqkje1 = rz_sdqkje1;
    }

    public Integer getRz_sdqkje1() {
    return rz_sdqkje1;
    }

    public void setRz_yd_jydss(Integer rz_yd_jydss) {
    this.rz_yd_jydss = rz_yd_jydss;
    }

    public Integer getRz_yd_jydss() {
    return rz_yd_jydss;
    }

    public void setRz_df_sdzjhm_ryb(Integer rz_df_sdzjhm_ryb) {
    this.rz_df_sdzjhm_ryb = rz_df_sdzjhm_ryb;
    }

    public Integer getRz_df_sdzjhm_ryb() {
    return rz_df_sdzjhm_ryb;
    }

    public void setRz_mgsjl(String rz_mgsjl) {
    this.rz_mgsjl = rz_mgsjl;
    }

    public String getRz_mgsjl() {
    return rz_mgsjl;
    }

    public void setRz_zsjyl(String rz_zsjyl) {
    this.rz_zsjyl = rz_zsjyl;
    }

    public String getRz_zsjyl() {
    return rz_zsjyl;
    }

    public void setRz_sdrs(Integer rz_sdrs) {
    this.rz_sdrs = rz_sdrs;
    }

    public Integer getRz_sdrs() {
    return rz_sdrs;
    }

    public void setRz_df_zjhm_wxglr(Integer rz_df_zjhm_wxglr) {
    this.rz_df_zjhm_wxglr = rz_df_zjhm_wxglr;
    }

    public Integer getRz_df_zjhm_wxglr() {
    return rz_df_zjhm_wxglr;
    }

    public void setCz_wf_wxzh_x1(String cz_wf_wxzh_x1) {
    this.cz_wf_wxzh_x1 = cz_wf_wxzh_x1;
    }

    public String getCz_wf_wxzh_x1() {
    return cz_wf_wxzh_x1;
    }

    public void setCz_wf_gwhj(Integer cz_wf_gwhj) {
    this.cz_wf_gwhj = cz_wf_gwhj;
    }

    public Integer getCz_wf_gwhj() {
    return cz_wf_gwhj;
    }

    public void setCz_jydss(Integer cz_jydss) {
    this.cz_jydss = cz_jydss;
    }

    public Integer getCz_jydss() {
    return cz_jydss;
    }

    public void setCz_jycs(Integer cz_jycs) {
    this.cz_jycs = cz_jycs;
    }

    public Integer getCz_jycs() {
    return cz_jycs;
    }

    public void setCz_jyje(Integer cz_jyje) {
    this.cz_jyje = cz_jyje;
    }

    public Integer getCz_jyje() {
    return cz_jyje;
    }

    public void setCz_sdqkje(Integer cz_sdqkje) {
    this.cz_sdqkje = cz_sdqkje;
    }

    public Integer getCz_sdqkje() {
    return cz_sdqkje;
    }

    public void setCz_sdqkje1(Integer cz_sdqkje1) {
    this.cz_sdqkje1 = cz_sdqkje1;
    }

    public Integer getCz_sdqkje1() {
    return cz_sdqkje1;
    }

    public void setCz_yd_jydss(Integer cz_yd_jydss) {
    this.cz_yd_jydss = cz_yd_jydss;
    }

    public Integer getCz_yd_jydss() {
    return cz_yd_jydss;
    }

    public void setCz_df_sdzjhm_ryb(Integer cz_df_sdzjhm_ryb) {
    this.cz_df_sdzjhm_ryb = cz_df_sdzjhm_ryb;
    }

    public Integer getCz_df_sdzjhm_ryb() {
    return cz_df_sdzjhm_ryb;
    }

    public void setCz_mgsjl(String cz_mgsjl) {
    this.cz_mgsjl = cz_mgsjl;
    }

    public String getCz_mgsjl() {
    return cz_mgsjl;
    }

    public void setCz_zsjyl(String cz_zsjyl) {
    this.cz_zsjyl = cz_zsjyl;
    }

    public String getCz_zsjyl() {
    return cz_zsjyl;
    }

    public void setCz_sdrs(Integer cz_sdrs) {
    this.cz_sdrs = cz_sdrs;
    }

    public Integer getCz_sdrs() {
    return cz_sdrs;
    }

    public void setCz_df_zjhm_wxglr(Integer cz_df_zjhm_wxglr) {
    this.cz_df_zjhm_wxglr = cz_df_zjhm_wxglr;
    }

    public Integer getCz_df_zjhm_wxglr() {
    return cz_df_zjhm_wxglr;
    }

    public void setZjycs(Integer zjycs) {
    this.zjycs = zjycs;
    }

    public Integer getZjycs() {
    return zjycs;
    }

    public void setZjyds(Integer zjyds) {
    this.zjyds = zjyds;
    }

    public Integer getZjyds() {
    return zjyds;
    }

    public void setZjyje(Integer zjyje) {
    this.zjyje = zjyje;
    }

    public Integer getZjyje() {
    return zjyje;
    }

    public void setYdjyds(Integer ydjyds) {
    this.ydjyds = ydjyds;
    }

    public Integer getYdjyds() {
    return ydjyds;
    }

    public void setDf_sdzjhm_ryb(Integer df_sdzjhm_ryb) {
    this.df_sdzjhm_ryb = df_sdzjhm_ryb;
    }

    public Integer getDf_sdzjhm_ryb() {
    return df_sdzjhm_ryb;
    }

    public void setDf_zjhm_wxglr(Integer df_zjhm_wxglr) {
    this.df_zjhm_wxglr = df_zjhm_wxglr;
    }

    public Integer getDf_zjhm_wxglr() {
    return df_zjhm_wxglr;
    }

    public void setWf_wxzh_x1(String wf_wxzh_x1) {
    this.wf_wxzh_x1 = wf_wxzh_x1;
    }

    public String getWf_wxzh_x1() {
    return wf_wxzh_x1;
    }

    public void setWf_gwhj(Integer wf_gwhj) {
    this.wf_gwhj = wf_gwhj;
    }

    public Integer getWf_gwhj() {
    return wf_gwhj;
    }

}
