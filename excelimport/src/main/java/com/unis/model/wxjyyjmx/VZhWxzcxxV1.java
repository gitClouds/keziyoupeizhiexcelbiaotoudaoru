package com.unis.model.wxjyyjmx;

import java.util.Date;
import javax.persistence.*;
import com.unis.common.annotation.ESField;
import com.unis.common.annotation.ESTable;
/**
* 微信注册信息V1.0
*
* @author Qx
* @version 1.0
* @since 2020-05-12
*/
@ESTable(name = "vzhwxzcxxv1",text = "微信注册信息V1.0")
public class VZhWxzcxxV1  {

private static final long serialVersionUID = 1L;

/****************************************
* Basic fields
****************************************/
    /**
    * 绑定银行卡: VARCHAR
    */
    @Column(name = "BDYHK")
    @ESField(text = "绑定银行卡")
    private String bdyhk;

    /**
    * 备注: VARCHAR
    */
    @Column(name = "BZ")
    @ESField(text = "备注")
    private String bz;

    /**
    * 开户主体或证件号码: VARCHAR
    */
    @Column(name = "KHZTZJHM")
    @ESField(text = "开户主体或证件号码")
    private String khztzjhm;

    /**
    * 证件有效期: VARCHAR
    */
    @Column(name = "ZJYXQ")
    @ESField(text = "证件有效期")
    private String zjyxq;

    /**
    * 查询反馈说明: VARCHAR
    */
    @Column(name = "CXFKSM")
    @ESField(text = "查询反馈说明")
    private String cxfksm;

    /**
    * 反馈机构名称: VARCHAR
    */
    @Column(name = "FKJGMC")
    @ESField(text = "反馈机构名称")
    private String fkjgmc;

    /**
    * 查询账号: VARCHAR
    */
    @Column(name = "CXZH")
    @ESField(text = "查询账号")
    private String cxzh;

    /**
    * 微信号: CHAR
    */
    @Column(name = "WXH")
    @ESField(text = "微信号")
    private String wxh;

    /**
    * 商户号码: VARCHAR
    */
    @Column(name = "SHHM")
    @ESField(text = "商户号码")
    private String shhm;

    /**
    * 来源: CHAR
    */
    @Column(name = "LY")
    @ESField(text = "来源")
    private String ly;

    /**
    * 来源分局: CHAR
    */
    @Column(name = "LYFJ")
    @ESField(text = "来源分局")
    private String lyfj;

    /**
    * 开户主体姓名: VARCHAR
    */
    @Column(name = "KHZTXM")
    @ESField(text = "开户主体姓名")
    private String khztxm;

    /**
    * 商户名称: VARCHAR
    */
    @Column(name = "SHMC")
    @ESField(text = "商户名称")
    private String shmc;

    /**
    * 开户主体证件类型: VARCHAR
    */
    @Column(name = "KHZTZJLX")
    @ESField(text = "开户主体证件类型")
    private String khztzjlx;

    /**
    * qq号: CHAR
    */
    @Column(name = "QQH")
    @ESField(text = "qq号")
    private String qqh;

    /**
    * 绑定手机号: VARCHAR
    */
    @Column(name = "BDSJH")
    @ESField(text = "绑定手机号")
    private String bdsjh;

    /**
    * 时间: DECIMAL
    */
    @Column(name = "SJ")
    @ESField(text = "时间")
    private Integer sj;

    /**
    * 注册信息录入时间: DATE
    */
    @Column(name = "ZCXXLRSJ")
    @ESField(text = "注册信息录入时间")
    private Date zcxxlrsj;

    /**
    * 查询账号是否是身份证: DECIMAL
    */
    @Column(name = "CXZH_SFSSFZ")
    @ESField(text = "查询账号是否是身份证")
    private Integer cxzh_sfssfz;

    /**
    * 微信账号新: VARCHAR
    */
    @Column(name = "WXZH_X")
    @ESField(text = "微信账号新")
    private String wxzh_x;

    /**
    * 去重字段新: DECIMAL
    */
    @Column(name = "QCZD_X")
    @ESField(text = "去重字段新")
    private Integer qczd_x;

    /**
    * 身份证件前6位: VARCHAR
    */
    @Column(name = "SFZJ6")
    @ESField(text = "身份证件前6位")
    private String sfzj6;

    /**
    * 高危户籍: DECIMAL
    */
    @Column(name = "GWHJ")
    @ESField(text = "高危户籍")
    private Integer gwhj;

    public void setBdyhk(String bdyhk) {
    this.bdyhk = bdyhk;
    }

    public String getBdyhk() {
    return bdyhk;
    }

    public void setBz(String bz) {
    this.bz = bz;
    }

    public String getBz() {
    return bz;
    }

    public void setKhztzjhm(String khztzjhm) {
    this.khztzjhm = khztzjhm;
    }

    public String getKhztzjhm() {
    return khztzjhm;
    }

    public void setZjyxq(String zjyxq) {
    this.zjyxq = zjyxq;
    }

    public String getZjyxq() {
    return zjyxq;
    }

    public void setCxfksm(String cxfksm) {
    this.cxfksm = cxfksm;
    }

    public String getCxfksm() {
    return cxfksm;
    }

    public void setFkjgmc(String fkjgmc) {
    this.fkjgmc = fkjgmc;
    }

    public String getFkjgmc() {
    return fkjgmc;
    }

    public void setCxzh(String cxzh) {
    this.cxzh = cxzh;
    }

    public String getCxzh() {
    return cxzh;
    }

    public void setWxh(String wxh) {
    this.wxh = wxh;
    }

    public String getWxh() {
    return wxh;
    }

    public void setShhm(String shhm) {
    this.shhm = shhm;
    }

    public String getShhm() {
    return shhm;
    }

    public void setLy(String ly) {
    this.ly = ly;
    }

    public String getLy() {
    return ly;
    }

    public void setLyfj(String lyfj) {
    this.lyfj = lyfj;
    }

    public String getLyfj() {
    return lyfj;
    }

    public void setKhztxm(String khztxm) {
    this.khztxm = khztxm;
    }

    public String getKhztxm() {
    return khztxm;
    }

    public void setShmc(String shmc) {
    this.shmc = shmc;
    }

    public String getShmc() {
    return shmc;
    }

    public void setKhztzjlx(String khztzjlx) {
    this.khztzjlx = khztzjlx;
    }

    public String getKhztzjlx() {
    return khztzjlx;
    }

    public void setQqh(String qqh) {
    this.qqh = qqh;
    }

    public String getQqh() {
    return qqh;
    }

    public void setBdsjh(String bdsjh) {
    this.bdsjh = bdsjh;
    }

    public String getBdsjh() {
    return bdsjh;
    }

    public void setSj(Integer sj) {
    this.sj = sj;
    }

    public Integer getSj() {
    return sj;
    }

    public void setZcxxlrsj(Date zcxxlrsj) {
    this.zcxxlrsj = zcxxlrsj;
    }

    public Date getZcxxlrsj() {
    return zcxxlrsj;
    }

    public void setCxzh_sfssfz(Integer cxzh_sfssfz) {
    this.cxzh_sfssfz = cxzh_sfssfz;
    }

    public Integer getCxzh_sfssfz() {
    return cxzh_sfssfz;
    }

    public void setWxzh_x(String wxzh_x) {
    this.wxzh_x = wxzh_x;
    }

    public String getWxzh_x() {
    return wxzh_x;
    }

    public void setQczd_x(Integer qczd_x) {
    this.qczd_x = qczd_x;
    }

    public Integer getQczd_x() {
    return qczd_x;
    }

    public void setSfzj6(String sfzj6) {
    this.sfzj6 = sfzj6;
    }

    public String getSfzj6() {
    return sfzj6;
    }

    public void setGwhj(Integer gwhj) {
    this.gwhj = gwhj;
    }

    public Integer getGwhj() {
    return gwhj;
    }

}
