package com.unis.model.wxjyyjmx;

import com.unis.common.annotation.ESField;
import com.unis.common.annotation.ESTable;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * 资金分析聚合表
 *
 * @author Qx
 * @version 1.0
 * @since 2020-05-12
 */
@ESTable(name = "vzhzjfxjhb", text = "资金分析聚合表")
@Table(name = "V_Zh_Zjfx_Jhb_TB")
public class VZhZjfxJhb {

    private static final long serialVersionUID = 1L;

/****************************************
 * Basic fields
 ****************************************/
    /**
     * 我方账号新1: VARCHAR
     */
    @Column(name = "WF_WXZH_X1")
    @ESField(text = "我方账号新1")
    private String wf_wxzh_x1;

    /**
     * 我方开户证件: VARCHAR
     */
    @Column(name = "WF_KHZJ")
    @ESField(text = "我方开户证件")
    private String wf_khzj;

    /**
     * 我方支付帐号: VARCHAR
     */
    @Column(name = "WFZFZH")
    @ESField(text = "我方支付帐号")
    private String wfzfzh;

    /**
     * 我方开户姓名: VARCHAR
     */
    @Column(name = "WF_KHXM")
    @ESField(text = "我方开户姓名")
    private String wf_khxm;

    /**
     * 交易类型: VARCHAR
     */
    @Column(name = "JYLX")
    @ESField(text = "交易类型")
    private String jylx;

    /**
     * 对方支付帐号: VARCHAR
     */
    @Column(name = "DFZFZH")
    @ESField(text = "对方支付帐号")
    private String dfzfzh;

    /**
     * 交易次数: DECIMAL
     */
    @Column(name = "JYCS")
    @ESField(text = "交易次数")
    private Integer jycs;

    /**
     * 交易金额: DECIMAL
     */
    @Column(name = "JYJE")
    @ESField(text = "交易金额")
    private Integer jyje;

    /**
     * 是否本地户籍人员对手: VARCHAR
     */
    @Column(name = "SFBDHJRYDS")
    @ESField(text = "是否本地户籍人员对手")
    private String sfbdhjryds;

    public void setWf_wxzh_x1(String wf_wxzh_x1) {
        this.wf_wxzh_x1 = wf_wxzh_x1;
    }

    public String getWf_wxzh_x1() {
        return wf_wxzh_x1;
    }

    public void setWf_khzj(String wf_khzj) {
        this.wf_khzj = wf_khzj;
    }

    public String getWf_khzj() {
        return wf_khzj;
    }

    public void setWfzfzh(String wfzfzh) {
        this.wfzfzh = wfzfzh;
    }

    public String getWfzfzh() {
        return wfzfzh;
    }

    public void setWf_khxm(String wf_khxm) {
        this.wf_khxm = wf_khxm;
    }

    public String getWf_khxm() {
        return wf_khxm;
    }

    public void setJylx(String jylx) {
        this.jylx = jylx;
    }

    public String getJylx() {
        return jylx;
    }

    public void setDfzfzh(String dfzfzh) {
        this.dfzfzh = dfzfzh;
    }

    public String getDfzfzh() {
        return dfzfzh;
    }

    public void setJycs(Integer jycs) {
        this.jycs = jycs;
    }

    public Integer getJycs() {
        return jycs;
    }

    public void setJyje(Integer jyje) {
        this.jyje = jyje;
    }

    public Integer getJyje() {
        return jyje;
    }

    public void setSfbdhjryds(String sfbdhjryds) {
        this.sfbdhjryds = sfbdhjryds;
    }

    public String getSfbdhjryds() {
        return sfbdhjryds;
    }

}
