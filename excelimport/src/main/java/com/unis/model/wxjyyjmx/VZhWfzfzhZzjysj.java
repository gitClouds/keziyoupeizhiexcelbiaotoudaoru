package com.unis.model.wxjyyjmx;

import com.unis.common.annotation.ESField;
import com.unis.common.annotation.ESTable;

import javax.persistence.Column;
import javax.persistence.Table;
/**
* 我方支付账号最早交易时间
*
* @author Qx
* @version 1.0
* @since 2020-05-12
*/
@Table(name = "V_ZH_WFZFZHZZJYSJ_TB")
@ESTable(name = "vzhwfzfzhzzjysj",text = "我方支付账号最早交易时间")
public class VZhWfzfzhZzjysj  {

private static final long serialVersionUID = 1L;

/****************************************
* Basic fields
****************************************/
    /**
    * 我方支付账号: VARCHAR
    */
    @Column(name = "WFZFZH")
    @ESField(text = "我方支付账号")
    private String wfzfzh;

    /**
    * 交易主体的出入账标识: VARCHAR
    */
    @Column(name = "JYZTDCRZBS")
    @ESField(text = "交易主体的出入账标识")
    private String jyztdcrzbs;

    /**
    * 对方支付账号: VARCHAR
    */
    @Column(name = "DFZFZH")
    @ESField(text = "对方支付账号")
    private String dfzfzh;

    /**
    * 交易类型: VARCHAR
    */
    @Column(name = "JYLX")
    @ESField(text = "交易类型")
    private String jylx;

    /**
    * 交易时间: VARCHAR
    */
    @Column(name = "JYSJ")
    @ESField(text = "交易时间")
    private String jysj;

    /**
    * 交易金额: VARCHAR
    */
    @Column(name = "JYJE")
    @ESField(text = "交易金额")
    private String jyje;

    /**
    * 我方支付账号最早交易情况: VARCHAR
    */
    @Column(name = "WFZFZHZZJYQK")
    @ESField(text = "我方支付账号最早交易情况")
    private String wfzfzhzzjyqk;

    public void setWfzfzh(String wfzfzh) {
    this.wfzfzh = wfzfzh;
    }

    public String getWfzfzh() {
    return wfzfzh;
    }

    public void setJyztdcrzbs(String jyztdcrzbs) {
    this.jyztdcrzbs = jyztdcrzbs;
    }

    public String getJyztdcrzbs() {
    return jyztdcrzbs;
    }

    public void setDfzfzh(String dfzfzh) {
    this.dfzfzh = dfzfzh;
    }

    public String getDfzfzh() {
    return dfzfzh;
    }

    public void setJylx(String jylx) {
    this.jylx = jylx;
    }

    public String getJylx() {
    return jylx;
    }

    public void setJysj(String jysj) {
    this.jysj = jysj;
    }

    public String getJysj() {
    return jysj;
    }

    public void setJyje(String jyje) {
    this.jyje = jyje;
    }

    public String getJyje() {
    return jyje;
    }

    public void setWfzfzhzzjyqk(String wfzfzhzzjyqk) {
    this.wfzfzhzzjyqk = wfzfzhzzjyqk;
    }

    public String getWfzfzhzzjyqk() {
    return wfzfzhzzjyqk;
    }

}
