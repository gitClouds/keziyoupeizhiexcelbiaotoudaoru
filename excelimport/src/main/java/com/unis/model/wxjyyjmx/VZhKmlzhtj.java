package com.unis.model.wxjyyjmx;

import com.unis.common.annotation.ESField;
import com.unis.common.annotation.ESTable;

import javax.persistence.Column;
import javax.persistence.Table;

/**
* 昆明来珠海统计
*
* @author Qx
* @version 1.0
* @since 2020-05-12
*/
@ESTable(name = "vzhkmlzhtj",text = "昆明来珠海统计")
@Table(name = "V_ZH_Kmlzhtj_TB")
public class VZhKmlzhtj  {

private static final long serialVersionUID = 1L;

/****************************************
* Basic fields
****************************************/
    /**
    * 编号: VARCHAR
    */
    @Column(name = "CERT_NO")
    @ESField(text = "编号")
    private String cert_no;

    /**
    * 次数: DECIMAL
    */
    @Column(name = "CS")
    @ESField(text = "次数")
    private Integer cs;

    public void setCert_no(String cert_no) {
    this.cert_no = cert_no;
    }

    public String getCert_no() {
    return cert_no;
    }

    public void setCs(Integer cs) {
    this.cs = cs;
    }

    public Integer getCs() {
    return cs;
    }

}
