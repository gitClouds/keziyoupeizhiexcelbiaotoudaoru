package com.unis.model.wxjyyjmx;

import java.util.Date;
import javax.persistence.*;
import com.unis.common.annotation.ESField;
import com.unis.common.annotation.ESTable;
/**
* 对方支付账号资金统计
*
* @author Qx
* @version 1.0
* @since 2020-05-12
*/
@ESTable(name = "vzhdfzfzhzjtj",text = "对方支付账号资金统计")
@Table(name = "V_ZH_Dfzfzh_Zjtj_TB")
public class VZhDfzfzhZjtj  {

private static final long serialVersionUID = 1L;

/****************************************
* Basic fields
****************************************/
    /**
    * 对方支付帐号: VARCHAR
    */
    @Column(name = "DFZFZH")
    @ESField(text = "对方支付帐号")
    private String dfzfzh;

    /**
    * 对方微信号: VARCHAR
    */
    @Column(name = "DF_WXH")
    @ESField(text = "对方微信号")
    private String df_wxh;

    /**
    * 对方开户名《2次查》: VARCHAR
    */
    @Column(name = "DF_KHXM_2CC")
    @ESField(text = "对方开户名《2次查》")
    private String df_khxm_2cc;

    /**
    * 对方开户证件《2次查》: VARCHAR
    */
    @Column(name = "DF_KHZJ_2CC")
    @ESField(text = "对方开户证件《2次查》")
    private String df_khzj_2cc;

    /**
    * 对方绑定手机号《2次查》: VARCHAR
    */
    @Column(name = "DF_BDSJH_2CC")
    @ESField(text = "对方绑定手机号《2次查》")
    private String df_bdsjh_2cc;

    /**
    * 高危户籍《对方》: DECIMAL
    */
    @Column(name = "DF_GWHJ")
    @ESField(text = "高危户籍《对方》")
    private Integer df_gwhj;

    /**
    * 出账次数: DECIMAL
    */
    @Column(name = "CZCS")
    @ESField(text = "出账次数")
    private Integer czcs;

    /**
    * 出账金额: DECIMAL
    */
    @Column(name = "CZJE")
    @ESField(text = "出账金额")
    private Integer czje;

    /**
    * 入账次数: DECIMAL
    */
    @Column(name = "RZCS")
    @ESField(text = "入账次数")
    private Integer rzcs;

    /**
    * 入账金额: DECIMAL
    */
    @Column(name = "RZJE")
    @ESField(text = "入账金额")
    private Integer rzje;

    /**
    * 资金关系人数: DECIMAL
    */
    @Column(name = "ZJGXRS")
    @ESField(text = "资金关系人数")
    private Integer zjgxrs;

    /**
    * 涉毒资金关系人数: DECIMAL
    */
    @Column(name = "SDZJGXRS")
    @ESField(text = "涉毒资金关系人数")
    private Integer sdzjgxrs;

    /**
    * 对方账号入账人数: DECIMAL
    */
    @Column(name = "DFZHRZRS")
    @ESField(text = "对方账号入账人数")
    private Integer dfzhrzrs;

    /**
    * 对方账号出账人数: DECIMAL
    */
    @Column(name = "DFZHCZRS")
    @ESField(text = "对方账号出账人数")
    private Integer dfzhczrs;

    /**
    * 对方入账涉毒人数: DECIMAL
    */
    @Column(name = "DFRZSDRS")
    @ESField(text = "对方入账涉毒人数")
    private Integer dfrzsdrs;

    /**
    * 对方出账涉毒人数: DECIMAL
    */
    @Column(name = "DFCZSDRS")
    @ESField(text = "对方出账涉毒人数")
    private Integer dfczsdrs;

    /**
    * 敏感时段率: VARCHAR
    */
    @Column(name = "MGSDL")
    @ESField(text = "敏感时段率")
    private String mgsdl;

    /**
    * 整数交易率: VARCHAR
    */
    @Column(name = "ZSJYL")
    @ESField(text = "整数交易率")
    private String zsjyl;

    public void setDfzfzh(String dfzfzh) {
    this.dfzfzh = dfzfzh;
    }

    public String getDfzfzh() {
    return dfzfzh;
    }

    public void setDf_wxh(String df_wxh) {
    this.df_wxh = df_wxh;
    }

    public String getDf_wxh() {
    return df_wxh;
    }

    public void setDf_khxm_2cc(String df_khxm_2cc) {
    this.df_khxm_2cc = df_khxm_2cc;
    }

    public String getDf_khxm_2cc() {
    return df_khxm_2cc;
    }

    public void setDf_khzj_2cc(String df_khzj_2cc) {
    this.df_khzj_2cc = df_khzj_2cc;
    }

    public String getDf_khzj_2cc() {
    return df_khzj_2cc;
    }

    public void setDf_bdsjh_2cc(String df_bdsjh_2cc) {
    this.df_bdsjh_2cc = df_bdsjh_2cc;
    }

    public String getDf_bdsjh_2cc() {
    return df_bdsjh_2cc;
    }

    public void setDf_gwhj(Integer df_gwhj) {
    this.df_gwhj = df_gwhj;
    }

    public Integer getDf_gwhj() {
    return df_gwhj;
    }

    public void setCzcs(Integer czcs) {
    this.czcs = czcs;
    }

    public Integer getCzcs() {
    return czcs;
    }

    public void setCzje(Integer czje) {
    this.czje = czje;
    }

    public Integer getCzje() {
    return czje;
    }

    public void setRzcs(Integer rzcs) {
    this.rzcs = rzcs;
    }

    public Integer getRzcs() {
    return rzcs;
    }

    public void setRzje(Integer rzje) {
    this.rzje = rzje;
    }

    public Integer getRzje() {
    return rzje;
    }

    public void setZjgxrs(Integer zjgxrs) {
    this.zjgxrs = zjgxrs;
    }

    public Integer getZjgxrs() {
    return zjgxrs;
    }

    public void setSdzjgxrs(Integer sdzjgxrs) {
    this.sdzjgxrs = sdzjgxrs;
    }

    public Integer getSdzjgxrs() {
    return sdzjgxrs;
    }

    public void setDfzhrzrs(Integer dfzhrzrs) {
    this.dfzhrzrs = dfzhrzrs;
    }

    public Integer getDfzhrzrs() {
    return dfzhrzrs;
    }

    public void setDfzhczrs(Integer dfzhczrs) {
    this.dfzhczrs = dfzhczrs;
    }

    public Integer getDfzhczrs() {
    return dfzhczrs;
    }

    public void setDfrzsdrs(Integer dfrzsdrs) {
    this.dfrzsdrs = dfrzsdrs;
    }

    public Integer getDfrzsdrs() {
    return dfrzsdrs;
    }

    public void setDfczsdrs(Integer dfczsdrs) {
    this.dfczsdrs = dfczsdrs;
    }

    public Integer getDfczsdrs() {
    return dfczsdrs;
    }

    public void setMgsdl(String mgsdl) {
    this.mgsdl = mgsdl;
    }

    public String getMgsdl() {
    return mgsdl;
    }

    public void setZsjyl(String zsjyl) {
    this.zsjyl = zsjyl;
    }

    public String getZsjyl() {
    return zsjyl;
    }

}
