package com.unis.model.wxjyyjmx;

import java.util.Date;
import javax.persistence.*;
import com.unis.common.annotation.ESField;
import com.unis.common.annotation.ESTable;
/**
* 涉毒人员总表去重
*
* @author Qx
* @version 1.0
* @since 2020-05-12
*/
@ESTable(name = "vzhsdryzbqc",text = "涉毒人员总表去重")
public class VZhSdryzbqc  {

private static final long serialVersionUID = 1L;

/****************************************
* Basic fields
****************************************/
    /**
    * 姓名: VARCHAR
    */
    @Column(name = "XM")
    @ESField(text = "姓名")
    private String xm;

    /**
    * 证件号码优化: VARCHAR
    */
    @Column(name = "ZJHMYH")
    @ESField(text = "证件号码优化")
    private String zjhmyh;

    /**
    * 嫌疑人电话: VARCHAR
    */
    @Column(name = "XYRDH")
    @ESField(text = "嫌疑人电话")
    private String xyrdh;

    /**
    * 审批时间: DATE
    */
    @Column(name = "SPSJ")
    @ESField(text = "审批时间")
    private Date spsj;

    /**
    * 人员户籍属地: VARCHAR
    */
    @Column(name = "RYHJSD")
    @ESField(text = "人员户籍属地")
    private String ryhjsd;

    /**
    * 人员地址: VARCHAR
    */
    @Column(name = "RYDZ")
    @ESField(text = "人员地址")
    private String rydz;

    /**
    * 受理单位: VARCHAR
    */
    @Column(name = "SLDW")
    @ESField(text = "受理单位")
    private String sldw;

    /**
    * 备注（案情）: VARCHAR
    */
    @Column(name = "AQ")
    @ESField(text = "备注（案情）")
    private String aq;

    /**
    * 案件编号证件号码姓名: VARCHAR
    */
    @Column(name = "AJBHZJHM")
    @ESField(text = "案件编号证件号码姓名")
    private String ajbhzjhm;

    /**
    * 数据来源: VARCHAR
    */
    @Column(name = "SJLY")
    @ESField(text = "数据来源")
    private String sjly;

    /**
    * 年龄: VARCHAR
    */
    @Column(name = "LL")
    @ESField(text = "年龄")
    private String ll;

    /**
    * 是否港台人员: VARCHAR
    */
    @Column(name = "SFGTRY")
    @ESField(text = "是否港台人员")
    private String sfgtry;

    /**
    * 绰号/别名: VARCHAR
    */
    @Column(name = "CH")
    @ESField(text = "绰号/别名")
    private String ch;

    /**
    * 出生日期: VARCHAR
    */
    @Column(name = "CSRQ")
    @ESField(text = "出生日期")
    private String csrq;

    /**
    * QQ: VARCHAR
    */
    @Column(name = "QQ")
    @ESField(text = "QQ")
    private String qq;

    /**
    * 国籍: DATE
    */
    @Column(name = "GJ")
    @ESField(text = "国籍")
    private String gj;

    /**
    * 收否台湾: DECIMAL
    */
    @Column(name = "SFTW")
    @ESField(text = "收否台湾")
    private Integer sftw;

    /**
    * 证件种类: VARCHAR
    */
    @Column(name = "ZJZL")
    @ESField(text = "证件种类")
    private String zjzl;

    /**
    * 户籍地详址: VARCHAR
    */
    @Column(name = "HJDXZ")
    @ESField(text = "户籍地详址")
    private String hjdxz;

    /**
    * 微信: DECIMAL
    */
    @Column(name = "WX")
    @ESField(text = "微信")
    private Integer wx;

    /**
    * 其他证件级号码: VARCHAR
    */
    @Column(name = "QTZJHM")
    @ESField(text = "其他证件级号码")
    private String qtzjhm;

    /**
    * 姓名别称: VARCHAR
    */
    @Column(name = "XMBC")
    @ESField(text = "姓名别称")
    private String xmbc;

    /**
    * 人员类别中午: VARCHAR
    */
    @Column(name = "RYLBZW")
    @ESField(text = "人员类别中午")
    private String rylbzw;

    /**
    * 联系方式电话: VARCHAR
    */
    @Column(name = "LXFSDH")
    @ESField(text = "联系方式电话")
    private String lxfsdh;

    /**
    * 是否澳门: DECIMAL
    */
    @Column(name = "SFAM")
    @ESField(text = "是否澳门")
    private Integer sfam;

    /**
    * 是否香港: DECIMAL
    */
    @Column(name = "SFXG")
    @ESField(text = "是否香港")
    private Integer sfxg;

    /**
    * 最新抓获去重（计算）: DECIMAL
    */
    @Column(name = "ZXGHQC")
    @ESField(text = "最新抓获去重（计算）")
    private Integer zxghqc;

    /**
    * 是否近三年（计算）: DECIMAL
    */
    @Column(name = "SFJSN")
    @ESField(text = "是否近三年（计算）")
    private Integer sfjsn;

    /**
    * 是否本地（计算）: DECIMAL
    */
    @Column(name = "SFBD")
    @ESField(text = "是否本地（计算）")
    private Integer sfbd;

    /**
    * 人员户籍归属新（计算）: VARCHAR
    */
    @Column(name = "RYHJGSX")
    @ESField(text = "人员户籍归属新（计算）")
    private String ryhjgsx;

    /**
    * 户籍地详细地址新（计算）: VARCHAR
    */
    @Column(name = "HJDXXDZX")
    @ESField(text = "户籍地详细地址新（计算）")
    private String hjdxxdzx;

    /**
    * 港澳台和其他人员标签: DECIMAL
    */
    @Column(name = "GATHQTRYBQ")
    @ESField(text = "港澳台和其他人员标签")
    private Integer gathqtrybq;

    /**
    * 最近抓获情况: VARCHAR
    */
    @Column(name = "ZJZHQK")
    @ESField(text = "最近抓获情况")
    private String zjzhqk;

    /**
    * 是否刑事（计算）: DECIMAL
    */
    @Column(name = "SFXS")
    @ESField(text = "是否刑事（计算）")
    private Integer sfxs;

    public void setXm(String xm) {
    this.xm = xm;
    }

    public String getXm() {
    return xm;
    }

    public void setZjhmyh(String zjhmyh) {
    this.zjhmyh = zjhmyh;
    }

    public String getZjhmyh() {
    return zjhmyh;
    }

    public void setXyrdh(String xyrdh) {
    this.xyrdh = xyrdh;
    }

    public String getXyrdh() {
    return xyrdh;
    }

    public void setSpsj(Date spsj) {
    this.spsj = spsj;
    }

    public Date getSpsj() {
    return spsj;
    }

    public void setRyhjsd(String ryhjsd) {
    this.ryhjsd = ryhjsd;
    }

    public String getRyhjsd() {
    return ryhjsd;
    }

    public void setRydz(String rydz) {
    this.rydz = rydz;
    }

    public String getRydz() {
    return rydz;
    }

    public void setSldw(String sldw) {
    this.sldw = sldw;
    }

    public String getSldw() {
    return sldw;
    }

    public void setAq(String aq) {
    this.aq = aq;
    }

    public String getAq() {
    return aq;
    }

    public void setAjbhzjhm(String ajbhzjhm) {
    this.ajbhzjhm = ajbhzjhm;
    }

    public String getAjbhzjhm() {
    return ajbhzjhm;
    }

    public void setSjly(String sjly) {
    this.sjly = sjly;
    }

    public String getSjly() {
    return sjly;
    }

    public void setLl(String ll) {
    this.ll = ll;
    }

    public String getLl() {
    return ll;
    }

    public void setSfgtry(String sfgtry) {
    this.sfgtry = sfgtry;
    }

    public String getSfgtry() {
    return sfgtry;
    }

    public void setCh(String ch) {
    this.ch = ch;
    }

    public String getCh() {
    return ch;
    }

    public void setCsrq(String csrq) {
    this.csrq = csrq;
    }

    public String getCsrq() {
    return csrq;
    }

    public void setQq(String qq) {
    this.qq = qq;
    }

    public String getQq() {
    return qq;
    }

    public String getGj() {
        return gj;
    }

    public void setGj(String gj) {
        this.gj = gj;
    }

    public void setSftw(Integer sftw) {
    this.sftw = sftw;
    }

    public Integer getSftw() {
    return sftw;
    }

    public void setZjzl(String zjzl) {
    this.zjzl = zjzl;
    }

    public String getZjzl() {
    return zjzl;
    }

    public void setHjdxz(String hjdxz) {
    this.hjdxz = hjdxz;
    }

    public String getHjdxz() {
    return hjdxz;
    }

    public void setWx(Integer wx) {
    this.wx = wx;
    }

    public Integer getWx() {
    return wx;
    }

    public void setQtzjhm(String qtzjhm) {
    this.qtzjhm = qtzjhm;
    }

    public String getQtzjhm() {
    return qtzjhm;
    }

    public void setXmbc(String xmbc) {
    this.xmbc = xmbc;
    }

    public String getXmbc() {
    return xmbc;
    }

    public void setRylbzw(String rylbzw) {
    this.rylbzw = rylbzw;
    }

    public String getRylbzw() {
    return rylbzw;
    }

    public void setLxfsdh(String lxfsdh) {
    this.lxfsdh = lxfsdh;
    }

    public String getLxfsdh() {
    return lxfsdh;
    }

    public void setSfam(Integer sfam) {
    this.sfam = sfam;
    }

    public Integer getSfam() {
    return sfam;
    }

    public void setSfxg(Integer sfxg) {
    this.sfxg = sfxg;
    }

    public Integer getSfxg() {
    return sfxg;
    }

    public void setZxghqc(Integer zxghqc) {
    this.zxghqc = zxghqc;
    }

    public Integer getZxghqc() {
    return zxghqc;
    }

    public void setSfjsn(Integer sfjsn) {
    this.sfjsn = sfjsn;
    }

    public Integer getSfjsn() {
    return sfjsn;
    }

    public void setSfbd(Integer sfbd) {
    this.sfbd = sfbd;
    }

    public Integer getSfbd() {
    return sfbd;
    }

    public void setRyhjgsx(String ryhjgsx) {
    this.ryhjgsx = ryhjgsx;
    }

    public String getRyhjgsx() {
    return ryhjgsx;
    }

    public void setHjdxxdzx(String hjdxxdzx) {
    this.hjdxxdzx = hjdxxdzx;
    }

    public String getHjdxxdzx() {
    return hjdxxdzx;
    }

    public void setGathqtrybq(Integer gathqtrybq) {
    this.gathqtrybq = gathqtrybq;
    }

    public Integer getGathqtrybq() {
    return gathqtrybq;
    }

    public void setZjzhqk(String zjzhqk) {
    this.zjzhqk = zjzhqk;
    }

    public String getZjzhqk() {
    return zjzhqk;
    }

    public void setSfxs(Integer sfxs) {
    this.sfxs = sfxs;
    }

    public Integer getSfxs() {
    return sfxs;
    }

}
