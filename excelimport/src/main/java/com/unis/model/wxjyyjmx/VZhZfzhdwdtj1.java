package com.unis.model.wxjyyjmx;

import java.util.Date;
import javax.persistence.*;

import com.unis.common.annotation.ESField;
import com.unis.common.annotation.ESTable;

/**
 * @author Qx
 * @version 1.0
 * @since 2020-06-02
 */
@ESTable(name = "vzhzfzhdwdtj1", text = "支付账号多维度统计V1.0")
@Table(name = "V_ZH_ZFZHDWDTJ_1")
public class VZhZfzhdwdtj1 {

    private static final long serialVersionUID = 1L;

/****************************************
 * Basic fields
 ****************************************/
    /**
     * 支付账号: VARCHAR
     */
    @Column(name = "CFTZH")
    @ESField(text = "支付账号")
    private String cftzh;

    /**
     * <我方>入账涉毒人数 : DECIMAL
     */
    @Column(name = "WF_RZ_SDRS")
    @ESField(text = "<我方>入账涉毒人数")
    private Integer wf_rz_sdrs;

    /**
     * 最后交易时间: VARCHAR
     */
    @Column(name = "ZHJYSJ")
    @ESField(text = " 最后交易时间")
    private String zhjysj;

    /**
     * 最后交易情况: VARCHAR
     */
    @Column(name = "ZHJYQK")
    @ESField(text = " 最后交易情况")
    private String zhjyqk;

    /**
     * 明细 : VARCHAR
     */
    @Column(name = "MX")
    @ESField(text = "明细")
    private String mx;

    /**
     * 绑定手机号新 : VARCHAR
     */
    @Column(name = "BDSJHMX")
    @ESField(text = "绑定手机号新")
    private String bdsjhmx;

    /**
     * 开户证件: VARCHAR
     */
    @Column(name = "KHZJ")
    @ESField(text = "开户证件")
    private String khzj;

    /**
     * 开户人 : VARCHAR
     */
    @Column(name = "KHR")
    @ESField(text = "开户人")
    private String khr;

    /**
     * 涉毒资金关系人数 : DECIMAL
     */
    @Column(name = "SDZJGXRS")
    @ESField(text = "涉毒资金关系人数")
    private Integer sdzjgxrs;

    /**
     * 入账人数 : VARCHAR
     */
    @Column(name = "RZ_RS")
    @ESField(text = "入账人数")
    private String rz_rs;

    /**
     * 入账涉毒人数 : DECIMAL
     */
    @Column(name = "RZ_SDRS")
    @ESField(text = "入账涉毒人数")
    private Integer rz_sdrs;

    /**
     * 入账金额 : DECIMAL
     */
    @Column(name = "RZ_JE")
    @ESField(text = "入账金额")
    private Integer rz_je;

    /**
     * 入账次数 : DECIMAL
     */
    @Column(name = "RZ_CS")
    @ESField(text = "入账次数")
    private Integer rz_cs;

    /**
     * 出账人数 : DECIMAL
     */
    @Column(name = "CZ_CS")
    @ESField(text = "出账人数")
    private Integer cz_cs;

    /**
     * 出账涉毒人数 : DECIMAL
     */
    @Column(name = "CZ_SDRS")
    @ESField(text = "出账涉毒人数")
    private Integer cz_sdrs;

    /**
     * 出账金额 : DECIMAL
     */
    @Column(name = "CZ_JE")
    @ESField(text = "出账金额")
    private Integer cz_je;

    /**
     * 已调资金关系人数 : VARCHAR
     */
    @Column(name = "YDZJGXRS")
    @ESField(text = "已调资金关系人数")
    private String ydzjgxrs;

    /**
     * 微信号 : VARCHAR
     */
    @Column(name = "WXH")
    @ESField(text = "微信号")
    private String wxh;

    /**
     * 下家: VARCHAR
     */
    @Column(name = "XJX")
    @ESField(text = "下家")
    private String xjx;

    /**
     * 上家 : VARCHAR
     */
    @Column(name = "SJX")
    @ESField(text = "上家 ")
    private String sjx;

    /**
     * 涉毒好友: VARCHAR
     */
    @Column(name = "SDHY")
    @ESField(text = "涉毒好友")
    private String sdhy;

    /**
     * 微信通联积分描述: VARCHAR
     */
    @Column(name = "WXTLJFMX")
    @ESField(text = "微信通联积分描述")
    private String wxtljfmx;

    /**
     * 微信通联积分: VARCHAR
     */
    @Column(name = "WXTLJF")
    @ESField(text = "微信通联积分")
    private String wxtljf;

    /**
     * 微信昵称: VARCHAR
     */
    @Column(name = "WXNC")
    @ESField(text = "微信昵称")
    private String wxnc;

    /**
     * 开户资料 : VARCHAR
     */
    @Column(name = "KHZL")
    @ESField(text = "开户资料")
    private String khzl;

    /**
     * 研判报告 : VARCHAR
     */
    @Column(name = "YPBG")
    @ESField(text = "研判报告")
    private String ypbg;

    /**
     * 经销商积分描述: VARCHAR
     */
    @Column(name = "JXSJFMS")
    @ESField(text = "经销商积分描述")
    private String jxsjfms;

    /**
     * 经销商积分 : VARCHAR
     */
    @Column(name = "JXSJF")
    @ESField(text = "经销商积分")
    private String jxsjf;

    /**
     * 来源案件: CHAR
     */
    @Column(name = "LYAJ")
    @ESField(text = "来源案件")
    private String lyaj;

    /**
     * 实名手机: CHAR
     */
    @Column(name = "SMSJ")
    @ESField(text = "实名手机")
    private String smsj;

    /**
     * 最新参考位置: CHAR
     */
    @Column(name = "ZXCKWZ")
    @ESField(text = "最新参考位置")
    private String zxckwz;

    /**
     * 夜间参考位置: CHAR
     */
    @Column(name = "YJCKWZ")
    @ESField(text = "夜间参考位置")
    private String yjckwz;

    /**
     * 收件地址: CHAR
     */
    @Column(name = "SJDZ")
    @ESField(text = "收件地址")
    private String sjdz;

    /**
     * 阿里标签数: CHAR
     */
    @Column(name = "ALBQS")
    @ESField(text = "阿里标签数")
    private String albqs;

    /**
     * 收款规律积分(我方) : DECIMAL
     */
    @Column(name = "SKGLJF_WF")
    @ESField(text = "收款规律积分(我方)")
    private Integer skgljf_wf;

    /**
     * 收款规律积分(对方): DECIMAL
     */
    @Column(name = "SKGLJF_DF")
    @ESField(text = "收款规律积分(对方)")
    private Integer skgljf_df;

    /**
     * 注释: DECIMAL
     */
    @Column(name = "SKGLJF_MAX")
    private Integer skgljf_max;

    /**
     * 积分描述新: VARCHAR
     */
    @Column(name = "JFMSX")
    @ESField(text = "积分描述新")
    private String jfmsx;

    /**
     * 积分新: DECIMAL
     */
    @Column(name = "JFX")
    @ESField(text = "积分新")
    private Integer jfx;

    /**
     * 研判报告1.0: VARCHAR
     */
    @Column(name = "YPBG1")
    @ESField(text = "研判报告1.0")
    private String ypbg1;

    public void setCftzh(String cftzh) {
        this.cftzh = cftzh;
    }

    public String getCftzh() {
        return cftzh;
    }

    public void setWf_rz_sdrs(Integer wf_rz_sdrs) {
        this.wf_rz_sdrs = wf_rz_sdrs;
    }

    public Integer getWf_rz_sdrs() {
        return wf_rz_sdrs;
    }

    public void setZhjysj(String zhjysj) {
        this.zhjysj = zhjysj;
    }

    public String getZhjysj() {
        return zhjysj;
    }

    public void setZhjyqk(String zhjyqk) {
        this.zhjyqk = zhjyqk;
    }

    public String getZhjyqk() {
        return zhjyqk;
    }

    public void setMx(String mx) {
        this.mx = mx;
    }

    public String getMx() {
        return mx;
    }

    public void setBdsjhmx(String bdsjhmx) {
        this.bdsjhmx = bdsjhmx;
    }

    public String getBdsjhmx() {
        return bdsjhmx;
    }

    public void setKhzj(String khzj) {
        this.khzj = khzj;
    }

    public String getKhzj() {
        return khzj;
    }

    public void setKhr(String khr) {
        this.khr = khr;
    }

    public String getKhr() {
        return khr;
    }

    public void setSdzjgxrs(Integer sdzjgxrs) {
        this.sdzjgxrs = sdzjgxrs;
    }

    public Integer getSdzjgxrs() {
        return sdzjgxrs;
    }

    public void setRz_rs(String rz_rs) {
        this.rz_rs = rz_rs;
    }

    public String getRz_rs() {
        return rz_rs;
    }

    public void setRz_sdrs(Integer rz_sdrs) {
        this.rz_sdrs = rz_sdrs;
    }

    public Integer getRz_sdrs() {
        return rz_sdrs;
    }

    public void setRz_je(Integer rz_je) {
        this.rz_je = rz_je;
    }

    public Integer getRz_je() {
        return rz_je;
    }

    public void setRz_cs(Integer rz_cs) {
        this.rz_cs = rz_cs;
    }

    public Integer getRz_cs() {
        return rz_cs;
    }

    public void setCz_cs(Integer cz_cs) {
        this.cz_cs = cz_cs;
    }

    public Integer getCz_cs() {
        return cz_cs;
    }

    public void setCz_sdrs(Integer cz_sdrs) {
        this.cz_sdrs = cz_sdrs;
    }

    public Integer getCz_sdrs() {
        return cz_sdrs;
    }

    public void setCz_je(Integer cz_je) {
        this.cz_je = cz_je;
    }

    public Integer getCz_je() {
        return cz_je;
    }

    public void setYdzjgxrs(String ydzjgxrs) {
        this.ydzjgxrs = ydzjgxrs;
    }

    public String getYdzjgxrs() {
        return ydzjgxrs;
    }

    public void setWxh(String wxh) {
        this.wxh = wxh;
    }

    public String getWxh() {
        return wxh;
    }

    public void setXjx(String xjx) {
        this.xjx = xjx;
    }

    public String getXjx() {
        return xjx;
    }

    public void setSjx(String sjx) {
        this.sjx = sjx;
    }

    public String getSjx() {
        return sjx;
    }

    public void setSdhy(String sdhy) {
        this.sdhy = sdhy;
    }

    public String getSdhy() {
        return sdhy;
    }

    public void setWxtljfmx(String wxtljfmx) {
        this.wxtljfmx = wxtljfmx;
    }

    public String getWxtljfmx() {
        return wxtljfmx;
    }

    public void setWxtljf(String wxtljf) {
        this.wxtljf = wxtljf;
    }

    public String getWxtljf() {
        return wxtljf;
    }

    public void setWxnc(String wxnc) {
        this.wxnc = wxnc;
    }

    public String getWxnc() {
        return wxnc;
    }

    public void setKhzl(String khzl) {
        this.khzl = khzl;
    }

    public String getKhzl() {
        return khzl;
    }

    public void setYpbg(String ypbg) {
        this.ypbg = ypbg;
    }

    public String getYpbg() {
        return ypbg;
    }

    public void setJxsjfms(String jxsjfms) {
        this.jxsjfms = jxsjfms;
    }

    public String getJxsjfms() {
        return jxsjfms;
    }

    public void setJxsjf(String jxsjf) {
        this.jxsjf = jxsjf;
    }

    public String getJxsjf() {
        return jxsjf;
    }

    public void setLyaj(String lyaj) {
        this.lyaj = lyaj;
    }

    public String getLyaj() {
        return lyaj;
    }

    public void setSmsj(String smsj) {
        this.smsj = smsj;
    }

    public String getSmsj() {
        return smsj;
    }

    public void setZxckwz(String zxckwz) {
        this.zxckwz = zxckwz;
    }

    public String getZxckwz() {
        return zxckwz;
    }

    public void setYjckwz(String yjckwz) {
        this.yjckwz = yjckwz;
    }

    public String getYjckwz() {
        return yjckwz;
    }

    public void setSjdz(String sjdz) {
        this.sjdz = sjdz;
    }

    public String getSjdz() {
        return sjdz;
    }

    public void setAlbqs(String albqs) {
        this.albqs = albqs;
    }

    public String getAlbqs() {
        return albqs;
    }

    public void setSkgljf_wf(Integer skgljf_wf) {
        this.skgljf_wf = skgljf_wf;
    }

    public Integer getSkgljf_wf() {
        return skgljf_wf;
    }

    public void setSkgljf_df(Integer skgljf_df) {
        this.skgljf_df = skgljf_df;
    }

    public Integer getSkgljf_df() {
        return skgljf_df;
    }

    public void setSkgljf_max(Integer skgljf_max) {
        this.skgljf_max = skgljf_max;
    }

    public Integer getSkgljf_max() {
        return skgljf_max;
    }

    public void setJfmsx(String jfmsx) {
        this.jfmsx = jfmsx;
    }

    public String getJfmsx() {
        return jfmsx;
    }

    public void setJfx(Integer jfx) {
        this.jfx = jfx;
    }

    public Integer getJfx() {
        return jfx;
    }

    public void setYpbg1(String ypbg1) {
        this.ypbg1 = ypbg1;
    }

    public String getYpbg1() {
        return ypbg1;
    }

}
