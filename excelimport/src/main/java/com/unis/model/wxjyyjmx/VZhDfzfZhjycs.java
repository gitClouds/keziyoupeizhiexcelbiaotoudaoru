package com.unis.model.wxjyyjmx;

import java.util.Date;
import javax.persistence.*;
import com.unis.common.annotation.ESField;
import com.unis.common.annotation.ESTable;
/**
* 对方支付账号交易次数
*
* @author Qx
* @version 1.0
* @since 2020-05-12
*/
@Table(name = "V_ZH_DFZFZHJYCS_TB")
@ESTable(name = "vzhdfzfzhjycs",text = "对方支付账号交易次数")
public class VZhDfzfZhjycs  {

private static final long serialVersionUID = 1L;

/****************************************
* Basic fields
****************************************/
    /**
    * 对方支付账号: VARCHAR
    */
    @Column(name = "DFZFZH")
    @ESField(text = "对方支付账号")
    private String dfzfzh;

    /**
    * 交易次数: DECIMAL
    */
    @Column(name = "JYCS")
    @ESField(text = "交易次数")
    private Integer jycs;

    public void setDfzfzh(String dfzfzh) {
    this.dfzfzh = dfzfzh;
    }

    public String getDfzfzh() {
    return dfzfzh;
    }

    public void setJycs(int jycs) {
    this.jycs = jycs;
    }

    public int getJycs() {
    return jycs;
    }

}
