package com.unis.model.wxjyyjmx;

import java.util.Date;
import javax.persistence.*;
import com.unis.common.annotation.ESField;
import com.unis.common.annotation.ESTable;
/**
* 对方支付账号下家统计新
*
* @author Qx
* @version 1.0
* @since 2020-05-12
*/
@ESTable(name = "vzhdfzfzhxjtjx",text = "对方支付账号下家统计新")
@Table(name = "V_ZH_DFZFZHXJTJX_TB")
public class VZhDfzfzhxjtjx  {

private static final long serialVersionUID = 1L;

/****************************************
* Basic fields
****************************************/
    /**
    * 对方支付账号《出账》: VARCHAR
    */
    @Column(name = "DF_ZFZH_CZ")
    @ESField(text = "对方支付账号《出账》")
    private String df_zfzh_cz;

    /**
    * 对方下家: VARCHAR
    */
    @Column(name = "DFXJ")
    @ESField(text = "对方下家")
    private String dfxj;

    public void setDf_zfzh_cz(String df_zfzh_cz) {
    this.df_zfzh_cz = df_zfzh_cz;
    }

    public String getDf_zfzh_cz() {
    return df_zfzh_cz;
    }

    public void setDfxj(String dfxj) {
    this.dfxj = dfxj;
    }

    public String getDfxj() {
    return dfxj;
    }

}
