package com.unis.model.wxjyyjmx;

import com.unis.common.annotation.ESField;
import com.unis.common.annotation.ESTable;

import javax.persistence.Column;
import javax.persistence.Table;
/**
* 对方支付账号最后活跃时间 
*
* @author xuk
* @version 1.0
* @since 2020-05-06
*/
@ESTable(name = "vzhdfzfzhzhhysj",text = "对方支付账号最后活跃时间 ")
@Table(name = "V_ZH_DFZFZH_ZHHYSJ_TB")
public class VZhDfzfzhZhysj  {

private static final long serialVersionUID = 1L;

/****************************************
* Basic fields
****************************************/
    /**
    * 对方支付账号: VARCHAR
    */
    @Column(name = "DFZFZH")
    @ESField(text = "对方支付账号")
    private String dfzfzh;

    /**
    * 交易时间: VARCHAR
    */
    @Column(name = "JYSJ")
    @ESField(text = "交易时间")
    private String jysj;

    /**
    * 对方支付账号最后活跃时间: DECIMAL
    */
    @Column(name = "DFZFZHZHHYSJ")
    @ESField(text = "对方支付账号最后活跃时间")
    private Integer dfzfzhzhhysj;

    /**
    * 我方开户证件: VARCHAR
    */
    @Column(name = "WF_KHZJ")
    @ESField(text = "我方开户证件")
    private String wf_khzj;

    /**
    * 交易类型: VARCHAR
    */
    @Column(name = "JYLX")
    @ESField(text = "交易类型")
    private String jylx;

    /**
    * 我方来源: CHAR
    */
    @Column(name = "WF_LY")
    @ESField(text = "我方来源")
    private String wf_ly;

    /**
    * 我方开户姓名: VARCHAR
    */
    @Column(name = "WF_KHXM")
    @ESField(text = "我方开户姓名")
    private String wf_khxm;

    /**
    * 交易金额: VARCHAR
    */
    @Column(name = "JYJE")
    @ESField(text = "交易金额")
    private String jyje;

    /**
    * 最后活跃情况: VARCHAR
    */
    @Column(name = "ZHHYQK")
    @ESField(text = "最后活跃情况")
    private String zhhyqk;

    public void setDfzfzh(String dfzfzh) {
    this.dfzfzh = dfzfzh;
    }

    public String getDfzfzh() {
    return dfzfzh;
    }

    public void setJysj(String jysj) {
    this.jysj = jysj;
    }

    public String getJysj() {
    return jysj;
    }

    public void setDfzfzhzhhysj(Integer dfzfzhzhhysj) {
    this.dfzfzhzhhysj = dfzfzhzhhysj;
    }

    public Integer getDfzfzhzhhysj() {
    return dfzfzhzhhysj;
    }

    public void setWf_khzj(String wf_khzj) {
    this.wf_khzj = wf_khzj;
    }

    public String getWf_khzj() {
    return wf_khzj;
    }

    public void setJylx(String jylx) {
    this.jylx = jylx;
    }

    public String getJylx() {
    return jylx;
    }

    public void setWf_ly(String wf_ly) {
    this.wf_ly = wf_ly;
    }

    public String getWf_ly() {
    return wf_ly;
    }

    public void setWf_khxm(String wf_khxm) {
    this.wf_khxm = wf_khxm;
    }

    public String getWf_khxm() {
    return wf_khxm;
    }

    public void setJyje(String jyje) {
    this.jyje = jyje;
    }

    public String getJyje() {
    return jyje;
    }

    public void setZhhyqk(String zhhyqk) {
    this.zhhyqk = zhhyqk;
    }

    public String getZhhyqk() {
    return zhhyqk;
    }

}
