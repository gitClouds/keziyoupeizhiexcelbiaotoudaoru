package com.unis.model.wxjyyjmx;

import java.util.Date;
import javax.persistence.*;
import com.unis.common.annotation.ESField;
import com.unis.common.annotation.ESTable;
/**
* 涉毒前科聚合
*
* @author Qx
* @version 1.0
* @since 2020-05-12
*/
@ESTable(name = "vzhsdqkjh",text = "涉毒前科聚合")
@Table(name = "V_ZH_Sdqkjh_TB")
public class VZhSdqkjh  {

private static final long serialVersionUID = 1L;

/****************************************
* Basic fields
****************************************/
    /**
    * 证件号码优化: VARCHAR
    */
    @Column(name = "ZJHMYH")
    @ESField(text = "证件号码优化")
    private String zjhmyh;

    /**
    * 前科聚合: VARCHAR
    */
    @Column(name = "QKJH")
    @ESField(text = "前科聚合")
    private String qkjh;

    public void setZjhmyh(String zjhmyh) {
    this.zjhmyh = zjhmyh;
    }

    public String getZjhmyh() {
    return zjhmyh;
    }

    public void setQkjh(String qkjh) {
    this.qkjh = qkjh;
    }

    public String getQkjh() {
    return qkjh;
    }

}
