package com.unis.model.wxjyyjmx;

import com.unis.common.annotation.ESField;
import com.unis.common.annotation.ESTable;

import javax.persistence.Column;
import javax.persistence.Table;

/**
* 是否调取支付账号
*
* @author Qx
* @version 1.0
* @since 2020-05-12
*/
@ESTable(name = "vzhffdqzfzh",text = "是否调取支付账号")
@Table(name = "V_ZH_Ffdqzfzh_TB")

public class VZhFfdqzfzh  {

private static final long serialVersionUID = 1L;

/****************************************
* Basic fields
****************************************/
    /**
    * 交易时间型: VARCHAR
    */
    @Column(name = "JYSJX")
    @ESField(text = "交易时间型")
    private String jysjx;

    /**
    * 我方支付账号: VARCHAR
    */
    @Column(name = "WFZFZH")
    @ESField(text = "我方支付账号")
    private String wfzfzh;

    public void setJysjx(String jysjx) {
    this.jysjx = jysjx;
    }

    public String getJysjx() {
    return jysjx;
    }

    public void setWfzfzh(String wfzfzh) {
    this.wfzfzh = wfzfzh;
    }

    public String getWfzfzh() {
    return wfzfzh;
    }

}
