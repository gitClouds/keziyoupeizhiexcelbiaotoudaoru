package com.unis.model.wxjyyjmx;

import java.util.Date;
import javax.persistence.*;
import com.unis.common.annotation.ESField;
import com.unis.common.annotation.ESTable;
/**
* 我方上家聚合
*
* @author Qx
* @version 1.0
* @since 2020-05-12
*/
@ESTable(name = "vzhwfsjjh",text = "我方上家聚合")
@Table(name = "V_ZH_Wf_Sjjh_TB")
public class VZhWfSjjh  {

private static final long serialVersionUID = 1L;

/****************************************
* Basic fields
****************************************/
    /**
    * 我方支付账户《出账》: VARCHAR
    */
    @Column(name = "WF_ZFZH_CZ")
    @ESField(text = "我方支付账户《出账》")
    private String wf_zfzh_cz;

    /**
    * 我方上家: VARCHAR
    */
    @Column(name = "WFSJ")
    @ESField(text = "我方上家")
    private String wfsj;

    public void setWf_zfzh_cz(String wf_zfzh_cz) {
    this.wf_zfzh_cz = wf_zfzh_cz;
    }

    public String getWf_zfzh_cz() {
    return wf_zfzh_cz;
    }

    public void setWfsj(String wfsj) {
    this.wfsj = wfsj;
    }

    public String getWfsj() {
    return wfsj;
    }

}
