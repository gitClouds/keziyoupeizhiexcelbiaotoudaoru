package com.unis.model.wxjyyjmx;

import java.util.Date;
import javax.persistence.*;

import com.unis.common.annotation.ESField;
import com.unis.common.annotation.ESTable;

/**
 * @author Qx
 * @version 1.0
 * @since 2020-05-29
 */
//@ESTable(name = "zhMxdrdata", text = "模型导入数据")
public class ZhMxdrdata {

    private static final long serialVersionUID = 1L;

/****************************************
 * Basic fields
 ****************************************/
    /**
     * 注释: VARCHAR
     */
    @Column(name = "ID")
    private String id;

    /**
     * 查询账号: VARCHAR
     */
    @Column(name = "CXZH")
    @ESField(text = "查询账号")
    private String cxzh;

    /**
     * 得分: VARCHAR
     */
    @Column(name = "DF")
    @ESField(text = "得分")
    private String df;

    /**
     * 数据来源: VARCHAR
     */
    @Column(name = "SJLY")
    @ESField(text = "数据来源")
    private String sjly;

    /**
     * 录入时间: DATE
     */
    @Column(name = "LRSJ")
    @ESField(text = "录入时间")
    private Date lrsj;

    /**
     * 录入人警号: VARCHAR
     */
    @Column(name = "LRRJH")
    @ESField(text = "录入人警号")
    private String lrrjh;

    /**
     * 录入人姓名: VARCHAR
     */
    @Column(name = "LRRXM")
    @ESField(text = "录入人姓名")
    private String lrrxm;

    /**
     * 录入单位名称: VARCHAR
     */
    @Column(name = "LRRDWDM")
    @ESField(text = "录入单位名称")
    private String lrrdwdm;

    /**
     * 录入单位代码: VARCHAR
     */
    @Column(name = "LRRDWMC")
    @ESField(text = "录入单位代码")
    private String lrrdwmc;
    @Column(name = "YXX")
    private String yxx;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setCxzh(String cxzh) {
        this.cxzh = cxzh;
    }

    public String getCxzh() {
        return cxzh;
    }

    public void setDf(String df) {
        this.df = df;
    }

    public String getDf() {
        return df;
    }

    public void setSjly(String sjly) {
        this.sjly = sjly;
    }

    public String getSjly() {
        return sjly;
    }

    public void setLrsj(Date lrsj) {
        this.lrsj = lrsj;
    }

    public Date getLrsj() {
        return lrsj;
    }

    public void setLrrjh(String lrrjh) {
        this.lrrjh = lrrjh;
    }

    public String getLrrjh() {
        return lrrjh;
    }

    public void setLrrxm(String lrrxm) {
        this.lrrxm = lrrxm;
    }

    public String getLrrxm() {
        return lrrxm;
    }

    public void setLrrdwdm(String lrrdwdm) {
        this.lrrdwdm = lrrdwdm;
    }

    public String getLrrdwdm() {
        return lrrdwdm;
    }

    public void setLrrdwmc(String lrrdwmc) {
        this.lrrdwmc = lrrdwmc;
    }

    public String getLrrdwmc() {
        return lrrdwmc;
    }

    public String getYxx() {
        return yxx;
    }

    public void setYxx(String yxx) {
        this.yxx = yxx;
    }
}
