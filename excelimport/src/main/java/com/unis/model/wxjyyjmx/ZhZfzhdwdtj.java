package com.unis.model.wxjyyjmx;

import java.util.Date;
import javax.persistence.*;

/**
 * @author Qx
 * @version 1.0
 * @since 2020-05-09
 */

public class ZhZfzhdwdtj {

    private static final long serialVersionUID = 1L;

/****************************************
 * Basic fields
 ****************************************/
    /**
     * 最后交易时间日期形: VARCHAR
     */
    @Column(name = "ZHJYSJRQX")
    private String zhjysjrqx;

    /**
     * 已调明细: VARCHAR
     */
    @Column(name = "YDMX")
    private String ydmx;

    /**
     * 昆明到港次数《证件关联》: DECIMAL
     */
    @Column(name = "KMDGCS_ZJGL")
    private Integer kmdgcs_zjgl;

    /**
     * 对方支付账号交易次数: DECIMAL
     */
    @Column(name = "DFZFZHJYCS")
    private Integer dfzfzhjycs;

    /**
     * 《对方》最后交易时间: VARCHAR
     */
    @Column(name = "DFZHJYSJ")
    private String dfzhjysj;

    /**
     * 《对方》最后交易情况: VARCHAR
     */
    @Column(name = "DFZHJYQK")
    private String dfzhjyqk;

    /**
     * 《对方》最早交易时间: VARCHAR
     */
    @Column(name = "DFZZJYSJ")
    private String dfzzjysj;

    /**
     * 《对方》最早交易情况: VARCHAR
     */
    @Column(name = "DFZZJYQK")
    private String dfzzjyqk;

    /**
     * 《我方》最后交易余额: VARCHAR
     */
    @Column(name = "WFZHJYYE")
    private String wfzhjyye;

    /**
     * 《我方》最后来源案件: VARCHAR
     */
    @Column(name = "WFZHLYAJ")
    private String wfzhlyaj;

    /**
     * 《我方》最后交易时间1: VARCHAR
     */
    @Column(name = "WFZHJYSJ1")
    private String wfzhjysj1;

    /**
     * 《我方》最后交易情况1: VARCHAR
     */
    @Column(name = "WFZHJYQK1")
    private String wfzhjyqk1;

    /**
     * 全部前科信息: VARCHAR
     */
    @Column(name = "QBQKXX")
    private String qbqkxx;

    /**
     * 最近抓获时间: DATE
     */
    @Column(name = "ZJZHSJ")
    private Date zjzhsj;

    /**
     * 最近抓获数据来源: VARCHAR
     */
    @Column(name = "ZJZHSJLY")
    private String zjzhsjly;

    /**
     * 前科人员户籍属地: VARCHAR
     */
    @Column(name = "QKRYHJSD")
    private String qkryhjsd;

    /**
     * 最近抓获人员类别中文: VARCHAR
     */
    @Column(name = "ZJZHRYLBZW")
    private String zjzhrylbzw;

    /**
     * 前科受理单位: VARCHAR
     */
    @Column(name = "QKSLDW")
    private String qksldw;

    /**
     * 最近抓获情况: VARCHAR
     */
    @Column(name = "ZJZHQK")
    private String zjzhqk;

    /**
     * 绑定银行卡《微信账号关联》: VARCHAR
     */
    @Column(name = "BDYHK_WXZHGL")
    private String bdyhk_wxzhgl;

    /**
     * 开户主体证件号或证照号《微信账号关联》: VARCHAR
     */
    @Column(name = "KHZTZJHM_WXZHGL")
    private String khztzjhm_wxzhgl;

    /**
     * 开户主体姓名或企业名称《微信账号关联》: VARCHAR
     */
    @Column(name = "KHZTXM_WXZHGL")
    private String khztxm_wxzhgl;

    /**
     * 绑定手机号《微信账号关联》: VARCHAR
     */
    @Column(name = "BDSJH_WXZHGL")
    private String bdsjh_wxzhgl;

    /**
     * 昆明到港次数《微信关联》: DECIMAL
     */
    @Column(name = "KMDGCS_WXGL")
    private Integer kmdgcs_wxgl;

    /**
     * 对方微信号: VARCHAR
     */
    @Column(name = "DFWXH")
    private String dfwxh;

    /**
     * 已调明细2: VARCHAR
     */
    @Column(name = "YDMX2")
    private String ydmx2;

    /**
     * 《我方》最后交易时间: VARCHAR
     */
    @Column(name = "WFZHJYSJ")
    private String wfzhjysj;

    /**
     * 《我方》最后交易情况: VARCHAR
     */
    @Column(name = "WFZHJYQK")
    private String wfzhjyqk;

    /**
     * 我方支付账号交易次数: DECIMAL
     */
    @Column(name = "WFZFZHJYCS")
    private Integer wfzfzhjycs;

    /**
     * 《我方》最早交易时间: VARCHAR
     */
    @Column(name = "WFZZJYSJ")
    private String wfzzjysj;

    /**
     * 《我方》最早交易情况: VARCHAR
     */
    @Column(name = "WFZZJYQK")
    private String wfzzjyqk;

    /**
     * 绑定银行卡《我方支付》: VARCHAR
     */
    @Column(name = "BDYHK_WFZF")
    private String bdyhk_wfzf;

    /**
     * 证件号《我方支付》: VARCHAR
     */
    @Column(name = "ZJH_WFZF")
    private String zjh_wfzf;

    /**
     * 开户姓名《我方支付》: VARCHAR
     */
    @Column(name = "KHXM_WFZF")
    private String khxm_wfzf;

    /**
     * 绑定手机号《我方支付》: VARCHAR
     */
    @Column(name = "BDSJH_WFZF")
    private String bdsjh_wfzf;

    /**
     * 高位户籍《我方支付》: DECIMAL
     */
    @Column(name = "GWHJ_WFZF")
    private Integer gwhj_wfzf;

    /**
     * 微信号新《我方支付》: VARCHAR
     */
    @Column(name = "WXZH_X_WFZF")
    private String wxzh_x_wfzf;

    /**
     * 昆明到港次数（我方证件）: DECIMAL
     */
    @Column(name = "KMDGCS_WFZJ")
    private Integer kmdgcs_wfzj;

    /**
     * 财付通账号: VARCHAR
     */
    @Column(name = "CFTZH")
    private String cftzh;

    /**
     * 微信号: VARCHAR
     */
    @Column(name = "WXH")
    private String wxh;

    /**
     * 财付通开户人: VARCHAR
     */
    @Column(name = "CFTKHR")
    private String cftkhr;

    /**
     * 财付通开户证件: VARCHAR
     */
    @Column(name = "CFTKHZJ")
    private String cftkhzj;

    /**
     * 财付通绑定手机: VARCHAR
     */
    @Column(name = "CFTBDSJH")
    private String cftbdsjh;

    /**
     * 《对方》高危户籍: DECIMAL
     */
    @Column(name = "DF_GWHJ")
    private Integer df_gwhj;

    /**
     * 出账次数: DECIMAL
     */
    @Column(name = "CZCS")
    private Integer czcs;

    /**
     * 出账金额: DECIMAL
     */
    @Column(name = "CZJE")
    private Integer czje;

    /**
     * 入账次数: DECIMAL
     */
    @Column(name = "RZCS")
    private Integer rzcs;

    /**
     * 入账金额: DECIMAL
     */
    @Column(name = "RZJE")
    private Integer rzje;

    /**
     * 资金关系人数: DECIMAL
     */
    @Column(name = "ZJGXRS")
    private Integer zjgxrs;

    /**
     * 涉毒资金关系人数: DECIMAL
     */
    @Column(name = "SDZJGXRS")
    private Integer sdzjgxrs;

    /**
     * 对方账号入账人数: DECIMAL
     */
    @Column(name = "DFZHRZRS")
    private Integer dfzhrzrs;

    /**
     * 对方账号出账人数: DECIMAL
     */
    @Column(name = "DFZHCZRS")
    private Integer dfzhczrs;

    /**
     * 对方入账涉毒人数: DECIMAL
     */
    @Column(name = "DFRZSDRS")
    private Integer dfrzsdrs;

    /**
     * 对方出账涉毒人数: DECIMAL
     */
    @Column(name = "DFCZSDRS")
    private Integer dfczsdrs;

    /**
     * 敏感时段占比%: VARCHAR
     */
    @Column(name = "MFSDZB")
    private String mfsdzb;

    /**
     * 整数交易占比%: VARCHAR
     */
    @Column(name = "ZSJYZB")
    private String zsjyzb;

    /**
     * 是否本地户籍人员对手: VARCHAR
     */
    @Column(name = "SFBDHJRYDS")
    private String sfbdhjryds;

    /**
     * 最后交易时间: VARCHAR
     */
    @Column(name = "ZHJYSJ")
    private String zhjysj;

    /**
     * 最后交易情况: VARCHAR
     */
    @Column(name = "ZHJYQK")
    private String zhjyqk;

    /**
     * 《我方》微信号新1: VARCHAR
     */
    @Column(name = "RZ_WF_WXZH_X1")
    private String rz_wf_wxzh_x1;

    /**
     * 《我方》入账交易对手数: DECIMAL
     */
    @Column(name = "RZ_JYDSS")
    private Integer rz_jydss;

    /**
     * 《我方》入账交易次数: DECIMAL
     */
    @Column(name = "RZ_JYCS")
    private Integer rz_jycs;

    /**
     * 《我方》入账交易金额: DECIMAL
     */
    @Column(name = "RZ_JYJE")
    private Integer rz_jyje;

    /**
     * 《我方》已调入账交易对手数: DECIMAL
     */
    @Column(name = "RZ_YD_JYDSS")
    private Integer rz_yd_jydss;

    /**
     * 《我方》入账涉毒对手《人员表》: DECIMAL
     */
    @Column(name = "RZ_DF_SDZJHM_RYB")
    private Integer rz_df_sdzjhm_ryb;

    /**
     * 《我方》入账涉赌人数: DECIMAL
     */
    @Column(name = "RZ_SDRS")
    private Integer rz_sdrs;

    /**
     * 《我方》入账涉毒对手《微信关联人》: DECIMAL
     */
    @Column(name = "RZ_DF_ZJHM_WXGLR")
    private Integer rz_df_zjhm_wxglr;

    /**
     * 《我方》入账敏感时段率: VARCHAR
     */
    @Column(name = "RZ_MGSJL")
    private String rz_mgsjl;

    /**
     * 《我方》入账整数交易率: VARCHAR
     */
    @Column(name = "RZ_ZSJYL")
    private String rz_zsjyl;

    /**
     * 《我方》入账涉毒前科金额: DECIMAL
     */
    @Column(name = "RZ_SDQKJE")
    private Integer rz_sdqkje;

    /**
     * 《我方》入账涉赌前科金额: DECIMAL
     */
    @Column(name = "RZ_SDQKJE1")
    private Integer rz_sdqkje1;

    /**
     * 《我方》出账交易对手数: DECIMAL
     */
    @Column(name = "CZ_JYDSS")
    private Integer cz_jydss;

    /**
     * 《我方》出账交易次数: DECIMAL
     */
    @Column(name = "CZ_JYCS")
    private Integer cz_jycs;

    /**
     * 《我方》出账交易金额: DECIMAL
     */
    @Column(name = "CZ_JYJE")
    private Integer cz_jyje;

    /**
     * 《我方》出账已调交易对手数: DECIMAL
     */
    @Column(name = "CZ_YD_JYDSS")
    private Integer cz_yd_jydss;

    /**
     * 《我方》出账涉毒对手《人员表》: DECIMAL
     */
    @Column(name = "CZ_DF_SDZJHM_RYB")
    private Integer cz_df_sdzjhm_ryb;

    /**
     * 《我方》出账涉赌人数: DECIMAL
     */
    @Column(name = "CZ_SDRS")
    private Integer cz_sdrs;

    /**
     * 《我方》出账涉毒对手《微信关联人》: DECIMAL
     */
    @Column(name = "CZ_DF_ZJHM_WXGLR")
    private Integer cz_df_zjhm_wxglr;

    /**
     * 《我方》出账敏感时段率: VARCHAR
     */
    @Column(name = "CZ_MGSJL")
    private String cz_mgsjl;

    /**
     * 《我方》出账整数交易率: VARCHAR
     */
    @Column(name = "CZ_ZSJYL")
    private String cz_zsjyl;

    /**
     * 《我方》出账涉毒前科金额: DECIMAL
     */
    @Column(name = "CZ_SDQKJE")
    private Integer cz_sdqkje;

    /**
     * 《我方》出账涉赌前科金额: DECIMAL
     */
    @Column(name = "CZ_SDQKJE1")
    private Integer cz_sdqkje1;

    /**
     * 《我方》总交易次数: DECIMAL
     */
    @Column(name = "ZJYCS")
    private Integer zjycs;

    /**
     * 《我方》总交易对手: DECIMAL
     */
    @Column(name = "ZJYDS")
    private Integer zjyds;

    /**
     * 《我方》总交易金额: DECIMAL
     */
    @Column(name = "ZJYJE")
    private Integer zjyje;

    /**
     * 《我方》已调交易对手: DECIMAL
     */
    @Column(name = "YDJYDS")
    private Integer ydjyds;

    /**
     * 《我方》涉毒对手《人员表》: DECIMAL
     */
    @Column(name = "DF_SDZJHM_RYB")
    private Integer df_sdzjhm_ryb;

    /**
     * 《我方》涉毒对手《微信关联人》: DECIMAL
     */
    @Column(name = "DF_ZJHM_WXGLR")
    private Integer df_zjhm_wxglr;

    /**
     * 《我方》微信账号新2: VARCHAR
     */
    @Column(name = "WF_WXZH_X1")
    private String wf_wxzh_x1;

    /**
     * 《我方》高危户籍: DECIMAL
     */
    @Column(name = "WF_GWHJ")
    private Integer wf_gwhj;

    /**
     * 上家: VARCHAR
     */
    @Column(name = "SJ")
    private String sj;

    /**
     * 下家: VARCHAR
     */
    @Column(name = "XJ")
    private String xj;

    /**
     * 我方上家: VARCHAR
     */
    @Column(name = "WFSJ")
    private String wfsj;

    /**
     * 我方下家: VARCHAR
     */
    @Column(name = "WFXJ")
    private String wfxj;

    /**
     * 敏感时段率新: VARCHAR
     */
    @Column(name = "MGSDLX")
    private String mgsdlx;

    /**
     * 整数交易率新: VARCHAR
     */
    @Column(name = "ZSJYLX")
    private String zsjylx;

    /**
     * 最早交易时间(新): VARCHAR
     */
    @Column(name = "ZZJYSJX")
    private String zzjysjx;

    /**
     * 最后交易情况新: VARCHAR
     */
    @Column(name = "ZHJYQKX")
    private String zhjyqkx;

    /**
     * 昆明到港次数新: DECIMAL
     */
    @Column(name = "KMDGCSX")
    private Integer kmdgcsx;

    /**
     * 是否调取明细A: VARCHAR
     */
    @Column(name = "SFDQMXA")
    private String sfdqmxa;

    /**
     * 绑定银行卡新A: VARCHAR
     */
    @Column(name = "BDYHKXA")
    private String bdyhkxa;

    /**
     * 最后交易时间新: VARCHAR
     */
    @Column(name = "ZHJYSJX")
    private String zhjysjx;

    /**
     * 绑定手机号码新: VARCHAR
     */
    @Column(name = "BDSJHMX")
    private String bdsjhmx;

    /**
     * 财富通开户证件（新）: VARCHAR
     */
    @Column(name = "CFTKHZJX")
    private String cftkhzjx;

    /**
     * 财富通开户人（新）: VARCHAR
     */
    @Column(name = "CFTKHRX")
    private String cftkhrx;

    /**
     * 出账涉毒人数占比: DECIMAL
     */
    @Column(name = "CZSDRHZB")
    private Integer czsdrhzb;

    /**
     * 入账涉毒人数占比: DECIMAL
     */
    @Column(name = "RZSDRSZB")
    private Integer rzsdrszb;

    /**
     * 最后交易时间日期格式: VARCHAR
     */
    @Column(name = "ZHJYSJRQGS")
    private String zhjysjrqgs;

    /**
     * 高危户籍新: DECIMAL
     */
    @Column(name = "GWHJX")
    private Integer gwhjx;

    /**
     * 涉毒资金关系人数A: DECIMAL
     */
    @Column(name = "SDZIGXRSA")
    private Integer sdzigxrsa;

    /**
     * 入账人数新A: VARCHAR
     */
    @Column(name = "RZRSXA")
    private String rzrsxa;

    /**
     * 入账涉毒人数新A: DECIMAL
     */
    @Column(name = "RZSDRSXA")
    private Integer rzsdrsxa;

    /**
     * 入账金额新A: DECIMAL
     */
    @Column(name = "RZJEXA")
    private Integer rzjexa;

    /**
     * 入账次数新A: DECIMAL
     */
    @Column(name = "RZCSXA")
    private Integer rzcsxa;

    /**
     * 出账人数新A: DECIMAL
     */
    @Column(name = "CZRSXA")
    private Integer czrsxa;

    /**
     * 出账涉毒人数新A: DECIMAL
     */
    @Column(name = "CZSDRSXA")
    private Integer czsdrsxa;

    /**
     * 出账金额新A: DECIMAL
     */
    @Column(name = "CZJEXA")
    private Integer czjexa;

    /**
     * 已调资金关系人数: VARCHAR
     */
    @Column(name = "YDZJGXRS")
    private String ydzjgxrs;

    /**
     * 微信号新: VARCHAR
     */
    @Column(name = "WXHX")
    private String wxhx;

    /**
     * 《我方》敏感时段率: DECIMAL
     */
    @Column(name = "WFMGSDL")
    private Integer wfmgsdl;

    /**
     * 《我方》整数交易率: DECIMAL
     */
    @Column(name = "WFZSJYL")
    private Integer wfzsjyl;

    /**
     * 开户资料: VARCHAR
     */
    @Column(name = "KHZL")
    private String khzl;

    /**
     * 经销商积分描述新A: VARCHAR
     */
    @Column(name = "JXSDFMSXA")
    private String jxsdfmsxa;

    /**
     * 经销商积分新A: VARCHAR
     */
    @Column(name = "JXSDFXA")
    private String jxsdfxa;

    /**
     * 研判报告: VARCHAR
     */
    @Column(name = "YPBG")
    private String ypbg;

    /**
     * 涉毒好友: VARCHAR
     */
    @Column(name = "SDHY")
    private String sdhy;

    /**
     * 证件号码《微信通联》新: VARCHAR
     */
    @Column(name = "ZJHM_WXLT_X")
    private String zjhm_wxlt_x;

    /**
     * 姓名《微信通联》新: VARCHAR
     */
    @Column(name = "XM_WXLT_X")
    private String xm_wxlt_x;

    /**
     * 微信通联积分描述新: VARCHAR
     */
    @Column(name = "WXTLDFMSX")
    private String wxtldfmsx;

    /**
     * 微信通联积分新: VARCHAR
     */
    @Column(name = "WXTLDFX")
    private String wxtldfx;

    /**
     * 微信昵称新: VARCHAR
     */
    @Column(name = "WXNCX")
    private String wxncx;

    /**
     * 吸毒仔积分描述: VARCHAR
     */
    @Column(name = "XDZDFMS")
    private String xdzdfms;

    /**
     * 积分《吸毒仔》: VARCHAR
     */
    @Column(name = "DF_XDZ")
    private String df_xdz;

    /**
     * 积分《分销商》旧: VARCHAR
     */
    @Column(name = "DF_FXS_J")
    private String df_fxs_j;

    /**
     * 经销商积分描述: VARCHAR
     */
    @Column(name = "JXSDFMS")
    private String jxsdfms;

    /**
     * 上家新: VARCHAR
     */
    @Column(name = "SJX")
    private String sjx;

    /**
     * 下家新: VARCHAR
     */
    @Column(name = "XJX")
    private String xjx;

    public void setZhjysjrqx(String zhjysjrqx) {
        this.zhjysjrqx = zhjysjrqx;
    }

    public String getZhjysjrqx() {
        return zhjysjrqx;
    }

    public void setYdmx(String ydmx) {
        this.ydmx = ydmx;
    }

    public String getYdmx() {
        return ydmx;
    }

    public void setKmdgcs_zjgl(Integer kmdgcs_zjgl) {
        this.kmdgcs_zjgl = kmdgcs_zjgl;
    }

    public Integer getKmdgcs_zjgl() {
        return kmdgcs_zjgl;
    }

    public void setDfzfzhjycs(Integer dfzfzhjycs) {
        this.dfzfzhjycs = dfzfzhjycs;
    }

    public Integer getDfzfzhjycs() {
        return dfzfzhjycs;
    }

    public void setDfzhjysj(String dfzhjysj) {
        this.dfzhjysj = dfzhjysj;
    }

    public String getDfzhjysj() {
        return dfzhjysj;
    }

    public void setDfzhjyqk(String dfzhjyqk) {
        this.dfzhjyqk = dfzhjyqk;
    }

    public String getDfzhjyqk() {
        return dfzhjyqk;
    }

    public void setDfzzjysj(String dfzzjysj) {
        this.dfzzjysj = dfzzjysj;
    }

    public String getDfzzjysj() {
        return dfzzjysj;
    }

    public void setDfzzjyqk(String dfzzjyqk) {
        this.dfzzjyqk = dfzzjyqk;
    }

    public String getDfzzjyqk() {
        return dfzzjyqk;
    }

    public void setWfzhjyye(String wfzhjyye) {
        this.wfzhjyye = wfzhjyye;
    }

    public String getWfzhjyye() {
        return wfzhjyye;
    }

    public void setWfzhlyaj(String wfzhlyaj) {
        this.wfzhlyaj = wfzhlyaj;
    }

    public String getWfzhlyaj() {
        return wfzhlyaj;
    }

    public void setWfzhjysj1(String wfzhjysj1) {
        this.wfzhjysj1 = wfzhjysj1;
    }

    public String getWfzhjysj1() {
        return wfzhjysj1;
    }

    public void setWfzhjyqk1(String wfzhjyqk1) {
        this.wfzhjyqk1 = wfzhjyqk1;
    }

    public String getWfzhjyqk1() {
        return wfzhjyqk1;
    }

    public void setQbqkxx(String qbqkxx) {
        this.qbqkxx = qbqkxx;
    }

    public String getQbqkxx() {
        return qbqkxx;
    }

    public void setZjzhsj(Date zjzhsj) {
        this.zjzhsj = zjzhsj;
    }

    public Date getZjzhsj() {
        return zjzhsj;
    }

    public void setZjzhsjly(String zjzhsjly) {
        this.zjzhsjly = zjzhsjly;
    }

    public String getZjzhsjly() {
        return zjzhsjly;
    }

    public void setQkryhjsd(String qkryhjsd) {
        this.qkryhjsd = qkryhjsd;
    }

    public String getQkryhjsd() {
        return qkryhjsd;
    }

    public void setZjzhrylbzw(String zjzhrylbzw) {
        this.zjzhrylbzw = zjzhrylbzw;
    }

    public String getZjzhrylbzw() {
        return zjzhrylbzw;
    }

    public void setQksldw(String qksldw) {
        this.qksldw = qksldw;
    }

    public String getQksldw() {
        return qksldw;
    }

    public void setZjzhqk(String zjzhqk) {
        this.zjzhqk = zjzhqk;
    }

    public String getZjzhqk() {
        return zjzhqk;
    }

    public void setBdyhk_wxzhgl(String bdyhk_wxzhgl) {
        this.bdyhk_wxzhgl = bdyhk_wxzhgl;
    }

    public String getBdyhk_wxzhgl() {
        return bdyhk_wxzhgl;
    }

    public void setKhztzjhm_wxzhgl(String khztzjhm_wxzhgl) {
        this.khztzjhm_wxzhgl = khztzjhm_wxzhgl;
    }

    public String getKhztzjhm_wxzhgl() {
        return khztzjhm_wxzhgl;
    }

    public void setKhztxm_wxzhgl(String khztxm_wxzhgl) {
        this.khztxm_wxzhgl = khztxm_wxzhgl;
    }

    public String getKhztxm_wxzhgl() {
        return khztxm_wxzhgl;
    }

    public void setBdsjh_wxzhgl(String bdsjh_wxzhgl) {
        this.bdsjh_wxzhgl = bdsjh_wxzhgl;
    }

    public String getBdsjh_wxzhgl() {
        return bdsjh_wxzhgl;
    }

    public void setKmdgcs_wxgl(Integer kmdgcs_wxgl) {
        this.kmdgcs_wxgl = kmdgcs_wxgl;
    }

    public Integer getKmdgcs_wxgl() {
        return kmdgcs_wxgl;
    }

    public void setDfwxh(String dfwxh) {
        this.dfwxh = dfwxh;
    }

    public String getDfwxh() {
        return dfwxh;
    }

    public void setYdmx2(String ydmx2) {
        this.ydmx2 = ydmx2;
    }

    public String getYdmx2() {
        return ydmx2;
    }

    public void setWfzhjysj(String wfzhjysj) {
        this.wfzhjysj = wfzhjysj;
    }

    public String getWfzhjysj() {
        return wfzhjysj;
    }

    public void setWfzhjyqk(String wfzhjyqk) {
        this.wfzhjyqk = wfzhjyqk;
    }

    public String getWfzhjyqk() {
        return wfzhjyqk;
    }

    public void setWfzfzhjycs(Integer wfzfzhjycs) {
        this.wfzfzhjycs = wfzfzhjycs;
    }

    public Integer getWfzfzhjycs() {
        return wfzfzhjycs;
    }

    public void setWfzzjysj(String wfzzjysj) {
        this.wfzzjysj = wfzzjysj;
    }

    public String getWfzzjysj() {
        return wfzzjysj;
    }

    public void setWfzzjyqk(String wfzzjyqk) {
        this.wfzzjyqk = wfzzjyqk;
    }

    public String getWfzzjyqk() {
        return wfzzjyqk;
    }

    public void setBdyhk_wfzf(String bdyhk_wfzf) {
        this.bdyhk_wfzf = bdyhk_wfzf;
    }

    public String getBdyhk_wfzf() {
        return bdyhk_wfzf;
    }

    public void setZjh_wfzf(String zjh_wfzf) {
        this.zjh_wfzf = zjh_wfzf;
    }

    public String getZjh_wfzf() {
        return zjh_wfzf;
    }

    public void setKhxm_wfzf(String khxm_wfzf) {
        this.khxm_wfzf = khxm_wfzf;
    }

    public String getKhxm_wfzf() {
        return khxm_wfzf;
    }

    public void setBdsjh_wfzf(String bdsjh_wfzf) {
        this.bdsjh_wfzf = bdsjh_wfzf;
    }

    public String getBdsjh_wfzf() {
        return bdsjh_wfzf;
    }

    public void setGwhj_wfzf(Integer gwhj_wfzf) {
        this.gwhj_wfzf = gwhj_wfzf;
    }

    public Integer getGwhj_wfzf() {
        return gwhj_wfzf;
    }

    public void setWxzh_x_wfzf(String wxzh_x_wfzf) {
        this.wxzh_x_wfzf = wxzh_x_wfzf;
    }

    public String getWxzh_x_wfzf() {
        return wxzh_x_wfzf;
    }

    public void setKmdgcs_wfzj(Integer kmdgcs_wfzj) {
        this.kmdgcs_wfzj = kmdgcs_wfzj;
    }

    public Integer getKmdgcs_wfzj() {
        return kmdgcs_wfzj;
    }

    public void setCftzh(String cftzh) {
        this.cftzh = cftzh;
    }

    public String getCftzh() {
        return cftzh;
    }

    public void setWxh(String wxh) {
        this.wxh = wxh;
    }

    public String getWxh() {
        return wxh;
    }

    public void setCftkhr(String cftkhr) {
        this.cftkhr = cftkhr;
    }

    public String getCftkhr() {
        return cftkhr;
    }

    public void setCftkhzj(String cftkhzj) {
        this.cftkhzj = cftkhzj;
    }

    public String getCftkhzj() {
        return cftkhzj;
    }

    public void setCftbdsjh(String cftbdsjh) {
        this.cftbdsjh = cftbdsjh;
    }

    public String getCftbdsjh() {
        return cftbdsjh;
    }

    public void setDf_gwhj(Integer df_gwhj) {
        this.df_gwhj = df_gwhj;
    }

    public Integer getDf_gwhj() {
        return df_gwhj;
    }

    public void setCzcs(Integer czcs) {
        this.czcs = czcs;
    }

    public Integer getCzcs() {
        return czcs;
    }

    public void setCzje(Integer czje) {
        this.czje = czje;
    }

    public Integer getCzje() {
        return czje;
    }

    public void setRzcs(Integer rzcs) {
        this.rzcs = rzcs;
    }

    public Integer getRzcs() {
        return rzcs;
    }

    public void setRzje(Integer rzje) {
        this.rzje = rzje;
    }

    public Integer getRzje() {
        return rzje;
    }

    public void setZjgxrs(Integer zjgxrs) {
        this.zjgxrs = zjgxrs;
    }

    public Integer getZjgxrs() {
        return zjgxrs;
    }

    public void setSdzjgxrs(Integer sdzjgxrs) {
        this.sdzjgxrs = sdzjgxrs;
    }

    public Integer getSdzjgxrs() {
        return sdzjgxrs;
    }

    public void setDfzhrzrs(Integer dfzhrzrs) {
        this.dfzhrzrs = dfzhrzrs;
    }

    public Integer getDfzhrzrs() {
        return dfzhrzrs;
    }

    public void setDfzhczrs(Integer dfzhczrs) {
        this.dfzhczrs = dfzhczrs;
    }

    public Integer getDfzhczrs() {
        return dfzhczrs;
    }

    public void setDfrzsdrs(Integer dfrzsdrs) {
        this.dfrzsdrs = dfrzsdrs;
    }

    public Integer getDfrzsdrs() {
        return dfrzsdrs;
    }

    public void setDfczsdrs(Integer dfczsdrs) {
        this.dfczsdrs = dfczsdrs;
    }

    public Integer getDfczsdrs() {
        return dfczsdrs;
    }

    public void setMfsdzb(String mfsdzb) {
        this.mfsdzb = mfsdzb;
    }

    public String getMfsdzb() {
        return mfsdzb;
    }

    public void setZsjyzb(String zsjyzb) {
        this.zsjyzb = zsjyzb;
    }

    public String getZsjyzb() {
        return zsjyzb;
    }

    public void setSfbdhjryds(String sfbdhjryds) {
        this.sfbdhjryds = sfbdhjryds;
    }

    public String getSfbdhjryds() {
        return sfbdhjryds;
    }

    public void setZhjysj(String zhjysj) {
        this.zhjysj = zhjysj;
    }

    public String getZhjysj() {
        return zhjysj;
    }

    public void setZhjyqk(String zhjyqk) {
        this.zhjyqk = zhjyqk;
    }

    public String getZhjyqk() {
        return zhjyqk;
    }

    public void setRz_wf_wxzh_x1(String rz_wf_wxzh_x1) {
        this.rz_wf_wxzh_x1 = rz_wf_wxzh_x1;
    }

    public String getRz_wf_wxzh_x1() {
        return rz_wf_wxzh_x1;
    }

    public void setRz_jydss(Integer rz_jydss) {
        this.rz_jydss = rz_jydss;
    }

    public Integer getRz_jydss() {
        return rz_jydss;
    }

    public void setRz_jycs(Integer rz_jycs) {
        this.rz_jycs = rz_jycs;
    }

    public Integer getRz_jycs() {
        return rz_jycs;
    }

    public void setRz_jyje(Integer rz_jyje) {
        this.rz_jyje = rz_jyje;
    }

    public Integer getRz_jyje() {
        return rz_jyje;
    }

    public void setRz_yd_jydss(Integer rz_yd_jydss) {
        this.rz_yd_jydss = rz_yd_jydss;
    }

    public Integer getRz_yd_jydss() {
        return rz_yd_jydss;
    }

    public void setRz_df_sdzjhm_ryb(Integer rz_df_sdzjhm_ryb) {
        this.rz_df_sdzjhm_ryb = rz_df_sdzjhm_ryb;
    }

    public Integer getRz_df_sdzjhm_ryb() {
        return rz_df_sdzjhm_ryb;
    }

    public void setRz_sdrs(Integer rz_sdrs) {
        this.rz_sdrs = rz_sdrs;
    }

    public Integer getRz_sdrs() {
        return rz_sdrs;
    }

    public void setRz_df_zjhm_wxglr(Integer rz_df_zjhm_wxglr) {
        this.rz_df_zjhm_wxglr = rz_df_zjhm_wxglr;
    }

    public Integer getRz_df_zjhm_wxglr() {
        return rz_df_zjhm_wxglr;
    }

    public void setRz_mgsjl(String rz_mgsjl) {
        this.rz_mgsjl = rz_mgsjl;
    }

    public String getRz_mgsjl() {
        return rz_mgsjl;
    }

    public void setRz_zsjyl(String rz_zsjyl) {
        this.rz_zsjyl = rz_zsjyl;
    }

    public String getRz_zsjyl() {
        return rz_zsjyl;
    }

    public void setRz_sdqkje(Integer rz_sdqkje) {
        this.rz_sdqkje = rz_sdqkje;
    }

    public Integer getRz_sdqkje() {
        return rz_sdqkje;
    }

    public void setRz_sdqkje1(Integer rz_sdqkje1) {
        this.rz_sdqkje1 = rz_sdqkje1;
    }

    public Integer getRz_sdqkje1() {
        return rz_sdqkje1;
    }

    public void setCz_jydss(Integer cz_jydss) {
        this.cz_jydss = cz_jydss;
    }

    public Integer getCz_jydss() {
        return cz_jydss;
    }

    public void setCz_jycs(Integer cz_jycs) {
        this.cz_jycs = cz_jycs;
    }

    public Integer getCz_jycs() {
        return cz_jycs;
    }

    public void setCz_jyje(Integer cz_jyje) {
        this.cz_jyje = cz_jyje;
    }

    public Integer getCz_jyje() {
        return cz_jyje;
    }

    public void setCz_yd_jydss(Integer cz_yd_jydss) {
        this.cz_yd_jydss = cz_yd_jydss;
    }

    public Integer getCz_yd_jydss() {
        return cz_yd_jydss;
    }

    public void setCz_df_sdzjhm_ryb(Integer cz_df_sdzjhm_ryb) {
        this.cz_df_sdzjhm_ryb = cz_df_sdzjhm_ryb;
    }

    public Integer getCz_df_sdzjhm_ryb() {
        return cz_df_sdzjhm_ryb;
    }

    public void setCz_sdrs(Integer cz_sdrs) {
        this.cz_sdrs = cz_sdrs;
    }

    public Integer getCz_sdrs() {
        return cz_sdrs;
    }

    public void setCz_df_zjhm_wxglr(Integer cz_df_zjhm_wxglr) {
        this.cz_df_zjhm_wxglr = cz_df_zjhm_wxglr;
    }

    public Integer getCz_df_zjhm_wxglr() {
        return cz_df_zjhm_wxglr;
    }

    public void setCz_mgsjl(String cz_mgsjl) {
        this.cz_mgsjl = cz_mgsjl;
    }

    public String getCz_mgsjl() {
        return cz_mgsjl;
    }

    public void setCz_zsjyl(String cz_zsjyl) {
        this.cz_zsjyl = cz_zsjyl;
    }

    public String getCz_zsjyl() {
        return cz_zsjyl;
    }

    public void setCz_sdqkje(Integer cz_sdqkje) {
        this.cz_sdqkje = cz_sdqkje;
    }

    public Integer getCz_sdqkje() {
        return cz_sdqkje;
    }

    public void setCz_sdqkje1(Integer cz_sdqkje1) {
        this.cz_sdqkje1 = cz_sdqkje1;
    }

    public Integer getCz_sdqkje1() {
        return cz_sdqkje1;
    }

    public void setZjycs(Integer zjycs) {
        this.zjycs = zjycs;
    }

    public Integer getZjycs() {
        return zjycs;
    }

    public void setZjyds(Integer zjyds) {
        this.zjyds = zjyds;
    }

    public Integer getZjyds() {
        return zjyds;
    }

    public void setZjyje(Integer zjyje) {
        this.zjyje = zjyje;
    }

    public Integer getZjyje() {
        return zjyje;
    }

    public void setYdjyds(Integer ydjyds) {
        this.ydjyds = ydjyds;
    }

    public Integer getYdjyds() {
        return ydjyds;
    }

    public void setDf_sdzjhm_ryb(Integer df_sdzjhm_ryb) {
        this.df_sdzjhm_ryb = df_sdzjhm_ryb;
    }

    public Integer getDf_sdzjhm_ryb() {
        return df_sdzjhm_ryb;
    }

    public void setDf_zjhm_wxglr(Integer df_zjhm_wxglr) {
        this.df_zjhm_wxglr = df_zjhm_wxglr;
    }

    public Integer getDf_zjhm_wxglr() {
        return df_zjhm_wxglr;
    }

    public void setWf_wxzh_x1(String wf_wxzh_x1) {
        this.wf_wxzh_x1 = wf_wxzh_x1;
    }

    public String getWf_wxzh_x1() {
        return wf_wxzh_x1;
    }

    public void setWf_gwhj(Integer wf_gwhj) {
        this.wf_gwhj = wf_gwhj;
    }

    public Integer getWf_gwhj() {
        return wf_gwhj;
    }

    public void setSj(String sj) {
        this.sj = sj;
    }

    public String getSj() {
        return sj;
    }

    public void setXj(String xj) {
        this.xj = xj;
    }

    public String getXj() {
        return xj;
    }

    public void setWfsj(String wfsj) {
        this.wfsj = wfsj;
    }

    public String getWfsj() {
        return wfsj;
    }

    public void setWfxj(String wfxj) {
        this.wfxj = wfxj;
    }

    public String getWfxj() {
        return wfxj;
    }

    public void setMgsdlx(String mgsdlx) {
        this.mgsdlx = mgsdlx;
    }

    public String getMgsdlx() {
        return mgsdlx;
    }

    public void setZsjylx(String zsjylx) {
        this.zsjylx = zsjylx;
    }

    public String getZsjylx() {
        return zsjylx;
    }

    public void setZzjysjx(String zzjysjx) {
        this.zzjysjx = zzjysjx;
    }

    public String getZzjysjx() {
        return zzjysjx;
    }

    public void setZhjyqkx(String zhjyqkx) {
        this.zhjyqkx = zhjyqkx;
    }

    public String getZhjyqkx() {
        return zhjyqkx;
    }

    public void setKmdgcsx(Integer kmdgcsx) {
        this.kmdgcsx = kmdgcsx;
    }

    public Integer getKmdgcsx() {
        return kmdgcsx;
    }

    public void setSfdqmxa(String sfdqmxa) {
        this.sfdqmxa = sfdqmxa;
    }

    public String getSfdqmxa() {
        return sfdqmxa;
    }

    public void setBdyhkxa(String bdyhkxa) {
        this.bdyhkxa = bdyhkxa;
    }

    public String getBdyhkxa() {
        return bdyhkxa;
    }

    public void setZhjysjx(String zhjysjx) {
        this.zhjysjx = zhjysjx;
    }

    public String getZhjysjx() {
        return zhjysjx;
    }

    public void setBdsjhmx(String bdsjhmx) {
        this.bdsjhmx = bdsjhmx;
    }

    public String getBdsjhmx() {
        return bdsjhmx;
    }

    public void setCftkhzjx(String cftkhzjx) {
        this.cftkhzjx = cftkhzjx;
    }

    public String getCftkhzjx() {
        return cftkhzjx;
    }

    public void setCftkhrx(String cftkhrx) {
        this.cftkhrx = cftkhrx;
    }

    public String getCftkhrx() {
        return cftkhrx;
    }

    public void setCzsdrhzb(Integer czsdrhzb) {
        this.czsdrhzb = czsdrhzb;
    }

    public Integer getCzsdrhzb() {
        return czsdrhzb;
    }

    public void setRzsdrszb(Integer rzsdrszb) {
        this.rzsdrszb = rzsdrszb;
    }

    public Integer getRzsdrszb() {
        return rzsdrszb;
    }

    public void setZhjysjrqgs(String zhjysjrqgs) {
        this.zhjysjrqgs = zhjysjrqgs;
    }

    public String getZhjysjrqgs() {
        return zhjysjrqgs;
    }

    public void setGwhjx(Integer gwhjx) {
        this.gwhjx = gwhjx;
    }

    public Integer getGwhjx() {
        return gwhjx;
    }

    public void setSdzigxrsa(Integer sdzigxrsa) {
        this.sdzigxrsa = sdzigxrsa;
    }

    public Integer getSdzigxrsa() {
        return sdzigxrsa;
    }

    public void setRzrsxa(String rzrsxa) {
        this.rzrsxa = rzrsxa;
    }

    public String getRzrsxa() {
        return rzrsxa;
    }

    public void setRzsdrsxa(Integer rzsdrsxa) {
        this.rzsdrsxa = rzsdrsxa;
    }

    public Integer getRzsdrsxa() {
        return rzsdrsxa;
    }

    public void setRzjexa(Integer rzjexa) {
        this.rzjexa = rzjexa;
    }

    public Integer getRzjexa() {
        return rzjexa;
    }

    public void setRzcsxa(Integer rzcsxa) {
        this.rzcsxa = rzcsxa;
    }

    public Integer getRzcsxa() {
        return rzcsxa;
    }

    public void setCzrsxa(Integer czrsxa) {
        this.czrsxa = czrsxa;
    }

    public Integer getCzrsxa() {
        return czrsxa;
    }

    public void setCzsdrsxa(Integer czsdrsxa) {
        this.czsdrsxa = czsdrsxa;
    }

    public Integer getCzsdrsxa() {
        return czsdrsxa;
    }

    public void setCzjexa(Integer czjexa) {
        this.czjexa = czjexa;
    }

    public Integer getCzjexa() {
        return czjexa;
    }

    public void setYdzjgxrs(String ydzjgxrs) {
        this.ydzjgxrs = ydzjgxrs;
    }

    public String getYdzjgxrs() {
        return ydzjgxrs;
    }

    public void setWxhx(String wxhx) {
        this.wxhx = wxhx;
    }

    public String getWxhx() {
        return wxhx;
    }

    public void setWfmgsdl(Integer wfmgsdl) {
        this.wfmgsdl = wfmgsdl;
    }

    public Integer getWfmgsdl() {
        return wfmgsdl;
    }

    public void setWfzsjyl(Integer wfzsjyl) {
        this.wfzsjyl = wfzsjyl;
    }

    public Integer getWfzsjyl() {
        return wfzsjyl;
    }

    public void setKhzl(String khzl) {
        this.khzl = khzl;
    }

    public String getKhzl() {
        return khzl;
    }

    public void setJxsdfmsxa(String jxsdfmsxa) {
        this.jxsdfmsxa = jxsdfmsxa;
    }

    public String getJxsdfmsxa() {
        return jxsdfmsxa;
    }

    public void setJxsdfxa(String jxsdfxa) {
        this.jxsdfxa = jxsdfxa;
    }

    public String getJxsdfxa() {
        return jxsdfxa;
    }

    public void setYpbg(String ypbg) {
        this.ypbg = ypbg;
    }

    public String getYpbg() {
        return ypbg;
    }

    public void setSdhy(String sdhy) {
        this.sdhy = sdhy;
    }

    public String getSdhy() {
        return sdhy;
    }

    public void setZjhm_wxlt_x(String zjhm_wxlt_x) {
        this.zjhm_wxlt_x = zjhm_wxlt_x;
    }

    public String getZjhm_wxlt_x() {
        return zjhm_wxlt_x;
    }

    public void setXm_wxlt_x(String xm_wxlt_x) {
        this.xm_wxlt_x = xm_wxlt_x;
    }

    public String getXm_wxlt_x() {
        return xm_wxlt_x;
    }

    public void setWxtldfmsx(String wxtldfmsx) {
        this.wxtldfmsx = wxtldfmsx;
    }

    public String getWxtldfmsx() {
        return wxtldfmsx;
    }

    public void setWxtldfx(String wxtldfx) {
        this.wxtldfx = wxtldfx;
    }

    public String getWxtldfx() {
        return wxtldfx;
    }

    public void setWxncx(String wxncx) {
        this.wxncx = wxncx;
    }

    public String getWxncx() {
        return wxncx;
    }

    public void setXdzdfms(String xdzdfms) {
        this.xdzdfms = xdzdfms;
    }

    public String getXdzdfms() {
        return xdzdfms;
    }

    public void setDf_xdz(String df_xdz) {
        this.df_xdz = df_xdz;
    }

    public String getDf_xdz() {
        return df_xdz;
    }

    public void setDf_fxs_j(String df_fxs_j) {
        this.df_fxs_j = df_fxs_j;
    }

    public String getDf_fxs_j() {
        return df_fxs_j;
    }

    public void setJxsdfms(String jxsdfms) {
        this.jxsdfms = jxsdfms;
    }

    public String getJxsdfms() {
        return jxsdfms;
    }

    public void setSjx(String sjx) {
        this.sjx = sjx;
    }

    public String getSjx() {
        return sjx;
    }

    public void setXjx(String xjx) {
        this.xjx = xjx;
    }

    public String getXjx() {
        return xjx;
    }

}
