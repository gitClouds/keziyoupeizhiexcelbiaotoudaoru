package com.unis.model.wxjyyjmx;

import java.util.Date;
import javax.persistence.*;
import com.unis.common.annotation.ESField;
import com.unis.common.annotation.ESTable;
/**
* 全支付账号V1.0
*
* @author Qx
* @version 1.0
* @since 2020-05-12
*/
@ESTable(name = "vzhqzfzhv1",text = "全支付账号V1.0")
public class VZhQzfzhv1  {

private static final long serialVersionUID = 1L;

/****************************************
* Basic fields
****************************************/
    /**
    * 登录号: VARCHAR
    */
    @Column(name = "DLH")
    @ESField(text = "登录号")
    private String dlh;

    /**
    * 姓名/企业名称: VARCHAR
    */
    @Column(name = "XM_QYMC")
    @ESField(text = "姓名/企业名称")
    private String xm_qymc;

    /**
    * 银行卡认证状态: VARCHAR
    */
    @Column(name = "YHKRZZT")
    @ESField(text = "银行卡认证状态")
    private String yhkrzzt;

    /**
    * 微信号: VARCHAR
    */
    @Column(name = "WXH")
    @ESField(text = "微信号")
    private String wxh;

    /**
    * 证件类型: VARCHAR
    */
    @Column(name = "ZJLX")
    @ESField(text = "证件类型")
    private String zjlx;

    /**
    * 绑定手机号: VARCHAR
    */
    @Column(name = "BDSJH")
    @ESField(text = "绑定手机号")
    private String bdsjh;

    /**
    * 查询账号: VARCHAR
    */
    @Column(name = "CXZH")
    @ESField(text = "查询账号")
    private String cxzh;

    /**
    * 支付账号: VARCHAR
    */
    @Column(name = "ZFZH")
    @ESField(text = "支付账号")
    private String zfzh;

    /**
    * 证件号: VARCHAR
    */
    @Column(name = "ZJH")
    @ESField(text = "证件号")
    private String zjh;

    /**
    * 账号类别: VARCHAR
    */
    @Column(name = "ZHLB")
    @ESField(text = "账号类别")
    private String zhlb;

    /**
    * 银行卡类型: VARCHAR
    */
    @Column(name = "YHKLX")
    @ESField(text = "银行卡类型")
    private String yhklx;

    /**
    * 证照有效期: VARCHAR
    */
    @Column(name = "ZZYXQ")
    @ESField(text = "证照有效期")
    private String zzyxq;

    /**
    * 去重字段: DECIMAL
    */
    @Column(name = "QCZD")
    @ESField(text = "去重字段")
    private Integer qczd;

    /**
    * 银行卡所属银行名称: VARCHAR
    */
    @Column(name = "YHKSSYHMC")
    @ESField(text = "银行卡所属银行名称")
    private String yhkssyhmc;

    /**
    * 银行卡有效期: VARCHAR
    */
    @Column(name = "YHKYXQ")
    @ESField(text = "银行卡有效期")
    private String yhkyxq;

    /**
    * 时间: DATE
    */
    @Column(name = "SJ")
    @ESField(text = "时间")
    private Date sj;

    /**
    * 银行卡其他信息: VARCHAR
    */
    @Column(name = "YHKQTXX")
    @ESField(text = "银行卡其他信息")
    private String yhkqtxx;

    /**
    * 银行卡卡号: VARCHAR
    */
    @Column(name = "YHKKH")
    @ESField(text = "银行卡卡号")
    private String yhkkh;

    /**
    * QQ号: VARCHAR
    */
    @Column(name = "QQH")
    @ESField(text = "QQ号")
    private String qqh;

    /**
    * 银行卡所属银行机构编码: VARCHAR
    */
    @Column(name = "YHKSSYHJGBM")
    @ESField(text = "银行卡所属银行机构编码")
    private String yhkssyhjgbm;

    /**
    * 绑定手机号《微信主体》: VARCHAR
    */
    @Column(name = "BDSJH_WXZT")
    @ESField(text = "绑定手机号《微信主体》")
    private String bdsjh_wxzt;

    /**
    * 绑定银行卡《微信主体》: VARCHAR
    */
    @Column(name = "BDYHK_WXZT")
    @ESField(text = "绑定银行卡《微信主体》")
    private String bdyhk_wxzt;

    public void setDlh(String dlh) {
    this.dlh = dlh;
    }

    public String getDlh() {
    return dlh;
    }

    public void setXm_qymc(String xm_qymc) {
    this.xm_qymc = xm_qymc;
    }

    public String getXm_qymc() {
    return xm_qymc;
    }

    public void setYhkrzzt(String yhkrzzt) {
    this.yhkrzzt = yhkrzzt;
    }

    public String getYhkrzzt() {
    return yhkrzzt;
    }

    public void setWxh(String wxh) {
    this.wxh = wxh;
    }

    public String getWxh() {
    return wxh;
    }

    public void setZjlx(String zjlx) {
    this.zjlx = zjlx;
    }

    public String getZjlx() {
    return zjlx;
    }

    public void setBdsjh(String bdsjh) {
    this.bdsjh = bdsjh;
    }

    public String getBdsjh() {
    return bdsjh;
    }

    public void setCxzh(String cxzh) {
    this.cxzh = cxzh;
    }

    public String getCxzh() {
    return cxzh;
    }

    public void setZfzh(String zfzh) {
    this.zfzh = zfzh;
    }

    public String getZfzh() {
    return zfzh;
    }

    public void setZjh(String zjh) {
    this.zjh = zjh;
    }

    public String getZjh() {
    return zjh;
    }

    public void setZhlb(String zhlb) {
    this.zhlb = zhlb;
    }

    public String getZhlb() {
    return zhlb;
    }

    public void setYhklx(String yhklx) {
    this.yhklx = yhklx;
    }

    public String getYhklx() {
    return yhklx;
    }

    public void setZzyxq(String zzyxq) {
    this.zzyxq = zzyxq;
    }

    public String getZzyxq() {
    return zzyxq;
    }

    public void setQczd(Integer qczd) {
    this.qczd = qczd;
    }

    public Integer getQczd() {
    return qczd;
    }

    public void setYhkssyhmc(String yhkssyhmc) {
    this.yhkssyhmc = yhkssyhmc;
    }

    public String getYhkssyhmc() {
    return yhkssyhmc;
    }

    public void setYhkyxq(String yhkyxq) {
    this.yhkyxq = yhkyxq;
    }

    public String getYhkyxq() {
    return yhkyxq;
    }

    public void setSj(Date sj) {
    this.sj = sj;
    }

    public Date getSj() {
    return sj;
    }

    public void setYhkqtxx(String yhkqtxx) {
    this.yhkqtxx = yhkqtxx;
    }

    public String getYhkqtxx() {
    return yhkqtxx;
    }

    public void setYhkkh(String yhkkh) {
    this.yhkkh = yhkkh;
    }

    public String getYhkkh() {
    return yhkkh;
    }

    public void setQqh(String qqh) {
    this.qqh = qqh;
    }

    public String getQqh() {
    return qqh;
    }

    public void setYhkssyhjgbm(String yhkssyhjgbm) {
    this.yhkssyhjgbm = yhkssyhjgbm;
    }

    public String getYhkssyhjgbm() {
    return yhkssyhjgbm;
    }

    public void setBdsjh_wxzt(String bdsjh_wxzt) {
    this.bdsjh_wxzt = bdsjh_wxzt;
    }

    public String getBdsjh_wxzt() {
    return bdsjh_wxzt;
    }

    public void setBdyhk_wxzt(String bdyhk_wxzt) {
    this.bdyhk_wxzt = bdyhk_wxzt;
    }

    public String getBdyhk_wxzt() {
    return bdyhk_wxzt;
    }

}
