package com.unis.model.wxjyyjmx;

import java.util.Date;
import javax.persistence.*;
import com.unis.common.annotation.ESField;
import com.unis.common.annotation.ESTable;
/**
* 我方下家聚合
*
* @author Qx
* @version 1.0
* @since 2020-05-12
*/
@ESTable(name = "vzhwfxjjh",text = "我方下家聚合")
@Table(name = "V_ZH_Wf_Xjjh_TB")

public class VZhWfXjjh  {

private static final long serialVersionUID = 1L;

/****************************************
* Basic fields
****************************************/
    /**
    * 我方支付账户《入账》: VARCHAR
    */
    @Column(name = "WF_ZFZH_RZ")
    @ESField(text = "我方支付账户《入账》")
    private String wf_zfzh_rz;

    /**
    * 我方下家: VARCHAR
    */
    @Column(name = "WFXJ")
    @ESField(text = "我方下家")
    private String wfxj;

    public void setWf_zfzh_rz(String wf_zfzh_rz) {
    this.wf_zfzh_rz = wf_zfzh_rz;
    }

    public String getWf_zfzh_rz() {
    return wf_zfzh_rz;
    }

    public void setWfxj(String wfxj) {
    this.wfxj = wfxj;
    }

    public String getWfxj() {
    return wfxj;
    }

}
