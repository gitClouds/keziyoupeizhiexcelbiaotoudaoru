package com.unis.model.wxjyyjmx;

import java.util.Date;
import javax.persistence.*;
import com.unis.common.annotation.ESField;
import com.unis.common.annotation.ESTable;
/**
* 我方支付账号最后交易时间
*
* @author Qx
* @version 1.0
* @since 2020-05-12
*/
@ESTable(name = "vzhwfzfzhzhjysj",text = "我方支付账号最后交易时间")
@Table(name = "V_ZH_WFzfzhzhjysj_TB")
public class VZhWfzfzhzhjysj  {

private static final long serialVersionUID = 1L;

/****************************************
* Basic fields
****************************************/
    /**
    * 我方支付账号: VARCHAR
    */
    @Column(name = "WFZFZH")
    @ESField(text = "我方支付账号")
    private String wfzfzh;

    /**
    * 交易余额: VARCHAR
    */
    @Column(name = "JYYE")
    @ESField(text = "交易余额")
    private String jyye;

    /**
    * 交易主体的出入账标识: VARCHAR
    */
    @Column(name = "JYZTDCRZBS")
    @ESField(text = "交易主体的出入账标识")
    private String jyztdcrzbs;

    /**
    * 对方支付账号: VARCHAR
    */
    @Column(name = "DFZFZH")
    @ESField(text = "对方支付账号")
    private String dfzfzh;

    /**
    * 交易类型: VARCHAR
    */
    @Column(name = "JYLX")
    @ESField(text = "交易类型")
    private String jylx;

    /**
    * 交易时间: VARCHAR
    */
    @Column(name = "JYSJ")
    @ESField(text = "交易时间")
    private String jysj;

    /**
    * 交易金额: VARCHAR
    */
    @Column(name = "JYJE")
    @ESField(text = "交易金额")
    private String jyje;

    /**
    * 注释: VARCHAR
    */
    @Column(name = "LYAJ")
    @ESField(text = "来源案件")
    private String lyaj;

    /**
    * 我方最后交易情况: VARCHAR
    */
    @Column(name = "WFZHJYQK")
    @ESField(text = "我方最后交易情况")
    private String wfzhjyqk;

    public void setWfzfzh(String wfzfzh) {
    this.wfzfzh = wfzfzh;
    }

    public String getWfzfzh() {
    return wfzfzh;
    }

    public void setJyye(String jyye) {
    this.jyye = jyye;
    }

    public String getJyye() {
    return jyye;
    }

    public void setJyztdcrzbs(String jyztdcrzbs) {
    this.jyztdcrzbs = jyztdcrzbs;
    }

    public String getJyztdcrzbs() {
    return jyztdcrzbs;
    }

    public void setDfzfzh(String dfzfzh) {
    this.dfzfzh = dfzfzh;
    }

    public String getDfzfzh() {
    return dfzfzh;
    }

    public void setJylx(String jylx) {
    this.jylx = jylx;
    }

    public String getJylx() {
    return jylx;
    }

    public void setJysj(String jysj) {
    this.jysj = jysj;
    }

    public String getJysj() {
    return jysj;
    }

    public void setJyje(String jyje) {
    this.jyje = jyje;
    }

    public String getJyje() {
    return jyje;
    }

    public void setLyaj(String lyaj) {
    this.lyaj = lyaj;
    }

    public String getLyaj() {
    return lyaj;
    }

    public void setWfzhjyqk(String wfzhjyqk) {
    this.wfzhjyqk = wfzhjyqk;
    }

    public String getWfzhjyqk() {
    return wfzhjyqk;
    }

}
