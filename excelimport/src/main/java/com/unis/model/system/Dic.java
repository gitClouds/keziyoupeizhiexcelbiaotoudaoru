package com.unis.model.system;

import javax.persistence.*;

@Table(name = "T_SYS_DIC")
public class Dic {
    @Id
    @Column(name = "ZDDM")
    private String zddm;

    @Column(name = "ZDMC")
    private String zdmc;

    @Column(name = "BZ")
    private String bz;

    @Column(name = "FJBS")
    private String fjbs;

    /**
     * @return ZDDM
     */
    public String getZddm() {
        return zddm;
    }

    /**
     * @param zddm
     */
    public void setZddm(String zddm) {
        this.zddm = zddm == null ? null : zddm.trim();
    }

    /**
     * @return ZDMC
     */
    public String getZdmc() {
        return zdmc;
    }

    /**
     * @param zdmc
     */
    public void setZdmc(String zdmc) {
        this.zdmc = zdmc == null ? null : zdmc.trim();
    }

    /**
     * @return BZ
     */
    public String getBz() {
        return bz;
    }

    /**
     * @param bz
     */
    public void setBz(String bz) {
        this.bz = bz == null ? null : bz.trim();
    }

    /**
     * @return FJBS
     */
    public String getFjbs() {
        return fjbs;
    }

    /**
     * @param fjbs
     */
    public void setFjbs(String fjbs) {
        this.fjbs = fjbs == null ? null : fjbs.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", zddm=").append(zddm);
        sb.append(", zdmc=").append(zdmc);
        sb.append(", bz=").append(bz);
        sb.append(", fjbs=").append(fjbs);
        sb.append("]");
        return sb.toString();
    }
}