package com.unis.model.system;

import javax.persistence.*;

@Table(name = "T_SYS_LOG_META")
public class SysLogMeta {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "CZBM")
    private String czbm;

    @Column(name = "CZDETAIL")
    private String czdetail;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return CZBM
     */
    public String getCzbm() {
        return czbm;
    }

    /**
     * @param czbm
     */
    public void setCzbm(String czbm) {
        this.czbm = czbm == null ? null : czbm.trim();
    }

    /**
     * @return CZDETAIL
     */
    public String getCzdetail() {
        return czdetail;
    }

    /**
     * @param czdetail
     */
    public void setCzdetail(String czdetail) {
        this.czdetail = czdetail == null ? null : czdetail.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", czbm=").append(czbm);
        sb.append(", czdetail=").append(czdetail);
        sb.append("]");
        return sb.toString();
    }
}