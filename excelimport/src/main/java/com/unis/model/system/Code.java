package com.unis.model.system;

import javax.persistence.*;

@Table(name = "T_SYS_CODE")
public class Code {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "ZDDM")
    private String zddm;

    @Column(name = "DM")
    private String dm;

    @Column(name = "MC")
    private String mc;

    @Column(name = "YXX")
    private Short yxx;

    @Column(name = "YHM")
    private String yhm;

    @Column(name = "NBDM")
    private String nbdm;

    @Column(name = "BZ")
    private String bz;

    @Column(name = "SJDM")
    private String sjdm;

    @Column(name = "BJDM")
    private String bjdm;

    @Column(name = "PX")
    private Integer px;
    @Column(name = "ISPARENT")
    private String isParent;
    @Column(name = "CODE_lEVEL")
    private Integer codeLevel;
    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return ZDDM
     */
    public String getZddm() {
        return zddm;
    }

    /**
     * @param zddm
     */
    public void setZddm(String zddm) {
        this.zddm = zddm == null ? null : zddm.trim();
    }

    /**
     * @return DM
     */
    public String getDm() {
        return dm;
    }

    /**
     * @param dm
     */
    public void setDm(String dm) {
        this.dm = dm == null ? null : dm.trim();
    }

    /**
     * @return MC
     */
    public String getMc() {
        return mc;
    }

    /**
     * @param mc
     */
    public void setMc(String mc) {
        this.mc = mc == null ? null : mc.trim();
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    /**
     * @return YHM
     */
    public String getYhm() {
        return yhm;
    }

    /**
     * @param yhm
     */
    public void setYhm(String yhm) {
        this.yhm = yhm == null ? null : yhm.trim();
    }

    /**
     * @return NBDM
     */
    public String getNbdm() {
        return nbdm;
    }

    /**
     * @param nbdm
     */
    public void setNbdm(String nbdm) {
        this.nbdm = nbdm == null ? null : nbdm.trim();
    }

    /**
     * @return BZ
     */
    public String getBz() {
        return bz;
    }

    /**
     * @param bz
     */
    public void setBz(String bz) {
        this.bz = bz == null ? null : bz.trim();
    }

    /**
     * @return SJDM
     */
    public String getSjdm() {
        return sjdm;
    }

    /**
     * @param sjdm
     */
    public void setSjdm(String sjdm) {
        this.sjdm = sjdm == null ? null : sjdm.trim();
    }

    /**
     * @return BJDM
     */
    public String getBjdm() {
        return bjdm;
    }

    /**
     * @param bjdm
     */
    public void setBjdm(String bjdm) {
        this.bjdm = bjdm == null ? null : bjdm.trim();
    }

    /**
     * @return PX
     */
    public Integer getPx() {
        return px;
    }

    /**
     * @param px
     */
    public void setPx(int px) {
        this.px = px ;
    }

    public String getIsParent() {
        return isParent;
    }

    public void setIsParent(String isParent) {
        this.isParent = isParent == null ? null : isParent.trim();;
    }

    public Integer getCodeLevel() {
        return codeLevel;
    }

    public void setCodeLevel(Integer codeLevel) {
        this.codeLevel = codeLevel;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", zddm=").append(zddm);
        sb.append(", dm=").append(dm);
        sb.append(", mc=").append(mc);
        sb.append(", yxx=").append(yxx);
        sb.append(", yhm=").append(yhm);
        sb.append(", nbdm=").append(nbdm);
        sb.append(", bz=").append(bz);
        sb.append(", sjdm=").append(sjdm);
        sb.append(", bjdm=").append(bjdm);
        sb.append(", px=").append(px);
        sb.append(", isParent=").append(isParent);
        sb.append(", codeLevel=").append(codeLevel);
        sb.append("]");
        return sb.toString();
    }
}