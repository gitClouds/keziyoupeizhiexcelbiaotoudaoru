package com.unis.model.system;

import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Table(name = "T_SYS_JGZ")
public class Jgz {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "USERPK")
    private String userpk;

    @Column(name = "USERNAME")
    private String username;

    @Column(name = "DWDM")
    private String dwdm;

    @Column(name = "DWMC")
    private String dwmc;

    @Column(name = "JBRZJZL")
    private String jbrzjzl;

    @NotNull(message = "经办人警号不能为空")
    @Pattern(regexp = "^\\d{6,7}$",message = "经办人警号必须为6/7位数字")
    @Column(name = "JBRZJHM")
    private String jbrzjhm;

    @NotNull(message="经办人姓名不能为空")
    @Size(min = 2,max = 10,message = "经办人姓名应该在2-10字符之间")
    @Pattern(regexp = "^[\\u4e00-\\u9fa5]+$",message = "经办人姓名只能输入中文")
    @Column(name = "JBRXM")
    private String jbrxm;

    @NotNull(message="经办人电话不能为空")
    @Pattern(regexp = "^\\d{10,12}$",message = "经办人电话只能输入10-12位数字")
    @Column(name = "JBRDH")
    private String jbrdh;

    @NotNull(message = "经办人警官证不能为空")
    @Column(name = "JBRMONGO")
    private String jbrmongo;

    @Column(name = "XCRZJZL")
    private String xcrzjzl;

    @NotNull(message = "协查人警号不能为空")
    @Pattern(regexp = "^\\d{6,7}$",message = "协查人警号必须为6/7位数字")
    @Column(name = "XCRZJHM")
    private String xcrzjhm;

    @NotNull(message="协查人姓名不能为空")
    @Size(min = 2,max = 10,message = "协查人姓名应该在2-10字符之间")
    @Pattern(regexp = "^[\\u4e00-\\u9fa5]+$",message = "协查人姓名只能输入中文")
    @Column(name = "XCRXM")
    private String xcrxm;

    @NotNull(message="协查人电话不能为空")
    @Pattern(regexp = "^\\d{10,12}$",message = "协查人电话只能输入10-12位数字")
    @Column(name = "XCRDH")
    private String xcrdh;

    @NotNull(message = "协查人警官证不能为空")
    @Column(name = "XCRMONGO")
    private String xcrmongo;

    @Column(name = "YXX")
    private String yxx;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "LRR")
    private String lrr;

    @Column(name = "LRRJH")
    private String lrrjh;

    @Column(name = "LRDWDM")
    private String lrdwdm;

    @Column(name = "LRDWMC")
    private String lrdwmc;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return USERPK
     */
    public String getUserpk() {
        return userpk;
    }

    /**
     * @param userpk
     */
    public void setUserpk(String userpk) {
        this.userpk = userpk == null ? null : userpk.trim();
    }

    /**
     * @return USERNAME
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username
     */
    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    /**
     * @return DWDM
     */
    public String getDwdm() {
        return dwdm;
    }

    /**
     * @param dwdm
     */
    public void setDwdm(String dwdm) {
        this.dwdm = dwdm == null ? null : dwdm.trim();
    }

    /**
     * @return DWMC
     */
    public String getDwmc() {
        return dwmc;
    }

    /**
     * @param dwmc
     */
    public void setDwmc(String dwmc) {
        this.dwmc = dwmc == null ? null : dwmc.trim();
    }

    /**
     * @return JBRZJZL
     */
    public String getJbrzjzl() {
        return jbrzjzl;
    }

    /**
     * @param jbrzjzl
     */
    public void setJbrzjzl(String jbrzjzl) {
        this.jbrzjzl = jbrzjzl == null ? null : jbrzjzl.trim();
    }

    /**
     * @return JBRZJHM
     */
    public String getJbrzjhm() {
        return jbrzjhm;
    }

    /**
     * @param jbrzjhm
     */
    public void setJbrzjhm(String jbrzjhm) {
        this.jbrzjhm = jbrzjhm == null ? null : jbrzjhm.trim();
    }

    /**
     * @return JBRXM
     */
    public String getJbrxm() {
        return jbrxm;
    }

    /**
     * @param jbrxm
     */
    public void setJbrxm(String jbrxm) {
        this.jbrxm = jbrxm == null ? null : jbrxm.trim();
    }

    /**
     * @return JBRDH
     */
    public String getJbrdh() {
        return jbrdh;
    }

    /**
     * @param jbrdh
     */
    public void setJbrdh(String jbrdh) {
        this.jbrdh = jbrdh == null ? null : jbrdh.trim();
    }

    /**
     * @return JBRMONGO
     */
    public String getJbrmongo() {
        return jbrmongo;
    }

    /**
     * @param jbrmongo
     */
    public void setJbrmongo(String jbrmongo) {
        this.jbrmongo = jbrmongo == null ? null : jbrmongo.trim();
    }

    /**
     * @return XCRZJZL
     */
    public String getXcrzjzl() {
        return xcrzjzl;
    }

    /**
     * @param xcrzjzl
     */
    public void setXcrzjzl(String xcrzjzl) {
        this.xcrzjzl = xcrzjzl == null ? null : xcrzjzl.trim();
    }

    /**
     * @return XCRZJHM
     */
    public String getXcrzjhm() {
        return xcrzjhm;
    }

    /**
     * @param xcrzjhm
     */
    public void setXcrzjhm(String xcrzjhm) {
        this.xcrzjhm = xcrzjhm == null ? null : xcrzjhm.trim();
    }

    /**
     * @return XCRXM
     */
    public String getXcrxm() {
        return xcrxm;
    }

    /**
     * @param xcrxm
     */
    public void setXcrxm(String xcrxm) {
        this.xcrxm = xcrxm == null ? null : xcrxm.trim();
    }

    /**
     * @return XCRDH
     */
    public String getXcrdh() {
        return xcrdh;
    }

    /**
     * @param xcrdh
     */
    public void setXcrdh(String xcrdh) {
        this.xcrdh = xcrdh == null ? null : xcrdh.trim();
    }

    /**
     * @return XCRMONGO
     */
    public String getXcrmongo() {
        return xcrmongo;
    }

    /**
     * @param xcrmongo
     */
    public void setXcrmongo(String xcrmongo) {
        this.xcrmongo = xcrmongo == null ? null : xcrmongo.trim();
    }

    /**
     * @return YXX
     */
    public String getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(String yxx) {
        this.yxx = yxx == null ? null : yxx.trim();
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return LRR
     */
    public String getLrr() {
        return lrr;
    }

    /**
     * @param lrr
     */
    public void setLrr(String lrr) {
        this.lrr = lrr == null ? null : lrr.trim();
    }

    /**
     * @return LRRJH
     */
    public String getLrrjh() {
        return lrrjh;
    }

    /**
     * @param lrrjh
     */
    public void setLrrjh(String lrrjh) {
        this.lrrjh = lrrjh == null ? null : lrrjh.trim();
    }

    /**
     * @return LRDWDM
     */
    public String getLrdwdm() {
        return lrdwdm;
    }

    /**
     * @param lrdwdm
     */
    public void setLrdwdm(String lrdwdm) {
        this.lrdwdm = lrdwdm == null ? null : lrdwdm.trim();
    }

    /**
     * @return LRDWMC
     */
    public String getLrdwmc() {
        return lrdwmc;
    }

    /**
     * @param lrdwmc
     */
    public void setLrdwmc(String lrdwmc) {
        this.lrdwmc = lrdwmc == null ? null : lrdwmc.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", userpk=").append(userpk);
        sb.append(", username=").append(username);
        sb.append(", dwdm=").append(dwdm);
        sb.append(", dwmc=").append(dwmc);
        sb.append(", jbrzjzl=").append(jbrzjzl);
        sb.append(", jbrzjhm=").append(jbrzjhm);
        sb.append(", jbrxm=").append(jbrxm);
        sb.append(", jbrdh=").append(jbrdh);
        sb.append(", jbrmongo=").append(jbrmongo);
        sb.append(", xcrzjzl=").append(xcrzjzl);
        sb.append(", xcrzjhm=").append(xcrzjhm);
        sb.append(", xcrxm=").append(xcrxm);
        sb.append(", xcrdh=").append(xcrdh);
        sb.append(", xcrmongo=").append(xcrmongo);
        sb.append(", yxx=").append(yxx);
        sb.append(", rksj=").append(rksj);
        sb.append(", lrr=").append(lrr);
        sb.append(", lrrjh=").append(lrrjh);
        sb.append(", lrdwdm=").append(lrdwdm);
        sb.append(", lrdwmc=").append(lrdwmc);
        sb.append("]");
        return sb.toString();
    }
}