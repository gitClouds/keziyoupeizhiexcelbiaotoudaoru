package com.unis.model.system;

import java.util.Date;
import javax.persistence.*;

@Table(name = "T_SYS_LOG")
public class SysLog {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "CZBM")
    private String czbm;

    @Column(name = "CZDETAIL")
    private String czdetail;

    @Column(name = "REQMETHOD")
    private String reqmethod;

    @Column(name = "FLAG")
    private Short flag;

    @Column(name = "CZDATA")
    private String czdata;

    @Column(name = "ERRDETAIL")
    private String errdetail;

    @Column(name = "JGDM")
    private String jgdm;

    @Column(name = "JGMC")
    private String jgmc;

    @Column(name = "USERPK")
    private String userpk;

    @Column(name = "USERNAME")
    private String username;

    @Column(name = "IDNO")
    private String idno;

    @Column(name = "JH")
    private String jh;

    @Column(name = "CZIP")
    private String czip;

    @Column(name = "RKSJ")
    private Date rksj;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return CZBM
     */
    public String getCzbm() {
        return czbm;
    }

    /**
     * @param czbm
     */
    public void setCzbm(String czbm) {
        this.czbm = czbm == null ? null : czbm.trim();
    }

    /**
     * @return FLAG
     */
    public Short getFlag() {
        return flag;
    }

    /**
     * @param flag
     */
    public void setFlag(Short flag) {
        this.flag = flag;
    }

    /**
     * @return CZDATA
     */
    public String getCzdata() {
        return czdata;
    }

    /**
     * @param czdata
     */
    public void setCzdata(String czdata) {
        this.czdata = czdata == null ? null : czdata.trim();
    }

    /**
     * @return JGDM
     */
    public String getJgdm() {
        return jgdm;
    }

    /**
     * @param jgdm
     */
    public void setJgdm(String jgdm) {
        this.jgdm = jgdm == null ? null : jgdm.trim();
    }

    /**
     * @return JGMC
     */
    public String getJgmc() {
        return jgmc;
    }

    /**
     * @param jgmc
     */
    public void setJgmc(String jgmc) {
        this.jgmc = jgmc == null ? null : jgmc.trim();
    }

    /**
     * @return USERPK
     */
    public String getUserpk() {
        return userpk;
    }

    /**
     * @param userpk
     */
    public void setUserpk(String userpk) {
        this.userpk = userpk == null ? null : userpk.trim();
    }

    /**
     * @return USERNAME
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username
     */
    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    /**
     * @return IDNO
     */
    public String getIdno() {
        return idno;
    }

    /**
     * @param idno
     */
    public void setIdno(String idno) {
        this.idno = idno == null ? null : idno.trim();
    }

    /**
     * @return JH
     */
    public String getJh() {
        return jh;
    }

    /**
     * @param jh
     */
    public void setJh(String jh) {
        this.jh = jh == null ? null : jh.trim();
    }

    /**
     * @return CZIP
     */
    public String getCzip() {
        return czip;
    }

    /**
     * @param czip
     */
    public void setCzip(String czip) {
        this.czip = czip == null ? null : czip.trim();
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    public String getCzdetail() {
        return czdetail;
    }

    public void setCzdetail(String czdetail) {
        this.czdetail = czdetail;
    }

    public String getReqmethod() {
        return reqmethod;
    }

    public void setReqmethod(String reqmethod) {
        this.reqmethod = reqmethod;
    }

    public String getErrdetail() {
        return errdetail;
    }

    public void setErrdetail(String errdetail) {
        this.errdetail = errdetail;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", czbm=").append(czbm);
        sb.append(", czdetail=").append(czdetail);
        sb.append(", reqmethod=").append(reqmethod);
        sb.append(", flag=").append(flag);
        sb.append(", czdata=").append(czdata);
        sb.append(", errdetail=").append(errdetail);
        sb.append(", jgdm=").append(jgdm);
        sb.append(", jgmc=").append(jgmc);
        sb.append(", userpk=").append(userpk);
        sb.append(", username=").append(username);
        sb.append(", idno=").append(idno);
        sb.append(", jh=").append(jh);
        sb.append(", czip=").append(czip);
        sb.append(", rksj=").append(rksj);
        sb.append("]");
        return sb.toString();
    }
}