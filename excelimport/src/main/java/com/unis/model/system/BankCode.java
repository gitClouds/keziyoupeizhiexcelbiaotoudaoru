package com.unis.model.system;

import javax.persistence.*;

@Table(name = "T_SYS_BANK_CODE")
public class BankCode {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "STARTACCOUNT")
    private String startaccount;

    @Column(name = "ACCOUNTLENGTH")
    private Short accountlength;

    @Column(name = "BANKCODE")
    private String bankcode;

    @Column(name = "BANKNAME")
    private String bankname;

    @Column(name = "ISOK")
    private Short isok;

    @Column(name = "YXX")
    private Short yxx;

    @Column(name = "SUBJECTTYPE")
    private Short subjecttype;
    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return STARTACCOUNT
     */
    public String getStartaccount() {
        return startaccount;
    }

    /**
     * @param startaccount
     */
    public void setStartaccount(String startaccount) {
        this.startaccount = startaccount == null ? null : startaccount.trim();
    }

    /**
     * @return ACCOUNTLENGTH
     */
    public Short getAccountlength() {
        return accountlength;
    }

    /**
     * @param accountlength
     */
    public void setAccountlength(Short accountlength) {
        this.accountlength = accountlength;
    }

    /**
     * @return BANKCODE
     */
    public String getBankcode() {
        return bankcode;
    }

    /**
     * @param bankcode
     */
    public void setBankcode(String bankcode) {
        this.bankcode = bankcode == null ? null : bankcode.trim();
    }

    /**
     * @return BANKNAME
     */
    public String getBankname() {
        return bankname;
    }

    /**
     * @param bankname
     */
    public void setBankname(String bankname) {
        this.bankname = bankname == null ? null : bankname.trim();
    }

    /**
     * @return ISOK
     */
    public Short getIsok() {
        return isok;
    }

    /**
     * @param isok
     */
    public void setIsok(Short isok) {
        this.isok = isok;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    public Short getSubjecttype() {
        return subjecttype;
    }

    public void setSubjecttype(Short subjecttype) {
        this.subjecttype = subjecttype;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", startaccount=").append(startaccount);
        sb.append(", accountlength=").append(accountlength);
        sb.append(", bankcode=").append(bankcode);
        sb.append(", bankname=").append(bankname);
        sb.append(", isok=").append(isok);
        sb.append(", yxx=").append(yxx);
        sb.append(", subjecttype=").append(subjecttype);
        sb.append("]");
        return sb.toString();
    }
}