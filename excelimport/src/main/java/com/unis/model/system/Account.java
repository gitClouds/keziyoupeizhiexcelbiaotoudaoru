package com.unis.model.system;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Table(name = "T_SYS_ACCOUNT")
public class Account implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "PK")
    private String pk;

    @NotNull(message = "账号不能为空")
    @Size(min = 6,max = 16,message = "账号长度为6-16位")
    @Pattern(regexp = "^[0-9A-Za-z]*$",message = "账号只能输入数字和英文字符")
    @Column(name = "USERNAME")
    private String username;

    @NotNull(message = "身份证号不能为空")
    @Pattern(regexp = "^\\d{6}(18|19|20)?\\d{2}(0[1-9]|1[012])(0[1-9]|[12]\\d|3[01])\\d{3}(\\d|[xX])$",message = "请输入有效身份证")
    @Column(name = "IDNO")
    private String idno;

    @Size(min = 6,max = 18,message = "密码长度为6-18位")
    @Pattern(regexp = "^[0-9A-Za-z]*$",message = "密码只能输入数字和英文字符")
    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "SALT")
    private String salt;

    @NotNull(message="姓名不能为空")
    @Size(min = 2,max = 10,message = "jbrxm应该在2-10字符之间")
    @Pattern(regexp = "^[\\u4e00-\\u9fa5]+$",message = "姓名只能输入中文")
    @Column(name = "XM")
    private String xm;

    @NotNull(message="警号不能为空")
    @Pattern(regexp = "^\\d{6,7}$",message = "警号必须为6/7位数字")
    @Column(name = "JH")
    private String jh;

    @NotNull(message="联系方式不能为空")
    @Pattern(regexp = "^\\d{10,12}$",message = "联系方式只能输入10-12位数字")
    @Column(name = "LXFS")
    private String lxfs;

    @NotNull(message = "请选择所属机构")
    @Pattern(regexp = "^\\d{12}$",message = "机构代码不正确，机构代码为12位数字")
    @Column(name = "JGDM")
    private String jgdm;

    @Column(name = "JGMC")
    private String jgmc;

    @Column(name = "JZ")
    private String jz;

    @Column(name = "ZWJB")
    private String zwjb;

    @Column(name = "YHZSDM")
    private String yhzsdm;

    @Column(name = "ZT")
    private String zt = "1";

    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @NotNull(message = "请选择过期时间")
    @Column(name = "OUTTIME")
    private Date outtime;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    public Date getOuttime() {
        return outtime;
    }

    public void setOuttime(Date outtime) {
        this.outtime = outtime;
    }

    /**
     * @return USERNAME
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username
     */
    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    /**
     * @return IDNO
     */
    public String getIdno() {
        return idno;
    }

    /**
     * @param idno
     */
    public void setIdno(String idno) {
        this.idno = idno == null ? null : idno.trim();
    }

    /**
     * @return PASSWORD
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password
     */
    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    /**
     * @return SALT
     */
    public String getSalt() {
        return salt;
    }

    /**
     * @param salt
     */
    public void setSalt(String salt) {
        this.salt = salt == null ? null : salt.trim();
    }

    /**
     * @return XM
     */
    public String getXm() {
        return xm;
    }

    /**
     * @param xm
     */
    public void setXm(String xm) {
        this.xm = xm == null ? null : xm.trim();
    }

    /**
     * @return JH
     */
    public String getJh() {
        return jh;
    }

    /**
     * @param jh
     */
    public void setJh(String jh) {
        this.jh = jh == null ? null : jh.trim();
    }

    /**
     * @return LXFS
     */
    public String getLxfs() {
        return lxfs;
    }

    /**
     * @param lxfs
     */
    public void setLxfs(String lxfs) {
        this.lxfs = lxfs == null ? null : lxfs.trim();
    }

    /**
     * @return JGDM
     */
    public String getJgdm() {
        return jgdm;
    }

    /**
     * @param jgdm
     */
    public void setJgdm(String jgdm) {
        this.jgdm = jgdm == null ? null : jgdm.trim();
    }

    /**
     * @return JGMC
     */
    public String getJgmc() {
        return jgmc;
    }

    /**
     * @param jgmc
     */
    public void setJgmc(String jgmc) {
        this.jgmc = jgmc == null ? null : jgmc.trim();
    }

    /**
     * @return JZ
     */
    public String getJz() {
        return jz;
    }

    /**
     * @param jz
     */
    public void setJz(String jz) {
        this.jz = jz == null ? null : jz.trim();
    }

    /**
     * @return ZWJB
     */
    public String getZwjb() {
        return zwjb;
    }

    /**
     * @param zwjb
     */
    public void setZwjb(String zwjb) {
        this.zwjb = zwjb == null ? null : zwjb.trim();
    }

    /**
     * @return YHZSDM
     */
    public String getYhzsdm() {
        return yhzsdm;
    }

    /**
     * @param yhzsdm
     */
    public void setYhzsdm(String yhzsdm) {
        this.yhzsdm = yhzsdm == null ? null : yhzsdm.trim();
    }

    /**
     * @return ZT
     */
    public String getZt() {
        return zt;
    }

    /**
     * @param zt
     */
    public void setZt(String zt) {
        this.zt = zt;
    }



}