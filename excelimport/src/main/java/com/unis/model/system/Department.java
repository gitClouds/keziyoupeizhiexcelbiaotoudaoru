package com.unis.model.system;

import javax.persistence.*;

@Table(name = "T_SYS_DEPARTMENT")
public class Department {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "DEPTCODE")
    private String deptcode;

    @Column(name = "NAME")
    private String name;

    @Column(name = "COMMENTS")
    private String comments;

    @Column(name = "PARENT")
    private String parent;

    @Column(name = "ISPARENT")
    private String isParent;

    @Column(name = "DISPLAYORDER")
    private Integer displayorder;

    @Column(name = "YWSJDM")
    private String ywsjdm;
    @Column(name = "DEPT_LEVEL")
    private Integer deptLevel;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return DEPTCODE
     */
    public String getDeptcode() {
        return deptcode;
    }

    /**
     * @param deptcode
     */
    public void setDeptcode(String deptcode) {
        this.deptcode = deptcode == null ? null : deptcode.trim();
    }

    /**
     * @return NAME
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * @return COMMENTS
     */
    public String getComments() {
        return comments;
    }

    /**
     * @param comments
     */
    public void setComments(String comments) {
        this.comments = comments == null ? null : comments.trim();
    }

    /**
     * @return PARENT
     */
    public String getParent() {
        return parent;
    }

    /**
     * @param parent
     */
    public void setParent(String parent) {
        this.parent = parent == null ? null : parent.trim();
    }

    /**
     * @return ISPARENT
     */
    public String getIsParent() {
        return isParent;
    }

    /**
     * @param isParent
     */
    public void setIsParent(String isParent) {
        this.isParent = isParent== null ? null : isParent.trim();
    }

    /**
     * @return DISPLAYORDER
     */
    public Integer getDisplayorder() {
        return displayorder;
    }

    /**
     * @param displayorder
     */
    public void setDisplayorder(Integer displayorder) {
        this.displayorder = displayorder;
    }

    /**
     * @return YWSJDM
     */
    public String getYwsjdm() {
        return ywsjdm;
    }

    /**
     * @param ywsjdm
     */
    public void setYwsjdm(String ywsjdm) {
        this.ywsjdm = ywsjdm == null ? null : ywsjdm.trim();
    }

    public Integer getDeptLevel() {
        return deptLevel;
    }

    public void setDeptLevel(Integer deptLevel) {
        this.deptLevel = deptLevel;
    }

    @Override
    public String toString() {
        return "Department{" +
                "pk='" + pk + '\'' +
                ", deptcode='" + deptcode + '\'' +
                ", name='" + name + '\'' +
                ", comments='" + comments + '\'' +
                ", parent='" + parent + '\'' +
                ", isParent=" + isParent +
                ", displayorder=" + displayorder +
                ", ywsjdm='" + ywsjdm + '\'' +
                ", deptLevel=" + deptLevel +
                '}';
    }

}