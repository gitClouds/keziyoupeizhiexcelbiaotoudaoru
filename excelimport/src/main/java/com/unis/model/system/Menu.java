package com.unis.model.system;

import javax.persistence.*;

@Table(name = "T_SYS_MENU")
public class Menu {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "CODE")
    private String code;

    @Column(name = "NAME")
    private String name;

    @Column(name = "ICON")
    private String icon;

    @Column(name = "PAGE_ID")
    private String pageId;

    @Column(name = "ISPARENT")
    private Short isparent;

    @Column(name = "URL")
    private String url;

    @Column(name = "PARENT_CODE")
    private String parentCode;

    @Column(name = "CODE_LEVEL")
    private Short codeLevel;

    @Column(name = "C_TYPE")
    private String cType;

    @Column(name = "PX")
    private Integer px;

    @Column(name = "COMMENTS")
    private String comments;

    @Column(name = "YXX")
    private Short yxx;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return CODE
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     */
    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    /**
     * @return NAME
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    /**
     * @return PAGE_ID
     */
    public String getPageId() {
        return pageId;
    }

    /**
     * @param pageId
     */
    public void setPageId(String pageId) {
        this.pageId = pageId == null ? null : pageId.trim();
    }

    /**
     * @return ISPARENT
     */
    public Short getIsparent() {
        return isparent;
    }

    /**
     * @param isparent
     */
    public void setIsparent(Short isparent) {
        this.isparent = isparent;
    }

    /**
     * @return URL
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url
     */
    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    /**
     * @return PARENT_CODE
     */
    public String getParentCode() {
        return parentCode;
    }

    /**
     * @param parentCode
     */
    public void setParentCode(String parentCode) {
        this.parentCode = parentCode == null ? null : parentCode.trim();
    }

    /**
     * @return CODE_LEVEL
     */
    public Short getCodeLevel() {
        return codeLevel;
    }

    /**
     * @param codeLevel
     */
    public void setCodeLevel(Short codeLevel) {
        this.codeLevel = codeLevel;
    }

    /**
     * @return C_TYPE
     */
    public String getcType() {
        return cType;
    }

    /**
     * @param cType
     */
    public void setcType(String cType) {
        this.cType = cType == null ? null : cType.trim();
    }

    /**
     * @return PX
     */
    public Integer getPx() {
        return px;
    }

    /**
     * @param px
     */
    public void setPx(Integer px) {
        this.px = px;
    }

    /**
     * @return COMMENTS
     */
    public String getComments() {
        return comments;
    }

    /**
     * @param comments
     */
    public void setComments(String comments) {
        this.comments = comments == null ? null : comments.trim();
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", code=").append(code);
        sb.append(", name=").append(name);
        sb.append(", icon=").append(icon);
        sb.append(", pageId=").append(pageId);
        sb.append(", isparent=").append(isparent);
        sb.append(", url=").append(url);
        sb.append(", parentCode=").append(parentCode);
        sb.append(", codeLevel=").append(codeLevel);
        sb.append(", cType=").append(cType);
        sb.append(", px=").append(px);
        sb.append(", comments=").append(comments);
        sb.append(", yxx=").append(yxx);
        sb.append("]");
        return sb.toString();
    }
}