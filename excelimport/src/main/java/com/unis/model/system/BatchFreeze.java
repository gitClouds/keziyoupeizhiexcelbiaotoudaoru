package com.unis.model.system;

import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Table(name = "T_SYS_BATCH_FREEZE")
public class BatchFreeze {
    @Id
    @Column(name = "ID")
    private String id;

    @NotNull(message = "请填写0单位1个人")
    @Column(name = "TYPE")
    private Short type;

    @NotNull(message = "请填写0银行1三方")
    @Column(name = "YHSF")
    private Short yhsf;

    @Column(name = "YHXM")
    private String yhxm;

    @NotNull(message = "请填写单位")
    @Column(name = "DWDM")
    private String dwdm;

    @Column(name = "DWMC")
    private String dwmc;

    @Column(name = "SFZH")
    private String sfzh;

    @Column(name = "YXX")
    private Short yxx;

    @Column(name = "MAX")
    private String max;

    @Column(name = "MIN")
    private String min;

    @Column(name = "RKSJ")
    private Date rksj;

    /**
     * @return ID
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * @return TYPE
     */
    public Short getType() {
        return type;
    }

    public String getMax() {
        return max;
    }

    public void setMax(String max) {
        this.max = max;
    }

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }

    /**
     * @param type
     */
    public void setType(Short type) {
        this.type = type;
    }

    /**
     * @return YHSF
     */
    public Short getYhsf() {
        return yhsf;
    }

    /**
     * @param yhsf
     */
    public void setYhsf(Short yhsf) {
        this.yhsf = yhsf;
    }

    /**
     * @return YHXM
     */
    public String getYhxm() {
        return yhxm;
    }

    /**
     * @param yhxm
     */
    public void setYhxm(String yhxm) {
        this.yhxm = yhxm == null ? null : yhxm.trim();
    }

    /**
     * @return DWDM
     */
    public String getDwdm() {
        return dwdm;
    }

    /**
     * @param dwdm
     */
    public void setDwdm(String dwdm) {
        this.dwdm = dwdm == null ? null : dwdm.trim();
    }

    /**
     * @return DWMC
     */
    public String getDwmc() {
        return dwmc;
    }

    /**
     * @param dwmc
     */
    public void setDwmc(String dwmc) {
        this.dwmc = dwmc == null ? null : dwmc.trim();
    }

    /**
     * @return SFZH
     */
    public String getSfzh() {
        return sfzh;
    }

    /**
     * @param sfzh
     */
    public void setSfzh(String sfzh) {
        this.sfzh = sfzh == null ? null : sfzh.trim();
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }
}