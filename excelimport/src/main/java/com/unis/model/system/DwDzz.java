package com.unis.model.system;

import java.util.Date;
import javax.persistence.*;

@Table(name = "T_SYS_DW_DZZ")
public class DwDzz {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "DWDM")
    private String dwdm;

    @Column(name = "DWMC")
    private String dwmc;

    @Column(name = "DWJC")
    private String dwjc;

    @Column(name = "DZYEAR")
    private String dzyear;

    @Column(name = "DZZ")
    private Long dzz;

    @Column(name = "CXZ")
    private String cxz;

    @Column(name = "ZFZ")
    private String zfz;

    @Column(name = "LRR")
    private String lrr;

    @Column(name = "LRRJH")
    private String lrrjh;

    @Column(name = "LRDW")
    private String lrdw;

    @Column(name = "LRDWMC")
    private String lrdwmc;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "XGR")
    private String xgr;

    @Column(name = "XGRJH")
    private String xgrjh;

    @Column(name = "XGDW")
    private String xgdw;

    @Column(name = "XGDWMC")
    private String xgdwmc;

    @Column(name = "XGSJ")
    private Date xgsj;

    @Column(name = "YXX")
    private Short yxx;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return DWDM
     */
    public String getDwdm() {
        return dwdm;
    }

    /**
     * @param dwdm
     */
    public void setDwdm(String dwdm) {
        this.dwdm = dwdm == null ? null : dwdm.trim();
    }

    /**
     * @return DWMC
     */
    public String getDwmc() {
        return dwmc;
    }

    /**
     * @param dwmc
     */
    public void setDwmc(String dwmc) {
        this.dwmc = dwmc == null ? null : dwmc.trim();
    }

    /**
     * @return DWJC
     */
    public String getDwjc() {
        return dwjc;
    }

    /**
     * @param dwjc
     */
    public void setDwjc(String dwjc) {
        this.dwjc = dwjc == null ? null : dwjc.trim();
    }

    /**
     * @return DZYEAR
     */
    public String getDzyear() {
        return dzyear;
    }

    /**
     * @param dzyear
     */
    public void setDzyear(String dzyear) {
        this.dzyear = dzyear == null ? null : dzyear.trim();
    }

    /**
     * @return DZZ
     */
    public Long getDzz() {
        return dzz;
    }

    /**
     * @param dzz
     */
    public void setDzz(Long dzz) {
        this.dzz = dzz;
    }

    /**
     * @return CXZ
     */
    public String getCxz() {
        return cxz;
    }

    /**
     * @param cxz
     */
    public void setCxz(String cxz) {
        this.cxz = cxz == null ? null : cxz.trim();
    }

    /**
     * @return ZFZ
     */
    public String getZfz() {
        return zfz;
    }

    /**
     * @param zfz
     */
    public void setZfz(String zfz) {
        this.zfz = zfz == null ? null : zfz.trim();
    }

    /**
     * @return LRR
     */
    public String getLrr() {
        return lrr;
    }

    /**
     * @param lrr
     */
    public void setLrr(String lrr) {
        this.lrr = lrr == null ? null : lrr.trim();
    }

    /**
     * @return LRRJH
     */
    public String getLrrjh() {
        return lrrjh;
    }

    /**
     * @param lrrjh
     */
    public void setLrrjh(String lrrjh) {
        this.lrrjh = lrrjh == null ? null : lrrjh.trim();
    }

    /**
     * @return LRDW
     */
    public String getLrdw() {
        return lrdw;
    }

    /**
     * @param lrdw
     */
    public void setLrdw(String lrdw) {
        this.lrdw = lrdw == null ? null : lrdw.trim();
    }

    /**
     * @return LRDWMC
     */
    public String getLrdwmc() {
        return lrdwmc;
    }

    /**
     * @param lrdwmc
     */
    public void setLrdwmc(String lrdwmc) {
        this.lrdwmc = lrdwmc == null ? null : lrdwmc.trim();
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return XGR
     */
    public String getXgr() {
        return xgr;
    }

    /**
     * @param xgr
     */
    public void setXgr(String xgr) {
        this.xgr = xgr == null ? null : xgr.trim();
    }

    /**
     * @return XGRJH
     */
    public String getXgrjh() {
        return xgrjh;
    }

    /**
     * @param xgrjh
     */
    public void setXgrjh(String xgrjh) {
        this.xgrjh = xgrjh == null ? null : xgrjh.trim();
    }

    /**
     * @return XGDW
     */
    public String getXgdw() {
        return xgdw;
    }

    /**
     * @param xgdw
     */
    public void setXgdw(String xgdw) {
        this.xgdw = xgdw == null ? null : xgdw.trim();
    }

    /**
     * @return XGDWMC
     */
    public String getXgdwmc() {
        return xgdwmc;
    }

    /**
     * @param xgdwmc
     */
    public void setXgdwmc(String xgdwmc) {
        this.xgdwmc = xgdwmc == null ? null : xgdwmc.trim();
    }

    /**
     * @return XGSJ
     */
    public Date getXgsj() {
        return xgsj;
    }

    /**
     * @param xgsj
     */
    public void setXgsj(Date xgsj) {
        this.xgsj = xgsj;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", dwdm=").append(dwdm);
        sb.append(", dwmc=").append(dwmc);
        sb.append(", dwjc=").append(dwjc);
        sb.append(", dzyear=").append(dzyear);
        sb.append(", dzz=").append(dzz);
        sb.append(", cxz=").append(cxz);
        sb.append(", zfz=").append(zfz);
        sb.append(", lrr=").append(lrr);
        sb.append(", lrrjh=").append(lrrjh);
        sb.append(", lrdw=").append(lrdw);
        sb.append(", lrdwmc=").append(lrdwmc);
        sb.append(", rksj=").append(rksj);
        sb.append(", xgr=").append(xgr);
        sb.append(", xgrjh=").append(xgrjh);
        sb.append(", xgdw=").append(xgdw);
        sb.append(", xgdwmc=").append(xgdwmc);
        sb.append(", xgsj=").append(xgsj);
        sb.append(", yxx=").append(yxx);
        sb.append("]");
        return sb.toString();
    }
}