package com.unis.model.system;

import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Table(name = "TB_ZNZF_META")
public class Meta {
    @NotNull(message = "主键不能为空")
    @Id
    @Column(name = "PK")
    private String pk;
    @NotNull(message = "层级不能为空")
    @Column(name = "NLEVEL")
    private Short nlevel;
    @NotNull(message = "最小金额不能为空")
    @Column(name = "MINJE")
    private Long minje;
    @NotNull(message = "下级账号数不能为空")
    @Column(name = "TS")
    private Integer ts;
    @NotNull(message = "流水时间不能为空")
    @Column(name = "MAXHOUR")
    private Short maxhour;

    @Column(name = "JDGX")
    private String jdgx;

    @Column(name = "XGSJ")
    private Date xgsj;
    
    @Column(name = "JYSJB")
    private Short jysjb;
    
    @Column(name = "JYSJE")
    private Short jysje;

    @Column(name = "PHONE")
    private String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return NLEVEL
     */
    public Short getNlevel() {
        return nlevel;
    }

    /**
     * @param nlevel
     */
    public void setNlevel(Short nlevel) {
        this.nlevel = nlevel;
    }

    /**
     * @return MINJE
     */
    public Long getMinje() {
        return minje;
    }

    /**
     * @param minje
     */
    public void setMinje(Long minje) {
        this.minje = minje;
    }

    /**
     * @return TS
     */
    public Integer getTs() {
        return ts;
    }

    /**
     * @param ts
     */
    public void setTs(Integer ts) {
        this.ts = ts;
    }

    /**
     * @return MAXHOUR
     */
    public Short getMaxhour() {
        return maxhour;
    }

    /**
     * @param maxhour
     */
    public void setMaxhour(Short maxhour) {
        this.maxhour = maxhour;
    }

    /**
     * @return JDGX
     */
    public String getJdgx() {
        return jdgx;
    }

    /**
     * @param jdgx
     */
    public void setJdgx(String jdgx) {
        this.jdgx = jdgx == null ? null : jdgx.trim();
    }

    /**
     * @return XGSJ
     */
    public Date getXgsj() {
        return xgsj;
    }

    /**
     * @param xgsj
     */
    public void setXgsj(Date xgsj) {
        this.xgsj = xgsj;
    }

    public Short getJysjb() {
		return jysjb;
	}

	public void setJysjb(Short jysjb) {
		this.jysjb = jysjb;
	}

	public Short getJysje() {
		return jysje;
	}

	public void setJysje(Short jysje) {
		this.jysje = jysje;
	}

    @Override
    public String toString() {
        return "Meta{" +
                "pk='" + pk + '\'' +
                ", nlevel=" + nlevel +
                ", minje=" + minje +
                ", ts=" + ts +
                ", maxhour=" + maxhour +
                ", jdgx='" + jdgx + '\'' +
                ", xgsj=" + xgsj +
                ", jysjb=" + jysjb +
                ", jysje=" + jysje +
                ", phone='" + phone + '\'' +
                '}';
    }
}