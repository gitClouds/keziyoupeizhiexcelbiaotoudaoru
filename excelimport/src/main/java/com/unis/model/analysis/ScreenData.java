package com.unis.model.analysis;

import java.util.Date;
import javax.persistence.*;
/**
* 筛选数据表
*
* @author zcf
* @version 1.0
* @since 2020-03-27
*/
@Table(name = "TB_ZNZF_SCREEN_DATA")
public class ScreenData  {

private static final long serialVersionUID = 1L;

/****************************************
* Basic fields
****************************************/
    /**
    * 注释: varchar
    */
        @Id
    @Column(name = "PK")
    private String pk;

    /**
    * 类型（1：yhk，2：sf）: varchar
    */
    @Column(name = "TYPE")
    private String type;

    /**
    * 明细数据pk: varchar
    */
    @Column(name = "MXPK")
    private String mxpk;

    /**
    * 分析表pk: varchar
    */
    @Column(name = "ANALYSIS_PK")
    private String analysis_pk;

    public void setPk(String pk) {
    this.pk = pk;
    }

    public String getPk() {
    return pk;
    }

    public void setType(String type) {
    this.type = type;
    }

    public String getType() {
    return type;
    }

    public void setMxpk(String mxpk) {
    this.mxpk = mxpk;
    }

    public String getMxpk() {
    return mxpk;
    }

    public void setAnalysis_pk(String analysis_pk) {
    this.analysis_pk = analysis_pk;
    }

    public String getAnalysis_pk() {
    return analysis_pk;
    }

}
