package com.unis.model.analysis;

import java.util.Date;
import javax.persistence.*;
/**
* 手动分析表
*
* @author zcf
* @version 1.0
* @since 2020-03-26
*/
@Table(name = "TB_ZNZF_MANUAL_ANALYSIS")
public class ManulAnalysis  {

private static final long serialVersionUID = 1L;

/****************************************
* Basic fields
****************************************/
    /**
    * 注释: varchar
    */
        @Id
    @Column(name = "PK")
    private String pk;

    /**
    * 创建时间: date
    */
    @Column(name = "CREATE_DATE")
    private Date create_date;

    /**
    * 创建人: varchar
    */
    @Column(name = "CREATEOR_ID")
    private String createor_id;

    /**
    * 创建人: varchar
    */
    @Column(name = "CREATEOR_NAME")
    private String createor_name;

    /**
    * 筛选条件: varchar
    */
    @Column(name = "CONDITIONS")
    private String conditions;

    public void setPk(String pk) {
    this.pk = pk;
    }

    public String getPk() {
    return pk;
    }

    public void setCreate_date(Date create_date) {
    this.create_date = create_date;
    }

    public Date getCreate_date() {
    return create_date;
    }

    public void setCreateor_id(String createor_id) {
    this.createor_id = createor_id;
    }

    public String getCreateor_id() {
    return createor_id;
    }

    public void setCreateor_name(String createor_name) {
    this.createor_name = createor_name;
    }

    public String getCreateor_name() {
    return createor_name;
    }

    public void setConditions(String conditions) {
    this.conditions = conditions;
    }

    public String getConditions() {
    return conditions;
    }

}
