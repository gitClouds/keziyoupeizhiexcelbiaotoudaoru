package com.unis.model.analysis;

import java.util.Date;
import javax.persistence.*;
/**
* 
*
* @author zcf
* @version 1.0
* @since 2020-03-26
*/
@Table(name = "TB_ZNZF_SF_MX_JG_LIST")
public class SfMxJgList  {

private static final long serialVersionUID = 1L;

/****************************************
* Basic fields
****************************************/
    /**
    * 主键: varchar
    */
        @Id
    @Column(name = "PK")
    private String pk;

    /**
    * 接警单: varchar
    */
    @Column(name = "JJDPK")
    private String jjdpk;

    /**
    * 业务申请编号: varchar
    */
    @Column(name = "APPLICATIONID")
    private String applicationid;

    /**
    * 支付订单号(支付机构内部): varchar
    */
    @Column(name = "ZFDDH")
    private String zfddh;

    /**
    * 交易类型( 
1-支付账户充值; 
2-支付账户对支付账户转账; 
3-支付账户提现/转账至银行卡 
4-支付账户消费; 
5-代收 
6-代付 
9-其他): varchar
    */
    @Column(name = "JYLX")
    private String jylx;

    /**
    * 支付类型(
1-网关交易
2-快捷交易
3-POS收单
4-支付机构内其他交易): varchar
    */
    @Column(name = "ZFLX")
    private String zflx;

    /**
    * 交易主体的出入账标识(0-出账;1-入账): varchar
    */
    @Column(name = "JDBZ")
    private String jdbz;

    /**
    * 交易时间: varchar
    */
    @Column(name = "JYSJ")
    private String jysj;

    /**
    * 币种(CNY或RMB人民币、USD美元、EUR欧元…): varchar
    */
    @Column(name = "RMBZL")
    private String rmbzl;

    /**
    * 交易金额: varchar
    */
    @Column(name = "JYJE")
    private String jyje;

    /**
    * 交易流水号(支付机构内部流水号): varchar
    */
    @Column(name = "JYLSH")
    private String jylsh;

    /**
    * 交易余额: varchar
    */
    @Column(name = "YE")
    private String ye;

    /**
    * 收款方银行卡所属银行机构编码: varchar
    */
    @Column(name = "SKYHJGDM")
    private String skyhjgdm;

    /**
    * 收款方银行卡所属银行名称: varchar
    */
    @Column(name = "SKYHJGMC")
    private String skyhjgmc;

    /**
    * 收款方银行卡所属银行卡号: varchar
    */
    @Column(name = "SKYHZH")
    private String skyhzh;

    /**
    * 收款方的支付帐号（依据业务类型选填）: varchar
    */
    @Column(name = "SKZFZH")
    private String skzfzh;

    /**
    * 消费POS机编号: varchar
    */
    @Column(name = "POSSBH")
    private String possbh;

    /**
    * 收款方的商户号: varchar
    */
    @Column(name = "SKSHM")
    private String skshm;

    /**
    * 付款方银行卡所属银行机构编码(<OnlinePayCompanyType>=02,必填): varchar
    */
    @Column(name = "FKYHJGDM")
    private String fkyhjgdm;

    /**
    * 付款方银行卡所属银行名称: varchar
    */
    @Column(name = "FKYHJGMC")
    private String fkyhjgmc;

    /**
    * 付款方银行卡所属银行卡号: varchar
    */
    @Column(name = "FKYHZH")
    private String fkyhzh;

    /**
    * 付款方的支付帐号（依据业务类型选填）: varchar
    */
    @Column(name = "FKZFZH")
    private String fkzfzh;

    /**
    * 交易设备类型(1-电脑;2-手机;3-其他): varchar
    */
    @Column(name = "JYSBLX")
    private String jysblx;

    /**
    * 交易支付设备IP: varchar
    */
    @Column(name = "ZFSBIP")
    private String zfsbip;

    /**
    * MAC地址: varchar
    */
    @Column(name = "MACDZ")
    private String macdz;

    /**
    * 交易地点经度(WGS84坐标系，单位度): varchar
    */
    @Column(name = "JYDDJD")
    private String jyddjd;

    /**
    * 交易地点纬度(WGS84坐标系，单位度): varchar
    */
    @Column(name = "JYDDWD")
    private String jyddwd;

    /**
    * 交易设备号: varchar
    */
    @Column(name = "JYSBH")
    private String jysbh;

    /**
    * 银行外部渠道交易流水号: varchar
    */
    @Column(name = "YHWBLSH")
    private String yhwblsh;

    /**
    * 备注: varchar
    */
    @Column(name = "BZ")
    private String bz;

    /**
    * 入库时间: date
    */
    @Column(name = "RKSJ")
    private Date rksj;

    /**
    * 有效性: decimal
    */
    @Column(name = "YXX")
    private int yxx;

    /**
    * IP归属地: varchar
    */
    @Column(name = "IPXZ")
    private String ipxz;

    /**
    * 注释: varchar
    */
    @Column(name = "SKFSHMC")
    private String skfshmc;

    /**
    * 注释: varchar
    */
    @Column(name = "QQPK")
    private String qqpk;
    
    @Column(name = "ZTQQPK")
    private String ztqqpk;
    
    @Column(name = "XDRYPKS")
    private String xdrypks;
    
    @Column(name = "SDRYPKS")
    private String sdrypks;
    
    @Column(name = "ZTQQTYPE")
    private String ztqqtype;
    
    @Column(name = "DISAB")
    private Short disab;

    @Column(name = "SJLY")
    private String sjly;

    public String getSjly() {
        return sjly;
    }

    public void setSjly(String sjly) {
        this.sjly = sjly;
    }
    public void setPk(String pk) {
    this.pk = pk;
    }

    public String getPk() {
    return pk;
    }

    public void setJjdpk(String jjdpk) {
    this.jjdpk = jjdpk;
    }

    public String getJjdpk() {
    return jjdpk;
    }

    public void setApplicationid(String applicationid) {
    this.applicationid = applicationid;
    }

    public String getApplicationid() {
    return applicationid;
    }

    public void setZfddh(String zfddh) {
    this.zfddh = zfddh;
    }

    public String getZfddh() {
    return zfddh;
    }

    public void setJylx(String jylx) {
    this.jylx = jylx;
    }

    public String getJylx() {
    return jylx;
    }

    public void setZflx(String zflx) {
    this.zflx = zflx;
    }

    public String getZflx() {
    return zflx;
    }

    public void setJdbz(String jdbz) {
    this.jdbz = jdbz;
    }

    public String getJdbz() {
    return jdbz;
    }

    public void setJysj(String jysj) {
    this.jysj = jysj;
    }

    public String getJysj() {
    return jysj;
    }

    public void setRmbzl(String rmbzl) {
    this.rmbzl = rmbzl;
    }

    public String getRmbzl() {
    return rmbzl;
    }

    public void setJyje(String jyje) {
    this.jyje = jyje;
    }

    public String getJyje() {
    return jyje;
    }

    public void setJylsh(String jylsh) {
    this.jylsh = jylsh;
    }

    public String getJylsh() {
    return jylsh;
    }

    public void setYe(String ye) {
    this.ye = ye;
    }

    public String getYe() {
    return ye;
    }

    public void setSkyhjgdm(String skyhjgdm) {
    this.skyhjgdm = skyhjgdm;
    }

    public String getSkyhjgdm() {
    return skyhjgdm;
    }

    public void setSkyhjgmc(String skyhjgmc) {
    this.skyhjgmc = skyhjgmc;
    }

    public String getSkyhjgmc() {
    return skyhjgmc;
    }

    public void setSkyhzh(String skyhzh) {
    this.skyhzh = skyhzh;
    }

    public String getSkyhzh() {
    return skyhzh;
    }

    public void setSkzfzh(String skzfzh) {
    this.skzfzh = skzfzh;
    }

    public String getSkzfzh() {
    return skzfzh;
    }

    public void setPossbh(String possbh) {
    this.possbh = possbh;
    }

    public String getPossbh() {
    return possbh;
    }

    public void setSkshm(String skshm) {
    this.skshm = skshm;
    }

    public String getSkshm() {
    return skshm;
    }

    public void setFkyhjgdm(String fkyhjgdm) {
    this.fkyhjgdm = fkyhjgdm;
    }

    public String getFkyhjgdm() {
    return fkyhjgdm;
    }

    public void setFkyhjgmc(String fkyhjgmc) {
    this.fkyhjgmc = fkyhjgmc;
    }

    public String getFkyhjgmc() {
    return fkyhjgmc;
    }

    public void setFkyhzh(String fkyhzh) {
    this.fkyhzh = fkyhzh;
    }

    public String getFkyhzh() {
    return fkyhzh;
    }

    public void setFkzfzh(String fkzfzh) {
    this.fkzfzh = fkzfzh;
    }

    public String getFkzfzh() {
    return fkzfzh;
    }

    public void setJysblx(String jysblx) {
    this.jysblx = jysblx;
    }

    public String getJysblx() {
    return jysblx;
    }

    public void setZfsbip(String zfsbip) {
    this.zfsbip = zfsbip;
    }

    public String getZfsbip() {
    return zfsbip;
    }

    public void setMacdz(String macdz) {
    this.macdz = macdz;
    }

    public String getMacdz() {
    return macdz;
    }

    public void setJyddjd(String jyddjd) {
    this.jyddjd = jyddjd;
    }

    public String getJyddjd() {
    return jyddjd;
    }

    public void setJyddwd(String jyddwd) {
    this.jyddwd = jyddwd;
    }

    public String getJyddwd() {
    return jyddwd;
    }

    public void setJysbh(String jysbh) {
    this.jysbh = jysbh;
    }

    public String getJysbh() {
    return jysbh;
    }

    public void setYhwblsh(String yhwblsh) {
    this.yhwblsh = yhwblsh;
    }

    public String getYhwblsh() {
    return yhwblsh;
    }

    public void setBz(String bz) {
    this.bz = bz;
    }

    public String getBz() {
    return bz;
    }

    public void setRksj(Date rksj) {
    this.rksj = rksj;
    }

    public Date getRksj() {
    return rksj;
    }

    public void setYxx(int yxx) {
    this.yxx = yxx;
    }

    public int getYxx() {
    return yxx;
    }

    public void setIpxz(String ipxz) {
    this.ipxz = ipxz;
    }

    public String getIpxz() {
    return ipxz;
    }

    public void setSkfshmc(String skfshmc) {
    this.skfshmc = skfshmc;
    }

    public String getSkfshmc() {
    return skfshmc;
    }

    public void setQqpk(String qqpk) {
    this.qqpk = qqpk;
    }

    public String getQqpk() {
    return qqpk;
    }

	public String getZtqqpk() {
		return ztqqpk;
	}

	public void setZtqqpk(String ztqqpk) {
		this.ztqqpk = ztqqpk;
	}

	public String getXdrypks() {
		return xdrypks;
	}

	public void setXdrypks(String xdrypks) {
		this.xdrypks = xdrypks;
	}

	public String getSdrypks() {
		return sdrypks;
	}

	public void setSdrypks(String sdrypks) {
		this.sdrypks = sdrypks;
	}

	public String getZtqqtype() {
		return ztqqtype;
	}

	public void setZtqqtype(String ztqqtype) {
		this.ztqqtype = ztqqtype;
	}

	public Short getDisab() {
		return disab;
	}

	public void setDisab(Short disab) {
		this.disab = disab;
	}

}
