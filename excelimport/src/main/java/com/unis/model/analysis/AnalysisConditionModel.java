package com.unis.model.analysis;

public class AnalysisConditionModel {
	
	private String jyjebs;
	private String jysjBegin;
	private String jysjEnd;
	private String jysjb;
	private String jysje;
	private String isxdsd;
	private String cxzh;
	public String getIsxdsd() {
		return isxdsd;
	}
	public void setIsxdsd(String isxdsd) {
		this.isxdsd = isxdsd;
	}
	public String getJyjebs() {
		return jyjebs;
	}
	public void setJyjebs(String jyjebs) {
		this.jyjebs = jyjebs;
	}
	public String getJysjBegin() {
		return jysjBegin;
	}
	public void setJysjBegin(String jysjBegin) {
		this.jysjBegin = jysjBegin;
	}
	public String getJysjEnd() {
		return jysjEnd;
	}
	public void setJysjEnd(String jysjEnd) {
		this.jysjEnd = jysjEnd;
	}
	public String getJysjb() {
		return jysjb;
	}
	public void setJysjb(String jysjb) {
		this.jysjb = jysjb;
	}
	public String getJysje() {
		return jysje;
	}
	public void setJysje(String jysje) {
		this.jysje = jysje;
	}

    public String getCxzh() {
        return cxzh;
    }

    public void setCxzh(String cxzh) {
        this.cxzh = cxzh;
    }
}
