package com.unis.model.analysis;

import java.util.Date;
import javax.persistence.*;
/**
* 
*
* @author zcf
* @version 1.0
* @since 2020-03-26
*/
@Table(name = "TB_ZNZF_YHK_MX_JG_LIST")
public class YhkMxJgList  {

private static final long serialVersionUID = 1L;

/****************************************
* Basic fields
****************************************/
    /**
    * 主键: varchar
    */
        @Id
    @Column(name = "PK")
    private String pk;

    /**
    * 接警单: varchar
    */
    @Column(name = "JJDPK")
    private String jjdpk;

    /**
    * 业务申请编号: varchar
    */
    @Column(name = "APPLICATIONID")
    private String applicationid;

    /**
    * 交易类型: varchar
    */
    @Column(name = "JYLX")
    private String jylx;

    /**
    * 借贷标志(0-:借";1-"贷"): varchar
    */
    @Column(name = "JDBZ")
    private String jdbz;

    /**
    * 交易金额: decimal
    */
    @Column(name = "JYJE")
    private int jyje;

    /**
    * 交易余额: decimal
    */
    @Column(name = "JYYE")
    private int jyye;

    /**
    * 交易时间: date
    */
    @Column(name = "JYSJ")
    private Date jysj;

    /**
    * 交易流水号: varchar
    */
    @Column(name = "JYLSH")
    private String jylsh;

    /**
    * 交易对方名称: varchar
    */
    @Column(name = "JYDFMC")
    private String jydfmc;

    /**
    * 交易对方账卡号: varchar
    */
    @Column(name = "JYDFZH")
    private String jydfzh;

    /**
    * 交易对方证件号码: varchar
    */
    @Column(name = "JYDFZJHM")
    private String jydfzjhm;

    /**
    * 交易对方账号开户行: varchar
    */
    @Column(name = "JYDFKHH")
    private String jydfkhh;

    /**
    * 交易是否成功(00-成功;01-失败): varchar
    */
    @Column(name = "JYSFCG")
    private String jysfcg;

    /**
    * 现金标志(00-其它;01-现金交易): varchar
    */
    @Column(name = "XJBZ")
    private String xjbz;

    /**
    * 交易网点名称: varchar
    */
    @Column(name = "JYWDMC")
    private String jywdmc;

    /**
    * 交易网点代码: varchar
    */
    @Column(name = "JYWDDM")
    private String jywddm;

    /**
    * 交易摘要: varchar
    */
    @Column(name = "JYZY")
    private String jyzy;

    /**
    * IP地址: varchar
    */
    @Column(name = "IPDZ")
    private String ipdz;

    /**
    * MAC地址: varchar
    */
    @Column(name = "MACDZ")
    private String macdz;

    /**
    * 日志号: varchar
    */
    @Column(name = "RZH")
    private String rzh;

    /**
    * 传票号: varchar
    */
    @Column(name = "CPH")
    private String cph;

    /**
    * 凭证号: varchar
    */
    @Column(name = "PZH")
    private String pzh;

    /**
    * 终端号: varchar
    */
    @Column(name = "ZDH")
    private String zdh;

    /**
    * 交易柜员号: varchar
    */
    @Column(name = "JYGYH")
    private String jygyh;

    /**
    * 商户名称: varchar
    */
    @Column(name = "SHMC")
    private String shmc;

    /**
    * 商户号: varchar
    */
    @Column(name = "SHH")
    private String shh;

    /**
    * 币种(CNY人民币、USD美元、EUR欧元…): varchar
    */
    @Column(name = "RMBZL")
    private String rmbzl;

    /**
    * 备注(网关交易时需要反馈外部流水号): varchar
    */
    @Column(name = "BZ")
    private String bz;

    /**
    * 凭证种类: varchar
    */
    @Column(name = "PZZL")
    private String pzzl;

    /**
    * 入库时间: date
    */
    @Column(name = "RKSJ")
    private Date rksj;

    /**
    * 有效性: decimal
    */
    @Column(name = "YXX")
    private int yxx;

    /**
    * ip归属地: varchar
    */
    @Column(name = "IPXZ")
    private String ipxz;

    /**
    * 注释: varchar
    */
    @Column(name = "QQPK")
    private String qqpk;
    
    @Column(name = "ZTQQPK")
    private String ztqqpk;
    
    @Column(name = "XDRYPKS")
    private String xdrypks;
    
    @Column(name = "SDRYPKS")
    private String sdrypks;
    
    @Column(name = "ZTQQTYPE")
    private String ztqqtype;
    
    @Column(name = "DISAB")
    private Short disab;

    public void setPk(String pk) {
    this.pk = pk;
    }

    public String getPk() {
    return pk;
    }

    public void setJjdpk(String jjdpk) {
    this.jjdpk = jjdpk;
    }

    public String getJjdpk() {
    return jjdpk;
    }

    public void setApplicationid(String applicationid) {
    this.applicationid = applicationid;
    }

    public String getApplicationid() {
    return applicationid;
    }

    public void setJylx(String jylx) {
    this.jylx = jylx;
    }

    public String getJylx() {
    return jylx;
    }

    public void setJdbz(String jdbz) {
    this.jdbz = jdbz;
    }

    public String getJdbz() {
    return jdbz;
    }

    public void setJyje(int jyje) {
    this.jyje = jyje;
    }

    public int getJyje() {
    return jyje;
    }

    public void setJyye(int jyye) {
    this.jyye = jyye;
    }

    public int getJyye() {
    return jyye;
    }

    public void setJysj(Date jysj) {
    this.jysj = jysj;
    }

    public Date getJysj() {
    return jysj;
    }

    public void setJylsh(String jylsh) {
    this.jylsh = jylsh;
    }

    public String getJylsh() {
    return jylsh;
    }

    public void setJydfmc(String jydfmc) {
    this.jydfmc = jydfmc;
    }

    public String getJydfmc() {
    return jydfmc;
    }

    public void setJydfzh(String jydfzh) {
    this.jydfzh = jydfzh;
    }

    public String getJydfzh() {
    return jydfzh;
    }

    public void setJydfzjhm(String jydfzjhm) {
    this.jydfzjhm = jydfzjhm;
    }

    public String getJydfzjhm() {
    return jydfzjhm;
    }

    public void setJydfkhh(String jydfkhh) {
    this.jydfkhh = jydfkhh;
    }

    public String getJydfkhh() {
    return jydfkhh;
    }

    public void setJysfcg(String jysfcg) {
    this.jysfcg = jysfcg;
    }

    public String getJysfcg() {
    return jysfcg;
    }

    public void setXjbz(String xjbz) {
    this.xjbz = xjbz;
    }

    public String getXjbz() {
    return xjbz;
    }

    public void setJywdmc(String jywdmc) {
    this.jywdmc = jywdmc;
    }

    public String getJywdmc() {
    return jywdmc;
    }

    public void setJywddm(String jywddm) {
    this.jywddm = jywddm;
    }

    public String getJywddm() {
    return jywddm;
    }

    public void setJyzy(String jyzy) {
    this.jyzy = jyzy;
    }

    public String getJyzy() {
    return jyzy;
    }

    public void setIpdz(String ipdz) {
    this.ipdz = ipdz;
    }

    public String getIpdz() {
    return ipdz;
    }

    public void setMacdz(String macdz) {
    this.macdz = macdz;
    }

    public String getMacdz() {
    return macdz;
    }

    public void setRzh(String rzh) {
    this.rzh = rzh;
    }

    public String getRzh() {
    return rzh;
    }

    public void setCph(String cph) {
    this.cph = cph;
    }

    public String getCph() {
    return cph;
    }

    public void setPzh(String pzh) {
    this.pzh = pzh;
    }

    public String getPzh() {
    return pzh;
    }

    public void setZdh(String zdh) {
    this.zdh = zdh;
    }

    public String getZdh() {
    return zdh;
    }

    public void setJygyh(String jygyh) {
    this.jygyh = jygyh;
    }

    public String getJygyh() {
    return jygyh;
    }

    public void setShmc(String shmc) {
    this.shmc = shmc;
    }

    public String getShmc() {
    return shmc;
    }

    public void setShh(String shh) {
    this.shh = shh;
    }

    public String getShh() {
    return shh;
    }

    public void setRmbzl(String rmbzl) {
    this.rmbzl = rmbzl;
    }

    public String getRmbzl() {
    return rmbzl;
    }

    public void setBz(String bz) {
    this.bz = bz;
    }

    public String getBz() {
    return bz;
    }

    public void setPzzl(String pzzl) {
    this.pzzl = pzzl;
    }

    public String getPzzl() {
    return pzzl;
    }

    public void setRksj(Date rksj) {
    this.rksj = rksj;
    }

    public Date getRksj() {
    return rksj;
    }

    public void setYxx(int yxx) {
    this.yxx = yxx;
    }

    public int getYxx() {
    return yxx;
    }

    public void setIpxz(String ipxz) {
    this.ipxz = ipxz;
    }

    public String getIpxz() {
    return ipxz;
    }

    public void setQqpk(String qqpk) {
    this.qqpk = qqpk;
    }

    public String getQqpk() {
    return qqpk;
    }

	public String getZtqqpk() {
		return ztqqpk;
	}

	public void setZtqqpk(String ztqqpk) {
		this.ztqqpk = ztqqpk;
	}

	public String getXdrypks() {
		return xdrypks;
	}

	public void setXdrypks(String xdrypks) {
		this.xdrypks = xdrypks;
	}

	public String getSdrypks() {
		return sdrypks;
	}

	public void setSdrypks(String sdrypks) {
		this.sdrypks = sdrypks;
	}

	public String getZtqqtype() {
		return ztqqtype;
	}

	public void setZtqqtype(String ztqqtype) {
		this.ztqqtype = ztqqtype;
	}

	public Short getDisab() {
		return disab;
	}

	public void setDisab(Short disab) {
		this.disab = disab;
	}
	

}
