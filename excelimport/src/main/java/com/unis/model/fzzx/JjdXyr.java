package com.unis.model.fzzx;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;
import javax.persistence.*;

@Table(name = "TB_FZZX_JJD_XYR")
public class JjdXyr {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "JJDPK")
    private String jjdpk;

    @Column(name = "SFZH")
    private String sfzh;

    @Column(name = "DH")
    private String dh;

    @Column(name = "YHK")
    private String yhk;

    @Column(name = "CKR")
    private String ckr;

    @Column(name = "ZFB")
    private String zfb;

    @Column(name = "WX")
    private String wx;

    @Column(name = "JYDH")
    private String jydh;

    @Column(name = "CFT")
    private String cft;

    @Column(name = "QQ")
    private String qq;

    @Column(name = "ZZFS")
    private Short zzfs;

    @Column(name = "ZZJE")
    private BigDecimal zzje;

    @Column(name = "ZZSJ")
    private Date zzsj;

    @Column(name = "BZ")
    private String bz;

    @Column(name = "CJSJ")
    private Date cjsj;

    @Column(name = "XGSJ")
    private Date xgsj;

    @Column(name = "LRDWDM")
    private String lrdwdm;

    @Column(name = "LRDWMC")
    private String lrdwmc;

    @Column(name = "LRRXM")
    private String lrrxm;

    @Column(name = "LRRJH")
    private String lrrjh;
    private String jgdm;
    private String jgmc;

    public String getJgdm() {
        return jgdm;
    }

    public void setJgdm(String jgdm) {
        this.jgdm = jgdm;
    }

    public String getJgmc() {
        return jgmc;
    }

    public void setJgmc(String jgmc) {
        this.jgmc = jgmc;
    }

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return JJDPK
     */
    public String getJjdpk() {
        return jjdpk;
    }

    /**
     * @param jjdpk
     */
    public void setJjdpk(String jjdpk) {
        this.jjdpk = jjdpk == null ? null : jjdpk.trim();
    }

    /**
     * @return SFZH
     */
    public String getSfzh() {
        return sfzh;
    }

    /**
     * @param sfzh
     */
    public void setSfzh(String sfzh) {
        this.sfzh = sfzh == null ? null : sfzh.trim();
    }

    /**
     * @return DH
     */
    public String getDh() {
        return dh;
    }

    /**
     * @param dh
     */
    public void setDh(String dh) {
        this.dh = dh == null ? null : dh.trim();
    }

    /**
     * @return YHK
     */
    public String getYhk() {
        return yhk;
    }

    /**
     * @param yhk
     */
    public void setYhk(String yhk) {
        this.yhk = yhk == null ? null : yhk.trim();
    }

    /**
     * @return CKR
     */
    public String getCkr() {
        return ckr;
    }

    /**
     * @param ckr
     */
    public void setCkr(String ckr) {
        this.ckr = ckr == null ? null : ckr.trim();
    }

    /**
     * @return ZFB
     */
    public String getZfb() {
        return zfb;
    }

    /**
     * @param zfb
     */
    public void setZfb(String zfb) {
        this.zfb = zfb == null ? null : zfb.trim();
    }

    /**
     * @return WX
     */
    public String getWx() {
        return wx;
    }

    /**
     * @param wx
     */
    public void setWx(String wx) {
        this.wx = wx == null ? null : wx.trim();
    }

    /**
     * @return JYDH
     */
    public String getJydh() {
        return jydh;
    }

    /**
     * @param jydh
     */
    public void setJydh(String jydh) {
        this.jydh = jydh == null ? null : jydh.trim();
    }

    /**
     * @return CFT
     */
    public String getCft() {
        return cft;
    }

    /**
     * @param cft
     */
    public void setCft(String cft) {
        this.cft = cft == null ? null : cft.trim();
    }

    /**
     * @return QQ
     */
    public String getQq() {
        return qq;
    }

    /**
     * @param qq
     */
    public void setQq(String qq) {
        this.qq = qq == null ? null : qq.trim();
    }

    /**
     * @return ZZFS
     */
    public Short getZzfs() {
        return zzfs;
    }

    /**
     * @param zzfs
     */
    public void setZzfs(Short zzfs) {
        this.zzfs = zzfs;
    }

    /**
     * @return ZZJE
     */
    public BigDecimal getZzje() {
        return zzje;
    }

    /**
     * @param zzje
     */
    public void setZzje(BigDecimal zzje) {
        this.zzje = zzje;
    }

    /**
     * @return ZZSJ
     */
    public Date getZzsj() {
        return zzsj;
    }

    /**
     * @param zzsj
     */
    public void setZzsj(Date zzsj) {
        this.zzsj = zzsj;
    }

    /**
     * @return BZ
     */
    public String getBz() {
        return bz;
    }

    /**
     * @param bz
     */
    public void setBz(String bz) {
        this.bz = bz == null ? null : bz.trim();
    }

    /**
     * @return CJSJ
     */
    public Date getCjsj() {
        return cjsj;
    }

    /**
     * @param cjsj
     */
    public void setCjsj(Date cjsj) {
        this.cjsj = cjsj;
    }

    /**
     * @return XGSJ
     */
    public Date getXgsj() {
        return xgsj;
    }

    /**
     * @param xgsj
     */
    public void setXgsj(Date xgsj) {
        this.xgsj = xgsj;
    }

    /**
     * @return LRDWDM
     */
    public String getLrdwdm() {
        return lrdwdm;
    }

    /**
     * @param lrdwdm
     */
    public void setLrdwdm(String lrdwdm) {
        this.lrdwdm = lrdwdm == null ? null : lrdwdm.trim();
    }

    /**
     * @return LRDWMC
     */
    public String getLrdwmc() {
        return lrdwmc;
    }

    /**
     * @param lrdwmc
     */
    public void setLrdwmc(String lrdwmc) {
        this.lrdwmc = lrdwmc == null ? null : lrdwmc.trim();
    }

    /**
     * @return LRRXM
     */
    public String getLrrxm() {
        return lrrxm;
    }

    /**
     * @param lrrxm
     */
    public void setLrrxm(String lrrxm) {
        this.lrrxm = lrrxm == null ? null : lrrxm.trim();
    }

    /**
     * @return LRRJH
     */
    public String getLrrjh() {
        return lrrjh;
    }

    /**
     * @param lrrjh
     */
    public void setLrrjh(String lrrjh) {
        this.lrrjh = lrrjh == null ? null : lrrjh.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", jjdpk=").append(jjdpk);
        sb.append(", sfzh=").append(sfzh);
        sb.append(", dh=").append(dh);
        sb.append(", yhk=").append(yhk);
        sb.append(", ckr=").append(ckr);
        sb.append(", zfb=").append(zfb);
        sb.append(", wx=").append(wx);
        sb.append(", jydh=").append(jydh);
        sb.append(", cft=").append(cft);
        sb.append(", qq=").append(qq);
        sb.append(", zzfs=").append(zzfs);
        sb.append(", zzje=").append(zzje);
        sb.append(", zzsj=").append(zzsj);
        sb.append(", bz=").append(bz);
        sb.append(", cjsj=").append(cjsj);
        sb.append(", xgsj=").append(xgsj);
        sb.append(", lrdwdm=").append(lrdwdm);
        sb.append(", lrdwmc=").append(lrdwmc);
        sb.append(", lrrxm=").append(lrrxm);
        sb.append(", lrrjh=").append(lrrjh);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JjdXyr jjdXyr = (JjdXyr) o;
        return Objects.equals(pk, jjdXyr.pk) &&
                Objects.equals(jjdpk, jjdXyr.jjdpk) &&
                Objects.equals(sfzh, jjdXyr.sfzh) &&
                Objects.equals(dh, jjdXyr.dh) &&
                Objects.equals(yhk, jjdXyr.yhk) &&
                Objects.equals(ckr, jjdXyr.ckr) &&
                Objects.equals(zfb, jjdXyr.zfb) &&
                Objects.equals(wx, jjdXyr.wx) &&
                Objects.equals(jydh, jjdXyr.jydh) &&
                Objects.equals(cft, jjdXyr.cft) &&
                Objects.equals(qq, jjdXyr.qq) &&
                Objects.equals(zzfs, jjdXyr.zzfs) &&
                Objects.equals(zzje, jjdXyr.zzje) &&
                Objects.equals(zzsj, jjdXyr.zzsj);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pk, jjdpk, sfzh, dh, yhk, ckr, zfb, wx, jydh, cft, qq, zzfs, zzje, zzsj);
    }
}