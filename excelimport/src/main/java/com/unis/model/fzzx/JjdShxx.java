package com.unis.model.fzzx;

import java.util.Date;
import java.util.Objects;
import javax.persistence.*;

@Table(name = "TB_FZZX_JJD_SHXX")
public class JjdShxx {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "JJDPK")
    private String jjdpk;

    @Column(name = "SHMC")
    private String shmc;

    @Column(name = "LXFS")
    private String lxfs;

    @Column(name = "SHH")
    private String shh;

    @Column(name = "JYDH")
    private String jydh;

    @Column(name = "CJSJ")
    private Date cjsj;

    @Column(name = "XGSJ")
    private Date xgsj;

    @Column(name = "LRDWDM")
    private String lrdwdm;

    @Column(name = "LRDWMC")
    private String lrdwmc;

    @Column(name = "LRRXM")
    private String lrrxm;

    @Column(name = "LRRJH")
    private String lrrjh;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return JJDPK
     */
    public String getJjdpk() {
        return jjdpk;
    }

    /**
     * @param jjdpk
     */
    public void setJjdpk(String jjdpk) {
        this.jjdpk = jjdpk == null ? null : jjdpk.trim();
    }

    /**
     * @return SHMC
     */
    public String getShmc() {
        return shmc;
    }

    /**
     * @param shmc
     */
    public void setShmc(String shmc) {
        this.shmc = shmc == null ? null : shmc.trim();
    }

    /**
     * @return LXFS
     */
    public String getLxfs() {
        return lxfs;
    }

    /**
     * @param lxfs
     */
    public void setLxfs(String lxfs) {
        this.lxfs = lxfs == null ? null : lxfs.trim();
    }

    /**
     * @return SHH
     */
    public String getShh() {
        return shh;
    }

    /**
     * @param shh
     */
    public void setShh(String shh) {
        this.shh = shh == null ? null : shh.trim();
    }

    /**
     * @return JYDH
     */
    public String getJydh() {
        return jydh;
    }

    /**
     * @param jydh
     */
    public void setJydh(String jydh) {
        this.jydh = jydh == null ? null : jydh.trim();
    }

    /**
     * @return CJSJ
     */
    public Date getCjsj() {
        return cjsj;
    }

    /**
     * @param cjsj
     */
    public void setCjsj(Date cjsj) {
        this.cjsj = cjsj;
    }

    /**
     * @return XGSJ
     */
    public Date getXgsj() {
        return xgsj;
    }

    /**
     * @param xgsj
     */
    public void setXgsj(Date xgsj) {
        this.xgsj = xgsj;
    }

    /**
     * @return LRDWDM
     */
    public String getLrdwdm() {
        return lrdwdm;
    }

    /**
     * @param lrdwdm
     */
    public void setLrdwdm(String lrdwdm) {
        this.lrdwdm = lrdwdm == null ? null : lrdwdm.trim();
    }

    /**
     * @return LRDWMC
     */
    public String getLrdwmc() {
        return lrdwmc;
    }

    /**
     * @param lrdwmc
     */
    public void setLrdwmc(String lrdwmc) {
        this.lrdwmc = lrdwmc == null ? null : lrdwmc.trim();
    }

    /**
     * @return LRRXM
     */
    public String getLrrxm() {
        return lrrxm;
    }

    /**
     * @param lrrxm
     */
    public void setLrrxm(String lrrxm) {
        this.lrrxm = lrrxm == null ? null : lrrxm.trim();
    }

    /**
     * @return LRRJH
     */
    public String getLrrjh() {
        return lrrjh;
    }

    /**
     * @param lrrjh
     */
    public void setLrrjh(String lrrjh) {
        this.lrrjh = lrrjh == null ? null : lrrjh.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", jjdpk=").append(jjdpk);
        sb.append(", shmc=").append(shmc);
        sb.append(", lxfs=").append(lxfs);
        sb.append(", shh=").append(shh);
        sb.append(", jydh=").append(jydh);
        sb.append(", cjsj=").append(cjsj);
        sb.append(", xgsj=").append(xgsj);
        sb.append(", lrdwdm=").append(lrdwdm);
        sb.append(", lrdwmc=").append(lrdwmc);
        sb.append(", lrrxm=").append(lrrxm);
        sb.append(", lrrjh=").append(lrrjh);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JjdShxx shxx = (JjdShxx) o;
        return Objects.equals(pk, shxx.pk) &&
                Objects.equals(jjdpk, shxx.jjdpk) &&
                Objects.equals(shmc, shxx.shmc) &&
                Objects.equals(lxfs, shxx.lxfs) &&
                Objects.equals(shh, shxx.shh) &&
                Objects.equals(jydh, shxx.jydh);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pk, jjdpk, shmc, lxfs, shh, jydh);
    }
}