package com.unis.model.fzzx;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_FZZX_ZNZF_BMD")
public class JjdBmd {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "ZH")
    private String zh;

    @Column(name = "ZHMC")
    private String zhmc;

    @Column(name = "BZ")
    private String bz;

    @Column(name = "LRDWDM")
    private String lrdwdm;

    @Column(name = "LRDWMC")
    private String lrdwmc;

    @Column(name = "LRRXM")
    private String lrrxm;

    @Column(name = "LRRJH")
    private String lrrjh;

    @Column(name = "CJSJ")
    private Date cjsj;

    @Column(name = "XGSJ")
    private Date xgsj;

    @Column(name = "XGDWDM")
    private String xgdwdm;

    @Column(name = "XGDWMC")
    private String xgdwmc;

    @Column(name = "XGR")
    private String xgr;

    @Column(name = "XGRJH")
    private String xgrjh;

    @Column(name = "YXX")
    private Short yxx;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return ZH
     */
    public String getZh() {
        return zh;
    }

    /**
     * @param zh
     */
    public void setZh(String zh) {
        this.zh = zh == null ? null : zh.trim();
    }

    /**
     * @return ZHMC
     */
    public String getZhmc() {
        return zhmc;
    }

    /**
     * @param zhmc
     */
    public void setZhmc(String zhmc) {
        this.zhmc = zhmc == null ? null : zhmc.trim();
    }

    /**
     * @return BZ
     */
    public String getBz() {
        return bz;
    }

    /**
     * @param bz
     */
    public void setBz(String bz) {
        this.bz = bz == null ? null : bz.trim();
    }

    /**
     * @return LRDWDM
     */
    public String getLrdwdm() {
        return lrdwdm;
    }

    /**
     * @param lrdwdm
     */
    public void setLrdwdm(String lrdwdm) {
        this.lrdwdm = lrdwdm == null ? null : lrdwdm.trim();
    }

    /**
     * @return LRDWMC
     */
    public String getLrdwmc() {
        return lrdwmc;
    }

    /**
     * @param lrdwmc
     */
    public void setLrdwmc(String lrdwmc) {
        this.lrdwmc = lrdwmc == null ? null : lrdwmc.trim();
    }

    /**
     * @return LRRXM
     */
    public String getLrrxm() {
        return lrrxm;
    }

    /**
     * @param lrrxm
     */
    public void setLrrxm(String lrrxm) {
        this.lrrxm = lrrxm == null ? null : lrrxm.trim();
    }

    /**
     * @return LRRJH
     */
    public String getLrrjh() {
        return lrrjh;
    }

    /**
     * @param lrrjh
     */
    public void setLrrjh(String lrrjh) {
        this.lrrjh = lrrjh == null ? null : lrrjh.trim();
    }

    /**
     * @return CJSJ
     */
    public Date getCjsj() {
        return cjsj;
    }

    /**
     * @param cjsj
     */
    public void setCjsj(Date cjsj) {
        this.cjsj = cjsj;
    }

    /**
     * @return XGSJ
     */
    public Date getXgsj() {
        return xgsj;
    }

    /**
     * @param xgsj
     */
    public void setXgsj(Date xgsj) {
        this.xgsj = xgsj;
    }

    /**
     * @return XGDWDM
     */
    public String getXgdwdm() {
        return xgdwdm;
    }

    /**
     * @param xgdwdm
     */
    public void setXgdwdm(String xgdwdm) {
        this.xgdwdm = xgdwdm == null ? null : xgdwdm.trim();
    }

    /**
     * @return XGDWMC
     */
    public String getXgdwmc() {
        return xgdwmc;
    }

    /**
     * @param xgdwmc
     */
    public void setXgdwmc(String xgdwmc) {
        this.xgdwmc = xgdwmc == null ? null : xgdwmc.trim();
    }

    /**
     * @return XGR
     */
    public String getXgr() {
        return xgr;
    }

    /**
     * @param xgr
     */
    public void setXgr(String xgr) {
        this.xgr = xgr == null ? null : xgr.trim();
    }

    /**
     * @return XGRJH
     */
    public String getXgrjh() {
        return xgrjh;
    }

    /**
     * @param xgrjh
     */
    public void setXgrjh(String xgrjh) {
        this.xgrjh = xgrjh == null ? null : xgrjh.trim();
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", zh=").append(zh);
        sb.append(", zhmc=").append(zhmc);
        sb.append(", bz=").append(bz);
        sb.append(", lrdwdm=").append(lrdwdm);
        sb.append(", lrdwmc=").append(lrdwmc);
        sb.append(", lrrxm=").append(lrrxm);
        sb.append(", lrrjh=").append(lrrjh);
        sb.append(", cjsj=").append(cjsj);
        sb.append(", xgsj=").append(xgsj);
        sb.append(", xgdwdm=").append(xgdwdm);
        sb.append(", xgdwmc=").append(xgdwmc);
        sb.append(", xgr=").append(xgr);
        sb.append(", xgrjh=").append(xgrjh);
        sb.append(", yxx=").append(yxx);
        sb.append("]");
        return sb.toString();
    }
}