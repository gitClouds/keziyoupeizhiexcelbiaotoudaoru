package com.unis.model.fzzx;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_FZZX_USER_FOCUS")
public class UserFocus {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "JJDPK")
    private String jjdpk;

    @Column(name = "USERPK")
    private String userpk;

    @Column(name = "YXX")
    private Short yxx;

    @Column(name = "RKSJ")
    private Date rksj;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return JJDPK
     */
    public String getJjdpk() {
        return jjdpk;
    }

    /**
     * @param jjdpk
     */
    public void setJjdpk(String jjdpk) {
        this.jjdpk = jjdpk == null ? null : jjdpk.trim();
    }

    /**
     * @return USERPK
     */
    public String getUserpk() {
        return userpk;
    }

    /**
     * @param userpk
     */
    public void setUserpk(String userpk) {
        this.userpk = userpk == null ? null : userpk.trim();
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", jjdpk=").append(jjdpk);
        sb.append(", userpk=").append(userpk);
        sb.append(", yxx=").append(yxx);
        sb.append(", rksj=").append(rksj);
        sb.append("]");
        return sb.toString();
    }
}