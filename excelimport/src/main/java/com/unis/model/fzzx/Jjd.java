package com.unis.model.fzzx;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;
import javax.persistence.*;

@Table(name = "TB_FZZX_JJD")
public class Jjd {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "JJDHM")
    private String jjdhm;

    @Column(name = "AJBH")
    private String ajbh;

    @Column(name = "AJID")
    private String ajid;

    @Column(name = "ZFJE")
    private BigDecimal zfje;

    @Column(name = "JDXZ")
    private String jdxz;

    @Column(name = "SLDW")
    private String sldw;

    @Column(name = "SLDWMC")
    private String sldwmc;

    @Column(name = "JJSJ")
    private Date jjsj;

    @Column(name = "BARXM")
    private String barxm;

    @Column(name = "BARSFZH")
    private String barsfzh;

    @Column(name = "BARLXDH")
    private String barlxdh;

    @Column(name = "JJFS")
    private String jjfs;

    @Column(name = "AJLX")
    private String ajlx;

    @Column(name = "AJLB")
    private String ajlb;

    @Column(name = "AFSJ")
    private Date afsj;

    @Column(name = "SAJE")
    private BigDecimal saje;

    @Column(name = "AFDD")
    private String afdd;

    @Column(name = "JYAQ")
    private String jyaq;

    @Column(name = "SAWZ")
    private String sawz;

    @Column(name = "BZ")
    private String bz;

    @Column(name = "ZT")
    private Short zt;

    @Column(name = "CJSJ")
    private Date cjsj;

    @Column(name = "XGSJ")
    private Date xgsj;

    @Column(name = "AREA")
    private String area;

    @Column(name = "LRDWDM")
    private String lrdwdm;

    @Column(name = "LRDWMC")
    private String lrdwmc;

    @Column(name = "LRRXM")
    private String lrrxm;

    @Column(name = "LRRJH")
    private String lrrjh;

    @Column(name = "ACCURATETYPE")
    private Short accuratetype;
    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return JJDHM
     */
    public String getJjdhm() {
        return jjdhm;
    }

    /**
     * @param jjdhm
     */
    public void setJjdhm(String jjdhm) {
        this.jjdhm = jjdhm == null ? null : jjdhm.trim();
    }

    /**
     * @return AJBH
     */
    public String getAjbh() {
        return ajbh;
    }

    /**
     * @param ajbh
     */
    public void setAjbh(String ajbh) {
        this.ajbh = ajbh == null ? null : ajbh.trim();
    }

    /**
     * @return AJID
     */
    public String getAjid() {
        return ajid;
    }

    /**
     * @param ajid
     */
    public void setAjid(String ajid) {
        this.ajid = ajid == null ? null : ajid.trim();
    }

    /**
     * @return ZFJE
     */
    public BigDecimal getZfje() {
        return zfje;
    }

    /**
     * @param zfje
     */
    public void setZfje(BigDecimal zfje) {
        this.zfje = zfje;
    }

    /**
     * @return JDXZ
     */
    public String getJdxz() {
        return jdxz;
    }

    /**
     * @param jdxz
     */
    public void setJdxz(String jdxz) {
        this.jdxz = jdxz == null ? null : jdxz.trim();
    }

    /**
     * @return SLDW
     */
    public String getSldw() {
        return sldw;
    }

    /**
     * @param sldw
     */
    public void setSldw(String sldw) {
        this.sldw = sldw == null ? null : sldw.trim();
    }

    /**
     * @return SLDWMC
     */
    public String getSldwmc() {
        return sldwmc;
    }

    /**
     * @param sldwmc
     */
    public void setSldwmc(String sldwmc) {
        this.sldwmc = sldwmc == null ? null : sldwmc.trim();
    }

    /**
     * @return JJSJ
     */
    public Date getJjsj() {
        return jjsj;
    }

    /**
     * @param jjsj
     */
    public void setJjsj(Date jjsj) {
        this.jjsj = jjsj;
    }

    /**
     * @return BARXM
     */
    public String getBarxm() {
        return barxm;
    }

    /**
     * @param barxm
     */
    public void setBarxm(String barxm) {
        this.barxm = barxm == null ? null : barxm.trim();
    }

    /**
     * @return BARSFZH
     */
    public String getBarsfzh() {
        return barsfzh;
    }

    /**
     * @param barsfzh
     */
    public void setBarsfzh(String barsfzh) {
        this.barsfzh = barsfzh == null ? null : barsfzh.trim();
    }

    /**
     * @return BARLXDH
     */
    public String getBarlxdh() {
        return barlxdh;
    }

    /**
     * @param barlxdh
     */
    public void setBarlxdh(String barlxdh) {
        this.barlxdh = barlxdh == null ? null : barlxdh.trim();
    }

    /**
     * @return JJFS
     */
    public String getJjfs() {
        return jjfs;
    }

    /**
     * @param jjfs
     */
    public void setJjfs(String jjfs) {
        this.jjfs = jjfs == null ? null : jjfs.trim();
    }

    /**
     * @return AJLX
     */
    public String getAjlx() {
        return ajlx;
    }

    /**
     * @param ajlx
     */
    public void setAjlx(String ajlx) {
        this.ajlx = ajlx == null ? null : ajlx.trim();
    }

    /**
     * @return AJLB
     */
    public String getAjlb() {
        return ajlb;
    }

    /**
     * @param ajlb
     */
    public void setAjlb(String ajlb) {
        this.ajlb = ajlb == null ? null : ajlb.trim();
    }

    /**
     * @return AFSJ
     */
    public Date getAfsj() {
        return afsj;
    }

    /**
     * @param afsj
     */
    public void setAfsj(Date afsj) {
        this.afsj = afsj;
    }

    /**
     * @return SAJE
     */
    public BigDecimal getSaje() {
        return saje;
    }

    /**
     * @param saje
     */
    public void setSaje(BigDecimal saje) {
        this.saje = saje;
    }

    /**
     * @return AFDD
     */
    public String getAfdd() {
        return afdd;
    }

    /**
     * @param afdd
     */
    public void setAfdd(String afdd) {
        this.afdd = afdd == null ? null : afdd.trim();
    }

    /**
     * @return JYAQ
     */
    public String getJyaq() {
        return jyaq;
    }

    /**
     * @param jyaq
     */
    public void setJyaq(String jyaq) {
        this.jyaq = jyaq == null ? null : jyaq.trim();
    }

    /**
     * @return SAWZ
     */
    public String getSawz() {
        return sawz;
    }

    /**
     * @param sawz
     */
    public void setSawz(String sawz) {
        this.sawz = sawz == null ? null : sawz.trim();
    }

    /**
     * @return BZ
     */
    public String getBz() {
        return bz;
    }

    /**
     * @param bz
     */
    public void setBz(String bz) {
        this.bz = bz == null ? null : bz.trim();
    }

    /**
     * @return ZT
     */
    public Short getZt() {
        return zt;
    }

    /**
     * @param zt
     */
    public void setZt(Short zt) {
        this.zt = zt;
    }

    /**
     * @return CJSJ
     */
    public Date getCjsj() {
        return cjsj;
    }

    /**
     * @param cjsj
     */
    public void setCjsj(Date cjsj) {
        this.cjsj = cjsj;
    }

    /**
     * @return XGSJ
     */
    public Date getXgsj() {
        return xgsj;
    }

    /**
     * @param xgsj
     */
    public void setXgsj(Date xgsj) {
        this.xgsj = xgsj;
    }

    /**
     * @return AREA
     */
    public String getArea() {
        return area;
    }

    /**
     * @param area
     */
    public void setArea(String area) {
        this.area = area == null ? null : area.trim();
    }

    /**
     * @return LRDWDM
     */
    public String getLrdwdm() {
        return lrdwdm;
    }

    /**
     * @param lrdwdm
     */
    public void setLrdwdm(String lrdwdm) {
        this.lrdwdm = lrdwdm == null ? null : lrdwdm.trim();
    }

    /**
     * @return LRDWMC
     */
    public String getLrdwmc() {
        return lrdwmc;
    }

    /**
     * @param lrdwmc
     */
    public void setLrdwmc(String lrdwmc) {
        this.lrdwmc = lrdwmc == null ? null : lrdwmc.trim();
    }

    /**
     * @return LRRXM
     */
    public String getLrrxm() {
        return lrrxm;
    }

    /**
     * @param lrrxm
     */
    public void setLrrxm(String lrrxm) {
        this.lrrxm = lrrxm == null ? null : lrrxm.trim();
    }

    /**
     * @return LRRJH
     */
    public String getLrrjh() {
        return lrrjh;
    }

    public Short getAccuratetype() {
        return accuratetype;
    }

    public void setAccuratetype(Short accuratetype) {
        this.accuratetype = accuratetype;
    }

    /**
     * @param lrrjh
     */
    public void setLrrjh(String lrrjh) {
        this.lrrjh = lrrjh == null ? null : lrrjh.trim();
    }

    public Jjd() {
    }

    public Jjd(String pk, String jjdhm, String ajbh, String ajid, BigDecimal zfje, String jdxz, String sldw, String sldwmc, Date jjsj, String barxm, String barsfzh, String barlxdh, String jjfs, String ajlx, String ajlb, Date afsj, BigDecimal saje, String afdd, String jyaq, String sawz, String bz, Short zt, Date cjsj, Date xgsj, String area, String lrdwdm, String lrdwmc, String lrrxm, String lrrjh, Short accuratetype) {
        this.pk = pk;
        this.jjdhm = jjdhm;
        this.ajbh = ajbh;
        this.ajid = ajid;
        this.zfje = zfje;
        this.jdxz = jdxz;
        this.sldw = sldw;
        this.sldwmc = sldwmc;
        this.jjsj = jjsj;
        this.barxm = barxm;
        this.barsfzh = barsfzh;
        this.barlxdh = barlxdh;
        this.jjfs = jjfs;
        this.ajlx = ajlx;
        this.ajlb = ajlb;
        this.afsj = afsj;
        this.saje = saje;
        this.afdd = afdd;
        this.jyaq = jyaq;
        this.sawz = sawz;
        this.bz = bz;
        this.zt = zt;
        this.cjsj = cjsj;
        this.xgsj = xgsj;
        this.area = area;
        this.lrdwdm = lrdwdm;
        this.lrdwmc = lrdwmc;
        this.lrrxm = lrrxm;
        this.lrrjh = lrrjh;
        this.accuratetype = accuratetype;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", jjdhm=").append(jjdhm);
        sb.append(", ajbh=").append(ajbh);
        sb.append(", ajid=").append(ajid);
        sb.append(", zfje=").append(zfje);
        sb.append(", jdxz=").append(jdxz);
        sb.append(", sldw=").append(sldw);
        sb.append(", sldwmc=").append(sldwmc);
        sb.append(", jjsj=").append(jjsj);
        sb.append(", barxm=").append(barxm);
        sb.append(", barsfzh=").append(barsfzh);
        sb.append(", barlxdh=").append(barlxdh);
        sb.append(", jjfs=").append(jjfs);
        sb.append(", ajlx=").append(ajlx);
        sb.append(", ajlb=").append(ajlb);
        sb.append(", afsj=").append(afsj);
        sb.append(", saje=").append(saje);
        sb.append(", afdd=").append(afdd);
        sb.append(", jyaq=").append(jyaq);
        sb.append(", sawz=").append(sawz);
        sb.append(", bz=").append(bz);
        sb.append(", zt=").append(zt);
        sb.append(", cjsj=").append(cjsj);
        sb.append(", xgsj=").append(xgsj);
        sb.append(", area=").append(area);
        sb.append(", lrdwdm=").append(lrdwdm);
        sb.append(", lrdwmc=").append(lrdwmc);
        sb.append(", lrrxm=").append(lrrxm);
        sb.append(", lrrjh=").append(lrrjh);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Jjd jjd = (Jjd) o;
        return Objects.equals(pk, jjd.pk) &&
                Objects.equals(jjdhm, jjd.jjdhm) &&
                Objects.equals(zfje, jjd.zfje) &&
                Objects.equals(jdxz, jjd.jdxz) &&
                Objects.equals(sldw, jjd.sldw) &&
                Objects.equals(sldwmc, jjd.sldwmc) &&
                Objects.equals(jjsj, jjd.jjsj) &&
                Objects.equals(barxm, jjd.barxm) &&
                Objects.equals(barsfzh, jjd.barsfzh) &&
                Objects.equals(barlxdh, jjd.barlxdh) &&
                Objects.equals(jjfs, jjd.jjfs) &&
                Objects.equals(ajlx, jjd.ajlx) &&
                Objects.equals(ajlb, jjd.ajlb) &&
                Objects.equals(afsj, jjd.afsj) &&
                Objects.equals(saje, jjd.saje) &&
                Objects.equals(afdd, jjd.afdd) &&
                Objects.equals(jyaq, jjd.jyaq) &&
                Objects.equals(sawz, jjd.sawz) &&
                Objects.equals(bz, jjd.bz);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pk, jjdhm, zfje, jdxz, sldw, sldwmc, jjsj, barxm, barsfzh, barlxdh, jjfs, ajlx, ajlb, afsj, saje, afdd, jyaq, sawz, bz);
    }
}