package com.unis.model.tool;

import javax.persistence.*;

@Table(name = "T_SYS_PHONE_ADDR")
public class PhoneAddr {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "PREFIX")
    private String prefix;

    @Column(name = "PHONE")
    private String phone;

    @Column(name = "PMC")
    private String pmc;

    @Column(name = "CMC")
    private String cmc;

    @Column(name = "ISPMC")
    private String ispmc;

    @Column(name = "CODEDM")
    private String codedm;

    @Column(name = "ZIPDM")
    private String zipdm;

    @Column(name = "TYPESMC")
    private String typesmc;

    @Column(name = "SJDM")
    private String sjdm;

    @Column(name = "DM")
    private String dm;

    @Column(name = "YXX")
    private String yxx;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return PREFIX
     */
    public String getPrefix() {
        return prefix;
    }

    /**
     * @param prefix
     */
    public void setPrefix(String prefix) {
        this.prefix = prefix == null ? null : prefix.trim();
    }

    /**
     * @return PHONE
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone
     */
    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    /**
     * @return PMC
     */
    public String getPmc() {
        return pmc;
    }

    /**
     * @param pmc
     */
    public void setPmc(String pmc) {
        this.pmc = pmc == null ? null : pmc.trim();
    }

    /**
     * @return CMC
     */
    public String getCmc() {
        return cmc;
    }

    /**
     * @param cmc
     */
    public void setCmc(String cmc) {
        this.cmc = cmc == null ? null : cmc.trim();
    }

    /**
     * @return ISPMC
     */
    public String getIspmc() {
        return ispmc;
    }

    /**
     * @param ispmc
     */
    public void setIspmc(String ispmc) {
        this.ispmc = ispmc == null ? null : ispmc.trim();
    }

    /**
     * @return CODEDM
     */
    public String getCodedm() {
        return codedm;
    }

    /**
     * @param codedm
     */
    public void setCodedm(String codedm) {
        this.codedm = codedm == null ? null : codedm.trim();
    }

    /**
     * @return ZIPDM
     */
    public String getZipdm() {
        return zipdm;
    }

    /**
     * @param zipdm
     */
    public void setZipdm(String zipdm) {
        this.zipdm = zipdm == null ? null : zipdm.trim();
    }

    /**
     * @return TYPESMC
     */
    public String getTypesmc() {
        return typesmc;
    }

    /**
     * @param typesmc
     */
    public void setTypesmc(String typesmc) {
        this.typesmc = typesmc == null ? null : typesmc.trim();
    }

    /**
     * @return SJDM
     */
    public String getSjdm() {
        return sjdm;
    }

    /**
     * @param sjdm
     */
    public void setSjdm(String sjdm) {
        this.sjdm = sjdm == null ? null : sjdm.trim();
    }

    /**
     * @return DM
     */
    public String getDm() {
        return dm;
    }

    /**
     * @param dm
     */
    public void setDm(String dm) {
        this.dm = dm == null ? null : dm.trim();
    }

    /**
     * @return YXX
     */
    public String getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(String yxx) {
        this.yxx = yxx == null ? null : yxx.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", prefix=").append(prefix);
        sb.append(", phone=").append(phone);
        sb.append(", pmc=").append(pmc);
        sb.append(", cmc=").append(cmc);
        sb.append(", ispmc=").append(ispmc);
        sb.append(", codedm=").append(codedm);
        sb.append(", zipdm=").append(zipdm);
        sb.append(", typesmc=").append(typesmc);
        sb.append(", sjdm=").append(sjdm);
        sb.append(", dm=").append(dm);
        sb.append(", yxx=").append(yxx);
        sb.append("]");
        return sb.toString();
    }
}