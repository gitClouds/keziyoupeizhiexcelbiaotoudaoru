package com.unis.model.tool;

import javax.persistence.Column;
import javax.persistence.Id;
import java.util.Date;
/**
 *
 *
 * @author Qx
 * @version 1.0
 * @since 2020-05-21
 */
public class TbXxInfo  {

    private static final long serialVersionUID = 1L;

/****************************************
 * Basic fields
 ****************************************/
    /**
     * 注释: VARCHAR
     */
    @Id
    @Column(name = "XX_ID")
    private String xx_id;

    /**
     * 注释: VARCHAR
     */
    @Column(name = "XX_LX")
    private String xx_lx;

    /**
     * 注释: VARCHAR
     */
    @Column(name = "XXLY_ID")
    private String xxly_id;

    /**
     * 注释: DECIMAL
     */
    @Column(name = "YXX")
    private Integer yxx;

    /**
     * 注释: DATE
     */
    @Column(name = "XX_LRSJ")
    private Date xx_lrsj;

    /**
     * 注释: VARCHAR
     */
    @Column(name = "XX_NR")
    private String xx_nr;

    /**
     * 注释: DECIMAL
     */
    @Column(name = "XX_SL")
    private Integer xx_sl;

    /**
     * 注释: VARCHAR
     */
    @Column(name = "LRRJH")
    private String lrrjh;

    /**
     * 注释: VARCHAR
     */
    @Column(name = "LRRXM")
    private String lrrxm;

    /**
     * 注释: VARCHAR
     */
    @Column(name = "LRRDWDM")
    private String lrrdwdm;

    /**
     * 注释: VARCHAR
     */
    @Column(name = "LRRDWMC")
    private String lrrdwmc;

    public void setXx_id(String xx_id) {
        this.xx_id = xx_id;
    }

    public String getXx_id() {
        return xx_id;
    }

    public void setXx_lx(String xx_lx) {
        this.xx_lx = xx_lx;
    }

    public String getXx_lx() {
        return xx_lx;
    }

    public void setXxly_id(String xxly_id) {
        this.xxly_id = xxly_id;
    }

    public String getXxly_id() {
        return xxly_id;
    }

    public void setYxx(Integer yxx) {
        this.yxx = yxx;
    }

    public Integer getYxx() {
        return yxx;
    }

    public void setXx_lrsj(Date xx_lrsj) {
        this.xx_lrsj = xx_lrsj;
    }

    public Date getXx_lrsj() {
        return xx_lrsj;
    }

    public void setXx_nr(String xx_nr) {
        this.xx_nr = xx_nr;
    }

    public String getXx_nr() {
        return xx_nr;
    }

    public void setXx_sl(Integer xx_sl) {
        this.xx_sl = xx_sl;
    }

    public Integer getXx_sl() {
        return xx_sl;
    }

    public void setLrrjh(String lrrjh) {
        this.lrrjh = lrrjh;
    }

    public String getLrrjh() {
        return lrrjh;
    }

    public void setLrrxm(String lrrxm) {
        this.lrrxm = lrrxm;
    }

    public String getLrrxm() {
        return lrrxm;
    }

    public void setLrrdwdm(String lrrdwdm) {
        this.lrrdwdm = lrrdwdm;
    }

    public String getLrrdwdm() {
        return lrrdwdm;
    }

    public void setLrrdwmc(String lrrdwmc) {
        this.lrrdwmc = lrrdwmc;
    }

    public String getLrrdwmc() {
        return lrrdwmc;
    }

}
