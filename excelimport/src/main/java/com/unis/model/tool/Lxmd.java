package com.unis.model.tool;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_TOOL_LXMD")
public class Lxmd {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "JTYPE")
    private Short jtype;

    @Column(name = "JGH")
    private String jgh;

    @Column(name = "DQM")
    private String dqm;

    @Column(name = "DWMC")
    private String dwmc;

    @Column(name = "DZSX")
    private String dzsx;

    @Column(name = "LXR")
    private String lxr;

    @Column(name = "TELEPHONE")
    private String telephone;

    @Column(name = "PHONE")
    private String phone;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "XZ")
    private String xz;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private Short yxx;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return JTYPE
     */
    public Short getJtype() {
        return jtype;
    }

    /**
     * @param jtype
     */
    public void setJtype(Short jtype) {
        this.jtype = jtype;
    }

    /**
     * @return JGH
     */
    public String getJgh() {
        return jgh;
    }

    /**
     * @param jgh
     */
    public void setJgh(String jgh) {
        this.jgh = jgh == null ? null : jgh.trim();
    }

    /**
     * @return DQM
     */
    public String getDqm() {
        return dqm;
    }

    /**
     * @param dqm
     */
    public void setDqm(String dqm) {
        this.dqm = dqm == null ? null : dqm.trim();
    }

    /**
     * @return DWMC
     */
    public String getDwmc() {
        return dwmc;
    }

    /**
     * @param dwmc
     */
    public void setDwmc(String dwmc) {
        this.dwmc = dwmc == null ? null : dwmc.trim();
    }

    /**
     * @return DZSX
     */
    public String getDzsx() {
        return dzsx;
    }

    /**
     * @param dzsx
     */
    public void setDzsx(String dzsx) {
        this.dzsx = dzsx == null ? null : dzsx.trim();
    }

    /**
     * @return LXR
     */
    public String getLxr() {
        return lxr;
    }

    /**
     * @param lxr
     */
    public void setLxr(String lxr) {
        this.lxr = lxr == null ? null : lxr.trim();
    }

    /**
     * @return TELEPHONE
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * @param telephone
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone == null ? null : telephone.trim();
    }

    /**
     * @return PHONE
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone
     */
    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    /**
     * @return EMAIL
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     */
    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    /**
     * @return XZ
     */
    public String getXz() {
        return xz;
    }

    /**
     * @param xz
     */
    public void setXz(String xz) {
        this.xz = xz == null ? null : xz.trim();
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", jtype=").append(jtype);
        sb.append(", jgh=").append(jgh);
        sb.append(", dqm=").append(dqm);
        sb.append(", dwmc=").append(dwmc);
        sb.append(", dzsx=").append(dzsx);
        sb.append(", lxr=").append(lxr);
        sb.append(", telephone=").append(telephone);
        sb.append(", phone=").append(phone);
        sb.append(", email=").append(email);
        sb.append(", xz=").append(xz);
        sb.append(", rksj=").append(rksj);
        sb.append(", yxx=").append(yxx);
        sb.append("]");
        return sb.toString();
    }
}