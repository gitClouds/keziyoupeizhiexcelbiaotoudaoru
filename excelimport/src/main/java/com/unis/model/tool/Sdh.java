package com.unis.model.tool;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_TOOL_SDH")
public class Sdh {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "JTYPE")
    private Short jtype;

    @Column(name = "JGH")
    private String jgh;

    @Column(name = "DQM")
    private String dqm;

    @Column(name = "DWMC")
    private String dwmc;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private Short yxx;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return JTYPE
     */
    public Short getJtype() {
        return jtype;
    }

    /**
     * @param jtype
     */
    public void setJtype(Short jtype) {
        this.jtype = jtype;
    }

    /**
     * @return JGH
     */
    public String getJgh() {
        return jgh;
    }

    /**
     * @param jgh
     */
    public void setJgh(String jgh) {
        this.jgh = jgh == null ? null : jgh.trim();
    }

    /**
     * @return DQM
     */
    public String getDqm() {
        return dqm;
    }

    /**
     * @param dqm
     */
    public void setDqm(String dqm) {
        this.dqm = dqm == null ? null : dqm.trim();
    }

    /**
     * @return DWMC
     */
    public String getDwmc() {
        return dwmc;
    }

    /**
     * @param dwmc
     */
    public void setDwmc(String dwmc) {
        this.dwmc = dwmc == null ? null : dwmc.trim();
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", jtype=").append(jtype);
        sb.append(", jgh=").append(jgh);
        sb.append(", dqm=").append(dqm);
        sb.append(", dwmc=").append(dwmc);
        sb.append(", rksj=").append(rksj);
        sb.append(", yxx=").append(yxx);
        sb.append("]");
        return sb.toString();
    }
}