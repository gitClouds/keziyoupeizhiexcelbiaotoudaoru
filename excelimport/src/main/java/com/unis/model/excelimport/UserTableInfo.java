package com.unis.model.excelimport;

import java.util.Date;
import javax.persistence.*;
/**
* 
*
* @author fdzptwx
* @version 1.0
* @since 2020-05-08
*/

public class UserTableInfo  {

private static final long serialVersionUID = 1L;

/****************************************
* Basic fields
****************************************/
    /**
    * ID: VARCHAR
    */
    @Column(name = "ID")
    private String id;

    /**
    * 表名: VARCHAR
    */
    @Column(name = "TABLE_NAME")
    private String table_name;

    /**
    * 类型: VARCHAR
    */
    @Column(name = "TABLE_TYPE")
    private String table_type;

    /**
    * 中文名（注释）: VARCHAR
    */
    @Column(name = "COMMENTS")
    private String comments;
    /**
     * 中文名（注释）: VARCHAR
     */
    @Transient
    private String constraint_name;
    /**
     * 中文名（注释）: VARCHAR
     */
    @Transient
    private String column_name;
    public void setId(String id) {
    this.id = id;
    }

    public String getId() {
    return id;
    }

    public void setTable_name(String table_name) {
    this.table_name = table_name;
    }

    public String getTable_name() {
    return table_name;
    }

    public void setTable_type(String table_type) {
    this.table_type = table_type;
    }

    public String getTable_type() {
    return table_type;
    }

    public void setComments(String comments) {
    this.comments = comments;
    }

    public String getComments() {
    return comments;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getConstraint_name() {
        return constraint_name;
    }

    public void setConstraint_name(String constraint_name) {
        this.constraint_name = constraint_name;
    }

    public String getColumn_name() {
        return column_name;
    }

    public void setColumn_name(String column_name) {
        this.column_name = column_name;
    }
}
