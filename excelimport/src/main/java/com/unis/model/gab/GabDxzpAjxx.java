package com.unis.model.gab;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "XZ_DYFZ.TB_DXZP_AJXX")
public class GabDxzpAjxx {
    @Id
    @Column(name = "AJID")
    private String ajid;

    @Column(name = "AJBH")
    private String ajbh;

    @Column(name = "AJGB")
    private String ajgb;

    @Column(name = "AJLB")
    private String ajlb;

    @Column(name = "BFASJ")
    private Date bfasj;

    @Column(name = "AFASJ")
    private Date afasj;

    @Column(name = "FADQH")
    private String fadqh;

    @Column(name = "FADMC")
    private String fadmc;

    @Column(name = "FADXC")
    private String fadxc;

    @Column(name = "JYAQ")
    private String jyaq;

    @Column(name = "SAJZ")
    private BigDecimal sajz;

    @Column(name = "BAR")
    private String bar;

    @Column(name = "BARSFZHLX")
    private String barsfzhlx;

    @Column(name = "BARSFZH")
    private String barsfzh;

    @Column(name = "BARLXDH")
    private String barlxdh;

    @Column(name = "SHRDWMC")
    private String shrdwmc;

    @Column(name = "BASJ")
    private Date basj;

    @Column(name = "LADWDM")
    private String ladwdm;

    @Column(name = "LADWMC")
    private String ladwmc;

    @Column(name = "LASJ")
    private Date lasj;

    @Column(name = "LXR")
    private String lxr;

    @Column(name = "LXDH")
    private String lxdh;

    @Column(name = "PADWDM")
    private String padwdm;

    @Column(name = "PADWMC")
    private String padwmc;

    @Column(name = "PASJ")
    private Date pasj;

    @Column(name = "PALXR")
    private String palxr;

    @Column(name = "PALXRDH")
    private String palxrdh;

    @Column(name = "PAZT")
    private String pazt;

    @Column(name = "AJZT")
    private String ajzt;

    @Column(name = "LRDWDM")
    private String lrdwdm;

    @Column(name = "LRDWMC")
    private String lrdwmc;

    @Column(name = "LRR")
    private String lrr;

    @Column(name = "LRSJ")
    private Date lrsj;

    @Column(name = "XGDWDM")
    private String xgdwdm;

    @Column(name = "XGDWMC")
    private String xgdwmc;

    @Column(name = "XGR")
    private String xgr;

    @Column(name = "XGSJ")
    private Date xgsj;

    @Column(name = "XXLSFDA")
    private String xxlsfda;

    @Column(name = "XXLDADWDM")
    private String xxldadwdm;

    @Column(name = "XXLDADWMC")
    private String xxldadwmc;

    @Column(name = "XXLDASJ")
    private Date xxldasj;

    @Column(name = "XXLDAR")
    private String xxldar;

    @Column(name = "ZJLSFDA")
    private String zjlsfda;

    @Column(name = "ZJLDADWDM")
    private String zjldadwdm;

    @Column(name = "ZJLDADWMC")
    private String zjldadwmc;

    @Column(name = "ZJLDASJ")
    private Date zjldasj;

    @Column(name = "ZJLDAR")
    private String zjldar;

    @Column(name = "FASJHQSM")
    private String fasjhqsm;

    @Column(name = "SJBS")
    private String sjbs;

    @Column(name = "YJZT")
    private String yjzt;

    @Column(name = "YJDWDM")
    private String yjdwdm;

    @Column(name = "YJDWMC")
    private String yjdwmc;

    @Column(name = "YJCZDWDM")
    private String yjczdwdm;

    @Column(name = "YJCZDWMC")
    private String yjczdwmc;

    @Column(name = "YJSJ")
    private Date yjsj;

    @Column(name = "YJCZR")
    private String yjczr;

    @Column(name = "FBSJ")
    private Date fbsj;

    @Column(name = "FBR")
    private String fbr;

    @Column(name = "FBDWDM")
    private String fbdwdm;

    @Column(name = "FBDWMC")
    private String fbdwmc;

    @Column(name = "SPR")
    private String spr;

    @Column(name = "SPSJ")
    private Date spsj;

    @Column(name = "SPDWDM")
    private String spdwdm;

    @Column(name = "SPDWMC")
    private String spdwmc;

    @Column(name = "SPJG")
    private String spjg;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private String yxx;

    @Column(name = "SFYSCZJL")
    private String sfysczjl;

    @Column(name = "SFCSXLA")
    private String sfcsxla;

    @Column(name = "LXRSJ")
    private String lxrsj;

    @Column(name = "SPYJ")
    private String spyj;

    @Column(name = "ISFILTER")
    private String isfilter;

    @Column(name = "C_XH")
    private String cXh;

    @Column(name = "SFHMD")
    private String sfhmd;

    @Column(name = "SHR")
    private String shr;

    @Column(name = "SHSJ")
    private Date shsj;

    @Column(name = "SHDWDM")
    private String shdwdm;

    @Column(name = "SHDWMC")
    private String shdwmc;

    @Column(name = "SHZT")
    private String shzt;

    @Column(name = "SHYJ")
    private String shyj;

    @Column(name = "ALIBABABS")
    private String alibababs;

    @Column(name = "AJLBZL")
    private String ajlbzl;

    /**
     * @return AJID
     */
    public String getAjid() {
        return ajid;
    }

    /**
     * @param ajid
     */
    public void setAjid(String ajid) {
        this.ajid = ajid == null ? null : ajid.trim();
    }

    /**
     * @return AJBH
     */
    public String getAjbh() {
        return ajbh;
    }

    /**
     * @param ajbh
     */
    public void setAjbh(String ajbh) {
        this.ajbh = ajbh == null ? null : ajbh.trim();
    }

    /**
     * @return AJGB
     */
    public String getAjgb() {
        return ajgb;
    }

    /**
     * @param ajgb
     */
    public void setAjgb(String ajgb) {
        this.ajgb = ajgb == null ? null : ajgb.trim();
    }

    /**
     * @return AJLB
     */
    public String getAjlb() {
        return ajlb;
    }

    /**
     * @param ajlb
     */
    public void setAjlb(String ajlb) {
        this.ajlb = ajlb == null ? null : ajlb.trim();
    }

    /**
     * @return BFASJ
     */
    public Date getBfasj() {
        return bfasj;
    }

    /**
     * @param bfasj
     */
    public void setBfasj(Date bfasj) {
        this.bfasj = bfasj;
    }

    /**
     * @return AFASJ
     */
    public Date getAfasj() {
        return afasj;
    }

    /**
     * @param afasj
     */
    public void setAfasj(Date afasj) {
        this.afasj = afasj;
    }

    /**
     * @return FADQH
     */
    public String getFadqh() {
        return fadqh;
    }

    /**
     * @param fadqh
     */
    public void setFadqh(String fadqh) {
        this.fadqh = fadqh == null ? null : fadqh.trim();
    }

    /**
     * @return FADMC
     */
    public String getFadmc() {
        return fadmc;
    }

    /**
     * @param fadmc
     */
    public void setFadmc(String fadmc) {
        this.fadmc = fadmc == null ? null : fadmc.trim();
    }

    /**
     * @return FADXC
     */
    public String getFadxc() {
        return fadxc;
    }

    /**
     * @param fadxc
     */
    public void setFadxc(String fadxc) {
        this.fadxc = fadxc == null ? null : fadxc.trim();
    }

    /**
     * @return JYAQ
     */
    public String getJyaq() {
        return jyaq;
    }

    /**
     * @param jyaq
     */
    public void setJyaq(String jyaq) {
        this.jyaq = jyaq == null ? null : jyaq.trim();
    }

    /**
     * @return SAJZ
     */
    public BigDecimal getSajz() {
        return sajz;
    }

    /**
     * @param sajz
     */
    public void setSajz(BigDecimal sajz) {
        this.sajz = sajz;
    }

    /**
     * @return BAR
     */
    public String getBar() {
        return bar;
    }

    /**
     * @param bar
     */
    public void setBar(String bar) {
        this.bar = bar == null ? null : bar.trim();
    }

    /**
     * @return BARSFZHLX
     */
    public String getBarsfzhlx() {
        return barsfzhlx;
    }

    /**
     * @param barsfzhlx
     */
    public void setBarsfzhlx(String barsfzhlx) {
        this.barsfzhlx = barsfzhlx == null ? null : barsfzhlx.trim();
    }

    /**
     * @return BARSFZH
     */
    public String getBarsfzh() {
        return barsfzh;
    }

    /**
     * @param barsfzh
     */
    public void setBarsfzh(String barsfzh) {
        this.barsfzh = barsfzh == null ? null : barsfzh.trim();
    }

    /**
     * @return BARLXDH
     */
    public String getBarlxdh() {
        return barlxdh;
    }

    /**
     * @param barlxdh
     */
    public void setBarlxdh(String barlxdh) {
        this.barlxdh = barlxdh == null ? null : barlxdh.trim();
    }

    /**
     * @return SHRDWMC
     */
    public String getShrdwmc() {
        return shrdwmc;
    }

    /**
     * @param shrdwmc
     */
    public void setShrdwmc(String shrdwmc) {
        this.shrdwmc = shrdwmc == null ? null : shrdwmc.trim();
    }

    /**
     * @return BASJ
     */
    public Date getBasj() {
        return basj;
    }

    /**
     * @param basj
     */
    public void setBasj(Date basj) {
        this.basj = basj;
    }

    /**
     * @return LADWDM
     */
    public String getLadwdm() {
        return ladwdm;
    }

    /**
     * @param ladwdm
     */
    public void setLadwdm(String ladwdm) {
        this.ladwdm = ladwdm == null ? null : ladwdm.trim();
    }

    /**
     * @return LADWMC
     */
    public String getLadwmc() {
        return ladwmc;
    }

    /**
     * @param ladwmc
     */
    public void setLadwmc(String ladwmc) {
        this.ladwmc = ladwmc == null ? null : ladwmc.trim();
    }

    /**
     * @return LASJ
     */
    public Date getLasj() {
        return lasj;
    }

    /**
     * @param lasj
     */
    public void setLasj(Date lasj) {
        this.lasj = lasj;
    }

    /**
     * @return LXR
     */
    public String getLxr() {
        return lxr;
    }

    /**
     * @param lxr
     */
    public void setLxr(String lxr) {
        this.lxr = lxr == null ? null : lxr.trim();
    }

    /**
     * @return LXDH
     */
    public String getLxdh() {
        return lxdh;
    }

    /**
     * @param lxdh
     */
    public void setLxdh(String lxdh) {
        this.lxdh = lxdh == null ? null : lxdh.trim();
    }

    /**
     * @return PADWDM
     */
    public String getPadwdm() {
        return padwdm;
    }

    /**
     * @param padwdm
     */
    public void setPadwdm(String padwdm) {
        this.padwdm = padwdm == null ? null : padwdm.trim();
    }

    /**
     * @return PADWMC
     */
    public String getPadwmc() {
        return padwmc;
    }

    /**
     * @param padwmc
     */
    public void setPadwmc(String padwmc) {
        this.padwmc = padwmc == null ? null : padwmc.trim();
    }

    /**
     * @return PASJ
     */
    public Date getPasj() {
        return pasj;
    }

    /**
     * @param pasj
     */
    public void setPasj(Date pasj) {
        this.pasj = pasj;
    }

    /**
     * @return PALXR
     */
    public String getPalxr() {
        return palxr;
    }

    /**
     * @param palxr
     */
    public void setPalxr(String palxr) {
        this.palxr = palxr == null ? null : palxr.trim();
    }

    /**
     * @return PALXRDH
     */
    public String getPalxrdh() {
        return palxrdh;
    }

    /**
     * @param palxrdh
     */
    public void setPalxrdh(String palxrdh) {
        this.palxrdh = palxrdh == null ? null : palxrdh.trim();
    }

    /**
     * @return PAZT
     */
    public String getPazt() {
        return pazt;
    }

    /**
     * @param pazt
     */
    public void setPazt(String pazt) {
        this.pazt = pazt == null ? null : pazt.trim();
    }

    /**
     * @return AJZT
     */
    public String getAjzt() {
        return ajzt;
    }

    /**
     * @param ajzt
     */
    public void setAjzt(String ajzt) {
        this.ajzt = ajzt == null ? null : ajzt.trim();
    }

    /**
     * @return LRDWDM
     */
    public String getLrdwdm() {
        return lrdwdm;
    }

    /**
     * @param lrdwdm
     */
    public void setLrdwdm(String lrdwdm) {
        this.lrdwdm = lrdwdm == null ? null : lrdwdm.trim();
    }

    /**
     * @return LRDWMC
     */
    public String getLrdwmc() {
        return lrdwmc;
    }

    /**
     * @param lrdwmc
     */
    public void setLrdwmc(String lrdwmc) {
        this.lrdwmc = lrdwmc == null ? null : lrdwmc.trim();
    }

    /**
     * @return LRR
     */
    public String getLrr() {
        return lrr;
    }

    /**
     * @param lrr
     */
    public void setLrr(String lrr) {
        this.lrr = lrr == null ? null : lrr.trim();
    }

    /**
     * @return LRSJ
     */
    public Date getLrsj() {
        return lrsj;
    }

    /**
     * @param lrsj
     */
    public void setLrsj(Date lrsj) {
        this.lrsj = lrsj;
    }

    /**
     * @return XGDWDM
     */
    public String getXgdwdm() {
        return xgdwdm;
    }

    /**
     * @param xgdwdm
     */
    public void setXgdwdm(String xgdwdm) {
        this.xgdwdm = xgdwdm == null ? null : xgdwdm.trim();
    }

    /**
     * @return XGDWMC
     */
    public String getXgdwmc() {
        return xgdwmc;
    }

    /**
     * @param xgdwmc
     */
    public void setXgdwmc(String xgdwmc) {
        this.xgdwmc = xgdwmc == null ? null : xgdwmc.trim();
    }

    /**
     * @return XGR
     */
    public String getXgr() {
        return xgr;
    }

    /**
     * @param xgr
     */
    public void setXgr(String xgr) {
        this.xgr = xgr == null ? null : xgr.trim();
    }

    /**
     * @return XGSJ
     */
    public Date getXgsj() {
        return xgsj;
    }

    /**
     * @param xgsj
     */
    public void setXgsj(Date xgsj) {
        this.xgsj = xgsj;
    }

    /**
     * @return XXLSFDA
     */
    public String getXxlsfda() {
        return xxlsfda;
    }

    /**
     * @param xxlsfda
     */
    public void setXxlsfda(String xxlsfda) {
        this.xxlsfda = xxlsfda == null ? null : xxlsfda.trim();
    }

    /**
     * @return XXLDADWDM
     */
    public String getXxldadwdm() {
        return xxldadwdm;
    }

    /**
     * @param xxldadwdm
     */
    public void setXxldadwdm(String xxldadwdm) {
        this.xxldadwdm = xxldadwdm == null ? null : xxldadwdm.trim();
    }

    /**
     * @return XXLDADWMC
     */
    public String getXxldadwmc() {
        return xxldadwmc;
    }

    /**
     * @param xxldadwmc
     */
    public void setXxldadwmc(String xxldadwmc) {
        this.xxldadwmc = xxldadwmc == null ? null : xxldadwmc.trim();
    }

    /**
     * @return XXLDASJ
     */
    public Date getXxldasj() {
        return xxldasj;
    }

    /**
     * @param xxldasj
     */
    public void setXxldasj(Date xxldasj) {
        this.xxldasj = xxldasj;
    }

    /**
     * @return XXLDAR
     */
    public String getXxldar() {
        return xxldar;
    }

    /**
     * @param xxldar
     */
    public void setXxldar(String xxldar) {
        this.xxldar = xxldar == null ? null : xxldar.trim();
    }

    /**
     * @return ZJLSFDA
     */
    public String getZjlsfda() {
        return zjlsfda;
    }

    /**
     * @param zjlsfda
     */
    public void setZjlsfda(String zjlsfda) {
        this.zjlsfda = zjlsfda == null ? null : zjlsfda.trim();
    }

    /**
     * @return ZJLDADWDM
     */
    public String getZjldadwdm() {
        return zjldadwdm;
    }

    /**
     * @param zjldadwdm
     */
    public void setZjldadwdm(String zjldadwdm) {
        this.zjldadwdm = zjldadwdm == null ? null : zjldadwdm.trim();
    }

    /**
     * @return ZJLDADWMC
     */
    public String getZjldadwmc() {
        return zjldadwmc;
    }

    /**
     * @param zjldadwmc
     */
    public void setZjldadwmc(String zjldadwmc) {
        this.zjldadwmc = zjldadwmc == null ? null : zjldadwmc.trim();
    }

    /**
     * @return ZJLDASJ
     */
    public Date getZjldasj() {
        return zjldasj;
    }

    /**
     * @param zjldasj
     */
    public void setZjldasj(Date zjldasj) {
        this.zjldasj = zjldasj;
    }

    /**
     * @return ZJLDAR
     */
    public String getZjldar() {
        return zjldar;
    }

    /**
     * @param zjldar
     */
    public void setZjldar(String zjldar) {
        this.zjldar = zjldar == null ? null : zjldar.trim();
    }

    /**
     * @return FASJHQSM
     */
    public String getFasjhqsm() {
        return fasjhqsm;
    }

    /**
     * @param fasjhqsm
     */
    public void setFasjhqsm(String fasjhqsm) {
        this.fasjhqsm = fasjhqsm == null ? null : fasjhqsm.trim();
    }

    /**
     * @return SJBS
     */
    public String getSjbs() {
        return sjbs;
    }

    /**
     * @param sjbs
     */
    public void setSjbs(String sjbs) {
        this.sjbs = sjbs == null ? null : sjbs.trim();
    }

    /**
     * @return YJZT
     */
    public String getYjzt() {
        return yjzt;
    }

    /**
     * @param yjzt
     */
    public void setYjzt(String yjzt) {
        this.yjzt = yjzt == null ? null : yjzt.trim();
    }

    /**
     * @return YJDWDM
     */
    public String getYjdwdm() {
        return yjdwdm;
    }

    /**
     * @param yjdwdm
     */
    public void setYjdwdm(String yjdwdm) {
        this.yjdwdm = yjdwdm == null ? null : yjdwdm.trim();
    }

    /**
     * @return YJDWMC
     */
    public String getYjdwmc() {
        return yjdwmc;
    }

    /**
     * @param yjdwmc
     */
    public void setYjdwmc(String yjdwmc) {
        this.yjdwmc = yjdwmc == null ? null : yjdwmc.trim();
    }

    /**
     * @return YJCZDWDM
     */
    public String getYjczdwdm() {
        return yjczdwdm;
    }

    /**
     * @param yjczdwdm
     */
    public void setYjczdwdm(String yjczdwdm) {
        this.yjczdwdm = yjczdwdm == null ? null : yjczdwdm.trim();
    }

    /**
     * @return YJCZDWMC
     */
    public String getYjczdwmc() {
        return yjczdwmc;
    }

    /**
     * @param yjczdwmc
     */
    public void setYjczdwmc(String yjczdwmc) {
        this.yjczdwmc = yjczdwmc == null ? null : yjczdwmc.trim();
    }

    /**
     * @return YJSJ
     */
    public Date getYjsj() {
        return yjsj;
    }

    /**
     * @param yjsj
     */
    public void setYjsj(Date yjsj) {
        this.yjsj = yjsj;
    }

    /**
     * @return YJCZR
     */
    public String getYjczr() {
        return yjczr;
    }

    /**
     * @param yjczr
     */
    public void setYjczr(String yjczr) {
        this.yjczr = yjczr == null ? null : yjczr.trim();
    }

    /**
     * @return FBSJ
     */
    public Date getFbsj() {
        return fbsj;
    }

    /**
     * @param fbsj
     */
    public void setFbsj(Date fbsj) {
        this.fbsj = fbsj;
    }

    /**
     * @return FBR
     */
    public String getFbr() {
        return fbr;
    }

    /**
     * @param fbr
     */
    public void setFbr(String fbr) {
        this.fbr = fbr == null ? null : fbr.trim();
    }

    /**
     * @return FBDWDM
     */
    public String getFbdwdm() {
        return fbdwdm;
    }

    /**
     * @param fbdwdm
     */
    public void setFbdwdm(String fbdwdm) {
        this.fbdwdm = fbdwdm == null ? null : fbdwdm.trim();
    }

    /**
     * @return FBDWMC
     */
    public String getFbdwmc() {
        return fbdwmc;
    }

    /**
     * @param fbdwmc
     */
    public void setFbdwmc(String fbdwmc) {
        this.fbdwmc = fbdwmc == null ? null : fbdwmc.trim();
    }

    /**
     * @return SPR
     */
    public String getSpr() {
        return spr;
    }

    /**
     * @param spr
     */
    public void setSpr(String spr) {
        this.spr = spr == null ? null : spr.trim();
    }

    /**
     * @return SPSJ
     */
    public Date getSpsj() {
        return spsj;
    }

    /**
     * @param spsj
     */
    public void setSpsj(Date spsj) {
        this.spsj = spsj;
    }

    /**
     * @return SPDWDM
     */
    public String getSpdwdm() {
        return spdwdm;
    }

    /**
     * @param spdwdm
     */
    public void setSpdwdm(String spdwdm) {
        this.spdwdm = spdwdm == null ? null : spdwdm.trim();
    }

    /**
     * @return SPDWMC
     */
    public String getSpdwmc() {
        return spdwmc;
    }

    /**
     * @param spdwmc
     */
    public void setSpdwmc(String spdwmc) {
        this.spdwmc = spdwmc == null ? null : spdwmc.trim();
    }

    /**
     * @return SPJG
     */
    public String getSpjg() {
        return spjg;
    }

    /**
     * @param spjg
     */
    public void setSpjg(String spjg) {
        this.spjg = spjg == null ? null : spjg.trim();
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public String getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(String yxx) {
        this.yxx = yxx == null ? null : yxx.trim();
    }

    /**
     * @return SFYSCZJL
     */
    public String getSfysczjl() {
        return sfysczjl;
    }

    /**
     * @param sfysczjl
     */
    public void setSfysczjl(String sfysczjl) {
        this.sfysczjl = sfysczjl == null ? null : sfysczjl.trim();
    }

    /**
     * @return SFCSXLA
     */
    public String getSfcsxla() {
        return sfcsxla;
    }

    /**
     * @param sfcsxla
     */
    public void setSfcsxla(String sfcsxla) {
        this.sfcsxla = sfcsxla == null ? null : sfcsxla.trim();
    }

    /**
     * @return LXRSJ
     */
    public String getLxrsj() {
        return lxrsj;
    }

    /**
     * @param lxrsj
     */
    public void setLxrsj(String lxrsj) {
        this.lxrsj = lxrsj == null ? null : lxrsj.trim();
    }

    /**
     * @return SPYJ
     */
    public String getSpyj() {
        return spyj;
    }

    /**
     * @param spyj
     */
    public void setSpyj(String spyj) {
        this.spyj = spyj == null ? null : spyj.trim();
    }

    /**
     * @return ISFILTER
     */
    public String getIsfilter() {
        return isfilter;
    }

    /**
     * @param isfilter
     */
    public void setIsfilter(String isfilter) {
        this.isfilter = isfilter == null ? null : isfilter.trim();
    }

    /**
     * @return C_XH
     */
    public String getcXh() {
        return cXh;
    }

    /**
     * @param cXh
     */
    public void setcXh(String cXh) {
        this.cXh = cXh == null ? null : cXh.trim();
    }

    /**
     * @return SFHMD
     */
    public String getSfhmd() {
        return sfhmd;
    }

    /**
     * @param sfhmd
     */
    public void setSfhmd(String sfhmd) {
        this.sfhmd = sfhmd == null ? null : sfhmd.trim();
    }

    /**
     * @return SHR
     */
    public String getShr() {
        return shr;
    }

    /**
     * @param shr
     */
    public void setShr(String shr) {
        this.shr = shr == null ? null : shr.trim();
    }

    /**
     * @return SHSJ
     */
    public Date getShsj() {
        return shsj;
    }

    /**
     * @param shsj
     */
    public void setShsj(Date shsj) {
        this.shsj = shsj;
    }

    /**
     * @return SHDWDM
     */
    public String getShdwdm() {
        return shdwdm;
    }

    /**
     * @param shdwdm
     */
    public void setShdwdm(String shdwdm) {
        this.shdwdm = shdwdm == null ? null : shdwdm.trim();
    }

    /**
     * @return SHDWMC
     */
    public String getShdwmc() {
        return shdwmc;
    }

    /**
     * @param shdwmc
     */
    public void setShdwmc(String shdwmc) {
        this.shdwmc = shdwmc == null ? null : shdwmc.trim();
    }

    /**
     * @return SHZT
     */
    public String getShzt() {
        return shzt;
    }

    /**
     * @param shzt
     */
    public void setShzt(String shzt) {
        this.shzt = shzt == null ? null : shzt.trim();
    }

    /**
     * @return SHYJ
     */
    public String getShyj() {
        return shyj;
    }

    /**
     * @param shyj
     */
    public void setShyj(String shyj) {
        this.shyj = shyj == null ? null : shyj.trim();
    }

    /**
     * @return ALIBABABS
     */
    public String getAlibababs() {
        return alibababs;
    }

    /**
     * @param alibababs
     */
    public void setAlibababs(String alibababs) {
        this.alibababs = alibababs == null ? null : alibababs.trim();
    }

    /**
     * @return AJLBZL
     */
    public String getAjlbzl() {
        return ajlbzl;
    }

    /**
     * @param ajlbzl
     */
    public void setAjlbzl(String ajlbzl) {
        this.ajlbzl = ajlbzl == null ? null : ajlbzl.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", ajid=").append(ajid);
        sb.append(", ajbh=").append(ajbh);
        sb.append(", ajgb=").append(ajgb);
        sb.append(", ajlb=").append(ajlb);
        sb.append(", bfasj=").append(bfasj);
        sb.append(", afasj=").append(afasj);
        sb.append(", fadqh=").append(fadqh);
        sb.append(", fadmc=").append(fadmc);
        sb.append(", fadxc=").append(fadxc);
        sb.append(", jyaq=").append(jyaq);
        sb.append(", sajz=").append(sajz);
        sb.append(", bar=").append(bar);
        sb.append(", barsfzhlx=").append(barsfzhlx);
        sb.append(", barsfzh=").append(barsfzh);
        sb.append(", barlxdh=").append(barlxdh);
        sb.append(", shrdwmc=").append(shrdwmc);
        sb.append(", basj=").append(basj);
        sb.append(", ladwdm=").append(ladwdm);
        sb.append(", ladwmc=").append(ladwmc);
        sb.append(", lasj=").append(lasj);
        sb.append(", lxr=").append(lxr);
        sb.append(", lxdh=").append(lxdh);
        sb.append(", padwdm=").append(padwdm);
        sb.append(", padwmc=").append(padwmc);
        sb.append(", pasj=").append(pasj);
        sb.append(", palxr=").append(palxr);
        sb.append(", palxrdh=").append(palxrdh);
        sb.append(", pazt=").append(pazt);
        sb.append(", ajzt=").append(ajzt);
        sb.append(", lrdwdm=").append(lrdwdm);
        sb.append(", lrdwmc=").append(lrdwmc);
        sb.append(", lrr=").append(lrr);
        sb.append(", lrsj=").append(lrsj);
        sb.append(", xgdwdm=").append(xgdwdm);
        sb.append(", xgdwmc=").append(xgdwmc);
        sb.append(", xgr=").append(xgr);
        sb.append(", xgsj=").append(xgsj);
        sb.append(", xxlsfda=").append(xxlsfda);
        sb.append(", xxldadwdm=").append(xxldadwdm);
        sb.append(", xxldadwmc=").append(xxldadwmc);
        sb.append(", xxldasj=").append(xxldasj);
        sb.append(", xxldar=").append(xxldar);
        sb.append(", zjlsfda=").append(zjlsfda);
        sb.append(", zjldadwdm=").append(zjldadwdm);
        sb.append(", zjldadwmc=").append(zjldadwmc);
        sb.append(", zjldasj=").append(zjldasj);
        sb.append(", zjldar=").append(zjldar);
        sb.append(", fasjhqsm=").append(fasjhqsm);
        sb.append(", sjbs=").append(sjbs);
        sb.append(", yjzt=").append(yjzt);
        sb.append(", yjdwdm=").append(yjdwdm);
        sb.append(", yjdwmc=").append(yjdwmc);
        sb.append(", yjczdwdm=").append(yjczdwdm);
        sb.append(", yjczdwmc=").append(yjczdwmc);
        sb.append(", yjsj=").append(yjsj);
        sb.append(", yjczr=").append(yjczr);
        sb.append(", fbsj=").append(fbsj);
        sb.append(", fbr=").append(fbr);
        sb.append(", fbdwdm=").append(fbdwdm);
        sb.append(", fbdwmc=").append(fbdwmc);
        sb.append(", spr=").append(spr);
        sb.append(", spsj=").append(spsj);
        sb.append(", spdwdm=").append(spdwdm);
        sb.append(", spdwmc=").append(spdwmc);
        sb.append(", spjg=").append(spjg);
        sb.append(", rksj=").append(rksj);
        sb.append(", yxx=").append(yxx);
        sb.append(", sfysczjl=").append(sfysczjl);
        sb.append(", sfcsxla=").append(sfcsxla);
        sb.append(", lxrsj=").append(lxrsj);
        sb.append(", spyj=").append(spyj);
        sb.append(", isfilter=").append(isfilter);
        sb.append(", cXh=").append(cXh);
        sb.append(", sfhmd=").append(sfhmd);
        sb.append(", shr=").append(shr);
        sb.append(", shsj=").append(shsj);
        sb.append(", shdwdm=").append(shdwdm);
        sb.append(", shdwmc=").append(shdwmc);
        sb.append(", shzt=").append(shzt);
        sb.append(", shyj=").append(shyj);
        sb.append(", alibababs=").append(alibababs);
        sb.append(", ajlbzl=").append(ajlbzl);
        sb.append("]");
        return sb.toString();
    }
}