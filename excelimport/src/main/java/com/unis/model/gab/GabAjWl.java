package com.unis.model.gab;

import java.util.Date;
import javax.persistence.*;

@Table(name = "XZ_DYFZ.TB_DXZP_AJ_XXL_SAWL")
public class GabAjWl {
    @Id
    @Column(name = "XH")
    private String xh;

    @Column(name = "AJID")
    private String ajid;

    @Column(name = "WLDH")
    private String wldh;

    @Column(name = "SAWZ")
    private String sawz;

    @Column(name = "DLIP")
    private String dlip;

    @Column(name = "FWQDZ")
    private String fwqdz;

    @Column(name = "WDDZ")
    private String wddz;

    @Column(name = "KBSJH")
    private String kbsjh;

    @Column(name = "SJJSM")
    private String sjjsm;

    @Column(name = "LRR")
    private String lrr;

    @Column(name = "LRRSFZH")
    private String lrrsfzh;

    @Column(name = "LRRDWDM")
    private String lrrdwdm;

    @Column(name = "LRRDWMC")
    private String lrrdwmc;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private String yxx;

    @Column(name = "LDLRR")
    private String ldlrr;

    @Column(name = "LDLRRSFZH")
    private String ldlrrsfzh;

    @Column(name = "LDLRRDWDM")
    private String ldlrrdwdm;

    @Column(name = "LDLRRDWMC")
    private String ldlrrdwmc;

    @Column(name = "LDRKSJ")
    private Date ldrksj;

    @Column(name = "LDR")
    private String ldr;

    @Column(name = "LDSJ")
    private Date ldsj;

    @Column(name = "LDDZ")
    private String lddz;

    @Column(name = "LDDWMC")
    private String lddwmc;

    @Column(name = "LDDWDM")
    private String lddwdm;

    @Column(name = "C_XH")
    private String cXh;

    @Column(name = "SFHMD")
    private String sfhmd;

    /**
     * @return XH
     */
    public String getXh() {
        return xh;
    }

    /**
     * @param xh
     */
    public void setXh(String xh) {
        this.xh = xh == null ? null : xh.trim();
    }

    /**
     * @return AJID
     */
    public String getAjid() {
        return ajid;
    }

    /**
     * @param ajid
     */
    public void setAjid(String ajid) {
        this.ajid = ajid == null ? null : ajid.trim();
    }

    /**
     * @return WLDH
     */
    public String getWldh() {
        return wldh;
    }

    /**
     * @param wldh
     */
    public void setWldh(String wldh) {
        this.wldh = wldh == null ? null : wldh.trim();
    }

    /**
     * @return SAWZ
     */
    public String getSawz() {
        return sawz;
    }

    /**
     * @param sawz
     */
    public void setSawz(String sawz) {
        this.sawz = sawz == null ? null : sawz.trim();
    }

    /**
     * @return DLIP
     */
    public String getDlip() {
        return dlip;
    }

    /**
     * @param dlip
     */
    public void setDlip(String dlip) {
        this.dlip = dlip == null ? null : dlip.trim();
    }

    /**
     * @return FWQDZ
     */
    public String getFwqdz() {
        return fwqdz;
    }

    /**
     * @param fwqdz
     */
    public void setFwqdz(String fwqdz) {
        this.fwqdz = fwqdz == null ? null : fwqdz.trim();
    }

    /**
     * @return WDDZ
     */
    public String getWddz() {
        return wddz;
    }

    /**
     * @param wddz
     */
    public void setWddz(String wddz) {
        this.wddz = wddz == null ? null : wddz.trim();
    }

    /**
     * @return KBSJH
     */
    public String getKbsjh() {
        return kbsjh;
    }

    /**
     * @param kbsjh
     */
    public void setKbsjh(String kbsjh) {
        this.kbsjh = kbsjh == null ? null : kbsjh.trim();
    }

    /**
     * @return SJJSM
     */
    public String getSjjsm() {
        return sjjsm;
    }

    /**
     * @param sjjsm
     */
    public void setSjjsm(String sjjsm) {
        this.sjjsm = sjjsm == null ? null : sjjsm.trim();
    }

    /**
     * @return LRR
     */
    public String getLrr() {
        return lrr;
    }

    /**
     * @param lrr
     */
    public void setLrr(String lrr) {
        this.lrr = lrr == null ? null : lrr.trim();
    }

    /**
     * @return LRRSFZH
     */
    public String getLrrsfzh() {
        return lrrsfzh;
    }

    /**
     * @param lrrsfzh
     */
    public void setLrrsfzh(String lrrsfzh) {
        this.lrrsfzh = lrrsfzh == null ? null : lrrsfzh.trim();
    }

    /**
     * @return LRRDWDM
     */
    public String getLrrdwdm() {
        return lrrdwdm;
    }

    /**
     * @param lrrdwdm
     */
    public void setLrrdwdm(String lrrdwdm) {
        this.lrrdwdm = lrrdwdm == null ? null : lrrdwdm.trim();
    }

    /**
     * @return LRRDWMC
     */
    public String getLrrdwmc() {
        return lrrdwmc;
    }

    /**
     * @param lrrdwmc
     */
    public void setLrrdwmc(String lrrdwmc) {
        this.lrrdwmc = lrrdwmc == null ? null : lrrdwmc.trim();
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public String getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(String yxx) {
        this.yxx = yxx == null ? null : yxx.trim();
    }

    /**
     * @return LDLRR
     */
    public String getLdlrr() {
        return ldlrr;
    }

    /**
     * @param ldlrr
     */
    public void setLdlrr(String ldlrr) {
        this.ldlrr = ldlrr == null ? null : ldlrr.trim();
    }

    /**
     * @return LDLRRSFZH
     */
    public String getLdlrrsfzh() {
        return ldlrrsfzh;
    }

    /**
     * @param ldlrrsfzh
     */
    public void setLdlrrsfzh(String ldlrrsfzh) {
        this.ldlrrsfzh = ldlrrsfzh == null ? null : ldlrrsfzh.trim();
    }

    /**
     * @return LDLRRDWDM
     */
    public String getLdlrrdwdm() {
        return ldlrrdwdm;
    }

    /**
     * @param ldlrrdwdm
     */
    public void setLdlrrdwdm(String ldlrrdwdm) {
        this.ldlrrdwdm = ldlrrdwdm == null ? null : ldlrrdwdm.trim();
    }

    /**
     * @return LDLRRDWMC
     */
    public String getLdlrrdwmc() {
        return ldlrrdwmc;
    }

    /**
     * @param ldlrrdwmc
     */
    public void setLdlrrdwmc(String ldlrrdwmc) {
        this.ldlrrdwmc = ldlrrdwmc == null ? null : ldlrrdwmc.trim();
    }

    /**
     * @return LDRKSJ
     */
    public Date getLdrksj() {
        return ldrksj;
    }

    /**
     * @param ldrksj
     */
    public void setLdrksj(Date ldrksj) {
        this.ldrksj = ldrksj;
    }

    /**
     * @return LDR
     */
    public String getLdr() {
        return ldr;
    }

    /**
     * @param ldr
     */
    public void setLdr(String ldr) {
        this.ldr = ldr == null ? null : ldr.trim();
    }

    /**
     * @return LDSJ
     */
    public Date getLdsj() {
        return ldsj;
    }

    /**
     * @param ldsj
     */
    public void setLdsj(Date ldsj) {
        this.ldsj = ldsj;
    }

    /**
     * @return LDDZ
     */
    public String getLddz() {
        return lddz;
    }

    /**
     * @param lddz
     */
    public void setLddz(String lddz) {
        this.lddz = lddz == null ? null : lddz.trim();
    }

    /**
     * @return LDDWMC
     */
    public String getLddwmc() {
        return lddwmc;
    }

    /**
     * @param lddwmc
     */
    public void setLddwmc(String lddwmc) {
        this.lddwmc = lddwmc == null ? null : lddwmc.trim();
    }

    /**
     * @return LDDWDM
     */
    public String getLddwdm() {
        return lddwdm;
    }

    /**
     * @param lddwdm
     */
    public void setLddwdm(String lddwdm) {
        this.lddwdm = lddwdm == null ? null : lddwdm.trim();
    }

    /**
     * @return C_XH
     */
    public String getcXh() {
        return cXh;
    }

    /**
     * @param cXh
     */
    public void setcXh(String cXh) {
        this.cXh = cXh == null ? null : cXh.trim();
    }

    /**
     * @return SFHMD
     */
    public String getSfhmd() {
        return sfhmd;
    }

    /**
     * @param sfhmd
     */
    public void setSfhmd(String sfhmd) {
        this.sfhmd = sfhmd == null ? null : sfhmd.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", xh=").append(xh);
        sb.append(", ajid=").append(ajid);
        sb.append(", wldh=").append(wldh);
        sb.append(", sawz=").append(sawz);
        sb.append(", dlip=").append(dlip);
        sb.append(", fwqdz=").append(fwqdz);
        sb.append(", wddz=").append(wddz);
        sb.append(", kbsjh=").append(kbsjh);
        sb.append(", sjjsm=").append(sjjsm);
        sb.append(", lrr=").append(lrr);
        sb.append(", lrrsfzh=").append(lrrsfzh);
        sb.append(", lrrdwdm=").append(lrrdwdm);
        sb.append(", lrrdwmc=").append(lrrdwmc);
        sb.append(", rksj=").append(rksj);
        sb.append(", yxx=").append(yxx);
        sb.append(", ldlrr=").append(ldlrr);
        sb.append(", ldlrrsfzh=").append(ldlrrsfzh);
        sb.append(", ldlrrdwdm=").append(ldlrrdwdm);
        sb.append(", ldlrrdwmc=").append(ldlrrdwmc);
        sb.append(", ldrksj=").append(ldrksj);
        sb.append(", ldr=").append(ldr);
        sb.append(", ldsj=").append(ldsj);
        sb.append(", lddz=").append(lddz);
        sb.append(", lddwmc=").append(lddwmc);
        sb.append(", lddwdm=").append(lddwdm);
        sb.append(", cXh=").append(cXh);
        sb.append(", sfhmd=").append(sfhmd);
        sb.append("]");
        return sb.toString();
    }
}