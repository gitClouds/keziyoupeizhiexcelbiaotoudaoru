package com.unis.model.gab;

import java.util.Date;
import javax.persistence.*;

@Table(name = "XZ_DYFZ.TB_DXZP_AJ_XXL_QQ")
public class GabAjQq {
    @Id
    @Column(name = "XH")
    private String xh;

    @Column(name = "AJID")
    private String ajid;

    @Column(name = "PZQQ")
    private String pzqq;

    @Column(name = "QQIP")
    private String qqip;

    @Column(name = "QQDLSJ")
    private String qqdlsj;

    @Column(name = "LRR")
    private String lrr;

    @Column(name = "LRRSFZH")
    private String lrrsfzh;

    @Column(name = "LRRDWDM")
    private String lrrdwdm;

    @Column(name = "LRRDWMC")
    private String lrrdwmc;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private String yxx;

    @Column(name = "SFYLD")
    private String sfyld;

    @Column(name = "SJGJLX")
    private String sjgjlx;

    @Column(name = "SJGJMC")
    private String sjgjmc;

    @Column(name = "LDR")
    private String ldr;

    @Column(name = "LDSJ")
    private Date ldsj;

    @Column(name = "LDDZ")
    private String lddz;

    @Column(name = "LDDWMC")
    private String lddwmc;

    @Column(name = "LDDWDM")
    private String lddwdm;

    @Column(name = "C_XH")
    private String cXh;

    @Column(name = "SFHMD")
    private String sfhmd;

    /**
     * @return XH
     */
    public String getXh() {
        return xh;
    }

    /**
     * @param xh
     */
    public void setXh(String xh) {
        this.xh = xh == null ? null : xh.trim();
    }

    /**
     * @return AJID
     */
    public String getAjid() {
        return ajid;
    }

    /**
     * @param ajid
     */
    public void setAjid(String ajid) {
        this.ajid = ajid == null ? null : ajid.trim();
    }

    /**
     * @return PZQQ
     */
    public String getPzqq() {
        return pzqq;
    }

    /**
     * @param pzqq
     */
    public void setPzqq(String pzqq) {
        this.pzqq = pzqq == null ? null : pzqq.trim();
    }

    /**
     * @return QQIP
     */
    public String getQqip() {
        return qqip;
    }

    /**
     * @param qqip
     */
    public void setQqip(String qqip) {
        this.qqip = qqip == null ? null : qqip.trim();
    }

    /**
     * @return QQDLSJ
     */
    public String getQqdlsj() {
        return qqdlsj;
    }

    /**
     * @param qqdlsj
     */
    public void setQqdlsj(String qqdlsj) {
        this.qqdlsj = qqdlsj == null ? null : qqdlsj.trim();
    }

    /**
     * @return LRR
     */
    public String getLrr() {
        return lrr;
    }

    /**
     * @param lrr
     */
    public void setLrr(String lrr) {
        this.lrr = lrr == null ? null : lrr.trim();
    }

    /**
     * @return LRRSFZH
     */
    public String getLrrsfzh() {
        return lrrsfzh;
    }

    /**
     * @param lrrsfzh
     */
    public void setLrrsfzh(String lrrsfzh) {
        this.lrrsfzh = lrrsfzh == null ? null : lrrsfzh.trim();
    }

    /**
     * @return LRRDWDM
     */
    public String getLrrdwdm() {
        return lrrdwdm;
    }

    /**
     * @param lrrdwdm
     */
    public void setLrrdwdm(String lrrdwdm) {
        this.lrrdwdm = lrrdwdm == null ? null : lrrdwdm.trim();
    }

    /**
     * @return LRRDWMC
     */
    public String getLrrdwmc() {
        return lrrdwmc;
    }

    /**
     * @param lrrdwmc
     */
    public void setLrrdwmc(String lrrdwmc) {
        this.lrrdwmc = lrrdwmc == null ? null : lrrdwmc.trim();
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public String getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(String yxx) {
        this.yxx = yxx == null ? null : yxx.trim();
    }

    /**
     * @return SFYLD
     */
    public String getSfyld() {
        return sfyld;
    }

    /**
     * @param sfyld
     */
    public void setSfyld(String sfyld) {
        this.sfyld = sfyld == null ? null : sfyld.trim();
    }

    /**
     * @return SJGJLX
     */
    public String getSjgjlx() {
        return sjgjlx;
    }

    /**
     * @param sjgjlx
     */
    public void setSjgjlx(String sjgjlx) {
        this.sjgjlx = sjgjlx == null ? null : sjgjlx.trim();
    }

    /**
     * @return SJGJMC
     */
    public String getSjgjmc() {
        return sjgjmc;
    }

    /**
     * @param sjgjmc
     */
    public void setSjgjmc(String sjgjmc) {
        this.sjgjmc = sjgjmc == null ? null : sjgjmc.trim();
    }

    /**
     * @return LDR
     */
    public String getLdr() {
        return ldr;
    }

    /**
     * @param ldr
     */
    public void setLdr(String ldr) {
        this.ldr = ldr == null ? null : ldr.trim();
    }

    /**
     * @return LDSJ
     */
    public Date getLdsj() {
        return ldsj;
    }

    /**
     * @param ldsj
     */
    public void setLdsj(Date ldsj) {
        this.ldsj = ldsj;
    }

    /**
     * @return LDDZ
     */
    public String getLddz() {
        return lddz;
    }

    /**
     * @param lddz
     */
    public void setLddz(String lddz) {
        this.lddz = lddz == null ? null : lddz.trim();
    }

    /**
     * @return LDDWMC
     */
    public String getLddwmc() {
        return lddwmc;
    }

    /**
     * @param lddwmc
     */
    public void setLddwmc(String lddwmc) {
        this.lddwmc = lddwmc == null ? null : lddwmc.trim();
    }

    /**
     * @return LDDWDM
     */
    public String getLddwdm() {
        return lddwdm;
    }

    /**
     * @param lddwdm
     */
    public void setLddwdm(String lddwdm) {
        this.lddwdm = lddwdm == null ? null : lddwdm.trim();
    }

    /**
     * @return C_XH
     */
    public String getcXh() {
        return cXh;
    }

    /**
     * @param cXh
     */
    public void setcXh(String cXh) {
        this.cXh = cXh == null ? null : cXh.trim();
    }

    /**
     * @return SFHMD
     */
    public String getSfhmd() {
        return sfhmd;
    }

    /**
     * @param sfhmd
     */
    public void setSfhmd(String sfhmd) {
        this.sfhmd = sfhmd == null ? null : sfhmd.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", xh=").append(xh);
        sb.append(", ajid=").append(ajid);
        sb.append(", pzqq=").append(pzqq);
        sb.append(", qqip=").append(qqip);
        sb.append(", qqdlsj=").append(qqdlsj);
        sb.append(", lrr=").append(lrr);
        sb.append(", lrrsfzh=").append(lrrsfzh);
        sb.append(", lrrdwdm=").append(lrrdwdm);
        sb.append(", lrrdwmc=").append(lrrdwmc);
        sb.append(", rksj=").append(rksj);
        sb.append(", yxx=").append(yxx);
        sb.append(", sfyld=").append(sfyld);
        sb.append(", sjgjlx=").append(sjgjlx);
        sb.append(", sjgjmc=").append(sjgjmc);
        sb.append(", ldr=").append(ldr);
        sb.append(", ldsj=").append(ldsj);
        sb.append(", lddz=").append(lddz);
        sb.append(", lddwmc=").append(lddwmc);
        sb.append(", lddwdm=").append(lddwdm);
        sb.append(", cXh=").append(cXh);
        sb.append(", sfhmd=").append(sfhmd);
        sb.append("]");
        return sb.toString();
    }
}