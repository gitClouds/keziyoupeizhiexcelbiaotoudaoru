package com.unis.model.gab;

import java.util.Date;
import javax.persistence.*;

@Table(name = "XZ_DYFZ.TB_DXZP_AJ_SAYHK")
public class GabAjSayhk {
    @Id
    @Column(name = "XH")
    private String xh;

    @Column(name = "AJID")
    private String ajid;

    @Column(name = "ZZFS")
    private String zzfs;

    @Column(name = "SHRZHLX")
    private String shrzhlx;

    @Column(name = "SHRZH")
    private String shrzh;

    @Column(name = "XYRZHLX")
    private String xyrzhlx;

    @Column(name = "XYRZH")
    private String xyrzh;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private String yxx;

    @Column(name = "ZZSJ")
    private Date zzsj;

    @Column(name = "SFYSCJLT")
    private String sfyscjlt;

    @Column(name = "SFYJY")
    private String sfyjy;

    @Column(name = "LDR")
    private String ldr;

    @Column(name = "LDSJ")
    private Date ldsj;

    @Column(name = "LDDZ")
    private String lddz;

    @Column(name = "LDDWMC")
    private String lddwmc;

    @Column(name = "LDDWDM")
    private String lddwdm;

    @Column(name = "QKLD")
    private String qkld;

    @Column(name = "ZZLD")
    private String zzld;

    @Column(name = "POSLD")
    private String posld;

    @Column(name = "LDZT")
    private String ldzt;

    @Column(name = "C_XH")
    private String cXh;

    @Column(name = "FKDD")
    private String fkdd;

    /**
     * @return XH
     */
    public String getXh() {
        return xh;
    }

    /**
     * @param xh
     */
    public void setXh(String xh) {
        this.xh = xh == null ? null : xh.trim();
    }

    /**
     * @return AJID
     */
    public String getAjid() {
        return ajid;
    }

    /**
     * @param ajid
     */
    public void setAjid(String ajid) {
        this.ajid = ajid == null ? null : ajid.trim();
    }

    /**
     * @return ZZFS
     */
    public String getZzfs() {
        return zzfs;
    }

    /**
     * @param zzfs
     */
    public void setZzfs(String zzfs) {
        this.zzfs = zzfs == null ? null : zzfs.trim();
    }

    /**
     * @return SHRZHLX
     */
    public String getShrzhlx() {
        return shrzhlx;
    }

    /**
     * @param shrzhlx
     */
    public void setShrzhlx(String shrzhlx) {
        this.shrzhlx = shrzhlx == null ? null : shrzhlx.trim();
    }

    /**
     * @return SHRZH
     */
    public String getShrzh() {
        return shrzh;
    }

    /**
     * @param shrzh
     */
    public void setShrzh(String shrzh) {
        this.shrzh = shrzh == null ? null : shrzh.trim();
    }

    /**
     * @return XYRZHLX
     */
    public String getXyrzhlx() {
        return xyrzhlx;
    }

    /**
     * @param xyrzhlx
     */
    public void setXyrzhlx(String xyrzhlx) {
        this.xyrzhlx = xyrzhlx == null ? null : xyrzhlx.trim();
    }

    /**
     * @return XYRZH
     */
    public String getXyrzh() {
        return xyrzh;
    }

    /**
     * @param xyrzh
     */
    public void setXyrzh(String xyrzh) {
        this.xyrzh = xyrzh == null ? null : xyrzh.trim();
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public String getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(String yxx) {
        this.yxx = yxx == null ? null : yxx.trim();
    }

    /**
     * @return ZZSJ
     */
    public Date getZzsj() {
        return zzsj;
    }

    /**
     * @param zzsj
     */
    public void setZzsj(Date zzsj) {
        this.zzsj = zzsj;
    }

    /**
     * @return SFYSCJLT
     */
    public String getSfyscjlt() {
        return sfyscjlt;
    }

    /**
     * @param sfyscjlt
     */
    public void setSfyscjlt(String sfyscjlt) {
        this.sfyscjlt = sfyscjlt == null ? null : sfyscjlt.trim();
    }

    /**
     * @return SFYJY
     */
    public String getSfyjy() {
        return sfyjy;
    }

    /**
     * @param sfyjy
     */
    public void setSfyjy(String sfyjy) {
        this.sfyjy = sfyjy == null ? null : sfyjy.trim();
    }

    /**
     * @return LDR
     */
    public String getLdr() {
        return ldr;
    }

    /**
     * @param ldr
     */
    public void setLdr(String ldr) {
        this.ldr = ldr == null ? null : ldr.trim();
    }

    /**
     * @return LDSJ
     */
    public Date getLdsj() {
        return ldsj;
    }

    /**
     * @param ldsj
     */
    public void setLdsj(Date ldsj) {
        this.ldsj = ldsj;
    }

    /**
     * @return LDDZ
     */
    public String getLddz() {
        return lddz;
    }

    /**
     * @param lddz
     */
    public void setLddz(String lddz) {
        this.lddz = lddz == null ? null : lddz.trim();
    }

    /**
     * @return LDDWMC
     */
    public String getLddwmc() {
        return lddwmc;
    }

    /**
     * @param lddwmc
     */
    public void setLddwmc(String lddwmc) {
        this.lddwmc = lddwmc == null ? null : lddwmc.trim();
    }

    /**
     * @return LDDWDM
     */
    public String getLddwdm() {
        return lddwdm;
    }

    /**
     * @param lddwdm
     */
    public void setLddwdm(String lddwdm) {
        this.lddwdm = lddwdm == null ? null : lddwdm.trim();
    }

    /**
     * @return QKLD
     */
    public String getQkld() {
        return qkld;
    }

    /**
     * @param qkld
     */
    public void setQkld(String qkld) {
        this.qkld = qkld == null ? null : qkld.trim();
    }

    /**
     * @return ZZLD
     */
    public String getZzld() {
        return zzld;
    }

    /**
     * @param zzld
     */
    public void setZzld(String zzld) {
        this.zzld = zzld == null ? null : zzld.trim();
    }

    /**
     * @return POSLD
     */
    public String getPosld() {
        return posld;
    }

    /**
     * @param posld
     */
    public void setPosld(String posld) {
        this.posld = posld == null ? null : posld.trim();
    }

    /**
     * @return LDZT
     */
    public String getLdzt() {
        return ldzt;
    }

    /**
     * @param ldzt
     */
    public void setLdzt(String ldzt) {
        this.ldzt = ldzt == null ? null : ldzt.trim();
    }

    /**
     * @return C_XH
     */
    public String getcXh() {
        return cXh;
    }

    /**
     * @param cXh
     */
    public void setcXh(String cXh) {
        this.cXh = cXh == null ? null : cXh.trim();
    }

    /**
     * @return FKDD
     */
    public String getFkdd() {
        return fkdd;
    }

    /**
     * @param fkdd
     */
    public void setFkdd(String fkdd) {
        this.fkdd = fkdd == null ? null : fkdd.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", xh=").append(xh);
        sb.append(", ajid=").append(ajid);
        sb.append(", zzfs=").append(zzfs);
        sb.append(", shrzhlx=").append(shrzhlx);
        sb.append(", shrzh=").append(shrzh);
        sb.append(", xyrzhlx=").append(xyrzhlx);
        sb.append(", xyrzh=").append(xyrzh);
        sb.append(", rksj=").append(rksj);
        sb.append(", yxx=").append(yxx);
        sb.append(", zzsj=").append(zzsj);
        sb.append(", sfyscjlt=").append(sfyscjlt);
        sb.append(", sfyjy=").append(sfyjy);
        sb.append(", ldr=").append(ldr);
        sb.append(", ldsj=").append(ldsj);
        sb.append(", lddz=").append(lddz);
        sb.append(", lddwmc=").append(lddwmc);
        sb.append(", lddwdm=").append(lddwdm);
        sb.append(", qkld=").append(qkld);
        sb.append(", zzld=").append(zzld);
        sb.append(", posld=").append(posld);
        sb.append(", ldzt=").append(ldzt);
        sb.append(", cXh=").append(cXh);
        sb.append(", fkdd=").append(fkdd);
        sb.append("]");
        return sb.toString();
    }
}