package com.unis.model.gab;

import com.unis.common.util.TimeUtil;
import com.unis.model.system.Jgz;
import com.unis.model.znzf.YhkMxQq;
import com.unis.model.znzf.YhkZfQq;
import com.unis.model.znzf.YhkZtQq;
import org.apache.commons.lang.StringUtils;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "DYZY_YHK_QQ")
public class GabDyzyYhkQq {
    @Id
    @Column(name = "ID")
    private String id;

    @Column(name = "JYBM")
    private String jybm;

    @Column(name = "BWLSH")
    private String bwlsh;

    @Column(name = "YHID")
    private String yhid;

    @Column(name = "YWB")
    private String ywb;

    @Column(name = "AJID")
    private String ajid;

    @Column(name = "AJBH")
    private String ajbh;

    @Column(name = "YWSQBH")
    private String ywsqbh;

    @Column(name = "AJLX")
    private String ajlx;

    @Column(name = "JJCD")
    private String jjcd;

    @Column(name = "RMBZL")
    private String rmbzl;

    @Column(name = "ZHJGDM")
    private String zhjgdm;

    @Column(name = "ZHYHMC")
    private String zhyhmc;

    @Column(name = "ZHLB")
    private String zhlb;

    @Column(name = "ZHXM")
    private String zhxm;

    @Column(name = "ZH")
    private String zh;

    @Column(name = "SY")
    private String sy;

    @Column(name = "SM")
    private String sm;

    @Column(name = "DQR")
    private String dqr;

    @Column(name = "JGFKXDSJ")
    private String jgfkxdsj;

    @Column(name = "SQSJ")
    private Object sqsj;

    @Column(name = "SQJGDM")
    private String sqjgdm;

    @Column(name = "SQJGMC")
    private String sqjgmc;

    @Column(name = "JBRZJZL")
    private String jbrzjzl;

    @Column(name = "JBRZJHM")
    private String jbrzjhm;

    @Column(name = "JBRXM")
    private String jbrxm;

    @Column(name = "JBRDH")
    private String jbrdh;

    @Column(name = "XCRZJZL")
    private String xcrzjzl;

    @Column(name = "XCRZJHM")
    private String xcrzjhm;

    @Column(name = "XCRXM")
    private String xcrxm;

    @Column(name = "ZCJE")
    private String zcje;

    @Column(name = "ZCSJ")
    private String zcsj;

    @Column(name = "ZCZHJGDM")
    private String zczhjgdm;

    @Column(name = "ZCZHYHMC")
    private String zczhyhmc;

    @Column(name = "ZCZHXM")
    private String zczhxm;

    @Column(name = "ZCZH")
    private String zczh;

    @Column(name = "DJFS")
    private String djfs;

    @Column(name = "DJZHZJLX")
    private String djzhzjlx;

    @Column(name = "DJZHZJHM")
    private String djzhzjhm;

    @Column(name = "CXZTLB")
    private String cxztlb;

    @Column(name = "CXNR")
    private String cxnr;

    @Column(name = "JYLSKSSJ")
    private String jylskssj;

    @Column(name = "JYLSJSSJ")
    private String jylsjssj;

    @Column(name = "YSQBM")
    private String ysqbm;

    @Column(name = "FHM")
    private String fhm;

    @Column(name = "FHXX")
    private String fhxx;

    @Column(name = "LRRDWDM")
    private String lrrdwdm;

    @Column(name = "LRRSFZH")
    private String lrrsfzh;

    @Column(name = "LRRXM")
    private String lrrxm;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private String yxx;

    @Column(name = "QQLX")
    private String qqlx;

    @Column(name = "YWXH")
    private String ywxh;

    @Column(name = "FSSJ")
    private String fssj;

    @Column(name = "KSSJ")
    private String kssj;

    @Column(name = "JZSJ")
    private String jzsj;

    @Column(name = "ZT")
    private String zt;

    @Column(name = "SBYY")
    private String sbyy;

    @Column(name = "YE")
    private String ye;

    @Column(name = "XCRDH")
    private String xcrdh;

    @Column(name = "YJZF")
    private String yjzf;

    @Column(name = "JGKSSJ")
    private String jgkssj;

    @Column(name = "JGJSSJ")
    private String jgjssj;

    @Column(name = "XFZT")
    private String xfzt;

    @Column(name = "IP")
    private String ip;

    @Column(name = "FSZT")
    private String fszt;

    @Column(name = "CFFSZT")
    private String cffszt;

    @Column(name = "MXFSZT")
    private String mxfszt;

    @Column(name = "DQJZSJ")
    private String dqjzsj;

    @Column(name = "ZZHBFSZT")
    private String zzhbfszt;

    @Column(name = "CSBFSZT")
    private String csbfszt;

    @Column(name = "FSSBCS")
    private String fssbcs;

    @Column(name = "ALBS")
    private String albs;

    @Column(name = "SFSCBW")
    private String sfscbw;

    /**
     * @return ID
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * @return JYBM
     */
    public String getJybm() {
        return jybm;
    }

    /**
     * @param jybm
     */
    public void setJybm(String jybm) {
        this.jybm = jybm == null ? null : jybm.trim();
    }

    /**
     * @return BWLSH
     */
    public String getBwlsh() {
        return bwlsh;
    }

    /**
     * @param bwlsh
     */
    public void setBwlsh(String bwlsh) {
        this.bwlsh = bwlsh == null ? null : bwlsh.trim();
    }

    /**
     * @return YHID
     */
    public String getYhid() {
        return yhid;
    }

    /**
     * @param yhid
     */
    public void setYhid(String yhid) {
        this.yhid = yhid == null ? null : yhid.trim();
    }

    /**
     * @return YWB
     */
    public String getYwb() {
        return ywb;
    }

    /**
     * @param ywb
     */
    public void setYwb(String ywb) {
        this.ywb = ywb == null ? null : ywb.trim();
    }

    /**
     * @return AJID
     */
    public String getAjid() {
        return ajid;
    }

    /**
     * @param ajid
     */
    public void setAjid(String ajid) {
        this.ajid = ajid == null ? null : ajid.trim();
    }

    /**
     * @return AJBH
     */
    public String getAjbh() {
        return ajbh;
    }

    /**
     * @param ajbh
     */
    public void setAjbh(String ajbh) {
        this.ajbh = ajbh == null ? null : ajbh.trim();
    }

    /**
     * @return YWSQBH
     */
    public String getYwsqbh() {
        return ywsqbh;
    }

    /**
     * @param ywsqbh
     */
    public void setYwsqbh(String ywsqbh) {
        this.ywsqbh = ywsqbh == null ? null : ywsqbh.trim();
    }

    /**
     * @return AJLX
     */
    public String getAjlx() {
        return ajlx;
    }

    /**
     * @param ajlx
     */
    public void setAjlx(String ajlx) {
        this.ajlx = ajlx == null ? null : ajlx.trim();
    }

    /**
     * @return JJCD
     */
    public String getJjcd() {
        return jjcd;
    }

    /**
     * @param jjcd
     */
    public void setJjcd(String jjcd) {
        this.jjcd = jjcd == null ? null : jjcd.trim();
    }

    /**
     * @return RMBZL
     */
    public String getRmbzl() {
        return rmbzl;
    }

    /**
     * @param rmbzl
     */
    public void setRmbzl(String rmbzl) {
        this.rmbzl = rmbzl == null ? null : rmbzl.trim();
    }

    /**
     * @return ZHJGDM
     */
    public String getZhjgdm() {
        return zhjgdm;
    }

    /**
     * @param zhjgdm
     */
    public void setZhjgdm(String zhjgdm) {
        this.zhjgdm = zhjgdm == null ? null : zhjgdm.trim();
    }

    /**
     * @return ZHYHMC
     */
    public String getZhyhmc() {
        return zhyhmc;
    }

    /**
     * @param zhyhmc
     */
    public void setZhyhmc(String zhyhmc) {
        this.zhyhmc = zhyhmc == null ? null : zhyhmc.trim();
    }

    /**
     * @return ZHLB
     */
    public String getZhlb() {
        return zhlb;
    }

    /**
     * @param zhlb
     */
    public void setZhlb(String zhlb) {
        this.zhlb = zhlb == null ? null : zhlb.trim();
    }

    /**
     * @return ZHXM
     */
    public String getZhxm() {
        return zhxm;
    }

    /**
     * @param zhxm
     */
    public void setZhxm(String zhxm) {
        this.zhxm = zhxm == null ? null : zhxm.trim();
    }

    /**
     * @return ZH
     */
    public String getZh() {
        return zh;
    }

    /**
     * @param zh
     */
    public void setZh(String zh) {
        this.zh = zh == null ? null : zh.trim();
    }

    /**
     * @return SY
     */
    public String getSy() {
        return sy;
    }

    /**
     * @param sy
     */
    public void setSy(String sy) {
        this.sy = sy == null ? null : sy.trim();
    }

    /**
     * @return SM
     */
    public String getSm() {
        return sm;
    }

    /**
     * @param sm
     */
    public void setSm(String sm) {
        this.sm = sm == null ? null : sm.trim();
    }

    /**
     * @return DQR
     */
    public String getDqr() {
        return dqr;
    }

    /**
     * @param dqr
     */
    public void setDqr(String dqr) {
        this.dqr = dqr == null ? null : dqr.trim();
    }

    /**
     * @return JGFKXDSJ
     */
    public String getJgfkxdsj() {
        return jgfkxdsj;
    }

    /**
     * @param jgfkxdsj
     */
    public void setJgfkxdsj(String jgfkxdsj) {
        this.jgfkxdsj = jgfkxdsj == null ? null : jgfkxdsj.trim();
    }

    /**
     * @return SQSJ
     */
    public Object getSqsj() {
        return sqsj;
    }

    /**
     * @param sqsj
     */
    public void setSqsj(Object sqsj) {
        this.sqsj = sqsj;
    }

    /**
     * @return SQJGDM
     */
    public String getSqjgdm() {
        return sqjgdm;
    }

    /**
     * @param sqjgdm
     */
    public void setSqjgdm(String sqjgdm) {
        this.sqjgdm = sqjgdm == null ? null : sqjgdm.trim();
    }

    /**
     * @return SQJGMC
     */
    public String getSqjgmc() {
        return sqjgmc;
    }

    /**
     * @param sqjgmc
     */
    public void setSqjgmc(String sqjgmc) {
        this.sqjgmc = sqjgmc == null ? null : sqjgmc.trim();
    }

    /**
     * @return JBRZJZL
     */
    public String getJbrzjzl() {
        return jbrzjzl;
    }

    /**
     * @param jbrzjzl
     */
    public void setJbrzjzl(String jbrzjzl) {
        this.jbrzjzl = jbrzjzl == null ? null : jbrzjzl.trim();
    }

    /**
     * @return JBRZJHM
     */
    public String getJbrzjhm() {
        return jbrzjhm;
    }

    /**
     * @param jbrzjhm
     */
    public void setJbrzjhm(String jbrzjhm) {
        this.jbrzjhm = jbrzjhm == null ? null : jbrzjhm.trim();
    }

    /**
     * @return JBRXM
     */
    public String getJbrxm() {
        return jbrxm;
    }

    /**
     * @param jbrxm
     */
    public void setJbrxm(String jbrxm) {
        this.jbrxm = jbrxm == null ? null : jbrxm.trim();
    }

    /**
     * @return JBRDH
     */
    public String getJbrdh() {
        return jbrdh;
    }

    /**
     * @param jbrdh
     */
    public void setJbrdh(String jbrdh) {
        this.jbrdh = jbrdh == null ? null : jbrdh.trim();
    }

    /**
     * @return XCRZJZL
     */
    public String getXcrzjzl() {
        return xcrzjzl;
    }

    /**
     * @param xcrzjzl
     */
    public void setXcrzjzl(String xcrzjzl) {
        this.xcrzjzl = xcrzjzl == null ? null : xcrzjzl.trim();
    }

    /**
     * @return XCRZJHM
     */
    public String getXcrzjhm() {
        return xcrzjhm;
    }

    /**
     * @param xcrzjhm
     */
    public void setXcrzjhm(String xcrzjhm) {
        this.xcrzjhm = xcrzjhm == null ? null : xcrzjhm.trim();
    }

    /**
     * @return XCRXM
     */
    public String getXcrxm() {
        return xcrxm;
    }

    /**
     * @param xcrxm
     */
    public void setXcrxm(String xcrxm) {
        this.xcrxm = xcrxm == null ? null : xcrxm.trim();
    }

    /**
     * @return ZCJE
     */
    public String getZcje() {
        return zcje;
    }

    /**
     * @param zcje
     */
    public void setZcje(String zcje) {
        this.zcje = zcje == null ? null : zcje.trim();
    }

    /**
     * @return ZCSJ
     */
    public String getZcsj() {
        return zcsj;
    }

    /**
     * @param zcsj
     */
    public void setZcsj(String zcsj) {
        this.zcsj = zcsj == null ? null : zcsj.trim();
    }

    /**
     * @return ZCZHJGDM
     */
    public String getZczhjgdm() {
        return zczhjgdm;
    }

    /**
     * @param zczhjgdm
     */
    public void setZczhjgdm(String zczhjgdm) {
        this.zczhjgdm = zczhjgdm == null ? null : zczhjgdm.trim();
    }

    /**
     * @return ZCZHYHMC
     */
    public String getZczhyhmc() {
        return zczhyhmc;
    }

    /**
     * @param zczhyhmc
     */
    public void setZczhyhmc(String zczhyhmc) {
        this.zczhyhmc = zczhyhmc == null ? null : zczhyhmc.trim();
    }

    /**
     * @return ZCZHXM
     */
    public String getZczhxm() {
        return zczhxm;
    }

    /**
     * @param zczhxm
     */
    public void setZczhxm(String zczhxm) {
        this.zczhxm = zczhxm == null ? null : zczhxm.trim();
    }

    /**
     * @return ZCZH
     */
    public String getZczh() {
        return zczh;
    }

    /**
     * @param zczh
     */
    public void setZczh(String zczh) {
        this.zczh = zczh == null ? null : zczh.trim();
    }

    /**
     * @return DJFS
     */
    public String getDjfs() {
        return djfs;
    }

    /**
     * @param djfs
     */
    public void setDjfs(String djfs) {
        this.djfs = djfs == null ? null : djfs.trim();
    }

    /**
     * @return DJZHZJLX
     */
    public String getDjzhzjlx() {
        return djzhzjlx;
    }

    /**
     * @param djzhzjlx
     */
    public void setDjzhzjlx(String djzhzjlx) {
        this.djzhzjlx = djzhzjlx == null ? null : djzhzjlx.trim();
    }

    /**
     * @return DJZHZJHM
     */
    public String getDjzhzjhm() {
        return djzhzjhm;
    }

    /**
     * @param djzhzjhm
     */
    public void setDjzhzjhm(String djzhzjhm) {
        this.djzhzjhm = djzhzjhm == null ? null : djzhzjhm.trim();
    }

    /**
     * @return CXZTLB
     */
    public String getCxztlb() {
        return cxztlb;
    }

    /**
     * @param cxztlb
     */
    public void setCxztlb(String cxztlb) {
        this.cxztlb = cxztlb == null ? null : cxztlb.trim();
    }

    /**
     * @return CXNR
     */
    public String getCxnr() {
        return cxnr;
    }

    /**
     * @param cxnr
     */
    public void setCxnr(String cxnr) {
        this.cxnr = cxnr == null ? null : cxnr.trim();
    }

    /**
     * @return JYLSKSSJ
     */
    public String getJylskssj() {
        return jylskssj;
    }

    /**
     * @param jylskssj
     */
    public void setJylskssj(String jylskssj) {
        this.jylskssj = jylskssj == null ? null : jylskssj.trim();
    }

    /**
     * @return JYLSJSSJ
     */
    public String getJylsjssj() {
        return jylsjssj;
    }

    /**
     * @param jylsjssj
     */
    public void setJylsjssj(String jylsjssj) {
        this.jylsjssj = jylsjssj == null ? null : jylsjssj.trim();
    }

    /**
     * @return YSQBM
     */
    public String getYsqbm() {
        return ysqbm;
    }

    /**
     * @param ysqbm
     */
    public void setYsqbm(String ysqbm) {
        this.ysqbm = ysqbm == null ? null : ysqbm.trim();
    }

    /**
     * @return FHM
     */
    public String getFhm() {
        return fhm;
    }

    /**
     * @param fhm
     */
    public void setFhm(String fhm) {
        this.fhm = fhm == null ? null : fhm.trim();
    }

    /**
     * @return FHXX
     */
    public String getFhxx() {
        return fhxx;
    }

    /**
     * @param fhxx
     */
    public void setFhxx(String fhxx) {
        this.fhxx = fhxx == null ? null : fhxx.trim();
    }

    /**
     * @return LRRDWDM
     */
    public String getLrrdwdm() {
        return lrrdwdm;
    }

    /**
     * @param lrrdwdm
     */
    public void setLrrdwdm(String lrrdwdm) {
        this.lrrdwdm = lrrdwdm == null ? null : lrrdwdm.trim();
    }

    /**
     * @return LRRSFZH
     */
    public String getLrrsfzh() {
        return lrrsfzh;
    }

    /**
     * @param lrrsfzh
     */
    public void setLrrsfzh(String lrrsfzh) {
        this.lrrsfzh = lrrsfzh == null ? null : lrrsfzh.trim();
    }

    /**
     * @return LRRXM
     */
    public String getLrrxm() {
        return lrrxm;
    }

    /**
     * @param lrrxm
     */
    public void setLrrxm(String lrrxm) {
        this.lrrxm = lrrxm == null ? null : lrrxm.trim();
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public String getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(String yxx) {
        this.yxx = yxx == null ? null : yxx.trim();
    }

    /**
     * @return QQLX
     */
    public String getQqlx() {
        return qqlx;
    }

    /**
     * @param qqlx
     */
    public void setQqlx(String qqlx) {
        this.qqlx = qqlx == null ? null : qqlx.trim();
    }

    /**
     * @return YWXH
     */
    public String getYwxh() {
        return ywxh;
    }

    /**
     * @param ywxh
     */
    public void setYwxh(String ywxh) {
        this.ywxh = ywxh == null ? null : ywxh.trim();
    }

    /**
     * @return FSSJ
     */
    public String getFssj() {
        return fssj;
    }

    /**
     * @param fssj
     */
    public void setFssj(String fssj) {
        this.fssj = fssj == null ? null : fssj.trim();
    }

    /**
     * @return KSSJ
     */
    public String getKssj() {
        return kssj;
    }

    /**
     * @param kssj
     */
    public void setKssj(String kssj) {
        this.kssj = kssj == null ? null : kssj.trim();
    }

    /**
     * @return JZSJ
     */
    public String getJzsj() {
        return jzsj;
    }

    /**
     * @param jzsj
     */
    public void setJzsj(String jzsj) {
        this.jzsj = jzsj == null ? null : jzsj.trim();
    }

    /**
     * @return ZT
     */
    public String getZt() {
        return zt;
    }

    /**
     * @param zt
     */
    public void setZt(String zt) {
        this.zt = zt == null ? null : zt.trim();
    }

    /**
     * @return SBYY
     */
    public String getSbyy() {
        return sbyy;
    }

    /**
     * @param sbyy
     */
    public void setSbyy(String sbyy) {
        this.sbyy = sbyy == null ? null : sbyy.trim();
    }

    /**
     * @return YE
     */
    public String getYe() {
        return ye;
    }

    /**
     * @param ye
     */
    public void setYe(String ye) {
        this.ye = ye == null ? null : ye.trim();
    }

    /**
     * @return XCRDH
     */
    public String getXcrdh() {
        return xcrdh;
    }

    /**
     * @param xcrdh
     */
    public void setXcrdh(String xcrdh) {
        this.xcrdh = xcrdh == null ? null : xcrdh.trim();
    }

    /**
     * @return YJZF
     */
    public String getYjzf() {
        return yjzf;
    }

    /**
     * @param yjzf
     */
    public void setYjzf(String yjzf) {
        this.yjzf = yjzf == null ? null : yjzf.trim();
    }

    /**
     * @return JGKSSJ
     */
    public String getJgkssj() {
        return jgkssj;
    }

    /**
     * @param jgkssj
     */
    public void setJgkssj(String jgkssj) {
        this.jgkssj = jgkssj == null ? null : jgkssj.trim();
    }

    /**
     * @return JGJSSJ
     */
    public String getJgjssj() {
        return jgjssj;
    }

    /**
     * @param jgjssj
     */
    public void setJgjssj(String jgjssj) {
        this.jgjssj = jgjssj == null ? null : jgjssj.trim();
    }

    /**
     * @return XFZT
     */
    public String getXfzt() {
        return xfzt;
    }

    /**
     * @param xfzt
     */
    public void setXfzt(String xfzt) {
        this.xfzt = xfzt == null ? null : xfzt.trim();
    }

    /**
     * @return IP
     */
    public String getIp() {
        return ip;
    }

    /**
     * @param ip
     */
    public void setIp(String ip) {
        this.ip = ip == null ? null : ip.trim();
    }

    /**
     * @return FSZT
     */
    public String getFszt() {
        return fszt;
    }

    /**
     * @param fszt
     */
    public void setFszt(String fszt) {
        this.fszt = fszt == null ? null : fszt.trim();
    }

    /**
     * @return CFFSZT
     */
    public String getCffszt() {
        return cffszt;
    }

    /**
     * @param cffszt
     */
    public void setCffszt(String cffszt) {
        this.cffszt = cffszt == null ? null : cffszt.trim();
    }

    /**
     * @return MXFSZT
     */
    public String getMxfszt() {
        return mxfszt;
    }

    /**
     * @param mxfszt
     */
    public void setMxfszt(String mxfszt) {
        this.mxfszt = mxfszt == null ? null : mxfszt.trim();
    }

    /**
     * @return DQJZSJ
     */
    public String getDqjzsj() {
        return dqjzsj;
    }

    /**
     * @param dqjzsj
     */
    public void setDqjzsj(String dqjzsj) {
        this.dqjzsj = dqjzsj == null ? null : dqjzsj.trim();
    }

    /**
     * @return ZZHBFSZT
     */
    public String getZzhbfszt() {
        return zzhbfszt;
    }

    /**
     * @param zzhbfszt
     */
    public void setZzhbfszt(String zzhbfszt) {
        this.zzhbfszt = zzhbfszt == null ? null : zzhbfszt.trim();
    }

    /**
     * @return CSBFSZT
     */
    public String getCsbfszt() {
        return csbfszt;
    }

    /**
     * @param csbfszt
     */
    public void setCsbfszt(String csbfszt) {
        this.csbfszt = csbfszt == null ? null : csbfszt.trim();
    }

    /**
     * @return FSSBCS
     */
    public String getFssbcs() {
        return fssbcs;
    }

    /**
     * @param fssbcs
     */
    public void setFssbcs(String fssbcs) {
        this.fssbcs = fssbcs == null ? null : fssbcs.trim();
    }

    /**
     * @return ALBS
     */
    public String getAlbs() {
        return albs;
    }

    /**
     * @param albs
     */
    public void setAlbs(String albs) {
        this.albs = albs == null ? null : albs.trim();
    }

    /**
     * @return SFSCBW
     */
    public String getSfscbw() {
        return sfscbw;
    }

    /**
     * @param sfscbw
     */
    public void setSfscbw(String sfscbw) {
        this.sfscbw = sfscbw == null ? null : sfscbw.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", jybm=").append(jybm);
        sb.append(", bwlsh=").append(bwlsh);
        sb.append(", yhid=").append(yhid);
        sb.append(", ywb=").append(ywb);
        sb.append(", ajid=").append(ajid);
        sb.append(", ajbh=").append(ajbh);
        sb.append(", ywsqbh=").append(ywsqbh);
        sb.append(", ajlx=").append(ajlx);
        sb.append(", jjcd=").append(jjcd);
        sb.append(", rmbzl=").append(rmbzl);
        sb.append(", zhjgdm=").append(zhjgdm);
        sb.append(", zhyhmc=").append(zhyhmc);
        sb.append(", zhlb=").append(zhlb);
        sb.append(", zhxm=").append(zhxm);
        sb.append(", zh=").append(zh);
        sb.append(", sy=").append(sy);
        sb.append(", sm=").append(sm);
        sb.append(", dqr=").append(dqr);
        sb.append(", jgfkxdsj=").append(jgfkxdsj);
        sb.append(", sqsj=").append(sqsj);
        sb.append(", sqjgdm=").append(sqjgdm);
        sb.append(", sqjgmc=").append(sqjgmc);
        sb.append(", jbrzjzl=").append(jbrzjzl);
        sb.append(", jbrzjhm=").append(jbrzjhm);
        sb.append(", jbrxm=").append(jbrxm);
        sb.append(", jbrdh=").append(jbrdh);
        sb.append(", xcrzjzl=").append(xcrzjzl);
        sb.append(", xcrzjhm=").append(xcrzjhm);
        sb.append(", xcrxm=").append(xcrxm);
        sb.append(", zcje=").append(zcje);
        sb.append(", zcsj=").append(zcsj);
        sb.append(", zczhjgdm=").append(zczhjgdm);
        sb.append(", zczhyhmc=").append(zczhyhmc);
        sb.append(", zczhxm=").append(zczhxm);
        sb.append(", zczh=").append(zczh);
        sb.append(", djfs=").append(djfs);
        sb.append(", djzhzjlx=").append(djzhzjlx);
        sb.append(", djzhzjhm=").append(djzhzjhm);
        sb.append(", cxztlb=").append(cxztlb);
        sb.append(", cxnr=").append(cxnr);
        sb.append(", jylskssj=").append(jylskssj);
        sb.append(", jylsjssj=").append(jylsjssj);
        sb.append(", ysqbm=").append(ysqbm);
        sb.append(", fhm=").append(fhm);
        sb.append(", fhxx=").append(fhxx);
        sb.append(", lrrdwdm=").append(lrrdwdm);
        sb.append(", lrrsfzh=").append(lrrsfzh);
        sb.append(", lrrxm=").append(lrrxm);
        sb.append(", rksj=").append(rksj);
        sb.append(", yxx=").append(yxx);
        sb.append(", qqlx=").append(qqlx);
        sb.append(", ywxh=").append(ywxh);
        sb.append(", fssj=").append(fssj);
        sb.append(", kssj=").append(kssj);
        sb.append(", jzsj=").append(jzsj);
        sb.append(", zt=").append(zt);
        sb.append(", sbyy=").append(sbyy);
        sb.append(", ye=").append(ye);
        sb.append(", xcrdh=").append(xcrdh);
        sb.append(", yjzf=").append(yjzf);
        sb.append(", jgkssj=").append(jgkssj);
        sb.append(", jgjssj=").append(jgjssj);
        sb.append(", xfzt=").append(xfzt);
        sb.append(", ip=").append(ip);
        sb.append(", fszt=").append(fszt);
        sb.append(", cffszt=").append(cffszt);
        sb.append(", mxfszt=").append(mxfszt);
        sb.append(", dqjzsj=").append(dqjzsj);
        sb.append(", zzhbfszt=").append(zzhbfszt);
        sb.append(", csbfszt=").append(csbfszt);
        sb.append(", fssbcs=").append(fssbcs);
        sb.append(", albs=").append(albs);
        sb.append(", sfscbw=").append(sfscbw);
        sb.append("]");
        return sb.toString();
    }

    public GabDyzyYhkQq(String id, String jybm, String bwlsh, String yhid, String ywb, String ajid, String ajbh, String ywsqbh, String ajlx, String jjcd, String rmbzl, String zhjgdm, String zhyhmc, String zhlb, String zhxm, String zh, String sy, String sm, String dqr, String jgfkxdsj, Object sqsj, String sqjgdm, String sqjgmc, String jbrzjzl, String jbrzjhm, String jbrxm, String jbrdh, String xcrzjzl, String xcrzjhm, String xcrxm, String zcje, String zcsj, String zczhjgdm, String zczhyhmc, String zczhxm, String zczh, String djfs, String djzhzjlx, String djzhzjhm, String cxztlb, String cxnr, String jylskssj, String jylsjssj, String ysqbm, String fhm, String fhxx, String lrrdwdm, String lrrsfzh, String lrrxm, Date rksj, String yxx, String qqlx, String ywxh, String fssj, String kssj, String jzsj, String zt, String sbyy, String ye, String xcrdh, String yjzf, String jgkssj, String jgjssj, String xfzt, String ip, String fszt, String cffszt, String mxfszt, String dqjzsj, String zzhbfszt, String csbfszt, String fssbcs, String albs, String sfscbw) {
        this.id = id;
        this.jybm = jybm;
        this.bwlsh = bwlsh;
        this.yhid = yhid;
        this.ywb = ywb;
        this.ajid = ajid;
        this.ajbh = ajbh;
        this.ywsqbh = ywsqbh;
        this.ajlx = ajlx;
        this.jjcd = jjcd;
        this.rmbzl = rmbzl;
        this.zhjgdm = zhjgdm;
        this.zhyhmc = zhyhmc;
        this.zhlb = zhlb;
        this.zhxm = zhxm;
        this.zh = zh;
        this.sy = sy;
        this.sm = sm;
        this.dqr = dqr;
        this.jgfkxdsj = jgfkxdsj;
        this.sqsj = sqsj;
        this.sqjgdm = sqjgdm;
        this.sqjgmc = sqjgmc;
        this.jbrzjzl = jbrzjzl;
        this.jbrzjhm = jbrzjhm;
        this.jbrxm = jbrxm;
        this.jbrdh = jbrdh;
        this.xcrzjzl = xcrzjzl;
        this.xcrzjhm = xcrzjhm;
        this.xcrxm = xcrxm;
        this.zcje = zcje;
        this.zcsj = zcsj;
        this.zczhjgdm = zczhjgdm;
        this.zczhyhmc = zczhyhmc;
        this.zczhxm = zczhxm;
        this.zczh = zczh;
        this.djfs = djfs;
        this.djzhzjlx = djzhzjlx;
        this.djzhzjhm = djzhzjhm;
        this.cxztlb = cxztlb;
        this.cxnr = cxnr;
        this.jylskssj = jylskssj;
        this.jylsjssj = jylsjssj;
        this.ysqbm = ysqbm;
        this.fhm = fhm;
        this.fhxx = fhxx;
        this.lrrdwdm = lrrdwdm;
        this.lrrsfzh = lrrsfzh;
        this.lrrxm = lrrxm;
        this.rksj = rksj;
        this.yxx = yxx;
        this.qqlx = qqlx;
        this.ywxh = ywxh;
        this.fssj = fssj;
        this.kssj = kssj;
        this.jzsj = jzsj;
        this.zt = zt;
        this.sbyy = sbyy;
        this.ye = ye;
        this.xcrdh = xcrdh;
        this.yjzf = yjzf;
        this.jgkssj = jgkssj;
        this.jgjssj = jgjssj;
        this.xfzt = xfzt;
        this.ip = ip;
        this.fszt = fszt;
        this.cffszt = cffszt;
        this.mxfszt = mxfszt;
        this.dqjzsj = dqjzsj;
        this.zzhbfszt = zzhbfszt;
        this.csbfszt = csbfszt;
        this.fssbcs = fssbcs;
        this.albs = albs;
        this.sfscbw = sfscbw;
    }

    public GabDyzyYhkQq() {
    }
    public GabDyzyYhkQq(YhkMxQq bean, Jgz jgz) {
        this.qqlx="07";
        this.jjcd="02";
        this.ywb="2";
        this.zhlb=bean.getSubjecttype()+"";
        this.cxztlb=bean.getSubjecttype()+"";
        this.fhm="FDZPT";
        this.zhxm = bean.getAccountname();
        this.cxnr=bean.getInquirymode();

        this.zt="02";
        this.zh=bean.getCardnumber();
        this.zhjgdm=bean.getBankcode();
        //this.zczhyhmc=bean.getBankname();
        this.zhyhmc=bean.getBankname();
        this.kssj= TimeUtil.fmtDate(bean.getInquiryperiodstart(),"yyyyMMdd")+"000000";
        Date today = new Date();
        if(TimeUtil.addDay(bean.getInquiryperiodend(),1).before(today)){
            this.jzsj= TimeUtil.fmtDate(bean.getInquiryperiodend(),"yyyyMMdd")+"235959";
        }else{
            this.jzsj= TimeUtil.fmtDate(today,"yyyyMMddHHmmss");
        }
        this.rksj = bean.getRksj();

        this.sy=StringUtils.length(bean.getReason())>150? StringUtils.substring(bean.getReason(),0,150) : bean.getReason();
        this.lrrdwdm=bean.getLrdwdm().substring(0,6);
        this.lrrxm=bean.getLrrxm();
        this.lrrsfzh=bean.getLrrjh();

        this.jbrxm=jgz.getJbrxm();
        this.jbrzjzl=jgz.getJbrzjzl();
        this.jbrzjhm=jgz.getJbrzjhm();
        this.jbrdh=jgz.getJbrdh();
        this.xcrxm=jgz.getXcrxm();
        this.xcrzjzl=jgz.getXcrzjzl();
        this.xcrzjhm=jgz.getXcrzjhm();
        this.xcrdh=jgz.getXcrdh();
    }
    public GabDyzyYhkQq(YhkZtQq bean, Jgz jgz) {
        this.qqlx="08";
        this.jjcd="02";
        this.ywb="2";
        this.cxztlb=bean.getSubjecttype()+"";
        this.fhm="FDZPT";
        this.zhxm = bean.getAccountname();

        this.zt="02";
        this.zh=bean.getCardnumber();
        this.zhjgdm=bean.getBankcode();
        this.zhyhmc=bean.getBankname();
        this.rksj = bean.getRksj();

        this.sy=StringUtils.length(bean.getReason())>150? StringUtils.substring(bean.getReason(),0,150) : bean.getReason();
        this.lrrdwdm=bean.getLrdwdm().substring(0,6);
        this.lrrxm=bean.getLrrxm();
        this.lrrsfzh=bean.getLrrjh();

        this.jbrxm=jgz.getJbrxm();
        this.jbrzjzl=jgz.getJbrzjzl();
        this.jbrzjhm=jgz.getJbrzjhm();
        this.jbrdh=jgz.getJbrdh();
        this.xcrxm=jgz.getXcrxm();
        this.xcrzjzl=jgz.getXcrzjzl();
        this.xcrzjhm=jgz.getXcrzjhm();
        this.xcrdh=jgz.getXcrdh();
    }
    public GabDyzyYhkQq(YhkZfQq bean, Jgz jgz) {
        this.yjzf="0";
        this.qqlx="01";
        this.jjcd="02";
        this.ywb="2";
        this.zhlb="0"+bean.getSubjecttype();
        this.fhm="FDZPT";
        this.zhxm = bean.getAccountname();

        this.zt="02";
        this.zh=bean.getCardnumber();
        this.zhjgdm=bean.getBankcode();
        this.zczhyhmc=bean.getBankname();
        this.zhyhmc=bean.getBankname();
        this.zcje=bean.getTransferamount()+"";
        this.zcsj= TimeUtil.fmtDate(bean.getTransfertime(),"yyyy-MM-dd HH:mm:ss");
        this.rksj = bean.getRksj();

        this.sy=StringUtils.length(bean.getReason())>150? StringUtils.substring(bean.getReason(),0,150) : bean.getReason();
        this.lrrdwdm=bean.getLrdwdm().substring(0,6);
        this.lrrxm=bean.getLrrxm();
        this.lrrsfzh=bean.getLrrjh();

        this.jbrxm=jgz.getJbrxm();
        this.jbrzjzl=jgz.getJbrzjzl();
        this.jbrzjhm=jgz.getJbrzjhm();
        this.jbrdh=jgz.getJbrdh();
        this.xcrxm=jgz.getXcrxm();
        this.xcrzjzl=jgz.getXcrzjzl();
        this.xcrzjhm=jgz.getXcrzjhm();
        this.xcrdh=jgz.getXcrdh();
    }

}