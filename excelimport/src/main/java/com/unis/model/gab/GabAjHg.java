package com.unis.model.gab;

import java.util.Date;
import javax.persistence.*;

@Table(name = "XZ_DYFZ.TB_DXZP_AJ_HGXX")
public class GabAjHg {
    @Id
    @Column(name = "XH")
    private String xh;

    @Column(name = "AJID")
    private String ajid;

    @Column(name = "FADWDM")
    private String fadwdm;

    @Column(name = "FADWMC")
    private String fadwmc;

    @Column(name = "JDMS")
    private String jdms;

    @Column(name = "JZLB")
    private String jzlb;

    @Column(name = "YWXH")
    private String ywxh;

    @Column(name = "JZCZSJ")
    private Date jzczsj;

    @Column(name = "JZCZR")
    private String jzczr;

    @Column(name = "JZCZDWDM")
    private String jzczdwdm;

    @Column(name = "JZCZDWMC")
    private String jzczdwmc;

    @Column(name = "SFZX")
    private String sfzx;

    @Column(name = "ZXSJ")
    private Date zxsj;

    @Column(name = "ZXR")
    private String zxr;

    @Column(name = "ZXDWDM")
    private String zxdwdm;

    @Column(name = "ZXDWMC")
    private String zxdwmc;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private String yxx;

    /**
     * @return XH
     */
    public String getXh() {
        return xh;
    }

    /**
     * @param xh
     */
    public void setXh(String xh) {
        this.xh = xh == null ? null : xh.trim();
    }

    /**
     * @return AJID
     */
    public String getAjid() {
        return ajid;
    }

    /**
     * @param ajid
     */
    public void setAjid(String ajid) {
        this.ajid = ajid == null ? null : ajid.trim();
    }

    /**
     * @return FADWDM
     */
    public String getFadwdm() {
        return fadwdm;
    }

    /**
     * @param fadwdm
     */
    public void setFadwdm(String fadwdm) {
        this.fadwdm = fadwdm == null ? null : fadwdm.trim();
    }

    /**
     * @return FADWMC
     */
    public String getFadwmc() {
        return fadwmc;
    }

    /**
     * @param fadwmc
     */
    public void setFadwmc(String fadwmc) {
        this.fadwmc = fadwmc == null ? null : fadwmc.trim();
    }

    /**
     * @return JDMS
     */
    public String getJdms() {
        return jdms;
    }

    /**
     * @param jdms
     */
    public void setJdms(String jdms) {
        this.jdms = jdms == null ? null : jdms.trim();
    }

    /**
     * @return JZLB
     */
    public String getJzlb() {
        return jzlb;
    }

    /**
     * @param jzlb
     */
    public void setJzlb(String jzlb) {
        this.jzlb = jzlb == null ? null : jzlb.trim();
    }

    /**
     * @return YWXH
     */
    public String getYwxh() {
        return ywxh;
    }

    /**
     * @param ywxh
     */
    public void setYwxh(String ywxh) {
        this.ywxh = ywxh == null ? null : ywxh.trim();
    }

    /**
     * @return JZCZSJ
     */
    public Date getJzczsj() {
        return jzczsj;
    }

    /**
     * @param jzczsj
     */
    public void setJzczsj(Date jzczsj) {
        this.jzczsj = jzczsj;
    }

    /**
     * @return JZCZR
     */
    public String getJzczr() {
        return jzczr;
    }

    /**
     * @param jzczr
     */
    public void setJzczr(String jzczr) {
        this.jzczr = jzczr == null ? null : jzczr.trim();
    }

    /**
     * @return JZCZDWDM
     */
    public String getJzczdwdm() {
        return jzczdwdm;
    }

    /**
     * @param jzczdwdm
     */
    public void setJzczdwdm(String jzczdwdm) {
        this.jzczdwdm = jzczdwdm == null ? null : jzczdwdm.trim();
    }

    /**
     * @return JZCZDWMC
     */
    public String getJzczdwmc() {
        return jzczdwmc;
    }

    /**
     * @param jzczdwmc
     */
    public void setJzczdwmc(String jzczdwmc) {
        this.jzczdwmc = jzczdwmc == null ? null : jzczdwmc.trim();
    }

    /**
     * @return SFZX
     */
    public String getSfzx() {
        return sfzx;
    }

    /**
     * @param sfzx
     */
    public void setSfzx(String sfzx) {
        this.sfzx = sfzx == null ? null : sfzx.trim();
    }

    /**
     * @return ZXSJ
     */
    public Date getZxsj() {
        return zxsj;
    }

    /**
     * @param zxsj
     */
    public void setZxsj(Date zxsj) {
        this.zxsj = zxsj;
    }

    /**
     * @return ZXR
     */
    public String getZxr() {
        return zxr;
    }

    /**
     * @param zxr
     */
    public void setZxr(String zxr) {
        this.zxr = zxr == null ? null : zxr.trim();
    }

    /**
     * @return ZXDWDM
     */
    public String getZxdwdm() {
        return zxdwdm;
    }

    /**
     * @param zxdwdm
     */
    public void setZxdwdm(String zxdwdm) {
        this.zxdwdm = zxdwdm == null ? null : zxdwdm.trim();
    }

    /**
     * @return ZXDWMC
     */
    public String getZxdwmc() {
        return zxdwmc;
    }

    /**
     * @param zxdwmc
     */
    public void setZxdwmc(String zxdwmc) {
        this.zxdwmc = zxdwmc == null ? null : zxdwmc.trim();
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public String getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(String yxx) {
        this.yxx = yxx == null ? null : yxx.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", xh=").append(xh);
        sb.append(", ajid=").append(ajid);
        sb.append(", fadwdm=").append(fadwdm);
        sb.append(", fadwmc=").append(fadwmc);
        sb.append(", jdms=").append(jdms);
        sb.append(", jzlb=").append(jzlb);
        sb.append(", ywxh=").append(ywxh);
        sb.append(", jzczsj=").append(jzczsj);
        sb.append(", jzczr=").append(jzczr);
        sb.append(", jzczdwdm=").append(jzczdwdm);
        sb.append(", jzczdwmc=").append(jzczdwmc);
        sb.append(", sfzx=").append(sfzx);
        sb.append(", zxsj=").append(zxsj);
        sb.append(", zxr=").append(zxr);
        sb.append(", zxdwdm=").append(zxdwdm);
        sb.append(", zxdwmc=").append(zxdwmc);
        sb.append(", rksj=").append(rksj);
        sb.append(", yxx=").append(yxx);
        sb.append("]");
        return sb.toString();
    }
}