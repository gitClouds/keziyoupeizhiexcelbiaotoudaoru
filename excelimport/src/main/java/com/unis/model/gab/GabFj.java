package com.unis.model.gab;

import java.util.Date;
import javax.persistence.*;

@Table(name = "XZ_DYFZ.DYZY_FJ_MONGO")
public class GabFj {
    @Id
    @Column(name = "FJID")
    private String fjid;

    @Column(name = "YWLX")
    private String ywlx;

    @Column(name = "YWID")
    private String ywid;

    @Column(name = "FJMC")
    private String fjmc;

    @Column(name = "FJLX")
    private String fjlx;

    @Column(name = "FJ")
    private String fj;

    @Column(name = "FJSM")
    private String fjsm;

    @Column(name = "LRR")
    private String lrr;

    @Column(name = "LRSJ")
    private Date lrsj;

    @Column(name = "LRDW")
    private String lrdw;

    @Column(name = "BJSJ")
    private String bjsj;

    @Column(name = "DD")
    private String dd;

    @Column(name = "YHBH")
    private String yhbh;

    @Column(name = "ATMBH")
    private String atmbh;

    @Column(name = "YWXH")
    private String ywxh;

    @Column(name = "YXX")
    private String yxx;

    @Column(name = "C_XH")
    private String cXh;

    /**
     * @return FJID
     */
    public String getFjid() {
        return fjid;
    }

    /**
     * @param fjid
     */
    public void setFjid(String fjid) {
        this.fjid = fjid == null ? null : fjid.trim();
    }

    /**
     * @return YWLX
     */
    public String getYwlx() {
        return ywlx;
    }

    /**
     * @param ywlx
     */
    public void setYwlx(String ywlx) {
        this.ywlx = ywlx == null ? null : ywlx.trim();
    }

    /**
     * @return YWID
     */
    public String getYwid() {
        return ywid;
    }

    /**
     * @param ywid
     */
    public void setYwid(String ywid) {
        this.ywid = ywid == null ? null : ywid.trim();
    }

    /**
     * @return FJMC
     */
    public String getFjmc() {
        return fjmc;
    }

    /**
     * @param fjmc
     */
    public void setFjmc(String fjmc) {
        this.fjmc = fjmc == null ? null : fjmc.trim();
    }

    /**
     * @return FJLX
     */
    public String getFjlx() {
        return fjlx;
    }

    /**
     * @param fjlx
     */
    public void setFjlx(String fjlx) {
        this.fjlx = fjlx == null ? null : fjlx.trim();
    }

    /**
     * @return FJ
     */
    public String getFj() {
        return fj;
    }

    /**
     * @param fj
     */
    public void setFj(String fj) {
        this.fj = fj == null ? null : fj.trim();
    }

    /**
     * @return FJSM
     */
    public String getFjsm() {
        return fjsm;
    }

    /**
     * @param fjsm
     */
    public void setFjsm(String fjsm) {
        this.fjsm = fjsm == null ? null : fjsm.trim();
    }

    /**
     * @return LRR
     */
    public String getLrr() {
        return lrr;
    }

    /**
     * @param lrr
     */
    public void setLrr(String lrr) {
        this.lrr = lrr == null ? null : lrr.trim();
    }

    /**
     * @return LRSJ
     */
    public Date getLrsj() {
        return lrsj;
    }

    /**
     * @param lrsj
     */
    public void setLrsj(Date lrsj) {
        this.lrsj = lrsj;
    }

    /**
     * @return LRDW
     */
    public String getLrdw() {
        return lrdw;
    }

    /**
     * @param lrdw
     */
    public void setLrdw(String lrdw) {
        this.lrdw = lrdw == null ? null : lrdw.trim();
    }

    /**
     * @return BJSJ
     */
    public String getBjsj() {
        return bjsj;
    }

    /**
     * @param bjsj
     */
    public void setBjsj(String bjsj) {
        this.bjsj = bjsj == null ? null : bjsj.trim();
    }

    /**
     * @return DD
     */
    public String getDd() {
        return dd;
    }

    /**
     * @param dd
     */
    public void setDd(String dd) {
        this.dd = dd == null ? null : dd.trim();
    }

    /**
     * @return YHBH
     */
    public String getYhbh() {
        return yhbh;
    }

    /**
     * @param yhbh
     */
    public void setYhbh(String yhbh) {
        this.yhbh = yhbh == null ? null : yhbh.trim();
    }

    /**
     * @return ATMBH
     */
    public String getAtmbh() {
        return atmbh;
    }

    /**
     * @param atmbh
     */
    public void setAtmbh(String atmbh) {
        this.atmbh = atmbh == null ? null : atmbh.trim();
    }

    /**
     * @return YWXH
     */
    public String getYwxh() {
        return ywxh;
    }

    /**
     * @param ywxh
     */
    public void setYwxh(String ywxh) {
        this.ywxh = ywxh == null ? null : ywxh.trim();
    }

    /**
     * @return YXX
     */
    public String getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(String yxx) {
        this.yxx = yxx == null ? null : yxx.trim();
    }

    /**
     * @return C_XH
     */
    public String getcXh() {
        return cXh;
    }

    /**
     * @param cXh
     */
    public void setcXh(String cXh) {
        this.cXh = cXh == null ? null : cXh.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", fjid=").append(fjid);
        sb.append(", ywlx=").append(ywlx);
        sb.append(", ywid=").append(ywid);
        sb.append(", fjmc=").append(fjmc);
        sb.append(", fjlx=").append(fjlx);
        sb.append(", fj=").append(fj);
        sb.append(", fjsm=").append(fjsm);
        sb.append(", lrr=").append(lrr);
        sb.append(", lrsj=").append(lrsj);
        sb.append(", lrdw=").append(lrdw);
        sb.append(", bjsj=").append(bjsj);
        sb.append(", dd=").append(dd);
        sb.append(", yhbh=").append(yhbh);
        sb.append(", atmbh=").append(atmbh);
        sb.append(", ywxh=").append(ywxh);
        sb.append(", yxx=").append(yxx);
        sb.append(", cXh=").append(cXh);
        sb.append("]");
        return sb.toString();
    }
}