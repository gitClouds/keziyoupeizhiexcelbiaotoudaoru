package com.unis.model.gab;

import java.util.Date;
import javax.persistence.*;

@Table(name = "XZ_DYFZ.TB_DXZP_AJ_XXL_DH")
public class GabAjDh {
    @Id
    @Column(name = "XH")
    private String xh;

    @Column(name = "AJID")
    private String ajid;

    @Column(name = "SADHHM")
    private String sadhhm;

    @Column(name = "DHGSDDM")
    private String dhgsddm;

    @Column(name = "DHGSDMC")
    private String dhgsdmc;

    @Column(name = "KSSJ")
    private Date kssj;

    @Column(name = "JSSJ")
    private Date jssj;

    @Column(name = "LRR")
    private String lrr;

    @Column(name = "LRRSFZH")
    private String lrrsfzh;

    @Column(name = "LRRDWDM")
    private String lrrdwdm;

    @Column(name = "LRRDWMC")
    private String lrrdwmc;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private String yxx;

    @Column(name = "DHLX")
    private String dhlx;

    @Column(name = "YYS")
    private String yys;

    @Column(name = "SASJHM")
    private String sasjhm;

    @Column(name = "SHRDH")
    private String shrdh;

    @Column(name = "SFYLD")
    private String sfyld;

    @Column(name = "THNR")
    private String thnr;

    @Column(name = "BDIP")
    private String bdip;

    @Column(name = "BDID")
    private String bdid;

    @Column(name = "LDR")
    private String ldr;

    @Column(name = "LDSJ")
    private Date ldsj;

    @Column(name = "LDDZ")
    private String lddz;

    @Column(name = "LDDWMC")
    private String lddwmc;

    @Column(name = "LDDWDM")
    private String lddwdm;

    @Column(name = "C_XH")
    private String cXh;

    @Column(name = "ISFILTER")
    private String isfilter;

    @Column(name = "SFHMD")
    private String sfhmd;

    /**
     * @return XH
     */
    public String getXh() {
        return xh;
    }

    /**
     * @param xh
     */
    public void setXh(String xh) {
        this.xh = xh == null ? null : xh.trim();
    }

    /**
     * @return AJID
     */
    public String getAjid() {
        return ajid;
    }

    /**
     * @param ajid
     */
    public void setAjid(String ajid) {
        this.ajid = ajid == null ? null : ajid.trim();
    }

    /**
     * @return SADHHM
     */
    public String getSadhhm() {
        return sadhhm;
    }

    /**
     * @param sadhhm
     */
    public void setSadhhm(String sadhhm) {
        this.sadhhm = sadhhm == null ? null : sadhhm.trim();
    }

    /**
     * @return DHGSDDM
     */
    public String getDhgsddm() {
        return dhgsddm;
    }

    /**
     * @param dhgsddm
     */
    public void setDhgsddm(String dhgsddm) {
        this.dhgsddm = dhgsddm == null ? null : dhgsddm.trim();
    }

    /**
     * @return DHGSDMC
     */
    public String getDhgsdmc() {
        return dhgsdmc;
    }

    /**
     * @param dhgsdmc
     */
    public void setDhgsdmc(String dhgsdmc) {
        this.dhgsdmc = dhgsdmc == null ? null : dhgsdmc.trim();
    }

    /**
     * @return KSSJ
     */
    public Date getKssj() {
        return kssj;
    }

    /**
     * @param kssj
     */
    public void setKssj(Date kssj) {
        this.kssj = kssj;
    }

    /**
     * @return JSSJ
     */
    public Date getJssj() {
        return jssj;
    }

    /**
     * @param jssj
     */
    public void setJssj(Date jssj) {
        this.jssj = jssj;
    }

    /**
     * @return LRR
     */
    public String getLrr() {
        return lrr;
    }

    /**
     * @param lrr
     */
    public void setLrr(String lrr) {
        this.lrr = lrr == null ? null : lrr.trim();
    }

    /**
     * @return LRRSFZH
     */
    public String getLrrsfzh() {
        return lrrsfzh;
    }

    /**
     * @param lrrsfzh
     */
    public void setLrrsfzh(String lrrsfzh) {
        this.lrrsfzh = lrrsfzh == null ? null : lrrsfzh.trim();
    }

    /**
     * @return LRRDWDM
     */
    public String getLrrdwdm() {
        return lrrdwdm;
    }

    /**
     * @param lrrdwdm
     */
    public void setLrrdwdm(String lrrdwdm) {
        this.lrrdwdm = lrrdwdm == null ? null : lrrdwdm.trim();
    }

    /**
     * @return LRRDWMC
     */
    public String getLrrdwmc() {
        return lrrdwmc;
    }

    /**
     * @param lrrdwmc
     */
    public void setLrrdwmc(String lrrdwmc) {
        this.lrrdwmc = lrrdwmc == null ? null : lrrdwmc.trim();
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public String getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(String yxx) {
        this.yxx = yxx == null ? null : yxx.trim();
    }

    /**
     * @return DHLX
     */
    public String getDhlx() {
        return dhlx;
    }

    /**
     * @param dhlx
     */
    public void setDhlx(String dhlx) {
        this.dhlx = dhlx == null ? null : dhlx.trim();
    }

    /**
     * @return YYS
     */
    public String getYys() {
        return yys;
    }

    /**
     * @param yys
     */
    public void setYys(String yys) {
        this.yys = yys == null ? null : yys.trim();
    }

    /**
     * @return SASJHM
     */
    public String getSasjhm() {
        return sasjhm;
    }

    /**
     * @param sasjhm
     */
    public void setSasjhm(String sasjhm) {
        this.sasjhm = sasjhm == null ? null : sasjhm.trim();
    }

    /**
     * @return SHRDH
     */
    public String getShrdh() {
        return shrdh;
    }

    /**
     * @param shrdh
     */
    public void setShrdh(String shrdh) {
        this.shrdh = shrdh == null ? null : shrdh.trim();
    }

    /**
     * @return SFYLD
     */
    public String getSfyld() {
        return sfyld;
    }

    /**
     * @param sfyld
     */
    public void setSfyld(String sfyld) {
        this.sfyld = sfyld == null ? null : sfyld.trim();
    }

    /**
     * @return THNR
     */
    public String getThnr() {
        return thnr;
    }

    /**
     * @param thnr
     */
    public void setThnr(String thnr) {
        this.thnr = thnr == null ? null : thnr.trim();
    }

    /**
     * @return BDIP
     */
    public String getBdip() {
        return bdip;
    }

    /**
     * @param bdip
     */
    public void setBdip(String bdip) {
        this.bdip = bdip == null ? null : bdip.trim();
    }

    /**
     * @return BDID
     */
    public String getBdid() {
        return bdid;
    }

    /**
     * @param bdid
     */
    public void setBdid(String bdid) {
        this.bdid = bdid == null ? null : bdid.trim();
    }

    /**
     * @return LDR
     */
    public String getLdr() {
        return ldr;
    }

    /**
     * @param ldr
     */
    public void setLdr(String ldr) {
        this.ldr = ldr == null ? null : ldr.trim();
    }

    /**
     * @return LDSJ
     */
    public Date getLdsj() {
        return ldsj;
    }

    /**
     * @param ldsj
     */
    public void setLdsj(Date ldsj) {
        this.ldsj = ldsj;
    }

    /**
     * @return LDDZ
     */
    public String getLddz() {
        return lddz;
    }

    /**
     * @param lddz
     */
    public void setLddz(String lddz) {
        this.lddz = lddz == null ? null : lddz.trim();
    }

    /**
     * @return LDDWMC
     */
    public String getLddwmc() {
        return lddwmc;
    }

    /**
     * @param lddwmc
     */
    public void setLddwmc(String lddwmc) {
        this.lddwmc = lddwmc == null ? null : lddwmc.trim();
    }

    /**
     * @return LDDWDM
     */
    public String getLddwdm() {
        return lddwdm;
    }

    /**
     * @param lddwdm
     */
    public void setLddwdm(String lddwdm) {
        this.lddwdm = lddwdm == null ? null : lddwdm.trim();
    }

    /**
     * @return C_XH
     */
    public String getcXh() {
        return cXh;
    }

    /**
     * @param cXh
     */
    public void setcXh(String cXh) {
        this.cXh = cXh == null ? null : cXh.trim();
    }

    /**
     * @return ISFILTER
     */
    public String getIsfilter() {
        return isfilter;
    }

    /**
     * @param isfilter
     */
    public void setIsfilter(String isfilter) {
        this.isfilter = isfilter == null ? null : isfilter.trim();
    }

    /**
     * @return SFHMD
     */
    public String getSfhmd() {
        return sfhmd;
    }

    /**
     * @param sfhmd
     */
    public void setSfhmd(String sfhmd) {
        this.sfhmd = sfhmd == null ? null : sfhmd.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", xh=").append(xh);
        sb.append(", ajid=").append(ajid);
        sb.append(", sadhhm=").append(sadhhm);
        sb.append(", dhgsddm=").append(dhgsddm);
        sb.append(", dhgsdmc=").append(dhgsdmc);
        sb.append(", kssj=").append(kssj);
        sb.append(", jssj=").append(jssj);
        sb.append(", lrr=").append(lrr);
        sb.append(", lrrsfzh=").append(lrrsfzh);
        sb.append(", lrrdwdm=").append(lrrdwdm);
        sb.append(", lrrdwmc=").append(lrrdwmc);
        sb.append(", rksj=").append(rksj);
        sb.append(", yxx=").append(yxx);
        sb.append(", dhlx=").append(dhlx);
        sb.append(", yys=").append(yys);
        sb.append(", sasjhm=").append(sasjhm);
        sb.append(", shrdh=").append(shrdh);
        sb.append(", sfyld=").append(sfyld);
        sb.append(", thnr=").append(thnr);
        sb.append(", bdip=").append(bdip);
        sb.append(", bdid=").append(bdid);
        sb.append(", ldr=").append(ldr);
        sb.append(", ldsj=").append(ldsj);
        sb.append(", lddz=").append(lddz);
        sb.append(", lddwmc=").append(lddwmc);
        sb.append(", lddwdm=").append(lddwdm);
        sb.append(", cXh=").append(cXh);
        sb.append(", isfilter=").append(isfilter);
        sb.append(", sfhmd=").append(sfhmd);
        sb.append("]");
        return sb.toString();
    }
}