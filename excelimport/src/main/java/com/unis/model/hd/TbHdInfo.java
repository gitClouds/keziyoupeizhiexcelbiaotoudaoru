package com.unis.model.hd;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author xuk
 * @version 1.0
 * @since 2020-03-06
 */
@Table(name = "TB_HD_INFO")
public class TbHdInfo {

    private static final long serialVersionUID = 1L;

/****************************************
 * Basic fields
 ****************************************/
    /**
     * 注释: varchar
     */
    @Id
    @Column(name = "HD_ID")
    private String hd_id;

    /**
     * 手机号/身份证号: varchar
     */
    @Column(name = "HD_HM")
    private String hd_hm;

    /**
     * 户主: varchar
     */
    @Column(name = "HD_HZ")
    private String hd_hz;

    /**
     * 话单来源,1,系统录入,2,账号查询: decimal
     */
    @Column(name = "HD_LY")
    private Integer hd_ly;

    /**
     * 注释: varchar
     */
    @Column(name = "HD_LRR_XM")
    private String hd_lrr_xm;

    /**
     * 注释: varchar
     */
    @Column(name = "HD_LRR_DM")
    private String hd_lrr_dm;

    /**
     * 注释: varchar
     */
    @Column(name = "HD_LRR_DW_MC")
    private String hd_lrr_dw_mc;

    /**
     * 注释: varchar
     */
    @Column(name = "HD_LRR_DW_DM")
    private String hd_lrr_dw_dm;

    /**
     * 有效性: decimal
     */
    @Column(name = "HD_YXX")
    private Integer hd_yxx;

    /**
     * 注释: date
     */
    @Column(name = "HD_LRSJ")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date hd_lrsj;

    /**
     * 手机号类型: decimal
     */
    @Column(name = "HD_PHONE_TYPE")
    private Integer hd_phone_type;

    /**
     * 是否查询过,1查询过,0 未查询: decimal
     */
    @Column(name = "HD_CX")
    private Integer hd_cx;

    /**
     * 最后一次查询时间: date
     */
    @Column(name = "HD_LAST_CXSJ")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date hd_last_cxsj;


    @Column(name = "ZH_ID")
    private String zh_id;

    public String getHd_id() {
        return hd_id;
    }

    public void setHd_id(String hd_id) {
        this.hd_id = hd_id;
    }

    public String getHd_hm() {
        return hd_hm;
    }

    public void setHd_hm(String hd_hm) {
        this.hd_hm = hd_hm;
    }

    public String getHd_hz() {
        return hd_hz;
    }

    public void setHd_hz(String hd_hz) {
        this.hd_hz = hd_hz;
    }

    public Integer getHd_ly() {
        return hd_ly;
    }

    public void setHd_ly(Integer hd_ly) {
        this.hd_ly = hd_ly;
    }

    public String getHd_lrr_xm() {
        return hd_lrr_xm;
    }

    public void setHd_lrr_xm(String hd_lrr_xm) {
        this.hd_lrr_xm = hd_lrr_xm;
    }

    public String getHd_lrr_dm() {
        return hd_lrr_dm;
    }

    public void setHd_lrr_dm(String hd_lrr_dm) {
        this.hd_lrr_dm = hd_lrr_dm;
    }

    public String getHd_lrr_dw_mc() {
        return hd_lrr_dw_mc;
    }

    public void setHd_lrr_dw_mc(String hd_lrr_dw_mc) {
        this.hd_lrr_dw_mc = hd_lrr_dw_mc;
    }

    public String getHd_lrr_dw_dm() {
        return hd_lrr_dw_dm;
    }

    public void setHd_lrr_dw_dm(String hd_lrr_dw_dm) {
        this.hd_lrr_dw_dm = hd_lrr_dw_dm;
    }

    public Integer getHd_yxx() {
        return hd_yxx;
    }

    public void setHd_yxx(Integer hd_yxx) {
        this.hd_yxx = hd_yxx;
    }

    public Date getHd_lrsj() {
        return hd_lrsj;
    }

    public void setHd_lrsj(Date hd_lrsj) {
        this.hd_lrsj = hd_lrsj;
    }

    public Integer getHd_phone_type() {
        return hd_phone_type;
    }

    public void setHd_phone_type(Integer hd_phone_type) {
        this.hd_phone_type = hd_phone_type;
    }

    public Integer getHd_cx() {
        return hd_cx;
    }

    public void setHd_cx(Integer hd_cx) {
        this.hd_cx = hd_cx;
    }

    public Date getHd_last_cxsj() {
        return hd_last_cxsj;
    }

    public void setHd_last_cxsj(Date hd_last_cxsj) {
        this.hd_last_cxsj = hd_last_cxsj;
    }

    public String getZh_id() {
        return zh_id;
    }

    public void setZh_id(String zh_id) {
        this.zh_id = zh_id;
    }
}
