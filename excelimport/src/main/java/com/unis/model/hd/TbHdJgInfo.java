package com.unis.model.hd;


import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

/**
 * @author Qx
 * @version 1.0
 * @since 2020-03-06
 */
@Table(name = "TB_HD_JG_INFO")
public class TbHdJgInfo {

    private static final long serialVersionUID = 1L;

/****************************************
 * Basic fields
 ****************************************/
    /**
     * 注释: varchar
     */
    @Id
    @Column(name = "JG_ID")
    private String jg_id;

    /**
     * 注释: varchar
     */
    @Column(name = "HD_ID")
    private String hd_id;

    /**
     * 身份证号/电话号码: varchar
     */
    @Column(name = "JG_HM")
    private String jg_hm;

    /**
     * 号码类别  01,电话号码,  02,身份证号: varchar
     */
    @Column(name = "JG_HMLB")
    private String jg_hmlb;

    /**
     * 运营商类型 01移动 02联通 03电信 04广电: varchar
     */
    @Column(name = "JG_YYSLX")
    private String jg_yyslx;

    /**
     * 创建时间，号码在黑恶克星的申请 时间，因通话记录记录查询时间范 围，可根据申请时间进行模糊查 询，不能作为通话记录的查询时间 段: date
     */
    @Column(name = "JG_CREATETIME")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date jg_createtime;

    /**
     * 黑恶克星中申请查询号码的描述， 描述中体现一些查询要求，包含时 间范围、所查内容。: varchar
     */
    @Column(name = "JG_EVIDENDESC")
    private String jg_evidendesc;

    /**
     * 文件名称: varchar
     */
    @Column(name = "JG_TITLE")
    private String jg_title;

    /**
     * 文件存储地址，珠海云文件中心: varchar
     */
    @Column(name = "JG_FILE_DIR")
    private String jg_file_dir;

    /**
     * 压缩文件密码: varchar
     */
    @Column(name = "JG_FILEPASSWORD")
    private String jg_filepassword;

    /**
     * 注释: date
     */
    @Column(name = "JG_LRSJ")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date jg_lrsj;

    /**
     * 本地地址: varchar
     */
    @Column(name = "FJ_MONG_ID")
    private String fj_mong_id;

    /**
     * 本地地址: varchar
     */
    @Transient
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private String jg_begin;

    /**
     * 本地地址: varchar
     */
    @Transient
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private String jg_end;

    public String getJg_id() {
        return jg_id;
    }

    public void setJg_id(String jg_id) {
        this.jg_id = jg_id;
    }

    public String getHd_id() {
        return hd_id;
    }

    public void setHd_id(String hd_id) {
        this.hd_id = hd_id;
    }

    public String getJg_hm() {
        return jg_hm;
    }

    public void setJg_hm(String jg_hm) {
        this.jg_hm = jg_hm;
    }

    public String getJg_hmlb() {
        return jg_hmlb;
    }

    public void setJg_hmlb(String jg_hmlb) {
        this.jg_hmlb = jg_hmlb;
    }

    public String getJg_yyslx() {
        return jg_yyslx;
    }

    public void setJg_yyslx(String jg_yyslx) {
        this.jg_yyslx = jg_yyslx;
    }

    public Date getJg_createtime() {
        return jg_createtime;
    }

    public void setJg_createtime(Date jg_createtime) {
        this.jg_createtime = jg_createtime;
    }

    public String getJg_evidendesc() {
        return jg_evidendesc;
    }

    public void setJg_evidendesc(String jg_evidendesc) {
        this.jg_evidendesc = jg_evidendesc;
    }

    public String getJg_title() {
        return jg_title;
    }

    public void setJg_title(String jg_title) {
        this.jg_title = jg_title;
    }

    public String getJg_file_dir() {
        return jg_file_dir;
    }

    public void setJg_file_dir(String jg_file_dir) {
        this.jg_file_dir = jg_file_dir;
    }

    public String getJg_filepassword() {
        return jg_filepassword;
    }

    public void setJg_filepassword(String jg_filepassword) {
        this.jg_filepassword = jg_filepassword;
    }

    public Date getJg_lrsj() {
        return jg_lrsj;
    }

    public void setJg_lrsj(Date jg_lrsj) {
        this.jg_lrsj = jg_lrsj;
    }

    public String getFj_mong_id() {
        return fj_mong_id;
    }

    public void setFj_mong_id(String fj_mong_id) {
        this.fj_mong_id = fj_mong_id;
    }

    public String getJg_begin() {
        return jg_begin;
    }

    public void setJg_begin(String jg_begin) {
        this.jg_begin = jg_begin;
    }

    public String getJg_end() {
        return jg_end;
    }

    public void setJg_end(String jg_end) {
        this.jg_end = jg_end;
    }
}
