package com.unis.model.znzf;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_ZNZF_QUEUE_MAC")
public class QueueMac {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "SERTYPE")
    private Short sertype;

    @Column(name = "APPLICATIONID")
    private String applicationid;

    @Column(name = "QQPK")
    private String qqpk;

    @Column(name = "JJDPK")
    private String jjdpk;

    @Column(name = "QQZH")
    private String qqzh;

    @Column(name = "QQZHXM")
    private String qqzhxm;

    @Column(name = "QQZHJGDM")
    private String qqzhjgdm;

    @Column(name = "QQZHJGMC")
    private String qqzhjgmc;

    @Column(name = "LRSJ")
    private Date lrsj;

    @Column(name = "THZF")
    private String thzf;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return SERTYPE
     */
    public Short getSertype() {
        return sertype;
    }

    /**
     * @param sertype
     */
    public void setSertype(Short sertype) {
        this.sertype = sertype;
    }

    /**
     * @return APPLICATIONID
     */
    public String getApplicationid() {
        return applicationid;
    }

    /**
     * @param applicationid
     */
    public void setApplicationid(String applicationid) {
        this.applicationid = applicationid == null ? null : applicationid.trim();
    }

    /**
     * @return QQPK
     */
    public String getQqpk() {
        return qqpk;
    }

    /**
     * @param qqpk
     */
    public void setQqpk(String qqpk) {
        this.qqpk = qqpk == null ? null : qqpk.trim();
    }

    /**
     * @return JJDPK
     */
    public String getJjdpk() {
        return jjdpk;
    }

    /**
     * @param jjdpk
     */
    public void setJjdpk(String jjdpk) {
        this.jjdpk = jjdpk == null ? null : jjdpk.trim();
    }

    /**
     * @return QQZH
     */
    public String getQqzh() {
        return qqzh;
    }

    /**
     * @param qqzh
     */
    public void setQqzh(String qqzh) {
        this.qqzh = qqzh == null ? null : qqzh.trim();
    }

    /**
     * @return QQZHXM
     */
    public String getQqzhxm() {
        return qqzhxm;
    }

    /**
     * @param qqzhxm
     */
    public void setQqzhxm(String qqzhxm) {
        this.qqzhxm = qqzhxm == null ? null : qqzhxm.trim();
    }

    /**
     * @return QQZHJGDM
     */
    public String getQqzhjgdm() {
        return qqzhjgdm;
    }

    /**
     * @param qqzhjgdm
     */
    public void setQqzhjgdm(String qqzhjgdm) {
        this.qqzhjgdm = qqzhjgdm == null ? null : qqzhjgdm.trim();
    }

    /**
     * @return QQZHJGMC
     */
    public String getQqzhjgmc() {
        return qqzhjgmc;
    }

    /**
     * @param qqzhjgmc
     */
    public void setQqzhjgmc(String qqzhjgmc) {
        this.qqzhjgmc = qqzhjgmc == null ? null : qqzhjgmc.trim();
    }

    /**
     * @return LRSJ
     */
    public Date getLrsj() {
        return lrsj;
    }

    /**
     * @param lrsj
     */
    public void setLrsj(Date lrsj) {
        this.lrsj = lrsj;
    }

    /**
     * @return THZF
     */
    public String getThzf() {
        return thzf;
    }

    /**
     * @param thzf
     */
    public void setThzf(String thzf) {
        this.thzf = thzf == null ? null : thzf.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", sertype=").append(sertype);
        sb.append(", applicationid=").append(applicationid);
        sb.append(", qqpk=").append(qqpk);
        sb.append(", jjdpk=").append(jjdpk);
        sb.append(", qqzh=").append(qqzh);
        sb.append(", qqzhxm=").append(qqzhxm);
        sb.append(", qqzhjgdm=").append(qqzhjgdm);
        sb.append(", qqzhjgmc=").append(qqzhjgmc);
        sb.append(", lrsj=").append(lrsj);
        sb.append(", thzf=").append(thzf);
        sb.append("]");
        return sb.toString();
    }
}