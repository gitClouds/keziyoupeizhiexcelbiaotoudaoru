package com.unis.model.znzf;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_ZNZF_CB_MAC_GAB_JJD")
public class CbMacGabJjd {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "SERTYPE")
    private Short sertype;

    @Column(name = "JJDPK")
    private String jjdpk;

    @Column(name = "ZT")
    private Short zt;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private Short yxx;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return SERTYPE
     */
    public Short getSertype() {
        return sertype;
    }

    /**
     * @param sertype
     */
    public void setSertype(Short sertype) {
        this.sertype = sertype;
    }

    /**
     * @return JJDPK
     */
    public String getJjdpk() {
        return jjdpk;
    }

    /**
     * @param jjdpk
     */
    public void setJjdpk(String jjdpk) {
        this.jjdpk = jjdpk == null ? null : jjdpk.trim();
    }

    /**
     * @return ZT
     */
    public Short getZt() {
        return zt;
    }

    /**
     * @param zt
     */
    public void setZt(Short zt) {
        this.zt = zt;
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", sertype=").append(sertype);
        sb.append(", jjdpk=").append(jjdpk);
        sb.append(", zt=").append(zt);
        sb.append(", rksj=").append(rksj);
        sb.append(", yxx=").append(yxx);
        sb.append("]");
        return sb.toString();
    }
}