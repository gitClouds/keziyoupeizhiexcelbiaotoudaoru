package com.unis.model.znzf;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

@Table(name = "TB_ZNZF_JLT")
public class Jlt {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "JJDPK")
    private String jjdpk;

    @Column(name = "SERTYPE")
    private Short sertype;

    @Column(name = "BANKCODE")
    private String bankcode;

    @Column(name = "BANKNAME")
    private String bankname;

    @Column(name = "ACCOUNTNAME")
    private String accountname;

    @Column(name = "CARDNUMBER")
    private String cardnumber;

    @Column(name = "NLEVEL")
    private Short nlevel;

    @Column(name = "JTYPE")
    private Short jtype;

    @Column(name = "PARENTPK")
    private String parentpk;

    @Column(name = "PARENTZH")
    private String parentzh;

    @Column(name = "ZFZT")
    private Short zfzt;

    @Column(name = "ZFPK")
    private String zfpk;

    @Column(name = "DJZT")
    private Short djzt;

    @Column(name = "DJPK")
    private String djpk;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private Short yxx;
    @Column(name = "BZ")
    private String bz;

    @Column(name = "CARDID")
    private String cardid;
    @Column(name = "PHONE")
    private String phone;

    @Column(name = "QZHZT")
    private Short qzhzt;

    @Column(name = "QZHPK")
    private String qzhpk;

    @Column(name = "CBFX")
    private Short cbfx;

    @Column(name = "DISAB")
    private Short disab;

    @Column(name = "PARENTISLSH")
    private Short parentislsh;

    @Column(name = "ISZCYGZS")
    private Short iszcygzs;

    @Column(name = "ISSHOW")
    private Short isshow;

    @Column(name = "KEEPON")
    private Short keepon;

    @Column(name = "FIRSTDATE")
    private Date firstDate;

    @Column(name = "ZZCS")
    private Short zzcs;

    @Column(name = "ZJE")
    private BigDecimal zje;

    @Column(name = "FXBS")
    private Short fxbs;

    @Column(name = "XSKBS")
    private Short xskbs;
    
    @Column(name = "MXQQPK")
    private String mxqqpk;
    
    @Column(name = "ZTQQPK")
    private String ztqqpk;
    
    @Column(name = "XDRYPK")
    private String xdrypk;
    
    @Column(name = "SDRYPK")
    private String sdrypk;

    @Column(name = "JLTFXBS")
    private Short jltfxbs;

    @Transient
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date begin_time;

    @Transient
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date end_time;

    @Transient
    private Integer money;

    @Transient
    private List<String> pklist;

    public List<String> getPklist() {
        return pklist;
    }

    public void setPklist(List<String> pklist) {
        this.pklist = pklist;
    }

    public Short getJltfxbs() {
        return jltfxbs;
    }

    public void setJltfxbs(Short jltfxbs) {
        this.jltfxbs = jltfxbs;
    }

    public Short getFxbs() {
        return fxbs;
    }

    public void setFxbs(Short fxbs) {
        this.fxbs = fxbs;
    }

    public Short getXskbs() {
        return xskbs;
    }

    public void setXskbs(Short xskbs) {
        this.xskbs = xskbs;
    }

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return JJDPK
     */
    public String getJjdpk() {
        return jjdpk;
    }

    /**
     * @param jjdpk
     */
    public void setJjdpk(String jjdpk) {
        this.jjdpk = jjdpk == null ? null : jjdpk.trim();
    }

    /**
     * @return SERTYPE
     */
    public Short getSertype() {
        return sertype;
    }

    /**
     * @param sertype
     */
    public void setSertype(Short sertype) {
        this.sertype = sertype;
    }

    /**
     * @return BANKCODE
     */
    public String getBankcode() {
        return bankcode;
    }

    /**
     * @param bankcode
     */
    public void setBankcode(String bankcode) {
        this.bankcode = bankcode == null ? null : bankcode.trim();
    }

    /**
     * @return BANKNAME
     */
    public String getBankname() {
        return bankname;
    }

    /**
     * @param bankname
     */
    public void setBankname(String bankname) {
        this.bankname = bankname == null ? null : bankname.trim();
    }

    /**
     * @return ACCOUNTNAME
     */
    public String getAccountname() {
        return accountname;
    }

    /**
     * @param accountname
     */
    public void setAccountname(String accountname) {
        this.accountname = accountname == null ? null : accountname.trim();
    }

    /**
     * @return CARDNUMBER
     */
    public String getCardnumber() {
        return cardnumber;
    }

    /**
     * @param cardnumber
     */
    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber == null ? null : cardnumber.trim();
    }

    /**
     * @return NLEVEL
     */
    public Short getNlevel() {
        return nlevel;
    }

    /**
     * @param nlevel
     */
    public void setNlevel(Short nlevel) {
        this.nlevel = nlevel;
    }

    /**
     * @return JTYPE
     */
    public Short getJtype() {
        return jtype;
    }

    /**
     * @param jtype
     */
    public void setJtype(Short jtype) {
        this.jtype = jtype;
    }

    /**
     * @return PARENTPK
     */
    public String getParentpk() {
        return parentpk;
    }

    /**
     * @param parentpk
     */
    public void setParentpk(String parentpk) {
        this.parentpk = parentpk == null ? null : parentpk.trim();
    }

    /**
     * @return PARENTZH
     */
    public String getParentzh() {
        return parentzh;
    }

    /**
     * @param parentzh
     */
    public void setParentzh(String parentzh) {
        this.parentzh = parentzh == null ? null : parentzh.trim();
    }

    /**
     * @return ZFZT
     */
    public Short getZfzt() {
        return zfzt;
    }

    /**
     * @param zfzt
     */
    public void setZfzt(Short zfzt) {
        this.zfzt = zfzt;
    }

    /**
     * @return ZFPK
     */
    public String getZfpk() {
        return zfpk;
    }

    /**
     * @param zfpk
     */
    public void setZfpk(String zfpk) {
        this.zfpk = zfpk == null ? null : zfpk.trim();
    }

    /**
     * @return DJZT
     */
    public Short getDjzt() {
        return djzt;
    }

    /**
     * @param djzt
     */
    public void setDjzt(Short djzt) {
        this.djzt = djzt;
    }

    /**
     * @return DJPK
     */
    public String getDjpk() {
        return djpk;
    }

    /**
     * @param djpk
     */
    public void setDjpk(String djpk) {
        this.djpk = djpk == null ? null : djpk.trim();
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }


    public String getBz() {
        return bz;
    }

    public Jlt setBz(String bz) {
        this.bz = bz;
        return this;
    }

    public String getCardid() {
        return cardid;
    }

    public Jlt setCardid(String cardid) {
        this.cardid = cardid;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public Jlt setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public Short getQzhzt() {
        return qzhzt;
    }

    public Jlt setQzhzt(Short qzhzt) {
        this.qzhzt = qzhzt;
        return this;
    }

    public String getQzhpk() {
        return qzhpk;
    }

    public Jlt setQzhpk(String qzhpk) {
        this.qzhpk = qzhpk;
        return this;
    }

    public Short getCbfx() {
        return cbfx;
    }

    public void setCbfx(Short cbfx) {
        this.cbfx = cbfx;
    }

    public Short getDisab() {
        return disab;
    }

    public void setDisab(Short disab) {
        this.disab = disab;
    }

    public Short getParentislsh() {
        return parentislsh;
    }

    public void setParentislsh(Short parentislsh) {
        this.parentislsh = parentislsh;
    }

    public Short getIszcygzs() {
        return iszcygzs;
    }

    public void setIszcygzs(Short iszcygzs) {
        this.iszcygzs = iszcygzs;
    }

    public Short getIsshow() {
        return isshow;
    }

    public void setIsshow(Short isshow) {
        this.isshow = isshow;
    }

    public Short getKeepon() {
        return keepon;
    }
    public void setKeepon(Short keepon) {
        this.keepon = keepon;
    }

    public Date getFirstDate() {
        return firstDate;
    }

    public Jlt setFirstDate(Date firstDate) {
        this.firstDate = firstDate;
        return this;
    }

    public Short getZzcs() {
        return zzcs;
    }

    public Jlt setZzcs(Short zzcs) {
        this.zzcs = zzcs;
        return this;
    }

    public BigDecimal getZje() {
        return zje;
    }

    public Jlt setZje(BigDecimal zje) {
        this.zje = zje;
        return this;
    }

    public String getMxqqpk() {
		return mxqqpk;
	}

	public void setMxqqpk(String mxqqpk) {
		this.mxqqpk = mxqqpk;
	}

	public String getZtqqpk() {
		return ztqqpk;
	}

	public void setZtqqpk(String ztqqpk) {
		this.ztqqpk = ztqqpk;
	}

	public String getXdrypk() {
		return xdrypk;
	}

	public void setXdrypk(String xdrypk) {
		this.xdrypk = xdrypk;
	}

	public String getSdrypk() {
		return sdrypk;
	}

	public void setSdrypk(String sdrypk) {
		this.sdrypk = sdrypk;
	}

    public Date getBegin_time() {
        return begin_time;
    }

    public void setBegin_time(Date begin_time) {
        this.begin_time = begin_time;
    }

    public Date getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Date end_time) {
        this.end_time = end_time;
    }

    public Integer getMoney() {
        return money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }

    @Override
    public String toString() {
        return "Jlt{" +
                "pk='" + pk + '\'' +
                ", jjdpk='" + jjdpk + '\'' +
                ", sertype=" + sertype +
                ", bankcode='" + bankcode + '\'' +
                ", bankname='" + bankname + '\'' +
                ", accountname='" + accountname + '\'' +
                ", cardnumber='" + cardnumber + '\'' +
                ", nlevel=" + nlevel +
                ", jtype=" + jtype +
                ", parentpk='" + parentpk + '\'' +
                ", parentzh='" + parentzh + '\'' +
                ", zfzt=" + zfzt +
                ", zfpk='" + zfpk + '\'' +
                ", djzt=" + djzt +
                ", djpk='" + djpk + '\'' +
                ", rksj=" + rksj +
                ", yxx=" + yxx +
                ", bz='" + bz + '\'' +
                ", cardid='" + cardid + '\'' +
                ", phone='" + phone + '\'' +
                ", qzhzt=" + qzhzt +
                ", qzhpk='" + qzhpk + '\'' +
                ", cbfx=" + cbfx +
                ", disab=" + disab +
                ", parentislsh=" + parentislsh +
                ", iszcygzs=" + iszcygzs +
                ", isshow=" + isshow +
                ", keepon=" + keepon +
                ", firstDate=" + firstDate +
                ", zzcs=" + zzcs +
                ", zje=" + zje +
                ", fxbs=" + fxbs +
                ", xskbs=" + xskbs +
                ", mxqqpk='" + mxqqpk + '\'' +
                ", ztqqpk='" + ztqqpk + '\'' +
                ", xdrypk='" + xdrypk + '\'' +
                ", sdrypk='" + sdrypk + '\'' +
                ", jltfxbs=" + jltfxbs +
                ", begin_time=" + begin_time +
                ", end_time=" + end_time +
                ", money=" + money +
                ", pklist=" + pklist +
                '}';
    }
}