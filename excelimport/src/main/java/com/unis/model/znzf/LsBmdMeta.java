package com.unis.model.znzf;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_ZNZF_LS_BMD_META")
public class LsBmdMeta {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "BANKCODE")
    private String bankcode;

    @Column(name = "PAYCODE")
    private String paycode;

    @Column(name = "CHECKVALUE")
    private String checkvalue;

    @Column(name = "POSITIONVAL")
    private Short positionval;

    @Column(name = "YESORNO")
    private Short yesorno;

    @Column(name = "YXX")
    private Short yxx;

    @Column(name = "RKSJ")
    private Date rksj;
    
    @Column(name = "ZH")
    private String zh;
    
    @Column(name = "BANKNAME")
    private String bankname;
    
    @Column(name = "PAYNAME")
    private String payname;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return BANKCODE
     */
    public String getBankcode() {
        return bankcode;
    }

    /**
     * @param bankcode
     */
    public void setBankcode(String bankcode) {
        this.bankcode = bankcode == null ? null : bankcode.trim();
    }

    /**
     * @return PAYCODE
     */
    public String getPaycode() {
        return paycode;
    }

    /**
     * @param paycode
     */
    public void setPaycode(String paycode) {
        this.paycode = paycode == null ? null : paycode.trim();
    }

    /**
     * @return CHECKVALUE
     */
    public String getCheckvalue() {
        return checkvalue;
    }

    /**
     * @param checkvalue
     */
    public void setCheckvalue(String checkvalue) {
        this.checkvalue = checkvalue == null ? null : checkvalue.trim();
    }

    /**
     * @return POSITIONVAL
     */
    public Short getPositionval() {
        return positionval;
    }

    /**
     * @param positionval
     */
    public void setPositionval(Short positionval) {
        this.positionval = positionval;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    public Short getYesorno() {
        return yesorno;
    }

    public void setYesorno(Short yesorno) {
        this.yesorno = yesorno;
    }

    public String getZh() {
		return zh;
	}

	public void setZh(String zh) {
		this.zh = zh;
	}

	public String getBankname() {
		return bankname;
	}

	public void setBankname(String bankname) {
		this.bankname = bankname;
	}

	public String getPayname() {
		return payname;
	}

	public void setPayname(String payname) {
		this.payname = payname;
	}

	@Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", bankcode=").append(bankcode);
        sb.append(", paycode=").append(paycode);
        sb.append(", checkvalue=").append(checkvalue);
        sb.append(", positionval=").append(positionval);
        sb.append(", yesorno=").append(yesorno);
        sb.append(", yxx=").append(yxx);
        sb.append(", rksj=").append(rksj);
        sb.append("]");
        return sb.toString();
    }
}