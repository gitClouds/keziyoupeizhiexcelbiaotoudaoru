package com.unis.model.znzf;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_ZNZF_CB_MAC_GL")
public class CbMacGl {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "CBBH")
    private String cbbh;

    @Column(name = "SERTYPE")
    private Short sertype;

    @Column(name = "QQPK")
    private String qqpk;

    @Column(name = "JJDPK")
    private String jjdpk;

    @Column(name = "MACDZ")
    private String macdz;

    @Column(name = "QQZH")
    private String qqzh;

    @Column(name = "QQZHXM")
    private String qqzhxm;

    @Column(name = "QQZHJGDM")
    private String qqzhjgdm;

    @Column(name = "QQZHJGMC")
    private String qqzhjgmc;

    @Column(name = "NLEVEL")
    private String nlevel;

    @Column(name = "JJDHM")
    private String jjdhm;

    @Column(name = "BARXM")
    private String barxm;

    @Column(name = "SLDWDM")
    private String sldwdm;

    @Column(name = "SLDWMC")
    private String sldwmc;

    @Column(name = "AFSJ")
    private Date afsj;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private Short yxx;

    public String getNlevel() {
        return nlevel;
    }

    public void setNlevel(String nlevel) {
        this.nlevel = nlevel;
    }

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return CBBH
     */
    public String getCbbh() {
        return cbbh;
    }

    /**
     * @param cbbh
     */
    public void setCbbh(String cbbh) {
        this.cbbh = cbbh == null ? null : cbbh.trim();
    }

    /**
     * @return SERTYPE
     */
    public Short getSertype() {
        return sertype;
    }

    /**
     * @param sertype
     */
    public void setSertype(Short sertype) {
        this.sertype = sertype;
    }

    /**
     * @return QQPK
     */
    public String getQqpk() {
        return qqpk;
    }

    /**
     * @param qqpk
     */
    public void setQqpk(String qqpk) {
        this.qqpk = qqpk == null ? null : qqpk.trim();
    }

    /**
     * @return JJDPK
     */
    public String getJjdpk() {
        return jjdpk;
    }

    /**
     * @param jjdpk
     */
    public void setJjdpk(String jjdpk) {
        this.jjdpk = jjdpk == null ? null : jjdpk.trim();
    }

    /**
     * @return MACDZ
     */
    public String getMacdz() {
        return macdz;
    }

    /**
     * @param macdz
     */
    public void setMacdz(String macdz) {
        this.macdz = macdz == null ? null : macdz.trim();
    }

    /**
     * @return QQZH
     */
    public String getQqzh() {
        return qqzh;
    }

    /**
     * @param qqzh
     */
    public void setQqzh(String qqzh) {
        this.qqzh = qqzh == null ? null : qqzh.trim();
    }

    /**
     * @return QQZHXM
     */
    public String getQqzhxm() {
        return qqzhxm;
    }

    /**
     * @param qqzhxm
     */
    public void setQqzhxm(String qqzhxm) {
        this.qqzhxm = qqzhxm == null ? null : qqzhxm.trim();
    }

    /**
     * @return QQZHJGDM
     */
    public String getQqzhjgdm() {
        return qqzhjgdm;
    }

    /**
     * @param qqzhjgdm
     */
    public void setQqzhjgdm(String qqzhjgdm) {
        this.qqzhjgdm = qqzhjgdm == null ? null : qqzhjgdm.trim();
    }

    /**
     * @return QQZHJGMC
     */
    public String getQqzhjgmc() {
        return qqzhjgmc;
    }

    /**
     * @param qqzhjgmc
     */
    public void setQqzhjgmc(String qqzhjgmc) {
        this.qqzhjgmc = qqzhjgmc == null ? null : qqzhjgmc.trim();
    }

    /**
     * @return JJDHM
     */
    public String getJjdhm() {
        return jjdhm;
    }

    /**
     * @param jjdhm
     */
    public void setJjdhm(String jjdhm) {
        this.jjdhm = jjdhm == null ? null : jjdhm.trim();
    }

    /**
     * @return BARXM
     */
    public String getBarxm() {
        return barxm;
    }

    /**
     * @param barxm
     */
    public void setBarxm(String barxm) {
        this.barxm = barxm == null ? null : barxm.trim();
    }

    /**
     * @return SLDWDM
     */
    public String getSldwdm() {
        return sldwdm;
    }

    /**
     * @param sldwdm
     */
    public void setSldwdm(String sldwdm) {
        this.sldwdm = sldwdm == null ? null : sldwdm.trim();
    }

    /**
     * @return SLDWMC
     */
    public String getSldwmc() {
        return sldwmc;
    }

    /**
     * @param sldwmc
     */
    public void setSldwmc(String sldwmc) {
        this.sldwmc = sldwmc == null ? null : sldwmc.trim();
    }

    /**
     * @return AFSJ
     */
    public Date getAfsj() {
        return afsj;
    }

    /**
     * @param afsj
     */
    public void setAfsj(Date afsj) {
        this.afsj = afsj;
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", cbbh=").append(cbbh);
        sb.append(", sertype=").append(sertype);
        sb.append(", qqpk=").append(qqpk);
        sb.append(", jjdpk=").append(jjdpk);
        sb.append(", macdz=").append(macdz);
        sb.append(", qqzh=").append(qqzh);
        sb.append(", qqzhxm=").append(qqzhxm);
        sb.append(", qqzhjgdm=").append(qqzhjgdm);
        sb.append(", qqzhjgmc=").append(qqzhjgmc);
        sb.append(", jjdhm=").append(jjdhm);
        sb.append(", barxm=").append(barxm);
        sb.append(", sldwdm=").append(sldwdm);
        sb.append(", sldwmc=").append(sldwmc);
        sb.append(", afsj=").append(afsj);
        sb.append(", rksj=").append(rksj);
        sb.append(", yxx=").append(yxx);
        sb.append("]");
        return sb.toString();
    }
}