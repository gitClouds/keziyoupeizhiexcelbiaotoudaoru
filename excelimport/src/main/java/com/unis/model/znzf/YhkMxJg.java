package com.unis.model.znzf;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_ZNZF_YHK_MX_JG")
public class YhkMxJg {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "QQPK")
    private String qqpk;

    @Column(name = "JJDPK")
    private String jjdpk;

    @Column(name = "APPLICATIONID")
    private String applicationid;

    @Column(name = "RESULTCODE")
    private String resultcode;

    @Column(name = "OPERATORNAME")
    private String operatorname;

    @Column(name = "OPERATORPHONE")
    private String operatorphone;

    @Column(name = "FEEDBACKORGNAME")
    private String feedbackorgname;

    @Column(name = "FEEDBACKREMARK")
    private String feedbackremark;

    @Column(name = "ACCOUNTNAME")
    private String accountname;

    @Column(name = "CARDNUMBER")
    private String cardnumber;

    @Column(name = "DEPBANKBRANCH")
    private String depbankbranch;

    @Column(name = "DEPBANKBRANCHCODE")
    private String depbankbranchcode;

    @Column(name = "ACCOPENTIME")
    private Date accopentime;

    @Column(name = "ACCCANTNTIME")
    private Date acccantntime;

    @Column(name = "ACCCANTNBRANCH")
    private String acccantnbranch;

    @Column(name = "REMARK")
    private String remark;

    @Column(name = "ACCNUMBER")
    private String accnumber;

    @Column(name = "ACCSERIAL")
    private String accserial;

    @Column(name = "ACCTYPE")
    private String acctype;

    @Column(name = "ACCSTATUS")
    private String accstatus;

    @Column(name = "ACCBALANCE")
    private BigDecimal accbalance;

    @Column(name = "AVAILABLEBALANCE")
    private BigDecimal availablebalance;

    @Column(name = "LASTTRANSACTIONTIME")
    private Date lasttransactiontime;

    @Column(name = "FKSJ")
    private Date fksj;

    @Column(name = "CURRENCY")
    private String currency;

    @Column(name = "CASHREMIT")
    private String cashremit;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private Short yxx;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return QQPK
     */
    public String getQqpk() {
        return qqpk;
    }

    /**
     * @param qqpk
     */
    public void setQqpk(String qqpk) {
        this.qqpk = qqpk == null ? null : qqpk.trim();
    }

    /**
     * @return JJDPK
     */
    public String getJjdpk() {
        return jjdpk;
    }

    /**
     * @param jjdpk
     */
    public void setJjdpk(String jjdpk) {
        this.jjdpk = jjdpk == null ? null : jjdpk.trim();
    }

    /**
     * @return APPLICATIONID
     */
    public String getApplicationid() {
        return applicationid;
    }

    /**
     * @param applicationid
     */
    public void setApplicationid(String applicationid) {
        this.applicationid = applicationid == null ? null : applicationid.trim();
    }

    /**
     * @return RESULTCODE
     */
    public String getResultcode() {
        return resultcode;
    }

    /**
     * @param resultcode
     */
    public void setResultcode(String resultcode) {
        this.resultcode = resultcode == null ? null : resultcode.trim();
    }

    /**
     * @return OPERATORNAME
     */
    public String getOperatorname() {
        return operatorname;
    }

    /**
     * @param operatorname
     */
    public void setOperatorname(String operatorname) {
        this.operatorname = operatorname == null ? null : operatorname.trim();
    }

    /**
     * @return OPERATORPHONE
     */
    public String getOperatorphone() {
        return operatorphone;
    }

    /**
     * @param operatorphone
     */
    public void setOperatorphone(String operatorphone) {
        this.operatorphone = operatorphone == null ? null : operatorphone.trim();
    }

    /**
     * @return FEEDBACKORGNAME
     */
    public String getFeedbackorgname() {
        return feedbackorgname;
    }

    /**
     * @param feedbackorgname
     */
    public void setFeedbackorgname(String feedbackorgname) {
        this.feedbackorgname = feedbackorgname == null ? null : feedbackorgname.trim();
    }

    /**
     * @return FEEDBACKREMARK
     */
    public String getFeedbackremark() {
        return feedbackremark;
    }

    /**
     * @param feedbackremark
     */
    public void setFeedbackremark(String feedbackremark) {
        this.feedbackremark = feedbackremark == null ? null : feedbackremark.trim();
    }

    /**
     * @return ACCOUNTNAME
     */
    public String getAccountname() {
        return accountname;
    }

    /**
     * @param accountname
     */
    public void setAccountname(String accountname) {
        this.accountname = accountname == null ? null : accountname.trim();
    }

    /**
     * @return CARDNUMBER
     */
    public String getCardnumber() {
        return cardnumber;
    }

    /**
     * @param cardnumber
     */
    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber == null ? null : cardnumber.trim();
    }

    /**
     * @return DEPBANKBRANCH
     */
    public String getDepbankbranch() {
        return depbankbranch;
    }

    /**
     * @param depbankbranch
     */
    public void setDepbankbranch(String depbankbranch) {
        this.depbankbranch = depbankbranch == null ? null : depbankbranch.trim();
    }

    /**
     * @return DEPBANKBRANCHCODE
     */
    public String getDepbankbranchcode() {
        return depbankbranchcode;
    }

    /**
     * @param depbankbranchcode
     */
    public void setDepbankbranchcode(String depbankbranchcode) {
        this.depbankbranchcode = depbankbranchcode == null ? null : depbankbranchcode.trim();
    }

    /**
     * @return ACCOPENTIME
     */
    public Date getAccopentime() {
        return accopentime;
    }

    /**
     * @param accopentime
     */
    public void setAccopentime(Date accopentime) {
        this.accopentime = accopentime;
    }

    /**
     * @return ACCCANTNTIME
     */
    public Date getAcccantntime() {
        return acccantntime;
    }

    /**
     * @param acccantntime
     */
    public void setAcccantntime(Date acccantntime) {
        this.acccantntime = acccantntime;
    }

    /**
     * @return ACCCANTNBRANCH
     */
    public String getAcccantnbranch() {
        return acccantnbranch;
    }

    /**
     * @param acccantnbranch
     */
    public void setAcccantnbranch(String acccantnbranch) {
        this.acccantnbranch = acccantnbranch == null ? null : acccantnbranch.trim();
    }

    /**
     * @return REMARK
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * @return ACCNUMBER
     */
    public String getAccnumber() {
        return accnumber;
    }

    /**
     * @param accnumber
     */
    public void setAccnumber(String accnumber) {
        this.accnumber = accnumber == null ? null : accnumber.trim();
    }

    /**
     * @return ACCSERIAL
     */
    public String getAccserial() {
        return accserial;
    }

    /**
     * @param accserial
     */
    public void setAccserial(String accserial) {
        this.accserial = accserial == null ? null : accserial.trim();
    }

    /**
     * @return ACCTYPE
     */
    public String getAcctype() {
        return acctype;
    }

    /**
     * @param acctype
     */
    public void setAcctype(String acctype) {
        this.acctype = acctype == null ? null : acctype.trim();
    }

    /**
     * @return ACCSTATUS
     */
    public String getAccstatus() {
        return accstatus;
    }

    /**
     * @param accstatus
     */
    public void setAccstatus(String accstatus) {
        this.accstatus = accstatus == null ? null : accstatus.trim();
    }

    /**
     * @return ACCBALANCE
     */
    public BigDecimal getAccbalance() {
        return accbalance;
    }

    /**
     * @param accbalance
     */
    public void setAccbalance(BigDecimal accbalance) {
        this.accbalance = accbalance;
    }

    /**
     * @return AVAILABLEBALANCE
     */
    public BigDecimal getAvailablebalance() {
        return availablebalance;
    }

    /**
     * @param availablebalance
     */
    public void setAvailablebalance(BigDecimal availablebalance) {
        this.availablebalance = availablebalance;
    }

    /**
     * @return LASTTRANSACTIONTIME
     */
    public Date getLasttransactiontime() {
        return lasttransactiontime;
    }

    /**
     * @param lasttransactiontime
     */
    public void setLasttransactiontime(Date lasttransactiontime) {
        this.lasttransactiontime = lasttransactiontime;
    }

    /**
     * @return FKSJ
     */
    public Date getFksj() {
        return fksj;
    }

    /**
     * @param fksj
     */
    public void setFksj(Date fksj) {
        this.fksj = fksj;
    }

    /**
     * @return CURRENCY
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency
     */
    public void setCurrency(String currency) {
        this.currency = currency == null ? null : currency.trim();
    }

    /**
     * @return CASHREMIT
     */
    public String getCashremit() {
        return cashremit;
    }

    /**
     * @param cashremit
     */
    public void setCashremit(String cashremit) {
        this.cashremit = cashremit == null ? null : cashremit.trim();
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", qqpk=").append(qqpk);
        sb.append(", jjdpk=").append(jjdpk);
        sb.append(", applicationid=").append(applicationid);
        sb.append(", resultcode=").append(resultcode);
        sb.append(", operatorname=").append(operatorname);
        sb.append(", operatorphone=").append(operatorphone);
        sb.append(", feedbackorgname=").append(feedbackorgname);
        sb.append(", feedbackremark=").append(feedbackremark);
        sb.append(", accountname=").append(accountname);
        sb.append(", cardnumber=").append(cardnumber);
        sb.append(", depbankbranch=").append(depbankbranch);
        sb.append(", depbankbranchcode=").append(depbankbranchcode);
        sb.append(", accopentime=").append(accopentime);
        sb.append(", acccantntime=").append(acccantntime);
        sb.append(", acccantnbranch=").append(acccantnbranch);
        sb.append(", remark=").append(remark);
        sb.append(", accnumber=").append(accnumber);
        sb.append(", accserial=").append(accserial);
        sb.append(", acctype=").append(acctype);
        sb.append(", accstatus=").append(accstatus);
        sb.append(", accbalance=").append(accbalance);
        sb.append(", availablebalance=").append(availablebalance);
        sb.append(", lasttransactiontime=").append(lasttransactiontime);
        sb.append(", fksj=").append(fksj);
        sb.append(", currency=").append(currency);
        sb.append(", cashremit=").append(cashremit);
        sb.append(", rksj=").append(rksj);
        sb.append(", yxx=").append(yxx);
        sb.append("]");
        return sb.toString();
    }
}