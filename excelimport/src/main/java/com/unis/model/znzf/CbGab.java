package com.unis.model.znzf;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_ZNZF_CB_GAB")
public class CbGab {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "SERTYPE")
    private Short sertype;

    @Column(name = "JJDPK")
    private String jjdpk;

    @Column(name = "YWSQBH")
    private String ywsqbh;

    @Column(name = "BANKCODE")
    private String bankcode;

    @Column(name = "CARDNUMBER")
    private String cardnumber;

    @Column(name = "CARDNAME")
    private String cardname;

    @Column(name = "LRDWDM")
    private String lrdwdm;

    @Column(name = "LRSJ")
    private Date lrsj;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private Short yxx;

    @Column(name = "SY")
    private String sy;

    @Column(name = "COUNT")
    private Integer count;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return SERTYPE
     */
    public Short getSertype() {
        return sertype;
    }

    /**
     * @param sertype
     */
    public void setSertype(Short sertype) {
        this.sertype = sertype;
    }

    /**
     * @return JJDPK
     */
    public String getJjdpk() {
        return jjdpk;
    }

    /**
     * @param jjdpk
     */
    public void setJjdpk(String jjdpk) {
        this.jjdpk = jjdpk == null ? null : jjdpk.trim();
    }

    /**
     * @return YWSQBH
     */
    public String getYwsqbh() {
        return ywsqbh;
    }

    /**
     * @param ywsqbh
     */
    public void setYwsqbh(String ywsqbh) {
        this.ywsqbh = ywsqbh == null ? null : ywsqbh.trim();
    }

    /**
     * @return BANKCODE
     */
    public String getBankcode() {
        return bankcode;
    }

    /**
     * @param bankcode
     */
    public void setBankcode(String bankcode) {
        this.bankcode = bankcode == null ? null : bankcode.trim();
    }

    /**
     * @return CARDNUMBER
     */
    public String getCardnumber() {
        return cardnumber;
    }

    /**
     * @param cardnumber
     */
    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber == null ? null : cardnumber.trim();
    }

    /**
     * @return CARDNAME
     */
    public String getCardname() {
        return cardname;
    }

    /**
     * @param cardname
     */
    public void setCardname(String cardname) {
        this.cardname = cardname == null ? null : cardname.trim();
    }

    /**
     * @return LRDWDM
     */
    public String getLrdwdm() {
        return lrdwdm;
    }

    /**
     * @param lrdwdm
     */
    public void setLrdwdm(String lrdwdm) {
        this.lrdwdm = lrdwdm == null ? null : lrdwdm.trim();
    }

    /**
     * @return LRSJ
     */
    public Date getLrsj() {
        return lrsj;
    }

    /**
     * @param lrsj
     */
    public void setLrsj(Date lrsj) {
        this.lrsj = lrsj;
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    /**
     * @return SY
     */
    public String getSy() {
        return sy;
    }

    /**
     * @param sy
     */
    public void setSy(String sy) {
        this.sy = sy == null ? null : sy.trim();
    }

    @Override
    public String toString() {
        return "CbGab{" +
                "pk='" + pk + '\'' +
                ", sertype=" + sertype +
                ", jjdpk='" + jjdpk + '\'' +
                ", ywsqbh='" + ywsqbh + '\'' +
                ", bankcode='" + bankcode + '\'' +
                ", cardnumber='" + cardnumber + '\'' +
                ", cardname='" + cardname + '\'' +
                ", lrdwdm='" + lrdwdm + '\'' +
                ", lrsj=" + lrsj +
                ", rksj=" + rksj +
                ", yxx=" + yxx +
                ", sy='" + sy + '\'' +
                ", count=" + count +
                '}';
    }
}