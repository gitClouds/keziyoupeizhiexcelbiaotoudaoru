package com.unis.model.znzf;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_ZNZF_SF_MX_JG")
public class SfMxJg {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "QQPK")
    private String qqpk;

    @Column(name = "JJDPK")
    private String jjdpk;

    @Column(name = "APPLICATIONID")
    private String applicationid;

    @Column(name = "RESULTCODE")
    private String resultcode;

    @Column(name = "SUBJECTTYPE")
    private String subjecttype;

    @Column(name = "ACCOUNTNUMBER")
    private String accountnumber;

    @Column(name = "ACCOUNTTYPE")
    private String accounttype;

    @Column(name = "ACCOUNTSTATUS")
    private String accountstatus;

    @Column(name = "CURRENCY")
    private String currency;

    @Column(name = "ACCOUNTBALANCE")
    private BigDecimal accountbalance;

    @Column(name = "ACCOUNTINFO")
    private String accountinfo;

    @Column(name = "ACCOUNTOPENTIME")
    private Date accountopentime;

    @Column(name = "ACCOUNTOPENIP")
    private String accountopenip;

    @Column(name = "ACCOUNTOPENMAC")
    private String accountopenmac;

    @Column(name = "LASTTRANSACTIONTIME")
    private Date lasttransactiontime;

    @Column(name = "FEEDBACKREMARK")
    private String feedbackremark;

    @Column(name = "FEEDBACKORGNAME")
    private String feedbackorgname;

    @Column(name = "OPERATORNAME")
    private String operatorname;

    @Column(name = "OPERATORPHONENUMBER")
    private String operatorphonenumber;

    @Column(name = "REMARK")
    private String remark;

    @Column(name = "FKSJ")
    private Date fksj;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private Short yxx;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return QQPK
     */
    public String getQqpk() {
        return qqpk;
    }

    /**
     * @param qqpk
     */
    public void setQqpk(String qqpk) {
        this.qqpk = qqpk == null ? null : qqpk.trim();
    }

    /**
     * @return JJDPK
     */
    public String getJjdpk() {
        return jjdpk;
    }

    /**
     * @param jjdpk
     */
    public void setJjdpk(String jjdpk) {
        this.jjdpk = jjdpk == null ? null : jjdpk.trim();
    }

    /**
     * @return APPLICATIONID
     */
    public String getApplicationid() {
        return applicationid;
    }

    /**
     * @param applicationid
     */
    public void setApplicationid(String applicationid) {
        this.applicationid = applicationid == null ? null : applicationid.trim();
    }

    /**
     * @return RESULTCODE
     */
    public String getResultcode() {
        return resultcode;
    }

    /**
     * @param resultcode
     */
    public void setResultcode(String resultcode) {
        this.resultcode = resultcode == null ? null : resultcode.trim();
    }

    /**
     * @return SUBJECTTYPE
     */
    public String getSubjecttype() {
        return subjecttype;
    }

    /**
     * @param subjecttype
     */
    public void setSubjecttype(String subjecttype) {
        this.subjecttype = subjecttype == null ? null : subjecttype.trim();
    }

    /**
     * @return ACCOUNTNUMBER
     */
    public String getAccountnumber() {
        return accountnumber;
    }

    /**
     * @param accountnumber
     */
    public void setAccountnumber(String accountnumber) {
        this.accountnumber = accountnumber == null ? null : accountnumber.trim();
    }

    /**
     * @return ACCOUNTTYPE
     */
    public String getAccounttype() {
        return accounttype;
    }

    /**
     * @param accounttype
     */
    public void setAccounttype(String accounttype) {
        this.accounttype = accounttype == null ? null : accounttype.trim();
    }

    /**
     * @return ACCOUNTSTATUS
     */
    public String getAccountstatus() {
        return accountstatus;
    }

    /**
     * @param accountstatus
     */
    public void setAccountstatus(String accountstatus) {
        this.accountstatus = accountstatus == null ? null : accountstatus.trim();
    }

    /**
     * @return CURRENCY
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency
     */
    public void setCurrency(String currency) {
        this.currency = currency == null ? null : currency.trim();
    }

    /**
     * @return ACCOUNTBALANCE
     */
    public BigDecimal getAccountbalance() {
        return accountbalance;
    }

    /**
     * @param accountbalance
     */
    public void setAccountbalance(BigDecimal accountbalance) {
        this.accountbalance = accountbalance;
    }

    /**
     * @return ACCOUNTINFO
     */
    public String getAccountinfo() {
        return accountinfo;
    }

    /**
     * @param accountinfo
     */
    public void setAccountinfo(String accountinfo) {
        this.accountinfo = accountinfo == null ? null : accountinfo.trim();
    }

    /**
     * @return ACCOUNTOPENTIME
     */
    public Date getAccountopentime() {
        return accountopentime;
    }

    /**
     * @param accountopentime
     */
    public void setAccountopentime(Date accountopentime) {
        this.accountopentime = accountopentime;
    }

    /**
     * @return ACCOUNTOPENIP
     */
    public String getAccountopenip() {
        return accountopenip;
    }

    /**
     * @param accountopenip
     */
    public void setAccountopenip(String accountopenip) {
        this.accountopenip = accountopenip == null ? null : accountopenip.trim();
    }

    /**
     * @return ACCOUNTOPENMAC
     */
    public String getAccountopenmac() {
        return accountopenmac;
    }

    /**
     * @param accountopenmac
     */
    public void setAccountopenmac(String accountopenmac) {
        this.accountopenmac = accountopenmac == null ? null : accountopenmac.trim();
    }

    /**
     * @return LASTTRANSACTIONTIME
     */
    public Date getLasttransactiontime() {
        return lasttransactiontime;
    }

    /**
     * @param lasttransactiontime
     */
    public void setLasttransactiontime(Date lasttransactiontime) {
        this.lasttransactiontime = lasttransactiontime;
    }

    /**
     * @return FEEDBACKREMARK
     */
    public String getFeedbackremark() {
        return feedbackremark;
    }

    /**
     * @param feedbackremark
     */
    public void setFeedbackremark(String feedbackremark) {
        this.feedbackremark = feedbackremark == null ? null : feedbackremark.trim();
    }

    /**
     * @return FEEDBACKORGNAME
     */
    public String getFeedbackorgname() {
        return feedbackorgname;
    }

    /**
     * @param feedbackorgname
     */
    public void setFeedbackorgname(String feedbackorgname) {
        this.feedbackorgname = feedbackorgname == null ? null : feedbackorgname.trim();
    }

    /**
     * @return OPERATORNAME
     */
    public String getOperatorname() {
        return operatorname;
    }

    /**
     * @param operatorname
     */
    public void setOperatorname(String operatorname) {
        this.operatorname = operatorname == null ? null : operatorname.trim();
    }

    /**
     * @return OPERATORPHONENUMBER
     */
    public String getOperatorphonenumber() {
        return operatorphonenumber;
    }

    /**
     * @param operatorphonenumber
     */
    public void setOperatorphonenumber(String operatorphonenumber) {
        this.operatorphonenumber = operatorphonenumber == null ? null : operatorphonenumber.trim();
    }

    /**
     * @return REMARK
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * @return FKSJ
     */
    public Date getFksj() {
        return fksj;
    }

    /**
     * @param fksj
     */
    public void setFksj(Date fksj) {
        this.fksj = fksj;
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", qqpk=").append(qqpk);
        sb.append(", jjdpk=").append(jjdpk);
        sb.append(", applicationid=").append(applicationid);
        sb.append(", resultcode=").append(resultcode);
        sb.append(", subjecttype=").append(subjecttype);
        sb.append(", accountnumber=").append(accountnumber);
        sb.append(", accounttype=").append(accounttype);
        sb.append(", accountstatus=").append(accountstatus);
        sb.append(", currency=").append(currency);
        sb.append(", accountbalance=").append(accountbalance);
        sb.append(", accountinfo=").append(accountinfo);
        sb.append(", accountopentime=").append(accountopentime);
        sb.append(", accountopenip=").append(accountopenip);
        sb.append(", accountopenmac=").append(accountopenmac);
        sb.append(", lasttransactiontime=").append(lasttransactiontime);
        sb.append(", feedbackremark=").append(feedbackremark);
        sb.append(", feedbackorgname=").append(feedbackorgname);
        sb.append(", operatorname=").append(operatorname);
        sb.append(", operatorphonenumber=").append(operatorphonenumber);
        sb.append(", remark=").append(remark);
        sb.append(", fksj=").append(fksj);
        sb.append(", rksj=").append(rksj);
        sb.append(", yxx=").append(yxx);
        sb.append("]");
        return sb.toString();
    }
}