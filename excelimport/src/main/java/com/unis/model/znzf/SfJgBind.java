package com.unis.model.znzf;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_ZNZF_SF_JG_BIND")
public class SfJgBind {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "APPLICATIONID")
    private String applicationid;

    @Column(name = "DATATYPE")
    private String datatype;

    @Column(name = "BINDINGDATA")
    private String bindingdata;

    @Column(name = "FKSJ")
    private Date fksj;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private Short yxx;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return APPLICATIONID
     */
    public String getApplicationid() {
        return applicationid;
    }

    /**
     * @param applicationid
     */
    public void setApplicationid(String applicationid) {
        this.applicationid = applicationid == null ? null : applicationid.trim();
    }

    /**
     * @return DATATYPE
     */
    public String getDatatype() {
        return datatype;
    }

    /**
     * @param datatype
     */
    public void setDatatype(String datatype) {
        this.datatype = datatype == null ? null : datatype.trim();
    }

    /**
     * @return BINDINGDATA
     */
    public String getBindingdata() {
        return bindingdata;
    }

    /**
     * @param bindingdata
     */
    public void setBindingdata(String bindingdata) {
        this.bindingdata = bindingdata == null ? null : bindingdata.trim();
    }

    /**
     * @return FKSJ
     */
    public Date getFksj() {
        return fksj;
    }

    /**
     * @param fksj
     */
    public void setFksj(Date fksj) {
        this.fksj = fksj;
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", applicationid=").append(applicationid);
        sb.append(", datatype=").append(datatype);
        sb.append(", bindingdata=").append(bindingdata);
        sb.append(", fksj=").append(fksj);
        sb.append(", rksj=").append(rksj);
        sb.append(", yxx=").append(yxx);
        sb.append("]");
        return sb.toString();
    }
}