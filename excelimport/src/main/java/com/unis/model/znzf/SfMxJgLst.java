package com.unis.model.znzf;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_ZNZF_SF_MX_JG_LIST")
public class SfMxJgLst {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "QQPK")
    private String qqpk;

    @Column(name = "JJDPK")
    private String jjdpk;

    @Column(name = "APPLICATIONID")
    private String applicationid;

    @Column(name = "ZFDDH")
    private String zfddh;

    @Column(name = "JYLX")
    private String jylx;

    @Column(name = "ZFLX")
    private String zflx;

    @Column(name = "JDBZ")
    private String jdbz;

    @Column(name = "JYSJ")
    private String jysj;

    @Column(name = "RMBZL")
    private String rmbzl;

    @Column(name = "JYJE")
    private String jyje;

    @Column(name = "JYLSH")
    private String jylsh;

    @Column(name = "YE")
    private String ye;

    @Column(name = "SKYHJGDM")
    private String skyhjgdm;

    @Column(name = "SKYHJGMC")
    private String skyhjgmc;

    @Column(name = "SKYHZH")
    private String skyhzh;

    @Column(name = "SKZFZH")
    private String skzfzh;

    @Column(name = "POSSBH")
    private String possbh;

    @Column(name = "SKSHM")
    private String skshm;

    @Column(name = "FKYHJGDM")
    private String fkyhjgdm;

    @Column(name = "FKYHJGMC")
    private String fkyhjgmc;

    @Column(name = "FKYHZH")
    private String fkyhzh;

    @Column(name = "FKZFZH")
    private String fkzfzh;

    @Column(name = "JYSBLX")
    private String jysblx;

    @Column(name = "ZFSBIP")
    private String zfsbip;

    @Column(name = "IPXZ")
    private String ipxz;

    @Column(name = "MACDZ")
    private String macdz;

    @Column(name = "JYDDJD")
    private String jyddjd;

    @Column(name = "JYDDWD")
    private String jyddwd;

    @Column(name = "JYSBH")
    private String jysbh;

    @Column(name = "YHWBLSH")
    private String yhwblsh;

    @Column(name = "BZ")
    private String bz;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private Short yxx;

    @Column(name = "SKFSHMC")
    private String skfshmc;

    public String getSkfshmc() {
        return skfshmc;
    }

    public void setSkfshmc(String skfshmc) {
        this.skfshmc = skfshmc;
    }

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return QQPK
     */
    public String getQqpk() {
        return qqpk;
    }

    /**
     * @param qqpk
     */
    public void setQqpk(String qqpk) {
        this.qqpk = qqpk == null ? null : qqpk.trim();
    }

    /**
     * @return JJDPK
     */
    public String getJjdpk() {
        return jjdpk;
    }

    /**
     * @param jjdpk
     */
    public void setJjdpk(String jjdpk) {
        this.jjdpk = jjdpk == null ? null : jjdpk.trim();
    }

    /**
     * @return APPLICATIONID
     */
    public String getApplicationid() {
        return applicationid;
    }

    /**
     * @param applicationid
     */
    public void setApplicationid(String applicationid) {
        this.applicationid = applicationid == null ? null : applicationid.trim();
    }

    /**
     * @return ZFDDH
     */
    public String getZfddh() {
        return zfddh;
    }

    /**
     * @param zfddh
     */
    public void setZfddh(String zfddh) {
        this.zfddh = zfddh == null ? null : zfddh.trim();
    }

    /**
     * @return JYLX
     */
    public String getJylx() {
        return jylx;
    }

    /**
     * @param jylx
     */
    public void setJylx(String jylx) {
        this.jylx = jylx == null ? null : jylx.trim();
    }

    /**
     * @return ZFLX
     */
    public String getZflx() {
        return zflx;
    }

    /**
     * @param zflx
     */
    public void setZflx(String zflx) {
        this.zflx = zflx == null ? null : zflx.trim();
    }

    /**
     * @return JDBZ
     */
    public String getJdbz() {
        return jdbz;
    }

    /**
     * @param jdbz
     */
    public void setJdbz(String jdbz) {
        this.jdbz = jdbz == null ? null : jdbz.trim();
    }

    /**
     * @return JYSJ
     */
    public String getJysj() {
        return jysj;
    }

    /**
     * @param jysj
     */
    public void setJysj(String jysj) {
        this.jysj = jysj == null ? null : jysj.trim();
    }

    /**
     * @return RMBZL
     */
    public String getRmbzl() {
        return rmbzl;
    }

    /**
     * @param rmbzl
     */
    public void setRmbzl(String rmbzl) {
        this.rmbzl = rmbzl == null ? null : rmbzl.trim();
    }

    /**
     * @return JYJE
     */
    public String getJyje() {
        return jyje;
    }

    /**
     * @param jyje
     */
    public void setJyje(String jyje) {
        this.jyje = jyje == null ? null : jyje.trim();
    }

    /**
     * @return JYLSH
     */
    public String getJylsh() {
        return jylsh;
    }

    /**
     * @param jylsh
     */
    public void setJylsh(String jylsh) {
        this.jylsh = jylsh == null ? null : jylsh.trim();
    }

    /**
     * @return YE
     */
    public String getYe() {
        return ye;
    }

    /**
     * @param ye
     */
    public void setYe(String ye) {
        this.ye = ye == null ? null : ye.trim();
    }

    /**
     * @return SKYHJGDM
     */
    public String getSkyhjgdm() {
        return skyhjgdm;
    }

    /**
     * @param skyhjgdm
     */
    public void setSkyhjgdm(String skyhjgdm) {
        this.skyhjgdm = skyhjgdm == null ? null : skyhjgdm.trim();
    }

    /**
     * @return SKYHJGMC
     */
    public String getSkyhjgmc() {
        return skyhjgmc;
    }

    /**
     * @param skyhjgmc
     */
    public void setSkyhjgmc(String skyhjgmc) {
        this.skyhjgmc = skyhjgmc == null ? null : skyhjgmc.trim();
    }

    /**
     * @return SKYHZH
     */
    public String getSkyhzh() {
        return skyhzh;
    }

    /**
     * @param skyhzh
     */
    public void setSkyhzh(String skyhzh) {
        this.skyhzh = skyhzh == null ? null : skyhzh.trim();
    }

    /**
     * @return SKZFZH
     */
    public String getSkzfzh() {
        return skzfzh;
    }

    /**
     * @param skzfzh
     */
    public void setSkzfzh(String skzfzh) {
        this.skzfzh = skzfzh == null ? null : skzfzh.trim();
    }

    /**
     * @return POSSBH
     */
    public String getPossbh() {
        return possbh;
    }

    /**
     * @param possbh
     */
    public void setPossbh(String possbh) {
        this.possbh = possbh == null ? null : possbh.trim();
    }

    /**
     * @return SKSHM
     */
    public String getSkshm() {
        return skshm;
    }

    /**
     * @param skshm
     */
    public void setSkshm(String skshm) {
        this.skshm = skshm == null ? null : skshm.trim();
    }

    /**
     * @return FKYHJGDM
     */
    public String getFkyhjgdm() {
        return fkyhjgdm;
    }

    /**
     * @param fkyhjgdm
     */
    public void setFkyhjgdm(String fkyhjgdm) {
        this.fkyhjgdm = fkyhjgdm == null ? null : fkyhjgdm.trim();
    }

    /**
     * @return FKYHJGMC
     */
    public String getFkyhjgmc() {
        return fkyhjgmc;
    }

    /**
     * @param fkyhjgmc
     */
    public void setFkyhjgmc(String fkyhjgmc) {
        this.fkyhjgmc = fkyhjgmc == null ? null : fkyhjgmc.trim();
    }

    /**
     * @return FKYHZH
     */
    public String getFkyhzh() {
        return fkyhzh;
    }

    /**
     * @param fkyhzh
     */
    public void setFkyhzh(String fkyhzh) {
        this.fkyhzh = fkyhzh == null ? null : fkyhzh.trim();
    }

    /**
     * @return FKZFZH
     */
    public String getFkzfzh() {
        return fkzfzh;
    }

    /**
     * @param fkzfzh
     */
    public void setFkzfzh(String fkzfzh) {
        this.fkzfzh = fkzfzh == null ? null : fkzfzh.trim();
    }

    /**
     * @return JYSBLX
     */
    public String getJysblx() {
        return jysblx;
    }

    /**
     * @param jysblx
     */
    public void setJysblx(String jysblx) {
        this.jysblx = jysblx == null ? null : jysblx.trim();
    }

    /**
     * @return ZFSBIP
     */
    public String getZfsbip() {
        return zfsbip;
    }

    /**
     * @param zfsbip
     */
    public void setZfsbip(String zfsbip) {
        this.zfsbip = zfsbip == null ? null : zfsbip.trim();
    }

    /**
     * @return MACDZ
     */
    public String getMacdz() {
        return macdz;
    }

    /**
     * @param macdz
     */
    public void setMacdz(String macdz) {
        this.macdz = macdz == null ? null : macdz.trim();
    }

    /**
     * @return JYDDJD
     */
    public String getJyddjd() {
        return jyddjd;
    }

    /**
     * @param jyddjd
     */
    public void setJyddjd(String jyddjd) {
        this.jyddjd = jyddjd == null ? null : jyddjd.trim();
    }

    /**
     * @return JYDDWD
     */
    public String getJyddwd() {
        return jyddwd;
    }

    /**
     * @param jyddwd
     */
    public void setJyddwd(String jyddwd) {
        this.jyddwd = jyddwd == null ? null : jyddwd.trim();
    }

    /**
     * @return JYSBH
     */
    public String getJysbh() {
        return jysbh;
    }

    /**
     * @param jysbh
     */
    public void setJysbh(String jysbh) {
        this.jysbh = jysbh == null ? null : jysbh.trim();
    }

    /**
     * @return YHWBLSH
     */
    public String getYhwblsh() {
        return yhwblsh;
    }

    /**
     * @param yhwblsh
     */
    public void setYhwblsh(String yhwblsh) {
        this.yhwblsh = yhwblsh == null ? null : yhwblsh.trim();
    }

    /**
     * @return BZ
     */
    public String getBz() {
        return bz;
    }

    /**
     * @param bz
     */
    public void setBz(String bz) {
        this.bz = bz == null ? null : bz.trim();
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    public String getIpxz() {
        return ipxz;
    }

    public void setIpxz(String ipxz) {
        this.ipxz = ipxz;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", qqpk=").append(qqpk);
        sb.append(", jjdpk=").append(jjdpk);
        sb.append(", applicationid=").append(applicationid);
        sb.append(", zfddh=").append(zfddh);
        sb.append(", jylx=").append(jylx);
        sb.append(", zflx=").append(zflx);
        sb.append(", jdbz=").append(jdbz);
        sb.append(", jysj=").append(jysj);
        sb.append(", rmbzl=").append(rmbzl);
        sb.append(", jyje=").append(jyje);
        sb.append(", jylsh=").append(jylsh);
        sb.append(", ye=").append(ye);
        sb.append(", skyhjgdm=").append(skyhjgdm);
        sb.append(", skyhjgmc=").append(skyhjgmc);
        sb.append(", skyhzh=").append(skyhzh);
        sb.append(", skzfzh=").append(skzfzh);
        sb.append(", possbh=").append(possbh);
        sb.append(", skshm=").append(skshm);
        sb.append(", fkyhjgdm=").append(fkyhjgdm);
        sb.append(", fkyhjgmc=").append(fkyhjgmc);
        sb.append(", fkyhzh=").append(fkyhzh);
        sb.append(", fkzfzh=").append(fkzfzh);
        sb.append(", jysblx=").append(jysblx);
        sb.append(", zfsbip=").append(zfsbip);
        sb.append(", ipxz=").append(ipxz);
        sb.append(", macdz=").append(macdz);
        sb.append(", jyddjd=").append(jyddjd);
        sb.append(", jyddwd=").append(jyddwd);
        sb.append(", jysbh=").append(jysbh);
        sb.append(", yhwblsh=").append(yhwblsh);
        sb.append(", bz=").append(bz);
        sb.append(", rksj=").append(rksj);
        sb.append(", yxx=").append(yxx);
        sb.append("]");
        return sb.toString();
    }
}