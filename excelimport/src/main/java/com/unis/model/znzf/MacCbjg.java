package com.unis.model.znzf;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_ZNZF_MAC_CBJG")
public class MacCbjg {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "SERTYPE")
    private Short sertype;

    @Column(name = "ZHJGDM")
    private String zhjgdm;

    @Column(name = "ZHJGMC")
    private String zhjgmc;

    @Column(name = "THZF")
    private String thzf;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private Short yxx;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return SERTYPE
     */
    public Short getSertype() {
        return sertype;
    }

    /**
     * @param sertype
     */
    public void setSertype(Short sertype) {
        this.sertype = sertype;
    }

    /**
     * @return ZHJGDM
     */
    public String getZhjgdm() {
        return zhjgdm;
    }

    /**
     * @param zhjgdm
     */
    public void setZhjgdm(String zhjgdm) {
        this.zhjgdm = zhjgdm == null ? null : zhjgdm.trim();
    }

    /**
     * @return ZHJGMC
     */
    public String getZhjgmc() {
        return zhjgmc;
    }

    /**
     * @param zhjgmc
     */
    public void setZhjgmc(String zhjgmc) {
        this.zhjgmc = zhjgmc == null ? null : zhjgmc.trim();
    }

    /**
     * @return THZF
     */
    public String getThzf() {
        return thzf;
    }

    /**
     * @param thzf
     */
    public void setThzf(String thzf) {
        this.thzf = thzf == null ? null : thzf.trim();
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", sertype=").append(sertype);
        sb.append(", zhjgdm=").append(zhjgdm);
        sb.append(", zhjgmc=").append(zhjgmc);
        sb.append(", thzf=").append(thzf);
        sb.append(", rksj=").append(rksj);
        sb.append(", yxx=").append(yxx);
        sb.append("]");
        return sb.toString();
    }
}