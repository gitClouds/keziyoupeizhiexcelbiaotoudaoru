package com.unis.model.znzf;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_ZNZF_YHK_MX_JG_LIST")
public class YhkMxJgLst {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "QQPK")
    private String qqpk;

    @Column(name = "JJDPK")
    private String jjdpk;

    @Column(name = "APPLICATIONID")
    private String applicationid;

    @Column(name = "JYLX")
    private String jylx;

    @Column(name = "JDBZ")
    private String jdbz;

    @Column(name = "JYJE")
    private BigDecimal jyje;

    @Column(name = "JYYE")
    private BigDecimal jyye;

    @Column(name = "JYSJ")
    private Date jysj;

    @Column(name = "JYLSH")
    private String jylsh;

    @Column(name = "JYDFMC")
    private String jydfmc;

    @Column(name = "JYDFZH")
    private String jydfzh;

    @Column(name = "JYDFZJHM")
    private String jydfzjhm;

    @Column(name = "JYDFKHH")
    private String jydfkhh;

    @Column(name = "JYSFCG")
    private String jysfcg;

    @Column(name = "XJBZ")
    private String xjbz;

    @Column(name = "JYWDMC")
    private String jywdmc;

    @Column(name = "JYWDDM")
    private String jywddm;

    @Column(name = "JYZY")
    private String jyzy;

    @Column(name = "IPDZ")
    private String ipdz;

    @Column(name = "IPXZ")
    private String ipxz;

    @Column(name = "MACDZ")
    private String macdz;

    @Column(name = "RZH")
    private String rzh;

    @Column(name = "CPH")
    private String cph;

    @Column(name = "PZH")
    private String pzh;

    @Column(name = "ZDH")
    private String zdh;

    @Column(name = "JYGYH")
    private String jygyh;

    @Column(name = "SHMC")
    private String shmc;

    @Column(name = "SHH")
    private String shh;

    @Column(name = "RMBZL")
    private String rmbzl;

    @Column(name = "BZ")
    private String bz;

    @Column(name = "PZZL")
    private String pzzl;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private Short yxx;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return QQPK
     */
    public String getQqpk() {
        return qqpk;
    }

    /**
     * @param qqpk
     */
    public void setQqpk(String qqpk) {
        this.qqpk = qqpk == null ? null : qqpk.trim();
    }

    /**
     * @return JJDPK
     */
    public String getJjdpk() {
        return jjdpk;
    }

    /**
     * @param jjdpk
     */
    public void setJjdpk(String jjdpk) {
        this.jjdpk = jjdpk == null ? null : jjdpk.trim();
    }

    /**
     * @return APPLICATIONID
     */
    public String getApplicationid() {
        return applicationid;
    }

    /**
     * @param applicationid
     */
    public void setApplicationid(String applicationid) {
        this.applicationid = applicationid == null ? null : applicationid.trim();
    }

    /**
     * @return JYLX
     */
    public String getJylx() {
        return jylx;
    }

    /**
     * @param jylx
     */
    public void setJylx(String jylx) {
        this.jylx = jylx == null ? null : jylx.trim();
    }

    /**
     * @return JDBZ
     */
    public String getJdbz() {
        return jdbz;
    }

    /**
     * @param jdbz
     */
    public void setJdbz(String jdbz) {
        this.jdbz = jdbz == null ? null : jdbz.trim();
    }

    /**
     * @return JYJE
     */
    public BigDecimal getJyje() {
        return jyje;
    }

    /**
     * @param jyje
     */
    public void setJyje(BigDecimal jyje) {
        this.jyje = jyje;
    }

    /**
     * @return JYYE
     */
    public BigDecimal getJyye() {
        return jyye;
    }

    /**
     * @param jyye
     */
    public void setJyye(BigDecimal jyye) {
        this.jyye = jyye;
    }

    /**
     * @return JYSJ
     */
    public Date getJysj() {
        return jysj;
    }

    /**
     * @param jysj
     */
    public void setJysj(Date jysj) {
        this.jysj = jysj;
    }

    /**
     * @return JYLSH
     */
    public String getJylsh() {
        return jylsh;
    }

    /**
     * @param jylsh
     */
    public void setJylsh(String jylsh) {
        this.jylsh = jylsh == null ? null : jylsh.trim();
    }

    /**
     * @return JYDFMC
     */
    public String getJydfmc() {
        return jydfmc;
    }

    /**
     * @param jydfmc
     */
    public void setJydfmc(String jydfmc) {
        this.jydfmc = jydfmc == null ? null : jydfmc.trim();
    }

    /**
     * @return JYDFZH
     */
    public String getJydfzh() {
        return jydfzh;
    }

    /**
     * @param jydfzh
     */
    public void setJydfzh(String jydfzh) {
        this.jydfzh = jydfzh == null ? null : jydfzh.trim();
    }

    /**
     * @return JYDFZJHM
     */
    public String getJydfzjhm() {
        return jydfzjhm;
    }

    /**
     * @param jydfzjhm
     */
    public void setJydfzjhm(String jydfzjhm) {
        this.jydfzjhm = jydfzjhm == null ? null : jydfzjhm.trim();
    }

    /**
     * @return JYDFKHH
     */
    public String getJydfkhh() {
        return jydfkhh;
    }

    /**
     * @param jydfkhh
     */
    public void setJydfkhh(String jydfkhh) {
        this.jydfkhh = jydfkhh == null ? null : jydfkhh.trim();
    }

    /**
     * @return JYSFCG
     */
    public String getJysfcg() {
        return jysfcg;
    }

    /**
     * @param jysfcg
     */
    public void setJysfcg(String jysfcg) {
        this.jysfcg = jysfcg == null ? null : jysfcg.trim();
    }

    /**
     * @return XJBZ
     */
    public String getXjbz() {
        return xjbz;
    }

    /**
     * @param xjbz
     */
    public void setXjbz(String xjbz) {
        this.xjbz = xjbz == null ? null : xjbz.trim();
    }

    /**
     * @return JYWDMC
     */
    public String getJywdmc() {
        return jywdmc;
    }

    /**
     * @param jywdmc
     */
    public void setJywdmc(String jywdmc) {
        this.jywdmc = jywdmc == null ? null : jywdmc.trim();
    }

    /**
     * @return JYWDDM
     */
    public String getJywddm() {
        return jywddm;
    }

    /**
     * @param jywddm
     */
    public void setJywddm(String jywddm) {
        this.jywddm = jywddm == null ? null : jywddm.trim();
    }

    /**
     * @return JYZY
     */
    public String getJyzy() {
        return jyzy;
    }

    /**
     * @param jyzy
     */
    public void setJyzy(String jyzy) {
        this.jyzy = jyzy == null ? null : jyzy.trim();
    }

    /**
     * @return IPDZ
     */
    public String getIpdz() {
        return ipdz;
    }

    /**
     * @param ipdz
     */
    public void setIpdz(String ipdz) {
        this.ipdz = ipdz == null ? null : ipdz.trim();
    }

    /**
     * @return MACDZ
     */
    public String getMacdz() {
        return macdz;
    }

    /**
     * @param macdz
     */
    public void setMacdz(String macdz) {
        this.macdz = macdz == null ? null : macdz.trim();
    }

    /**
     * @return RZH
     */
    public String getRzh() {
        return rzh;
    }

    /**
     * @param rzh
     */
    public void setRzh(String rzh) {
        this.rzh = rzh == null ? null : rzh.trim();
    }

    /**
     * @return CPH
     */
    public String getCph() {
        return cph;
    }

    /**
     * @param cph
     */
    public void setCph(String cph) {
        this.cph = cph == null ? null : cph.trim();
    }

    /**
     * @return PZH
     */
    public String getPzh() {
        return pzh;
    }

    /**
     * @param pzh
     */
    public void setPzh(String pzh) {
        this.pzh = pzh == null ? null : pzh.trim();
    }

    /**
     * @return ZDH
     */
    public String getZdh() {
        return zdh;
    }

    /**
     * @param zdh
     */
    public void setZdh(String zdh) {
        this.zdh = zdh == null ? null : zdh.trim();
    }

    /**
     * @return JYGYH
     */
    public String getJygyh() {
        return jygyh;
    }

    /**
     * @param jygyh
     */
    public void setJygyh(String jygyh) {
        this.jygyh = jygyh == null ? null : jygyh.trim();
    }

    /**
     * @return SHMC
     */
    public String getShmc() {
        return shmc;
    }

    /**
     * @param shmc
     */
    public void setShmc(String shmc) {
        this.shmc = shmc == null ? null : shmc.trim();
    }

    /**
     * @return SHH
     */
    public String getShh() {
        return shh;
    }

    /**
     * @param shh
     */
    public void setShh(String shh) {
        this.shh = shh == null ? null : shh.trim();
    }

    /**
     * @return RMBZL
     */
    public String getRmbzl() {
        return rmbzl;
    }

    /**
     * @param rmbzl
     */
    public void setRmbzl(String rmbzl) {
        this.rmbzl = rmbzl == null ? null : rmbzl.trim();
    }

    /**
     * @return BZ
     */
    public String getBz() {
        return bz;
    }

    /**
     * @param bz
     */
    public void setBz(String bz) {
        this.bz = bz == null ? null : bz.trim();
    }

    /**
     * @return PZZL
     */
    public String getPzzl() {
        return pzzl;
    }

    /**
     * @param pzzl
     */
    public void setPzzl(String pzzl) {
        this.pzzl = pzzl == null ? null : pzzl.trim();
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    public String getIpxz() {
        return ipxz;
    }

    public void setIpxz(String ipxz) {
        this.ipxz = ipxz;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", qqpk=").append(qqpk);
        sb.append(", jjdpk=").append(jjdpk);
        sb.append(", applicationid=").append(applicationid);
        sb.append(", jylx=").append(jylx);
        sb.append(", jdbz=").append(jdbz);
        sb.append(", jyje=").append(jyje);
        sb.append(", jyye=").append(jyye);
        sb.append(", jysj=").append(jysj);
        sb.append(", jylsh=").append(jylsh);
        sb.append(", jydfmc=").append(jydfmc);
        sb.append(", jydfzh=").append(jydfzh);
        sb.append(", jydfzjhm=").append(jydfzjhm);
        sb.append(", jydfkhh=").append(jydfkhh);
        sb.append(", jysfcg=").append(jysfcg);
        sb.append(", xjbz=").append(xjbz);
        sb.append(", jywdmc=").append(jywdmc);
        sb.append(", jywddm=").append(jywddm);
        sb.append(", jyzy=").append(jyzy);
        sb.append(", ipdz=").append(ipdz);
        sb.append(", ipxz=").append(ipxz);
        sb.append(", macdz=").append(macdz);
        sb.append(", rzh=").append(rzh);
        sb.append(", cph=").append(cph);
        sb.append(", pzh=").append(pzh);
        sb.append(", zdh=").append(zdh);
        sb.append(", jygyh=").append(jygyh);
        sb.append(", shmc=").append(shmc);
        sb.append(", shh=").append(shh);
        sb.append(", rmbzl=").append(rmbzl);
        sb.append(", bz=").append(bz);
        sb.append(", pzzl=").append(pzzl);
        sb.append(", rksj=").append(rksj);
        sb.append(", yxx=").append(yxx);
        sb.append("]");
        return sb.toString();
    }
}