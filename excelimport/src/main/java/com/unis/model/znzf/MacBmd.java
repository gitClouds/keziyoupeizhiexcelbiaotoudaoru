package com.unis.model.znzf;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_ZNZF_MAC_BMD")
public class MacBmd {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "SERTYPE")
    private Short sertype;

    @Column(name = "BMDLX")
    private Short bmdlx;

    @Column(name = "ZH")
    private String zh;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private Short yxx;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return SERTYPE
     */
    public Short getSertype() {
        return sertype;
    }

    /**
     * @param sertype
     */
    public void setSertype(Short sertype) {
        this.sertype = sertype;
    }

    /**
     * @return BMDLX
     */
    public Short getBmdlx() {
        return bmdlx;
    }

    /**
     * @param bmdlx
     */
    public void setBmdlx(Short bmdlx) {
        this.bmdlx = bmdlx;
    }

    /**
     * @return ZH
     */
    public String getZh() {
        return zh;
    }

    /**
     * @param zh
     */
    public void setZh(String zh) {
        this.zh = zh == null ? null : zh.trim();
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", sertype=").append(sertype);
        sb.append(", bmdlx=").append(bmdlx);
        sb.append(", zh=").append(zh);
        sb.append(", rksj=").append(rksj);
        sb.append(", yxx=").append(yxx);
        sb.append("]");
        return sb.toString();
    }
}