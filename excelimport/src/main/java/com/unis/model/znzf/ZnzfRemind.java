package com.unis.model.znzf;

import javax.persistence.*;

@Table(name = "TB_ZNZF_REMIND")
public class ZnzfRemind {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "JJDPK")
    private String jjdpk;

    @Column(name = "JLTPK")
    private String jltpk;

    @Column(name = "CARDNUMBER")
    private String cardnumber;

    @Column(name = "REMINDTYPE")
    private Short remindtype;

    @Column(name = "REMARK")
    private String remark;

    @Column(name = "MEASURESPK")
    private String measurespk;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return JJDPK
     */
    public String getJjdpk() {
        return jjdpk;
    }

    /**
     * @param jjdpk
     */
    public void setJjdpk(String jjdpk) {
        this.jjdpk = jjdpk == null ? null : jjdpk.trim();
    }

    /**
     * @return CARDNUMBER
     */
    public String getCardnumber() {
        return cardnumber;
    }

    /**
     * @param cardnumber
     */
    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber == null ? null : cardnumber.trim();
    }

    /**
     * @return REMINDTYPE
     */
    public Short getRemindtype() {
        return remindtype;
    }

    /**
     * @param remindtype
     */
    public void setRemindtype(Short remindtype) {
        this.remindtype = remindtype;
    }

    /**
     * @return REMARK
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * @return MEASURESPK
     */
    public String getMeasurespk() {
        return measurespk;
    }

    /**
     * @param measurespk
     */
    public void setMeasurespk(String measurespk) {
        this.measurespk = measurespk == null ? null : measurespk.trim();
    }

    public String getJltpk() {
        return jltpk;
    }

    public void setJltpk(String jltpk) {
        this.jltpk = jltpk;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", jjdpk=").append(jjdpk);
        sb.append(", jltpk=").append(jltpk);
        sb.append(", cardnumber=").append(cardnumber);
        sb.append(", remindtype=").append(remindtype);
        sb.append(", remark=").append(remark);
        sb.append(", measurespk=").append(measurespk);
        sb.append("]");
        return sb.toString();
    }
}