package com.unis.model.znzf;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_ZNZF_YHK_ZF_QQ")
public class YhkZfQq {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "JJDPK")
    private String jjdpk;

    @Column(name = "APPLICATIONID")
    private String applicationid;

    @Column(name = "SUBJECTTYPE")
    private Short subjecttype;

    @Column(name = "BANKCODE")
    private String bankcode;

    @Column(name = "BANKNAME")
    private String bankname;

    @Column(name = "ACCOUNTNAME")
    private String accountname;

    @Column(name = "CARDNUMBER")
    private String cardnumber;

    @Column(name = "TRANSFERTIME")
    private Date transfertime;

    @Column(name = "TRANSFERAMOUNT")
    private Long transferamount;

    @Column(name = "REASON")
    private String reason;

    @Column(name = "LRDWDM")
    private String lrdwdm;

    @Column(name = "LRDWMC")
    private String lrdwmc;

    @Column(name = "LRRXM")
    private String lrrxm;

    @Column(name = "LRRJH")
    private String lrrjh;

    @Column(name = "STARTTIME")
    private Date starttime;

    @Column(name = "EXPIRETIME")
    private Date expiretime;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private Short yxx;

    @Column(name = "REQFLAG")
    private Short reqflag;

    @Column(name = "REQDATE")
    private Date reqdate;

    @Column(name = "REQCOUNT")
    private Short reqcount;

    @Column(name = "RESFLAG")
    private Short resflag;

    @Column(name = "RESDATE")
    private Date resdate;

    @Column(name = "RESULTCODE")
    private String resultcode;

    @Column(name = "FEEDBACKREMARK")
    private String feedbackremark;

    @Column(name = "FLWS")
    private String flws;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return JJDPK
     */
    public String getJjdpk() {
        return jjdpk;
    }

    /**
     * @param jjdpk
     */
    public void setJjdpk(String jjdpk) {
        this.jjdpk = jjdpk == null ? null : jjdpk.trim();
    }

    /**
     * @return APPLICATIONID
     */
    public String getApplicationid() {
        return applicationid;
    }

    /**
     * @param applicationid
     */
    public void setApplicationid(String applicationid) {
        this.applicationid = applicationid == null ? null : applicationid.trim();
    }

    /**
     * @return SUBJECTTYPE
     */
    public Short getSubjecttype() {
        return subjecttype;
    }

    /**
     * @param subjecttype
     */
    public void setSubjecttype(Short subjecttype) {
        this.subjecttype = subjecttype;
    }

    /**
     * @return BANKCODE
     */
    public String getBankcode() {
        return bankcode;
    }

    /**
     * @param bankcode
     */
    public void setBankcode(String bankcode) {
        this.bankcode = bankcode == null ? null : bankcode.trim();
    }

    /**
     * @return BANKNAME
     */
    public String getBankname() {
        return bankname;
    }

    /**
     * @param bankname
     */
    public void setBankname(String bankname) {
        this.bankname = bankname == null ? null : bankname.trim();
    }

    /**
     * @return ACCOUNTNAME
     */
    public String getAccountname() {
        return accountname;
    }

    /**
     * @param accountname
     */
    public void setAccountname(String accountname) {
        this.accountname = accountname == null ? null : accountname.trim();
    }

    /**
     * @return CARDNUMBER
     */
    public String getCardnumber() {
        return cardnumber;
    }

    /**
     * @param cardnumber
     */
    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber == null ? null : cardnumber.trim();
    }

    /**
     * @return TRANSFERTIME
     */
    public Date getTransfertime() {
        return transfertime;
    }

    /**
     * @param transfertime
     */
    public void setTransfertime(Date transfertime) {
        this.transfertime = transfertime;
    }

    /**
     * @return TRANSFERAMOUNT
     */
    public Long getTransferamount() {
        return transferamount;
    }

    /**
     * @param transferamount
     */
    public void setTransferamount(Long transferamount) {
        this.transferamount = transferamount;
    }

    /**
     * @return REASON
     */
    public String getReason() {
        return reason;
    }

    /**
     * @param reason
     */
    public void setReason(String reason) {
        this.reason = reason == null ? null : reason.trim();
    }

    /**
     * @return LRDWDM
     */
    public String getLrdwdm() {
        return lrdwdm;
    }

    /**
     * @param lrdwdm
     */
    public void setLrdwdm(String lrdwdm) {
        this.lrdwdm = lrdwdm == null ? null : lrdwdm.trim();
    }

    /**
     * @return LRDWMC
     */
    public String getLrdwmc() {
        return lrdwmc;
    }

    /**
     * @param lrdwmc
     */
    public void setLrdwmc(String lrdwmc) {
        this.lrdwmc = lrdwmc == null ? null : lrdwmc.trim();
    }

    /**
     * @return LRRXM
     */
    public String getLrrxm() {
        return lrrxm;
    }

    /**
     * @param lrrxm
     */
    public void setLrrxm(String lrrxm) {
        this.lrrxm = lrrxm == null ? null : lrrxm.trim();
    }

    /**
     * @return LRRJH
     */
    public String getLrrjh() {
        return lrrjh;
    }

    /**
     * @param lrrjh
     */
    public void setLrrjh(String lrrjh) {
        this.lrrjh = lrrjh == null ? null : lrrjh.trim();
    }

    /**
     * @return STARTTIME
     */
    public Date getStarttime() {
        return starttime;
    }

    /**
     * @param starttime
     */
    public void setStarttime(Date starttime) {
        this.starttime = starttime;
    }

    /**
     * @return EXPIRETIME
     */
    public Date getExpiretime() {
        return expiretime;
    }

    /**
     * @param expiretime
     */
    public void setExpiretime(Date expiretime) {
        this.expiretime = expiretime;
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    /**
     * @return REQFLAG
     */
    public Short getReqflag() {
        return reqflag;
    }

    /**
     * @param reqflag
     */
    public void setReqflag(Short reqflag) {
        this.reqflag = reqflag;
    }

    /**
     * @return REQDATE
     */
    public Date getReqdate() {
        return reqdate;
    }

    /**
     * @param reqdate
     */
    public void setReqdate(Date reqdate) {
        this.reqdate = reqdate;
    }

    /**
     * @return REQCOUNT
     */
    public Short getReqcount() {
        return reqcount;
    }

    /**
     * @param reqcount
     */
    public void setReqcount(Short reqcount) {
        this.reqcount = reqcount;
    }

    /**
     * @return RESFLAG
     */
    public Short getResflag() {
        return resflag;
    }

    /**
     * @param resflag
     */
    public void setResflag(Short resflag) {
        this.resflag = resflag;
    }

    /**
     * @return RESDATE
     */
    public Date getResdate() {
        return resdate;
    }

    /**
     * @param resdate
     */
    public void setResdate(Date resdate) {
        this.resdate = resdate;
    }

    /**
     * @return RESULTCODE
     */
    public String getResultcode() {
        return resultcode;
    }

    /**
     * @param resultcode
     */
    public void setResultcode(String resultcode) {
        this.resultcode = resultcode == null ? null : resultcode.trim();
    }

    /**
     * @return FEEDBACKREMARK
     */
    public String getFeedbackremark() {
        return feedbackremark;
    }

    /**
     * @param feedbackremark
     */
    public void setFeedbackremark(String feedbackremark) {
        this.feedbackremark = feedbackremark == null ? null : feedbackremark.trim();
    }

    /**
     * @return FLWS
     */
    public String getFlws() {
        return flws;
    }

    /**
     * @param flws
     */
    public void setFlws(String flws) {
        this.flws = flws == null ? null : flws.trim();
    }

    @Override
    public String toString() {
        return "YhkZfQq{" +
                "pk='" + pk + '\'' +
                ", jjdpk='" + jjdpk + '\'' +
                ", applicationid='" + applicationid + '\'' +
                ", subjecttype=" + subjecttype +
                ", bankcode='" + bankcode + '\'' +
                ", bankname='" + bankname + '\'' +
                ", accountname='" + accountname + '\'' +
                ", cardnumber='" + cardnumber + '\'' +
                ", transfertime=" + transfertime +
                ", transferamount=" + transferamount +
                ", reason='" + reason + '\'' +
                ", lrdwdm='" + lrdwdm + '\'' +
                ", lrdwmc='" + lrdwmc + '\'' +
                ", lrrxm='" + lrrxm + '\'' +
                ", lrrjh='" + lrrjh + '\'' +
                ", starttime=" + starttime +
                ", expiretime=" + expiretime +
                ", rksj=" + rksj +
                ", yxx=" + yxx +
                ", reqflag=" + reqflag +
                ", reqdate=" + reqdate +
                ", reqcount=" + reqcount +
                ", resflag=" + resflag +
                ", resdate=" + resdate +
                ", resultcode='" + resultcode + '\'' +
                ", feedbackremark='" + feedbackremark + '\'' +
                ", flws='" + flws + '\'' +
                '}';
    }
}