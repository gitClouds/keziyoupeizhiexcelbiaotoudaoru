package com.unis.model.znzf;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_ZNZF_CB")
public class Cb {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "CBBH")
    private String cbbh;

    @Column(name = "SERTYPE")
    private Short sertype;

    @Column(name = "BANKCODE")
    private String bankcode;

    @Column(name = "BANKNAME")
    private String bankname;

    @Column(name = "CARDNAME")
    private String cardname;

    @Column(name = "CARDNUMBER")
    private String cardnumber;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private Short yxx;

    @Column(name = "BZ")
    private String bz;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return CBBH
     */
    public String getCbbh() {
        return cbbh;
    }

    /**
     * @param cbbh
     */
    public void setCbbh(String cbbh) {
        this.cbbh = cbbh == null ? null : cbbh.trim();
    }

    /**
     * @return SERTYPE
     */
    public Short getSertype() {
        return sertype;
    }

    /**
     * @param sertype
     */
    public void setSertype(Short sertype) {
        this.sertype = sertype;
    }

    /**
     * @return BANKCODE
     */
    public String getBankcode() {
        return bankcode;
    }

    /**
     * @param bankcode
     */
    public void setBankcode(String bankcode) {
        this.bankcode = bankcode == null ? null : bankcode.trim();
    }

    /**
     * @return BANKNAME
     */
    public String getBankname() {
        return bankname;
    }

    /**
     * @param bankname
     */
    public void setBankname(String bankname) {
        this.bankname = bankname == null ? null : bankname.trim();
    }

    /**
     * @return CARDNAME
     */
    public String getCardname() {
        return cardname;
    }

    /**
     * @param cardname
     */
    public void setCardname(String cardname) {
        this.cardname = cardname == null ? null : cardname.trim();
    }

    /**
     * @return CARDNUMBER
     */
    public String getCardnumber() {
        return cardnumber;
    }

    /**
     * @param cardnumber
     */
    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber == null ? null : cardnumber.trim();
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    /**
     * @return BZ
     */
    public String getBz() {
        return bz;
    }

    /**
     * @param bz
     */
    public void setBz(String bz) {
        this.bz = bz == null ? null : bz.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", cbbh=").append(cbbh);
        sb.append(", sertype=").append(sertype);
        sb.append(", bankcode=").append(bankcode);
        sb.append(", bankname=").append(bankname);
        sb.append(", cardname=").append(cardname);
        sb.append(", cardnumber=").append(cardnumber);
        sb.append(", rksj=").append(rksj);
        sb.append(", yxx=").append(yxx);
        sb.append(", bz=").append(bz);
        sb.append("]");
        return sb.toString();
    }
}