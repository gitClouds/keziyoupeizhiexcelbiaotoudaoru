package com.unis.model.znzf;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_ZNZF_SF_LSH_JG")
public class SfLshJg {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "QQPK")
    private String qqpk;

    @Column(name = "JJDPK")
    private String jjdpk;

    @Column(name = "APPLICATIONID")
    private String applicationid;

    @Column(name = "RESULTCODE")
    private String resultcode;

    @Column(name = "ZFJGLX")
    private String zfjglx;

    @Column(name = "JYLX")
    private String jylx;

    @Column(name = "ZFLX")
    private String zflx;

    @Column(name = "RMBZL")
    private String rmbzl;

    @Column(name = "JE")
    private String je;

    @Column(name = "SKFYHJGDM")
    private String skfyhjgdm;

    @Column(name = "SKFYHJGMC")
    private String skfyhjgmc;

    @Column(name = "SKFZH")
    private String skfzh;

    @Column(name = "SKFZFZH")
    private String skfzfzh;

    @Column(name = "POSBH")
    private String posbh;

    @Column(name = "SKFSHHM")
    private String skfshhm;

    @Column(name = "FKFYHJGDM")
    private String fkfyhjgdm;

    @Column(name = "FKFYHJGMC")
    private String fkfyhjgmc;

    @Column(name = "FKFZH")
    private String fkfzh;

    @Column(name = "FKFZFZH")
    private String fkfzfzh;

    @Column(name = "JYSBLX")
    private String jysblx;

    @Column(name = "SBIP")
    private String sbip;

    @Column(name = "MACDZ")
    private String macdz;

    @Column(name = "SBHM")
    private String sbhm;

    @Column(name = "JYDDJD")
    private String jyddjd;

    @Column(name = "JYDDWD")
    private String jyddwd;

    @Column(name = "WBQDLSH")
    private String wbqdlsh;

    @Column(name = "FKSM")
    private String fksm;

    @Column(name = "FKJGMC")
    private String fkjgmc;

    @Column(name = "JBRXM")
    private String jbrxm;

    @Column(name = "JBRDH")
    private String jbrdh;

    @Column(name = "BZ")
    private String bz;

    @Column(name = "FKSJ")
    private Date fksj;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private Short yxx;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return QQPK
     */
    public String getQqpk() {
        return qqpk;
    }

    /**
     * @param qqpk
     */
    public void setQqpk(String qqpk) {
        this.qqpk = qqpk == null ? null : qqpk.trim();
    }

    /**
     * @return JJDPK
     */
    public String getJjdpk() {
        return jjdpk;
    }

    /**
     * @param jjdpk
     */
    public void setJjdpk(String jjdpk) {
        this.jjdpk = jjdpk == null ? null : jjdpk.trim();
    }

    /**
     * @return APPLICATIONID
     */
    public String getApplicationid() {
        return applicationid;
    }

    /**
     * @param applicationid
     */
    public void setApplicationid(String applicationid) {
        this.applicationid = applicationid == null ? null : applicationid.trim();
    }

    /**
     * @return RESULTCODE
     */
    public String getResultcode() {
        return resultcode;
    }

    /**
     * @param resultcode
     */
    public void setResultcode(String resultcode) {
        this.resultcode = resultcode == null ? null : resultcode.trim();
    }

    /**
     * @return ZFJGLX
     */
    public String getZfjglx() {
        return zfjglx;
    }

    /**
     * @param zfjglx
     */
    public void setZfjglx(String zfjglx) {
        this.zfjglx = zfjglx == null ? null : zfjglx.trim();
    }

    /**
     * @return JYLX
     */
    public String getJylx() {
        return jylx;
    }

    /**
     * @param jylx
     */
    public void setJylx(String jylx) {
        this.jylx = jylx == null ? null : jylx.trim();
    }

    /**
     * @return ZFLX
     */
    public String getZflx() {
        return zflx;
    }

    /**
     * @param zflx
     */
    public void setZflx(String zflx) {
        this.zflx = zflx == null ? null : zflx.trim();
    }

    /**
     * @return RMBZL
     */
    public String getRmbzl() {
        return rmbzl;
    }

    /**
     * @param rmbzl
     */
    public void setRmbzl(String rmbzl) {
        this.rmbzl = rmbzl == null ? null : rmbzl.trim();
    }

    /**
     * @return JE
     */
    public String getJe() {
        return je;
    }

    /**
     * @param je
     */
    public void setJe(String je) {
        this.je = je == null ? null : je.trim();
    }

    /**
     * @return SKFYHJGDM
     */
    public String getSkfyhjgdm() {
        return skfyhjgdm;
    }

    /**
     * @param skfyhjgdm
     */
    public void setSkfyhjgdm(String skfyhjgdm) {
        this.skfyhjgdm = skfyhjgdm == null ? null : skfyhjgdm.trim();
    }

    /**
     * @return SKFYHJGMC
     */
    public String getSkfyhjgmc() {
        return skfyhjgmc;
    }

    /**
     * @param skfyhjgmc
     */
    public void setSkfyhjgmc(String skfyhjgmc) {
        this.skfyhjgmc = skfyhjgmc == null ? null : skfyhjgmc.trim();
    }

    /**
     * @return SKFZH
     */
    public String getSkfzh() {
        return skfzh;
    }

    /**
     * @param skfzh
     */
    public void setSkfzh(String skfzh) {
        this.skfzh = skfzh == null ? null : skfzh.trim();
    }

    /**
     * @return SKFZFZH
     */
    public String getSkfzfzh() {
        return skfzfzh;
    }

    /**
     * @param skfzfzh
     */
    public void setSkfzfzh(String skfzfzh) {
        this.skfzfzh = skfzfzh == null ? null : skfzfzh.trim();
    }

    /**
     * @return POSBH
     */
    public String getPosbh() {
        return posbh;
    }

    /**
     * @param posbh
     */
    public void setPosbh(String posbh) {
        this.posbh = posbh == null ? null : posbh.trim();
    }

    /**
     * @return SKFSHHM
     */
    public String getSkfshhm() {
        return skfshhm;
    }

    /**
     * @param skfshhm
     */
    public void setSkfshhm(String skfshhm) {
        this.skfshhm = skfshhm == null ? null : skfshhm.trim();
    }

    /**
     * @return FKFYHJGDM
     */
    public String getFkfyhjgdm() {
        return fkfyhjgdm;
    }

    /**
     * @param fkfyhjgdm
     */
    public void setFkfyhjgdm(String fkfyhjgdm) {
        this.fkfyhjgdm = fkfyhjgdm == null ? null : fkfyhjgdm.trim();
    }

    /**
     * @return FKFYHJGMC
     */
    public String getFkfyhjgmc() {
        return fkfyhjgmc;
    }

    /**
     * @param fkfyhjgmc
     */
    public void setFkfyhjgmc(String fkfyhjgmc) {
        this.fkfyhjgmc = fkfyhjgmc == null ? null : fkfyhjgmc.trim();
    }

    /**
     * @return FKFZH
     */
    public String getFkfzh() {
        return fkfzh;
    }

    /**
     * @param fkfzh
     */
    public void setFkfzh(String fkfzh) {
        this.fkfzh = fkfzh == null ? null : fkfzh.trim();
    }

    /**
     * @return FKFZFZH
     */
    public String getFkfzfzh() {
        return fkfzfzh;
    }

    /**
     * @param fkfzfzh
     */
    public void setFkfzfzh(String fkfzfzh) {
        this.fkfzfzh = fkfzfzh == null ? null : fkfzfzh.trim();
    }

    /**
     * @return JYSBLX
     */
    public String getJysblx() {
        return jysblx;
    }

    /**
     * @param jysblx
     */
    public void setJysblx(String jysblx) {
        this.jysblx = jysblx == null ? null : jysblx.trim();
    }

    /**
     * @return SBIP
     */
    public String getSbip() {
        return sbip;
    }

    /**
     * @param sbip
     */
    public void setSbip(String sbip) {
        this.sbip = sbip == null ? null : sbip.trim();
    }

    /**
     * @return MACDZ
     */
    public String getMacdz() {
        return macdz;
    }

    /**
     * @param macdz
     */
    public void setMacdz(String macdz) {
        this.macdz = macdz == null ? null : macdz.trim();
    }

    /**
     * @return SBHM
     */
    public String getSbhm() {
        return sbhm;
    }

    /**
     * @param sbhm
     */
    public void setSbhm(String sbhm) {
        this.sbhm = sbhm == null ? null : sbhm.trim();
    }

    /**
     * @return JYDDJD
     */
    public String getJyddjd() {
        return jyddjd;
    }

    /**
     * @param jyddjd
     */
    public void setJyddjd(String jyddjd) {
        this.jyddjd = jyddjd == null ? null : jyddjd.trim();
    }

    /**
     * @return JYDDWD
     */
    public String getJyddwd() {
        return jyddwd;
    }

    /**
     * @param jyddwd
     */
    public void setJyddwd(String jyddwd) {
        this.jyddwd = jyddwd == null ? null : jyddwd.trim();
    }

    /**
     * @return WBQDLSH
     */
    public String getWbqdlsh() {
        return wbqdlsh;
    }

    /**
     * @param wbqdlsh
     */
    public void setWbqdlsh(String wbqdlsh) {
        this.wbqdlsh = wbqdlsh == null ? null : wbqdlsh.trim();
    }

    /**
     * @return FKSM
     */
    public String getFksm() {
        return fksm;
    }

    /**
     * @param fksm
     */
    public void setFksm(String fksm) {
        this.fksm = fksm == null ? null : fksm.trim();
    }

    /**
     * @return FKJGMC
     */
    public String getFkjgmc() {
        return fkjgmc;
    }

    /**
     * @param fkjgmc
     */
    public void setFkjgmc(String fkjgmc) {
        this.fkjgmc = fkjgmc == null ? null : fkjgmc.trim();
    }

    /**
     * @return JBRXM
     */
    public String getJbrxm() {
        return jbrxm;
    }

    /**
     * @param jbrxm
     */
    public void setJbrxm(String jbrxm) {
        this.jbrxm = jbrxm == null ? null : jbrxm.trim();
    }

    /**
     * @return JBRDH
     */
    public String getJbrdh() {
        return jbrdh;
    }

    /**
     * @param jbrdh
     */
    public void setJbrdh(String jbrdh) {
        this.jbrdh = jbrdh == null ? null : jbrdh.trim();
    }

    /**
     * @return BZ
     */
    public String getBz() {
        return bz;
    }

    /**
     * @param bz
     */
    public void setBz(String bz) {
        this.bz = bz == null ? null : bz.trim();
    }

    /**
     * @return FKSJ
     */
    public Date getFksj() {
        return fksj;
    }

    /**
     * @param fksj
     */
    public void setFksj(Date fksj) {
        this.fksj = fksj;
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", qqpk=").append(qqpk);
        sb.append(", jjdpk=").append(jjdpk);
        sb.append(", applicationid=").append(applicationid);
        sb.append(", resultcode=").append(resultcode);
        sb.append(", zfjglx=").append(zfjglx);
        sb.append(", jylx=").append(jylx);
        sb.append(", zflx=").append(zflx);
        sb.append(", rmbzl=").append(rmbzl);
        sb.append(", je=").append(je);
        sb.append(", skfyhjgdm=").append(skfyhjgdm);
        sb.append(", skfyhjgmc=").append(skfyhjgmc);
        sb.append(", skfzh=").append(skfzh);
        sb.append(", skfzfzh=").append(skfzfzh);
        sb.append(", posbh=").append(posbh);
        sb.append(", skfshhm=").append(skfshhm);
        sb.append(", fkfyhjgdm=").append(fkfyhjgdm);
        sb.append(", fkfyhjgmc=").append(fkfyhjgmc);
        sb.append(", fkfzh=").append(fkfzh);
        sb.append(", fkfzfzh=").append(fkfzfzh);
        sb.append(", jysblx=").append(jysblx);
        sb.append(", sbip=").append(sbip);
        sb.append(", macdz=").append(macdz);
        sb.append(", sbhm=").append(sbhm);
        sb.append(", jyddjd=").append(jyddjd);
        sb.append(", jyddwd=").append(jyddwd);
        sb.append(", wbqdlsh=").append(wbqdlsh);
        sb.append(", fksm=").append(fksm);
        sb.append(", fkjgmc=").append(fkjgmc);
        sb.append(", jbrxm=").append(jbrxm);
        sb.append(", jbrdh=").append(jbrdh);
        sb.append(", bz=").append(bz);
        sb.append(", fksj=").append(fksj);
        sb.append(", rksj=").append(rksj);
        sb.append(", yxx=").append(yxx);
        sb.append("]");
        return sb.toString();
    }
}