package com.unis.model.znzf;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_ZNZF_YHK_ZF_JG")
public class YhkZfJg {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "QQPK")
    private String qqpk;

    @Column(name = "JJDPK")
    private String jjdpk;

    @Column(name = "APPLICATIONID")
    private String applicationid;

    @Column(name = "RESULTCODE")
    private String resultcode;

    @Column(name = "ACCOUNTTYPE")
    private String accounttype;

    @Column(name = "ACCOUNTNUMBER")
    private String accountnumber;

    @Column(name = "CARDNUMBER")
    private String cardnumber;

    @Column(name = "ACCOUNTBALANCE")
    private BigDecimal accountbalance;

    @Column(name = "STARTTIME")
    private Date starttime;

    @Column(name = "EXPIRETIME")
    private Date expiretime;

    @Column(name = "FAILURECAUSE")
    private String failurecause;

    @Column(name = "FEEDBACKREMARK")
    private String feedbackremark;

    @Column(name = "FEEDBACKORGNAME")
    private String feedbackorgname;

    @Column(name = "OPERATORNAME")
    private String operatorname;

    @Column(name = "OPERATORPHONENUMBER")
    private String operatorphonenumber;

    @Column(name = "FKSJ")
    private Date fksj;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private Short yxx;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return QQPK
     */
    public String getQqpk() {
        return qqpk;
    }

    /**
     * @param qqpk
     */
    public void setQqpk(String qqpk) {
        this.qqpk = qqpk == null ? null : qqpk.trim();
    }

    /**
     * @return JJDPK
     */
    public String getJjdpk() {
        return jjdpk;
    }

    /**
     * @param jjdpk
     */
    public void setJjdpk(String jjdpk) {
        this.jjdpk = jjdpk == null ? null : jjdpk.trim();
    }

    /**
     * @return APPLICATIONID
     */
    public String getApplicationid() {
        return applicationid;
    }

    /**
     * @param applicationid
     */
    public void setApplicationid(String applicationid) {
        this.applicationid = applicationid == null ? null : applicationid.trim();
    }

    /**
     * @return RESULTCODE
     */
    public String getResultcode() {
        return resultcode;
    }

    /**
     * @param resultcode
     */
    public void setResultcode(String resultcode) {
        this.resultcode = resultcode == null ? null : resultcode.trim();
    }

    /**
     * @return ACCOUNTTYPE
     */
    public String getAccounttype() {
        return accounttype;
    }

    /**
     * @param accounttype
     */
    public void setAccounttype(String accounttype) {
        this.accounttype = accounttype == null ? null : accounttype.trim();
    }

    /**
     * @return ACCOUNTNUMBER
     */
    public String getAccountnumber() {
        return accountnumber;
    }

    /**
     * @param accountnumber
     */
    public void setAccountnumber(String accountnumber) {
        this.accountnumber = accountnumber == null ? null : accountnumber.trim();
    }

    /**
     * @return CARDNUMBER
     */
    public String getCardnumber() {
        return cardnumber;
    }

    /**
     * @param cardnumber
     */
    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber == null ? null : cardnumber.trim();
    }

    /**
     * @return ACCOUNTBALANCE
     */
    public BigDecimal getAccountbalance() {
        return accountbalance;
    }

    /**
     * @param accountbalance
     */
    public void setAccountbalance(BigDecimal accountbalance) {
        this.accountbalance = accountbalance;
    }

    /**
     * @return STARTTIME
     */
    public Date getStarttime() {
        return starttime;
    }

    /**
     * @param starttime
     */
    public void setStarttime(Date starttime) {
        this.starttime = starttime;
    }

    /**
     * @return EXPIRETIME
     */
    public Date getExpiretime() {
        return expiretime;
    }

    /**
     * @param expiretime
     */
    public void setExpiretime(Date expiretime) {
        this.expiretime = expiretime;
    }

    /**
     * @return FAILURECAUSE
     */
    public String getFailurecause() {
        return failurecause;
    }

    /**
     * @param failurecause
     */
    public void setFailurecause(String failurecause) {
        this.failurecause = failurecause == null ? null : failurecause.trim();
    }

    /**
     * @return FEEDBACKREMARK
     */
    public String getFeedbackremark() {
        return feedbackremark;
    }

    /**
     * @param feedbackremark
     */
    public void setFeedbackremark(String feedbackremark) {
        this.feedbackremark = feedbackremark == null ? null : feedbackremark.trim();
    }

    /**
     * @return FEEDBACKORGNAME
     */
    public String getFeedbackorgname() {
        return feedbackorgname;
    }

    /**
     * @param feedbackorgname
     */
    public void setFeedbackorgname(String feedbackorgname) {
        this.feedbackorgname = feedbackorgname == null ? null : feedbackorgname.trim();
    }

    /**
     * @return OPERATORNAME
     */
    public String getOperatorname() {
        return operatorname;
    }

    /**
     * @param operatorname
     */
    public void setOperatorname(String operatorname) {
        this.operatorname = operatorname == null ? null : operatorname.trim();
    }

    /**
     * @return OPERATORPHONENUMBER
     */
    public String getOperatorphonenumber() {
        return operatorphonenumber;
    }

    /**
     * @param operatorphonenumber
     */
    public void setOperatorphonenumber(String operatorphonenumber) {
        this.operatorphonenumber = operatorphonenumber == null ? null : operatorphonenumber.trim();
    }

    /**
     * @return FKSJ
     */
    public Date getFksj() {
        return fksj;
    }

    /**
     * @param fksj
     */
    public void setFksj(Date fksj) {
        this.fksj = fksj;
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", qqpk=").append(qqpk);
        sb.append(", jjdpk=").append(jjdpk);
        sb.append(", applicationid=").append(applicationid);
        sb.append(", resultcode=").append(resultcode);
        sb.append(", accounttype=").append(accounttype);
        sb.append(", accountnumber=").append(accountnumber);
        sb.append(", cardnumber=").append(cardnumber);
        sb.append(", accountbalance=").append(accountbalance);
        sb.append(", starttime=").append(starttime);
        sb.append(", expiretime=").append(expiretime);
        sb.append(", failurecause=").append(failurecause);
        sb.append(", feedbackremark=").append(feedbackremark);
        sb.append(", feedbackorgname=").append(feedbackorgname);
        sb.append(", operatorname=").append(operatorname);
        sb.append(", operatorphonenumber=").append(operatorphonenumber);
        sb.append(", fksj=").append(fksj);
        sb.append(", rksj=").append(rksj);
        sb.append(", yxx=").append(yxx);
        sb.append("]");
        return sb.toString();
    }
}