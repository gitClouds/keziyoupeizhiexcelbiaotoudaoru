package com.unis.model.znzf;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_ZNZF_SPECIAL_META")
public class SpecialMeta {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "BANKCODE")
    private String bankcode;

    @Column(name = "BANKNAME")
    private String bankname;

    @Column(name = "JDBZ")
    private String jdbz;

    @Column(name = "CHECKFIELD")
    private String checkfield;

    @Column(name = "CHECKVALUE")
    private String checkvalue;

    @Column(name = "SERTYPE")
    private String sertype;

    @Column(name = "DATAFIELD")
    private String datafield;

    @Column(name = "POSITIONVAL")
    private Short positionval;

    @Column(name = "STARTPFX")
    private String startpfx;

    @Column(name = "ENDPFX")
    private String endpfx;

    @Column(name = "PAYCODE")
    private String paycode;

    @Column(name = "PAYNAME")
    private String payname;

    @Column(name = "YXX")
    private Short yxx;

    @Column(name = "RKSJ")
    private Date rksj;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return BANKCODE
     */
    public String getBankcode() {
        return bankcode;
    }

    /**
     * @param bankcode
     */
    public void setBankcode(String bankcode) {
        this.bankcode = bankcode == null ? null : bankcode.trim();
    }

    /**
     * @return JDBZ
     */
    public String getJdbz() {
        return jdbz;
    }

    /**
     * @param jdbz
     */
    public void setJdbz(String jdbz) {
        this.jdbz = jdbz == null ? null : jdbz.trim();
    }

    /**
     * @return CHECKFIELD
     */
    public String getCheckfield() {
        return checkfield;
    }

    /**
     * @param checkfield
     */
    public void setCheckfield(String checkfield) {
        this.checkfield = checkfield == null ? null : checkfield.trim();
    }

    /**
     * @return CHECKVALUE
     */
    public String getCheckvalue() {
        return checkvalue;
    }

    /**
     * @param checkvalue
     */
    public void setCheckvalue(String checkvalue) {
        this.checkvalue = checkvalue == null ? null : checkvalue.trim();
    }

    /**
     * @return SERTYPE
     */
    public String getSertype() {
        return sertype;
    }

    /**
     * @param sertype
     */
    public void setSertype(String sertype) {
        this.sertype = sertype == null ? null : sertype.trim();
    }

    /**
     * @return DATAFIELD
     */
    public String getDatafield() {
        return datafield;
    }

    /**
     * @param datafield
     */
    public void setDatafield(String datafield) {
        this.datafield = datafield == null ? null : datafield.trim();
    }

    /**
     * @return POSITIONVAL
     */
    public Short getPositionval() {
        return positionval;
    }

    /**
     * @param positionval
     */
    public void setPositionval(Short positionval) {
        this.positionval = positionval;
    }

    /**
     * @return STARTPFX
     */
    public String getStartpfx() {
        return startpfx;
    }

    /**
     * @param startpfx
     */
    public void setStartpfx(String startpfx) {
        this.startpfx = startpfx == null ? null : startpfx.trim();
    }

    /**
     * @return ENDPFX
     */
    public String getEndpfx() {
        return endpfx;
    }

    /**
     * @param endpfx
     */
    public void setEndpfx(String endpfx) {
        this.endpfx = endpfx == null ? null : endpfx.trim();
    }

    /**
     * @return PAYCODE
     */
    public String getPaycode() {
        return paycode;
    }

    /**
     * @param paycode
     */
    public void setPaycode(String paycode) {
        this.paycode = paycode == null ? null : paycode.trim();
    }

    /**
     * @return PAYNAME
     */
    public String getPayname() {
        return payname;
    }

    /**
     * @param payname
     */
    public void setPayname(String payname) {
        this.payname = payname == null ? null : payname.trim();
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", bankcode=").append(bankcode);
        sb.append(", bankname=").append(bankname);
        sb.append(", jdbz=").append(jdbz);
        sb.append(", checkfield=").append(checkfield);
        sb.append(", checkvalue=").append(checkvalue);
        sb.append(", sertype=").append(sertype);
        sb.append(", datafield=").append(datafield);
        sb.append(", positionval=").append(positionval);
        sb.append(", startpfx=").append(startpfx);
        sb.append(", endpfx=").append(endpfx);
        sb.append(", paycode=").append(paycode);
        sb.append(", payname=").append(payname);
        sb.append(", yxx=").append(yxx);
        sb.append(", rksj=").append(rksj);
        sb.append("]");
        return sb.toString();
    }
}