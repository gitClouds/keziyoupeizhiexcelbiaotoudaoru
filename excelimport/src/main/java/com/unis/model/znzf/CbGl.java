package com.unis.model.znzf;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_ZNZF_CB_GL")
public class CbGl {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "CBBH")
    private String cbbh;

    @Column(name = "SERTYPE")
    private Short sertype;

    @Column(name = "JJDPK")
    private String jjdpk;

    @Column(name = "BANKCODE")
    private String bankcode;

    @Column(name = "CARDNUMBER")
    private String cardnumber;

    @Column(name = "LRDWDM")
    private String lrdwdm;

    @Column(name = "LRDWMC")
    private String lrdwmc;

    @Column(name = "LRRXM")
    private String lrrxm;

    @Column(name = "LRRJH")
    private String lrrjh;

    @Column(name = "LRSJ")
    private Date lrsj;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private Short yxx;
    @Column(name = "JJDHM")
    private String jjdhm;
    @Column(name = "AFSJ")
    private Date afsj;
    @Column(name = "BARXM")
    private String barxm;
    @Column(name = "SLDWMC")
    private String sldwmc;
    private String cbyj;

    public String getJjdhm() {
        return jjdhm;
    }

    public void setJjdhm(String jjdhm) {
        this.jjdhm = jjdhm;
    }

    public Date getAfsj() {
        return afsj;
    }

    public void setAfsj(Date afsj) {
        this.afsj = afsj;
    }

    public String getBarxm() {
        return barxm;
    }

    public void setBarxm(String barxm) {
        this.barxm = barxm;
    }

    public String getSldwmc() {
        return sldwmc;
    }

    public void setSldwmc(String sldwmc) {
        this.sldwmc = sldwmc;
    }

    public String getCbyj() {
        return cbyj;
    }

    public void setCbyj(String cbyj) {
        this.cbyj = cbyj;
    }

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return CBBH
     */
    public String getCbbh() {
        return cbbh;
    }

    /**
     * @param cbbh
     */
    public void setCbbh(String cbbh) {
        this.cbbh = cbbh == null ? null : cbbh.trim();
    }

    /**
     * @return SERTYPE
     */
    public Short getSertype() {
        return sertype;
    }

    /**
     * @param sertype
     */
    public void setSertype(Short sertype) {
        this.sertype = sertype;
    }

    /**
     * @return JJDPK
     */
    public String getJjdpk() {
        return jjdpk;
    }

    /**
     * @param jjdpk
     */
    public void setJjdpk(String jjdpk) {
        this.jjdpk = jjdpk == null ? null : jjdpk.trim();
    }

    /**
     * @return BANKCODE
     */
    public String getBankcode() {
        return bankcode;
    }

    /**
     * @param bankcode
     */
    public void setBankcode(String bankcode) {
        this.bankcode = bankcode == null ? null : bankcode.trim();
    }

    /**
     * @return CARDNUMBER
     */
    public String getCardnumber() {
        return cardnumber;
    }

    /**
     * @param cardnumber
     */
    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber == null ? null : cardnumber.trim();
    }

    /**
     * @return LRDWDM
     */
    public String getLrdwdm() {
        return lrdwdm;
    }

    /**
     * @param lrdwdm
     */
    public void setLrdwdm(String lrdwdm) {
        this.lrdwdm = lrdwdm == null ? null : lrdwdm.trim();
    }

    /**
     * @return LRDWMC
     */
    public String getLrdwmc() {
        return lrdwmc;
    }

    /**
     * @param lrdwmc
     */
    public void setLrdwmc(String lrdwmc) {
        this.lrdwmc = lrdwmc == null ? null : lrdwmc.trim();
    }

    /**
     * @return LRRXM
     */
    public String getLrrxm() {
        return lrrxm;
    }

    /**
     * @param lrrxm
     */
    public void setLrrxm(String lrrxm) {
        this.lrrxm = lrrxm == null ? null : lrrxm.trim();
    }

    /**
     * @return LRRJH
     */
    public String getLrrjh() {
        return lrrjh;
    }

    /**
     * @param lrrjh
     */
    public void setLrrjh(String lrrjh) {
        this.lrrjh = lrrjh == null ? null : lrrjh.trim();
    }

    /**
     * @return LRSJ
     */
    public Date getLrsj() {
        return lrsj;
    }

    /**
     * @param lrsj
     */
    public void setLrsj(Date lrsj) {
        this.lrsj = lrsj;
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    @Override
    public String toString() {
        return "CbGl{" +
                "pk='" + pk + '\'' +
                ", cbbh='" + cbbh + '\'' +
                ", sertype=" + sertype +
                ", jjdpk='" + jjdpk + '\'' +
                ", bankcode='" + bankcode + '\'' +
                ", cardnumber='" + cardnumber + '\'' +
                ", lrdwdm='" + lrdwdm + '\'' +
                ", lrdwmc='" + lrdwmc + '\'' +
                ", lrrxm='" + lrrxm + '\'' +
                ", lrrjh='" + lrrjh + '\'' +
                ", lrsj=" + lrsj +
                ", rksj=" + rksj +
                ", yxx=" + yxx +
                ", jjdhm='" + jjdhm + '\'' +
                ", afsj=" + afsj +
                ", barxm='" + barxm + '\'' +
                ", sldwmc='" + sldwmc + '\'' +
                ", cbyj='" + cbyj + '\'' +
                '}';
    }
}