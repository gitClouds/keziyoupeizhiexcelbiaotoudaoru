package com.unis.model.znzf;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_ZNZF_SF_JG_CARD")
public class SfJgCard {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "ACTPK")
    private String actpk;

    @Column(name = "APPLICATIONID")
    private String applicationid;

    @Column(name = "YHKJGDM")
    private String yhkjgdm;

    @Column(name = "YHKJGMC")
    private String yhkjgmc;

    @Column(name = "YHKHM")
    private String yhkhm;

    @Column(name = "YHKLX")
    private String yhklx;

    @Column(name = "YHKRZZT")
    private String yhkrzzt;

    @Column(name = "YXQ")
    private String yxq;

    @Column(name = "QTXX")
    private String qtxx;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private Short yxx;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return APPLICATIONID
     */
    public String getApplicationid() {
        return applicationid;
    }

    /**
     * @param applicationid
     */
    public void setApplicationid(String applicationid) {
        this.applicationid = applicationid == null ? null : applicationid.trim();
    }

    /**
     * @return YHKJGDM
     */
    public String getYhkjgdm() {
        return yhkjgdm;
    }

    /**
     * @param yhkjgdm
     */
    public void setYhkjgdm(String yhkjgdm) {
        this.yhkjgdm = yhkjgdm == null ? null : yhkjgdm.trim();
    }

    /**
     * @return YHKJGMC
     */
    public String getYhkjgmc() {
        return yhkjgmc;
    }

    /**
     * @param yhkjgmc
     */
    public void setYhkjgmc(String yhkjgmc) {
        this.yhkjgmc = yhkjgmc == null ? null : yhkjgmc.trim();
    }

    /**
     * @return YHKHM
     */
    public String getYhkhm() {
        return yhkhm;
    }

    /**
     * @param yhkhm
     */
    public void setYhkhm(String yhkhm) {
        this.yhkhm = yhkhm == null ? null : yhkhm.trim();
    }

    /**
     * @return YHKLX
     */
    public String getYhklx() {
        return yhklx;
    }

    /**
     * @param yhklx
     */
    public void setYhklx(String yhklx) {
        this.yhklx = yhklx == null ? null : yhklx.trim();
    }

    /**
     * @return YHKRZZT
     */
    public String getYhkrzzt() {
        return yhkrzzt;
    }

    /**
     * @param yhkrzzt
     */
    public void setYhkrzzt(String yhkrzzt) {
        this.yhkrzzt = yhkrzzt == null ? null : yhkrzzt.trim();
    }

    /**
     * @return YXQ
     */
    public String getYxq() {
        return yxq;
    }

    /**
     * @param yxq
     */
    public void setYxq(String yxq) {
        this.yxq = yxq == null ? null : yxq.trim();
    }

    /**
     * @return QTXX
     */
    public String getQtxx() {
        return qtxx;
    }

    /**
     * @param qtxx
     */
    public void setQtxx(String qtxx) {
        this.qtxx = qtxx == null ? null : qtxx.trim();
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    public String getActpk() {
        return actpk;
    }

    public SfJgCard setActpk(String actpk) {
        this.actpk = actpk;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", actpk=").append(actpk);
        sb.append(", applicationid=").append(applicationid);
        sb.append(", yhkjgdm=").append(yhkjgdm);
        sb.append(", yhkjgmc=").append(yhkjgmc);
        sb.append(", yhkhm=").append(yhkhm);
        sb.append(", yhklx=").append(yhklx);
        sb.append(", yhkrzzt=").append(yhkrzzt);
        sb.append(", yxq=").append(yxq);
        sb.append(", qtxx=").append(qtxx);
        sb.append(", rksj=").append(rksj);
        sb.append(", yxx=").append(yxx);
        sb.append("]");
        return sb.toString();
    }
}