package com.unis.model.znzf;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_ZNZF_MX_QQ_GL")
public class MxQqGl {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "QQPK")
    private String qqpk;

    @Column(name = "SERTYPE")
    private Short sertype;

    @Column(name = "APPLICATIONID")
    private String applicationid;

    @Column(name = "CARDNUMBER")
    private String cardnumber;

    @Column(name = "KSSJ")
    private Date kssj;

    @Column(name = "JSSJ")
    private Date jssj;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private Short yxx;

    @Column(name = "SFQQCG")
    private Short sfqqcg;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return QQPK
     */
    public String getQqpk() {
        return qqpk;
    }

    /**
     * @param qqpk
     */
    public void setQqpk(String qqpk) {
        this.qqpk = qqpk == null ? null : qqpk.trim();
    }

    /**
     * @return SERTYPE
     */
    public Short getSertype() {
        return sertype;
    }

    /**
     * @param sertype
     */
    public void setSertype(Short sertype) {
        this.sertype = sertype;
    }

    /**
     * @return APPLICATIONID
     */
    public String getApplicationid() {
        return applicationid;
    }

    /**
     * @param applicationid
     */
    public void setApplicationid(String applicationid) {
        this.applicationid = applicationid == null ? null : applicationid.trim();
    }

    /**
     * @return CARDNUMBER
     */
    public String getCardnumber() {
        return cardnumber;
    }

    /**
     * @param cardnumber
     */
    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber == null ? null : cardnumber.trim();
    }

    public Date getKssj() {
        return kssj;
    }

    public void setKssj(Date kssj) {
        this.kssj = kssj;
    }

    public Date getJssj() {
        return jssj;
    }

    public void setJssj(Date jssj) {
        this.jssj = jssj;
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    /**
     * @return SFQQCG
     */
    public Short getSfqqcg() {
        return sfqqcg;
    }

    /**
     * @param sfqqcg
     */
    public void setSfqqcg(Short sfqqcg) {
        this.sfqqcg = sfqqcg;
    }

    @Override
    public String toString() {
        return "MxQqGl{" +
                "pk='" + pk + '\'' +
                ", qqpk='" + qqpk + '\'' +
                ", sertype=" + sertype +
                ", applicationid='" + applicationid + '\'' +
                ", cardnumber='" + cardnumber + '\'' +
                ", kssj=" + kssj +
                ", jssj=" + jssj +
                ", rksj=" + rksj +
                ", yxx=" + yxx +
                ", sfqqcg=" + sfqqcg +
                '}';
    }
}