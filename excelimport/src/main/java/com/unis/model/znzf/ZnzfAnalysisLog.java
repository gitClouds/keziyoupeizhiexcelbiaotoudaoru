package com.unis.model.znzf;

import javax.persistence.*;

@Table(name = "TB_ZNZF_ANALYSIS_LOG")
public class ZnzfAnalysisLog {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "JJDPK")
    private String jjdpk;

    @Column(name = "CARDNUMBER")
    private String cardnumber;

    @Column(name = "MXJSON")
    private String mxjson;

    @Column(name = "ZTJSON")
    private String ztjson;

    @Column(name = "ISOK")
    private Short isok;

    @Column(name = "ERRMSG")
    private String errmsg;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return JJDPK
     */
    public String getJjdpk() {
        return jjdpk;
    }

    /**
     * @param jjdpk
     */
    public void setJjdpk(String jjdpk) {
        this.jjdpk = jjdpk == null ? null : jjdpk.trim();
    }

    /**
     * @return CARDNUMBER
     */
    public String getCardnumber() {
        return cardnumber;
    }

    /**
     * @param cardnumber
     */
    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber == null ? null : cardnumber.trim();
    }

    /**
     * @return MXJSON
     */
    public String getMxjson() {
        return mxjson;
    }

    /**
     * @param mxjson
     */
    public void setMxjson(String mxjson) {
        this.mxjson = mxjson == null ? null : mxjson.trim();
    }

    /**
     * @return ZTJSON
     */
    public String getZtjson() {
        return ztjson;
    }

    /**
     * @param ztjson
     */
    public void setZtjson(String ztjson) {
        this.ztjson = ztjson == null ? null : ztjson.trim();
    }

    /**
     * @return ISOK
     */
    public Short getIsok() {
        return isok;
    }

    /**
     * @param isok
     */
    public void setIsok(Short isok) {
        this.isok = isok;
    }

    /**
     * @return ERRMSG
     */
    public String getErrmsg() {
        return errmsg;
    }

    /**
     * @param errmsg
     */
    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg == null ? null : errmsg.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", jjdpk=").append(jjdpk);
        sb.append(", cardnumber=").append(cardnumber);
        sb.append(", mxjson=").append(mxjson);
        sb.append(", ztjson=").append(ztjson);
        sb.append(", isok=").append(isok);
        sb.append(", errmsg=").append(errmsg);
        sb.append("]");
        return sb.toString();
    }
}