package com.unis.model.znzf;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_ZNZF_CB_MAC_GAB")
public class CbMacGab {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "SERTYPE")
    private Short sertype;

    @Column(name = "JJDQQPK")
    private String jjdqqpk;

    @Column(name = "JJDPK")
    private String jjdpk;

    @Column(name = "YWSQBH")
    private String ywsqbh;

    @Column(name = "MACDZ")
    private String macdz;

    @Column(name = "QQZH")
    private String qqzh;

    @Column(name = "QQZHXM")
    private String qqzhxm;

    @Column(name = "QQZHJGDM")
    private String qqzhjgdm;

    @Column(name = "QQZHJGMC")
    private String qqzhjgmc;

    @Column(name = "QQLRDWDM")
    private String qqlrdwdm;

    @Column(name = "QQLRDWMC")
    private String qqlrdwmc;

    @Column(name = "LRSJ")
    private Date lrsj;

    @Column(name = "YXX")
    private Short yxx;

    @Column(name = "SY")
    private String sy;

    @Column(name = "COUNT")
    private Short count;

    @Column(name = "JJDQQZH")
    private String jjdqqzh;

    public String getJjdqqpk() {
        return jjdqqpk;
    }

    public void setJjdqqpk(String jjdqqpk) {
        this.jjdqqpk = jjdqqpk;
    }

    public String getJjdqqzh() {
        return jjdqqzh;
    }

    public void setJjdqqzh(String jjdqqzh) {
        this.jjdqqzh = jjdqqzh;
    }

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return SERTYPE
     */
    public Short getSertype() {
        return sertype;
    }

    /**
     * @param sertype
     */
    public void setSertype(Short sertype) {
        this.sertype = sertype;
    }


    /**
     * @return JJDPK
     */
    public String getJjdpk() {
        return jjdpk;
    }

    /**
     * @param jjdpk
     */
    public void setJjdpk(String jjdpk) {
        this.jjdpk = jjdpk == null ? null : jjdpk.trim();
    }

    /**
     * @return YWSQBH
     */
    public String getYwsqbh() {
        return ywsqbh;
    }

    /**
     * @param ywsqbh
     */
    public void setYwsqbh(String ywsqbh) {
        this.ywsqbh = ywsqbh == null ? null : ywsqbh.trim();
    }

    /**
     * @return MACDZ
     */
    public String getMacdz() {
        return macdz;
    }

    /**
     * @param macdz
     */
    public void setMacdz(String macdz) {
        this.macdz = macdz == null ? null : macdz.trim();
    }

    /**
     * @return QQZH
     */
    public String getQqzh() {
        return qqzh;
    }

    /**
     * @param qqzh
     */
    public void setQqzh(String qqzh) {
        this.qqzh = qqzh == null ? null : qqzh.trim();
    }

    /**
     * @return QQZHXM
     */
    public String getQqzhxm() {
        return qqzhxm;
    }

    /**
     * @param qqzhxm
     */
    public void setQqzhxm(String qqzhxm) {
        this.qqzhxm = qqzhxm == null ? null : qqzhxm.trim();
    }

    /**
     * @return QQZHJGDM
     */
    public String getQqzhjgdm() {
        return qqzhjgdm;
    }

    /**
     * @param qqzhjgdm
     */
    public void setQqzhjgdm(String qqzhjgdm) {
        this.qqzhjgdm = qqzhjgdm == null ? null : qqzhjgdm.trim();
    }

    /**
     * @return QQZHJGMC
     */
    public String getQqzhjgmc() {
        return qqzhjgmc;
    }

    /**
     * @param qqzhjgmc
     */
    public void setQqzhjgmc(String qqzhjgmc) {
        this.qqzhjgmc = qqzhjgmc == null ? null : qqzhjgmc.trim();
    }

    /**
     * @return QQLRDWDM
     */
    public String getQqlrdwdm() {
        return qqlrdwdm;
    }

    /**
     * @param qqlrdwdm
     */
    public void setQqlrdwdm(String qqlrdwdm) {
        this.qqlrdwdm = qqlrdwdm == null ? null : qqlrdwdm.trim();
    }

    /**
     * @return QQLRDWMC
     */
    public String getQqlrdwmc() {
        return qqlrdwmc;
    }

    /**
     * @param qqlrdwmc
     */
    public void setQqlrdwmc(String qqlrdwmc) {
        this.qqlrdwmc = qqlrdwmc == null ? null : qqlrdwmc.trim();
    }

    /**
     * @return LRSJ
     */
    public Date getLrsj() {
        return lrsj;
    }

    /**
     * @param lrsj
     */
    public void setLrsj(Date lrsj) {
        this.lrsj = lrsj;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    /**
     * @return SY
     */
    public String getSy() {
        return sy;
    }

    /**
     * @param sy
     */
    public void setSy(String sy) {
        this.sy = sy == null ? null : sy.trim();
    }

    /**
     * @return COUNT
     */
    public Short getCount() {
        return count;
    }

    /**
     * @param count
     */
    public void setCount(Short count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "CbMacGab{" +
                "pk='" + pk + '\'' +
                ", sertype=" + sertype +
                ", jjdqqpk='" + jjdqqpk + '\'' +
                ", jjdpk='" + jjdpk + '\'' +
                ", ywsqbh='" + ywsqbh + '\'' +
                ", macdz='" + macdz + '\'' +
                ", qqzh='" + qqzh + '\'' +
                ", qqzhxm='" + qqzhxm + '\'' +
                ", qqzhjgdm='" + qqzhjgdm + '\'' +
                ", qqzhjgmc='" + qqzhjgmc + '\'' +
                ", qqlrdwdm='" + qqlrdwdm + '\'' +
                ", qqlrdwmc='" + qqlrdwmc + '\'' +
                ", lrsj=" + lrsj +
                ", yxx=" + yxx +
                ", sy='" + sy + '\'' +
                ", count=" + count +
                ", jjdqqzh='" + jjdqqzh + '\'' +
                '}';
    }
}