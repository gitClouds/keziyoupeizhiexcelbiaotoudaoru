package com.unis.model.znzf;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_ZNZF_SJSC")
public class ZnzfSjsc {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "JJDPK")
    private String jjdpk;

    @Column(name = "DATATYPE")
    private Short datatype;

    @Column(name = "MONGOPK")
    private String mongopk;

    @Column(name = "FILENAME")
    private String filename;

    @Column(name = "FILESIZE")
    private Long filesize;

    @Column(name = "FILETYPE")
    private String filetype;

    @Column(name = "LRDWDM")
    private String lrdwdm;

    @Column(name = "LRDWMC")
    private String lrdwmc;

    @Column(name = "LRRXM")
    private String lrrxm;

    @Column(name = "LRRJH")
    private String lrrjh;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private Short yxx;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return JJDPK
     */
    public String getJjdpk() {
        return jjdpk;
    }

    /**
     * @param jjdpk
     */
    public void setJjdpk(String jjdpk) {
        this.jjdpk = jjdpk == null ? null : jjdpk.trim();
    }

    /**
     * @return DATATYPE
     */
    public Short getDatatype() {
        return datatype;
    }

    /**
     * @param datatype
     */
    public void setDatatype(Short datatype) {
        this.datatype = datatype;
    }

    /**
     * @return MONGOPK
     */
    public String getMongopk() {
        return mongopk;
    }

    /**
     * @param mongopk
     */
    public void setMongopk(String mongopk) {
        this.mongopk = mongopk == null ? null : mongopk.trim();
    }

    /**
     * @return FILENAME
     */
    public String getFilename() {
        return filename;
    }

    /**
     * @param filename
     */
    public void setFilename(String filename) {
        this.filename = filename == null ? null : filename.trim();
    }

    /**
     * @return FILESIZE
     */
    public Long getFilesize() {
        return filesize;
    }

    /**
     * @param filesize
     */
    public void setFilesize(Long filesize) {
        this.filesize = filesize;
    }

    /**
     * @return FILETYPE
     */
    public String getFiletype() {
        return filetype;
    }

    /**
     * @param filetype
     */
    public void setFiletype(String filetype) {
        this.filetype = filetype == null ? null : filetype.trim();
    }

    /**
     * @return LRDWDM
     */
    public String getLrdwdm() {
        return lrdwdm;
    }

    /**
     * @param lrdwdm
     */
    public void setLrdwdm(String lrdwdm) {
        this.lrdwdm = lrdwdm == null ? null : lrdwdm.trim();
    }

    /**
     * @return LRDWMC
     */
    public String getLrdwmc() {
        return lrdwmc;
    }

    /**
     * @param lrdwmc
     */
    public void setLrdwmc(String lrdwmc) {
        this.lrdwmc = lrdwmc == null ? null : lrdwmc.trim();
    }

    /**
     * @return LRRXM
     */
    public String getLrrxm() {
        return lrrxm;
    }

    /**
     * @param lrrxm
     */
    public void setLrrxm(String lrrxm) {
        this.lrrxm = lrrxm == null ? null : lrrxm.trim();
    }

    /**
     * @return LRRJH
     */
    public String getLrrjh() {
        return lrrjh;
    }

    /**
     * @param lrrjh
     */
    public void setLrrjh(String lrrjh) {
        this.lrrjh = lrrjh == null ? null : lrrjh.trim();
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", jjdpk=").append(jjdpk);
        sb.append(", datatype=").append(datatype);
        sb.append(", mongopk=").append(mongopk);
        sb.append(", filename=").append(filename);
        sb.append(", filesize=").append(filesize);
        sb.append(", filetype=").append(filetype);
        sb.append(", lrdwdm=").append(lrdwdm);
        sb.append(", lrdwmc=").append(lrdwmc);
        sb.append(", lrrxm=").append(lrrxm);
        sb.append(", lrrjh=").append(lrrjh);
        sb.append(", rksj=").append(rksj);
        sb.append(", yxx=").append(yxx);
        sb.append("]");
        return sb.toString();
    }
}