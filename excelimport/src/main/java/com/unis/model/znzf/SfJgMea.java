package com.unis.model.znzf;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_ZNZF_SF_JG_MEA")
public class SfJgMea {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "ACTPK")
    private String actpk;

    @Column(name = "APPLICATIONID")
    private String applicationid;

    @Column(name = "CSXH")
    private String csxh;

    @Column(name = "ZFZH")
    private String zfzh;

    @Column(name = "CSLX")
    private String cslx;

    @Column(name = "KSSJ")
    private String kssj;

    @Column(name = "JZSJ")
    private String jzsj;

    @Column(name = "JGMC")
    private String jgmc;

    @Column(name = "RMBZL")
    private String rmbzl;

    @Column(name = "ZXJE")
    private String zxje;

    @Column(name = "BZ")
    private String bz;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private Short yxx;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return ACTPK
     */
    public String getActpk() {
        return actpk;
    }

    /**
     * @param actpk
     */
    public void setActpk(String actpk) {
        this.actpk = actpk == null ? null : actpk.trim();
    }

    /**
     * @return APPLICATIONID
     */
    public String getApplicationid() {
        return applicationid;
    }

    /**
     * @param applicationid
     */
    public void setApplicationid(String applicationid) {
        this.applicationid = applicationid == null ? null : applicationid.trim();
    }

    /**
     * @return CSXH
     */
    public String getCsxh() {
        return csxh;
    }

    /**
     * @param csxh
     */
    public void setCsxh(String csxh) {
        this.csxh = csxh == null ? null : csxh.trim();
    }

    /**
     * @return ZFZH
     */
    public String getZfzh() {
        return zfzh;
    }

    /**
     * @param zfzh
     */
    public void setZfzh(String zfzh) {
        this.zfzh = zfzh == null ? null : zfzh.trim();
    }

    /**
     * @return CSLX
     */
    public String getCslx() {
        return cslx;
    }

    /**
     * @param cslx
     */
    public void setCslx(String cslx) {
        this.cslx = cslx == null ? null : cslx.trim();
    }

    /**
     * @return KSSJ
     */
    public String getKssj() {
        return kssj;
    }

    /**
     * @param kssj
     */
    public void setKssj(String kssj) {
        this.kssj = kssj == null ? null : kssj.trim();
    }

    /**
     * @return JZSJ
     */
    public String getJzsj() {
        return jzsj;
    }

    /**
     * @param jzsj
     */
    public void setJzsj(String jzsj) {
        this.jzsj = jzsj == null ? null : jzsj.trim();
    }

    /**
     * @return JGMC
     */
    public String getJgmc() {
        return jgmc;
    }

    /**
     * @param jgmc
     */
    public void setJgmc(String jgmc) {
        this.jgmc = jgmc == null ? null : jgmc.trim();
    }

    /**
     * @return RMBZL
     */
    public String getRmbzl() {
        return rmbzl;
    }

    /**
     * @param rmbzl
     */
    public void setRmbzl(String rmbzl) {
        this.rmbzl = rmbzl == null ? null : rmbzl.trim();
    }

    /**
     * @return ZXJE
     */
    public String getZxje() {
        return zxje;
    }

    /**
     * @param zxje
     */
    public void setZxje(String zxje) {
        this.zxje = zxje == null ? null : zxje.trim();
    }

    /**
     * @return BZ
     */
    public String getBz() {
        return bz;
    }

    /**
     * @param bz
     */
    public void setBz(String bz) {
        this.bz = bz == null ? null : bz.trim();
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", actpk=").append(actpk);
        sb.append(", applicationid=").append(applicationid);
        sb.append(", csxh=").append(csxh);
        sb.append(", zfzh=").append(zfzh);
        sb.append(", cslx=").append(cslx);
        sb.append(", kssj=").append(kssj);
        sb.append(", jzsj=").append(jzsj);
        sb.append(", jgmc=").append(jgmc);
        sb.append(", rmbzl=").append(rmbzl);
        sb.append(", zxje=").append(zxje);
        sb.append(", bz=").append(bz);
        sb.append(", rksj=").append(rksj);
        sb.append(", yxx=").append(yxx);
        sb.append("]");
        return sb.toString();
    }
}