package com.unis.model.znzf;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_ZNZF_YPSL")
public class ZnzfYpsl {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "NR")
    private String nr;

    @Column(name = "LRDWDM")
    private String lrdwdm;

    @Column(name = "LRDWMC")
    private String lrdwmc;

    @Column(name = "LRRXM")
    private String lrrxm;

    @Column(name = "LRRJH")
    private String lrrjh;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private Short yxx;
    @Column(name = "JJDPK")
    private String jjdpk;
    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return NR
     */
    public String getNr() {
        return nr;
    }

    /**
     * @param nr
     */
    public void setNr(String nr) {
        this.nr = nr == null ? null : nr.trim();
    }

    /**
     * @return LRDWDM
     */
    public String getLrdwdm() {
        return lrdwdm;
    }

    /**
     * @param lrdwdm
     */
    public void setLrdwdm(String lrdwdm) {
        this.lrdwdm = lrdwdm == null ? null : lrdwdm.trim();
    }

    /**
     * @return LRDWMC
     */
    public String getLrdwmc() {
        return lrdwmc;
    }

    /**
     * @param lrdwmc
     */
    public void setLrdwmc(String lrdwmc) {
        this.lrdwmc = lrdwmc == null ? null : lrdwmc.trim();
    }

    /**
     * @return LRRXM
     */
    public String getLrrxm() {
        return lrrxm;
    }

    /**
     * @param lrrxm
     */
    public void setLrrxm(String lrrxm) {
        this.lrrxm = lrrxm == null ? null : lrrxm.trim();
    }

    /**
     * @return LRRJH
     */
    public String getLrrjh() {
        return lrrjh;
    }

    /**
     * @param lrrjh
     */
    public void setLrrjh(String lrrjh) {
        this.lrrjh = lrrjh == null ? null : lrrjh.trim();
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    public String getJjdpk() {
        return jjdpk;
    }

    public void setJjdpk(String jjdpk) {
        this.jjdpk = jjdpk;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", nr=").append(nr);
        sb.append(", lrdwdm=").append(lrdwdm);
        sb.append(", lrdwmc=").append(lrdwmc);
        sb.append(", lrrxm=").append(lrrxm);
        sb.append(", lrrjh=").append(lrrjh);
        sb.append(", rksj=").append(rksj);
        sb.append(", yxx=").append(yxx);
        sb.append(", jjdpk=").append(jjdpk);
        sb.append("]");
        return sb.toString();
    }
}