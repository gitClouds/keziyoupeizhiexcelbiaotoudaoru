package com.unis.model.znzf;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_ZNZF_YHK_ZT_JG")
public class YhkZtJg {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "QQPK")
    private String qqpk;

    @Column(name = "JJDPK")
    private String jjdpk;

    @Column(name = "APPLICATIONID")
    private String applicationid;

    @Column(name = "RESULTCODE")
    private String resultcode;

    @Column(name = "JBRXM")
    private String jbrxm;

    @Column(name = "JBRDH")
    private String jbrdh;

    @Column(name = "FKJGMC")
    private String fkjgmc;

    @Column(name = "FKSM")
    private String fksm;

    @Column(name = "ZJLX")
    private String zjlx;

    @Column(name = "ZJLXHM")
    private String zjlxhm;

    @Column(name = "CXZTMC")
    private String cxztmc;

    @Column(name = "CXSJ")
    private String cxsj;

    @Column(name = "DBRXM")
    private String dbrxm;

    @Column(name = "DBRZJLX")
    private String dbrzjlx;

    @Column(name = "DBRZJLXHM")
    private String dbrzjlxhm;

    @Column(name = "ZZDZ")
    private String zzdz;

    @Column(name = "ZZDH")
    private String zzdh;

    @Column(name = "GZDW")
    private String gzdw;

    @Column(name = "GZDWDZ")
    private String gzdwdz;

    @Column(name = "GZDWDH")
    private String gzdwdh;

    @Column(name = "YXDZ")
    private String yxdz;

    @Column(name = "FRDB")
    private String frdb;

    @Column(name = "FRDBZJLX")
    private String frdbzjlx;

    @Column(name = "FRDBZJLXHM")
    private String frdbzjlxhm;

    @Column(name = "KHYGSZCHM")
    private String khygszchm;

    @Column(name = "GSNSH")
    private String gsnsh;

    @Column(name = "DSNSH")
    private String dsnsh;

    @Column(name = "ZJSHRQ")
    private String zjshrq;

    @Column(name = "BZ")
    private String bz;

    @Column(name = "FKSJ")
    private Date fksj;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private Short yxx;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return QQPK
     */
    public String getQqpk() {
        return qqpk;
    }

    /**
     * @param qqpk
     */
    public void setQqpk(String qqpk) {
        this.qqpk = qqpk == null ? null : qqpk.trim();
    }

    /**
     * @return JJDPK
     */
    public String getJjdpk() {
        return jjdpk;
    }

    /**
     * @param jjdpk
     */
    public void setJjdpk(String jjdpk) {
        this.jjdpk = jjdpk == null ? null : jjdpk.trim();
    }

    /**
     * @return APPLICATIONID
     */
    public String getApplicationid() {
        return applicationid;
    }

    /**
     * @param applicationid
     */
    public void setApplicationid(String applicationid) {
        this.applicationid = applicationid == null ? null : applicationid.trim();
    }

    /**
     * @return RESULTCODE
     */
    public String getResultcode() {
        return resultcode;
    }

    /**
     * @param resultcode
     */
    public void setResultcode(String resultcode) {
        this.resultcode = resultcode == null ? null : resultcode.trim();
    }

    /**
     * @return JBRXM
     */
    public String getJbrxm() {
        return jbrxm;
    }

    /**
     * @param jbrxm
     */
    public void setJbrxm(String jbrxm) {
        this.jbrxm = jbrxm == null ? null : jbrxm.trim();
    }

    /**
     * @return JBRDH
     */
    public String getJbrdh() {
        return jbrdh;
    }

    /**
     * @param jbrdh
     */
    public void setJbrdh(String jbrdh) {
        this.jbrdh = jbrdh == null ? null : jbrdh.trim();
    }

    /**
     * @return FKJGMC
     */
    public String getFkjgmc() {
        return fkjgmc;
    }

    /**
     * @param fkjgmc
     */
    public void setFkjgmc(String fkjgmc) {
        this.fkjgmc = fkjgmc == null ? null : fkjgmc.trim();
    }

    /**
     * @return FKSM
     */
    public String getFksm() {
        return fksm;
    }

    /**
     * @param fksm
     */
    public void setFksm(String fksm) {
        this.fksm = fksm == null ? null : fksm.trim();
    }

    /**
     * @return ZJLX
     */
    public String getZjlx() {
        return zjlx;
    }

    /**
     * @param zjlx
     */
    public void setZjlx(String zjlx) {
        this.zjlx = zjlx == null ? null : zjlx.trim();
    }

    /**
     * @return ZJLXHM
     */
    public String getZjlxhm() {
        return zjlxhm;
    }

    /**
     * @param zjlxhm
     */
    public void setZjlxhm(String zjlxhm) {
        this.zjlxhm = zjlxhm == null ? null : zjlxhm.trim();
    }

    /**
     * @return CXZTMC
     */
    public String getCxztmc() {
        return cxztmc;
    }

    /**
     * @param cxztmc
     */
    public void setCxztmc(String cxztmc) {
        this.cxztmc = cxztmc == null ? null : cxztmc.trim();
    }

    /**
     * @return CXSJ
     */
    public String getCxsj() {
        return cxsj;
    }

    /**
     * @param cxsj
     */
    public void setCxsj(String cxsj) {
        this.cxsj = cxsj == null ? null : cxsj.trim();
    }

    /**
     * @return DBRXM
     */
    public String getDbrxm() {
        return dbrxm;
    }

    /**
     * @param dbrxm
     */
    public void setDbrxm(String dbrxm) {
        this.dbrxm = dbrxm == null ? null : dbrxm.trim();
    }

    /**
     * @return DBRZJLX
     */
    public String getDbrzjlx() {
        return dbrzjlx;
    }

    /**
     * @param dbrzjlx
     */
    public void setDbrzjlx(String dbrzjlx) {
        this.dbrzjlx = dbrzjlx == null ? null : dbrzjlx.trim();
    }

    /**
     * @return DBRZJLXHM
     */
    public String getDbrzjlxhm() {
        return dbrzjlxhm;
    }

    /**
     * @param dbrzjlxhm
     */
    public void setDbrzjlxhm(String dbrzjlxhm) {
        this.dbrzjlxhm = dbrzjlxhm == null ? null : dbrzjlxhm.trim();
    }

    /**
     * @return ZZDZ
     */
    public String getZzdz() {
        return zzdz;
    }

    /**
     * @param zzdz
     */
    public void setZzdz(String zzdz) {
        this.zzdz = zzdz == null ? null : zzdz.trim();
    }

    /**
     * @return ZZDH
     */
    public String getZzdh() {
        return zzdh;
    }

    /**
     * @param zzdh
     */
    public void setZzdh(String zzdh) {
        this.zzdh = zzdh == null ? null : zzdh.trim();
    }

    /**
     * @return GZDW
     */
    public String getGzdw() {
        return gzdw;
    }

    /**
     * @param gzdw
     */
    public void setGzdw(String gzdw) {
        this.gzdw = gzdw == null ? null : gzdw.trim();
    }

    /**
     * @return GZDWDZ
     */
    public String getGzdwdz() {
        return gzdwdz;
    }

    /**
     * @param gzdwdz
     */
    public void setGzdwdz(String gzdwdz) {
        this.gzdwdz = gzdwdz == null ? null : gzdwdz.trim();
    }

    /**
     * @return GZDWDH
     */
    public String getGzdwdh() {
        return gzdwdh;
    }

    /**
     * @param gzdwdh
     */
    public void setGzdwdh(String gzdwdh) {
        this.gzdwdh = gzdwdh == null ? null : gzdwdh.trim();
    }

    /**
     * @return YXDZ
     */
    public String getYxdz() {
        return yxdz;
    }

    /**
     * @param yxdz
     */
    public void setYxdz(String yxdz) {
        this.yxdz = yxdz == null ? null : yxdz.trim();
    }

    /**
     * @return FRDB
     */
    public String getFrdb() {
        return frdb;
    }

    /**
     * @param frdb
     */
    public void setFrdb(String frdb) {
        this.frdb = frdb == null ? null : frdb.trim();
    }

    /**
     * @return FRDBZJLX
     */
    public String getFrdbzjlx() {
        return frdbzjlx;
    }

    /**
     * @param frdbzjlx
     */
    public void setFrdbzjlx(String frdbzjlx) {
        this.frdbzjlx = frdbzjlx == null ? null : frdbzjlx.trim();
    }

    /**
     * @return FRDBZJLXHM
     */
    public String getFrdbzjlxhm() {
        return frdbzjlxhm;
    }

    /**
     * @param frdbzjlxhm
     */
    public void setFrdbzjlxhm(String frdbzjlxhm) {
        this.frdbzjlxhm = frdbzjlxhm == null ? null : frdbzjlxhm.trim();
    }

    /**
     * @return KHYGSZCHM
     */
    public String getKhygszchm() {
        return khygszchm;
    }

    /**
     * @param khygszchm
     */
    public void setKhygszchm(String khygszchm) {
        this.khygszchm = khygszchm == null ? null : khygszchm.trim();
    }

    /**
     * @return GSNSH
     */
    public String getGsnsh() {
        return gsnsh;
    }

    /**
     * @param gsnsh
     */
    public void setGsnsh(String gsnsh) {
        this.gsnsh = gsnsh == null ? null : gsnsh.trim();
    }

    /**
     * @return DSNSH
     */
    public String getDsnsh() {
        return dsnsh;
    }

    /**
     * @param dsnsh
     */
    public void setDsnsh(String dsnsh) {
        this.dsnsh = dsnsh == null ? null : dsnsh.trim();
    }

    /**
     * @return ZJSHRQ
     */
    public String getZjshrq() {
        return zjshrq;
    }

    /**
     * @param zjshrq
     */
    public void setZjshrq(String zjshrq) {
        this.zjshrq = zjshrq == null ? null : zjshrq.trim();
    }

    /**
     * @return BZ
     */
    public String getBz() {
        return bz;
    }

    /**
     * @param bz
     */
    public void setBz(String bz) {
        this.bz = bz == null ? null : bz.trim();
    }

    /**
     * @return FKSJ
     */
    public Date getFksj() {
        return fksj;
    }

    /**
     * @param fksj
     */
    public void setFksj(Date fksj) {
        this.fksj = fksj;
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", qqpk=").append(qqpk);
        sb.append(", jjdpk=").append(jjdpk);
        sb.append(", applicationid=").append(applicationid);
        sb.append(", resultcode=").append(resultcode);
        sb.append(", jbrxm=").append(jbrxm);
        sb.append(", jbrdh=").append(jbrdh);
        sb.append(", fkjgmc=").append(fkjgmc);
        sb.append(", fksm=").append(fksm);
        sb.append(", zjlx=").append(zjlx);
        sb.append(", zjlxhm=").append(zjlxhm);
        sb.append(", cxztmc=").append(cxztmc);
        sb.append(", cxsj=").append(cxsj);
        sb.append(", dbrxm=").append(dbrxm);
        sb.append(", dbrzjlx=").append(dbrzjlx);
        sb.append(", dbrzjlxhm=").append(dbrzjlxhm);
        sb.append(", zzdz=").append(zzdz);
        sb.append(", zzdh=").append(zzdh);
        sb.append(", gzdw=").append(gzdw);
        sb.append(", gzdwdz=").append(gzdwdz);
        sb.append(", gzdwdh=").append(gzdwdh);
        sb.append(", yxdz=").append(yxdz);
        sb.append(", frdb=").append(frdb);
        sb.append(", frdbzjlx=").append(frdbzjlx);
        sb.append(", frdbzjlxhm=").append(frdbzjlxhm);
        sb.append(", khygszchm=").append(khygszchm);
        sb.append(", gsnsh=").append(gsnsh);
        sb.append(", dsnsh=").append(dsnsh);
        sb.append(", zjshrq=").append(zjshrq);
        sb.append(", bz=").append(bz);
        sb.append(", fksj=").append(fksj);
        sb.append(", rksj=").append(rksj);
        sb.append(", yxx=").append(yxx);
        sb.append("]");
        return sb.toString();
    }
}