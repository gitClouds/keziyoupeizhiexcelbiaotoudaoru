package com.unis.model.znzf;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "TB_ZNZF_ANALYSIS_QUEUE")
public class AnalysisQueue {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "JJDPK")
    private String jjdpk;

    @Column(name = "QQPK")
    private String qqpk;

    @Column(name = "QQTYPE")
    private String qqtype;

    @Column(name = "APPLICATIONID")
    private String applicationid;

    @Column(name = "CARDNUMBER")
    private String cardnumber;

    @Column(name = "FXBS")
    private Short fxbs;

    @Column(name = "RKSJ")
    private Date rksj;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return JJDPK
     */
    public String getJjdpk() {
        return jjdpk;
    }

    /**
     * @param jjdpk
     */
    public void setJjdpk(String jjdpk) {
        this.jjdpk = jjdpk == null ? null : jjdpk.trim();
    }

    /**
     * @return QQPK
     */
    public String getQqpk() {
        return qqpk;
    }

    /**
     * @param qqpk
     */
    public void setQqpk(String qqpk) {
        this.qqpk = qqpk == null ? null : qqpk.trim();
    }

    /**
     * @return QQTYPE
     */
    public String getQqtype() {
        return qqtype;
    }

    /**
     * @param qqtype
     */
    public void setQqtype(String qqtype) {
        this.qqtype = qqtype == null ? null : qqtype.trim();
    }

    /**
     * @return APPLICATIONID
     */
    public String getApplicationid() {
        return applicationid;
    }

    /**
     * @param applicationid
     */
    public void setApplicationid(String applicationid) {
        this.applicationid = applicationid == null ? null : applicationid.trim();
    }

    /**
     * @return CARDNUMBER
     */
    public String getCardnumber() {
        return cardnumber;
    }

    /**
     * @param cardnumber
     */
    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber == null ? null : cardnumber.trim();
    }

    /**
     * @return FXBS
     */
    public Short getFxbs() {
        return fxbs;
    }

    /**
     * @param fxbs
     */
    public void setFxbs(Short fxbs) {
        this.fxbs = fxbs;
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", jjdpk=").append(jjdpk);
        sb.append(", qqpk=").append(qqpk);
        sb.append(", qqtype=").append(qqtype);
        sb.append(", applicationid=").append(applicationid);
        sb.append(", cardnumber=").append(cardnumber);
        sb.append(", fxbs=").append(fxbs);
        sb.append(", rksj=").append(rksj);
        sb.append("]");
        return sb.toString();
    }
}