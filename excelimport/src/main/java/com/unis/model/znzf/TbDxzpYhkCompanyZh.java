package com.unis.model.znzf;

import javax.persistence.*;

@Table(name = "TB_DXZP_YHK_COMPANY_ZH")
public class TbDxzpYhkCompanyZh {
    @Id
    @Column(name = "ID")
    private String id;

    @Column(name = "ZH")
    private String zh;

    @Column(name = "ZHXM")
    private String zhxm;

    @Column(name = "ZHJGDM")
    private String zhjgdm;

    @Column(name = "ZHJGMC")
    private String zhjgmc;

    /**
     * @return ID
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * @return ZH
     */
    public String getZh() {
        return zh;
    }

    /**
     * @param zh
     */
    public void setZh(String zh) {
        this.zh = zh == null ? null : zh.trim();
    }

    /**
     * @return ZHXM
     */
    public String getZhxm() {
        return zhxm;
    }

    /**
     * @param zhxm
     */
    public void setZhxm(String zhxm) {
        this.zhxm = zhxm == null ? null : zhxm.trim();
    }

    /**
     * @return ZHJGDM
     */
    public String getZhjgdm() {
        return zhjgdm;
    }

    /**
     * @param zhjgdm
     */
    public void setZhjgdm(String zhjgdm) {
        this.zhjgdm = zhjgdm == null ? null : zhjgdm.trim();
    }

    /**
     * @return ZHJGMC
     */
    public String getZhjgmc() {
        return zhjgmc;
    }

    /**
     * @param zhjgmc
     */
    public void setZhjgmc(String zhjgmc) {
        this.zhjgmc = zhjgmc == null ? null : zhjgmc.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", zh=").append(zh);
        sb.append(", zhxm=").append(zhxm);
        sb.append(", zhjgdm=").append(zhjgdm);
        sb.append(", zhjgmc=").append(zhjgmc);
        sb.append("]");
        return sb.toString();
    }
}