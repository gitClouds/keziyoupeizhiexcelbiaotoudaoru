package com.unis.model.znzf;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_ZNZF_SF_ZF_JG")
public class SfZfJg {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "QQPK")
    private String qqpk;

    @Column(name = "JJDPK")
    private String jjdpk;

    @Column(name = "APPLICATIONID")
    private String applicationid;

    @Column(name = "RESULTCODE")
    private String resultcode;

    @Column(name = "SUBJECTTYPE")
    private String subjecttype;

    @Column(name = "ZHTYPE")
    private String zhtype;

    @Column(name = "ZH")
    private String zh;

    @Column(name = "ACCOUNTNAME")
    private String accountname;

    @Column(name = "IDTYPE")
    private String idtype;

    @Column(name = "IDNUMBER")
    private String idnumber;

    @Column(name = "ACCOUNTBALANCE")
    private BigDecimal accountbalance;

    @Column(name = "SUSPENSIONAMOUNT")
    private BigDecimal suspensionamount;

    @Column(name = "STARTTIME")
    private Date starttime;

    @Column(name = "EXPIRETIME")
    private Date expiretime;

    @Column(name = "FAILURECAUSE")
    private String failurecause;

    @Column(name = "FEEDBACKREMARK")
    private String feedbackremark;

    @Column(name = "FEEDBACKORGNAME")
    private String feedbackorgname;

    @Column(name = "OPERATORNAME")
    private String operatorname;

    @Column(name = "OPERATORPHONENUMBER")
    private String operatorphonenumber;

    @Column(name = "FKSJ")
    private Date fksj;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private Short yxx;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return QQPK
     */
    public String getQqpk() {
        return qqpk;
    }

    /**
     * @param qqpk
     */
    public void setQqpk(String qqpk) {
        this.qqpk = qqpk == null ? null : qqpk.trim();
    }

    /**
     * @return JJDPK
     */
    public String getJjdpk() {
        return jjdpk;
    }

    /**
     * @param jjdpk
     */
    public void setJjdpk(String jjdpk) {
        this.jjdpk = jjdpk == null ? null : jjdpk.trim();
    }

    /**
     * @return APPLICATIONID
     */
    public String getApplicationid() {
        return applicationid;
    }

    /**
     * @param applicationid
     */
    public void setApplicationid(String applicationid) {
        this.applicationid = applicationid == null ? null : applicationid.trim();
    }

    /**
     * @return RESULTCODE
     */
    public String getResultcode() {
        return resultcode;
    }

    /**
     * @param resultcode
     */
    public void setResultcode(String resultcode) {
        this.resultcode = resultcode == null ? null : resultcode.trim();
    }

    /**
     * @return SUBJECTTYPE
     */
    public String getSubjecttype() {
        return subjecttype;
    }

    /**
     * @param subjecttype
     */
    public void setSubjecttype(String subjecttype) {
        this.subjecttype = subjecttype == null ? null : subjecttype.trim();
    }

    /**
     * @return ZHTYPE
     */
    public String getZhtype() {
        return zhtype;
    }

    /**
     * @param zhtype
     */
    public void setZhtype(String zhtype) {
        this.zhtype = zhtype == null ? null : zhtype.trim();
    }

    /**
     * @return ZH
     */
    public String getZh() {
        return zh;
    }

    /**
     * @param zh
     */
    public void setZh(String zh) {
        this.zh = zh == null ? null : zh.trim();
    }

    /**
     * @return ACCOUNTNAME
     */
    public String getAccountname() {
        return accountname;
    }

    /**
     * @param accountname
     */
    public void setAccountname(String accountname) {
        this.accountname = accountname == null ? null : accountname.trim();
    }

    /**
     * @return IDTYPE
     */
    public String getIdtype() {
        return idtype;
    }

    /**
     * @param idtype
     */
    public void setIdtype(String idtype) {
        this.idtype = idtype == null ? null : idtype.trim();
    }

    /**
     * @return IDNUMBER
     */
    public String getIdnumber() {
        return idnumber;
    }

    /**
     * @param idnumber
     */
    public void setIdnumber(String idnumber) {
        this.idnumber = idnumber == null ? null : idnumber.trim();
    }

    /**
     * @return ACCOUNTBALANCE
     */
    public BigDecimal getAccountbalance() {
        return accountbalance;
    }

    /**
     * @param accountbalance
     */
    public void setAccountbalance(BigDecimal accountbalance) {
        this.accountbalance = accountbalance;
    }

    /**
     * @return SUSPENSIONAMOUNT
     */
    public BigDecimal getSuspensionamount() {
        return suspensionamount;
    }

    /**
     * @param suspensionamount
     */
    public void setSuspensionamount(BigDecimal suspensionamount) {
        this.suspensionamount = suspensionamount;
    }

    /**
     * @return STARTTIME
     */
    public Date getStarttime() {
        return starttime;
    }

    /**
     * @param starttime
     */
    public void setStarttime(Date starttime) {
        this.starttime = starttime;
    }

    /**
     * @return EXPIRETIME
     */
    public Date getExpiretime() {
        return expiretime;
    }

    /**
     * @param expiretime
     */
    public void setExpiretime(Date expiretime) {
        this.expiretime = expiretime;
    }

    /**
     * @return FAILURECAUSE
     */
    public String getFailurecause() {
        return failurecause;
    }

    /**
     * @param failurecause
     */
    public void setFailurecause(String failurecause) {
        this.failurecause = failurecause == null ? null : failurecause.trim();
    }

    /**
     * @return FEEDBACKREMARK
     */
    public String getFeedbackremark() {
        return feedbackremark;
    }

    /**
     * @param feedbackremark
     */
    public void setFeedbackremark(String feedbackremark) {
        this.feedbackremark = feedbackremark == null ? null : feedbackremark.trim();
    }

    /**
     * @return FEEDBACKORGNAME
     */
    public String getFeedbackorgname() {
        return feedbackorgname;
    }

    /**
     * @param feedbackorgname
     */
    public void setFeedbackorgname(String feedbackorgname) {
        this.feedbackorgname = feedbackorgname == null ? null : feedbackorgname.trim();
    }

    /**
     * @return OPERATORNAME
     */
    public String getOperatorname() {
        return operatorname;
    }

    /**
     * @param operatorname
     */
    public void setOperatorname(String operatorname) {
        this.operatorname = operatorname == null ? null : operatorname.trim();
    }

    /**
     * @return OPERATORPHONENUMBER
     */
    public String getOperatorphonenumber() {
        return operatorphonenumber;
    }

    /**
     * @param operatorphonenumber
     */
    public void setOperatorphonenumber(String operatorphonenumber) {
        this.operatorphonenumber = operatorphonenumber == null ? null : operatorphonenumber.trim();
    }

    /**
     * @return FKSJ
     */
    public Date getFksj() {
        return fksj;
    }

    /**
     * @param fksj
     */
    public void setFksj(Date fksj) {
        this.fksj = fksj;
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", qqpk=").append(qqpk);
        sb.append(", jjdpk=").append(jjdpk);
        sb.append(", applicationid=").append(applicationid);
        sb.append(", resultcode=").append(resultcode);
        sb.append(", subjecttype=").append(subjecttype);
        sb.append(", zhtype=").append(zhtype);
        sb.append(", zh=").append(zh);
        sb.append(", accountname=").append(accountname);
        sb.append(", idtype=").append(idtype);
        sb.append(", idnumber=").append(idnumber);
        sb.append(", accountbalance=").append(accountbalance);
        sb.append(", suspensionamount=").append(suspensionamount);
        sb.append(", starttime=").append(starttime);
        sb.append(", expiretime=").append(expiretime);
        sb.append(", failurecause=").append(failurecause);
        sb.append(", feedbackremark=").append(feedbackremark);
        sb.append(", feedbackorgname=").append(feedbackorgname);
        sb.append(", operatorname=").append(operatorname);
        sb.append(", operatorphonenumber=").append(operatorphonenumber);
        sb.append(", fksj=").append(fksj);
        sb.append(", rksj=").append(rksj);
        sb.append(", yxx=").append(yxx);
        sb.append("]");
        return sb.toString();
    }
}