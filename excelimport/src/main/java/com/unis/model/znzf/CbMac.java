package com.unis.model.znzf;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_ZNZF_CB_MAC")
public class CbMac {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "CBBH")
    private String cbbh;

    @Column(name = "SERTYPE")
    private Short sertype;

    @Column(name = "MAZDZ")
    private String mazdz;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private Short yxx;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return CBBH
     */
    public String getCbbh() {
        return cbbh;
    }

    /**
     * @param cbbh
     */
    public void setCbbh(String cbbh) {
        this.cbbh = cbbh == null ? null : cbbh.trim();
    }

    /**
     * @return SERTYPE
     */
    public Short getSertype() {
        return sertype;
    }

    /**
     * @param sertype
     */
    public void setSertype(Short sertype) {
        this.sertype = sertype;
    }

    /**
     * @return MAZDZ
     */
    public String getMazdz() {
        return mazdz;
    }

    /**
     * @param mazdz
     */
    public void setMazdz(String mazdz) {
        this.mazdz = mazdz == null ? null : mazdz.trim();
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", cbbh=").append(cbbh);
        sb.append(", sertype=").append(sertype);
        sb.append(", mazdz=").append(mazdz);
        sb.append(", rksj=").append(rksj);
        sb.append(", yxx=").append(yxx);
        sb.append("]");
        return sb.toString();
    }
}