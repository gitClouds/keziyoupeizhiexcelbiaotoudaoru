package com.unis.model.znzf;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_ZNZF_SF_QZH_QQ")
public class SfQzhQq {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "JJDPK")
    private String jjdpk;

    @Column(name = "APPLICATIONID")
    private String applicationid;

    @Column(name = "SUBJECTTYPE")
    private Short subjecttype;

    @Column(name = "PAYCODE")
    private String paycode;

    @Column(name = "PAYNAME")
    private String payname;

    @Column(name = "ACCTYPE")
    private String acctype;

    @Column(name = "ACCNUMBER")
    private String accnumber;

    @Column(name = "ACCOUNTNAME")
    private String accountname;

    @Column(name = "ACCOUNTYPE")
    private String accountype;

    @Column(name = "REASON")
    private String reason;

    @Column(name = "REMARK")
    private String remark;

    @Column(name = "LRDWDM")
    private String lrdwdm;

    @Column(name = "LRDWMC")
    private String lrdwmc;

    @Column(name = "LRRXM")
    private String lrrxm;

    @Column(name = "LRRJH")
    private String lrrjh;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private Short yxx;

    @Column(name = "REQFLAG")
    private Short reqflag;

    @Column(name = "REQDATE")
    private Date reqdate;

    @Column(name = "REQCOUNT")
    private Short reqcount;

    @Column(name = "RESFLAG")
    private Short resflag;

    @Column(name = "RESDATE")
    private Date resdate;

    @Column(name = "RESULTCODE")
    private String resultcode;

    @Column(name = "FEEDBACKREMARK")
    private String feedbackremark;

    @Column(name = "FLWS")
    private String flws;

    @Column(name = "FXBS")
    private Short fxbs;

    @Column(name = "ZZSJ")
    private Date zzsj;

    @Column(name = "NLEVEL")
    private Short nlevel;
    
    @Column(name = "SJLY")
    private String sjly;

    public Short getFxbs() {
        return fxbs;
    }

    public void setFxbs(Short fxbs) {
        this.fxbs = fxbs;
    }

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return JJDPK
     */
    public String getJjdpk() {
        return jjdpk;
    }

    /**
     * @param jjdpk
     */
    public void setJjdpk(String jjdpk) {
        this.jjdpk = jjdpk == null ? null : jjdpk.trim();
    }

    /**
     * @return APPLICATIONID
     */
    public String getApplicationid() {
        return applicationid;
    }

    /**
     * @param applicationid
     */
    public void setApplicationid(String applicationid) {
        this.applicationid = applicationid == null ? null : applicationid.trim();
    }

    /**
     * @return SUBJECTTYPE
     */
    public Short getSubjecttype() {
        return subjecttype;
    }

    /**
     * @param subjecttype
     */
    public void setSubjecttype(Short subjecttype) {
        this.subjecttype = subjecttype;
    }

    /**
     * @return PAYCODE
     */
    public String getPaycode() {
        return paycode;
    }

    /**
     * @param paycode
     */
    public void setPaycode(String paycode) {
        this.paycode = paycode == null ? null : paycode.trim();
    }

    /**
     * @return PAYNAME
     */
    public String getPayname() {
        return payname;
    }

    /**
     * @param payname
     */
    public void setPayname(String payname) {
        this.payname = payname == null ? null : payname.trim();
    }

    /**
     * @return ACCTYPE
     */
    public String getAcctype() {
        return acctype;
    }

    /**
     * @param acctype
     */
    public void setAcctype(String acctype) {
        this.acctype = acctype == null ? null : acctype.trim();
    }

    /**
     * @return ACCNUMBER
     */
    public String getAccnumber() {
        return accnumber;
    }

    /**
     * @param accnumber
     */
    public void setAccnumber(String accnumber) {
        this.accnumber = accnumber == null ? null : accnumber.trim();
    }

    /**
     * @return ACCOUNTNAME
     */
    public String getAccountname() {
        return accountname;
    }

    /**
     * @param accountname
     */
    public void setAccountname(String accountname) {
        this.accountname = accountname == null ? null : accountname.trim();
    }

    /**
     * @return ACCOUNTYPE
     */
    public String getAccountype() {
        return accountype;
    }

    /**
     * @param accountype
     */
    public void setAccountype(String accountype) {
        this.accountype = accountype == null ? null : accountype.trim();
    }

    /**
     * @return REASON
     */
    public String getReason() {
        return reason;
    }

    /**
     * @param reason
     */
    public void setReason(String reason) {
        this.reason = reason == null ? null : reason.trim();
    }

    /**
     * @return REMARK
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * @return LRDWDM
     */
    public String getLrdwdm() {
        return lrdwdm;
    }

    /**
     * @param lrdwdm
     */
    public void setLrdwdm(String lrdwdm) {
        this.lrdwdm = lrdwdm == null ? null : lrdwdm.trim();
    }

    /**
     * @return LRDWMC
     */
    public String getLrdwmc() {
        return lrdwmc;
    }

    /**
     * @param lrdwmc
     */
    public void setLrdwmc(String lrdwmc) {
        this.lrdwmc = lrdwmc == null ? null : lrdwmc.trim();
    }

    /**
     * @return LRRXM
     */
    public String getLrrxm() {
        return lrrxm;
    }

    /**
     * @param lrrxm
     */
    public void setLrrxm(String lrrxm) {
        this.lrrxm = lrrxm == null ? null : lrrxm.trim();
    }

    /**
     * @return LRRJH
     */
    public String getLrrjh() {
        return lrrjh;
    }

    /**
     * @param lrrjh
     */
    public void setLrrjh(String lrrjh) {
        this.lrrjh = lrrjh == null ? null : lrrjh.trim();
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    /**
     * @return REQFLAG
     */
    public Short getReqflag() {
        return reqflag;
    }

    /**
     * @param reqflag
     */
    public void setReqflag(Short reqflag) {
        this.reqflag = reqflag;
    }

    /**
     * @return REQDATE
     */
    public Date getReqdate() {
        return reqdate;
    }

    /**
     * @param reqdate
     */
    public void setReqdate(Date reqdate) {
        this.reqdate = reqdate;
    }

    /**
     * @return REQCOUNT
     */
    public Short getReqcount() {
        return reqcount;
    }

    /**
     * @param reqcount
     */
    public void setReqcount(Short reqcount) {
        this.reqcount = reqcount;
    }

    /**
     * @return RESFLAG
     */
    public Short getResflag() {
        return resflag;
    }

    /**
     * @param resflag
     */
    public void setResflag(Short resflag) {
        this.resflag = resflag;
    }

    /**
     * @return RESDATE
     */
    public Date getResdate() {
        return resdate;
    }

    /**
     * @param resdate
     */
    public void setResdate(Date resdate) {
        this.resdate = resdate;
    }

    /**
     * @return RESULTCODE
     */
    public String getResultcode() {
        return resultcode;
    }

    /**
     * @param resultcode
     */
    public void setResultcode(String resultcode) {
        this.resultcode = resultcode == null ? null : resultcode.trim();
    }

    /**
     * @return FEEDBACKREMARK
     */
    public String getFeedbackremark() {
        return feedbackremark;
    }

    /**
     * @param feedbackremark
     */
    public void setFeedbackremark(String feedbackremark) {
        this.feedbackremark = feedbackremark == null ? null : feedbackremark.trim();
    }

    /**
     * @return FLWS
     */
    public String getFlws() {
        return flws;
    }

    /**
     * @param flws
     */
    public void setFlws(String flws) {
        this.flws = flws == null ? null : flws.trim();
    }

    public Date getZzsj() {
        return zzsj;
    }

    public void setZzsj(Date zzsj) {
        this.zzsj = zzsj;
    }

    public Short getNlevel() {
        return nlevel;
    }

    public void setNlevel(Short nlevel) {
        this.nlevel = nlevel;
    }

    @Override
    public String toString() {
        return "SfQzhQq{" +
                "pk='" + pk + '\'' +
                ", jjdpk='" + jjdpk + '\'' +
                ", applicationid='" + applicationid + '\'' +
                ", subjecttype=" + subjecttype +
                ", paycode='" + paycode + '\'' +
                ", payname='" + payname + '\'' +
                ", acctype='" + acctype + '\'' +
                ", accnumber='" + accnumber + '\'' +
                ", accountname='" + accountname + '\'' +
                ", accountype='" + accountype + '\'' +
                ", reason='" + reason + '\'' +
                ", remark='" + remark + '\'' +
                ", lrdwdm='" + lrdwdm + '\'' +
                ", lrdwmc='" + lrdwmc + '\'' +
                ", lrrxm='" + lrrxm + '\'' +
                ", lrrjh='" + lrrjh + '\'' +
                ", rksj=" + rksj +
                ", yxx=" + yxx +
                ", reqflag=" + reqflag +
                ", reqdate=" + reqdate +
                ", reqcount=" + reqcount +
                ", resflag=" + resflag +
                ", resdate=" + resdate +
                ", resultcode='" + resultcode + '\'' +
                ", feedbackremark='" + feedbackremark + '\'' +
                ", flws='" + flws + '\'' +
                ", fxbs=" + fxbs +
                ", nlevel=" + nlevel +
                ", zzsj=" + zzsj +
                '}';
    }

	public String getSjly() {
		return sjly;
	}

	public void setSjly(String sjly) {
		this.sjly = sjly;
	}
}