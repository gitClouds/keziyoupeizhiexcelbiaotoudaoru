package com.unis.model.znzf;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_ZNZF_SF_ZT_JG")
public class SfZtJg {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "QQPK")
    private String qqpk;

    @Column(name = "JJDPK")
    private String jjdpk;

    @Column(name = "APPLICATIONID")
    private String applicationid;

    @Column(name = "RESULTCODE")
    private String resultcode;

    @Column(name = "JBRXM")
    private String jbrxm;

    @Column(name = "JBRDH")
    private String jbrdh;

    @Column(name = "CXCSLX")
    private String cxcslx;

    @Column(name = "CS")
    private String cs;

    @Column(name = "ZHLB")
    private String zhlb;

    @Column(name = "KHZTXM")
    private String khztxm;

    @Column(name = "KHZTZJLX")
    private String khztzjlx;

    @Column(name = "KHZTZJHM")
    private String khztzjhm;

    @Column(name = "ZJYXQ")
    private String zjyxq;

    @Column(name = "KHZTBDSJH")
    private String khztbdsjh;

    @Column(name = "SHMC")
    private String shmc;

    @Column(name = "SHHM")
    private String shhm;

    @Column(name = "FKJGMC")
    private String fkjgmc;

    @Column(name = "CXFKSM")
    private String cxfksm;

    @Column(name = "BZ")
    private String bz;

    @Column(name = "FKSJ")
    private Date fksj;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private Short yxx;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return QQPK
     */
    public String getQqpk() {
        return qqpk;
    }

    /**
     * @param qqpk
     */
    public void setQqpk(String qqpk) {
        this.qqpk = qqpk == null ? null : qqpk.trim();
    }

    /**
     * @return JJDPK
     */
    public String getJjdpk() {
        return jjdpk;
    }

    /**
     * @param jjdpk
     */
    public void setJjdpk(String jjdpk) {
        this.jjdpk = jjdpk == null ? null : jjdpk.trim();
    }

    /**
     * @return APPLICATIONID
     */
    public String getApplicationid() {
        return applicationid;
    }

    /**
     * @param applicationid
     */
    public void setApplicationid(String applicationid) {
        this.applicationid = applicationid == null ? null : applicationid.trim();
    }

    /**
     * @return RESULTCODE
     */
    public String getResultcode() {
        return resultcode;
    }

    /**
     * @param resultcode
     */
    public void setResultcode(String resultcode) {
        this.resultcode = resultcode == null ? null : resultcode.trim();
    }

    /**
     * @return JBRXM
     */
    public String getJbrxm() {
        return jbrxm;
    }

    /**
     * @param jbrxm
     */
    public void setJbrxm(String jbrxm) {
        this.jbrxm = jbrxm == null ? null : jbrxm.trim();
    }

    /**
     * @return JBRDH
     */
    public String getJbrdh() {
        return jbrdh;
    }

    /**
     * @param jbrdh
     */
    public void setJbrdh(String jbrdh) {
        this.jbrdh = jbrdh == null ? null : jbrdh.trim();
    }

    /**
     * @return CXCSLX
     */
    public String getCxcslx() {
        return cxcslx;
    }

    /**
     * @param cxcslx
     */
    public void setCxcslx(String cxcslx) {
        this.cxcslx = cxcslx == null ? null : cxcslx.trim();
    }

    /**
     * @return CS
     */
    public String getCs() {
        return cs;
    }

    /**
     * @param cs
     */
    public void setCs(String cs) {
        this.cs = cs == null ? null : cs.trim();
    }

    /**
     * @return ZHLB
     */
    public String getZhlb() {
        return zhlb;
    }

    /**
     * @param zhlb
     */
    public void setZhlb(String zhlb) {
        this.zhlb = zhlb == null ? null : zhlb.trim();
    }

    /**
     * @return KHZTXM
     */
    public String getKhztxm() {
        return khztxm;
    }

    /**
     * @param khztxm
     */
    public void setKhztxm(String khztxm) {
        this.khztxm = khztxm == null ? null : khztxm.trim();
    }

    /**
     * @return KHZTZJLX
     */
    public String getKhztzjlx() {
        return khztzjlx;
    }

    /**
     * @param khztzjlx
     */
    public void setKhztzjlx(String khztzjlx) {
        this.khztzjlx = khztzjlx == null ? null : khztzjlx.trim();
    }

    /**
     * @return KHZTZJHM
     */
    public String getKhztzjhm() {
        return khztzjhm;
    }

    /**
     * @param khztzjhm
     */
    public void setKhztzjhm(String khztzjhm) {
        this.khztzjhm = khztzjhm == null ? null : khztzjhm.trim();
    }

    /**
     * @return ZJYXQ
     */
    public String getZjyxq() {
        return zjyxq;
    }

    /**
     * @param zjyxq
     */
    public void setZjyxq(String zjyxq) {
        this.zjyxq = zjyxq == null ? null : zjyxq.trim();
    }

    /**
     * @return KHZTBDSJH
     */
    public String getKhztbdsjh() {
        return khztbdsjh;
    }

    /**
     * @param khztbdsjh
     */
    public void setKhztbdsjh(String khztbdsjh) {
        this.khztbdsjh = khztbdsjh == null ? null : khztbdsjh.trim();
    }

    /**
     * @return SHMC
     */
    public String getShmc() {
        return shmc;
    }

    /**
     * @param shmc
     */
    public void setShmc(String shmc) {
        this.shmc = shmc == null ? null : shmc.trim();
    }

    /**
     * @return SHHM
     */
    public String getShhm() {
        return shhm;
    }

    /**
     * @param shhm
     */
    public void setShhm(String shhm) {
        this.shhm = shhm == null ? null : shhm.trim();
    }

    /**
     * @return FKJGMC
     */
    public String getFkjgmc() {
        return fkjgmc;
    }

    /**
     * @param fkjgmc
     */
    public void setFkjgmc(String fkjgmc) {
        this.fkjgmc = fkjgmc == null ? null : fkjgmc.trim();
    }

    /**
     * @return CXFKSM
     */
    public String getCxfksm() {
        return cxfksm;
    }

    /**
     * @param cxfksm
     */
    public void setCxfksm(String cxfksm) {
        this.cxfksm = cxfksm == null ? null : cxfksm.trim();
    }

    /**
     * @return BZ
     */
    public String getBz() {
        return bz;
    }

    /**
     * @param bz
     */
    public void setBz(String bz) {
        this.bz = bz == null ? null : bz.trim();
    }

    /**
     * @return FKSJ
     */
    public Date getFksj() {
        return fksj;
    }

    /**
     * @param fksj
     */
    public void setFksj(Date fksj) {
        this.fksj = fksj;
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", qqpk=").append(qqpk);
        sb.append(", jjdpk=").append(jjdpk);
        sb.append(", applicationid=").append(applicationid);
        sb.append(", resultcode=").append(resultcode);
        sb.append(", jbrxm=").append(jbrxm);
        sb.append(", jbrdh=").append(jbrdh);
        sb.append(", cxcslx=").append(cxcslx);
        sb.append(", cs=").append(cs);
        sb.append(", zhlb=").append(zhlb);
        sb.append(", khztxm=").append(khztxm);
        sb.append(", khztzjlx=").append(khztzjlx);
        sb.append(", khztzjhm=").append(khztzjhm);
        sb.append(", zjyxq=").append(zjyxq);
        sb.append(", khztbdsjh=").append(khztbdsjh);
        sb.append(", shmc=").append(shmc);
        sb.append(", shhm=").append(shhm);
        sb.append(", fkjgmc=").append(fkjgmc);
        sb.append(", cxfksm=").append(cxfksm);
        sb.append(", bz=").append(bz);
        sb.append(", fksj=").append(fksj);
        sb.append(", rksj=").append(rksj);
        sb.append(", yxx=").append(yxx);
        sb.append("]");
        return sb.toString();
    }
}