package com.unis.model.znzf;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_ZNZF_REMIND_META")
public class ZnzfRemindMeta {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "BANKCODE")
    private String bankcode;

    @Column(name = "REMINDTYPE")
    private Short remindtype;

    @Column(name = "REMARK")
    private String remark;

    @Column(name = "CHECKFIELD")
    private String checkfield;

    @Column(name = "CHECKDATA")
    private String checkdata;

    @Column(name = "POSITIONVAL")
    private String positionval;

    @Column(name = "YXX")
    private Short yxx;

    @Column(name = "RKSJ")
    private Date rksj;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return BANKCODE
     */
    public String getBankcode() {
        return bankcode;
    }

    /**
     * @param bankcode
     */
    public void setBankcode(String bankcode) {
        this.bankcode = bankcode == null ? null : bankcode.trim();
    }

    /**
     * @return REMINDTYPE
     */
    public Short getRemindtype() {
        return remindtype;
    }

    /**
     * @param remindtype
     */
    public void setRemindtype(Short remindtype) {
        this.remindtype = remindtype;
    }

    /**
     * @return REMARK
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * @return CHECKFIELD
     */
    public String getCheckfield() {
        return checkfield;
    }

    /**
     * @param checkfield
     */
    public void setCheckfield(String checkfield) {
        this.checkfield = checkfield == null ? null : checkfield.trim();
    }

    /**
     * @return CHECKDATA
     */
    public String getCheckdata() {
        return checkdata;
    }

    /**
     * @param checkdata
     */
    public void setCheckdata(String checkdata) {
        this.checkdata = checkdata == null ? null : checkdata.trim();
    }

    /**
     * @return POSITIONVAL
     */
    public String getPositionval() {
        return positionval;
    }

    /**
     * @param positionval
     */
    public void setPositionval(String positionval) {
        this.positionval = positionval == null ? null : positionval.trim();
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", bankcode=").append(bankcode);
        sb.append(", remindtype=").append(remindtype);
        sb.append(", remark=").append(remark);
        sb.append(", checkfield=").append(checkfield);
        sb.append(", checkdata=").append(checkdata);
        sb.append(", positionval=").append(positionval);
        sb.append(", yxx=").append(yxx);
        sb.append(", rksj=").append(rksj);
        sb.append("]");
        return sb.toString();
    }
}