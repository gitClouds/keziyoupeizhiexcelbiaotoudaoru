package com.unis.model.znzf;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_ZNZF_QUEUE")
public class ZnzfQueue {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "JJDPK")
    private String jjdpk;

    @Column(name = "SERPK")
    private String serpk;

    @Column(name = "SERTYPE")
    private String sertype;

    @Column(name = "BANKCODE")
    private String bankcode;

    @Column(name = "BANKNAME")
    private String bankname;

    @Column(name = "CARDNUMBER")
    private String cardnumber;

    @Column(name = "LRDWDM")
    private String lrdwdm;

    @Column(name = "LRDWMC")
    private String lrdwmc;

    @Column(name = "LRRXM")
    private String lrrxm;

    @Column(name = "LRRJH")
    private String lrrjh;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private Short yxx;

    @Column(name = "YXJ")
    private Short yxj;
    @Column(name = "REQCOUNT")
    private Short reqCount;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return JJDPK
     */
    public String getJjdpk() {
        return jjdpk;
    }

    /**
     * @param jjdpk
     */
    public void setJjdpk(String jjdpk) {
        this.jjdpk = jjdpk == null ? null : jjdpk.trim();
    }

    /**
     * @return SERPK
     */
    public String getSerpk() {
        return serpk;
    }

    /**
     * @param serpk
     */
    public void setSerpk(String serpk) {
        this.serpk = serpk == null ? null : serpk.trim();
    }

    /**
     * @return SERTYPE
     */
    public String getSertype() {
        return sertype;
    }

    /**
     * @param sertype
     */
    public void setSertype(String sertype) {
        this.sertype = sertype == null ? null : sertype.trim();
    }

    /**
     * @return BANKCODE
     */
    public String getBankcode() {
        return bankcode;
    }

    /**
     * @param bankcode
     */
    public void setBankcode(String bankcode) {
        this.bankcode = bankcode == null ? null : bankcode.trim();
    }

    /**
     * @return BANKNAME
     */
    public String getBankname() {
        return bankname;
    }

    /**
     * @param bankname
     */
    public void setBankname(String bankname) {
        this.bankname = bankname == null ? null : bankname.trim();
    }

    /**
     * @return CARDNUMBER
     */
    public String getCardnumber() {
        return cardnumber;
    }

    /**
     * @param cardnumber
     */
    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber == null ? null : cardnumber.trim();
    }

    /**
     * @return LRDWDM
     */
    public String getLrdwdm() {
        return lrdwdm;
    }

    /**
     * @param lrdwdm
     */
    public void setLrdwdm(String lrdwdm) {
        this.lrdwdm = lrdwdm == null ? null : lrdwdm.trim();
    }

    /**
     * @return LRDWMC
     */
    public String getLrdwmc() {
        return lrdwmc;
    }

    /**
     * @param lrdwmc
     */
    public void setLrdwmc(String lrdwmc) {
        this.lrdwmc = lrdwmc == null ? null : lrdwmc.trim();
    }

    /**
     * @return LRRXM
     */
    public String getLrrxm() {
        return lrrxm;
    }

    /**
     * @param lrrxm
     */
    public void setLrrxm(String lrrxm) {
        this.lrrxm = lrrxm == null ? null : lrrxm.trim();
    }

    /**
     * @return LRRJH
     */
    public String getLrrjh() {
        return lrrjh;
    }

    /**
     * @param lrrjh
     */
    public void setLrrjh(String lrrjh) {
        this.lrrjh = lrrjh == null ? null : lrrjh.trim();
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    /**
     * @return YXJ
     */
    public Short getYxj() {
        return yxj;
    }

    /**
     * @param yxj
     */
    public void setYxj(Short yxj) {
        this.yxj = yxj;
    }

    /**
     * @param reqCount
     */
    public void setReqCount(Short reqCount) {
        this.reqCount = reqCount;
    }
    /**
     * @return REQCOUNT
     */
    public Short getReqCount() {
        return reqCount;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", jjdpk=").append(jjdpk);
        sb.append(", serpk=").append(serpk);
        sb.append(", sertype=").append(sertype);
        sb.append(", bankcode=").append(bankcode);
        sb.append(", bankname=").append(bankname);
        sb.append(", cardnumber=").append(cardnumber);
        sb.append(", lrdwdm=").append(lrdwdm);
        sb.append(", lrdwmc=").append(lrdwmc);
        sb.append(", lrrxm=").append(lrrxm);
        sb.append(", lrrjh=").append(lrrjh);
        sb.append(", rksj=").append(rksj);
        sb.append(", yxx=").append(yxx);
        sb.append(", yxj=").append(yxj);
        sb.append(", reqCount=").append(reqCount);
        sb.append("]");
        return sb.toString();
    }
}