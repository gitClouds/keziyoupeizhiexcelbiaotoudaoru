package com.unis.model.znzf;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TB_ZNZF_SF_JG_ACT")
public class SfJgAct {
    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "QQPK")
    private String qqpk;

    @Column(name = "JJDPK")
    private String jjdpk;

    @Column(name = "APPLICATIONID")
    private String applicationid;

    @Column(name = "ZFZH")
    private String zfzh;

    @Column(name = "ZHLB")
    private String zhlb;

    @Column(name = "KHXM")
    private String khxm;

    @Column(name = "KHZJLX")
    private String khzjlx;

    @Column(name = "KHZJHM")
    private String khzjhm;

    @Column(name = "ZJYXQ")
    private String zjyxq;

    @Column(name = "BDSJH")
    private String bdsjh;

    @Column(name = "DLHM")
    private String dlhm;

    @Column(name = "WX")
    private String wx;

    @Column(name = "QQ")
    private String qq;

    @Column(name = "IP")
    private String ip;

    @Column(name = "SBHM")
    private String sbhm;

    @Column(name = "SHMC")
    private String shmc;

    @Column(name = "SHHM")
    private String shhm;

    @Column(name = "POSBH")
    private String posbh;

    @Column(name = "POSDZ")
    private String posdz;

    @Column(name = "POSDWDZ")
    private String posdwdz;

    @Column(name = "RKSJ")
    private Date rksj;

    @Column(name = "YXX")
    private Short yxx;

    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return QQPK
     */
    public String getQqpk() {
        return qqpk;
    }

    /**
     * @param qqpk
     */
    public void setQqpk(String qqpk) {
        this.qqpk = qqpk == null ? null : qqpk.trim();
    }

    /**
     * @return JJDPK
     */
    public String getJjdpk() {
        return jjdpk;
    }

    /**
     * @param jjdpk
     */
    public void setJjdpk(String jjdpk) {
        this.jjdpk = jjdpk == null ? null : jjdpk.trim();
    }

    /**
     * @return APPLICATIONID
     */
    public String getApplicationid() {
        return applicationid;
    }

    /**
     * @param applicationid
     */
    public void setApplicationid(String applicationid) {
        this.applicationid = applicationid == null ? null : applicationid.trim();
    }

    /**
     * @return ZFZH
     */
    public String getZfzh() {
        return zfzh;
    }

    /**
     * @param zfzh
     */
    public void setZfzh(String zfzh) {
        this.zfzh = zfzh == null ? null : zfzh.trim();
    }

    /**
     * @return ZHLB
     */
    public String getZhlb() {
        return zhlb;
    }

    /**
     * @param zhlb
     */
    public void setZhlb(String zhlb) {
        this.zhlb = zhlb == null ? null : zhlb.trim();
    }

    /**
     * @return KHXM
     */
    public String getKhxm() {
        return khxm;
    }

    /**
     * @param khxm
     */
    public void setKhxm(String khxm) {
        this.khxm = khxm == null ? null : khxm.trim();
    }

    /**
     * @return KHZJLX
     */
    public String getKhzjlx() {
        return khzjlx;
    }

    /**
     * @param khzjlx
     */
    public void setKhzjlx(String khzjlx) {
        this.khzjlx = khzjlx == null ? null : khzjlx.trim();
    }

    /**
     * @return KHZJHM
     */
    public String getKhzjhm() {
        return khzjhm;
    }

    /**
     * @param khzjhm
     */
    public void setKhzjhm(String khzjhm) {
        this.khzjhm = khzjhm == null ? null : khzjhm.trim();
    }

    /**
     * @return ZJYXQ
     */
    public String getZjyxq() {
        return zjyxq;
    }

    /**
     * @param zjyxq
     */
    public void setZjyxq(String zjyxq) {
        this.zjyxq = zjyxq == null ? null : zjyxq.trim();
    }

    /**
     * @return BDSJH
     */
    public String getBdsjh() {
        return bdsjh;
    }

    /**
     * @param bdsjh
     */
    public void setBdsjh(String bdsjh) {
        this.bdsjh = bdsjh == null ? null : bdsjh.trim();
    }

    /**
     * @return DLHM
     */
    public String getDlhm() {
        return dlhm;
    }

    /**
     * @param dlhm
     */
    public void setDlhm(String dlhm) {
        this.dlhm = dlhm == null ? null : dlhm.trim();
    }

    /**
     * @return WX
     */
    public String getWx() {
        return wx;
    }

    /**
     * @param wx
     */
    public void setWx(String wx) {
        this.wx = wx == null ? null : wx.trim();
    }

    /**
     * @return QQ
     */
    public String getQq() {
        return qq;
    }

    /**
     * @param qq
     */
    public void setQq(String qq) {
        this.qq = qq == null ? null : qq.trim();
    }

    /**
     * @return IP
     */
    public String getIp() {
        return ip;
    }

    /**
     * @param ip
     */
    public void setIp(String ip) {
        this.ip = ip == null ? null : ip.trim();
    }

    /**
     * @return SBHM
     */
    public String getSbhm() {
        return sbhm;
    }

    /**
     * @param sbhm
     */
    public void setSbhm(String sbhm) {
        this.sbhm = sbhm == null ? null : sbhm.trim();
    }

    /**
     * @return SHMC
     */
    public String getShmc() {
        return shmc;
    }

    /**
     * @param shmc
     */
    public void setShmc(String shmc) {
        this.shmc = shmc == null ? null : shmc.trim();
    }

    /**
     * @return SHHM
     */
    public String getShhm() {
        return shhm;
    }

    /**
     * @param shhm
     */
    public void setShhm(String shhm) {
        this.shhm = shhm == null ? null : shhm.trim();
    }

    /**
     * @return POSBH
     */
    public String getPosbh() {
        return posbh;
    }

    /**
     * @param posbh
     */
    public void setPosbh(String posbh) {
        this.posbh = posbh == null ? null : posbh.trim();
    }

    /**
     * @return POSDZ
     */
    public String getPosdz() {
        return posdz;
    }

    /**
     * @param posdz
     */
    public void setPosdz(String posdz) {
        this.posdz = posdz == null ? null : posdz.trim();
    }

    /**
     * @return POSDWDZ
     */
    public String getPosdwdz() {
        return posdwdz;
    }

    /**
     * @param posdwdz
     */
    public void setPosdwdz(String posdwdz) {
        this.posdwdz = posdwdz == null ? null : posdwdz.trim();
    }

    /**
     * @return RKSJ
     */
    public Date getRksj() {
        return rksj;
    }

    /**
     * @param rksj
     */
    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    /**
     * @return YXX
     */
    public Short getYxx() {
        return yxx;
    }

    /**
     * @param yxx
     */
    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pk=").append(pk);
        sb.append(", qqpk=").append(qqpk);
        sb.append(", jjdpk=").append(jjdpk);
        sb.append(", applicationid=").append(applicationid);
        sb.append(", zfzh=").append(zfzh);
        sb.append(", zhlb=").append(zhlb);
        sb.append(", khxm=").append(khxm);
        sb.append(", khzjlx=").append(khzjlx);
        sb.append(", khzjhm=").append(khzjhm);
        sb.append(", zjyxq=").append(zjyxq);
        sb.append(", bdsjh=").append(bdsjh);
        sb.append(", dlhm=").append(dlhm);
        sb.append(", wx=").append(wx);
        sb.append(", qq=").append(qq);
        sb.append(", ip=").append(ip);
        sb.append(", sbhm=").append(sbhm);
        sb.append(", shmc=").append(shmc);
        sb.append(", shhm=").append(shhm);
        sb.append(", posbh=").append(posbh);
        sb.append(", posdz=").append(posdz);
        sb.append(", posdwdz=").append(posdwdz);
        sb.append(", rksj=").append(rksj);
        sb.append(", yxx=").append(yxx);
        sb.append("]");
        return sb.toString();
    }
}