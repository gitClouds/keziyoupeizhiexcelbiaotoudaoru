package com.unis.model.tjfx;

import java.util.Date;
import javax.persistence.*;
/**
* 
*
* @author gongjl
* @version 1.0
* @since 2020-06-05
*/
public class TbZhQkss  {


/****************************************
* Basic fields
****************************************/
    /**
    * 账号: VARCHAR
    */
    private String zh;

    /**
    * 姓名: VARCHAR
    */
    private String xm;

    /**
    * 证件号码: VARCHAR
    */
    private String zjhm;

    /**
    * 证件类型: VARCHAR
    */
    private String zjlx;

    /**
    * 账号机构代码: VARCHAR
    */
    private String jgdm;

    /**
    * 账户机构名称: VARCHAR
    */
    private String jgmc;

    /**
    * 账号类型1：银行卡2：三方: VARCHAR
    */
    private String zhlx;

    /**
    * 业务类型qqlx: VARCHAR
    */
    private String ywlx;

    /**
    * 业务发送人姓名: VARCHAR
    */
    private String ywfsrxm;

    /**
    * 业务发送单位名称: VARCHAR
    */
    private String ywfsrdwmc;

    private String sjh;

    private String qqpk;

    private String resflag;

    public String getResflag() {
        return resflag;
    }

    public void setResflag(String resflag) {
        this.resflag = resflag;
    }

    public String getSjh() {
        return sjh;
    }

    public void setSjh(String sjh) {
        this.sjh = sjh;
    }

    public String getQqpk() {
        return qqpk;
    }

    public void setQqpk(String qqpk) {
        this.qqpk = qqpk;
    }

    public void setZh(String zh) {
    this.zh = zh;
    }

    public String getZh() {
    return zh;
    }

    public void setXm(String xm) {
    this.xm = xm;
    }

    public String getXm() {
    return xm;
    }

    public void setZjhm(String zjhm) {
    this.zjhm = zjhm;
    }

    public String getZjhm() {
    return zjhm;
    }

    public void setZjlx(String zjlx) {
    this.zjlx = zjlx;
    }

    public String getZjlx() {
    return zjlx;
    }

    public void setJgdm(String jgdm) {
    this.jgdm = jgdm;
    }

    public String getJgdm() {
    return jgdm;
    }

    public void setJgmc(String jgmc) {
    this.jgmc = jgmc;
    }

    public String getJgmc() {
    return jgmc;
    }

    public void setZhlx(String zhlx) {
    this.zhlx = zhlx;
    }

    public String getZhlx() {
    return zhlx;
    }

    public void setYwlx(String ywlx) {
    this.ywlx = ywlx;
    }

    public String getYwlx() {
    return ywlx;
    }

    public void setYwfsrxm(String ywfsrxm) {
    this.ywfsrxm = ywfsrxm;
    }

    public String getYwfsrxm() {
    return ywfsrxm;
    }

    public void setYwfsrdwmc(String ywfsrdwmc) {
    this.ywfsrdwmc = ywfsrdwmc;
    }

    public String getYwfsrdwmc() {
    return ywfsrdwmc;
    }

}
