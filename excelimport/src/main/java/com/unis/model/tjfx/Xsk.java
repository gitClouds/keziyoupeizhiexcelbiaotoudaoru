package com.unis.model.tjfx;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
/**
* 线索库
*
* @author xuk
* @version 1.0
* @since 2020-03-03
*/
@Table(name = "TB_TJFX_XSK")
public class Xsk {

private static final long serialVersionUID = 1L;

/****************************************
* Basic fields
****************************************/
    /**
    * 主键: varchar
    */
    @Id
    @Column(name = "PK")
    private String pk;

    /**
    * 账号: varchar
    */
    @Column(name = "ZH")
    private String zh;

    /**
    * 姓名: varchar
    */
    @Column(name = "XM")
    private String xm;

    /**
    * 身份证号: varchar
    */
    @Column(name = "SFZH")
    private String sfzh;

    /**
    * 昵称: varchar
    */
    @Column(name = "NC")
    private String nc;

    /**
    * 入库时间: date
    */
    @Column(name = "RKSJ")
    private Date rksj;

    /**
    * 录入单位: varchar
    */
    @Column(name = "LRDWDM")
    private String lrdwdm;

    /**
     * 录入单位: varchar
     */
    @Column(name = "LRDWMC")
    private String lrdwmc;

    /**
    * 积分: decimal
    */
    @Column(name = "JF")
    private Short jf;

    /**
    * 积分描述: varchar
    */
    @Column(name = "JFMS")
    private String jfms;

    /**
    * 总关系人: BigDecimal
    */
    @Column(name = "GXR")
    private int gxr;

    /**
     * 涉毒关系人: BigDecimal
     */
    @Column(name = "SDGXR")
    private int sdgxr;

    /**
    * 总金额: BigDecimal
    */
    @Column(name = "ZJE")
    private BigDecimal zje;

    /**
     * 总金额（毒贩转账）: BigDecimal
     */
    @Column(name = "SDJE")
    private BigDecimal sdje;

    /**
    * 复吸犯（0：否，1：是）: decimal
    */
    @Column(name = "FXF")
    private Short fxf;

    /**
    * 抓获单位: varchar
    */
    @Column(name = "ZHDWDM")
    private String zhdwdm;

    /**
     * 抓获单位: varchar
     */
    @Column(name = "ZHDWMC")
    private String zhdwmc;

    /**
    * 有效性: decimal
    */
    @Column(name = "YXX")
    private Short yxx;

    /**
     * 账号id: varchar
     */
    @Column(name = "ZHPK")
    private String zhpk;
    /**
     * 账号机构名称: varchar
     */
    @Column(name = "ZHJGMC")
    private String zhjgmc;
    /**
     * 账号机构代码: varchar
     */
    @Column(name = "ZHJGMDM")
    private String zhjgdm;

    /**
     * 层级: decimal
     */
    @Column(name = "NLEVEL")
    private Short nlevel;

    /**
     * 金流图中pk: string
     */
    @Column(name = "JLTPK")
    private String jltpk;

    @Transient
    private Short sertype;
    @Transient
    private String mxqqpk;
    @Transient
    private String sdrypk;
    @Transient
    private String xdrypk;

    public String getSdrypk() {
        return sdrypk;
    }

    public void setSdrypk(String sdrypk) {
        this.sdrypk = sdrypk;
    }

    public String getXdrypk() {
        return xdrypk;
    }

    public void setXdrypk(String xdrypk) {
        this.xdrypk = xdrypk;
    }

    public Short getSertype() {
        return sertype;
    }

    public void setSertype(Short sertype) {
        this.sertype = sertype;
    }

    public String getMxqqpk() {
        return mxqqpk;
    }

    public void setMxqqpk(String mxqqpk) {
        this.mxqqpk = mxqqpk;
    }

    public String getJltpk() {
        return jltpk;
    }

    public void setJltpk(String jltpk) {
        this.jltpk = jltpk;
    }

    public Short getNlevel() {
        return nlevel;
    }

    public void setNlevel(Short nlevel) {
        this.nlevel = nlevel;
    }

    public String getZhjgmc() {
        return zhjgmc;
    }

    public void setZhjgmc(String zhjgmc) {
        this.zhjgmc = zhjgmc;
    }

    public String getZhjgdm() {
        return zhjgdm;
    }

    public void setZhjgdm(String zhjgdm) {
        this.zhjgdm = zhjgdm;
    }

    public String getZhpk() {
        return zhpk;
    }

    public void setZhpk(String zhpk) {
        this.zhpk = zhpk;
    }

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public String getZh() {
        return zh;
    }

    public void setZh(String zh) {
        this.zh = zh;
    }

    public String getXm() {
        return xm;
    }

    public void setXm(String xm) {
        this.xm = xm;
    }

    public String getSfzh() {
        return sfzh;
    }

    public void setSfzh(String sfzh) {
        this.sfzh = sfzh;
    }

    public String getNc() {
        return nc;
    }

    public void setNc(String nc) {
        this.nc = nc;
    }

    public Date getRksj() {
        return rksj;
    }

    public void setRksj(Date rksj) {
        this.rksj = rksj;
    }

    public String getLrdwdm() {
        return lrdwdm;
    }

    public void setLrdwdm(String lrdwdm) {
        this.lrdwdm = lrdwdm;
    }

    public String getLrdwmc() {
        return lrdwmc;
    }

    public void setLrdwmc(String lrdwmc) {
        this.lrdwmc = lrdwmc;
    }

    public Short getJf() {
        return jf;
    }

    public void setJf(Short jf) {
        this.jf = jf;
    }

    public String getJfms() {
        return jfms;
    }

    public void setJfms(String jfms) {
        this.jfms = jfms;
    }

    public int getGxr() {
        return gxr;
    }

    public void setGxr(int gxr) {
        this.gxr = gxr;
    }

    public int getSdgxr() {
        return sdgxr;
    }

    public void setSdgxr(int sdgxr) {
        this.sdgxr = sdgxr;
    }

    public BigDecimal getZje() {
        return zje;
    }

    public void setZje(BigDecimal zje) {
        this.zje = zje;
    }

    public BigDecimal getSdje() {
        return sdje;
    }

    public void setSdje(BigDecimal sdje) {
        this.sdje = sdje;
    }

    public Short getFxf() {
        return fxf;
    }

    public void setFxf(Short fxf) {
        this.fxf = fxf;
    }

    public String getZhdwdm() {
        return zhdwdm;
    }

    public void setZhdwdm(String zhdwdm) {
        this.zhdwdm = zhdwdm;
    }

    public String getZhdwmc() {
        return zhdwmc;
    }

    public void setZhdwmc(String zhdwmc) {
        this.zhdwmc = zhdwmc;
    }

    public Short getYxx() {
        return yxx;
    }

    public void setYxx(Short yxx) {
        this.yxx = yxx;
    }
}
