package com.unis.model.jdzh;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import java.math.BigDecimal;
import java.util.Date;

@Table(name = "TB_DPAJ_SDRYJBXX")
public class TbDpajSdryJbxx {
    /**
     * 记录编号--DExxxxx--编号规则为“XXXXXXXXXXXX（公安机关机构代码）+YYYYMM（年月）+XXXXXXXXXXXXXX（顺序号）”                         : varchar
     */
    @Id
    @Column(name = "JLBH")
    private String jlbh;

    /**
     * 涉毒人员编号--DE01106、DE00038--编号规则为：R+XXXXXXXXXXXX(公安机关机构代码)+XXXXXX(年月)+XXXX(顺序号)              : varchar
     */
    @Column(name = "XYRBH")
    private String xyrbh;

    /**
     * 填报单位名称--DE00065--: varchar
     */
    @Column(name = "TBDW_DWMC")
    private String tbdw_dwmc;

    /**
     * 登记人--DE00002--: varchar
     */
    @Column(name = "DJR_XM")
    private String djr_xm;

    /**
     * 姓名--DE00002--: varchar
     */
    @Column(name = "XM")
    private String xm;

    /**
     * 曾用名--DE00029--: varchar
     */
    @Column(name = "CYM")
    private String cym;

    /**
     * 证件种类--DE00085--采用GA/T 517《常用证件代码》: varchar
     */
    @Column(name = "CYZJDM")
    private String cyzjdm;

    /**
     * 证件号码--DE00618--: varchar
     */
    @Column(name = "ZJHM")
    private String zjhm;

    /**
     * 其它证件及号码--DE00618--: varchar
     */
    @Column(name = "QTZJHM")
    private String qtzjhm;

    /**
     * QQ--DExxxxx--: varchar
     */
    @Column(name = "QQ")
    private String qq;

    /**
     * 户籍所在地行政区划--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码: varchar
     */
    @Column(name = "HJD_XZQH")
    private String hjd_xzqh;

    /**
     * 户籍所在地详址--DE00076--: varchar
     */
    @Column(name = "HJDXZ")
    private String hjdxz;

    /**
     * 户籍地派出所--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码: varchar
     */
    @Column(name = "HJD_DWDM")
    private String hjd_dwdm;

    /**
     * 户籍地派出所--DE00538--: varchar
     */
    @Column(name = "HJD_DWMC")
    private String hjd_dwmc;

    /**
     * 实际居住地行政区划--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码: varchar
     */
    @Column(name = "SJJZD_XZQH")
    private String sjjzd_xzqh;

    /**
     * 实际居住地详址--DE00076--: varchar
     */
    @Column(name = "SJJZDXZ")
    private String sjjzdxz;

    /**
     * 实际居住地派出所--DE00538--: varchar
     */
    @Column(name = "SJJZD_DWMC")
    private String sjjzd_dwmc;

    /**
     * 实际居住地派出所--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码: varchar
     */
    @Column(name = "SJJZD_DWDM")
    private String sjjzd_dwdm;

    /**
     * 其他住址省市县(区)--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码: varchar
     */
    @Column(name = "QTDZ_XZQH")
    private String qtdz_xzqh;

    /**
     * 其他住址详址--DE00076--: varchar
     */
    @Column(name = "QTZZXZ")
    private String qtzzxz;

    /**
     * 绰号/别名--DE00030--: varchar
     */
    @Column(name = "BMCH")
    private String bmch;

    /**
     * 性别--DE00007--采用GB/T 2261.1 《个人基本信息分类与代码 第1部分: 人的性别代码》: varchar
     */
    @Column(name = "XBDM")
    private String xbdm;

    /**
     * 出生日期--DE00008--: timestamp
     */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Column(name = "CSRQ")
    private Date csrq;

    /**
     * 出生地--DE00010--采用GB/T 2260《中华人民共和国行政区划代码》中的数字码: varchar
     */
    @Column(name = "CSDSSXDM")
    private String csdssxdm;

    /**
     * 身份--DE00018--采用GB/T 2261.4《个人基本信息分类与代码 第4部分: 从业状况（个人身份）代码》: varchar
     */
    @Column(name = "GRSFDM")
    private String grsfdm;

    /**
     * 文化程度--DE00013--采用GB/T 4658《学历代码》: varchar
     */
    @Column(name = "XLDM")
    private String xldm;

    /**
     * 民族--DE00011--采用GB/T 3304《中国各民族名称的罗马字母拼写法和代码》中的数字码: varchar
     */
    @Column(name = "MZDM")
    private String mzdm;

    /**
     * 服务场所--DE00021--: varchar
     */
    @Column(name = "FWCS")
    private String fwcs;

    /**
     * 身高--DE00012--: decimal
     */
    @Column(name = "SG")
    private Integer sg;

    /**
     * 特殊体貌特征--DE00952--: varchar
     */
    @Column(name = "TMTZMS")
    private String tmtzms;

    /**
     * 本人联系方法--DE00216--固定电话号码、移动电话号码: varchar
     */
    @Column(name = "BR_LXDH")
    private String br_lxdh;

    /**
     * 联系人信息--DE00216--固定电话号码、移动电话号码: varchar
     */
    @Column(name = "LXRXX")
    private String lxrxx;

    /**
     * 国籍--DE00017--采用GB/T 2659《世界各国和地区名称代码》中的3位字母代码: varchar
     */
    @Column(name = "GJDM")
    private String gjdm;

    /**
     * 婚姻状况--DE00014--采用GB/T 2261.2《个人基本信息代码 第2部分: 婚姻状况代码》: varchar
     */
    @Column(name = "HYZKDM")
    private String hyzkdm;

    /**
     * 人员现状--DExxxxx--: varchar
     */
    @Column(name = "RYXZ")
    private String ryxz;

    /**
     * 现状登记日期--DE00554--: timestamp
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name = "XZDJRQ")
    private Date xzdjrq;

    /**
     * 初次吸毒日期--DE00554--: timestamp
     */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Column(name = "CCXDRQ")
    private Date ccxdrq;

    /**
     * 毒品犯罪嫌疑人类型--DExxxxx--: decimal
     */
    @Column(name = "DPFZXYRLX")
    private Integer dpfzxyrlx;

    /**
     * 备注--DE00503--: varchar
     */
    @Column(name = "BZ")
    private String bz;

    /**
     * 登记日期--DE00554--: timestamp
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name = "DJRQ")
    private Date djrq;

    /**
     * 涉毒人员类型--DExxxxx--: varchar
     */
    @Column(name = "SDRYLX")
    private String sdrylx;

    /**
     * 查询单位--DE00065--: varchar
     */
    @Column(name = "CXDW_DWMC")
    private String cxdw_dwmc;

    /**
     * 姓名/绰号拼音--DE00003--: varchar
     */
    @Column(name = "XMHYPY")
    private String xmhypy;

    /**
     * 填报地区--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码: varchar
     */
    @Column(name = "TBDQ_XZQH")
    private String tbdq_xzqh;

    /**
     * 在逃类型--DE00052--采用GA 240.14《刑事犯罪信息管理代码 第14部分：在逃人员类型代码》: varchar
     */
    @Column(name = "ZTRYLXDM")
    private String ztrylxdm;

    /**
     * 法律文书编号--DE00805--: varchar
     */
    @Column(name = "WSBH")
    private String wsbh;

    /**
     * 易制毒信息嫌疑人录入来源--DExxxxx--: decimal
     */
    @Column(name = "YZDXXXYRLRLY")
    private Integer yzdxxxyrlrly;

    /**
     * 刑期--DE00966--: decimal
     */
    @Column(name = "YQTXQX")
    private Integer yqtxqx;

    /**
     * 其它状态--DExxxxx--: varchar
     */
    @Column(name = "QTZT")
    private String qtzt;

    /**
     * 人员类别--DExxxxx--: decimal
     */
    @Column(name = "RYLB")
    private Integer rylb;

    /**
     * 人员角色--DE00962--采用 GA/T XXX《案事件相关人员角色分类与代码》: varchar
     */
    @Column(name = "RYJSDM")
    private String ryjsdm;

    /**
     * 其它角色--DE00962--采用 GA/T XXX《案事件相关人员角色分类与代码》: varchar
     */
    @Column(name = "QTJSDM")
    private String qtjsdm;

    /**
     * 年龄--DE00836--: decimal
     */
    @Column(name = "NL")
    private Integer nl;

    /**
     * 籍贯--DE00016--采用GB/T 2260《中华人民共和国行政区划代码》中的数字码: varchar
     */
    @Column(name = "JGSSXDM")
    private String jgssxdm;

    /**
     * 简介--DE00955--: varchar
     */
    @Column(name = "JL")
    private String jl;

    /**
     * 境外地址--DE00075--: varchar
     */
    @Column(name = "JWDZ")
    private String jwdz;

    /**
     * 通讯地址--DE00075--: varchar
     */
    @Column(name = "TXDZ")
    private String txdz;

    /**
     * 英文名--DE00005--: varchar
     */
    @Column(name = "WWM")
    private String wwm;

    /**
     * 英文姓--DE00004--: varchar
     */
    @Column(name = "WWX")
    private String wwx;

    /**
     * 血型--DE00631--采用GA/T 2000.37《公安信息代码 第37部分：血型代码》: varchar
     */
    @Column(name = "XXDM")
    private String xxdm;

    /**
     * 宗教信仰--DE00025--采用GA 214.12《常住人口管理信息规范 第12部分: 宗教信仰代码》: varchar
     */
    @Column(name = "ZJXYDM")
    private String zjxydm;

    /**
     * 职业类别--DE00019--: varchar
     */
    @Column(name = "ZY")
    private String zy;

    /**
     * 职业名称--DE00020--采用GB/T 6565《职业分类与代码》，不包含代码中的“-”: varchar
     */
    @Column(name = "ZYLBDM")
    private String zylbdm;

    /**
     * 政治面貌--DE00036--采用GB/T 4762《政治面貌代码》: varchar
     */
    @Column(name = "ZZMMDM")
    private String zzmmdm;

    /**
     * 口音--DE00034--采用GA/T 240.57《刑事犯罪信息管理代码 第57部分: 汉语口音编码规则》: varchar
     */
    @Column(name = "HYKYDM")
    private String hykydm;

    /**
     * 是否死亡--DExxxxx--: decimal
     */
    @Column(name = "SFSW")
    private Integer sfsw;

    /**
     * 死亡时间--DE00839--: timestamp
     */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Column(name = "SWSJ")
    private Date swsj;

    /**
     * 是否吸毒--DE00838--采用GA/T XXXXX-XXXX《吸毒记录核查结果代码》: varchar
     */
    @Column(name = "XDJLHCJGDM")
    private String xdjlhcjgdm;

    /**
     * 语种--DExxxxx--: varchar
     */
    @Column(name = "YZ")
    private String yz;

    /**
     * 特长爱好--DE00039--采用GA 240.2《刑事犯罪信息管理代码 第2部分：专长代码》: varchar
     */
    @Column(name = "SARYZCDM")
    private String saryzcdm;

    /**
     * 犯罪子类型--DExxxxx--: varchar
     */
    @Column(name = "FZZLX")
    private String fzzlx;

    /**
     * 活动区域--DE00576--采用GA/T 2000.23《公安信息代码 第23部分：人口迁移（流动）区域范围代码》: varchar
     */
    @Column(name = "QYFWDM")
    private String qyfwdm;

    /**
     * 布控状态--DExxxxx--: decimal
     */
    @Column(name = "BKZT")
    private Integer bkzt;

    /**
     * 犯罪类型--DE00221--犯罪主体类型代码: varchar
     */
    @Column(name = "FZZTLXDM")
    private String fzztlxdm;

    /**
     * 填报单位: varchar
     */
    @Column(name = "TBDW_DWDM")
    private String tbdw_dwdm;

    /**
     * 填报地区: varchar
     */
    @Column(name = "TBDQ")
    private String tbdq;

    /**
     * 健康状况: varchar
     */
    @Column(name = "JKZK")
    private String jkzk;

    /**
     * 查重标志: varchar
     */
    @Column(name = "CZBZ")
    private String czbz;

    /**
     * 网络身份信息: varchar
     */
    @Column(name = "WLSFXX")
    private String wlsfxx;

    /**
     * 更新时间: timestamp
     */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Column(name = "GJSJ")
    private String gjsj;

    /**
     * 有效性: decimal
     */
    @Column(name = "YXX")
    private Integer yxx;

    /**
     * 更新时间: timestamp
     */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Column(name = "GXSJ")
    private Date gxsj;

    /**
     * 更新时间戳--DEXXXXX--: varchar
     */
    @Column(name = "GXSJC")
    private String gxsjc;

    /**
     * 修改部署单位--DEXXXXX--: varchar
     */
    @Column(name = "XGBSDW")
    private String xgbsdw;

    /**
     * 注释: varchar
     */
    @Column(name = "SJCH")
    private String sjch;

    /**
     * 注释: varchar
     */
    @Column(name = "SJXH")
    private String sjxh;

    /**
     * 注释: varchar
     */
    @Column(name = "GH")
    private String gh;

    /**
     * 注释: varchar
     */
    @Column(name = "EMAIL")
    private String email;

    /**
     * 注释: varchar
     */
    @Column(name = "XNSF")
    private String xnsf;

    /**
     * 注释: varchar
     */
    @Column(name = "WX")
    private String wx;

    /**
     * 注释: varchar
     */
    @Column(name = "RY_DWMC")
    private String ry_dwmc;

    /**
     * 注释: varchar
     */
    @Column(name = "TSBJ")
    private String tsbj;

    /**
     * 注释: varchar
     */
    @Column(name = "SCZT")
    private String sczt;

    /**
     * 注释: varchar
     */
    @Column(name = "SJHGSD")
    private String sjhgsd;

    /**
     * 注释: varchar
     */
    @Column(name = "RYZP")
    private String ryzp;

    /**
     * 注释: varchar
     */
    @Column(name = "FZLX")
    private String fzlx;

    /**
     * 注释: varchar
     */
    @Column(name = "RYLY")
    private String ryly;

    /**
     * 操作标识: varchar
     */
    @Column(name = "CZBS")
    private String czbs;

    /**
     * 传输状态。0:未传输。1:已传输（新老系统数据传输）: varchar
     */
    @Column(name = "CSZT")
    private String cszt;

    /**
     * 刑期 老表备份: varchar
     */
    @Column(name = "YQTXQXBF")
    private String yqtxqxbf;

    /**
     * 出生地 老表备份: varchar
     */
    @Column(name = "CSDSSXDMBF")
    private String csdssxdmbf;

    /**
     * 籍贯 老表备份: varchar
     */
    @Column(name = "JGSSXDMBF")
    private String jgssxdmbf;

    /**
     * 身高  老表备份: varchar
     */
    @Column(name = "SGBF")
    private String sgbf;

    /**
     * 保护伞编号: varchar
     */
    @Column(name = "BHSBH")
    private String bhsbh;

    /**
     * 涉毒人员人口库照片: varchar
     */
    @Column(name = "XP")
    private String xp;

    /**
     * 注释: decimal
     */
    @Column(name = "N_YXH")
    private BigDecimal n_yxh;

    /**
     * 联系人联系方式: varchar
     */
    @Column(name = "LXRLXFS")
    private String lxrlxfs;

    /**
     * 与涉毒人员关系: varchar
     */
    @Column(name = "LXRGX")
    private String lxrgx;

    /**
     * 注释: varchar
     */
    @Column(name = "SJLY")
    private String sjly;

    /**
     * 注释: Short来源(1：接口下发2：本地录入)
     */
    @Column(name = "LY")
    private Short ly;

    /**
     * 注释: varchar
     */
    @Column(name = "BDXGR")
    private String bdxgr;

    /**
     * 注释: varchar
     */
    @Column(name = "BDXGRDWDM")
    private String bdxgrdwdm;

    /**
     * 注释: varchar
     */
    @Column(name = "BDXGRDWMC")
    private String bdxgrdwmc;

    /**
     * 注释: Date
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name = "BDXGRQ")
    private Date bdxgrq;

    /**
     * 注释: varchar
     */
    @Column(name = "ZHPK")
    private String zhpk;

    /**
     * 注释: varchar
     */
    @Column(name = "SSZH")
    private String sszh;

    /**
     * 注释: varchar
     */
    @Column(name = "SSCJ")
    private Short sscj;

    @Transient
    private Short lx;//区分1涉毒2吸毒

    public String getSszh() {
        return sszh;
    }

    public void setSszh(String sszh) {
        this.sszh = sszh;
    }

    public Short getSscj() {
        return sscj;
    }

    public void setSscj(Short sscj) {
        this.sscj = sscj;
    }

    public String getZhpk() {
        return zhpk;
    }

    public void setZhpk(String zhpk) {
        this.zhpk = zhpk;
    }

    public String getJlbh() {
        return jlbh;
    }

    public void setJlbh(String jlbh) {
        this.jlbh = jlbh;
    }

    public String getXyrbh() {
        return xyrbh;
    }

    public void setXyrbh(String xyrbh) {
        this.xyrbh = xyrbh;
    }

    public String getTbdw_dwmc() {
        return tbdw_dwmc;
    }

    public void setTbdw_dwmc(String tbdw_dwmc) {
        this.tbdw_dwmc = tbdw_dwmc;
    }

    public String getDjr_xm() {
        return djr_xm;
    }

    public void setDjr_xm(String djr_xm) {
        this.djr_xm = djr_xm;
    }

    public String getXm() {
        return xm;
    }

    public void setXm(String xm) {
        this.xm = xm;
    }

    public String getCym() {
        return cym;
    }

    public void setCym(String cym) {
        this.cym = cym;
    }

    public String getCyzjdm() {
        return cyzjdm;
    }

    public void setCyzjdm(String cyzjdm) {
        this.cyzjdm = cyzjdm;
    }

    public String getZjhm() {
        return zjhm;
    }

    public void setZjhm(String zjhm) {
        this.zjhm = zjhm;
    }

    public String getQtzjhm() {
        return qtzjhm;
    }

    public void setQtzjhm(String qtzjhm) {
        this.qtzjhm = qtzjhm;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getHjd_xzqh() {
        return hjd_xzqh;
    }

    public void setHjd_xzqh(String hjd_xzqh) {
        this.hjd_xzqh = hjd_xzqh;
    }

    public String getHjdxz() {
        return hjdxz;
    }

    public void setHjdxz(String hjdxz) {
        this.hjdxz = hjdxz;
    }

    public String getHjd_dwdm() {
        return hjd_dwdm;
    }

    public void setHjd_dwdm(String hjd_dwdm) {
        this.hjd_dwdm = hjd_dwdm;
    }

    public String getHjd_dwmc() {
        return hjd_dwmc;
    }

    public void setHjd_dwmc(String hjd_dwmc) {
        this.hjd_dwmc = hjd_dwmc;
    }

    public String getSjjzd_xzqh() {
        return sjjzd_xzqh;
    }

    public void setSjjzd_xzqh(String sjjzd_xzqh) {
        this.sjjzd_xzqh = sjjzd_xzqh;
    }

    public String getSjjzdxz() {
        return sjjzdxz;
    }

    public void setSjjzdxz(String sjjzdxz) {
        this.sjjzdxz = sjjzdxz;
    }

    public String getSjjzd_dwmc() {
        return sjjzd_dwmc;
    }

    public void setSjjzd_dwmc(String sjjzd_dwmc) {
        this.sjjzd_dwmc = sjjzd_dwmc;
    }

    public String getSjjzd_dwdm() {
        return sjjzd_dwdm;
    }

    public void setSjjzd_dwdm(String sjjzd_dwdm) {
        this.sjjzd_dwdm = sjjzd_dwdm;
    }

    public String getQtdz_xzqh() {
        return qtdz_xzqh;
    }

    public void setQtdz_xzqh(String qtdz_xzqh) {
        this.qtdz_xzqh = qtdz_xzqh;
    }

    public String getQtzzxz() {
        return qtzzxz;
    }

    public void setQtzzxz(String qtzzxz) {
        this.qtzzxz = qtzzxz;
    }

    public String getBmch() {
        return bmch;
    }

    public void setBmch(String bmch) {
        this.bmch = bmch;
    }

    public String getXbdm() {
        return xbdm;
    }

    public void setXbdm(String xbdm) {
        this.xbdm = xbdm;
    }

    public Date getCsrq() {
        return csrq;
    }

    public void setCsrq(Date csrq) {
        this.csrq = csrq;
    }

    public String getCsdssxdm() {
        return csdssxdm;
    }

    public void setCsdssxdm(String csdssxdm) {
        this.csdssxdm = csdssxdm;
    }

    public String getGrsfdm() {
        return grsfdm;
    }

    public void setGrsfdm(String grsfdm) {
        this.grsfdm = grsfdm;
    }

    public String getXldm() {
        return xldm;
    }

    public void setXldm(String xldm) {
        this.xldm = xldm;
    }

    public String getMzdm() {
        return mzdm;
    }

    public void setMzdm(String mzdm) {
        this.mzdm = mzdm;
    }

    public String getFwcs() {
        return fwcs;
    }

    public void setFwcs(String fwcs) {
        this.fwcs = fwcs;
    }

    public Integer getSg() {
        return sg;
    }

    public void setSg(Integer sg) {
        this.sg = sg;
    }

    public String getTmtzms() {
        return tmtzms;
    }

    public void setTmtzms(String tmtzms) {
        this.tmtzms = tmtzms;
    }

    public String getBr_lxdh() {
        return br_lxdh;
    }

    public void setBr_lxdh(String br_lxdh) {
        this.br_lxdh = br_lxdh;
    }

    public String getLxrxx() {
        return lxrxx;
    }

    public void setLxrxx(String lxrxx) {
        this.lxrxx = lxrxx;
    }

    public String getGjdm() {
        return gjdm;
    }

    public void setGjdm(String gjdm) {
        this.gjdm = gjdm;
    }

    public String getHyzkdm() {
        return hyzkdm;
    }

    public void setHyzkdm(String hyzkdm) {
        this.hyzkdm = hyzkdm;
    }

    public String getRyxz() {
        return ryxz;
    }

    public void setRyxz(String ryxz) {
        this.ryxz = ryxz;
    }

    public Date getXzdjrq() {
        return xzdjrq;
    }

    public void setXzdjrq(Date xzdjrq) {
        this.xzdjrq = xzdjrq;
    }

    public Date getCcxdrq() {
        return ccxdrq;
    }

    public void setCcxdrq(Date ccxdrq) {
        this.ccxdrq = ccxdrq;
    }

    public Integer getDpfzxyrlx() {
        return dpfzxyrlx;
    }

    public void setDpfzxyrlx(Integer dpfzxyrlx) {
        this.dpfzxyrlx = dpfzxyrlx;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }

    public Date getDjrq() {
        return djrq;
    }

    public void setDjrq(Date djrq) {
        this.djrq = djrq;
    }

    public String getSdrylx() {
        return sdrylx;
    }

    public void setSdrylx(String sdrylx) {
        this.sdrylx = sdrylx;
    }

    public String getCxdw_dwmc() {
        return cxdw_dwmc;
    }

    public void setCxdw_dwmc(String cxdw_dwmc) {
        this.cxdw_dwmc = cxdw_dwmc;
    }

    public String getXmhypy() {
        return xmhypy;
    }

    public void setXmhypy(String xmhypy) {
        this.xmhypy = xmhypy;
    }

    public String getTbdq_xzqh() {
        return tbdq_xzqh;
    }

    public void setTbdq_xzqh(String tbdq_xzqh) {
        this.tbdq_xzqh = tbdq_xzqh;
    }

    public String getZtrylxdm() {
        return ztrylxdm;
    }

    public void setZtrylxdm(String ztrylxdm) {
        this.ztrylxdm = ztrylxdm;
    }

    public String getWsbh() {
        return wsbh;
    }

    public void setWsbh(String wsbh) {
        this.wsbh = wsbh;
    }

    public Integer getYzdxxxyrlrly() {
        return yzdxxxyrlrly;
    }

    public void setYzdxxxyrlrly(Integer yzdxxxyrlrly) {
        this.yzdxxxyrlrly = yzdxxxyrlrly;
    }

    public Integer getYqtxqx() {
        return yqtxqx;
    }

    public void setYqtxqx(Integer yqtxqx) {
        this.yqtxqx = yqtxqx;
    }

    public String getQtzt() {
        return qtzt;
    }

    public void setQtzt(String qtzt) {
        this.qtzt = qtzt;
    }

    public Integer getRylb() {
        return rylb;
    }

    public void setRylb(Integer rylb) {
        this.rylb = rylb;
    }

    public String getRyjsdm() {
        return ryjsdm;
    }

    public void setRyjsdm(String ryjsdm) {
        this.ryjsdm = ryjsdm;
    }

    public String getQtjsdm() {
        return qtjsdm;
    }

    public void setQtjsdm(String qtjsdm) {
        this.qtjsdm = qtjsdm;
    }

    public Integer getNl() {
        return nl;
    }

    public void setNl(Integer nl) {
        this.nl = nl;
    }

    public String getJgssxdm() {
        return jgssxdm;
    }

    public void setJgssxdm(String jgssxdm) {
        this.jgssxdm = jgssxdm;
    }

    public String getJl() {
        return jl;
    }

    public void setJl(String jl) {
        this.jl = jl;
    }

    public String getJwdz() {
        return jwdz;
    }

    public void setJwdz(String jwdz) {
        this.jwdz = jwdz;
    }

    public String getTxdz() {
        return txdz;
    }

    public void setTxdz(String txdz) {
        this.txdz = txdz;
    }

    public String getWwm() {
        return wwm;
    }

    public void setWwm(String wwm) {
        this.wwm = wwm;
    }

    public String getWwx() {
        return wwx;
    }

    public void setWwx(String wwx) {
        this.wwx = wwx;
    }

    public String getXxdm() {
        return xxdm;
    }

    public void setXxdm(String xxdm) {
        this.xxdm = xxdm;
    }

    public String getZjxydm() {
        return zjxydm;
    }

    public void setZjxydm(String zjxydm) {
        this.zjxydm = zjxydm;
    }

    public String getZy() {
        return zy;
    }

    public void setZy(String zy) {
        this.zy = zy;
    }

    public String getZylbdm() {
        return zylbdm;
    }

    public void setZylbdm(String zylbdm) {
        this.zylbdm = zylbdm;
    }

    public String getZzmmdm() {
        return zzmmdm;
    }

    public void setZzmmdm(String zzmmdm) {
        this.zzmmdm = zzmmdm;
    }

    public String getHykydm() {
        return hykydm;
    }

    public void setHykydm(String hykydm) {
        this.hykydm = hykydm;
    }

    public Integer getSfsw() {
        return sfsw;
    }

    public void setSfsw(Integer sfsw) {
        this.sfsw = sfsw;
    }

    public Date getSwsj() {
        return swsj;
    }

    public void setSwsj(Date swsj) {
        this.swsj = swsj;
    }

    public String getXdjlhcjgdm() {
        return xdjlhcjgdm;
    }

    public void setXdjlhcjgdm(String xdjlhcjgdm) {
        this.xdjlhcjgdm = xdjlhcjgdm;
    }

    public String getYz() {
        return yz;
    }

    public void setYz(String yz) {
        this.yz = yz;
    }

    public String getSaryzcdm() {
        return saryzcdm;
    }

    public void setSaryzcdm(String saryzcdm) {
        this.saryzcdm = saryzcdm;
    }

    public String getFzzlx() {
        return fzzlx;
    }

    public void setFzzlx(String fzzlx) {
        this.fzzlx = fzzlx;
    }

    public String getQyfwdm() {
        return qyfwdm;
    }

    public void setQyfwdm(String qyfwdm) {
        this.qyfwdm = qyfwdm;
    }

    public Integer getBkzt() {
        return bkzt;
    }

    public void setBkzt(Integer bkzt) {
        this.bkzt = bkzt;
    }

    public String getFzztlxdm() {
        return fzztlxdm;
    }

    public void setFzztlxdm(String fzztlxdm) {
        this.fzztlxdm = fzztlxdm;
    }

    public String getTbdw_dwdm() {
        return tbdw_dwdm;
    }

    public void setTbdw_dwdm(String tbdw_dwdm) {
        this.tbdw_dwdm = tbdw_dwdm;
    }

    public String getTbdq() {
        return tbdq;
    }

    public void setTbdq(String tbdq) {
        this.tbdq = tbdq;
    }

    public String getJkzk() {
        return jkzk;
    }

    public void setJkzk(String jkzk) {
        this.jkzk = jkzk;
    }

    public String getCzbz() {
        return czbz;
    }

    public void setCzbz(String czbz) {
        this.czbz = czbz;
    }

    public String getWlsfxx() {
        return wlsfxx;
    }

    public void setWlsfxx(String wlsfxx) {
        this.wlsfxx = wlsfxx;
    }

    public String getGjsj() {
        return gjsj;
    }

    public void setGjsj(String gjsj) {
        this.gjsj = gjsj;
    }

    public Integer getYxx() {
        return yxx;
    }

    public void setYxx(Integer yxx) {
        this.yxx = yxx;
    }

    public Date getGxsj() {
        return gxsj;
    }

    public void setGxsj(Date gxsj) {
        this.gxsj = gxsj;
    }

    public String getGxsjc() {
        return gxsjc;
    }

    public void setGxsjc(String gxsjc) {
        this.gxsjc = gxsjc;
    }

    public String getXgbsdw() {
        return xgbsdw;
    }

    public void setXgbsdw(String xgbsdw) {
        this.xgbsdw = xgbsdw;
    }

    public String getSjch() {
        return sjch;
    }

    public void setSjch(String sjch) {
        this.sjch = sjch;
    }

    public String getSjxh() {
        return sjxh;
    }

    public void setSjxh(String sjxh) {
        this.sjxh = sjxh;
    }

    public String getGh() {
        return gh;
    }

    public void setGh(String gh) {
        this.gh = gh;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getXnsf() {
        return xnsf;
    }

    public void setXnsf(String xnsf) {
        this.xnsf = xnsf;
    }

    public String getWx() {
        return wx;
    }

    public void setWx(String wx) {
        this.wx = wx;
    }

    public String getRy_dwmc() {
        return ry_dwmc;
    }

    public void setRy_dwmc(String ry_dwmc) {
        this.ry_dwmc = ry_dwmc;
    }

    public String getTsbj() {
        return tsbj;
    }

    public void setTsbj(String tsbj) {
        this.tsbj = tsbj;
    }

    public String getSczt() {
        return sczt;
    }

    public void setSczt(String sczt) {
        this.sczt = sczt;
    }

    public String getSjhgsd() {
        return sjhgsd;
    }

    public void setSjhgsd(String sjhgsd) {
        this.sjhgsd = sjhgsd;
    }

    public String getRyzp() {
        return ryzp;
    }

    public void setRyzp(String ryzp) {
        this.ryzp = ryzp;
    }

    public String getFzlx() {
        return fzlx;
    }

    public void setFzlx(String fzlx) {
        this.fzlx = fzlx;
    }

    public String getRyly() {
        return ryly;
    }

    public void setRyly(String ryly) {
        this.ryly = ryly;
    }

    public String getCzbs() {
        return czbs;
    }

    public void setCzbs(String czbs) {
        this.czbs = czbs;
    }

    public String getCszt() {
        return cszt;
    }

    public void setCszt(String cszt) {
        this.cszt = cszt;
    }

    public String getYqtxqxbf() {
        return yqtxqxbf;
    }

    public void setYqtxqxbf(String yqtxqxbf) {
        this.yqtxqxbf = yqtxqxbf;
    }

    public String getCsdssxdmbf() {
        return csdssxdmbf;
    }

    public void setCsdssxdmbf(String csdssxdmbf) {
        this.csdssxdmbf = csdssxdmbf;
    }

    public String getJgssxdmbf() {
        return jgssxdmbf;
    }

    public void setJgssxdmbf(String jgssxdmbf) {
        this.jgssxdmbf = jgssxdmbf;
    }

    public String getSgbf() {
        return sgbf;
    }

    public void setSgbf(String sgbf) {
        this.sgbf = sgbf;
    }

    public String getBhsbh() {
        return bhsbh;
    }

    public void setBhsbh(String bhsbh) {
        this.bhsbh = bhsbh;
    }

    public String getXp() {
        return xp;
    }

    public void setXp(String xp) {
        this.xp = xp;
    }

    public BigDecimal getN_yxh() {
        return n_yxh;
    }

    public void setN_yxh(BigDecimal n_yxh) {
        this.n_yxh = n_yxh;
    }

    public String getLxrlxfs() {
        return lxrlxfs;
    }

    public void setLxrlxfs(String lxrlxfs) {
        this.lxrlxfs = lxrlxfs;
    }

    public String getLxrgx() {
        return lxrgx;
    }

    public void setLxrgx(String lxrgx) {
        this.lxrgx = lxrgx;
    }

    public String getSjly() {
        return sjly;
    }

    public void setSjly(String sjly) {
        this.sjly = sjly;
    }

    public Short getLy() {
        return ly;
    }

    public void setLy(Short ly) {
        this.ly = ly;
    }

    public String getBdxgr() {
        return bdxgr;
    }

    public void setBdxgr(String bdxgr) {
        this.bdxgr = bdxgr;
    }

    public String getBdxgrdwdm() {
        return bdxgrdwdm;
    }

    public void setBdxgrdwdm(String bdxgrdwdm) {
        this.bdxgrdwdm = bdxgrdwdm;
    }

    public String getBdxgrdwmc() {
        return bdxgrdwmc;
    }

    public void setBdxgrdwmc(String bdxgrdwmc) {
        this.bdxgrdwmc = bdxgrdwmc;
    }

    public Date getBdxgrq() {
        return bdxgrq;
    }

    public void setBdxgrq(Date bdxgrq) {
        this.bdxgrq = bdxgrq;
    }

    public Short getLx() {
        return lx;
    }

    public void setLx(Short lx) {
        this.lx = lx;
    }
}
