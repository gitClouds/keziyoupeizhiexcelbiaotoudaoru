package com.unis.model.jdzh;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

@Table(name = "TB_XDRY_JBXX")
public class TbXdryJbxx {
    /**
     * 人员序号--DEXXXXX--: varchar
     */
    @Id
    @Column(name = "RYXH")
    private String ryxh;

    /**
     * 人员类型--DEXXXXX--: varchar
     */
    @Column(name = "XDRYLY")
    private String xdryly;

    /**
     * 业务类型DEXXXXX: varchar
     */
    @Column(name = "YWLX")
    private String ywlx;

    /**
     * 业务序号--DEXXXXX--: varchar
     */
    @Column(name = "YWXH")
    private String ywxh;

    /**
     * 人员编号--DE00038--编号规则为：R+XXXXXXXXXXXX(公安机关机构代码)+XXXXXX(年月)+XXXX(顺序号)              : varchar
     */
    @Column(name = "XYRBH")
    private String xyrbh;

    /**
     * 姓名--DE00002--: varchar
     */
    @Column(name = "XM")
    private String xm;

    /**
     * 吸毒人员登记来源--DE01131--采用GA/TXXXX《吸毒人员登记来源代码》: varchar
     */
    @Column(name = "XDRYDJLYDM")
    private String xdrydjlydm;

    /**
     * 证件种类--DE00085--: varchar
     */
    @Column(name = "CYZJDM")
    private String cyzjdm;

    /**
     * 18位身份证号--DEXXXXX--: varchar
     */
    @Column(name = "SFZHM18")
    private String sfzhm18;

    /**
     * 公民身份号码--DE00001--符合GB 11643《公民身份号码》                  : varchar
     */
    @Column(name = "GMSFHM")
    private String gmsfhm;

    /**
     * 证件号码--DE00618--: varchar
     */
    @Column(name = "ZJHM")
    private String zjhm;

    /**
     * 别名/绰号--DE00030--: varchar
     */
    @Column(name = "BMCH")
    private String bmch;

    /**
     * 性别--DE00007--采用GB/T 2261.1 《个人基本信息分类与代码 第1部分: 人的性别代码》: varchar
     */
    @Column(name = "XBDM")
    private String xbdm;

    /**
     * 出生日期--DE00008--: timestamp
     */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Column(name = "CSRQ")
    private Date csrq;

    /**
     * 民族--DE00011--采用GB/T 3304《中国各民族名称的罗马字母拼写法和代码》中的数字码: varchar
     */
    @Column(name = "MZDM")
    private String mzdm;

    /**
     * 身高--DE00012--: decimal
     */
    @Column(name = "SG")
    private Integer sg;

    /**
     * 国籍--DE00015--采用GB/T 2659--2000《世界各国和地区名称代码》中的3位字母代码: varchar
     */
    @Column(name = "JGGJDQDM")
    private String jggjdqdm;

    /**
     * 文化程度--DE00013--采用GB/T 4658《学历代码》: varchar
     */
    @Column(name = "XLDM")
    private String xldm;

    /**
     * 个人身份代码--DE00018--采用GB/T 2261.4《个人基本信息分类与代码 第4部分: 从业状况（个人身份）代码》: varchar
     */
    @Column(name = "GRSFDM")
    private String grsfdm;

    /**
     * 婚姻状况--DE00014--采用GB/T 2261.2《个人基本信息代码 第2部分: 婚姻状况代码》: varchar
     */
    @Column(name = "HYZKDM")
    private String hyzkdm;

    /**
     * 就业情况--DEXXXXX--: varchar
     */
    @Column(name = "JYQK")
    private String jyqk;

    /**
     * 工作单位_单位名称--DE00065--: varchar
     */
    @Column(name = "GZDW_DWMC")
    private String gzdw_dwmc;

    /**
     * 指纹编号--DE01110--: varchar
     */
    @Column(name = "SZZWBH")
    private String szzwbh;

    /**
     * DNA编号--DE01111--: varchar
     */
    @Column(name = "RYDNABH")
    private String rydnabh;

    /**
     *   户籍地址_行政区划名称--DE00619--: varchar
     */
    @Column(name = "HJDZ_XZQHMC")
    private String hjdz_xzqhmc;

    /**
     *   户籍地址_详址--DE00075--: varchar
     */
    @Column(name = "HJDZ_DZMC")
    private String hjdz_dzmc;

    /**
     *   户籍地址_公安机关名称--DE00538--: varchar
     */
    @Column(name = "HJDZ_GAJGMC")
    private String hjdz_gajgmc;

    /**
     *   实际居住地_行政区划名称--DE00619--: varchar
     */
    @Column(name = "SJJZD_XZQHMC")
    private String sjjzd_xzqhmc;

    /**
     *   实际居住地_详址--DE00075--: varchar
     */
    @Column(name = "SJJZD_DZMC")
    private String sjjzd_dzmc;

    /**
     *   实际居住地_公安机关名称--DE00538--: varchar
     */
    @Column(name = "SJJZD_GAJGMC")
    private String sjjzd_gajgmc;

    /**
     * 录入人_姓名--DE00002--: varchar
     */
    @Column(name = "LRR_XM")
    private String lrr_xm;

    /**
     * 录入单位_单位名称--DE00065--: varchar
     */
    @Column(name = "LRDW_DWMC")
    private String lrdw_dwmc;

    /**
     * 录入时间--DE00739--: timestamp
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name = "LRSJ")
    private Date lrsj;

    /**
     * 是否828库人员--DEXXXXX--: decimal
     */
    @Column(name = "SF828K")
    private Integer sf828k;

    /**
     * 涉毒人员编号--DEXXXXX--: varchar
     */
    @Column(name = "SDRYBH")
    private String sdrybh;

    /**
     * 原序号--DEXXXXX--: varchar
     */
    @Column(name = "YXH")
    private String yxh;

    /**
     * 内外网标志--DEXXXXX--: varchar
     */
    @Column(name = "INOUTBZ")
    private String inoutbz;

    /**
     * 归属单位--DEXXXXX--: varchar
     */
    @Column(name = "GSDW")
    private String gsdw;

    /**
     * 从业状况--DEXXXXX--: varchar
     */
    @Column(name = "CYZK")
    private String cyzk;

    /**
     * 人员标识--DEXXXXX--: varchar
     */
    @Column(name = "RYBS")
    private String rybs;

    /**
     * 标志--DEXXXXX--: varchar
     */
    @Column(name = "NCCBZ")
    private String nccbz;

    /**
     * 操作标识--DEXXXXX--: varchar
     */
    @Column(name = "CZBS")
    private String czbs;

    /**
     * 更新时间戳--DEXXXXX--: varchar
     */
    @Column(name = "GXSJC")
    private String gxsjc;

    /**
     * 修改部署单位--DEXXXXX--: varchar
     */
    @Column(name = "XGBSDW")
    private String xgbsdw;

    /**
     * 查重标志--DEXXXXX--: varchar
     */
    @Column(name = "CCBZ")
    private String ccbz;

    /**
     * 有效性--DEXXXXX--: decimal
     */
    @Column(name = "YXX")
    private Integer yxx;

    /**
     * 更新时间--DEXXXXX--: timestamp
     */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Column(name = "GXSJ")
    private String gxsj;

    /**
     * 录入单位_单位代码--DE00065--: varchar
     */
    @Column(name = "LRDW_DWDM")
    private String lrdw_dwdm;

    /**
     *   户籍地址_行政区划代码--DE00619--: varchar
     */
    @Column(name = "HJDZ_XZQHDM")
    private String hjdz_xzqhdm;

    /**
     *   户籍地址_公安机关代码--DE00538--: varchar
     */
    @Column(name = "HJDZ_GAJGDM")
    private String hjdz_gajgdm;

    /**
     *   实际居住地_行政区划代码--DE00619--: varchar
     */
    @Column(name = "SJJZD_XZQHDM")
    private String sjjzd_xzqhdm;

    /**
     *   实际居住地_公安机关代码--DE00538--: varchar
     */
    @Column(name = "SJJZD_GAJGDM")
    private String sjjzd_gajgdm;

    /**
     * 注释: varchar
     */
    @Column(name = "XP")
    private String xp;

    /**
     * 注释: varchar
     */
    @Column(name = "FJXH")
    private String fjxh;

    /**
     * 学校名称: varchar
     */
    @Column(name = "XXMC")
    private String xxmc;

    /**
     * 注释: Short来源(1：接口下发2：本地录入)
     */
    @Column(name = "LY")
    private Short ly;

    /**
     * 注释: varchar
     */
    @Column(name = "BDXGR")
    private String bdxgr;

    /**
     * 注释: varchar
     */
    @Column(name = "BDXGRDWDM")
    private String bdxgrdwdm;

    /**
     * 注释: varchar
     */
    @Column(name = "BDXGRDWMC")
    private String bdxgrdwmc;

    /**
     * 注释: Date
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name = "BDXGRQ")
    private Date bdxgrq;

    /**
     * 注释: Short是否复吸犯（0：是1：不是）
     */
    @Column(name = "ISFX")
    private Short isfx;

    /**
     * 注释: varchar
     */
    @Column(name = "ZHPK")
    private String zhpk;

    /**
     * 注释: varchar
     */
    @Column(name = "SSZH")
    private String sszh;

    /**
     * 注释: varchar
     */
    @Column(name = "SSCJ")
    private Short sscj;

    @Transient
    private Short lx;//区分1涉毒2吸毒

    public Short getLx() {
        return lx;
    }

    public void setLx(Short lx) {
        this.lx = lx;
    }

    public String getSszh() {
        return sszh;
    }

    public void setSszh(String sszh) {
        this.sszh = sszh;
    }

    public Short getSscj() {
        return sscj;
    }

    public void setSscj(Short sscj) {
        this.sscj = sscj;
    }

    public String getZhpk() {
        return zhpk;
    }

    public void setZhpk(String zhpk) {
        this.zhpk = zhpk;
    }

    public String getRyxh() {
        return ryxh;
    }

    public void setRyxh(String ryxh) {
        this.ryxh = ryxh;
    }

    public String getXdryly() {
        return xdryly;
    }

    public void setXdryly(String xdryly) {
        this.xdryly = xdryly;
    }

    public String getYwlx() {
        return ywlx;
    }

    public void setYwlx(String ywlx) {
        this.ywlx = ywlx;
    }

    public String getYwxh() {
        return ywxh;
    }

    public void setYwxh(String ywxh) {
        this.ywxh = ywxh;
    }

    public String getXyrbh() {
        return xyrbh;
    }

    public void setXyrbh(String xyrbh) {
        this.xyrbh = xyrbh;
    }

    public String getXm() {
        return xm;
    }

    public void setXm(String xm) {
        this.xm = xm;
    }

    public String getXdrydjlydm() {
        return xdrydjlydm;
    }

    public void setXdrydjlydm(String xdrydjlydm) {
        this.xdrydjlydm = xdrydjlydm;
    }

    public String getCyzjdm() {
        return cyzjdm;
    }

    public void setCyzjdm(String cyzjdm) {
        this.cyzjdm = cyzjdm;
    }

    public String getSfzhm18() {
        return sfzhm18;
    }

    public void setSfzhm18(String sfzhm18) {
        this.sfzhm18 = sfzhm18;
    }

    public String getGmsfhm() {
        return gmsfhm;
    }

    public void setGmsfhm(String gmsfhm) {
        this.gmsfhm = gmsfhm;
    }

    public String getZjhm() {
        return zjhm;
    }

    public void setZjhm(String zjhm) {
        this.zjhm = zjhm;
    }

    public String getBmch() {
        return bmch;
    }

    public void setBmch(String bmch) {
        this.bmch = bmch;
    }

    public String getXbdm() {
        return xbdm;
    }

    public void setXbdm(String xbdm) {
        this.xbdm = xbdm;
    }

    public Date getCsrq() {
        return csrq;
    }

    public void setCsrq(Date csrq) {
        this.csrq = csrq;
    }

    public String getMzdm() {
        return mzdm;
    }

    public void setMzdm(String mzdm) {
        this.mzdm = mzdm;
    }

    public Integer getSg() {
        return sg;
    }

    public void setSg(Integer sg) {
        this.sg = sg;
    }

    public String getJggjdqdm() {
        return jggjdqdm;
    }

    public void setJggjdqdm(String jggjdqdm) {
        this.jggjdqdm = jggjdqdm;
    }

    public String getXldm() {
        return xldm;
    }

    public void setXldm(String xldm) {
        this.xldm = xldm;
    }

    public String getGrsfdm() {
        return grsfdm;
    }

    public void setGrsfdm(String grsfdm) {
        this.grsfdm = grsfdm;
    }

    public String getHyzkdm() {
        return hyzkdm;
    }

    public void setHyzkdm(String hyzkdm) {
        this.hyzkdm = hyzkdm;
    }

    public String getJyqk() {
        return jyqk;
    }

    public void setJyqk(String jyqk) {
        this.jyqk = jyqk;
    }

    public String getGzdw_dwmc() {
        return gzdw_dwmc;
    }

    public void setGzdw_dwmc(String gzdw_dwmc) {
        this.gzdw_dwmc = gzdw_dwmc;
    }

    public String getSzzwbh() {
        return szzwbh;
    }

    public void setSzzwbh(String szzwbh) {
        this.szzwbh = szzwbh;
    }

    public String getRydnabh() {
        return rydnabh;
    }

    public void setRydnabh(String rydnabh) {
        this.rydnabh = rydnabh;
    }

    public String getHjdz_xzqhmc() {
        return hjdz_xzqhmc;
    }

    public void setHjdz_xzqhmc(String hjdz_xzqhmc) {
        this.hjdz_xzqhmc = hjdz_xzqhmc;
    }

    public String getHjdz_dzmc() {
        return hjdz_dzmc;
    }

    public void setHjdz_dzmc(String hjdz_dzmc) {
        this.hjdz_dzmc = hjdz_dzmc;
    }

    public String getHjdz_gajgmc() {
        return hjdz_gajgmc;
    }

    public void setHjdz_gajgmc(String hjdz_gajgmc) {
        this.hjdz_gajgmc = hjdz_gajgmc;
    }

    public String getSjjzd_xzqhmc() {
        return sjjzd_xzqhmc;
    }

    public void setSjjzd_xzqhmc(String sjjzd_xzqhmc) {
        this.sjjzd_xzqhmc = sjjzd_xzqhmc;
    }

    public String getSjjzd_dzmc() {
        return sjjzd_dzmc;
    }

    public void setSjjzd_dzmc(String sjjzd_dzmc) {
        this.sjjzd_dzmc = sjjzd_dzmc;
    }

    public String getSjjzd_gajgmc() {
        return sjjzd_gajgmc;
    }

    public void setSjjzd_gajgmc(String sjjzd_gajgmc) {
        this.sjjzd_gajgmc = sjjzd_gajgmc;
    }

    public String getLrr_xm() {
        return lrr_xm;
    }

    public void setLrr_xm(String lrr_xm) {
        this.lrr_xm = lrr_xm;
    }

    public String getLrdw_dwmc() {
        return lrdw_dwmc;
    }

    public void setLrdw_dwmc(String lrdw_dwmc) {
        this.lrdw_dwmc = lrdw_dwmc;
    }

    public Date getLrsj() {
        return lrsj;
    }

    public void setLrsj(Date lrsj) {
        this.lrsj = lrsj;
    }

    public Integer getSf828k() {
        return sf828k;
    }

    public void setSf828k(Integer sf828k) {
        this.sf828k = sf828k;
    }

    public String getSdrybh() {
        return sdrybh;
    }

    public void setSdrybh(String sdrybh) {
        this.sdrybh = sdrybh;
    }

    public String getYxh() {
        return yxh;
    }

    public void setYxh(String yxh) {
        this.yxh = yxh;
    }

    public String getInoutbz() {
        return inoutbz;
    }

    public void setInoutbz(String inoutbz) {
        this.inoutbz = inoutbz;
    }

    public String getGsdw() {
        return gsdw;
    }

    public void setGsdw(String gsdw) {
        this.gsdw = gsdw;
    }

    public String getCyzk() {
        return cyzk;
    }

    public void setCyzk(String cyzk) {
        this.cyzk = cyzk;
    }

    public String getRybs() {
        return rybs;
    }

    public void setRybs(String rybs) {
        this.rybs = rybs;
    }

    public String getNccbz() {
        return nccbz;
    }

    public void setNccbz(String nccbz) {
        this.nccbz = nccbz;
    }

    public String getCzbs() {
        return czbs;
    }

    public void setCzbs(String czbs) {
        this.czbs = czbs;
    }

    public String getGxsjc() {
        return gxsjc;
    }

    public void setGxsjc(String gxsjc) {
        this.gxsjc = gxsjc;
    }

    public String getXgbsdw() {
        return xgbsdw;
    }

    public void setXgbsdw(String xgbsdw) {
        this.xgbsdw = xgbsdw;
    }

    public String getCcbz() {
        return ccbz;
    }

    public void setCcbz(String ccbz) {
        this.ccbz = ccbz;
    }

    public Integer getYxx() {
        return yxx;
    }

    public void setYxx(Integer yxx) {
        this.yxx = yxx;
    }

    public String getGxsj() {
        return gxsj;
    }

    public void setGxsj(String gxsj) {
        this.gxsj = gxsj;
    }

    public String getLrdw_dwdm() {
        return lrdw_dwdm;
    }

    public void setLrdw_dwdm(String lrdw_dwdm) {
        this.lrdw_dwdm = lrdw_dwdm;
    }

    public String getHjdz_xzqhdm() {
        return hjdz_xzqhdm;
    }

    public void setHjdz_xzqhdm(String hjdz_xzqhdm) {
        this.hjdz_xzqhdm = hjdz_xzqhdm;
    }

    public String getHjdz_gajgdm() {
        return hjdz_gajgdm;
    }

    public void setHjdz_gajgdm(String hjdz_gajgdm) {
        this.hjdz_gajgdm = hjdz_gajgdm;
    }

    public String getSjjzd_xzqhdm() {
        return sjjzd_xzqhdm;
    }

    public void setSjjzd_xzqhdm(String sjjzd_xzqhdm) {
        this.sjjzd_xzqhdm = sjjzd_xzqhdm;
    }

    public String getSjjzd_gajgdm() {
        return sjjzd_gajgdm;
    }

    public void setSjjzd_gajgdm(String sjjzd_gajgdm) {
        this.sjjzd_gajgdm = sjjzd_gajgdm;
    }

    public String getXp() {
        return xp;
    }

    public void setXp(String xp) {
        this.xp = xp;
    }

    public String getFjxh() {
        return fjxh;
    }

    public void setFjxh(String fjxh) {
        this.fjxh = fjxh;
    }

    public String getXxmc() {
        return xxmc;
    }

    public void setXxmc(String xxmc) {
        this.xxmc = xxmc;
    }

    public Short getLy() {
        return ly;
    }

    public void setLy(Short ly) {
        this.ly = ly;
    }

    public String getBdxgr() {
        return bdxgr;
    }

    public void setBdxgr(String bdxgr) {
        this.bdxgr = bdxgr;
    }

    public String getBdxgrdwdm() {
        return bdxgrdwdm;
    }

    public void setBdxgrdwdm(String bdxgrdwdm) {
        this.bdxgrdwdm = bdxgrdwdm;
    }

    public String getBdxgrdwmc() {
        return bdxgrdwmc;
    }

    public void setBdxgrdwmc(String bdxgrdwmc) {
        this.bdxgrdwmc = bdxgrdwmc;
    }

    public Date getBdxgrq() {
        return bdxgrq;
    }

    public void setBdxgrq(Date bdxgrq) {
        this.bdxgrq = bdxgrq;
    }

    public Short getIsfx() {
        return isfx;
    }

    public void setIsfx(Short isfx) {
        this.isfx = isfx;
    }
}
