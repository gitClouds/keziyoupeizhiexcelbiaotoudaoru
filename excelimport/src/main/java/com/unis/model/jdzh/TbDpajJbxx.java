package com.unis.model.jdzh;

import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
/**
* 毒品案件基本信息
*
* @author gongjl
* @version 1.0
* @since 2020-03-10
*/
@Table(name = "TB_DPAJ_JBXX")
public class TbDpajJbxx  {

private static final long serialVersionUID = 1L;

/****************************************
* Basic fields
****************************************/
    /**
    * 记录编号--DExxxxx--编号规则为“XXXXXXXXXXXX（公安机关机构代码）+YYYYMM（年月）+XXXXXXXXXXXXXX（顺序号）”                         : varchar
    */
    @Id
    @Column(name = "JLBH")
    private String jlbh;

    /**
    * 立案编号--DE00092--编号规则为“A+XXXXXXXXXXXX（公安机关机构代码）+YYYYMM（年月）+XXXX（顺序号）”                         : varchar
    */
    @Column(name = "AJBH")
    private String ajbh;

    /**
    * 案件名称--DE00094--: varchar
    */
    @Column(name = "AJMC")
    private String ajmc;

    /**
    * 案件类别--DE00093--采用GA 240.1《刑事犯罪信息管理代码 第1部分: 案件类别代码》: varchar
    */
    @Column(name = "AJLBDM")
    private String ajlbdm;

    /**
    * 发现时间--DE01085、DE01121--: date
    */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Column(name = "FXSJ")
    private Date fxsj;

    /**
    * 发案地点--DE00075--: varchar
    */
    @Column(name = "DZMC")
    private String dzmc;

    /**
    * 发案地点详址--DE00076--: varchar
    */
    @Column(name = "FAZDXZ")
    private String fazdxz;

    /**
    * 破案地区--DE00076--: varchar
    */
    @Column(name = "PADQ")
    private String padq;

    /**
    * 立案单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码: varchar
    */
    @Column(name = "LADW_DWDM")
    private String ladw_dwdm;

    /**
    * 立案/承办单位名称--DE00065--: varchar
    */
    @Column(name = "LADW_DWMC")
    private String ladw_dwmc;

    /**
    * 承办人--DE00002--: varchar
    */
    @Column(name = "CBRXM")
    private String cbrxm;

    /**
    * 承办人联系信息--DE00216--固定电话号码、移动电话号码: varchar
    */
    @Column(name = "CBR_LXDH")
    private String cbr_lxdh;

    /**
    * 立案日期--DE00220--: date
    */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Column(name = "LARQ")
    private Date larq;

    /**
    * 破案单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码: varchar
    */
    @Column(name = "PADW_DWDM")
    private String padw_dwdm;

    /**
    * 破案单位名称--DE00065--: varchar
    */
    @Column(name = "PADW_DWMC")
    private String padw_dwmc;

    /**
    * 破案部门--DE00063--采用GA 300.15《看守所在押人员信息管理代码 第15部分: 办案单位类型代码》: varchar
    */
    @Column(name = "BADWLXDM")
    private String badwlxdm;

    /**
    * 破案日期--DE00223--: date
    */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Column(name = "PARQ")
    private Date parq;

    /**
    * 藏毒方式代码--DE00171--采用GA 332.10《禁毒信息管理代码 第10部分:藏毒方式代码》: varchar
    */
    @Column(name = "CDFSDM")
    private String cdfsdm;

    /**
    * 贩运方式代码--DExxxxx--: varchar
    */
    @Column(name = "FYFSDM")
    private String fyfsdm;

    /**
    * 是否团伙/集团作案--DExxxxx--: decimal
    */
    @Column(name = "SFJT")
    private Short sfjt;

    /**
    * 作案团伙/集团类型--DE00998--采用 GA/T XXX《犯罪团伙性质代码》: varchar
    */
    @Column(name = "FZTHXZDM")
    private String fzthxzdm;

    /**
    * 作案团伙/集团名称--DE00997--: varchar
    */
    @Column(name = "FZTHMC")
    private String fzthmc;

    /**
    * 案情--DE00100--: varchar
    */
    @Column(name = "JYAQ")
    private String jyaq;

    /**
    * 见证人提供情况--DE00521--: varchar
    */
    @Column(name = "JZRTGQK")
    private String jzrtgqk;

    /**
    * 作案人情况分析--DE00521--: varchar
    */
    @Column(name = "ZARQKFX")
    private String zarqkfx;

    /**
    * 侦察措施--DE01009--采用 GA/T XXX《案事件侦查行为分类与代码》: varchar
    */
    @Column(name = "ASJZCXWLBDM")
    private String asjzcxwlbdm;

    /**
    * 破案方式代码--DE00097--采用GA 240.18《刑事犯罪信息管理代码 第18部分: 破案方式代码》: varchar
    */
    @Column(name = "PAFSDM")
    private String pafsdm;

    /**
    * 撤案原因--DE00099--采用GA 398.7《经济犯罪案件管理信息系统技术规范 第7部分：撤销案件原因代码》: varchar
    */
    @Column(name = "CXAJYYDM")
    private String cxajyydm;

    /**
    * 撤案日期--DE01102--: date
    */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Column(name = "CXAJRQ")
    private Date cxajrq;

    /**
    * 查结情况--DE00521--: varchar
    */
    @Column(name = "CJQK")
    private String cjqk;

    /**
    * 查结日期--DE00101--: date
    */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Column(name = "CJRQ")
    private Date cjrq;

    /**
    * 公开方式--DExxxxx--: varchar
    */
    @Column(name = "GKFS")
    private String gkfs;

    /**
    * 完全公开日期--DE00101--: date
    */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Column(name = "WQGKRQ")
    private Date wqgkrq;

    /**
    * 可查状态--DE00596--采用GA/T 2000.40《公安信息代码 第40部分：使用状态代码》: varchar
    */
    @Column(name = "SYZTDM")
    private String syztdm;

    /**
    * 查询单位名称--DE00065--: varchar
    */
    @Column(name = "CXDW_DWMC")
    private String cxdw_dwmc;

    /**
    * 查询单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码: varchar
    */
    @Column(name = "CXDW_DWDM")
    private String cxdw_dwdm;

    /**
    * 案件阶段--DE00096--采用GA 398.5《经济犯罪案件管理信息系统技术规范 第5部分：侦查工作阶段代码》: varchar
    */
    @Column(name = "ZCJDDM")
    private String zcjddm;

    /**
    * 填报单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码: varchar
    */
    @Column(name = "TBDW_DWDM")
    private String tbdw_dwdm;

    /**
    * 填报单位名称--DE00065--: varchar
    */
    @Column(name = "TBDW_DWMC")
    private String tbdw_dwmc;

    /**
    * 登记人--DE00002--: varchar
    */
    @Column(name = "DJR_XM")
    private String djr_xm;

    /**
    * 登记日期--DE00524--: date
    */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Column(name = "DJRQ")
    private Date djrq;

    /**
    * 备注--DE00503--: varchar
    */
    @Column(name = "BZ")
    private String bz;

    /**
    * 审核单位名称--DE00065--: varchar
    */
    @Column(name = "SHDW_DWMC")
    private String shdw_dwmc;

    /**
    * 审核单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码: varchar
    */
    @Column(name = "SHDW_DWDM")
    private String shdw_dwdm;

    /**
    * 缴毒总量（克）--DExxxxx--: decimal
    */
    @Column(name = "JHDP")
    private Short jhdp;

    /**
    * 情报提供单位--DE00065--: varchar
    */
    @Column(name = "QBTGDW_DWMC")
    private String qbtgdw_dwmc;

    /**
    * 情报提供单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码: varchar
    */
    @Column(name = "QBTGDW_DWDM")
    private String qbtgdw_dwdm;

    /**
    * 贩运渠道说明--DE00521--: varchar
    */
    @Column(name = "FYQDSM")
    private String fyqdsm;

    /**
    * 存在不合格--DE00742--判断标识: varchar
    */
    @Column(name = "PDBZ")
    private String pdbz;

    /**
    * 案件代号--DExxxxx--: varchar
    */
    @Column(name = "AJDH")
    private String ajdh;

    /**
    * 案件性质--DExxxxx--: varchar
    */
    @Column(name = "AJXZ")
    private String ajxz;

    /**
    * 主侦单位名称--DE00060--: varchar
    */
    @Column(name = "ZZDW_DWMC")
    private String zzdw_dwmc;

    /**
    * 主侦单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码: varchar
    */
    @Column(name = "ZZDW_DWDM")
    private String zzdw_dwdm;

    /**
    * 指导单位名称--DE00060--: other
    */
    @Column(name = "ZDDW_DWMC")
    private String zddw_dwmc;

    /**
    * 指导单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码: other
    */
    @Column(name = "ZDDW_DWDM")
    private String zddw_dwdm;

    /**
    * 涉案国家--DE00069--采用GB/T 2659《世界各国和地区名称代码》中全部三位字母代码: varchar
    */
    @Column(name = "GJHDQDM")
    private String gjhdqdm;

    /**
    * 涉案地区--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码: varchar
    */
    @Column(name = "SADQ_XZQH")
    private String sadq_xzqh;

    /**
    * 负责人--DE00002--: varchar
    */
    @Column(name = "FZRXM")
    private String fzrxm;

    /**
    * 负责人电话--DE00216--固定电话号码、移动电话号码: other
    */
    @Column(name = "FZR_LXDH")
    private String fzr_lxdh;

    /**
    * 线索来源----: decimal
    */
    @Column(name = "XSLY")
    private Short xsly;

    /**
    * 线索来源单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码: varchar
    */
    @Column(name = "XSLY_DWDM")
    private String xsly_dwdm;

    /**
    * 线索来源单位名称--DE00060--: varchar
    */
    @Column(name = "XSLY_DWMC")
    private String xsly_dwmc;

    /**
    * 其它线索来源--DExxxxx--: varchar
    */
    @Column(name = "QYXSLY")
    private String qyxsly;

    /**
    * 目前侦查工作进展情况--DE01012--: varchar
    */
    @Column(name = "MQZCGZJZQK")
    private String mqzcgzjzqk;

    /**
    * 侦查工作中遇到的主要问题--DE01012--: varchar
    */
    @Column(name = "ZCGZZWT")
    private String zcgzzwt;

    /**
    * 下一步工作建议--DE01012--: varchar
    */
    @Column(name = "XYBGZJY")
    private String xybgzjy;

    /**
    * 请示信息内容(DZWJNR)--DE01076--: varchar
    */
    @Column(name = "INFORCONTENT")
    private String inforcontent;

    /**
    * 督办级别--DE00091--采用GA 398.3《经济犯罪案件管理信息系统技术规范 第3部分：督办级别代码》0 部级  1省级  2市级。默认为空: varchar
    */
    @Column(name = "ASJDBJBDM")
    private String asjdbjbdm;

    /**
    * 破案状态--DExxxxx--: decimal
    */
    @Column(name = "PAZT")
    private Short pazt;

    /**
    * 业务类别--DExxxxx--: decimal
    */
    @Column(name = "YWLB")
    private Short ywlb;

    /**
    * 确立时间--DE00101--: date
    */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Column(name = "QLSJ")
    private Date qlsj;

    /**
    * 再次申报请示--DE01012--: varchar
    */
    @Column(name = "ZCSBQS")
    private String zcsbqs;

    /**
    * 申请联合请示--DE01012--: varchar
    */
    @Column(name = "SQLHQS")
    private String sqlhqs;

    /**
    * 申请联合审批标识--DExxxxx--: decimal
    */
    @Column(name = "SQLHSPBS")
    private Short sqlhspbs;

    /**
    * 联合办案--DExxxxx--: decimal
    */
    @Column(name = "LHBA")
    private Short lhba;

    /**
    * 联合协查机构--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码: varchar
    */
    @Column(name = "LHZC_GAJGJGDM")
    private String lhzc_gajgjgdm;

    /**
    * 案件破案申请表--DE01012--: varchar
    */
    @Column(name = "AJPASQB")
    private String ajpasqb;

    /**
    * 破案确立时间--DE00554--: date
    */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Column(name = "PAQLSJ")
    private Date paqlsj;

    /**
    * 撤销请示--DExxxxx--: varchar
    */
    @Column(name = "CXQS")
    private String cxqs;

    /**
    * 立案/承办地区: varchar
    */
    @Column(name = "LACBDQ")
    private String lacbdq;

    /**
    * 当前审核结果: varchar
    */
    @Column(name = "DQSHJG")
    private String dqshjg;

    /**
    * 审核标志: varchar
    */
    @Column(name = "SHBZ")
    private String shbz;

    /**
    * 更新时间: date
    */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Column(name = "GXSJ")
    private Date gxsj;

    /**
    * 有效性: decimal
    */
    @Column(name = "YXX")
    private Integer yxx;

    /**
    * 更新时间戳--DEXXXXX--: varchar
    */
    @Column(name = "GXSJC")
    private String gxsjc;

    /**
    * 修改部署单位--DEXXXXX--: varchar
    */
    @Column(name = "XGBSDW")
    private String xgbsdw;

    /**
    * 是否互联网案件--46: decimal
    */
    @Column(name = "SFHLWAJ")
    private Short sfhlwaj;

    /**
    * 互联网案件类型--860: varchar
    */
    @Column(name = "HLWAJLX")
    private String hlwajlx;

    /**
    * 其他说明: varchar
    */
    @Column(name = "QTSM")
    private String qtsm;

    /**
    * 是否涉嫌洗钱--46: decimal
    */
    @Column(name = "SFSXXQ")
    private Short sfsxxq;

    /**
    * 涉毒洗钱方式--861: varchar
    */
    @Column(name = "SDXQFS")
    private String sdxqfs;

    /**
    * 其他说明(洗钱): varchar
    */
    @Column(name = "XQQTSM")
    private String xqqtsm;

    /**
    * 是否黑社会性质--DExxxxx--: decimal
    */
    @Column(name = "SFHSH")
    private Short sfhsh;

    /**
    * 是否武装贩毒--DExxxxx--: decimal
    */
    @Column(name = "SFWZFD")
    private Short sfwzfd;

    /**
    * 目标案件操作级别： 1县建议。2县审核。3市建议。4市审核。5省建议。6省审核。7部建议。8部审核。默认为空: decimal
    */
    @Column(name = "MBAJZT")
    private Short mbajzt;

    /**
    * 目标案件操作类别：  0申请目标案件。 1申请降级。2申请破案。3申请撤销。4已确立 。默认为空: decimal
    */
    @Column(name = "AJDJ")
    private Short ajdj;

    /**
    * 案件类型 --DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码: varchar
    */
    @Column(name = "AJLX")
    private String ajlx;

    /**
    * 部级目标案件编号--DExxxxx--编号规则为“XXXX（行政代码）+YYYY（年）+XXXX（顺序号）”: varchar
    */
    @Column(name = "B_M_JLBH")
    private String b_m_jlbh;

    /**
    * 省级目标案件编号--DExxxxx--编号规则为“XXXX（行政代码）+YYYY（年）+XXXX（顺序号）”: varchar
    */
    @Column(name = "S_M_JLBH")
    private String s_m_jlbh;

    /**
    * 市级目标案件编号--DExxxxx--编号规则为“XXXX（行政代码）+YYYY（年）+XXXX（顺序号）”: varchar
    */
    @Column(name = "C_M_JLBH")
    private String c_m_jlbh;

    /**
    * 目标案件录入单位级别  0 部级  1省级  2市级  3县级   4派出所: varchar
    */
    @Column(name = "MBAJSBDJ")
    private String mbajsbdj;

    /**
    * 宣传形式: varchar
    */
    @Column(name = "XCXS")
    private String xcxs;

    /**
    * 是否宣传--46: decimal
    */
    @Column(name = "SFXC")
    private Short sfxc;

    /**
    * 是否群众举报--46: decimal
    */
    @Column(name = "SFQZJB")
    private Short sfqzjb;

    /**
    * 降级申报请示: varchar
    */
    @Column(name = "JJSQXX")
    private String jjsqxx;

    /**
    * 上报单位名称--DE00060--: varchar
    */
    @Column(name = "SBDW_DWMC")
    private String sbdw_dwmc;

    /**
    * 上报单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码: varchar
    */
    @Column(name = "SBDW_DWDM")
    private String sbdw_dwdm;

    /**
    * 联系人: varchar
    */
    @Column(name = "LXR")
    private String lxr;

    /**
    * 联系人电话: varchar
    */
    @Column(name = "LXR_DH")
    private String lxr_dh;

    /**
    * 部级申报目标案件请示信息内容(DZWJNR)--DE01076--: varchar
    */
    @Column(name = "B_INFORCONTENT")
    private String b_inforcontent;

    /**
    * 省级申报目标案件请示信息内容(DZWJNR)--DE01076--: varchar
    */
    @Column(name = "S_INFORCONTENT")
    private String s_inforcontent;

    /**
    * 市级申报目标案件请示信息内容(DZWJNR)--DE01076--: varchar
    */
    @Column(name = "C_INFORCONTENT")
    private String c_inforcontent;

    /**
    * 县级申报目标案件请示信息内容(DZWJNR)--DE01076--: varchar
    */
    @Column(name = "X_INFORCONTENT")
    private String x_inforcontent;

    /**
    * 操作标识: varchar
    */
    @Column(name = "CZBS")
    private String czbs;

    /**
    * 转移清洗毒资方式: varchar
    */
    @Column(name = "ZYQXDZFS")
    private String zyqxdzfs;

    /**
    * 查缉站点编号: varchar
    */
    @Column(name = "CJZDBH")
    private String cjzdbh;

    /**
    * 特情人员编号: varchar
    */
    @Column(name = "TQRYBH")
    private String tqrybh;

    /**
    * 情报产品编号: varchar
    */
    @Column(name = "QBCPBH")
    private String qbcpbh;

    /**
    * 指示降级信息: varchar
    */
    @Column(name = "ZSJJSQXX")
    private String zsjjsqxx;

    /**
    * 部级申报目标案件请示信息时间: date
    */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Column(name = "B_INFORCONTENT_SQSJ")
    private Date b_inforcontent_sqsj;

    /**
    * 省级申报目标案件请示信息时间: date
    */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Column(name = "S_INFORCONTENT_SQSJ")
    private Date s_inforcontent_sqsj;

    /**
    * 市级申报目标案件请示信息时间: date
    */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Column(name = "C_INFORCONTENT_SQSJ")
    private Date c_inforcontent_sqsj;

    /**
    * 县级申报目标案件请示信息时间: date
    */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Column(name = "X_INFORCONTENT_SQSJ")
    private Date x_inforcontent_sqsj;

    /**
    * 报送项目类型: decimal
    */
    @Column(name = "BSXM_LX")
    private Short bsxm_lx;

    /**
    * 部级目标案件确立时间: date
    */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Column(name = "BM_QLSJ")
    private Date bm_qlsj;

    /**
    * 省级目标案件确立时间: date
    */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Column(name = "SM_QLSJ")
    private Date sm_qlsj;

    /**
    * 市级目标案件确立时间: date
    */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Column(name = "CM_QLSJ")
    private Date cm_qlsj;

    /**
    * 传输状态。0:未传输。1:已传输（新老系统数据传输）: varchar
    */
    @Column(name = "CSZT")
    private String cszt;

    /**
    * 注释: varchar
    */
    @Column(name = "CBRXM2")
    private String cbrxm2;

    /**
    * 注释: varchar
    */
    @Column(name = "CBR_LXDH2")
    private String cbr_lxdh2;

    /**
    * 旧系统中破案方式代码--DE00097--采用GA 240.18《刑事犯罪信息管理代码 第18部分: 破案方式代码》: varchar
    */
    @Column(name = "PAFSDM_OLD")
    private String pafsdm_old;

    /**
    * 注释: decimal
    */
    @Column(name = "N_YXH")
    private BigDecimal n_yxh;

    /**
    * 注释: varchar
    */
    @Column(name = "SJLY")
    private String sjly;

    /**
    * 前期处置措施: varchar
    */
    @Column(name = "QQCZCS")
    private String qqczcs;

    /**
    * 种子来源: varchar
    */
    @Column(name = "ZZLY")
    private String zzly;

    /**
    * 目标案件类别: varchar
    */
    @Column(name = "MBAJLB")
    private String mbajlb;

    /**
    * 目标案件序号: varchar
    */
    @Column(name = "MBAJXH")
    private String mbajxh;

    /**
    * 案件来源: varchar
    */
    @Column(name = "AJLY")
    private String ajly;

    /**
    * 禁种铲毒案件类型: varchar
    */
    @Column(name = "JZCD_AJLX")
    private String jzcd_ajlx;

    /**
    * 来源(1：接口下发2：本地录入): decimal
    */
    @Column(name = "LY")
    private Short ly;

    public String getJlbh() {
        return jlbh;
    }

    public void setJlbh(String jlbh) {
        this.jlbh = jlbh;
    }

    public String getAjbh() {
        return ajbh;
    }

    public void setAjbh(String ajbh) {
        this.ajbh = ajbh;
    }

    public String getAjmc() {
        return ajmc;
    }

    public void setAjmc(String ajmc) {
        this.ajmc = ajmc;
    }

    public String getAjlbdm() {
        return ajlbdm;
    }

    public void setAjlbdm(String ajlbdm) {
        this.ajlbdm = ajlbdm;
    }

    public Date getFxsj() {
        return fxsj;
    }

    public void setFxsj(Date fxsj) {
        this.fxsj = fxsj;
    }

    public String getDzmc() {
        return dzmc;
    }

    public void setDzmc(String dzmc) {
        this.dzmc = dzmc;
    }

    public String getFazdxz() {
        return fazdxz;
    }

    public void setFazdxz(String fazdxz) {
        this.fazdxz = fazdxz;
    }

    public String getPadq() {
        return padq;
    }

    public void setPadq(String padq) {
        this.padq = padq;
    }

    public String getLadw_dwdm() {
        return ladw_dwdm;
    }

    public void setLadw_dwdm(String ladw_dwdm) {
        this.ladw_dwdm = ladw_dwdm;
    }

    public String getLadw_dwmc() {
        return ladw_dwmc;
    }

    public void setLadw_dwmc(String ladw_dwmc) {
        this.ladw_dwmc = ladw_dwmc;
    }

    public String getCbrxm() {
        return cbrxm;
    }

    public void setCbrxm(String cbrxm) {
        this.cbrxm = cbrxm;
    }

    public String getCbr_lxdh() {
        return cbr_lxdh;
    }

    public void setCbr_lxdh(String cbr_lxdh) {
        this.cbr_lxdh = cbr_lxdh;
    }

    public Date getLarq() {
        return larq;
    }

    public void setLarq(Date larq) {
        this.larq = larq;
    }

    public String getPadw_dwdm() {
        return padw_dwdm;
    }

    public void setPadw_dwdm(String padw_dwdm) {
        this.padw_dwdm = padw_dwdm;
    }

    public String getPadw_dwmc() {
        return padw_dwmc;
    }

    public void setPadw_dwmc(String padw_dwmc) {
        this.padw_dwmc = padw_dwmc;
    }

    public String getBadwlxdm() {
        return badwlxdm;
    }

    public void setBadwlxdm(String badwlxdm) {
        this.badwlxdm = badwlxdm;
    }

    public Date getParq() {
        return parq;
    }

    public void setParq(Date parq) {
        this.parq = parq;
    }

    public String getCdfsdm() {
        return cdfsdm;
    }

    public void setCdfsdm(String cdfsdm) {
        this.cdfsdm = cdfsdm;
    }

    public String getFyfsdm() {
        return fyfsdm;
    }

    public void setFyfsdm(String fyfsdm) {
        this.fyfsdm = fyfsdm;
    }

    public Short getSfjt() {
        return sfjt;
    }

    public void setSfjt(Short sfjt) {
        this.sfjt = sfjt;
    }

    public String getFzthxzdm() {
        return fzthxzdm;
    }

    public void setFzthxzdm(String fzthxzdm) {
        this.fzthxzdm = fzthxzdm;
    }

    public String getFzthmc() {
        return fzthmc;
    }

    public void setFzthmc(String fzthmc) {
        this.fzthmc = fzthmc;
    }

    public String getJyaq() {
        return jyaq;
    }

    public void setJyaq(String jyaq) {
        this.jyaq = jyaq;
    }

    public String getJzrtgqk() {
        return jzrtgqk;
    }

    public void setJzrtgqk(String jzrtgqk) {
        this.jzrtgqk = jzrtgqk;
    }

    public String getZarqkfx() {
        return zarqkfx;
    }

    public void setZarqkfx(String zarqkfx) {
        this.zarqkfx = zarqkfx;
    }

    public String getAsjzcxwlbdm() {
        return asjzcxwlbdm;
    }

    public void setAsjzcxwlbdm(String asjzcxwlbdm) {
        this.asjzcxwlbdm = asjzcxwlbdm;
    }

    public String getPafsdm() {
        return pafsdm;
    }

    public void setPafsdm(String pafsdm) {
        this.pafsdm = pafsdm;
    }

    public String getCxajyydm() {
        return cxajyydm;
    }

    public void setCxajyydm(String cxajyydm) {
        this.cxajyydm = cxajyydm;
    }

    public Date getCxajrq() {
        return cxajrq;
    }

    public void setCxajrq(Date cxajrq) {
        this.cxajrq = cxajrq;
    }

    public String getCjqk() {
        return cjqk;
    }

    public void setCjqk(String cjqk) {
        this.cjqk = cjqk;
    }

    public Date getCjrq() {
        return cjrq;
    }

    public void setCjrq(Date cjrq) {
        this.cjrq = cjrq;
    }

    public String getGkfs() {
        return gkfs;
    }

    public void setGkfs(String gkfs) {
        this.gkfs = gkfs;
    }

    public Date getWqgkrq() {
        return wqgkrq;
    }

    public void setWqgkrq(Date wqgkrq) {
        this.wqgkrq = wqgkrq;
    }

    public String getSyztdm() {
        return syztdm;
    }

    public void setSyztdm(String syztdm) {
        this.syztdm = syztdm;
    }

    public String getCxdw_dwmc() {
        return cxdw_dwmc;
    }

    public void setCxdw_dwmc(String cxdw_dwmc) {
        this.cxdw_dwmc = cxdw_dwmc;
    }

    public String getCxdw_dwdm() {
        return cxdw_dwdm;
    }

    public void setCxdw_dwdm(String cxdw_dwdm) {
        this.cxdw_dwdm = cxdw_dwdm;
    }

    public String getZcjddm() {
        return zcjddm;
    }

    public void setZcjddm(String zcjddm) {
        this.zcjddm = zcjddm;
    }

    public String getTbdw_dwdm() {
        return tbdw_dwdm;
    }

    public void setTbdw_dwdm(String tbdw_dwdm) {
        this.tbdw_dwdm = tbdw_dwdm;
    }

    public String getTbdw_dwmc() {
        return tbdw_dwmc;
    }

    public void setTbdw_dwmc(String tbdw_dwmc) {
        this.tbdw_dwmc = tbdw_dwmc;
    }

    public String getDjr_xm() {
        return djr_xm;
    }

    public void setDjr_xm(String djr_xm) {
        this.djr_xm = djr_xm;
    }

    public Date getDjrq() {
        return djrq;
    }

    public void setDjrq(Date djrq) {
        this.djrq = djrq;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }

    public String getShdw_dwmc() {
        return shdw_dwmc;
    }

    public void setShdw_dwmc(String shdw_dwmc) {
        this.shdw_dwmc = shdw_dwmc;
    }

    public String getShdw_dwdm() {
        return shdw_dwdm;
    }

    public void setShdw_dwdm(String shdw_dwdm) {
        this.shdw_dwdm = shdw_dwdm;
    }

    public Short getJhdp() {
        return jhdp;
    }

    public void setJhdp(Short jhdp) {
        this.jhdp = jhdp;
    }

    public String getQbtgdw_dwmc() {
        return qbtgdw_dwmc;
    }

    public void setQbtgdw_dwmc(String qbtgdw_dwmc) {
        this.qbtgdw_dwmc = qbtgdw_dwmc;
    }

    public String getQbtgdw_dwdm() {
        return qbtgdw_dwdm;
    }

    public void setQbtgdw_dwdm(String qbtgdw_dwdm) {
        this.qbtgdw_dwdm = qbtgdw_dwdm;
    }

    public String getFyqdsm() {
        return fyqdsm;
    }

    public void setFyqdsm(String fyqdsm) {
        this.fyqdsm = fyqdsm;
    }

    public String getPdbz() {
        return pdbz;
    }

    public void setPdbz(String pdbz) {
        this.pdbz = pdbz;
    }

    public String getAjdh() {
        return ajdh;
    }

    public void setAjdh(String ajdh) {
        this.ajdh = ajdh;
    }

    public String getAjxz() {
        return ajxz;
    }

    public void setAjxz(String ajxz) {
        this.ajxz = ajxz;
    }

    public String getZzdw_dwmc() {
        return zzdw_dwmc;
    }

    public void setZzdw_dwmc(String zzdw_dwmc) {
        this.zzdw_dwmc = zzdw_dwmc;
    }

    public String getZzdw_dwdm() {
        return zzdw_dwdm;
    }

    public void setZzdw_dwdm(String zzdw_dwdm) {
        this.zzdw_dwdm = zzdw_dwdm;
    }

    public String getZddw_dwmc() {
        return zddw_dwmc;
    }

    public void setZddw_dwmc(String zddw_dwmc) {
        this.zddw_dwmc = zddw_dwmc;
    }

    public String getZddw_dwdm() {
        return zddw_dwdm;
    }

    public void setZddw_dwdm(String zddw_dwdm) {
        this.zddw_dwdm = zddw_dwdm;
    }

    public String getGjhdqdm() {
        return gjhdqdm;
    }

    public void setGjhdqdm(String gjhdqdm) {
        this.gjhdqdm = gjhdqdm;
    }

    public String getSadq_xzqh() {
        return sadq_xzqh;
    }

    public void setSadq_xzqh(String sadq_xzqh) {
        this.sadq_xzqh = sadq_xzqh;
    }

    public String getFzrxm() {
        return fzrxm;
    }

    public void setFzrxm(String fzrxm) {
        this.fzrxm = fzrxm;
    }

    public String getFzr_lxdh() {
        return fzr_lxdh;
    }

    public void setFzr_lxdh(String fzr_lxdh) {
        this.fzr_lxdh = fzr_lxdh;
    }

    public Short getXsly() {
        return xsly;
    }

    public void setXsly(Short xsly) {
        this.xsly = xsly;
    }

    public String getXsly_dwdm() {
        return xsly_dwdm;
    }

    public void setXsly_dwdm(String xsly_dwdm) {
        this.xsly_dwdm = xsly_dwdm;
    }

    public String getXsly_dwmc() {
        return xsly_dwmc;
    }

    public void setXsly_dwmc(String xsly_dwmc) {
        this.xsly_dwmc = xsly_dwmc;
    }

    public String getQyxsly() {
        return qyxsly;
    }

    public void setQyxsly(String qyxsly) {
        this.qyxsly = qyxsly;
    }

    public String getMqzcgzjzqk() {
        return mqzcgzjzqk;
    }

    public void setMqzcgzjzqk(String mqzcgzjzqk) {
        this.mqzcgzjzqk = mqzcgzjzqk;
    }

    public String getZcgzzwt() {
        return zcgzzwt;
    }

    public void setZcgzzwt(String zcgzzwt) {
        this.zcgzzwt = zcgzzwt;
    }

    public String getXybgzjy() {
        return xybgzjy;
    }

    public void setXybgzjy(String xybgzjy) {
        this.xybgzjy = xybgzjy;
    }

    public String getInforcontent() {
        return inforcontent;
    }

    public void setInforcontent(String inforcontent) {
        this.inforcontent = inforcontent;
    }

    public String getAsjdbjbdm() {
        return asjdbjbdm;
    }

    public void setAsjdbjbdm(String asjdbjbdm) {
        this.asjdbjbdm = asjdbjbdm;
    }

    public Short getPazt() {
        return pazt;
    }

    public void setPazt(Short pazt) {
        this.pazt = pazt;
    }

    public Short getYwlb() {
        return ywlb;
    }

    public void setYwlb(Short ywlb) {
        this.ywlb = ywlb;
    }

    public Date getQlsj() {
        return qlsj;
    }

    public void setQlsj(Date qlsj) {
        this.qlsj = qlsj;
    }

    public String getZcsbqs() {
        return zcsbqs;
    }

    public void setZcsbqs(String zcsbqs) {
        this.zcsbqs = zcsbqs;
    }

    public String getSqlhqs() {
        return sqlhqs;
    }

    public void setSqlhqs(String sqlhqs) {
        this.sqlhqs = sqlhqs;
    }

    public Short getSqlhspbs() {
        return sqlhspbs;
    }

    public void setSqlhspbs(Short sqlhspbs) {
        this.sqlhspbs = sqlhspbs;
    }

    public Short getLhba() {
        return lhba;
    }

    public void setLhba(Short lhba) {
        this.lhba = lhba;
    }

    public String getLhzc_gajgjgdm() {
        return lhzc_gajgjgdm;
    }

    public void setLhzc_gajgjgdm(String lhzc_gajgjgdm) {
        this.lhzc_gajgjgdm = lhzc_gajgjgdm;
    }

    public String getAjpasqb() {
        return ajpasqb;
    }

    public void setAjpasqb(String ajpasqb) {
        this.ajpasqb = ajpasqb;
    }

    public Date getPaqlsj() {
        return paqlsj;
    }

    public void setPaqlsj(Date paqlsj) {
        this.paqlsj = paqlsj;
    }

    public String getCxqs() {
        return cxqs;
    }

    public void setCxqs(String cxqs) {
        this.cxqs = cxqs;
    }

    public String getLacbdq() {
        return lacbdq;
    }

    public void setLacbdq(String lacbdq) {
        this.lacbdq = lacbdq;
    }

    public String getDqshjg() {
        return dqshjg;
    }

    public void setDqshjg(String dqshjg) {
        this.dqshjg = dqshjg;
    }

    public String getShbz() {
        return shbz;
    }

    public void setShbz(String shbz) {
        this.shbz = shbz;
    }

    public Date getGxsj() {
        return gxsj;
    }

    public void setGxsj(Date gxsj) {
        this.gxsj = gxsj;
    }

    public Integer getYxx() {
        return yxx;
    }

    public void setYxx(Integer yxx) {
        this.yxx = yxx;
    }

    public String getGxsjc() {
        return gxsjc;
    }

    public void setGxsjc(String gxsjc) {
        this.gxsjc = gxsjc;
    }

    public String getXgbsdw() {
        return xgbsdw;
    }

    public void setXgbsdw(String xgbsdw) {
        this.xgbsdw = xgbsdw;
    }

    public Short getSfhlwaj() {
        return sfhlwaj;
    }

    public void setSfhlwaj(Short sfhlwaj) {
        this.sfhlwaj = sfhlwaj;
    }

    public String getHlwajlx() {
        return hlwajlx;
    }

    public void setHlwajlx(String hlwajlx) {
        this.hlwajlx = hlwajlx;
    }

    public String getQtsm() {
        return qtsm;
    }

    public void setQtsm(String qtsm) {
        this.qtsm = qtsm;
    }

    public Short getSfsxxq() {
        return sfsxxq;
    }

    public void setSfsxxq(Short sfsxxq) {
        this.sfsxxq = sfsxxq;
    }

    public String getSdxqfs() {
        return sdxqfs;
    }

    public void setSdxqfs(String sdxqfs) {
        this.sdxqfs = sdxqfs;
    }

    public String getXqqtsm() {
        return xqqtsm;
    }

    public void setXqqtsm(String xqqtsm) {
        this.xqqtsm = xqqtsm;
    }

    public Short getSfhsh() {
        return sfhsh;
    }

    public void setSfhsh(Short sfhsh) {
        this.sfhsh = sfhsh;
    }

    public Short getSfwzfd() {
        return sfwzfd;
    }

    public void setSfwzfd(Short sfwzfd) {
        this.sfwzfd = sfwzfd;
    }

    public Short getMbajzt() {
        return mbajzt;
    }

    public void setMbajzt(Short mbajzt) {
        this.mbajzt = mbajzt;
    }

    public Short getAjdj() {
        return ajdj;
    }

    public void setAjdj(Short ajdj) {
        this.ajdj = ajdj;
    }

    public String getAjlx() {
        return ajlx;
    }

    public void setAjlx(String ajlx) {
        this.ajlx = ajlx;
    }

    public String getB_m_jlbh() {
        return b_m_jlbh;
    }

    public void setB_m_jlbh(String b_m_jlbh) {
        this.b_m_jlbh = b_m_jlbh;
    }

    public String getS_m_jlbh() {
        return s_m_jlbh;
    }

    public void setS_m_jlbh(String s_m_jlbh) {
        this.s_m_jlbh = s_m_jlbh;
    }

    public String getC_m_jlbh() {
        return c_m_jlbh;
    }

    public void setC_m_jlbh(String c_m_jlbh) {
        this.c_m_jlbh = c_m_jlbh;
    }

    public String getMbajsbdj() {
        return mbajsbdj;
    }

    public void setMbajsbdj(String mbajsbdj) {
        this.mbajsbdj = mbajsbdj;
    }

    public String getXcxs() {
        return xcxs;
    }

    public void setXcxs(String xcxs) {
        this.xcxs = xcxs;
    }

    public Short getSfxc() {
        return sfxc;
    }

    public void setSfxc(Short sfxc) {
        this.sfxc = sfxc;
    }

    public Short getSfqzjb() {
        return sfqzjb;
    }

    public void setSfqzjb(Short sfqzjb) {
        this.sfqzjb = sfqzjb;
    }

    public String getJjsqxx() {
        return jjsqxx;
    }

    public void setJjsqxx(String jjsqxx) {
        this.jjsqxx = jjsqxx;
    }

    public String getSbdw_dwmc() {
        return sbdw_dwmc;
    }

    public void setSbdw_dwmc(String sbdw_dwmc) {
        this.sbdw_dwmc = sbdw_dwmc;
    }

    public String getSbdw_dwdm() {
        return sbdw_dwdm;
    }

    public void setSbdw_dwdm(String sbdw_dwdm) {
        this.sbdw_dwdm = sbdw_dwdm;
    }

    public String getLxr() {
        return lxr;
    }

    public void setLxr(String lxr) {
        this.lxr = lxr;
    }

    public String getLxr_dh() {
        return lxr_dh;
    }

    public void setLxr_dh(String lxr_dh) {
        this.lxr_dh = lxr_dh;
    }

    public String getB_inforcontent() {
        return b_inforcontent;
    }

    public void setB_inforcontent(String b_inforcontent) {
        this.b_inforcontent = b_inforcontent;
    }

    public String getS_inforcontent() {
        return s_inforcontent;
    }

    public void setS_inforcontent(String s_inforcontent) {
        this.s_inforcontent = s_inforcontent;
    }

    public String getC_inforcontent() {
        return c_inforcontent;
    }

    public void setC_inforcontent(String c_inforcontent) {
        this.c_inforcontent = c_inforcontent;
    }

    public String getX_inforcontent() {
        return x_inforcontent;
    }

    public void setX_inforcontent(String x_inforcontent) {
        this.x_inforcontent = x_inforcontent;
    }

    public String getCzbs() {
        return czbs;
    }

    public void setCzbs(String czbs) {
        this.czbs = czbs;
    }

    public String getZyqxdzfs() {
        return zyqxdzfs;
    }

    public void setZyqxdzfs(String zyqxdzfs) {
        this.zyqxdzfs = zyqxdzfs;
    }

    public String getCjzdbh() {
        return cjzdbh;
    }

    public void setCjzdbh(String cjzdbh) {
        this.cjzdbh = cjzdbh;
    }

    public String getTqrybh() {
        return tqrybh;
    }

    public void setTqrybh(String tqrybh) {
        this.tqrybh = tqrybh;
    }

    public String getQbcpbh() {
        return qbcpbh;
    }

    public void setQbcpbh(String qbcpbh) {
        this.qbcpbh = qbcpbh;
    }

    public String getZsjjsqxx() {
        return zsjjsqxx;
    }

    public void setZsjjsqxx(String zsjjsqxx) {
        this.zsjjsqxx = zsjjsqxx;
    }

    public Date getB_inforcontent_sqsj() {
        return b_inforcontent_sqsj;
    }

    public void setB_inforcontent_sqsj(Date b_inforcontent_sqsj) {
        this.b_inforcontent_sqsj = b_inforcontent_sqsj;
    }

    public Date getS_inforcontent_sqsj() {
        return s_inforcontent_sqsj;
    }

    public void setS_inforcontent_sqsj(Date s_inforcontent_sqsj) {
        this.s_inforcontent_sqsj = s_inforcontent_sqsj;
    }

    public Date getC_inforcontent_sqsj() {
        return c_inforcontent_sqsj;
    }

    public void setC_inforcontent_sqsj(Date c_inforcontent_sqsj) {
        this.c_inforcontent_sqsj = c_inforcontent_sqsj;
    }

    public Date getX_inforcontent_sqsj() {
        return x_inforcontent_sqsj;
    }

    public void setX_inforcontent_sqsj(Date x_inforcontent_sqsj) {
        this.x_inforcontent_sqsj = x_inforcontent_sqsj;
    }

    public Short getBsxm_lx() {
        return bsxm_lx;
    }

    public void setBsxm_lx(Short bsxm_lx) {
        this.bsxm_lx = bsxm_lx;
    }

    public Date getBm_qlsj() {
        return bm_qlsj;
    }

    public void setBm_qlsj(Date bm_qlsj) {
        this.bm_qlsj = bm_qlsj;
    }

    public Date getSm_qlsj() {
        return sm_qlsj;
    }

    public void setSm_qlsj(Date sm_qlsj) {
        this.sm_qlsj = sm_qlsj;
    }

    public Date getCm_qlsj() {
        return cm_qlsj;
    }

    public void setCm_qlsj(Date cm_qlsj) {
        this.cm_qlsj = cm_qlsj;
    }

    public String getCszt() {
        return cszt;
    }

    public void setCszt(String cszt) {
        this.cszt = cszt;
    }

    public String getCbrxm2() {
        return cbrxm2;
    }

    public void setCbrxm2(String cbrxm2) {
        this.cbrxm2 = cbrxm2;
    }

    public String getCbr_lxdh2() {
        return cbr_lxdh2;
    }

    public void setCbr_lxdh2(String cbr_lxdh2) {
        this.cbr_lxdh2 = cbr_lxdh2;
    }

    public String getPafsdm_old() {
        return pafsdm_old;
    }

    public void setPafsdm_old(String pafsdm_old) {
        this.pafsdm_old = pafsdm_old;
    }

    public BigDecimal getN_yxh() {
        return n_yxh;
    }

    public void setN_yxh(BigDecimal n_yxh) {
        this.n_yxh = n_yxh;
    }

    public String getSjly() {
        return sjly;
    }

    public void setSjly(String sjly) {
        this.sjly = sjly;
    }

    public String getQqczcs() {
        return qqczcs;
    }

    public void setQqczcs(String qqczcs) {
        this.qqczcs = qqczcs;
    }

    public String getZzly() {
        return zzly;
    }

    public void setZzly(String zzly) {
        this.zzly = zzly;
    }

    public String getMbajlb() {
        return mbajlb;
    }

    public void setMbajlb(String mbajlb) {
        this.mbajlb = mbajlb;
    }

    public String getMbajxh() {
        return mbajxh;
    }

    public void setMbajxh(String mbajxh) {
        this.mbajxh = mbajxh;
    }

    public String getAjly() {
        return ajly;
    }

    public void setAjly(String ajly) {
        this.ajly = ajly;
    }

    public String getJzcd_ajlx() {
        return jzcd_ajlx;
    }

    public void setJzcd_ajlx(String jzcd_ajlx) {
        this.jzcd_ajlx = jzcd_ajlx;
    }

    public Short getLy() {
        return ly;
    }

    public void setLy(Short ly) {
        this.ly = ly;
    }
}
