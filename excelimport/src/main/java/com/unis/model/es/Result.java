package com.unis.model.es;

import java.util.HashMap;
import java.util.Map;

/**
 * 结果对象
 * 
 * @author qj
 *
 * @version 1.0
 * @since 2016年12月22日
 */
public class Result {

    /**是否成功， 默认为true */
    private boolean success = true;

    /**错误消息 */
    private String resultMsg;

    /**结果对象 */
    private Object resultObj;

    /**单条处理结果， key为ywsqbh， value为处理结果 */
    private Map<String, String> resultMap = new HashMap<String, String>();
    
    /**
     * 默认构造方法
     */
    public Result(){
        
    }

    /**
     * 设置返回错误消息
     * @param resultMsg
     */
    public Result(String resultMsg) {
        this.success = false;
        this.resultMsg= resultMsg;
    }

    public boolean isSuccess() {
        return success;
    }


    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getResultMsg() {
        return resultMsg;
    }


    public void setResultMsg(String resultMsg) {
        this.resultMsg = resultMsg;
        this.success = false;
    }

    public Object getResultObj() {
        return resultObj;
    }

    public void setResultObj(Object resultObj) {
        this.resultObj = resultObj;
    }

    public Map<String, String> getResultMap() {
        return resultMap;
    }

    public void setResultMap(Map<String, String> resultMap) {
        this.resultMap = resultMap;
    }

}
