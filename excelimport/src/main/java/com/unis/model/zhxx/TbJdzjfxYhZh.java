/*************************************************************************
 Copyright (C) Unpublished Unis Software, Inc. All rights reserved.
 Unis Software, Inc., Confidential and Proprietary.

 This software is subject to copyright protection
 under the laws of the Public of China and other countries.

 Unless otherwise explicitly stated, this software is provided
 by Unis "AS IS".
 *************************************************************************/
package com.unis.model.zhxx;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 警情信息中的银行卡信息一个警情中可以有多个银行卡信息
 *
 * @author fdzptwx
 * @version 1.0
 * @since 2020-02-22
 */

@Table(name = "TB_JDZJFX_YH_ZH")
public class TbJdzjfxYhZh implements Serializable {
    private static final long serialVersionUID = -5309223737750615336L;


    /****************************************
     * Basic fields
     ****************************************/
    /**
     * 警情银行卡ID: varchar
     */
    @Id
    @Column(name = "ID")
    private String id;

    /**
     * 银行账号: varchar
     */
    @Column(name = "ZH")
    private String zh;

    /**
     * 户主姓名: varchar
     */
    @Column(name = "XM")
    private String xm;

    /**
     * 户主证件类型: varchar
     */
    @Column(name = "ZJLX")
    private String zjlx;

    /**
     * 户主的证件号码: varchar
     */
    @Column(name = "ZJHM")
    private String zjhm;

    /**
     * 账号所属银行名称: varchar
     */
    @Column(name = "JGMC")
    private String jgmc;

    /**
     * 账户所属银行代码: varchar
     */
    @Column(name = "JGDM")
    private String jgdm;

    /**
     * 转账时间: date
     */
    @Column(name = "ZZSJ")
    private Date zzsj;

    /**
     * 转账金额: decimal
     */
    @Column(name = "ZZJE")
    private BigDecimal zzje;

    /**
     * 录入人: varchar
     */
    @Column(name = "LRR")
    private String lrr;

    /**
     * 录入时间: date
     */
    @Column(name = "LRSJ")
    private Date lrsj;

    /**
     * 所属关系，1：受害人，2：嫌疑人: varchar
     */
    @Column(name = "SSGX")
    private String ssgx;

    /**
     * 账户类型 01-个人 02：对公: varchar
     */
    @Column(name = "ZHLB")
    private String zhlb;

    /**
     * 录入人姓名: varchar
     */
    @Column(name = "LRRXM")
    private String lrrxm;

    /**
     * 录入人单位: varchar
     */
    @Column(name = "LRDW")
    private String lrdw;

    /**
     * 录入单位名称: varchar
     */
    @Column(name = "LRRDWMC")
    private String lrrdwmc;

    /**
     * 修改人姓名: varchar
     */
    @Column(name = "XGRXM")
    private String xgrxm;

    /**
     * 修改人: varchar
     */
    @Column(name = "XGR")
    private String xgr;

    /**
     * 修改人单位代码: varchar
     */
    @Column(name = "XGRDWDM")
    private String xgrdwdm;

    /**
     * 修改人单位名称: varchar
     */
    @Column(name = "XGRDWMC")
    private String xgrdwmc;

    /**
     * 修改时间: date
     */
    @Column(name = "XGSJ")
    private Date xgsj;

    /**
     * 有效性: varchar
     */
    @Column(name = "YXX")
    private String yxx;

    /**
     * 转账方式: varchar
     */
    @Column(name = "ZZFS")
    private String zzfs;

    /**
     * 是否已止付 0：未止付；1：已止付: varchar
     */
    @Column(name = "SFYZF")
    private String sfyzf;

    /**
     * 是否已冻结 0：未冻结； 1：已冻结: varchar
     */
    @Column(name = "SFYDJ")
    private String sfydj;

    /**
     * 余额: decimal
     */
    @Column(name = "YE")
    private BigDecimal ye;

    /**
     * 层级: varchar
     */
    @Column(name = "CJ")
    private String cj;

    /**
     * 是否为延伸卡号 0：否； 1：是: varchar
     */
    @Column(name = "SFYS")
    private String sfys;

    /**
     * 是否资金返还 0：否 1：是: varchar
     */
    @Column(name = "ZJFH")
    private String zjfh;

    /**
     * 账号类型：1：银行卡 3：pos机: varchar
     */
    @Column(name = "ZHLX")
    private String zhlx;

    /**
     * 返还金额: decimal
     */
    @Column(name = "FHJE")
    private BigDecimal fhje;

    /**
     * 是否下发: varchar
     */
    @Column(name = "SFXF")
    private String sfxf;

    /**
     * 实际返还金额: decimal
     */
    @Column(name = "SFJE")
    private BigDecimal sfje;

    /**
     * 资金返还状态 0：未下发 1：已下发 2：已完成: varchar
     */
    @Column(name = "ZJFHZT")
    private String zjfhzt;

    /**
     * 注释: varchar
     */
    @Column(name = "YHKSQBH")
    private String yhksqbh;

    /**
     * 注释: varchar
     */
    @Column(name = "KSZFBS")
    private String kszfbs;

    /**
     * 注释: varchar
     */
    @Column(name = "JJBH")
    private String jjbh;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getZh() {
        return zh;
    }

    public void setZh(String zh) {
        this.zh = zh;
    }

    public String getXm() {
        return xm;
    }

    public void setXm(String xm) {
        this.xm = xm;
    }

    public String getZjlx() {
        return zjlx;
    }

    public void setZjlx(String zjlx) {
        this.zjlx = zjlx;
    }

    public String getZjhm() {
        return zjhm;
    }

    public void setZjhm(String zjhm) {
        this.zjhm = zjhm;
    }

    public String getJgmc() {
        return jgmc;
    }

    public void setJgmc(String jgmc) {
        this.jgmc = jgmc;
    }

    public String getJgdm() {
        return jgdm;
    }

    public void setJgdm(String jgdm) {
        this.jgdm = jgdm;
    }

    public Date getZzsj() {
        return zzsj;
    }

    public void setZzsj(Date zzsj) {
        this.zzsj = zzsj;
    }

    public BigDecimal getZzje() {
        return zzje;
    }

    public void setZzje(BigDecimal zzje) {
        this.zzje = zzje;
    }

    public String getLrr() {
        return lrr;
    }

    public void setLrr(String lrr) {
        this.lrr = lrr;
    }

    public Date getLrsj() {
        return lrsj;
    }

    public void setLrsj(Date lrsj) {
        this.lrsj = lrsj;
    }

    public String getSsgx() {
        return ssgx;
    }

    public void setSsgx(String ssgx) {
        this.ssgx = ssgx;
    }

    public String getZhlb() {
        return zhlb;
    }

    public void setZhlb(String zhlb) {
        this.zhlb = zhlb;
    }

    public String getLrrxm() {
        return lrrxm;
    }

    public void setLrrxm(String lrrxm) {
        this.lrrxm = lrrxm;
    }

    public String getLrdw() {
        return lrdw;
    }

    public void setLrdw(String lrdw) {
        this.lrdw = lrdw;
    }

    public String getLrrdwmc() {
        return lrrdwmc;
    }

    public void setLrrdwmc(String lrrdwmc) {
        this.lrrdwmc = lrrdwmc;
    }

    public String getXgrxm() {
        return xgrxm;
    }

    public void setXgrxm(String xgrxm) {
        this.xgrxm = xgrxm;
    }

    public String getXgr() {
        return xgr;
    }

    public void setXgr(String xgr) {
        this.xgr = xgr;
    }

    public String getXgrdwdm() {
        return xgrdwdm;
    }

    public void setXgrdwdm(String xgrdwdm) {
        this.xgrdwdm = xgrdwdm;
    }

    public String getXgrdwmc() {
        return xgrdwmc;
    }

    public void setXgrdwmc(String xgrdwmc) {
        this.xgrdwmc = xgrdwmc;
    }

    public Date getXgsj() {
        return xgsj;
    }

    public void setXgsj(Date xgsj) {
        this.xgsj = xgsj;
    }

    public String getYxx() {
        return yxx;
    }

    public void setYxx(String yxx) {
        this.yxx = yxx;
    }

    public String getZzfs() {
        return zzfs;
    }

    public void setZzfs(String zzfs) {
        this.zzfs = zzfs;
    }

    public String getSfyzf() {
        return sfyzf;
    }

    public void setSfyzf(String sfyzf) {
        this.sfyzf = sfyzf;
    }

    public String getSfydj() {
        return sfydj;
    }

    public void setSfydj(String sfydj) {
        this.sfydj = sfydj;
    }

    public BigDecimal getYe() {
        return ye;
    }

    public void setYe(BigDecimal ye) {
        this.ye = ye;
    }

    public String getCj() {
        return cj;
    }

    public void setCj(String cj) {
        this.cj = cj;
    }

    public String getSfys() {
        return sfys;
    }

    public void setSfys(String sfys) {
        this.sfys = sfys;
    }

    public String getZjfh() {
        return zjfh;
    }

    public void setZjfh(String zjfh) {
        this.zjfh = zjfh;
    }

    public String getZhlx() {
        return zhlx;
    }

    public void setZhlx(String zhlx) {
        this.zhlx = zhlx;
    }

    public BigDecimal getFhje() {
        return fhje;
    }

    public void setFhje(BigDecimal fhje) {
        this.fhje = fhje;
    }

    public String getSfxf() {
        return sfxf;
    }

    public void setSfxf(String sfxf) {
        this.sfxf = sfxf;
    }

    public BigDecimal getSfje() {
        return sfje;
    }

    public void setSfje(BigDecimal sfje) {
        this.sfje = sfje;
    }

    public String getZjfhzt() {
        return zjfhzt;
    }

    public void setZjfhzt(String zjfhzt) {
        this.zjfhzt = zjfhzt;
    }

    public String getYhksqbh() {
        return yhksqbh;
    }

    public void setYhksqbh(String yhksqbh) {
        this.yhksqbh = yhksqbh;
    }

    public String getKszfbs() {
        return kszfbs;
    }

    public void setKszfbs(String kszfbs) {
        this.kszfbs = kszfbs;
    }

    public String getJjbh() {
        return jjbh;
    }

    public void setJjbh(String jjbh) {
        this.jjbh = jjbh;
    }

    @Override
    public String toString() {
        return "TbJdzjfxYhZh{" +
                "id='" + id + '\'' +
                ", zh='" + zh + '\'' +
                ", xm='" + xm + '\'' +
                ", zjlx='" + zjlx + '\'' +
                ", zjhm='" + zjhm + '\'' +
                ", jgmc='" + jgmc + '\'' +
                ", jgdm='" + jgdm + '\'' +
                ", zzsj=" + zzsj +
                ", zzje='" + zzje + '\'' +
                ", lrr='" + lrr + '\'' +
                ", lrsj=" + lrsj +
                ", ssgx='" + ssgx + '\'' +
                ", zhlb='" + zhlb + '\'' +
                ", lrrxm='" + lrrxm + '\'' +
                ", lrdw='" + lrdw + '\'' +
                ", lrrdwmc='" + lrrdwmc + '\'' +
                ", xgrxm='" + xgrxm + '\'' +
                ", xgr='" + xgr + '\'' +
                ", xgrdwdm='" + xgrdwdm + '\'' +
                ", xgrdwmc='" + xgrdwmc + '\'' +
                ", xgsj=" + xgsj +
                ", yxx='" + yxx + '\'' +
                ", zzfs='" + zzfs + '\'' +
                ", sfyzf='" + sfyzf + '\'' +
                ", sfydj='" + sfydj + '\'' +
                ", ye='" + ye + '\'' +
                ", cj='" + cj + '\'' +
                ", sfys='" + sfys + '\'' +
                ", zjfh='" + zjfh + '\'' +
                ", zhlx='" + zhlx + '\'' +
                ", fhje='" + fhje + '\'' +
                ", sfxf='" + sfxf + '\'' +
                ", sfje='" + sfje + '\'' +
                ", zjfhzt='" + zjfhzt + '\'' +
                ", yhksqbh='" + yhksqbh + '\'' +
                ", kszfbs='" + kszfbs + '\'' +
                ", jjbh='" + jjbh + '\'' +
                '}';
    }
}
