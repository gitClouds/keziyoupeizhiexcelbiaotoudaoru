package com.unis.common.util;

import com.unis.pojo.CxFlwsJtl;
import com.unis.pojo.ZfflwsJtl;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.RandomUtils;

import javax.imageio.ImageIO;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.font.TextAttribute;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.AttributedString;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Administrator on 2019/1/31/031.
 */
public class CreateImgUtil {

    /**
     * 根据CxFlwsJtl 生成查询法律文书
     * @param bean
     * @return
     */
    public static byte[] createCxFlwsImg(CxFlwsJtl bean) throws IOException {
        Calendar ca = Calendar.getInstance();
        ca.setTime(new Date());

        int width = 595;
        width *= 2;
        int height = 842;
        height *= 2;
        // 创建BufferedImage对象
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        // 获取Graphics2D
        Graphics2D g2d = image.createGraphics();
// 画图
        g2d.setBackground(new Color(255, 255, 255));
        // g2d.setPaint(new Color(0,0,0));
        g2d.setColor(Color.BLACK);
        g2d.clearRect(0, 0, width, height);
        Font font = new Font("宋体", Font.PLAIN, 20);
        g2d.setFont(font);
        // 抗锯齿
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        // 加粗矩形框
        int rectWidth = 420;
        rectWidth *= 2;
        int rectHeight = 610;
        rectHeight *= 2;
        int rectX = (width-rectWidth)/2;
        int rectY = 100;
        rectY *= 2;
        g2d.setStroke(new BasicStroke(3.0f));
        g2d.drawRect(rectX,rectY,rectWidth,rectHeight);//画线框

        // 细矩形框
        rectWidth -= 12;
        rectHeight -= 12;
        rectX = (width-rectWidth)/2;
        rectY += 6;
        g2d.setStroke(new BasicStroke(1.0f));
        g2d.drawRect(rectX,rectY,rectWidth,rectHeight);//画线框

        // ******************************* 标题
        int y = rectY;
        y += 100;
        String text = bean.getDwmc();//"无锡市公安局惠山分局";
        // 计算文字长度，计算居中的x点坐标
        font = new Font("宋体", Font.BOLD, 32);
        g2d.setFont(font);
        FontMetrics fm = g2d.getFontMetrics(font);
        int textWidth = fm.stringWidth(text);
        int widthX = (width - textWidth) / 2;
        // 表示这段文字在图片上的位置(x,y) .第一个是你设置的内容。
        g2d.drawString(text, widthX, y);

        // ******************************* 大标题
        y += 80;
        text = "协助查询财产通知书";
        // 计算文字长度，计算居中的x点坐标
        font = new Font("宋体", Font.BOLD, 52);
        g2d.setFont(font);
        fm = g2d.getFontMetrics(font);
        textWidth = fm.stringWidth(text);
        widthX = (width - textWidth) / 2;
        // 表示这段文字在图片上的位置(x,y) .第一个是你设置的内容。
        g2d.drawString(text, widthX, y);

        // ********************************副标题
        y += 80;
        text = bean.getDwjc()+"调证字 ["+bean.getDzyear()+"] "+bean.getDzz()+" 号";//"惠公（刑）调证字 ["+baYear+"] "+dzzh+" 号";
        Font contentFont = new Font("宋体", Font.BOLD, 28);
        g2d.setFont(contentFont);
        fm = g2d.getFontMetrics(contentFont);
        textWidth = fm.stringWidth(text);
        widthX = (width - textWidth -rectX-10);
        g2d.drawString(text, widthX, y);

        // *******************************正文头部
        y += 80;
        text = " "+bean.getYhjg()+"：";//" "+yhjg+"：";
        AttributedString as = new AttributedString(text);
        as.addAttribute(TextAttribute.FONT, contentFont);
        as.addAttribute(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON, 0, text.length());
        g2d.setFont(contentFont);
        fm = g2d.getFontMetrics(contentFont);
        g2d.drawString(as.getIterator(), rectX, y);

        // *******************************正文内容
        y += 60;
        text = "    ";
        text += "因侦查犯罪需要，根据《中华人民共和国刑事诉讼法》";
        g2d.drawString(text, rectX, y);

        y += 60;
        text = "第一百四十二条之规定，我局派员前往你处查询犯罪嫌疑人";
        g2d.drawString(text, rectX, y);

        //姓名
        text = StringUtils.isNotBlank(bean.getShr())?" "+bean.getShr()+" ":"   ";
        String text2 = text;
        /*as = new AttributedString(text);
        as.addAttribute(TextAttribute.FONT, contentFont);
        as.addAttribute(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON, 0, text.length());
        g2d.setFont(contentFont);
        g2d.drawString(as.getIterator(), rectX, y);*/
        textWidth = 0;
        int textWidth0 = g2d.getFontMetrics(contentFont).stringWidth(text);
        if(textWidth0+60<rectWidth) {
            as = new AttributedString(text);
            as.addAttribute(TextAttribute.FONT, contentFont);
            as.addAttribute(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON, 0, text.length());
            g2d.setFont(contentFont);
            g2d.drawString(as.getIterator(), rectX, y += 60);
        }else{
            char[] text_xm = text.toCharArray();
            String textXmTmp = "";
            int x = 0;
            int text2Width = textWidth;
            boolean first = true;
            for (; x < text_xm.length;) {
                textWidth = text2Width+ g2d.getFontMetrics(contentFont).stringWidth(textXmTmp);
                if(textWidth > rectWidth && StrUtil.checkIsNaN(textXmTmp)){
                    x -= 2;
                    textXmTmp = textXmTmp.substring(0, textXmTmp.length()-2);
                    textXmTmp += "";
                    if (text2Width==0){
                        y += 60;
                    }
                    as = new AttributedString(textXmTmp.trim());
                    as.addAttribute(TextAttribute.FONT, contentFont);
                    as.addAttribute(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON, 0, textXmTmp.trim().length());
                    g2d.setFont(contentFont);
                    if (first){
                        g2d.drawString(as.getIterator(), rectX, y);
                    }else{
                        widthX = (textWidth+rectX+70);
                        g2d.drawString(as.getIterator(), widthX, y);
                    }
                    /*widthX = (text2Width+rectX+10);
                    g2d.drawString(textXmTmp.trim(), widthX, y);*/
                    textXmTmp = "";
                    text2Width =0;
                }else{
                    textXmTmp += text_xm[x++]+"";
                }
            }
            textWidth = text2Width + g2d.getFontMetrics(contentFont).stringWidth(textXmTmp);
            text2 = textXmTmp;
            if(textWidth+60 <= rectWidth){
                textXmTmp += "";
                y += 60;
                as = new AttributedString(textXmTmp.trim());
                as.addAttribute(TextAttribute.FONT, contentFont);
                as.addAttribute(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON, 0, textXmTmp.trim().length());
                g2d.setFont(contentFont);
                widthX = (rectX+10);
                g2d.drawString(as.getIterator(), widthX, y);
            }
        }

        text ="(性别__出生日期______)的财产，请予协助！";
        textWidth = g2d.getFontMetrics(contentFont).stringWidth(text2);
        int textWidth2 = g2d.getFontMetrics(contentFont).stringWidth(text2+text);
        /*textWidth = 0;
        int textWidth2 = g2d.getFontMetrics(contentFont).stringWidth(text);*/
        if (textWidth+60>rectWidth){
            fm = g2d.getFontMetrics(contentFont);
            textWidth = fm.stringWidth(text2);
            widthX = (textWidth+rectX+10);
            g2d.drawString(text, widthX, y+60);
        }else if(textWidth2+60<rectWidth) {
            fm = g2d.getFontMetrics(contentFont);
            textWidth = fm.stringWidth(text2);
            widthX = (textWidth+rectX+10);
            g2d.drawString(text, widthX, y);
        }else{
            char[] text_xm = text.toCharArray();
            String textXmTmp = "";
            int x = 0;
            int text2Width = textWidth;
            boolean first = true;
            for (; x < text_xm.length;) {
                textWidth = text2Width+ g2d.getFontMetrics(contentFont).stringWidth(textXmTmp);
                if(textWidth > rectWidth && StrUtil.checkIsNaN(textXmTmp)){
                    x -= 2;
                    textXmTmp = textXmTmp.substring(0, textXmTmp.length()-2);
                    textXmTmp += "";
                    if (text2Width==0){
                        y += 60;
                    }
                    /*if (first){
                        g2d.drawString(textXmTmp.trim(), rectX, y);
                    }else{
                        widthX = (textWidth+rectX+70);
                        g2d.drawString(textXmTmp.trim(), widthX, y);
                    }*/
                    widthX = (text2Width+rectX+10);
                    g2d.drawString(textXmTmp.trim(), widthX, y);
                    textXmTmp = "";
                    text2Width =0;
                }else{
                    textXmTmp += text_xm[x++]+"";
                }
            }
            textWidth = text2Width + g2d.getFontMetrics(contentFont).stringWidth(textXmTmp);
            if(textWidth+60 <= rectWidth){
                textXmTmp += " ";
                y += 60;
                widthX = (rectX+10);
                g2d.drawString(textXmTmp, widthX, y);
            }
        }

        /*char[] text_r1 = text.toCharArray();
        String textTmp1 = "";
        int j = 0;
        for (; j < text_r1.length;) {
            textWidth = g2d.getFontMetrics(contentFont).stringWidth(textTmp1);
            if(textWidth+60 > rectWidth && StrUtil.checkIsNaN(textTmp1)){
                j -= 2;
                textTmp1 = textTmp1.substring(0, textTmp1.length()-2);
                textTmp1 += " ";
                y += 60;
                g2d.drawString(textTmp1, rectX, y);
                textTmp1 = "";
            }else{
                textTmp1 += text_r1[j++]+"";
            }
        }
        textWidth = g2d.getFontMetrics(contentFont).stringWidth(textTmp1);
        if(textWidth+60 <= rectWidth){
            textTmp1 += " ";
            y += 60;
            g2d.drawString(textTmp1, rectX, y);
        }*/

/*        String text2 = text;
        //性别
        text="(性别";
        fm = g2d.getFontMetrics(contentFont);
        textWidth = fm.stringWidth(text2);
        widthX = (textWidth+rectX+10);
        g2d.drawString(text, widthX, y);

        text2+=text;
        text="  ";
        as = new AttributedString(text);
        as.addAttribute(TextAttribute.FONT, contentFont);
        as.addAttribute(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON, 0, text.length());
        g2d.setFont(contentFont);
        fm = g2d.getFontMetrics(contentFont);
        textWidth = fm.stringWidth(text2);
        widthX = (textWidth+rectX+10);
        g2d.drawString(as.getIterator(), widthX, y);

        text2+=text;
        text="出生日期";
        fm = g2d.getFontMetrics(contentFont);
        textWidth = fm.stringWidth(text2);
        widthX = (textWidth+rectX+10);
        g2d.drawString(text, widthX, y);

        text2+=text;
        text="    ";
        as = new AttributedString(text);
        as.addAttribute(TextAttribute.FONT, contentFont);
        as.addAttribute(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON, 0, text.length());
        g2d.setFont(contentFont);
        fm = g2d.getFontMetrics(contentFont);
        textWidth = fm.stringWidth(text2);
        widthX = (textWidth+rectX+10);
        g2d.drawString(as.getIterator(), widthX, y);

        text2+=text;
        text=")的财产，请予协助！";
        *//*fm = g2d.getFontMetrics(contentFont);
        textWidth = fm.stringWidth(text2);
        widthX = (textWidth+rectX+10);
        g2d.drawString(text, widthX, y);*//*

        textWidth = g2d.getFontMetrics(contentFont).stringWidth(text2);

        int textWidth2 = g2d.getFontMetrics(contentFont).stringWidth(text2+text);
        if (textWidth+60>rectWidth){
            fm = g2d.getFontMetrics(contentFont);
            textWidth = fm.stringWidth(text2);
            widthX = (textWidth+rectX+10);
            g2d.drawString(text, widthX, y+60);
        }else if(textWidth2+60<rectWidth) {
            fm = g2d.getFontMetrics(contentFont);
            textWidth = fm.stringWidth(text2);
            widthX = (textWidth+rectX+10);
            g2d.drawString(text, widthX, y);
        }else{
            char[] text_xm = text.toCharArray();
            String textXmTmp = "";
            int x = 0;
            int text2Width = textWidth;
            for (; x < text_xm.length;) {
                textWidth = text2Width+ g2d.getFontMetrics(contentFont).stringWidth(textXmTmp);
                if(textWidth+60 > rectWidth){
                    x -= 2;
                    textXmTmp = textXmTmp.substring(0, textXmTmp.length()-2);
                    textXmTmp += " ";
                    if (text2Width==0){
                        y += 60;
                    }
                    fm = g2d.getFontMetrics(contentFont);
                    widthX = (textWidth+rectX+10);
                    g2d.drawString(textXmTmp, widthX-80, y);
                    textXmTmp = "";
                    text2Width =0;
                }else{
                    textXmTmp += text_xm[x++]+"";
                }
            }
            textWidth = text2Width + g2d.getFontMetrics(contentFont).stringWidth(textXmTmp);
            if(textWidth+60 <= rectWidth){
                textXmTmp += " ";
                y += 60;
                fm = g2d.getFontMetrics(contentFont);
                widthX = (rectX+10);
                g2d.drawString(textXmTmp, widthX, y);
            }
        }
        text2 = null;*/

        y += 60;
        text="财产种类：";
        g2d.drawString(text, rectX+60, y);

        text ="查询线索："+
             //   " "+bean.getZhlx()+": "+
                bean.getZh()+" 的交易明细、开户信息、联系方式。";
        char[] text_r = text.toCharArray();
        String textTmp = "";
        int i = 0;
        for (; i < text_r.length;) {
            textWidth = g2d.getFontMetrics(contentFont).stringWidth(textTmp);
            if(textWidth+60 > rectWidth){
                i -= 2;
                textTmp = textTmp.substring(0, textTmp.length()-2);
                textTmp += " ";
                y += 60;
                as = new AttributedString(textTmp);
                as.addAttribute(TextAttribute.FONT, contentFont);
                as.addAttribute(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON, 5, textTmp.length());
                g2d.drawString(as.getIterator(), rectX+60, y);
                textTmp = "";
            }else{
                textTmp += text_r[i++]+"";
            }
        }
        textWidth = g2d.getFontMetrics(contentFont).stringWidth(textTmp);
        if(textWidth+60 <= rectWidth){
            textTmp += " ";
            y += 60;
            as = new AttributedString(textTmp);
            as.addAttribute(TextAttribute.FONT, contentFont);
            as.addAttribute(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON, 0, textTmp.length());
            g2d.drawString(as.getIterator(), rectX+60, y);
        }

        /*y += 60;
        text = "    伪造证据、隐匿证据或者毁灭证据的，将受法律追究。";
        g2d.drawString(text, rectX, y);*/

// ******************************** 落款
        y += 240;


        text = bean.getDwmc();
        contentFont = new Font("宋体", Font.BOLD, 28);
        g2d.setFont(contentFont);
        fm = g2d.getFontMetrics(contentFont);
        textWidth = fm.stringWidth(text);
        widthX = (width - textWidth -rectX-50);
        g2d.drawString(text, widthX, y);

        y += 60;
        String[] DXNum = {"Ｏ","一","二","三","四","五","六","七","八","九","十"
                ,"十一","十二","十三","十四","十五","十六","十七","十八","十九","二十"
                ,"二十一","二十二","二十三","二十四","二十五","二十六","二十七","二十八"
                ,"二十九","三十","三十一"};

        String yearTmp = ca.get(Calendar.YEAR)+"";
        int month = ca.get(Calendar.MONTH)+1;
        int day = ca.get(Calendar.DAY_OF_MONTH);
        String year = "";
        for (i = 0; i < yearTmp.length(); i++) {
            char charAt = yearTmp.charAt(i);
            year += DXNum[Integer.parseInt(charAt+"")];
        }
        text = String.format("%s年%s月%s日", year,DXNum[month],DXNum[day]);
        textWidth = fm.stringWidth(text);
        widthX = width - textWidth - rectX - 50;
        g2d.drawString(text, widthX, y);

        // 加盖电子章
        int zWidth = 240;
        //电子章目前获取的是本地电子章，可把电子章放入系统
        //BufferedImage buffImg = ImageIO.read(new File("D:\\dzz.png"));
        ByteArrayInputStream in = new ByteArrayInputStream(bean.getCxz());
        BufferedImage buffImg = ImageIO.read(in);

        //获取随机数进行 电子章的 偏移，旋转
        int randNum = Integer.parseInt(RandomStringUtils.randomNumeric(2));
        int randNum2 = Integer.parseInt(RandomStringUtils.randomNumeric(2));
        buffImg = rotateImage(buffImg,(randNum+randNum2)/4);

        g2d.drawImage(buffImg, (widthX+textWidth/2)-(zWidth/2)+(randNum-50)/2, y-(zWidth/2)+(randNum2-50)/2, zWidth, zWidth, null);

        // ********************************副标题
        y = rectY+rectHeight+30;
        text = "此联交原证据持有人";
        contentFont = new Font("宋体", Font.BOLD, 24);
        g2d.setFont(contentFont);
        g2d.drawString(text, rectX+20, y);

        // 释放对象
        in.close();
        g2d.dispose();

        ByteArrayOutputStream  os = new ByteArrayOutputStream();
        ImageIO.write(image,"jpg",os);
        byte[] bytes = os.toByteArray();
        os.close();
        return bytes;
    }

    public static byte[] createZfFlwsImg(ZfflwsJtl bean) throws IOException {
        Calendar ca = Calendar.getInstance();
        ca.setTime(bean.getZzsj());

        int width = 595;
        width *= 2;
        int height = 842;
        height *= 2;
        // 创建BufferedImage对象
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        // 获取Graphics2D
        Graphics2D g2d = image.createGraphics();
// 画图
        g2d.setBackground(new Color(255, 255, 255));
        // g2d.setPaint(new Color(0,0,0));
        g2d.setColor(Color.BLACK);
        g2d.clearRect(0, 0, width, height);
        Font font = new Font("宋体", Font.PLAIN, 20);
        g2d.setFont(font);
        // 抗锯齿
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        // 加粗矩形框
        int rectWidth = 420;
        rectWidth *= 2;
        int rectHeight = 610;
        rectHeight *= 2;
        int rectX = (width-rectWidth)/2;
        int rectY = 100;
        rectY *= 2;
        g2d.setStroke(new BasicStroke(3.0f));
        g2d.drawRect(rectX,rectY,rectWidth,rectHeight);//画线框

        // 细矩形框
        rectWidth -= 12;
        rectHeight -= 12;
        rectX = (width-rectWidth)/2;
        rectY += 6;
        g2d.setStroke(new BasicStroke(1.0f));
        g2d.drawRect(rectX,rectY,rectWidth,rectHeight);//画线框

        // ******************************* 标题
        int y = rectY;
        y += 100;
        String text = "情 况 说 明";
        // 计算文字长度，计算居中的x点坐标
        font = new Font("宋体", Font.BOLD, 52);
        g2d.setFont(font);
        FontMetrics fm = g2d.getFontMetrics(font);
        int textWidth = fm.stringWidth(text);
        int widthX = (width - textWidth) / 2;
        // 表示这段文字在图片上的位置(x,y) .第一个是你设置的内容。
        g2d.drawString(text, widthX, y);

        // ******************************* 大标题
        y += 80;
        text = "(止付专用)";
        // 计算文字长度，计算居中的x点坐标
        font = new Font("宋体", Font.BOLD, 32);
        g2d.setFont(font);
        fm = g2d.getFontMetrics(font);
        textWidth = fm.stringWidth(text);
        widthX = (width - textWidth) / 2;
        // 表示这段文字在图片上的位置(x,y) .第一个是你设置的内容。
        g2d.drawString(text, widthX, y);

        // *******************************正文头部
        y += 80;
        text = ""+bean.getYhjg()+"(银行/公司)：";//" "+yhjg+"：";
        Font contentFont = new Font("宋体", Font.BOLD, 28);
        AttributedString as = new AttributedString(text);
        as.addAttribute(TextAttribute.FONT, contentFont);
        g2d.setFont(contentFont);
        fm = g2d.getFontMetrics(contentFont);
        g2d.drawString(as.getIterator(), rectX, y);

        // *******************************正文内容
        //y += 60;
        text = "    ";
        text += ca.get(Calendar.YEAR)+" 年 "+(ca.get(Calendar.MONTH)+1)+" 月 "+ca.get(Calendar.DAY_OF_MONTH)+" 日 "+ca.get(Calendar.HOUR_OF_DAY)+" 时 "+ca.get(Calendar.MINUTE)+" 分 "+ca.get(Calendar.SECOND)+" 秒许，"+bean.getDwmc()+"接报，受害人 "+bean.getShr()+" 在 ";
        text+= ca.get(Calendar.YEAR)+" 年 "+(ca.get(Calendar.MONTH)+1)+" 月 "+ca.get(Calendar.DAY_OF_MONTH)+" 日 "+ca.get(Calendar.HOUR_OF_DAY)+" 时左右被犯罪嫌疑人以 "+bean.getAjlb()+" 的诈骗手法实施诈骗，后受害人通过 "+bean.getZzfs()+" 方式向犯罪嫌疑人提供的账户内汇款 "+bean.getZzje().intValue()+" 元。";
        char[] text_r = text.toCharArray();
        String textTmp = "";
        int i = 0;
        for (; i < text_r.length;) {
            textWidth = g2d.getFontMetrics(contentFont).stringWidth(textTmp);
            if(textWidth+32 > rectWidth){
                i -= 1;
                textTmp = textTmp.substring(0, textTmp.length()-1);
                textTmp += " ";
                y += 60;
                as = new AttributedString(textTmp);
                as.addAttribute(TextAttribute.FONT, contentFont);
                g2d.drawString(as.getIterator(), rectX+10, y);
                textTmp = "";
            }else{
                textTmp += text_r[i++]+"";
            }
        }

        textWidth = g2d.getFontMetrics(contentFont).stringWidth(textTmp);
        //if(textWidth+10 <= rectWidth){
        textTmp += " ";
        y += 60;
        as = new AttributedString(textTmp);
        as.addAttribute(TextAttribute.FONT, contentFont);
        g2d.drawString(as.getIterator(), rectX+10, y);
        //}
        text ="    现申请对涉案账户:"+bean.getZh()+"实施应急止付。";
        text_r = text.toCharArray();
        textTmp = "";
        i = 0;
        for (; i < text_r.length;) {
            textWidth = g2d.getFontMetrics(contentFont).stringWidth(textTmp);
            if(textWidth+32 > rectWidth){
                i -= 1;
                textTmp = textTmp.substring(0, textTmp.length()-1);
                textTmp += " ";
                y += 60;
                as = new AttributedString(textTmp);
                as.addAttribute(TextAttribute.FONT, contentFont);
                g2d.drawString(as.getIterator(), rectX+10, y);
                textTmp = "";
            }else{
                textTmp += text_r[i++]+"";
            }
        }
        textWidth = g2d.getFontMetrics(contentFont).stringWidth(textTmp);
        //(textWidth+10 <= rectWidth){
        textTmp += " ";
        y += 60;
        as = new AttributedString(textTmp);
        as.addAttribute(TextAttribute.FONT, contentFont);
        g2d.drawString(as.getIterator(), rectX+10, y);
        //}

// ******************************** 落款
        y += 240;

        text = bean.getDwmc();
        contentFont = new Font("宋体", Font.BOLD, 28);
        g2d.setFont(contentFont);
        fm = g2d.getFontMetrics(contentFont);
        textWidth = fm.stringWidth(text);
        widthX = (width - textWidth -rectX-50);
        g2d.drawString(text, widthX, y);

        y += 60;
        text = String.format("%s年%s月%s日", ca.get(Calendar.YEAR),ca.get(Calendar.MONTH)+1,ca.get(Calendar.DAY_OF_MONTH));
        textWidth = fm.stringWidth(text);
        widthX = width - textWidth - rectX - 50;
        g2d.drawString(text, widthX, y);

        // 加盖电子章
        int zWidth = 240;
        //电子章目前获取的是本地电子章，可把电子章放入系统
        //BufferedImage buffImg = ImageIO.read(new File("D:\\dzz.png"));
        ByteArrayInputStream in = new ByteArrayInputStream(bean.getZfz());
        BufferedImage buffImg = ImageIO.read(in);

        //获取随机数进行 电子章的 偏移，旋转
        int randNum = Integer.parseInt(RandomStringUtils.randomNumeric(2));
        int randNum2 = Integer.parseInt(RandomStringUtils.randomNumeric(2));
        buffImg = rotateImage(buffImg,(randNum+randNum2)/4);

        g2d.drawImage(buffImg, (widthX+textWidth/2)-(zWidth/2)+(randNum-50)/2, y-(zWidth/2)+randNum2-50, zWidth, zWidth, null);

        // ********************************副标题
        /*y = rectY+rectHeight+30;
        text = "此联交原证据持有人";
        contentFont = new Font("宋体", Font.BOLD, 24);
        g2d.setFont(contentFont);
        g2d.drawString(text, rectX+20, y);*/

        // 释放对象
        in.close();
        g2d.dispose();

        ByteArrayOutputStream  os = new ByteArrayOutputStream();
        ImageIO.write(image,"jpg",os);
        byte[] bytes = os.toByteArray();
        os.close();
        return bytes;
    }

    /**
     * 旋转图片为指定角度
     *
     * @param bufferedimage
     *            目标图像
     * @param degree
     *            旋转角度
     * @return
     */
    private static BufferedImage rotateImage(final BufferedImage bufferedimage,
                                            final int degree) {
        int w = bufferedimage.getWidth();
        int h = bufferedimage.getHeight();
        int type = bufferedimage.getColorModel().getTransparency();
        BufferedImage img;
        Graphics2D graphics2d;
        (graphics2d = (img = new BufferedImage(w, h, type))
                .createGraphics()).setRenderingHint(
                RenderingHints.KEY_INTERPOLATION,
                RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics2d.rotate(Math.toRadians(degree), w / 2, h / 2);
        graphics2d.drawImage(bufferedimage, 0, 0, null);
        graphics2d.dispose();
        return img;
    }

}
