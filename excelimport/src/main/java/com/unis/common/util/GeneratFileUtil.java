package com.unis.common.util;

import freemarker.template.Configuration;
import freemarker.template.Template;
import org.apache.commons.io.FileUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Locale;
import java.util.Map;

/**
 * Created by xuk
 * on 2019/8/15.
 */
public class GeneratFileUtil {
    public static byte[] generatFile(Map dataMap,String templatePath,String templateName) throws Exception {
        Configuration configuration = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
        //configuration.setEncoding(Locale.getDefault(),"UTF-8");
        configuration.setDefaultEncoding("UTF-8");
        configuration.setEncoding(Locale.CHINA,"UTF-8");
        //指定模板路径的第二种方式,我的路径是D:/      还有其他方式
        configuration.setClassForTemplateLoading(GeneratFileUtil.class,templatePath);
        Template t =  configuration.getTemplate(templateName,"UTF-8");
        t.setEncoding("UTF-8");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Writer write = new BufferedWriter(new OutputStreamWriter(baos, "UTF-8"));
        t.process(dataMap, write);
        write.close();
        byte[] bytes = baos.toByteArray();
        baos.flush();
        baos.close();

        return bytes;
    }
    public static byte[] generatHtmlFile(Map dataMap,String templatePath,String templateName) throws Exception {
        Configuration configuration = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
        //configuration.setEncoding(Locale.getDefault(),"UTF-8");
        configuration.setDefaultEncoding("UTF-8");
        configuration.setEncoding(Locale.CHINA,"UTF-8");
        //指定模板路径的第二种方式,我的路径是D:/还有其他方式
        configuration.setClassForTemplateLoading(GeneratFileUtil.class,templatePath);
        //configuration.setDirectoryForTemplateLoading(new File("D:/"));


        Template t =  configuration.getTemplate(templateName,"UTF-8");
        t.setEncoding("UTF-8");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Writer write = new BufferedWriter(new OutputStreamWriter(baos, "UTF-8"));
        t.process(dataMap, write);
        byte[] bytes = baos.toByteArray();
        baos.flush();
        baos.close();

        return bytes;
    }

    public static String generatPDFFile(Map dataMap,String templatePath,String templateName,String filePath) throws Exception {
        HtmlToPdfUtil.html2Pdf(generatHtmlFile(dataMap,templatePath,templateName),filePath);
        return filePath;
    }
    public static void generatPDFFile(Map dataMap,String templatePath,String templateName,HttpServletResponse response) throws Exception {
        HtmlToPdfUtil.html2Pdf(generatHtmlFile(dataMap,templatePath,templateName),response);
    }

    public static byte[] generatPDFFileBytes(Map dataMap,String templatePath,String templateName,String filePath) throws Exception {
        /*Map dataMap = cxws.cxFlwsToMap();
        String filePath = flwsTmpDirPath + TemplateUtil.genUUID()+".pdf";
        System.out.println(filePath);
        //GeneratFileUtil.generatPDFFile(dataMap,"/template","cxflws.ftl",filePath);
        b = GeneratFileUtil.generatPDFFileBytes(dataMap,"/template","cxflws.ftl",filePath);
        */
        generatPDFFile(dataMap, templatePath, templateName, filePath);
        File file = FileUtils.getFile(filePath);
        if (file.exists() && file!=null && file.isFile()){
            byte[] b = FileUtils.readFileToByteArray(file);
            FileUtils.deleteQuietly(file);
            return b;
        }
        return null;
    }
}
