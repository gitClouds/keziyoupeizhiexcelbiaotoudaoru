package com.unis.common.util;

import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by Administrator on 2019/1/8/008.
 */
public class WebUtil {
    /****************************************
     * 获取请求参数hashMap
     *
     * @param request
     * @return map
     ****************************************/
    @SuppressWarnings("all")
    public static final Map<String, Object> getParamsMapFromRequest(HttpServletRequest request) {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        Map reqParamMap = request.getParameterMap();
        Set<Map.Entry<String, Object>> entitySet = reqParamMap.entrySet();
        for (Map.Entry<String, Object> entry : entitySet) {
            String value = ((String[]) entry.getValue())[0];
            //if (!PAGESTART.equals(entry.getKey()) || !PAGELIMIT.equals(entry.getKey())) {
                paramMap.put(entry.getKey(), value);
            //}
        }
        // 处理分页
        /*String start = request.getParameter(PAGESTART);
        String limit = request.getParameter(PAGELIMIT);
        if (StringUtils.isNotBlank(start) && StringUtils.isNotBlank(limit)) {
            paramMap.put(PAGESTART, start);
            paramMap.put(PAGELIMIT, Integer.valueOf(start) + Integer.valueOf(limit));
        }*/
        return paramMap;
    }

    /****************************************
     * 判断是否为Ajax请求
     *
     * @param request
     * @return
     ****************************************/
    public static final boolean isAjax(HttpServletRequest request) {
        return "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
    }

    public enum BrowserType {
        IE10, IE9, IE8, IE7, IE6, Firefox, Safari, Chrome, Opera, Camino, Gecko, Other
    }

    /** 判断是否是IE */
    public static boolean isIE(HttpServletRequest request) {
        return request.getHeader("USER-AGENT").toLowerCase().indexOf("msie") > 0 ? true : false;
    }

    /**
     * 获取浏览器类型
     *
     * @param request
     * @return
     */
    public static BrowserType getBrowserType(HttpServletRequest request) {
        BrowserType browserType = BrowserType.Other;
        if (getBrowserType(request, "msie 10.0")) {
            browserType = BrowserType.IE9;
        }
        if (getBrowserType(request, "msie 9.0")) {
            browserType = BrowserType.IE9;
        }
        if (getBrowserType(request, "msie 8.0")) {
            browserType = BrowserType.IE8;
        }
        if (getBrowserType(request, "msie 7.0")) {
            browserType = BrowserType.IE7;
        }
        if (getBrowserType(request, "msie 6.0")) {
            browserType = BrowserType.IE6;
        }
        if (getBrowserType(request, "firefox")) {
            browserType = BrowserType.Firefox;
        }
        if (getBrowserType(request, "safari")) {
            browserType = BrowserType.Safari;
        }
        if (getBrowserType(request, "chrome")) {
            browserType = BrowserType.Chrome;
        }
        if (getBrowserType(request, "opera")) {
            browserType = BrowserType.Opera;
        }
        if (getBrowserType(request, "camino")) {
            browserType = BrowserType.Camino;
        }
        return browserType;
    }

    private static boolean getBrowserType(HttpServletRequest request, String brosertype) {
        return request.getHeader("USER-AGENT").toLowerCase().indexOf(brosertype) > 0 ? true : false;
    }
}
