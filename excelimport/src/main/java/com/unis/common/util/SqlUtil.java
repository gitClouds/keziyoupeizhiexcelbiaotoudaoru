/************************************************************************
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ************************************************************************/
package com.unis.common.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * @author CSJ (email:raulcsj@163.com)
 * @since 2012-11-8
 */
public final class SqlUtil {
    private SqlUtil() {
    }

    /**
     * 处理字典代码如果只包含0,00,000,..导致查询问题(默认所有字典的顶层父代码为'000000')
     * 
     * @param dm
     * @return
     */
    public static boolean procDictQueryParam(String dm) {
        String regex = "^[0]{1,5}";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(dm);
        return m.matches();
    }

    /**
     * 根据单位代码是否包含@决定查询方式(精确或模糊)
     * 
     * @param deptcode
     * @return
     */
    public static boolean likeQueryByDeptcode(String deptcode) {
        return deptcode.contains("@");
    }

}
