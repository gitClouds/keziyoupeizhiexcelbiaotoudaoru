package com.unis.common.util;

import oracle.sql.CLOB;
import org.apache.commons.lang.StringUtils;

import java.io.BufferedReader;
import java.io.Reader;
import java.sql.Clob;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StrUtil {
    /**
     * clob转String
     * @param clob
     * @return
     */
    public static String ClobtoString(Clob clob){
        String reString = "";
        Reader is = null;
        try {
            is = clob.getCharacterStream();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 得到流
        BufferedReader br = new BufferedReader(is);
        String s = null;
        try {
            s = br.readLine();
        } catch (Exception e) {
            e.printStackTrace();
        }
        StringBuffer sb = new StringBuffer();
        while (s != null) {
            // 执行循环将字符串全部取出付值给StringBuffer由StringBuffer转成STRING
            sb.append(s);
            try {
                s = br.readLine();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        reString = sb.toString();
        return reString;
    }
    /**
     * 验证String是否为空， null或者"" 返回false，否则返回true
     *
     * @param str
     * @return
     */
    public static boolean checkIsNaN(String str) {
        boolean flag = false;
        if (str != null && !"".equals(str.trim())) {
            flag = true;
        }
        return flag;
    }

    /**
     * 将空(null)的String转换成""
     */
    public static String strToStr(String str) throws ParseException {
        if (str == null) {
            str = "";
        } else {
            str = str.trim();
        }
        return str;

    }

    /**
     * 生成指定长度随机数，
     * @param strLength strLength <=6
     * @return
     */
    public static String getFixLenthString(int strLength) {
         Random rm = new Random();
         // 获得随机数
         double pross = (1 + rm.nextDouble()) * Math.pow(10, strLength);
         // 将获得的获得随机数转化为字符串
         String fixLenthString = String.valueOf(pross);
         // 返回固定的长度的随机数
         return fixLenthString.substring(1, strLength + 1);
    }

    public static String removeZero(String str) {
        if(!"000000000000".equals(str)){
            for (int i = 0; i < str.length() / 2; i++) {
                if (i == 0 && str.substring(0, 2).equalsIgnoreCase("00")) {
                    break;
                } else if (str.substring(i * 2, i * 2 + 2).equalsIgnoreCase("00")) {
                    str = str.substring(0, i * 2);
                    break;
                }
            }
        }else{
            str ="";
        }
        return str;
    }
    public static String removeDmZeor(String str){
        if (StringUtils.isNotBlank(str)) {
            str = str.trim();
            if (str.startsWith("01")) {
                return "";
            } else {
                int length = str.length()/2;
                for (int i=0;i<length;i++){
                    if (str.endsWith("00")){
                        str = str.substring(0,str.length()-2);
                    }else{
                        break;
                    }
                }
                return str;
            }
        }else{
            return null;
        }
    }
    /**
     * 判断字符串中是否包含中文
     * @param str
     * 待校验字符串
     * @return 是否为中文
     * @warn 不能校验是否为中文标点符号
     */
    public static boolean isContainChinese(String str) {
        Pattern p = Pattern.compile("[\u4e00-\u9fa5]");
        Matcher m = p.matcher(str);
        if (m.find()) {
            return true;
        }
        return false;
    }

    /**
     * 去除字符串中的中文
     * @param str
     * @return
     */
    public static String removeChinese(String str){
        Pattern p = Pattern.compile("[\u4e00-\u9fa5]");
        Matcher m = p.matcher(str);
        return  m.replaceAll("");
    }
}
