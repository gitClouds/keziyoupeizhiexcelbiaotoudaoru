package com.unis.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

/**
 * Created by Administrator on 2019/11/26.
 */
public class Base64Util {
    public static  String fileToBase64(File file){
        if (file==null){
            return null;
        }
        String base64 = null;
        InputStream in = null;
        try {
            in = new FileInputStream(file);
            byte[] bytes=new byte[(int)file.length()];
            in.read(bytes);
            base64 = byteToBase64(bytes);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return base64;
    }

    public static String byteToBase64(byte[] bytes){
        return Base64.getEncoder().encodeToString(bytes);
    }
}
