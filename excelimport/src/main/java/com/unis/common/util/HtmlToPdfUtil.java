package com.unis.common.util;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfDocumentInfo;
import com.itextpdf.kernel.pdf.PdfString;
import com.itextpdf.kernel.pdf.PdfViewerPreferences;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.WriterProperties;
import com.itextpdf.layout.font.FontProvider;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by Administrator on 2019/11/27.
 */
public class HtmlToPdfUtil {

    public static byte[] html2Pdf(byte[] bytes,String outputPath){
        try {
            WriterProperties writerProperties = new WriterProperties();
            //Add metadata
            writerProperties.addXmpMetadata();
            FileOutputStream fos = new FileOutputStream(outputPath);//outputFile为生成存放的路径如：d:/test.pdf
            PdfWriter pdfWriter = new PdfWriter(fos, writerProperties);

            PdfDocument pdfDoc = new PdfDocument(pdfWriter);
            pdfDoc.getCatalog().setLang(new PdfString("UTF-8"));
            //Set the document to be tagged
            pdfDoc.setTagged();
            pdfDoc.getCatalog().setViewerPreferences(new PdfViewerPreferences().setDisplayDocTitle(true));

            //Set meta tags
            PdfDocumentInfo pdfMetaData = pdfDoc.getDocumentInfo();
            pdfMetaData.setAuthor("jzd");
            pdfMetaData.addCreationDate();
            pdfMetaData.getProducer();
            pdfMetaData.setCreator("fdzpt");
            pdfMetaData.setKeywords("pdfws");
            pdfMetaData.setSubject("fdzpt-pdf-ws");
            //Title is derived from html

            ConverterProperties props = new ConverterProperties();
            FontProvider fp = new FontProvider();
            fp.addStandardPdfFonts();
            fp.addDirectory(outputPath);
            fp.addSystemFonts();
            props.setFontProvider(fp);
            props.setBaseUri(outputPath);
            //inputFile是要转化的内容
            HtmlConverter.convertToPdf(new ByteArrayInputStream(bytes), pdfDoc, props);
            pdfDoc.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void html2Pdf(byte[] bytes,HttpServletResponse response){
        OutputStream os = null;
        try {
            WriterProperties writerProperties = new WriterProperties();
            //Add metadata
            writerProperties.addXmpMetadata();
            //FileOutputStream fos = new FileOutputStream(outputPath);//outputFile为生成存放的路径如：d:/test.pdf
            os = response.getOutputStream();
            PdfWriter pdfWriter = new PdfWriter(os, writerProperties);

            PdfDocument pdfDoc = new PdfDocument(pdfWriter);
            pdfDoc.getCatalog().setLang(new PdfString("UTF-8"));
            //Set the document to be tagged
            pdfDoc.setTagged();
            pdfDoc.getCatalog().setViewerPreferences(new PdfViewerPreferences().setDisplayDocTitle(true));

            //Set meta tags
            PdfDocumentInfo pdfMetaData = pdfDoc.getDocumentInfo();
            pdfMetaData.setAuthor("jzd");
            pdfMetaData.addCreationDate();
            pdfMetaData.getProducer();
            pdfMetaData.setCreator("fdzpt");
            pdfMetaData.setKeywords("pdfws");
            pdfMetaData.setSubject("fdzpt-pdf-ws");
            //Title is derived from html

            ConverterProperties props = new ConverterProperties();
            FontProvider fp = new FontProvider();
            fp.addStandardPdfFonts();
            fp.addDirectory("");
            fp.addSystemFonts();
            props.setFontProvider(fp);
            props.setBaseUri("");
            //inputFile是要转化的内容
            HtmlConverter.convertToPdf(new ByteArrayInputStream(bytes), pdfDoc, props);
            pdfDoc.close();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (os!=null){
                try {
                    os.flush();
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
