/************************************************************************
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ************************************************************************/
package com.unis.common.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.io.StringWriter;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 模板工具类
 * 
 * @author CSJ
 * @version 1.0
 */
public final class TemplateUtil {
    private TemplateUtil() {
    }

    /**
     * 生成随机32位UUID号
     * 
     * @return UUID
     */
    public static final String genUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    /**
     * 生成指定对象JSON 格式
     * 
     * @return JSON String
     */
    public static final String genJsonStr4Obj(Object obj) {
        StringWriter sw = new StringWriter();
        String jsonStr = "{}";
        JsonGenerator jsonGenerator = null;
        try {
            jsonGenerator = new ObjectMapper().getFactory().createGenerator(sw);
            jsonGenerator.writeObject(obj);
            jsonStr = sw.toString();
        } catch (Exception e) {
        } finally {
            if (sw != null) {
                try {
                    sw.close();
                } catch (IOException e) {
                } finally {
                    sw = null;
                }
            }
            if (jsonGenerator != null) {
                try {
                    jsonGenerator.close();
                } catch (IOException e) {
                } finally {
                    jsonGenerator = null;
                }
            }
        }
        return jsonStr;
    }

    /**
     * 反序列化JSON 格式 对象
     * 
     * @param jsonString
     * @param clazz
     * @return
     */
    public static final <T> T genObjFormJson(String jsonString, Class<T> clazz) {
        if (StringUtils.isBlank(jsonString)) {
            return null;
        }
        try {
            return new ObjectMapper().readValue(jsonString, clazz);
        } catch (IOException e) {
            return null;
        }
    }


    /**
     * 检测文本是否包含中文
     * 
     * @param chinese
     * @return
     */
    public static final boolean isChinese(String chinese) {
        if (StringUtils.isBlank(chinese)) {
            return false;
        }
        boolean res = false;
        char[] input = chinese.trim().toCharArray();
        for (int i = 0; i < input.length; i++) {
            if (Character.toString(input[i]).matches("[\u4E00-\u9FA5]+")) {
                res = true;
                break;
            }
        }
        return res;
    }

    /**
     * 检测文本是否只含有英文字符
     * 
     * @param inputStr
     * @return
     */
    public static final boolean isLetter(String inputStr) {
        if (StringUtils.isBlank(inputStr)) {
            return false;
        }
        String regx_letter = "[a-zA-Z]+";
        Pattern p = Pattern.compile(regx_letter);
        Matcher m = p.matcher(inputStr);
        return m.matches();
    }

    /**
     * 生成encache spring key 可变对象参数方法
     * 
     * @param keys
     * @return
     */
    public static final String genCacheKey4Array(String... keys) {
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        for (String key : keys) {
            builder.append(key);
        }
        builder.append("]");
        return builder.toString();
    }
}
