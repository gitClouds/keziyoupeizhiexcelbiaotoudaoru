package com.unis.common.secure.authc;

import org.apache.shiro.authc.UsernamePasswordToken;

/**
 * Created by Administrator on 2018/12/27/027.
 * 扩展 Token
 */
public class AccountLoginToken extends UsernamePasswordToken {
    private static final long serialVersionUID = 1L;
    private UserInfo userInfo;

    public AccountLoginToken(String username, char[] password, boolean rememberMe, String host, UserInfo userInfo) {
        super(username, password, rememberMe, host);
        this.userInfo = userInfo;
    }

    public AccountLoginToken(String username, String password, UserInfo userInfo) {
        super(username, password);
        this.userInfo = userInfo;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }
}
