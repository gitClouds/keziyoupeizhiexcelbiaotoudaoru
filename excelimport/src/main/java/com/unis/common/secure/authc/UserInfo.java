package com.unis.common.secure.authc;

import com.alibaba.fastjson.JSON;
import com.unis.model.system.Account;
import com.unis.model.system.Department;
import com.unis.model.system.Role;

import java.io.Serializable;
import java.util.Set;

/**
 * Created by Administrator on 2018/12/27/027.
 */
public class UserInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    private String pk;

    private String username;

    private String idno;

    private String password;

    private String salt;

    private String xm;

    private String jh;

    private String lxfs;

    private String jgdm;

    private String jgmc;

    private String jz;

    private String zwjb;

    private String yhzsdm;

    private String zt ="1";

    private Account account;


    public UserInfo(){}

    public UserInfo(Account account){
        this.account =account;
        setPk(account.getPk());
        setUsername(account.getUsername());
        setIdno(account.getIdno());
        setPassword(account.getPassword());
        setSalt(account.getSalt());
        setXm(account.getXm());
        setJh(account.getJh());
        setLxfs(account.getLxfs());
        setJgdm(account.getJgdm());
        setJgmc(account.getJgmc());
        setJz(account.getJz());
        setZwjb(account.getZwjb());
        setYhzsdm(account.getYhzsdm());
        setZt(account.getZt());
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    /**
     * Cookie 功能:不持久化
     */
    private boolean remeberMe;

    /**
     * 所属部门: varchar (部门表外键)
     */
    private Department dept;

    /**
     * 角色: varchar (关联表)
     */
    private Set<Role> roles;

    private Set<String> roleCodeSet;

    private Set<String> permissionCodeSet;


    /**
     * @return PK
     */
    public String getPk() {
        return pk;
    }

    /**
     * @param pk
     */
    public void setPk(String pk) {
        this.pk = pk == null ? null : pk.trim();
    }

    /**
     * @return USERNAME
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username
     */
    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    /**
     * @return IDNO
     */
    public String getIdno() {
        return idno;
    }

    /**
     * @param idno
     */
    public void setIdno(String idno) {
        this.idno = idno == null ? null : idno.trim();
    }

    /**
     * @return PASSWORD
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password
     */
    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    /**
     * @return SALT
     */
    public String getSalt() {
        return salt;
    }

    /**
     * @param salt
     */
    public void setSalt(String salt) {
        this.salt = salt == null ? null : salt.trim();
    }

    /**
     * @return XM
     */
    public String getXm() {
        return xm;
    }

    /**
     * @param xm
     */
    public void setXm(String xm) {
        this.xm = xm == null ? null : xm.trim();
    }

    /**
     * @return JH
     */
    public String getJh() {
        return jh;
    }

    /**
     * @param jh
     */
    public void setJh(String jh) {
        this.jh = jh == null ? null : jh.trim();
    }

    /**
     * @return LXFS
     */
    public String getLxfs() {
        return lxfs;
    }

    /**
     * @param lxfs
     */
    public void setLxfs(String lxfs) {
        this.lxfs = lxfs == null ? null : lxfs.trim();
    }

    /**
     * @return JGDM
     */
    public String getJgdm() {
        return jgdm;
    }

    /**
     * @param jgdm
     */
    public void setJgdm(String jgdm) {
        this.jgdm = jgdm == null ? null : jgdm.trim();
    }

    /**
     * @return JGMC
     */
    public String getJgmc() {
        return jgmc;
    }

    /**
     * @param jgmc
     */
    public void setJgmc(String jgmc) {
        this.jgmc = jgmc == null ? null : jgmc.trim();
    }

    /**
     * @return JZ
     */
    public String getJz() {
        return jz;
    }

    /**
     * @param jz
     */
    public void setJz(String jz) {
        this.jz = jz == null ? null : jz.trim();
    }

    /**
     * @return ZWJB
     */
    public String getZwjb() {
        return zwjb;
    }

    /**
     * @param zwjb
     */
    public void setZwjb(String zwjb) {
        this.zwjb = zwjb == null ? null : zwjb.trim();
    }

    /**
     * @return YHZSDM
     */
    public String getYhzsdm() {
        return yhzsdm;
    }

    /**
     * @param yhzsdm
     */
    public void setYhzsdm(String yhzsdm) {
        this.yhzsdm = yhzsdm == null ? null : yhzsdm.trim();
    }

    public String getZt() {
        return zt;
    }

    public void setZt(String zt) {
        this.zt = zt;
    }

    public boolean getRemeberMe() {
        return remeberMe;
    }

    public void setRemeberMe(boolean remeberMe) {
        this.remeberMe = remeberMe;
    }

    public Department getDept() {
        return dept;
    }

    public void setDept(Department dept) {
        this.dept = dept;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Set<String> getRoleCodeSet() {
        return roleCodeSet;
    }

    public void setRoleCodeSet(Set<String> roleCodeSet) {
        this.roleCodeSet = roleCodeSet;
    }

    public Set<String> getPermissionCodeSet() {
        return permissionCodeSet;
    }

    public void setPermissionCodeSet(Set<String> permissionCodeSet) {
        this.permissionCodeSet = permissionCodeSet;
    }

    public Account getAccount() {
        return account;
    }



}
