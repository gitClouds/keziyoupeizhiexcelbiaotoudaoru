package com.unis.common.enums;

/**
 * Created by Administrator on 2019/2/20/020.
 */
public enum QqlxEnum {
    yhkQqlxZhifu("Y-01", "止付"),
    yhkQqlxZfyq("Y-02", "止付延期"),
    yhkQqlxZfjc("Y-03", "止付解除"),
    yhkQqlxDongjie("Y-04", "冻结"),
    yhkQqlxDjyq("Y-05", "冻结延期"),
    yhkQqlxDjjc("Y-06", "冻结解除"),
    yhkQqlxMxcx("Y-07", "明细查询"),
    yhkQqlxZtcx("Y-08", "持卡主体查询"),
    yhkQqlxQzhcx("Y-09", "全账号查询"),
    yhkQqlxDtcx("Y-11", "动态查询"),
    yhkQqlxDtjc("Y-12", "动态解除"),
    yhkQqlxLshCx("Y-19", "按交易流水号查询银行卡/支付账号"),

    sfQqlxZhifu("S-01", "止付"),
    sfQqlxZfyq("S-02", "止付延期"),
    sfQqlxZfjc("S-03", "止付解除"),
    sfQqlxDongjie("S-04", "冻结"),
    sfQqlxDjyq("S-05", "冻结延期"),
    sfQqlxDjjc("S-06", "冻结解除"),
    sfQqlxMxcx("S-07", "明细查询"),
    sfQqlxZtcx("S-08", "持卡主体查询"),
    sfQqlxQzhcx("S-09", "全账号查询"),
    sfQqlxDtcx("S-11", "动态查询"),
    sfQqlxDtjc("S-12", "动态解除"),
    sfQqlxLshCx("S-19", "按交易流水号查询银行卡/支付账号");

    private String key; 		// 所属类型

    private String val; 		// 类型值

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }
    private QqlxEnum(String key,String val){
        this.key=key;
        this.val=val;
    }
}
