package com.unis.common.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface ESField {
    String name() default "";

    String text() default "";

    boolean showTime() default false;
}
