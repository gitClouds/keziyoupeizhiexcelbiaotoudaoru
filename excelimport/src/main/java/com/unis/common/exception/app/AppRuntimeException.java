package com.unis.common.exception.app;

/**
 * 应用程序运行期异常(HTTP STATUS:500 etc.)
 * Created by Administrator on 2018/12/21/021.
 */
public class AppRuntimeException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public AppRuntimeException() {
        super();
    }

    public AppRuntimeException(String message) {
        super(message);
    }

    public AppRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public AppRuntimeException(Throwable cause) {
        super(cause);
    }
}
