package com.unis.common.exception.app;

/**
 * 上传附件失败异常
 * Created by Administrator on 2018/12/21/021.
 */
public class UploadFailureException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public UploadFailureException() {
        super();
    }

    public UploadFailureException(String message) {
        super(message);
    }

    public UploadFailureException(String message, Throwable cause) {
        super(message, cause);
    }

    public UploadFailureException(Throwable cause) {
        super(cause);
    }
}
