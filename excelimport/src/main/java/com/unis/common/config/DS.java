package com.unis.common.config;

import java.lang.annotation.*;

/**
 * 自定义注解
 * 动态切换数据源
 * 默认datasource1 在方法上加入@DS("datasource2")切换
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Documented
public @interface DS {
    String value() default "datasource1";
}
