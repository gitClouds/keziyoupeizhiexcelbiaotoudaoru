package com.unis.common.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * Created by pure on 2018-05-08.
 * 动态切换数据源
 */
@Order(2)
public class DynamicDataSource extends AbstractRoutingDataSource {

    private static final Logger logger = LoggerFactory.getLogger(DynamicDataSource.class);

    @Override
    protected Object determineCurrentLookupKey() {
        if(logger.isDebugEnabled()){
            logger.debug("==============切换数据源，类型："+DataSourceContextHolder.getDB()+"================");
        }
        return DataSourceContextHolder.getDB();
    }

}
