package com.unis.common.listener;

import com.unis.common.annotation.ESTable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class MyApplicationListener implements ApplicationListener<ContextRefreshedEvent> {
    public static Map<String, Class<?>> esClassCache = new HashMap<>();


    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        try {
            // 获取上下文
            ApplicationContext context = event.getApplicationContext();
            // 获取所有beanNames
            String[] beanNames = context.getBeanNamesForType(Object.class);
            for (String beanName : beanNames) {
                ESTable esClass = context.findAnnotationOnBean(beanName, ESTable.class);
                if (esClass != null) {
                    esClassCache.put(esClass.name(), context.getBean(beanName).getClass());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}