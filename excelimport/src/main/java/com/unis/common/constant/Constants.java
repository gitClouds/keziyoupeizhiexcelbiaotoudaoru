package com.unis.common.constant;

/**
 * 系统常量
 * Created by Administrator on 2018/12/28/028.
 */
public class Constants {
    /**
     * 默认的错误提示视图名
     */
    public static final String ERROR_500_PAGE = "errors/500";
    public static final String ERROR_404_PAGE = "errors/404";
    public static final String ERROR_403_PAGE = "errors/403";

    // 系统管理员标记
    public static final String SYSTEM_ADMIN_FLAG = "_SYSTEMADMIN";

    // EnCache缓存池名称
    public static final String ENCACHE_POOL = "cachePool";
    
    public static final String SY = "涉及电信诈骗案";
}
