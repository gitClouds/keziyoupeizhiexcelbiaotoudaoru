package com.unis.job;

import com.unis.common.util.TimeUtil;
import com.unis.model.jdzh.TbDpajSdryJbxx;
import com.unis.model.jdzh.TbXdryJbxx;
import com.unis.model.tjfx.Xsk;
import com.unis.model.znzf.*;
import com.unis.service.jdzh.TbDpajSdryJbxxService;
import com.unis.service.jdzh.TbXdryJbxxService;
import com.unis.service.tjfx.XskService;
import com.unis.service.znzf.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@Component
public class XskJob {
    private static final Logger logger = LoggerFactory.getLogger(XskJob.class);
    private static Integer zgxr=0;
    private static Integer sdgxr=0;
    private static Double zje = new Double("0");
    private static Double sdje = new Double("0");
    private static Short fxf=0;
    private static int jf=0;

    @Autowired
    private JltService jltService;
    @Autowired
    private XskService xskService;
    @Autowired
    private TbDpajSdryJbxxService tbDpajSdryJbxxService;
    @Autowired
    private TbXdryJbxxService tbXdryJbxxService;
    @Autowired
    private SfMxQqService sfMxQqService;
    @Autowired
    private SfMxJgLstService sfMxJgLstService;
    @Autowired
    private YhkMxQqService yhkMxQqService;
    @Autowired
    private YhkMxJgLstService yhkMxJgLstService;

    @Scheduled(cron = "0 0/30 * * * ?")
    public void xskfx(){
        logger.info("金流图线索库分析开始");
        Example example = new Example(Jlt.class);
        example.setOrderByClause("RKSJ asc");//设置排序
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("fxbs",2);
        criteria.andEqualTo("xskbs",0);
        criteria.andEqualTo("yxx",1);
        try {
            List<Jlt> jlts = jltService.queryListByExample(example);
            if (jlts.size()>0 && jlts!=null){
                for (Jlt jlt:jlts) {
                    String pk = jlt.getPk();
                    Short nlevel = jlt.getNlevel();
                    String zhpk = jlt.getJjdpk();//本级和子节点共用一个账号主键
                    List<Jlt> iterjlt = jltService.queryXskTree(pk);
                    //该金流图下每一个账号fxbs都为2才处理，有一个未完成分析就不处理
                    boolean fxbsflag = true;
                    for (Jlt zjdjlt:iterjlt) {
                        Short zjdfxbs = zjdjlt.getFxbs();
                        if (zjdfxbs!=2){
                            fxbsflag = false;
                            break;
                        }
                    }
                    boolean fininsert = false;
                    if (!fxbsflag){
                        logger.info("该条数据子节点未全部分析完成,暂不分析，pk："+pk);
                        continue;
                    }else {
                        Xsk xsk = null;
                        if (iterjlt.size()>1){//有子节点
                            logger.info("该条数据有子节点，pk："+pk+"子节点数:"+(iterjlt.size()-1));
                            zgxr = iterjlt.size()-1;
                            for (int i=0;i<iterjlt.size();i++){//分析所有子节点中有涉毒或吸毒和金额
                                String xm = iterjlt.get(i).getAccountname();
                                String sfzh = iterjlt.get(i).getCardid();
                                String zh = iterjlt.get(i).getCardnumber();
                                String pid = iterjlt.get(i).getParentpk();
                                String mxqqpk = iterjlt.get(i).getMxqqpk();
                                Short type = iterjlt.get(i).getSertype();
                                String zhjgdm = iterjlt.get(i).getBankcode();
                                String zhjgmc = iterjlt.get(i).getBankname();
                                String lrrdwmc = null;
                                String lrrdwdm = null;
                                if (i==0){
                                    xsk = new Xsk();
                                    boolean fxfflag = checkfxf(xm,sfzh);
                                    //获取lrrdw
                                    if (type==1){
                                        YhkMxQq yhkMxQq = new YhkMxQq();
                                        yhkMxQq.setPk(mxqqpk);
                                        yhkMxQq.setJjdpk(zhpk);
                                        yhkMxQq.setCardnumber(zh);
                                        yhkMxQq.setYxx((short) 1);
                                        YhkMxQq qq = yhkMxQqService.queryAsObject(yhkMxQq);
                                        if (qq!=null){
                                            lrrdwdm = qq.getLrdwdm();
                                            lrrdwmc = qq.getLrdwmc();
                                        }
                                    }else if(type==2){
                                        SfMxQq sfMxQq = new SfMxQq();
                                        sfMxQq.setPk(mxqqpk);
                                        sfMxQq.setJjdpk(zhpk);
                                        sfMxQq.setAccnumber(zh);
                                        sfMxQq.setYxx((short) 1);
                                        SfMxQq qq = sfMxQqService.queryAsObject(sfMxQq);
                                        if (qq!=null){
                                            lrrdwdm = qq.getLrdwdm();
                                            lrrdwmc = qq.getLrdwmc();
                                        }
                                    }
                                    xsk.setZh(zh);
                                    xsk.setXm(xm);
                                    xsk.setSfzh(sfzh);
                                    xsk.setRksj(new Date());
                                    xsk.setLrdwdm(lrrdwdm);
                                    xsk.setLrdwmc(lrrdwmc);
                                    xsk.setZhjgdm(zhjgdm);
                                    xsk.setZhjgmc(zhjgmc);
                                    xsk.setZhpk(zhpk);
                                    if (fxfflag){
                                        fxf=1;
                                        xsk.setFxf(fxf);
                                    }
                                    continue;
                                }
                                //获取上级的信息查询ywsqbh
                                if (i>0){
                                    String bjpk = iterjlt.get(i).getPk();
                                    boolean sdflag = checkSd(xm,sfzh);
                                    queryJe(pid,zhpk,zh,sdflag,type,bjpk);
                                }
                            }
                            xsk.setJltpk(pk);
                            xsk.setNlevel(nlevel);
                            fininsert = clfunction(xsk,0);
                        }else {
                            logger.info("该条数据无子节点，pk："+pk+"，只分析是否复吸犯");
                            xsk = new Xsk();
                            Short type = iterjlt.get(0).getSertype();
                            String xm = iterjlt.get(0).getAccountname();
                            String mxqqpk = iterjlt.get(0).getMxqqpk();
                            String sfzh = iterjlt.get(0).getCardid();
                            String zh = iterjlt.get(0).getCardnumber();
                            String zhjgdm = iterjlt.get(0).getBankcode();
                            String zhjgmc = iterjlt.get(0).getBankname();
                            boolean flag = checkfxf(xm,sfzh);
                            if (flag){
                                fxf=1;
                                jf=2;
                                xsk.setFxf(fxf);
                                xsk.setJf((short) jf);
                            }
                            String lrrdwmc = null;
                            String lrrdwdm = null;
                            //获取lrrdw
                            if (type==1){
                                YhkMxQq yhkMxQq = new YhkMxQq();
                                yhkMxQq.setPk(mxqqpk);
                                yhkMxQq.setJjdpk(zhpk);
                                yhkMxQq.setCardnumber(zh);
                                yhkMxQq.setYxx((short) 1);
                                YhkMxQq qq = yhkMxQqService.queryAsObject(yhkMxQq);
                                if (qq!=null){
                                    lrrdwdm = qq.getLrdwdm();
                                    lrrdwmc = qq.getLrdwmc();
                                }
                            }else if(type==2){
                                SfMxQq sfMxQq = new SfMxQq();
                                sfMxQq.setPk(mxqqpk);
                                sfMxQq.setJjdpk(zhpk);
                                sfMxQq.setAccnumber(zh);
                                sfMxQq.setYxx((short) 1);
                                SfMxQq qq = sfMxQqService.queryAsObject(sfMxQq);
                                if (qq!=null){
                                    lrrdwdm = qq.getLrdwdm();
                                    lrrdwmc = qq.getLrdwmc();
                                }
                            }
                            xsk.setZh(zh);
                            xsk.setXm(xm);
                            xsk.setSfzh(sfzh);
                            xsk.setRksj(new Date());
                            xsk.setLrdwdm(lrrdwdm);
                            xsk.setLrdwmc(lrrdwmc);
                            xsk.setZhjgdm(zhjgdm);
                            xsk.setZhjgmc(zhjgmc);
                            xsk.setZhpk(zhpk);
                            xsk.setJltpk(pk);
                            xsk.setNlevel(nlevel);
                            fininsert = clfunction(xsk,1);
                        }
                        zgxr=0;
                        sdgxr=0;
                        zje=new Double("0");
                        sdje=new Double("0");
                        fxf=0;
                        jf=0;
                    }
                    if (fininsert){
                        Jlt finjlt = new Jlt();
                        finjlt.setPk(pk);
                        finjlt.setXskbs((short) 1);
                        jltService.updateByPrimaryKey(finjlt);
                    }
                    logger.info("金流图pk："+pk+"分析完成");
                }
                logger.info("金流图线索库分析结束");
            }else {
                zgxr=0;
                sdgxr=0;
                zje=new Double("0");
                sdje=new Double("0");
                fxf=0;
                jf=0;
                logger.info("金流图表没有可分析数据");
            }
        }catch (Exception e){
            zgxr=0;
            sdgxr=0;
            zje=new Double("0");
            sdje=new Double("0");
            fxf=0;
            jf=0;
            logger.error("金流图线索库分析错误:"+e);
        }
    }

    public boolean clfunction(Xsk xsk,int type) throws Exception {
        boolean flag = false;
        String jltpk = xsk.getJltpk();
        logger.info("插入线索库处理开始");
        if (type==0){
            if (zgxr!=0){
                xsk.setGxr(zgxr);
                jf+=1;
            }
            if (sdgxr>0){
                xsk.setSdgxr(sdgxr);
                jf+=2;
            }
            if (zje>0){
                xsk.setZje(BigDecimal.valueOf(zje));
                jf+=1;
            }
            if (sdje>0){
                xsk.setSdje(BigDecimal.valueOf(sdje));
                jf+=2;
            }
            if (fxf!=0){
                xsk.setFxf(fxf);
                jf+=2;
            }
            xsk.setJf((short) jf);
            Xsk xsks = xskService.queryByJltpk(jltpk);
            if (xsks!=null){
                throw new Exception("线索库已经存在该条数据jltpk："+jltpk);
            }else {
                int re = xskService.insert(xsk);
                flag = re!=0;
            }
        }else if(type==1){
            Xsk xsks = xskService.queryByJltpk(jltpk);
            if (xsks!=null){
                throw new Exception("线索库已经存在该条数据jltpk："+jltpk);
            }else {
                int re = xskService.insert(xsk);
                flag = re!=0;
            }
        }
        logger.info("插入线索库处理开结束");
        return flag;
    }

    public boolean checkSd(String xm,String sfzh) throws Exception {
        logger.info("检查是否涉毒处理开始");
        boolean flag = false;
        if (sfzh==null || "".equals(sfzh)){
            logger.info("检查是否涉毒处理结束,sfzh或xm为空");
            return flag;
        }
        Example example1 = new Example(TbDpajSdryJbxx.class);
        Example.Criteria criteria1 = example1.createCriteria();
        criteria1.andEqualTo("xm",xm);
        criteria1.andEqualTo("zjhm",sfzh);
        criteria1.andEqualTo("yxx",1);
        List<TbDpajSdryJbxx> sds = tbDpajSdryJbxxService.queryListByExample(example1);
        Example example2 = new Example(TbXdryJbxx.class);
        Example.Criteria criteria2 = example2.createCriteria();
        criteria2.andEqualTo("xm",xm);
        criteria2.andEqualTo("sfzhm18",sfzh);
        criteria2.andEqualTo("yxx",1);
        List<TbXdryJbxx> xds = tbXdryJbxxService.queryListByExample(example2);
        if ((xds.size()>0 && xds!=null) || (sds!=null && sds.size()>0)){
            sdgxr++;
            flag = true;
        }
        logger.info("检查是否涉毒处理结束,结果："+flag);
        return flag;
    }

    public boolean checkfxf(String xm,String sfzh) throws Exception {
        logger.info("检查是否复吸犯开始");
        boolean flag = false;
        if (sfzh==null || "".equals(sfzh)){
            logger.info("检查是否复吸犯结束,sfzf或xm为空");
            return flag;
        }
        Example example2 = new Example(TbXdryJbxx.class);
        Example.Criteria criteria2 = example2.createCriteria();
        criteria2.andEqualTo("xm",xm);
        criteria2.andEqualTo("sfzhm18",sfzh);
        criteria2.andEqualTo("yxx",1);
        List<TbXdryJbxx> xds = tbXdryJbxxService.queryListByExample(example2);
        if (xds.size()>0 && xds!=null){
            for (TbXdryJbxx tbXdryJbxx:xds) {
                Short isfx = tbXdryJbxx.getIsfx();
                if (isfx==0){
                    flag = true;
                    break;
                }
            }
        }
        logger.info("检查是否复吸犯结束,结果："+flag);
        return flag;
    }

    public void queryJe(String pid,String zhpk,String zh,boolean flag,Short bjtype,String bjpk) throws Exception {
        if (pid==null || "".equals(pid) || zh==null || "".equals(zh)){
            throw new Exception("查询金额账号或主键为空");
        }
        logger.info("查询所有子节点明细处理开始");
        String ywsqbh;
        Jlt sjjlt = jltService.queryJltByPrimaryKey(pid);
        Short sjtype = sjjlt.getSertype();
        String sjzh = sjjlt.getCardnumber();
        String mxqqpk = sjjlt.getMxqqpk();
        if (sjtype==1){//银行卡
            YhkMxQq yhkMxQq = new YhkMxQq();
            yhkMxQq.setPk(mxqqpk);
            yhkMxQq.setJjdpk(zhpk);
            yhkMxQq.setCardnumber(sjzh);
            yhkMxQq.setYxx((short) 1);
            YhkMxQq qq = yhkMxQqService.queryAsObject(yhkMxQq);
            if (qq!=null){
                ywsqbh = qq.getApplicationid();
                List<YhkMxJgLst> list = yhkMxJgLstService.queryJe(ywsqbh,zh);
                if (list!=null&&list.size()>0){
                    for (YhkMxJgLst lst:list) {
                        Double jyje = lst.getJyje().doubleValue();
                        zje += jyje;
                        if (flag){
                            sdje += jyje;
                        }
                    }
                }
            }
        }else if (sjtype==2){//三方
            SfMxQq sfMxQq = new SfMxQq();
            sfMxQq.setPk(mxqqpk);
            sfMxQq.setJjdpk(zhpk);
            sfMxQq.setAccnumber(sjzh);
            sfMxQq.setYxx((short) 1);
            SfMxQq qq = sfMxQqService.queryAsObject(sfMxQq);
            if (qq!=null){
                ywsqbh = qq.getApplicationid();
                List<SfMxJgLst> list = sfMxJgLstService.queryJe(ywsqbh,zh,bjtype);
                if (list!=null&&list.size()>0){
                    for (SfMxJgLst lst:list) {
                        Double jyje = Double.parseDouble(lst.getJyje());
                        zje += jyje;
                        if (flag){
                            sdje += jyje;
                        }
                    }
                }
            }
        }
        logger.info("查询所有子节点明细处理结束");
    }

    @Scheduled(cron = "0 0/20 * * * ?")
    public void jltfx(){
        logger.info("金流图交易金额、时间、次数分析开始");
        Example example = new Example(Jlt.class);
        example.setOrderByClause("RKSJ asc");//设置排序
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("jltfxbs",0);
        criteria.andEqualTo("yxx",1);
        criteria.andNotEqualTo("nlevel",1);
        try {
            List<Jlt> jlts = jltService.queryListByExample(example);
            if (jlts.size()>0){
                for (Jlt jlts1:jlts) {
                	Jlt jlt = jltService.queryJltByPrimaryKey(jlts1.getParentpk());
                    String mxqqpk = jlt.getMxqqpk();
                    int lx = jlt.getSertype();
                    if (mxqqpk!=null&&!"".equals(mxqqpk)){
                    	Date firstdate = null;
                        int zzcs = 0;
                        Double jltzje = new Double(0);
                        if (lx==1){
                            YhkMxQq yhkMxQq = yhkMxQqService.queryYhkMxQqByPrimaryKey(mxqqpk);
                            Example example1 = new Example(YhkMxJgLst.class);
                            example1.setOrderByClause("jysj asc");//设置排序
                            Example.Criteria criteria1 = example1.createCriteria();
                            criteria1.andEqualTo("applicationid",yhkMxQq.getApplicationid());
                            criteria1.andEqualTo("yxx",1);
                            List<YhkMxJgLst> yhkMxJgLsts = yhkMxJgLstService.queryListByExample(example1);
                            for (YhkMxJgLst yhkMxJgLst:yhkMxJgLsts) {
                            	if(jlts1.getCardnumber().equals(yhkMxJgLst.getJydfzh())) {
                            		zzcs++;
                            		jltzje += yhkMxJgLst.getJyje().doubleValue();
                            		if(firstdate ==  null) {
                            			firstdate=yhkMxJgLst.getJysj();
                            		}
                            	}
                            }
                        }else if (lx==2){
                            SfMxQq sfMxQq = sfMxQqService.querySfMxQqByPrimaryKey(mxqqpk);
                            Example example1 = new Example(SfMxJgLst.class);
                            example1.setOrderByClause("jysj asc");//设置排序
                            Example.Criteria criteria1 = example1.createCriteria();
                            criteria1.andEqualTo("applicationid",sfMxQq.getApplicationid());
                            criteria1.andEqualTo("yxx",1);
                            List<SfMxJgLst> sfMxJgLsts = sfMxJgLstService.queryListByExample(example1);
                            for (SfMxJgLst sfMxJgLst:sfMxJgLsts) {
                            	if(sfMxJgLst.getJylx().equals("2")) {
                            		if(jlts1.getCardnumber().equals(sfMxJgLst.getSkzfzh())) {
                                		zzcs++;
                                		jltzje += Double.valueOf(sfMxJgLst.getJyje());
                                		if(firstdate ==  null) {
                                			firstdate=TimeUtil.parseStringToDate(sfMxJgLst.getJysj());
                                		}
                                	}
                            	}else if(sfMxJgLst.getJylx().equals("3")){
                            		String jydfzh="";
                            		if(("-").equals(sfMxJgLst.getFkyhzh()) && !("-").equals(sfMxJgLst.getSkyhzh())) {
                            			jydfzh=sfMxJgLst.getSkyhzh();
                            		}
                            		if(!("-").equals(sfMxJgLst.getFkyhzh()) && ("-").equals(sfMxJgLst.getSkyhzh())) {
                            			jydfzh=sfMxJgLst.getFkyhzh();
                            		}
                            		if(jlts1.getCardnumber().equals(jydfzh)) {
                                		zzcs++;
                                		jltzje += Double.valueOf(sfMxJgLst.getJyje());
                                		if(firstdate ==  null) {
                                			firstdate=TimeUtil.parseStringToDate(sfMxJgLst.getJysj());
                                		}
                                	}
                            	}
                            }
                        }
                        jlts1.setJltfxbs((short) 1);
                        jlts1.setFirstDate(firstdate);
                        jlts1.setZzcs((short) zzcs);
                        jlts1.setZje(BigDecimal.valueOf(jltzje));
                        jltService.updateByPrimaryKey(jlts1);
                        logger.info("金流图pk:"+jlts1.getPk()+"分析完成，总金额:"+jltzje+"第一次交易时间:"+firstdate+"交易次数:"+zzcs);
                    }else {
                        throw new Exception("金流图mxqqpk为空");
                    }
                }
            }else {
                logger.info("金流图交易金额、时间、次数分析符合要求为空");
            }
        }catch (Exception e){
            logger.error("金流图交易金额、时间、次数分析错误:",e);
        }
        logger.info("金流图交易金额、时间、次数分析结束");
    }
}
