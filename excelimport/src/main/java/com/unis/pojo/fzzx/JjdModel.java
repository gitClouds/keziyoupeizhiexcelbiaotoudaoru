package com.unis.pojo.fzzx;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Administrator on 2019/1/22/022.
 */
public class JjdModel implements Serializable {
    @NotNull(message = "操作类型异常")
    private String curdType;

    private String pk;
    private String jjdhm;
    private BigDecimal zfje;
    private String jdxz;
    private String sldw;
    private String sldwmc;
    private String jjsj;
    private String barxm;
    private String barsfzh;
    private String barlxdh;
    private String jjfs;
    private String ajlx;
    private String ajlb;
    private String afsj;
    private BigDecimal saje;
    private String afdd;
    private String jyaq;
    private String sawz;
    private String bz;

    private List shr;
    private List xyr;
    private List shxx;

    public String getCurdType() {
        return curdType;
    }

    public JjdModel setCurdType(String curdType) {
        this.curdType = curdType;
        return this;
    }

    public String getPk() {
        return pk;
    }

    public JjdModel setPk(String pk) {
        this.pk = pk;
        return this;
    }

    public String getJjdhm() {
        return jjdhm;
    }

    public JjdModel setJjdhm(String jjdhm) {
        this.jjdhm = jjdhm;
        return this;
    }

    public BigDecimal getZfje() {
        return zfje;
    }

    public JjdModel setZfje(BigDecimal zfje) {
        this.zfje = zfje;
        return this;
    }

    public String getJdxz() {
        return jdxz;
    }

    public JjdModel setJdxz(String jdxz) {
        this.jdxz = jdxz;
        return this;
    }

    public String getJjsj() {
        return jjsj;
    }

    public JjdModel setJjsj(String jjsj) {
        this.jjsj = jjsj;
        return this;
    }

    public String getBarxm() {
        return barxm;
    }

    public JjdModel setBarxm(String barxm) {
        this.barxm = barxm;
        return this;
    }

    public String getBarsfzh() {
        return barsfzh;
    }

    public JjdModel setBarsfzh(String barsfzh) {
        this.barsfzh = barsfzh;
        return this;
    }

    public String getBarlxdh() {
        return barlxdh;
    }

    public JjdModel setBarlxdh(String barlxdh) {
        this.barlxdh = barlxdh;
        return this;
    }

    public String getJjfs() {
        return jjfs;
    }

    public JjdModel setJjfs(String jjfs) {
        this.jjfs = jjfs;
        return this;
    }

    public String getAjlx() {
        return ajlx;
    }

    public JjdModel setAjlx(String ajlx) {
        this.ajlx = ajlx;
        return this;
    }

    public String getAjlb() {
        return ajlb;
    }

    public JjdModel setAjlb(String ajlb) {
        this.ajlb = ajlb;
        return this;
    }

    public String getAfsj() {
        return afsj;
    }

    public JjdModel setAfsj(String afsj) {
        this.afsj = afsj;
        return this;
    }

    public BigDecimal getSaje() {
        return saje;
    }

    public JjdModel setSaje(BigDecimal saje) {
        this.saje = saje;
        return this;
    }

    public String getAfdd() {
        return afdd;
    }

    public JjdModel setAfdd(String afdd) {
        this.afdd = afdd;
        return this;
    }

    public String getJyaq() {
        return jyaq;
    }

    public JjdModel setJyaq(String jyaq) {
        this.jyaq = jyaq;
        return this;
    }

    public String getSawz() {
        return sawz;
    }

    public JjdModel setSawz(String sawz) {
        this.sawz = sawz;
        return this;
    }

    public String getBz() {
        return bz;
    }

    public JjdModel setBz(String bz) {
        this.bz = bz;
        return this;
    }


    public List getShr() {
        return shr;
    }

    public JjdModel setShr(List shr) {
        this.shr = shr;
        return this;
    }

    public List getXyr() {
        return xyr;
    }

    public JjdModel setXyr(List xyr) {
        this.xyr = xyr;
        return this;
    }

    public List getShxx() {
        return shxx;
    }

    public JjdModel setShxx(List shxx) {
        this.shxx = shxx;
        return this;
    }

    public String getSldw() {
        return sldw;
    }

    public JjdModel setSldw(String sldw) {
        this.sldw = sldw;
        return this;
    }

    public String getSldwmc() {
        return sldwmc;
    }

    public JjdModel setSldwmc(String sldwmc) {
        this.sldwmc = sldwmc;
        return this;
    }

    @Override
    public String toString() {
        return "JjdModel{" +
                "curdType='" + curdType + '\'' +
                ", pk='" + pk + '\'' +
                ", jjdhm='" + jjdhm + '\'' +
                ", zfje=" + zfje +
                ", jdxz='" + jdxz + '\'' +
                ", sldw='" + sldw + '\'' +
                ", sldwmc='" + sldwmc + '\'' +
                ", jjsj=" + jjsj +
                ", barxm='" + barxm + '\'' +
                ", barsfzh='" + barsfzh + '\'' +
                ", barlxdh='" + barlxdh + '\'' +
                ", jjfs='" + jjfs + '\'' +
                ", ajlx='" + ajlx + '\'' +
                ", ajlb='" + ajlb + '\'' +
                ", afsj=" + afsj +
                ", saje=" + saje +
                ", afdd='" + afdd + '\'' +
                ", jyaq='" + jyaq + '\'' +
                ", sawz='" + sawz + '\'' +
                ", bz='" + bz + '\'' +
                ", shr=" + shr +
                ", xyr=" + xyr +
                ", shxx=" + shxx +
                '}';
    }

}
