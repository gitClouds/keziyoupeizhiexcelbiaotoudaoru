package com.unis.pojo.fzzx;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Administrator on 2019/1/22/022.
 */
public class AjxxModel implements Serializable {
    @NotNull(message = "操作类型异常")
    private String curdType;

    private String pk;
    private String jjdhm;
    private String ajbh;
    private String ajlb;
    private String ajlbzl;
    private BigDecimal saje;
    private String afsj;
    private String sldw;
    private String sldwmc;
    private String afdd;
    private String jyaq;
    private String barxm;
    private String jjsj;
    private String barsfzh;
    private String barlxdh;
    private String ladwdm;
    private String ladwmc;
    private String lasj;
    private String lxr;
    private String lxdh;

    public String getCurdType() {
        return curdType;
    }

    public void setCurdType(String curdType) {
        this.curdType = curdType;
    }

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public String getJjdhm() {
        return jjdhm;
    }

    public void setJjdhm(String jjdhm) {
        this.jjdhm = jjdhm;
    }

    public String getAjbh() {
        return ajbh;
    }

    public void setAjbh(String ajbh) {
        this.ajbh = ajbh;
    }

    public String getAjlb() {
        return ajlb;
    }

    public void setAjlb(String ajlb) {
        this.ajlb = ajlb;
    }

    public String getAjlbzl() {
        return ajlbzl;
    }

    public void setAjlbzl(String ajlbzl) {
        this.ajlbzl = ajlbzl;
    }

    public BigDecimal getSaje() {
        return saje;
    }

    public void setSaje(BigDecimal saje) {
        this.saje = saje;
    }

    public String getAfsj() {
        return afsj;
    }

    public void setAfsj(String afsj) {
        this.afsj = afsj;
    }

    public String getSldw() {
        return sldw;
    }

    public void setSldw(String sldw) {
        this.sldw = sldw;
    }

    public String getSldwmc() {
        return sldwmc;
    }

    public void setSldwmc(String sldwmc) {
        this.sldwmc = sldwmc;
    }

    public String getAfdd() {
        return afdd;
    }

    public void setAfdd(String afdd) {
        this.afdd = afdd;
    }

    public String getJyaq() {
        return jyaq;
    }

    public void setJyaq(String jyaq) {
        this.jyaq = jyaq;
    }

    public String getBarxm() {
        return barxm;
    }

    public void setBarxm(String barxm) {
        this.barxm = barxm;
    }

    public String getJjsj() {
        return jjsj;
    }

    public void setJjsj(String jjsj) {
        this.jjsj = jjsj;
    }

    public String getBarsfzh() {
        return barsfzh;
    }

    public void setBarsfzh(String barsfzh) {
        this.barsfzh = barsfzh;
    }

    public String getBarlxdh() {
        return barlxdh;
    }

    public void setBarlxdh(String barlxdh) {
        this.barlxdh = barlxdh;
    }

    public String getLadwdm() {
        return ladwdm;
    }

    public void setLadwdm(String ladwdm) {
        this.ladwdm = ladwdm;
    }

    public String getLadwmc() {
        return ladwmc;
    }

    public void setLadwmc(String ladwmc) {
        this.ladwmc = ladwmc;
    }

    public String getLasj() {
        return lasj;
    }

    public void setLasj(String lasj) {
        this.lasj = lasj;
    }

    public String getLxr() {
        return lxr;
    }

    public void setLxr(String lxr) {
        this.lxr = lxr;
    }

    public String getLxdh() {
        return lxdh;
    }

    public void setLxdh(String lxdh) {
        this.lxdh = lxdh;
    }

    @Override
    public String toString() {
        return "AjxxModel{" +
                "curdType='" + curdType + '\'' +
                ", pk='" + pk + '\'' +
                ", jjdhm='" + jjdhm + '\'' +
                ", ajbh='" + ajbh + '\'' +
                ", ajlb='" + ajlb + '\'' +
                ", ajlbzl='" + ajlbzl + '\'' +
                ", saje=" + saje +
                ", afsj='" + afsj + '\'' +
                ", sldw='" + sldw + '\'' +
                ", sldwmc='" + sldwmc + '\'' +
                ", afdd='" + afdd + '\'' +
                ", jyaq='" + jyaq + '\'' +
                ", barxm='" + barxm + '\'' +
                ", jjsj='" + jjsj + '\'' +
                ", barsfzh='" + barsfzh + '\'' +
                ", barlxdh='" + barlxdh + '\'' +
                ", ladwdm='" + ladwdm + '\'' +
                ", ladwmc='" + ladwmc + '\'' +
                ", lasj='" + lasj + '\'' +
                ", lxr='" + lxr + '\'' +
                ", lxdh='" + lxdh + '\'' +
                '}';
    }
}
