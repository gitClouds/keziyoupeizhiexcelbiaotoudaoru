package com.unis.pojo;

import com.unis.common.secure.authc.UserInfo;

/**
 * Created by Administrator on 2019/1/30/030.
 */
public class Attachment {

    private static final long serialVersionUID = 1L;
    // 主键编号
    private String pk;
    // 文件名称
    private String filename;
    // 文件类型
    private String contentType;
    // 文件MD5码
    private String md5;
    // 文件大小byte
    private long length;
    // 文件存储块大小
    private long chunkSize;
    // 文件内容
    private byte[] content;
    // 是否共享
    private String shared = "true";
    // 附件压缩比(针对图片)
    private String range;
    // 附件分辨率(针对图片)
    private int dpi;
    private int width;
    private int height;
    // 上传时间
    private String uploadTime;
    // 上传目录
    private String catalog;
    //上传库地址
    private String attachmentDb;
    // 上传人
    private UserInfo info;

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }

    public long getChunkSize() {
        return chunkSize;
    }

    public void setChunkSize(long chunkSize) {
        this.chunkSize = chunkSize;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getShared() {
        return shared;
    }

    public void setShared(String shared) {
        this.shared = shared;
    }

    public String getUploadTime() {
        return uploadTime;
    }

    public void setUploadTime(String uploadTime) {
        this.uploadTime = uploadTime;
    }
    public String getRange() {
        return range;
    }
    public void setRange(String range) {
        this.range = range;
    }

    public UserInfo getInfo() {
        return info;
    }

    public void setInfo(UserInfo info) {
        this.info = info;
    }

    public int getDpi() {
        return dpi;
    }

    public void setDpi(int dpi) {
        this.dpi = dpi;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getCatalog() {
        return catalog;
    }

    public void setCatalog(String catalog) {
        this.catalog = catalog;
    }

    public String getAttachmentDb() {
        return attachmentDb;
    }

    public void setAttachmentDb(String attachmentDb) {
        this.attachmentDb = attachmentDb;
    }
}
