package com.unis.pojo;

import com.unis.common.util.Base64Util;
import com.unis.common.util.JsonUtil;
import com.unis.common.util.TimeUtil;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * Created by Administrator on 2019/1/31/031.
 */
public class CxFlwsJtl implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 单位名称
     */
    @NotNull(message="单位名称不能为空")
    /*@Pattern(regexp = "^[\\u4e00-\\u9fa5]+$",message = "单位名称只能输入中文")*/
    private String dwmc;
    /**
     * 单位简称(锡公(刑))
     */
    @NotNull(message="单位简称不能为空")
    private String dwjc;
    /**
     * 调证字中的年份
     */
    @NotNull(message="年份不能为空")
    @Pattern(regexp = "^\\d{4}$",message = "年份必须为4位数字")
    private String dzyear;
    /**
     * 调证字号
     */
    @NotNull(message="调证字号不能为空")
    private String dzz;
    /**
     * 查询专用章
     */
    @NotNull(message="电子签章不能为空")
    private byte[] cxz;
    /**
     * 银行名称/机构名称
     */
    @NotNull(message="银行名称/机构名称不能为空")
    //@Pattern(regexp = "^[\\u4e00-\\u9fa5]+$",message = "银行名称/机构名称只能输入中文")
    private String yhjg;
    /**
     * 受害人
     */
    //@NotNull(message="受害人不能为空")
    //@Pattern(regexp = "^[\\u4e00-\\u9fa5]+$",message = "受害人只能输入中文")
    private String shr;
    /**
     * 账号类型(银行卡/微信...)
     */
    //@NotNull(message="账号类型不能为空")
    /*@Pattern(regexp = "^[\\u4e00-\\u9fa5]+$",message = "账号类型只能输入中文")*/
    private String zhlx;
    /**
     * 账号
     */
    @NotNull(message="账号不能为空")
    private String zh;

    public String getDwmc() {
        return dwmc;
    }

    public CxFlwsJtl setDwmc(String dwmc) {
        this.dwmc = dwmc;
        return this;
    }

    public String getDwjc() {
        return dwjc;
    }

    public CxFlwsJtl setDwjc(String dwjc) {
        this.dwjc = dwjc;
        return this;
    }

    public String getDzyear() {
        return dzyear;
    }

    public CxFlwsJtl setDzyear(String dzyear) {
        this.dzyear = dzyear;
        return this;
    }

    public String getDzz() {
        return dzz;
    }

    public CxFlwsJtl setDzz(String dzz) {
        this.dzz = dzz;
        return this;
    }

    public byte[] getCxz() {
        return cxz;
    }

    public CxFlwsJtl setCxz(byte[] cxz) {
        this.cxz = cxz;
        return this;
    }

    public String getYhjg() {
        return yhjg;
    }

    public CxFlwsJtl setYhjg(String yhjg) {
        this.yhjg = yhjg;
        return this;
    }

    public String getShr() {
        return shr;
    }

    public CxFlwsJtl setShr(String shr) {
        this.shr = shr;
        return this;
    }

    public String getZhlx() {
        return zhlx;
    }

    public CxFlwsJtl setZhlx(String zhlx) {
        this.zhlx = zhlx;
        return this;
    }

    public String getZh() {
        return zh;
    }

    public CxFlwsJtl setZh(String zh) {
        this.zh = zh;
        return this;
    }

    public Map cxFlwsToMap(){
        Map dataMap = null;
        dataMap= JsonUtil.objectToMap(this);
        dataMap.put("cxz", Base64Util.byteToBase64(this.getCxz()));
        dataMap.put("nowDate", TimeUtil.dataToUpper(TimeUtil.fmtDate(new Date(),"yyyy-MM-dd")));
        return dataMap;
    }
}
