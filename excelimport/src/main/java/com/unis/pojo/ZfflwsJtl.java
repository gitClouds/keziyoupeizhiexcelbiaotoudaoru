package com.unis.pojo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Administrator on 2019/1/31/031.
 */
public class ZfflwsJtl implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 单位名称
     */
    @NotNull(message="单位名称不能为空")
    /*@Pattern(regexp = "^[\\u4e00-\\u9fa5]+$",message = "单位名称只能输入中文")*/
    private String dwmc;
    /**
     * 二级单位
     */
    //@NotNull(message="二级单位不能为空")
    //@Pattern(regexp = "^[\\u4e00-\\u9fa5]+$",message = "二级单位只能输入中文")
    private String dwjc;
    /**
     * 止付专用章
     */
    @NotNull(message="电子签章不能为空")
    private byte[] zfz;
    /**
     * 账号所属银行/机构
     */
    @NotNull(message="银行名称/机构名称不能为空")
    //@Pattern(regexp = "^[\\u4e00-\\u9fa5]+$",message = "银行名称/机构名称只能输入中文")
    private String yhjg;
    /**
     * 受害人
     */
    //@NotNull(message="受害人不能为空")
    //@Pattern(regexp = "^[\\u4e00-\\u9fa5]+$",message = "受害人只能输入中文")
    private String shr;
    /**
     * 诈骗手法/案件类别
     */
    @NotNull(message="诈骗手法不能为空")
    //@Pattern(regexp = "^[\\u4e00-\\u9fa5]+$",message = "诈骗手法只能输入中文")
    private String ajlb;
    /**
     * 转账方式
     */
//    @NotNull(message="转账方式不能为空")
//    @Pattern(regexp = "^[\\u4e00-\\u9fa5]+$",message = "转账方式只能输入中文")
    private String zzfs;
    /**
     * 转账金额
     */
    @NotNull(message="转账金额不能为空")
    private Double zzje;
    /**
     * 转入骗子账号
     */
    @NotNull(message="账号不能为空")
    private String zh;
    /**
     * 转账时间
     */
    @NotNull(message="转账时间不能为空")
    private Date zzsj;

    private String zzsj_;

    public String getDwmc() {
        return dwmc;
    }

    public ZfflwsJtl setDwmc(String dwmc) {
        this.dwmc = dwmc;
        return this;
    }

    public String getDwjc() {
        return dwjc;
    }

    public ZfflwsJtl setDwjc(String dwjc) {
        this.dwjc = dwjc;
        return this;
    }

    public byte[] getZfz() {
        return zfz;
    }

    public ZfflwsJtl setZfz(byte[] zfz) {
        this.zfz = zfz;
        return this;
    }

    public String getYhjg() {
        return yhjg;
    }

    public ZfflwsJtl setYhjg(String yhjg) {
        this.yhjg = yhjg;
        return this;
    }

    public String getShr() {
        return shr;
    }

    public ZfflwsJtl setShr(String shr) {
        this.shr = shr;
        return this;
    }

    public String getAjlb() {
        return ajlb;
    }

    public ZfflwsJtl setAjlb(String ajlb) {
        this.ajlb = ajlb;
        return this;
    }

    public String getZzfs() {
        return zzfs;
    }

    public ZfflwsJtl setZzfs(String zzfs) {
        this.zzfs = zzfs;
        return this;
    }

    public Double getZzje() {
        return zzje;
    }

    public ZfflwsJtl setZzje(Double zzje) {
        this.zzje = zzje;
        return this;
    }

    public String getZh() {
        return zh;
    }

    public ZfflwsJtl setZh(String zh) {
        this.zh = zh;
        return this;
    }

    public Date getZzsj() {
        return zzsj;
    }

    public ZfflwsJtl setZzsj(Date zzsj) {
        this.zzsj = zzsj;
        return this;
    }

    public String getZzsj_() {
        return zzsj_;
    }

    public ZfflwsJtl setZzsj_(String zzsj_) {
        this.zzsj_ = zzsj_;
        return this;
    }
}
