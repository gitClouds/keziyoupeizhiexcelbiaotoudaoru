package com.unis.pojo.znzf;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2019/2/14/014.
 */
public class ReqModel implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotNull(message = "信息类型异常")
    private String xxType;
    @NotNull(message = "相关接警单异常")
//    @Size(min = 32,max = 32,message = "相关接警单编码异常")
    private String jjdPk;
    @NotNull(message = "事由不能为空")
    @Size(min = 2,max = 2000,message = "事由长度为2-2000位")
    private String sy;

    private String ajlb;

    private String shr;

    private String jjsj;

    private List<Map<String,String>> bean;

    private Short nlevel;

    private String parentpk;

    private String parentzh;

    private String iszcygzs;

    private String jltpk;

    public String getJltpk() {
        return jltpk;
    }

    public void setJltpk(String jltpk) {
        this.jltpk = jltpk;
    }

    public String getParentzh() {
        return parentzh;
    }

    public void setParentzh(String parentzh) {
        this.parentzh = parentzh;
    }

    public String getParentpk() {
        return parentpk;
    }

    public void setParentpk(String parentpk) {
        this.parentpk = parentpk;
    }

    public Short getNlevel() {
        return nlevel;
    }

    public void setNlevel(Short nlevel) {
        this.nlevel = nlevel;
    }

    public String getXxType() {
        return xxType;
    }
    public ReqModel setXxType(String xxType) {
        this.xxType = xxType;
        return this;
    }
    public String getJjdPk() {
        return jjdPk;
    }
    public ReqModel setJjdPk(String jjdPk) {
        this.jjdPk = jjdPk;
        return this;
    }
    public String getSy() {
        return sy;
    }
    public ReqModel setSy(String sy) {
        this.sy = sy;
        return this;
    }
    public List<Map<String,String>> getBean() {
        return bean;
    }
    public ReqModel setBean(List bean) {
        this.bean = bean;
        return this;
    }
    public String getAjlb() {
        return ajlb;
    }
    public ReqModel setAjlb(String ajlb) {
        this.ajlb = ajlb;
        return this;
    }

    public String getShr() {
        return shr;
    }

    public ReqModel setShr(String shr) {
        this.shr = shr;
        return this;
    }

    public String getJjsj() {
        return jjsj;
    }

    public ReqModel setJjsj(String jjsj) {
        this.jjsj = jjsj;
        return this;
    }

    public String getIszcygzs() {
        return iszcygzs;
    }

    public ReqModel setIszcygzs(String iszcygzs) {
        this.iszcygzs = iszcygzs;
        return this;
    }
}
