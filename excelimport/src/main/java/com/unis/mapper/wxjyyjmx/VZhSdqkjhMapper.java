package com.unis.mapper.wxjyyjmx;


import com.unis.model.wxjyyjmx.VZhSdqkjh;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface VZhSdqkjhMapper extends Mapper<VZhSdqkjh> {
    VZhSdqkjh queryClob(@Param("zh")String zh);
}
