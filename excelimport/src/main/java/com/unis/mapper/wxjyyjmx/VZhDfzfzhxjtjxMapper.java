package com.unis.mapper.wxjyyjmx;


import com.unis.model.wxjyyjmx.VZhDfzfzhxjtjx;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface VZhDfzfzhxjtjxMapper extends Mapper<VZhDfzfzhxjtjx> {
    VZhDfzfzhxjtjx queryClob(@Param("zh")String zh);
}
