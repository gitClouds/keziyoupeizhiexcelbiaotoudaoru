package com.unis.mapper.wxjyyjmx;


import com.unis.model.wxjyyjmx.ZhMxdrdata;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
@Repository
public interface ZhMxdrdataMapper extends Mapper<ZhMxdrdata> {
    List<String> querySjly();

     ZhMxdrdata  querySjlyAndCxzh(ZhMxdrdata zhMxdrdata);
}
