package com.unis.mapper.wxjyyjmx;


import com.unis.model.wxjyyjmx.VZhWfXjjh;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
@Repository
public interface VZhWfXjjhMapper extends Mapper<VZhWfXjjh> {
    VZhWfXjjh queryClob(@Param("zh") String zh);
}
