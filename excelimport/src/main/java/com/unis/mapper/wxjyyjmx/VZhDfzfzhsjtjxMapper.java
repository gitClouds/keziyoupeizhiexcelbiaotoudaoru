package com.unis.mapper.wxjyyjmx;


import com.unis.model.wxjyyjmx.VZhDfzfzhsjtjx;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface VZhDfzfzhsjtjxMapper extends Mapper<VZhDfzfzhsjtjx> {
    VZhDfzfzhsjtjx queryClob(@Param("zh")String zh);
}
