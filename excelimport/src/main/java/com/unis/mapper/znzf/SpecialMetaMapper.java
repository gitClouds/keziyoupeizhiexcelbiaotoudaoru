package com.unis.mapper.znzf;

import com.unis.model.znzf.SpecialMeta;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
@Repository
public interface SpecialMetaMapper extends Mapper<SpecialMeta> {
}