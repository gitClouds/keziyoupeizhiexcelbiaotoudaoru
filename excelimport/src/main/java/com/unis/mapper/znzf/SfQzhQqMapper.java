package com.unis.mapper.znzf;

import com.unis.model.znzf.SfQzhQq;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface SfQzhQqMapper extends Mapper<SfQzhQq> {
    int insertBatch(@Param("list")List list);
    @Update("UPDATE TB_ZNZF_SF_QZH_QQ SET NLEVEL = NLEVEL+ #{newNlevel} - #{thisNlevel} WHERE JJDPK=#{jjdpk} AND ACCNUMBER=#{accnumber} AND NLEVEL=#{nlevel} ")
    int updateNlevel(@Param("jjdpk")String jjdpk,@Param("accnumber")String accnumber,@Param("nlevel")int nlevel,@Param("newNlevel")int newNlevel,@Param("thisNlevel")int thisNlevel);

}