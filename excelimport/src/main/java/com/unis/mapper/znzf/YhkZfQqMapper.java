package com.unis.mapper.znzf;

import com.unis.model.znzf.YhkZfQq;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Repository
public interface YhkZfQqMapper extends Mapper<YhkZfQq> {

    int insertBatch(@Param("list")List list);

    List<Map<String,BigDecimal>> indexStatistics(@Param("lrdwdm") String lrdwdm);
}