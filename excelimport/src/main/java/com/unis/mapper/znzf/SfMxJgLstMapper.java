package com.unis.mapper.znzf;

import com.unis.model.znzf.SfMxJgLst;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

@Repository
public interface SfMxJgLstMapper extends Mapper<SfMxJgLst> {
    @Select("SELECT DISTINCT applicationid FROM TB_ZNZF_SF_MX_JG_LIST D WHERE qqpk =#{qqpk} AND NOT EXISTS (SELECT 1 FROM TB_ZNZF_ANALYSIS_QUEUE Q WHERE Q.qqpk=D.qqpk AND D.applicationid=Q.applicationid AND FXBS=1) ")
    List<String> queryApplicationByQqpk(@Param("qqpk")String qqpk);

    List<Map<String,String>> queryMxListByJjd(@Param("jjdpk") String jjdpk,@Param("zh") String zh, @Param("nlevel")int nlevel);

    List<SfMxJgLst> queryJe(@Param("ywsqbh")String ywsqbh,@Param("zh")String zh,@Param("type")Short type);
}