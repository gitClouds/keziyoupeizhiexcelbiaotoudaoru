package com.unis.mapper.znzf;

import com.unis.model.znzf.ZnzfRemind;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
@Repository
public interface ZnzfRemindMapper extends Mapper<ZnzfRemind> {
}