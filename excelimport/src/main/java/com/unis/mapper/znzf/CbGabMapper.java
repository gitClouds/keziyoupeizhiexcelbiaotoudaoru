package com.unis.mapper.znzf;

import com.unis.mapper.fzzx.JjdMapper;
import com.unis.model.znzf.CbGab;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

@Repository
public interface CbGabMapper extends Mapper<CbGab> {
    @Select("SELECT SUM(A.COUNT) COUNT,A.CARDNUMBER CARDNUMBER,(SELECT MAX(CARDNAME) FROM TB_ZNZF_CB_GAB B WHERE B.CARDNUMBER=A.CARDNUMBER AND B.JJDPK=#{jjdpk}) CARDNAME FROM TB_ZNZF_CB_GAB A WHERE A.JJDPK=#{jjdpk} GROUP BY A.CARDNUMBER ORDER BY SUM(A.COUNT) DESC ")
    List<CbGab> queryCbGabByJjdpk(@Param("jjdpk")String jjdpk );
}