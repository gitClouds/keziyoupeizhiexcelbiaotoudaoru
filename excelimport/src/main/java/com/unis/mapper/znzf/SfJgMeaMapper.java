package com.unis.mapper.znzf;

import com.unis.model.znzf.SfJgMea;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface SfJgMeaMapper extends Mapper<SfJgMea> {
}