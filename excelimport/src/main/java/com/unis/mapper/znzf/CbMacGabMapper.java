package com.unis.mapper.znzf;

import com.unis.model.znzf.CbMacGab;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface CbMacGabMapper extends Mapper<CbMacGab> {
    @Select("SELECT SUM(A.COUNT) COUNT,A.QQZH QQZH,(SELECT MAX(QQZHXM) FROM TB_ZNZF_CB_MAC_GAB B WHERE B.QQZH=A.QQZH AND B.JJDPK=#{jjdpk}) QQZHXM, to_char(WMSYS.WM_CONCAT (MACDZ)) MACDZ FROM TB_ZNZF_CB_MAC_GAB A WHERE A.JJDPK=#{jjdpk} GROUP BY A.QQZH ORDER BY SUM(A.COUNT) DESC ")
    List<CbMacGab> queryCbMacGabByJjdpk(@Param("jjdpk") String jjdpk);
}