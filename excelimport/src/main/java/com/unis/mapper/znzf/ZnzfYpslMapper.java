package com.unis.mapper.znzf;

import com.unis.model.znzf.ZnzfYpsl;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
@Repository
public interface ZnzfYpslMapper extends Mapper<ZnzfYpsl> {
}