package com.unis.mapper.znzf;

import com.unis.model.znzf.CbMacGabJjd;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
@Repository
public interface CbMacGabJjdMapper extends Mapper<CbMacGabJjd> {
    @Select("select count(1) from TB_ZNZF_CB_MAC_GAB_JJD where jjdpk=#{jjdpk}")
    int selectCountbByJjdpk(@Param("jjdpk")String jjdpk);
    @Delete("delete from TB_ZNZF_CB_MAC_GAB_JJD where jjdpk=#{jjdpk}" )
    int deleteByJjdpk(@Param("jjdpk")String jjdpk);
}