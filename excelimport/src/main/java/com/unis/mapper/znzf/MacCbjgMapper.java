package com.unis.mapper.znzf;

import com.unis.model.znzf.MacCbjg;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
@Repository
public interface MacCbjgMapper extends Mapper<MacCbjg> {
}