package com.unis.mapper.znzf;

import com.unis.model.znzf.ZnzfAnalysisLog;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
@Repository
public interface ZnzfAnalysisLogMapper extends Mapper<ZnzfAnalysisLog> {
}