package com.unis.mapper.znzf;

import com.unis.model.znzf.ZnzfRemindMeta;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
@Repository
public interface ZnzfRemindMetaMapper extends Mapper<ZnzfRemindMeta> {
}