package com.unis.mapper.znzf;

import com.unis.model.znzf.AnalysisQueue;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface AnalysisQueueMapper extends Mapper<AnalysisQueue> {
    int insertBatch(@Param("list") List<AnalysisQueue> list);
}