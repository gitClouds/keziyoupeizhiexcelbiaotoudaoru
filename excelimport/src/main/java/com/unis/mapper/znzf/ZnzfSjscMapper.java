package com.unis.mapper.znzf;

import com.unis.model.znzf.ZnzfSjsc;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface ZnzfSjscMapper extends Mapper<ZnzfSjsc> {
}