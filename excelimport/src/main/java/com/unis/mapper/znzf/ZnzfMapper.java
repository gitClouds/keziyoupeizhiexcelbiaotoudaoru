package com.unis.mapper.znzf;

import com.unis.common.enums.QqlxEnum;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2019/3/25/025.
 */
@Repository
public interface ZnzfMapper {
    @SelectProvider(type = ZjckProvider.class,method = "queryListByJjdpk")
    List<Map<String,String>> queryListByJjdpk(@Param("jjdpk")String jjdpk,@Param("cType") String cType);

    class ZjckProvider{
        public String queryListByJjdpk(Map<String,Object> map){
            if (map!=null && !map.isEmpty() && map.size()>0){
                StringBuilder sb = new StringBuilder();
                String jjdpk =  map.get("jjdpk")==null?null: (String) map.get("jjdpk");
                String cType =  map.get("cType")==null?null: (String) map.get("cType");
                if (StringUtils.isNotBlank(jjdpk) && StringUtils.isNotBlank(cType)){
                    if (StringUtils.indexOfIgnoreCase(cType, QqlxEnum.yhkQqlxZhifu.getKey()) >= 0){
                        sb.append(" UNION ALL SELECT pk,jjdpk,applicationid,bankcode,bankname,accountname,cardnumber,lrdwmc,reqdate,resflag,(select MC from t_sys_code where zddm='010301' and dm=resultcode) resultcode,'止付' qqlx,(SELECT TO_CHAR(ACCOUNTBALANCE) FROM TB_ZNZF_YHK_ZF_JG J WHERE J.QQPK=Q.PK AND ROWNUM=1) YE FROM TB_ZNZF_YHK_ZF_QQ Q WHERE YXX=1 AND JJDPK = '").append(jjdpk).append("' ");
                    }
                    if (StringUtils.indexOfIgnoreCase(cType, QqlxEnum.sfQqlxZhifu.getKey()) >= 0){
                        sb.append(" UNION ALL SELECT pk,jjdpk,applicationid,paycode bankcode,payname bankname,'' accountname,accnumber cardnumber,lrdwmc,reqdate,resflag,(select MC from t_sys_code where zddm='010301' and dm=resultcode) resultcode,'止付' qqlx,(SELECT TO_CHAR(ACCOUNTBALANCE) FROM TB_ZNZF_SF_ZF_JG J WHERE J.QQPK=Q.PK AND ROWNUM=1) YE FROM TB_ZNZF_SF_ZF_QQ Q WHERE YXX=1 AND JJDPK = '").append(jjdpk).append("' ");
                    }

                    if (StringUtils.indexOfIgnoreCase(cType, QqlxEnum.yhkQqlxZtcx.getKey()) >= 0){
                        sb.append(" UNION ALL SELECT pk,jjdpk,applicationid,bankcode,bankname,accountname,cardnumber,lrdwmc,reqdate,resflag,(select MC from t_sys_code where zddm='010301' and dm=resultcode) resultcode,'主体查询' qqlx,'' YE FROM TB_ZNZF_YHK_ZT_QQ WHERE YXX=1 AND JJDPK = '").append(jjdpk).append("' ");
                    }
                    if (StringUtils.indexOfIgnoreCase(cType, QqlxEnum.yhkQqlxMxcx.getKey()) >= 0){
                        sb.append(" UNION ALL SELECT pk,jjdpk,applicationid,bankcode,bankname,accountname,cardnumber,lrdwmc,reqdate,resflag,(select MC from t_sys_code where zddm='010301' and dm=resultcode) resultcode,'明细查询' qqlx,(SELECT TO_CHAR(ACCBALANCE) FROM TB_ZNZF_YHK_MX_JG J WHERE J.QQPK=Q.PK AND ROWNUM=1) YE FROM TB_ZNZF_YHK_MX_QQ Q WHERE YXX=1 AND JJDPK = '").append(jjdpk).append("' ");
                    }

                    if (StringUtils.indexOfIgnoreCase(cType, QqlxEnum.sfQqlxZtcx.getKey()) >= 0){
                        sb.append(" UNION ALL SELECT pk,jjdpk,applicationid,paycode bankcode,payname bankname,'' accountname,accnumber cardnumber,lrdwmc,reqdate,resflag,(select MC from t_sys_code where zddm='010301' and dm=resultcode) resultcode,'主体查询' qqlx,'' YE FROM TB_ZNZF_SF_ZT_QQ WHERE YXX=1 AND JJDPK = '").append(jjdpk).append("' ");
                    }
                    if (StringUtils.indexOfIgnoreCase(cType, QqlxEnum.sfQqlxMxcx.getKey()) >= 0){
                        sb.append(" UNION ALL SELECT pk,jjdpk,applicationid,paycode bankcode,payname bankname,'' accountname,accnumber cardnumber,lrdwmc,reqdate,resflag,(select MC from t_sys_code where zddm='010301' and dm=resultcode) resultcode,'明细查询' qqlx,(SELECT TO_CHAR(ACCOUNTBALANCE) FROM TB_ZNZF_SF_MX_JG J WHERE J.QQPK=Q.PK AND ROWNUM=1) YE FROM TB_ZNZF_SF_MX_QQ Q WHERE YXX=1 AND JJDPK = '").append(jjdpk).append("' ");
                    }
                    if (StringUtils.indexOfIgnoreCase(cType, QqlxEnum.sfQqlxLshCx.getKey()) >= 0){
                        sb.append(" UNION ALL SELECT pk,jjdpk,applicationid,paycode bankcode,payname bankname,'' accountname,accnumber cardnumber,lrdwmc,reqdate,resflag,(select MC from t_sys_code where zddm='010301' and dm=resultcode) resultcode,'流水号查询' qqlx,'' YE FROM TB_ZNZF_SF_LS_QQ WHERE YXX=1 AND JJDPK = '").append(jjdpk).append("' ");
                    }
                    if (StringUtils.indexOfIgnoreCase(cType, QqlxEnum.sfQqlxQzhcx.getKey()) >= 0){
                        sb.append(" UNION ALL SELECT pk,jjdpk,applicationid,paycode bankcode,payname bankname,accountname,accnumber cardnumber,lrdwmc,reqdate,resflag,(select MC from t_sys_code where zddm='010301' and dm=resultcode) resultcode,'全账号查询' qqlx,'' YE FROM TB_ZNZF_SF_QZH_QQ WHERE YXX=1 AND JJDPK = '").append(jjdpk).append("' ");
                    }
                }
                if (sb!=null && sb.length()>0){
                    return StringUtils.replace(sb.toString(),"UNION ALL","",1)+" order by reqdate desc,pk";
                }
            }
            return "";
        }
    }



}
