package com.unis.mapper.znzf;

import com.unis.model.znzf.SfZtJg;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
@Repository
public interface SfZtJgMapper extends Mapper<SfZtJg> {
	@Delete("delete Tb_Znzf_Sf_Zt_Jg where applicationid = #{applicationid} ")
	void deleteByApplicationid(@Param("applicationid")String applicationid);
}