package com.unis.mapper.znzf;

import com.unis.model.znzf.SfJgBind;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
@Repository
public interface SfJgBindMapper extends Mapper<SfJgBind> {
}