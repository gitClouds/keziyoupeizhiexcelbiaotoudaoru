package com.unis.mapper.znzf;

import com.unis.model.znzf.CbMacGl;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public interface CbMacGlMapper extends Mapper<CbMacGl> {
    @Select(" select cardnumber,bankname,accountname,nlevel,iszcygzs,isshow from tb_znzf_jlt where jjdpk=#{jjdpk} and cardnumber in(select qqzh from tb_znzf_cb_mac_gl where jjdpk=#{jjdpk} and macdz=#{macdz}) ")
    List<Map> queryQqzhByJjdpkAndMacdz(@Param("jjdpk")String jjdpk,@Param("macdz")String macdz);
    @SelectProvider(type = CbMacGlProvider.class,method = "queryListByMap")
    List<Map> queryListByMap(@Param("parmMap")Map parmMap);

    class CbMacGlProvider{

        public String queryListByMap(Map<String,Object> map){
            Map parmMap = map!=null && map.get("parmMap")!=null ? (Map)map.get("parmMap"):new HashMap();
            StringBuilder sb  = new StringBuilder(" with cb_group as(select distinct a.cbbh,a.pk,a.qqpk,a.jjdpk,a.macdz,a.jjdhm,a.barxm,a.sldwmc,a.afsj,a.qqzh,a.qqzhxm,a.qqzhjgmc from tb_znzf_cb_mac_gl a,");

            if(parmMap.get("jjdpk")!=null && StringUtils.isNotBlank(parmMap.get("jjdpk").toString())){
                sb.append(" (select distinct cbbh,qqzh from tb_znzf_cb_mac_gl where jjdpk='").append(parmMap.get("jjdpk").toString()).append("') b ")
                  .append(" where a.cbbh=b.cbbh and a.qqzh<>b.qqzh and a.jjdpk<>'").append(parmMap.get("jjdpk").toString()).append("') ");
            }
            //sb.append(" select a.jjdpk jjdpk,a.jjdhm jjdhm,a.barxm barxm,a.sldwmc sldwmc,a.afsj afsj,to_char(b.qqpk) qqpk,to_char(b.cbyj) cbyj from cb_group a, ")
            sb.append(" select a.jjdpk jjdpk,a.jjdhm jjdhm,a.barxm barxm,a.sldwmc sldwmc,a.afsj afsj,b.qqpk qqpk,b.cbyj cbyj from cb_group a, ")
            .append(" (select max(pk) pk,jjdpk,wmsys.wm_concat (pk) qqpk,wmsys.wm_concat (macdz) cbyj from cb_group group by jjdpk) b where a.pk=b.pk ");

            /*Subject currentUser = SecurityUtils.getSubject();
            if (!currentUser.hasRole("_SYSTEMADMIN")&&!currentUser.hasRole("ADMIN")){
                UserInfo userInfo = (UserInfo) currentUser.getPrincipal();
                //criteria.andLike("lrdwdm", StrUtil.removeDmZeor(userInfo.getJgdm())+"%");
                sb.append(" AND lrdwdm like '").append(StrUtil.removeDmZeor(userInfo.getJgdm())).append("%' ");
            }*/

            sb.append(" order by a.afsj desc ");
            return sb.toString();
        }
    }
}