package com.unis.mapper.znzf;

import com.unis.model.znzf.Jlt;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

@Repository
public interface JltMapper extends Mapper<Jlt> {
    @Select("SELECT NVL(MAX(NLEVEL),-1) FROM TB_ZNZF_JLT WHERE JJDPK=#{jjdPk} AND ISSHOW = 1")
    int getZcxxJltLevel(@Param("jjdPk")String jjdPk);
    @Select("SELECT NVL(MAX(ISZCYGZS),-1) FROM TB_ZNZF_JLT WHERE JJDPK=#{jjdPk} AND ISZCYGZS >= 1")
    int getZcygzsJltLevel(@Param("jjdPk")String jjdPk);
    @Update("UPDATE TB_ZNZF_JLT SET DISAB=abs(DISAB-1) WHERE PK=#{pk}")
    int updateDisabled(@Param("pk")String pk);
    @Update("UPDATE TB_ZNZF_JLT SET ISSHOW=1,NLEVEL = NLEVEL+ #{nlevel} - #{thisNlevel} WHERE PK IN (SELECT PK from TB_ZNZF_JLT Start With PK = #{pk} Connect By nocycle Prior PK = PARENTPK) ")
    int updateZcygzsJltLevel(@Param("pk")String pk,@Param("nlevel")int nlevel,@Param("thisNlevel") int thisNlevel);
    @Select("SELECT PK,JJDPK,SERTYPE,CARDNUMBER,NLEVEL FROM TB_ZNZF_JLT Start With PK = #{pk} Connect By nocycle Prior PK = PARENTPK ")
    List<Jlt> queryChildJltByPk(@Param("pk")String pk);

    List<Map> selectJltTreeByJjd(@Param("jjdpk")String jjdpk);
    List<Jlt> queryXskTree(@Param("pk")String pk);
    List<Jlt> queryTreeFirstLevel(@Param("pk")String pk);
    List<Jlt> queryXskTreeCToP(@Param("pk") String pk);
    List<Jlt> queryXskTreeAll(Jlt jlt);
    List<Map<String, Object>> querySfMxJgMap(Map<String, Object> map) throws Exception;
    List<Map<String, Object>> queryYhkMxJgMap(Map<String, Object> map) throws Exception;
}