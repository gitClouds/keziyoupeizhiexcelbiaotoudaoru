package com.unis.mapper.znzf;

import com.unis.model.znzf.SfZtQq;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface SfZtQqMapper extends Mapper<SfZtQq> {
    int insertBatch(@Param("list")List list);
    //明细和主体请求，调用的主体批量插入
    int insertBatchZt(@Param("list")List list);
    @Update("UPDATE TB_ZNZF_SF_MX_QQ SET NLEVEL = NLEVEL+ #{newNlevel} - #{thisNlevel} WHERE JJDPK=#{jjdpk} AND ACCNUMBER=#{accnumber} AND NLEVEL=#{nlevel} ")
    int updateNlevel(@Param("jjdpk")String jjdpk,@Param("accnumber")String accnumber,@Param("nlevel")int nlevel,@Param("newNlevel")int newNlevel,@Param("thisNlevel")int thisNlevel);
	@Update("update TB_ZNZF_SF_ZT_QQ set applicationid=null,resflag=0,fxbs=1,reqflag=0,reqdate=null,reqcount=0,resdate=null,resultcode=null,feedbackremark=null where pk=#{pk}")
    void updateBypk(@Param("pk")String pk);

}