package com.unis.mapper.znzf;

import com.unis.model.znzf.YhkMxQq;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Repository
public interface YhkMxQqMapper extends Mapper<YhkMxQq> {

    int insertBatch(@Param("list")List list);
    @Update("UPDATE TB_ZNZF_YHK_MX_QQ SET NLEVEL = NLEVEL+ #{newNlevel} - #{thisNlevel} WHERE JJDPK=#{jjdpk} AND CARDNUMBER=#{cardnumber} AND NLEVEL=#{nlevel} ")
    int updateNlevel(@Param("jjdpk")String jjdpk,@Param("cardnumber")String cardnumber,@Param("nlevel")int nlevel,@Param("newNlevel")int newNlevel,@Param("thisNlevel")int thisNlevel);

    List<Map<String,BigDecimal>> indexStatistics(@Param("lrdwdm") String lrdwdm);
}