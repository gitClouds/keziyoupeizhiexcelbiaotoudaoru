package com.unis.mapper.znzf;

import com.unis.model.znzf.TbDxzpYhkCompanyZh;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
@Repository
public interface TbDxzpYhkCompanyZhMapper extends Mapper<TbDxzpYhkCompanyZh> {
}