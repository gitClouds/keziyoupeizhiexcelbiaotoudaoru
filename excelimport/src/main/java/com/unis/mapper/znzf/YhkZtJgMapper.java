package com.unis.mapper.znzf;

import com.unis.model.znzf.YhkZtJg;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
@Repository
public interface YhkZtJgMapper extends Mapper<YhkZtJg> {
	@Delete("delete Tb_Znzf_YHK_Zt_Jg where applicationid = #{applicationid} ")
	void deleteByApplicationid(@Param("applicationid")String applicationid);
}