package com.unis.mapper.znzf;

import com.unis.model.znzf.MacBmd;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
@Repository
public interface MacBmdMapper extends Mapper<MacBmd> {
}