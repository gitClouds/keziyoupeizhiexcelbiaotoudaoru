package com.unis.mapper.znzf;

import com.unis.model.znzf.ZnzfQueue;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface ZnzfQueueMapper extends Mapper<ZnzfQueue> {
    int insertBatch(@Param("list")List list);
}