package com.unis.mapper.znzf;

import com.unis.model.znzf.SfLshJg;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface SfLshJgMapper extends Mapper<SfLshJg> {
}