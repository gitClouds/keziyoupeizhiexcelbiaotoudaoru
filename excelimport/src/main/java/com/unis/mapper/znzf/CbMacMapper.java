package com.unis.mapper.znzf;

import com.unis.model.znzf.CbMac;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
@Repository
public interface CbMacMapper extends Mapper<CbMac> {
}