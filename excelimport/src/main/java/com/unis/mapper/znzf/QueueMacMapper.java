package com.unis.mapper.znzf;

import com.unis.model.znzf.QueueMac;
import tk.mybatis.mapper.common.Mapper;

public interface QueueMacMapper extends Mapper<QueueMac> {
}