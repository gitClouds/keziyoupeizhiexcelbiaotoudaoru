package com.unis.mapper.zhxx;

import com.unis.model.fzzx.Ajxx;
import com.unis.model.zhxx.TbJdzjfxSfZh;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface TbJdzjfxSfZhMapper extends Mapper<TbJdzjfxSfZh> {
}