package com.unis.mapper.analysis;


import com.unis.model.analysis.YhkMxJgList;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
@Repository
public interface YhkMxJgListMapper extends Mapper<YhkMxJgList> {

	void updateByPks(@Param("pkAndZtpkListYhk")List<Map<String, Object>> pkAndZtpkListYhk);
	
	@Update("UPDATE TB_ZNZF_YHK_MX_JG_LIST SET DISAB=abs(DISAB-1) WHERE PK=#{pk}")
	void updateDisabled(String pk);

	void updateByIsNotXdSdPks(@Param("isNotXdSdYhk")List<String> isNotXdSdYhk);
}
