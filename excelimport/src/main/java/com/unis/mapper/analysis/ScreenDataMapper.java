package com.unis.mapper.analysis;


import com.unis.model.analysis.ScreenData;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
@Repository
public interface ScreenDataMapper extends Mapper<ScreenData> {


	void insertBatch(@Param("screenDataList")List<Map<String, Object>> screenDataList);


    List<Map> querySfSdListByExample(Map map) throws Exception;
    List<Map> querySfXdListByExample(Map map) throws Exception;
    List<Map> queryYhkSdListByExample(Map map) throws Exception;
    List<Map> queryYhkXdListByExample(Map map) throws Exception;
    List<Map> querySfRyAccountNumber(Map map) throws Exception;
    List<Map> queryYhkRyAccountNumber(Map map) throws Exception;
}
