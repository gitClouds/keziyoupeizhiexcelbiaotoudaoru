package com.unis.mapper.analysis;


import com.unis.model.analysis.ManulAnalysis;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
@Repository
public interface ManulAnalysisMapper extends Mapper<ManulAnalysis> {
}
