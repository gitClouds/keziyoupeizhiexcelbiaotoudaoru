package com.unis.mapper.analysis;


import com.unis.model.analysis.SfMxJgList;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
@Repository
public interface SfMxJgListMapper extends Mapper<SfMxJgList> {

	void updateByPks(@Param("pkAndZtpkListSf")List<Map<String, Object>> pkAndZtpkListSf);
	
	@Update("UPDATE TB_ZNZF_SF_MX_JG_LIST SET DISAB=abs(DISAB-1) WHERE PK=#{pk}")
	void updateDisabled(String pk);

	void updateByIsNotXdSdPks(@Param("isNotXdSdSf")List<String> isNotXdSdSf);
}
