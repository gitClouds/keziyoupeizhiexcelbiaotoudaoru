package com.unis.mapper.tjfx;

import com.unis.model.tjfx.Xsk;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

@Repository
public interface XskMapper extends Mapper<Xsk> {
    Xsk queryByJltpk(@Param("jltpk")String jltpk);
    List<Xsk> queryjoin(Map<Object, Object> parmMap);
}
