package com.unis.mapper.tjfx;


import com.unis.model.tjfx.TbZhQkss;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
public interface TbZhQkssMapper{
    List<TbZhQkss> queryqk(Map<Object, Object> parmMap);
}
