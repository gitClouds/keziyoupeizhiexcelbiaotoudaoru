package com.unis.mapper.jdzh;

import com.unis.model.jdzh.TbDpajSdryJbxx;
import com.unis.model.jdzh.TbXdryJbxx;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

@org.apache.ibatis.annotations.Mapper
@Repository
public interface TbXdryJbxxMapper extends Mapper<TbXdryJbxx> {
    List<TbDpajSdryJbxx> queryXdry(Map<Object, Object> parmMap);
}
