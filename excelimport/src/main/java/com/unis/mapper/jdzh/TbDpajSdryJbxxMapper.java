package com.unis.mapper.jdzh;

import com.unis.model.jdzh.TbDpajSdryJbxx;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

@org.apache.ibatis.annotations.Mapper
@Repository
public interface TbDpajSdryJbxxMapper extends Mapper<TbDpajSdryJbxx> {
    List<TbDpajSdryJbxx> querySdry(Map<Object, Object> parmMap);
    TbDpajSdryJbxx queryByPk(@Param("pk")String pk);//除去lx字段
}
