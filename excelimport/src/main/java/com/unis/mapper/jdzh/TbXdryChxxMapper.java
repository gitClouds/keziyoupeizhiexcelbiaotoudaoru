package com.unis.mapper.jdzh;


import com.unis.model.jdzh.TbXdryChxx;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
@Repository
public interface TbXdryChxxMapper extends Mapper<TbXdryChxx> {
}
