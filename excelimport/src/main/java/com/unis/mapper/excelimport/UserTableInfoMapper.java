package com.unis.mapper.excelimport;


import com.unis.model.excelimport.UserTableInfo;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

@org.apache.ibatis.annotations.Mapper
@Repository
public interface UserTableInfoMapper extends Mapper<UserTableInfo> {
    List<Map<String, String>> queryTableCloAndComents(UserTableInfo userTableInfo) throws Exception;
    List<Map<String, String>> queryTableCloAndComentOrder(UserTableInfo userTableInfo) throws Exception;
    List<Map<String, Object>> queryTableDatas(Map<String, Object> map) throws Exception;
    List<UserTableInfo> queryPrimaryKey(UserTableInfo userTableInfo) throws Exception;
    List<Map<String, Object>> queryTableSpaceName(Map<String, Object> map) throws Exception;
    Map<String, Integer>queryTableDatasMax(Map<String, String> map) throws Exception;
    List<Map<String, Object>> queryTableDatasByMap(Map<String, Object> map) throws Exception;
    Integer queryTableDatasTotalCount(Map<String, Object> map) throws Exception;
    int insertExcelToData(Map<String, Object> map) throws Exception;
    int updateDataToTable(Map<String, Object> map) throws Exception;
    int deleteDataFromTable(Map<String, Object> map) throws Exception;
    int excuteSql(Map<String, Object> map) throws Exception;
    List<Map<String, Object>> excuteSqlReturnMap(Map<String, Object> map) throws Exception;
    List<Map<String, Object>> queryAllSqlStatement(Map<String, Object> map) throws Exception;
    List<Map<String, Object>> queryCreateTableInfo(Map<String, String> map) throws Exception;
    List<Map<String, Object>> querySqlTaskBatch(Map<String, String> map) throws Exception;
    List<Map<String, Object>> queryValidTaskData(Map<String, String> map) throws Exception;
    //查询存储过程的方法
    void callProcedure(Map map) throws Exception;
    //查询函数的方法
    void callFunction(Map map) throws Exception;
    int insertAllSqlStatement(Map<String, Object> map) throws Exception;
    int updateSqlTaskSchedule(Map<String, Object> map) throws Exception;
    List<Map<String, String>> querySqlBatchInfoList(Map<String, String> map) throws Exception;
}
