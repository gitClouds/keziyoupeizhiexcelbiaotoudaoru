package com.unis.mapper.fzzx;

import com.unis.model.fzzx.JjdXyr;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
@Repository
public interface JjdXyrMapper extends Mapper<JjdXyr> {
    @Delete("DELETE FROM TB_FZZX_JJD_XYR WHERE JJDPK=#{jjdPk}")
    int deleteByJjd(@Param("jjdPk")String jjdPk);
}