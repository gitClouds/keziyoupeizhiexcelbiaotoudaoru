package com.unis.mapper.fzzx;

import com.unis.common.secure.authc.UserInfo;
import com.unis.common.util.StrUtil;
import com.unis.common.util.TimeUtil;
import com.unis.model.fzzx.Jjd;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.jdbc.SQL;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public interface JjdMapper extends Mapper<Jjd> {
    @Select(" insert into tb_znzf_queue_mac\n" +
            " select sys_guid(),'3',b.applicationid,b.qqpk,a.jjdpk,a.cardnumber,a.accountname,a.bankcode,a.bankname,a.rksj,'',a.nlevel,a.lrdwdm,a.lrdwmc from tb_znzf_yhk_mx_qq a,\n" +
            " (select qqpk,applicationid from tb_znzf_yhk_mx_jg_list where jjdpk=#{jjdpk} group by qqpk,applicationid) b\n" +
            " where a.pk=b.qqpk")
    void insertQueueMac(@Param("jjdpk")String jjdpk);
    @SelectProvider(type = JjdProvider.class,method = "queryListByMap")
    List<Map> queryListByMap(@Param("parmMap")Map parmMap);

    class JjdProvider{

        public String queryListByMap(Map<String,Object> map){
            Map parmMap = map!=null && map.get("parmMap")!=null ? (Map)map.get("parmMap"):new HashMap();
            StringBuilder sb  = new StringBuilder("SELECT t.*,(SELECT max(nlevel) from TB_ZNZF_JLT where yxx=1 and jjdpk = t.pk) nlevel FROM TB_FZZX_JJD T WHERE 1=1 ");

            Subject currentUser = SecurityUtils.getSubject();
            if (!currentUser.hasRole("_SYSTEMADMIN")&&!currentUser.hasRole("ADMIN")){
                UserInfo userInfo = (UserInfo) currentUser.getPrincipal();
                //criteria.andLike("lrdwdm", StrUtil.removeDmZeor(userInfo.getJgdm())+"%");
                sb.append(" AND lrdwdm like '").append(StrUtil.removeDmZeor(userInfo.getJgdm())).append("%' ");
            }

            if (parmMap.get("userpk")!=null && StringUtils.isNotBlank(parmMap.get("userpk").toString())){
                sb.append(" AND EXISTS (SELECT 1 FROM TB_FZZX_USER_FOCUS F WHERE F.jjdpk=t.pk AND F.userpk='").append(parmMap.get("userpk").toString()).append("') ");
            }

            if(parmMap.get("pk")!=null && StringUtils.isNotBlank(parmMap.get("pk").toString())){
                sb.append(" AND pk='").append(parmMap.get("pk").toString()).append("' ");
            }
            if(parmMap.get("barxm")!=null && StringUtils.isNotBlank(parmMap.get("barxm").toString())){
                sb.append(" AND barxm like '%").append(parmMap.get("barxm").toString()).append("%' ");
            }
            if(parmMap.get("jjdhm")!=null && StringUtils.isNotBlank(parmMap.get("jjdhm").toString())){
                sb.append(" AND jjdhm='").append(parmMap.get("jjdhm").toString()).append("' ");
            }
            if(parmMap.get("ajbh")!=null && StringUtils.isNotBlank(parmMap.get("ajbh").toString())){
                sb.append(" AND ajbh='").append(parmMap.get("ajbh").toString()).append("' ");
            }
            if(parmMap.get("ajid")!=null && StringUtils.isNotBlank(parmMap.get("ajid").toString())){
                sb.append(" AND ajid='").append(parmMap.get("ajid").toString()).append("' ");
            }
            if(parmMap.get("jjfs")!=null && StringUtils.isNotBlank(parmMap.get("jjfs").toString())){
                sb.append(" AND jjfs='").append(parmMap.get("jjfs").toString()).append("' ");
            }
            if(parmMap.get("ajlx")!=null && StringUtils.isNotBlank(parmMap.get("ajlx").toString())){
                sb.append(" AND ajlx='").append(parmMap.get("ajlx").toString()).append("' ");
            }
            if(parmMap.get("ajlb")!=null && StringUtils.isNotBlank(parmMap.get("ajlb").toString())){
                sb.append(" AND ajlb='").append(parmMap.get("ajlb").toString()).append("' ");
            }
            if(parmMap.get("lrdwdm")!=null && StringUtils.isNotBlank(parmMap.get("lrdwdm").toString())){
                String jgdmLike = StrUtil.removeDmZeor(parmMap.get("lrdwdm").toString().trim())+"%";
                sb.append(" AND lrdwdm like '").append(jgdmLike).append("' ");
            }
            if(parmMap.get("sldw")!=null && StringUtils.isNotBlank(parmMap.get("sldw").toString())){
                String jgdmLike = StrUtil.removeDmZeor(parmMap.get("sldw").toString().trim())+"%";
                sb.append(" AND sldw like '").append(jgdmLike).append("' ");
            }
            if(parmMap.get("afsj_begin")!=null && StringUtils.isNotBlank(parmMap.get("afsj_begin").toString())){
                sb.append(" AND afsj >=to_date('").append(parmMap.get("afsj_begin").toString()).append("','yyyy-mm-dd hh24:mi:ss') ");
            }
            if(parmMap.get("afsj_end")!=null && StringUtils.isNotBlank(parmMap.get("afsj_end").toString())){
                sb.append(" AND afsj <=to_date('").append(parmMap.get("afsj_end").toString()).append("','yyyy-mm-dd hh24:mi:ss') ");
            }
            if(parmMap.get("jjsj_begin")!=null && StringUtils.isNotBlank(parmMap.get("jjsj_begin").toString())){
                sb.append(" AND jjsj >=to_date('").append(parmMap.get("jjsj_begin").toString()).append("','yyyy-mm-dd hh24:mi:ss') ");
            }
            if(parmMap.get("jjsj_end")!=null && StringUtils.isNotBlank(parmMap.get("jjsj_end").toString())){
                sb.append(" AND jjsj <=to_date('").append(parmMap.get("jjsj_end").toString()).append("','yyyy-mm-dd hh24:mi:ss') ");
            }
            if(parmMap.get("nlevel")!=null && StringUtils.isNotBlank(parmMap.get("nlevel").toString())){
                //criteria.andEqualTo("lrdwdm",parmMap.get("lrdwdm").toString().trim());
                sb.append(" AND EXISTS (SELECT 1 FROM TB_ZNZF_JLT L WHERE L.JJDPK=T.PK AND YXX=1 AND NLEVEL >=").append(parmMap.get("nlevel").toString()).append(") ");
            }
            if (parmMap.get("saje_begin")!=null && StringUtils.isNotBlank(parmMap.get("saje_begin").toString()) && NumberUtils.isNumber(parmMap.get("saje_begin").toString())){
                //sb.andGreaterThanOrEqualTo("saje", NumberUtils.toLong(parmMap.get("saje_begin").toString(),0));
                sb.append(" AND saje>='").append(NumberUtils.toLong(parmMap.get("saje_begin").toString(),0)).append("' ");
            }
            if (parmMap.get("saje_end")!=null && StringUtils.isNotBlank(parmMap.get("saje_end").toString()) && NumberUtils.isNumber(parmMap.get("saje_end").toString())){
                //sb.andLessThanOrEqualTo("saje", NumberUtils.toLong(parmMap.get("saje_end").toString(),999999999999L));
                sb.append(" AND saje<='").append(NumberUtils.toLong(parmMap.get("saje_end").toString(),999999999999L)).append("' ");
            }

            sb.append(" order by CJSJ DESC,pk asc ");
            return sb.toString();

        }
    }

    List<Map<String,String>> indexStatistics(@Param("lrdwdm") String lrdwdm);

    List<Map<String,String>> indexStatisticsByAjlb(@Param("lrdwdm") String lrdwdm);

    List<Map<String,String>> indexStatisticsByRksj(@Param("rksj") String lrdwdm);
    @Update("update tb_fzzx_jjd set accuratetype=abs(accuratetype-1) where pk=#{pk}")
    int updateAccuratetype(String pk);
}