package com.unis.mapper.fzzx;

import com.unis.model.fzzx.JjdShxx;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
@Repository
public interface JjdShxxMapper extends Mapper<JjdShxx> {
    @Delete("DELETE FROM TB_FZZX_JJD_SHXX WHERE JJDPK=#{jjdPk}")
    int deleteByJjd(@Param("jjdPk")String jjdPk);
}