package com.unis.mapper.fzzx;

import com.unis.model.fzzx.UserFocus;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface UserFocusMapper extends Mapper<UserFocus> {
}