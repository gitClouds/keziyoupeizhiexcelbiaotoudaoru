package com.unis.mapper.fzzx;

import com.unis.model.fzzx.JjdShr;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
@Repository
public interface JjdShrMapper extends Mapper<JjdShr> {

    @Delete("DELETE FROM TB_FZZX_JJD_SHR WHERE JJDPK=#{jjdPk}")
    int deleteByJjd(@Param("jjdPk")String jjdPk);
    @Delete("DELETE FROM TB_FZZX_JJD_SHR WHERE JJDPK=#{jjdPk} AND PK NOT IN (#{shrPks})")
    int deleteNotExistsByJjdPk(@Param("jjdPk")String jjdPk,@Param("shrPks")String shrPks);
}