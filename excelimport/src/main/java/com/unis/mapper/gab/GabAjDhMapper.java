package com.unis.mapper.gab;

import com.unis.model.gab.GabAjDh;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface GabAjDhMapper extends Mapper<GabAjDh> {
}