package com.unis.mapper.gab;

import com.unis.model.gab.GabFj;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface GabFjMapper extends Mapper<GabFj> {
}