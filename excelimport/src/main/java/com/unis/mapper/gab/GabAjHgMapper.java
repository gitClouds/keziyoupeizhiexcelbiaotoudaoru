package com.unis.mapper.gab;

import com.unis.model.gab.GabAjHg;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface GabAjHgMapper extends Mapper<GabAjHg> {
}