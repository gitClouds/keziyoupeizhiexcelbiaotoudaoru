package com.unis.mapper.gab;

import com.unis.model.gab.GabAjSayhk;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface GabAjSayhkMapper extends Mapper<GabAjSayhk> {
}