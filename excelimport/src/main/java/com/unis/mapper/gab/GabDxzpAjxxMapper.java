package com.unis.mapper.gab;

import com.unis.model.gab.GabDxzpAjxx;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

@Repository
public interface GabDxzpAjxxMapper extends Mapper<GabDxzpAjxx> {

    List<Map<String,String>> queryMacCbAjxxByMacs(@Param("macs")String[] macs);
}