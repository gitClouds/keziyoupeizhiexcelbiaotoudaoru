package com.unis.mapper.gab;

import com.unis.model.gab.GabAjWl;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
@Repository
public interface GabAjWlMapper extends Mapper<GabAjWl> {
}