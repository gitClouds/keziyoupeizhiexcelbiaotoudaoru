package com.unis.mapper.gab;

import com.unis.model.gab.GabDyzyYhkQq;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface GabDyzyYhkQqMapper extends Mapper<GabDyzyYhkQq> {
    @Select("select ywsqbh,zh,zhxm,zhjgdm,lrrdwdm,rksj,sy from xz_dyfz.dyzy_yhk_qq a WHERE YXX='1' AND EXISTS (select 1 from xz_dyfz.DYZY_YHK_MXJG_MAC c where c.ywsqbh=a.ywsqbh and c.mac=#{macdz}) order by rksj desc")
    List<GabDyzyYhkQq> selectByMacdz(@Param("macdz") String macdz);
}