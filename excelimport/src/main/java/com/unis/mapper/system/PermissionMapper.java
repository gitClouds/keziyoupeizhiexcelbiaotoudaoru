package com.unis.mapper.system;

import com.unis.model.system.Permission;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

@Repository
public interface PermissionMapper extends Mapper<Permission> {
    List<Permission> queryCurrentUserPermit(@Param("userId") String userId);

    List<Permission> queryDeptPermit(@Param("deptCode")String deptCode);

    List<Permission> queryROlePermit(@Param("rOleCode")String rOleCode);

    List<Permission> queryPermissionPermit(@Param("userId")String userId);

    List<Map> queryListByType(@Param("cType")String cType,@Param("foreingKey")String foreingKey,@Param("permType")String permType);

    @Delete("DELETE T_SYS_PERM_RELATION  WHERE YXX=1 AND C_TYPE=#{cType} AND FOREIGN_KEY=#{foreingKey} AND PERM_TYPE=#{permType}")
    int deletePermRelat(@Param("cType")String cType,@Param("foreingKey")String foreingKey,@Param("permType")String permType);

    int batchInsertPermRelat(@Param("list")List list);
}