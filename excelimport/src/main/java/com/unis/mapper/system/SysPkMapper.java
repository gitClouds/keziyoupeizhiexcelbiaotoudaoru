package com.unis.mapper.system;

import com.unis.model.system.SysPk;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface SysPkMapper extends Mapper<SysPk> {
    String getPrimaryKey(@Param("seqName")String seqName);

    String getDbDate();
}