package com.unis.mapper.system;

import com.unis.model.system.Department;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

@Repository
public interface DepartmentMapper extends Mapper<Department> {
    List<Map<String,Object>> selectTreeList(@Param("sjdm")String sjdm,@Param("filterSql")String filterSql);
}