package com.unis.mapper.system;

import com.unis.model.system.DwDzz;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
@Repository
public interface DwDzzMapper extends Mapper<DwDzz> {

    @Select("SELECT * FROM T_SYS_DW_DZZ WHERE YXX=1 AND DWDM=#{dwdm}")
    DwDzz queryDwDzzByDwdm(@Param("dwdm")String dwdm);
}