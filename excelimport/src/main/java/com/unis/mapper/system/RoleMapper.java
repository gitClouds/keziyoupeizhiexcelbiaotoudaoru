package com.unis.mapper.system;

import com.unis.model.system.Role;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface RoleMapper extends Mapper<Role>  {
    List<Role> queryCurrentUserRole(@Param("userid")String userid);

}