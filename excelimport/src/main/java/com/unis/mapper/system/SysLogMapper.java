package com.unis.mapper.system;

import com.unis.model.system.SysLog;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
@Repository
public interface SysLogMapper extends Mapper<SysLog> {
}