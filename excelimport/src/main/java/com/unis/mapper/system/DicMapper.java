package com.unis.mapper.system;

import com.unis.model.system.Dic;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
@Repository
public interface DicMapper extends Mapper<Dic> {
}