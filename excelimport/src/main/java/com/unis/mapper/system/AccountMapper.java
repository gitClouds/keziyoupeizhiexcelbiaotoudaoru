package com.unis.mapper.system;

import com.unis.model.system.Account;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

@Repository
public interface AccountMapper extends Mapper<Account> {
    @Select("SELECT PK AS \"pk\",CODE as \"code\",NAME as \"name\",comments as \"comments\", " +
            "(SELECT (CASE WHEN COUNT(1)>0 THEN 'TRUE' ELSE 'FALSE' END) FROM t_sys_account_role WHERE ROLEID=T.PK AND USERID=#{userPk}) AS \"ckd\" " +
            "FROM t_sys_role T WHERE CODE<>'_SYSTEMADMIN'")
    List<Map<String,String>> queryRoleListByPage(@Param("userPk")String userPk);
    @Delete("DELETE t_sys_account_role  WHERE USERID=#{userPk} AND ROLEID not in (SELECT PK FROM t_sys_role WHERE CODE='_SYSTEMADMIN') ")
    int deleteUserRoleByUserPk(@Param("userPk")String userPk);

    int insertBatchUserRole(@Param("list")List list);
    @Update("UPDATE T_SYS_ACCOUNT SET SALT=#{account.salt},PASSWORD=#{account.password} where PK=#{account.pk}")
    int updateUserPwd(@Param("account") Account account);
}