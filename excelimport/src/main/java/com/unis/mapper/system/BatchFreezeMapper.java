package com.unis.mapper.system;

import com.unis.model.system.BatchFreeze;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
@Repository
public interface BatchFreezeMapper extends Mapper<BatchFreeze> {
}