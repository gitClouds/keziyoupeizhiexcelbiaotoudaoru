package com.unis.mapper.system;

import com.unis.model.system.Menu;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface MenuMapper extends Mapper<Menu> {

    List<Menu> selectPermissionMenuByLevel(@Param("userId")String userId,@Param("codeLevel") int codeLevel,@Param("parentCode")String parentCode);
}