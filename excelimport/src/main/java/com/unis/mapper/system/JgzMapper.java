package com.unis.mapper.system;

import com.unis.model.system.Jgz;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface JgzMapper extends Mapper<Jgz> {
    //@Select("SELECT * FROM T_SYS_JGZ WHERE yxx='1' AND (dwdm=#{dwdm} or userpk=#{userpk} ) order by userpk asc,rksj desc")
    @Select({"<script>",
            "SELECT * FROM T_SYS_JGZ",
            "WHERE yxx='1'  AND (1=2 ",
            "<when test='dwdm!=null'>",
            " OR dwdm=#{dwdm} ",
            "</when>",
            "<when test='userpk!=null'>",
            " OR userpk=#{userpk} ",
            "</when>",
            ") order by userpk asc,rksj desc </script>"})
    List<Jgz> queryJgzByUserPkAndDwdm(@Param("userpk") String userpk,@Param("dwdm") String dwdm);
}