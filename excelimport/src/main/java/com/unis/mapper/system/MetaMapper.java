package com.unis.mapper.system;

import com.unis.model.system.Meta;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
@Repository
public interface MetaMapper extends Mapper<Meta> {
}