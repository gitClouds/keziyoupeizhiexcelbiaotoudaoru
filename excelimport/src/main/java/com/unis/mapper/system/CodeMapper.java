package com.unis.mapper.system;

import com.unis.model.system.Code;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

@Repository
public interface CodeMapper extends Mapper<Code> {

    List<Code> selectDictMap(@Param("zddm") String zddm);

    /*List<Code> selectList(@Param("zddm") String zddm,@Param("dm") String dm,@Param("mc") String mc,@Param("yhm") String yhm,@Param("filterSql") String filterSql);
    List<Code> selectListByLike(@Param("zddm") String zddm,@Param("mc") String mc,@Param("filterSql") String filterSql);

    List<Map<String,Object>> selectTreeList(@Param("zddm") String zddm, @Param("dm") String dm, @Param("sjdm") String sjdm, @Param("mc") String mc, @Param("yhm") String yhm, @Param("filterSql") String filterSql);
*/
    List<Map<String,Object>> selectTreeListByZddm(@Param("zddm") String zddm,@Param("sjdm") String sjdm);
    List<Map<String,Object>> selectListByZddm(@Param("zddm") String zddm,@Param("dm")String dm,@Param("filterSql")String filterSql);
}