package com.unis.mapper.system;

import com.unis.model.system.BankCode;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface BankCodeMapper extends Mapper<BankCode> {
    @Select("SELECT * FROM T_SYS_BANK_CODE WHERE YXX=1  AND #{account} like STARTACCOUNT || '%' and rownum=1")//AND ACCOUNTLENGTH=LENGTH(#{account})
    BankCode queryBankByAccount(@Param("account")String account);

    @Select("SELECT * FROM T_SYS_BANK_CODE WHERE YXX=1 AND #{account} like STARTACCOUNT || '%' order by  STARTACCOUNT desc ")
    List<BankCode> queryBankListByAccount(@Param("account") String account);
}