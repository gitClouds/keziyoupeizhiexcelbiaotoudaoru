package com.unis.mapper.system;

import com.unis.model.system.SysLogMeta;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
@Repository
public interface SysLogMetaMapper extends Mapper<SysLogMeta> {
}