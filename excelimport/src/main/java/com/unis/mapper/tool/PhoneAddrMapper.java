package com.unis.mapper.tool;

import com.unis.model.tool.PhoneAddr;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
@Repository
public interface PhoneAddrMapper extends Mapper<PhoneAddr> {
}