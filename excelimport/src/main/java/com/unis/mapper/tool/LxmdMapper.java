package com.unis.mapper.tool;

import com.unis.model.tool.Lxmd;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
@Repository
public interface LxmdMapper extends Mapper<Lxmd> {
}