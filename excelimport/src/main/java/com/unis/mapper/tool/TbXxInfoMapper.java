package com.unis.mapper.tool;


import com.unis.model.tool.TbXxInfo;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

@org.apache.ibatis.annotations.Mapper
@Repository
public interface TbXxInfoMapper extends Mapper<TbXxInfo> {
    List<TbXxInfo> queryList(Map<String, Object> paramMap);
    List<Map<String, String>> queryAccountNumEveryDay(Map<String, Object> map) throws Exception;
    List<Map<String, String>> queryXijkPhone() throws Exception;
}
