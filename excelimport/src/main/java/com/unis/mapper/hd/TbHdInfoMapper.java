package com.unis.mapper.hd;

import com.unis.model.hd.TbHdInfo;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
@org.apache.ibatis.annotations.Mapper
@Repository
public interface TbHdInfoMapper extends Mapper<TbHdInfo> {
}
