package com.unis.mapper.hd;

import com.unis.model.hd.TbHdJgInfo;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
@Repository
public interface TbHdJgInfoMapper extends Mapper<TbHdJgInfo> {
}
