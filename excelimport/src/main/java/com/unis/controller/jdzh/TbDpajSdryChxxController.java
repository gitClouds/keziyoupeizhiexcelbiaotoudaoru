package com.unis.controller.jdzh;

import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.Map;

import com.unis.dto.ResultDto;
import com.unis.common.exception.http.RequestEntityTooLargeException;
import com.unis.common.excel.ExportBeanExcel;
import com.unis.common.util.ValidateUtil;

import com.unis.controller.BaseController;
import com.unis.model.jdzh.TbDpajSdryChxx;
import com.unis.service.jdzh.TbDpajSdryChxxService;

/**
 * @author gongjl
 * @version 1.0
 * @since 2020-03-10
 */
@Controller
@RequestMapping("/jdzh/tbDpajSdryChxx")
public class TbDpajSdryChxxController extends BaseController {
	private static final Logger logger = LoggerFactory.getLogger(TbDpajSdryChxxController.class);

    @Autowired
    private TbDpajSdryChxxService tbDpajSdryChxxService;


    @RequestMapping("/page")
    public String forwordPage()  {
        return "jdzh/tbDpajSdryChxx/tbDpajSdryChxxPage";
    }

    @RequestMapping( "/list")
    @ResponseBody
    public ResultDto queryTbDpajSdryChxxList(@RequestParam Map<?, ?> paramMap){
    	int pageSize = paramMap.get("pageSize")==null ? 10 : Integer.parseInt(paramMap.get("pageSize").toString());
        int pageNumber = paramMap.get("pageNumber")==null ? 1 : Integer.parseInt(paramMap.get("pageNumber").toString());
        try {
            ResultDto page = tbDpajSdryChxxService.queryListByPage(paramMap,pageNumber,pageSize);
            return page;
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new RequestEntityTooLargeException(e.getMessage());
        }
    }

    @RequestMapping("/excel")
    public void exportTbDpajSdryChxxList(HttpServletResponse response,@RequestParam Map<?, ?> paramMap){
        //导出默认1000行
        int pageSize = EXPORT_SIZE;
        int pageNumber = 1;
        OutputStream os = null;
        try {
            ResultDto page = tbDpajSdryChxxService.queryListByPage(paramMap,pageNumber,pageSize);
            List list = page.getRows();

            response.setContentType("application/vnd.ms-excel");
            response.addHeader("Content-Disposition", "attachment;filename=exportTbDpajSdryChxx"+ System.currentTimeMillis() + ".xls");
            os = response.getOutputStream();

            ExportBeanExcel<TbDpajSdryChxx> export = new ExportBeanExcel<TbDpajSdryChxx>();
            //根据需求选择导出字段，注意数据库中带有_等字符字段修改
            //导出表头
            String[] headers = {"记录编号--DExxxxx--编号规则为“XXXXXXXXXXXX（公安机关机构代码）+YYYYMM（年月）+XXXXXXXXXXXXXX（顺序号）”                         ","立案编号--DE00092--编号规则为“A+XXXXXXXXXXXX（公安机关机构代码）+YYYYMM（年月）+XXXX（顺序号）”                         ","涉毒人员编号--DE00038--编号规则为：R+XXXXXXXXXXXX(公安机关机构代码)+XXXXXX(年月)+XXXX(顺序号)              ","填报单位名称--DE00065--","登记人--DE00002--","登记日期--DE00554--","涉毒类型--DExxxxx--","查获日期--DE00554--","查获地点--DE00768--","活动区域--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码","查获单位--DE00065--","处置情况--DExxxxx--","查获地区--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码","处置日期--DE00554--","违法犯罪事实--DE00100--","银行卡发卡银行--DE00243--","银行卡号--DExxxxx--","银行账号--DE00241--","车牌号--DE00307--","是否吸毒--DE00838--采用GA/T XXXXX-XXXX《吸毒记录核查结果代码》","在逃人员编号--DExxxxx、DE00618--","立案地--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码","上网追逃日期--DE00554--","抓获地--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码","通缉级别--DE00232--采用GA240.54-2003《通缉级别代码》","抓获方式--DE01017--采用 GA/T XXX《抓获方式代码》","抓获线索来源--DE00170--采用GA 332.9 《禁毒信息管理代码 第9部分:线索提供方式代码》","抓获经过--DE01012--","户籍地--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码","是否逃跑--DExxxxx--","审判后执行情况--DExxxxx--","移交地区--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码","结伙作案中作用--DExxxxx--","是/否特殊人群--DExxxxx--","是/否高危预警人员--DExxxxx--","逮捕证号--DExxxxx--","查获单位代码","填报单位","刑期-年","刑期-月","执行截止日期","更新时间","有效性","更新时间戳--DEXXXXX--","修改部署单位--DEXXXXX--","法律文书编号","是否主犯","是否在逃","注释","注释","注释","注释","注释","注释","注释","注释","操作标识","注释","传输状态。0:未传输。1:已传输（新老系统数据传输）","注释","注释","注释","旧系统特殊人群代码","注释","来源(1：接口下发2：本地录入)"};
            //对应字段 headers 与cols一一对应
            String[] cols = {"jlbh","ajbh","xyrbh","tbdw_dwmc","djr_xm","djrq","sdlx","chrq","ddmc","hdqh_xzqh","chdw_dwmc","czqk","chd_xzqh","czrq","jyaq","khyhmc","xykh","yhzh","jdchphm","xdjlhcjgdm","ztrybh","lad_xzqh","swzjrq","zhd_xzqh","tjjbdm","zhfsdm","xstgfsdm","zcxwnrms","hjd_xzqh","sftp","sphzxqk","yjd_xzqh","jhzazzy","sftsrq","sfgwyjry","dbzh","chdwdm","tbdw_dwdm","xqn","xqy","zxqzrq","gxsj","yxx","gxsjc","xgbsdw","flwsbh","sfzf","sfzt","sftsrqlx","sfjhgk","zhsj","zhd","zhd_xxdq","zhd_xxdz","fzqk","fzlx","czbs","ryly","cszt","gjdm","sdryjlbh","chdwxc","sftsrq_old","sjly","ly"};

            export.exportExcel("毒品案件涉毒人员查获信息",headers,cols,list,os);

            os.flush();
            os.close();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new RequestEntityTooLargeException(e.getMessage());
        }
    }

    @RequestMapping("/curd")
    @ResponseBody
    public Map<String,String> curd(String curdType,TbDpajSdryChxx tbDpajSdryChxx) throws Exception {
    	Map<String,String> resultMap = new HashMap<>();
    	String msg = null;
        if (tbDpajSdryChxx!=null){
        	if (StringUtils.equalsIgnoreCase(curdType,"insert")){
        		//valid校验需在TbDpajSdryChxx的字段上增加校验规则，规则与表单规则保持一致
        		msg = ValidateUtil.valid(tbDpajSdryChxx);
        		if (StringUtils.isNotBlank(msg)){
        			logger.error("valid insert TbDpajSdryChxx is error :"+msg);
                     resultMap.put("msg",msg);
                     return resultMap;
                 }
                 tbDpajSdryChxxService.insert(tbDpajSdryChxx);
            }else if (StringUtils.equalsIgnoreCase(curdType,"delete")){
            	if(StringUtils.isNotBlank(tbDpajSdryChxx.getJlbh())) {
                    tbDpajSdryChxxService.delete(tbDpajSdryChxx.getJlbh()); //主键删除
                }else {
                    msg="没有要删除的数据";
                    logger.error("valid delete TbDpajSdryChxx is error :"+msg);
                    resultMap.put("msg",msg);
                    return resultMap;
                }
            }else if (StringUtils.equalsIgnoreCase(curdType,"update")){
            	msg = ValidateUtil.valid(tbDpajSdryChxx);
        		if (StringUtils.isNotBlank(msg)){
        			 logger.error("valid update TbDpajSdryChxx is error :"+msg);
                     resultMap.put("msg",msg);
                     return resultMap;
                 }
                tbDpajSdryChxxService.updateByPrimaryKey(tbDpajSdryChxx);
            }
            msg = "操作成功";
        }else{
            msg="信息未填写";
            logger.error(msg);
        }
    	resultMap.put("msg",msg);
        return resultMap;
    }

}
