package com.unis.controller.jdzh;

import com.unis.common.exception.http.RequestEntityTooLargeException;
import com.unis.common.secure.authc.UserInfo;
import com.unis.common.util.TemplateUtil;
import com.unis.common.util.ValidateUtil;
import com.unis.controller.BaseController;
import com.unis.dto.ResultDto;
import com.unis.model.jdzh.*;
import com.unis.service.jdzh.*;
import com.unis.service.system.CodeService;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import tk.mybatis.mapper.entity.Example;

import java.util.*;

@Controller
@RequestMapping("/jdzh")
public class JdzhController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(JdzhController.class);
    @Autowired
    private TbDpajSdryJbxxService tbDpajSdryJbxxService;
    @Autowired
    private TbXdryJbxxService tbXdryJbxxService;
    @Autowired
    private TbDpajSdryChxxService tbDpajSdryChxxService;
    @Autowired
    private TbDpajJbxxService tbDpajJbxxService;
    @Autowired
    private TbXdryChxxService tbXdryChxxService;
    @Autowired
    private CodeService codeService;

    @RequestMapping("/sdpage")
    public String sdage(Model model,String pks,Integer lx) throws Exception {
        Map<String, Map<String, String>> dicMap = codeService.queryDicWithSpecCode
                ("CYZJDM","XB");
        String dict = TemplateUtil.genJsonStr4Obj(dicMap);
        model.addAttribute("dicMap",dict);
        model.addAttribute("pks",pks);
        model.addAttribute("lx",lx);
        return "jdzh/tbDpajSdryJbxxPage";
    }

    @RequestMapping("/xdpage")
    public String xdPage(Model model,String pks,Integer lx) throws Exception {
        Map<String, Map<String, String>> dicMap = codeService.queryDicWithSpecCode
                ("CYZJDM","XB");
        String dict = TemplateUtil.genJsonStr4Obj(dicMap);
        model.addAttribute("dicMap",dict);
        model.addAttribute("pks",pks);
        model.addAttribute("lx",lx);
        return "jdzh/tbXdryJbxxPage";
    }

    @RequestMapping( "/check")
    @ResponseBody
    public String check(String xyrbh, int lx) {
        String msg = "";
        try {
            if (xyrbh!=null&&!"".equals(xyrbh)){
                List q = new ArrayList();
                q.add("1111111");
                q.add("2222222");
                if (lx==1){
                    Example example = new Example(TbDpajSdryJbxx.class);
                    Example.Criteria criteria = example.createCriteria();
                    criteria.andEqualTo("xyrbh",xyrbh);
                    criteria.andIn("czbz",q);
                    criteria.andEqualTo("yxx",1);
                    List<TbDpajSdryJbxx> sds = tbDpajSdryJbxxService.queryListByExample(example);
                    if (sds.size()>0){
                        msg = "该嫌疑人已存在，请直接编辑或查看";
                    }
                }else if(lx==2){
                    Example example = new Example(TbXdryJbxx.class);
                    Example.Criteria criteria = example.createCriteria();
                    criteria.andEqualTo("xyrbh",xyrbh);
                    criteria.andIn("ccbz",q);
                    criteria.andEqualTo("yxx",1);
                    List<TbXdryJbxx> xds = tbXdryJbxxService.queryListByExample(example);
                    if (xds.size()>0){
                        msg = "该嫌疑人已存在，请直接编辑或查看";
                    }
                }
            }else {
                msg = "嫌疑人编号为空校验失败";
            }
        }catch (Exception e){
            msg = "嫌疑人编号校验出错";
            logger.error("嫌疑人编号校验出错："+e);
        }
        return msg;
    }

    @RequestMapping("/addeditPage")
    public String addeditPage(Model model,Integer lx,String type,String pk ) throws Exception {
        Map<String, Map<String, String>> dicMap = codeService.queryDicWithSpecCode
                ("CYZJDM","XB","MZ","WHCD","DPFZXYRLX","CYZK","CSLX","RYZT","XX","RYJS","XZQH","XDRYLY");
        Date now = new Date();
        UserInfo user = this.getUserInfo();
        model.addAttribute("dicMap",dicMap);
        model.addAttribute("djdw",user.getJgmc());
        model.addAttribute("djdwdm",user.getJgdm());
        model.addAttribute("djrxm",user.getXm());
        model.addAttribute("now",now);
        model.addAttribute("lx",lx);
        model.addAttribute("type",type);
        model.addAttribute("pk",pk);
        if (lx==1){
            if("update".equals(type)){
                if (pk!=null&&!"".equals(pk)){
                    TbDpajSdryJbxx tbDpajSdryJbxx = tbDpajSdryJbxxService.queryByPk(pk);
                    model.addAttribute("sdry",tbDpajSdryJbxx);
                }else {
                    throw new Exception("禁毒人员修改pk为空");
                }
            }
            return "jdzh/tbDpajSdryJbxxForm";
        }else if(lx==2){
            if("update".equals(type)){
                if (pk!=null&&!"".equals(pk)){
                    TbXdryJbxx tbXdryJbxx = tbXdryJbxxService.queryTbXdryJbxxByPrimaryKey(pk);
                    model.addAttribute("xdry",tbXdryJbxx);
                }else {
                    throw new Exception("禁毒人员修改pk为空");
                }
            }
            return "jdzh/tbXdryJbxxForm";
        }
        return null;
    }

    @RequestMapping( "/sdlist")
    @ResponseBody
    public ResultDto querySdry(@RequestParam Map<Object, Object> paramMap,String pks,Integer yayklx){
        int pageSize = paramMap.get("pageSize")==null ? 10 : Integer.parseInt(paramMap.get("pageSize").toString());
        int pageNumber = paramMap.get("pageNumber")==null ? 1 : Integer.parseInt(paramMap.get("pageNumber").toString());
        String[] fpks = !"".equals(pks)?pks.split(",") :null;
        List<String> pkss = null;
        if (fpks!=null){
            pkss = new ArrayList<String>(Arrays.asList(fpks));
        }
        paramMap.put("pks",pkss);
        paramMap.put("yayklx",yayklx);
        try {
            ResultDto page = tbDpajSdryJbxxService.querySdry(paramMap,pageNumber,pageSize);
            return page;
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new RequestEntityTooLargeException(e.getMessage());
        }
    }

    @RequestMapping( "/xdlist")
    @ResponseBody
    public ResultDto queryXdry(@RequestParam Map<Object, Object> paramMap,String pks,Integer yayklx){
        int pageSize = paramMap.get("pageSize")==null ? 10 : Integer.parseInt(paramMap.get("pageSize").toString());
        int pageNumber = paramMap.get("pageNumber")==null ? 1 : Integer.parseInt(paramMap.get("pageNumber").toString());
        String[] fpks = !"".equals(pks)?pks.split(",") :null;
        List<String> pkss = null;
        if (fpks!=null){
            pkss = new ArrayList<String>(Arrays.asList(fpks));
        }
        paramMap.put("pks",pkss);
        paramMap.put("yayklx",yayklx);
        try {
            ResultDto page = tbXdryJbxxService.queryXdry(paramMap,pageNumber,pageSize);
            return page;
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new RequestEntityTooLargeException(e.getMessage());
        }
    }

    @RequestMapping("/jdzhryxq")
    public String jdzhryxq(Model model,String pk,String lx) throws Exception {
        Map<String, Map<String, String>> dicMap = codeService.queryDicWithSpecCode("CYZJDM","XZQH","CYZK","WHCD","MZ","CSLX","RYZT","DPFZXYRLX","RYJS","XX","XDRYLY","XB","GJ");
        model.addAttribute("dicMap",dicMap);
        if ("1".equals(lx)){//涉毒
            TbDpajSdryJbxx tbDpajSdryJbxx;
            List<TbDpajSdryChxx> SdryChxxs = null;
            if (pk!=null&&!"".equals(pk)){
                tbDpajSdryJbxx = tbDpajSdryJbxxService.queryByPk(pk);
                String xyrbh = tbDpajSdryJbxx.getXyrbh();
                if (xyrbh!=null&&!"".equals(xyrbh)){
                    Example example = new Example(TbDpajSdryChxx.class);
                    Example.Criteria criteria = example.createCriteria();
                    criteria.andEqualTo("xyrbh",xyrbh);
                    criteria.andEqualTo("yxx",1);
                    SdryChxxs = tbDpajSdryChxxService.queryListByExample(example);
                }
            }else {
                throw new Exception("涉毒人员详情页面主键为空");
            }
            model.addAttribute("sdry",tbDpajSdryJbxx);
            model.addAttribute("chxxs",SdryChxxs);
            model.addAttribute("lx",lx);
            return "jdzh/tbDpajSdryJbxxShow";
        }else if("2".equals(lx)){//吸毒
            TbXdryJbxx tbXdryJbxx = null;
            List<TbXdryChxx> chxxs = null;
            if (pk!=null&&!"".equals(pk)){
                tbXdryJbxx = new TbXdryJbxx();
                tbXdryJbxx.setRyxh(pk);
                tbXdryJbxx.setYxx(1);
                tbXdryJbxx = tbXdryJbxxService.queryAsObject(tbXdryJbxx);
                String xyrbh = tbXdryJbxx.getXyrbh();
                if (xyrbh!=null&&!"".equals(xyrbh)) {
                    Example example = new Example(TbXdryChxx.class);
                    Example.Criteria criteria = example.createCriteria();
                    criteria.andEqualTo("xyrbh", xyrbh);
                    criteria.andEqualTo("yxx", 1);
                    chxxs = tbXdryChxxService.queryListByExample(example);
                }
            }else {
                throw new Exception("吸毒人员详情页面主键为空");
            }
            model.addAttribute("xdry",tbXdryJbxx);
            model.addAttribute("chxxs",chxxs);
            model.addAttribute("lx",lx);
            return "jdzh/tbXdryJbxxShow";
        }
        return null;
    }

    @RequestMapping("/jdzhchajxx")
    public String jdzhchajxx(Model model,String ajbh,String xyrbh,String lx) throws Exception {
        Map<String, Map<String, String>> dicMap = codeService.queryDicWithSpecCode
                ("CZQK","AJLBDM","PAFSDM","SDLX","DPMC","YXJG","XZQH","BADWLXDM","CDFSDM","FYFSDM","FZTHXZDM","ZCJDDM","AJXZ","XSLY","DQSHJG","HLWAJLX","SDXQFS","AJLX","ZYQXDZFS","ZZLY","ZHFSDM","XSTGFSDM","DPLY","XDCS","CHYY","CHLY","TSRQ","JBLX","XYRLB","GKXZ");
        model.addAttribute("dicMap",dicMap);
        String dict = TemplateUtil.genJsonStr4Obj(dicMap);
        model.addAttribute("dict",dict);
        if (ajbh!=null&&!"".equals(ajbh)&&xyrbh!=null&&!"".equals(xyrbh)) {
            if ("1".equals(lx)) {//涉毒
                TbDpajJbxx tbDpajJbxx = new TbDpajJbxx();
                tbDpajJbxx.setAjbh(ajbh);
                tbDpajJbxx.setYxx(1);
                TbDpajJbxx sdaj = tbDpajJbxxService.queryAsObject(tbDpajJbxx);
                TbDpajSdryChxx tbDpajSdryChxx = new TbDpajSdryChxx();
                tbDpajSdryChxx.setAjbh(ajbh);
                tbDpajSdryChxx.setXyrbh(xyrbh);
                tbDpajSdryChxx.setYxx(1);
                TbDpajSdryChxx sdch = tbDpajSdryChxxService.queryAsObject(tbDpajSdryChxx);
                model.addAttribute("sdaj",sdaj);
                model.addAttribute("sdch",sdch);
                return "jdzh/tbDpajJbxxShow";
            } else if ("2".equals(lx)) {//吸毒
                TbXdryChxx tbXdryChxx = new TbXdryChxx();
                tbXdryChxx.setChxh(ajbh);
                tbXdryChxx.setXyrbh(xyrbh);
                tbXdryChxx.setYxx(1);
                TbXdryChxx xdch = tbXdryChxxService.queryAsObject(tbXdryChxx);
                model.addAttribute("xdch",xdch);
                return "jdzh/tbXdryChxxShow";
            }
        } else {
            throw new Exception("人员详情页面案件编号，嫌疑人编号为空");
        }
        return null;
    }

    @RequestMapping("/tbDpajSdryJbxx/curd")
    @ResponseBody
    public Map<String,String> sdcurd(String curdType,String pk,TbDpajSdryJbxx tbDpajSdryJbxx) throws Exception {
        Map<String,String> resultMap = new HashMap<>();
        String msg = null;
        if (tbDpajSdryJbxx!=null){
            String zjzl = tbDpajSdryJbxx.getCyzjdm();
            String zjhm = tbDpajSdryJbxx.getZjhm();
            if (!"111".equals(zjzl)){
                tbDpajSdryJbxx.setZjhm("-");
                tbDpajSdryJbxx.setQtzjhm(zjhm);
            }else {
                tbDpajSdryJbxx.setQtzjhm("-");
            }
            if (StringUtils.equalsIgnoreCase(curdType,"insert")){
                //valid校验需在TbDpajSdryJbxx的字段上增加校验规则，规则与表单规则保持一致
                msg = ValidateUtil.valid(tbDpajSdryJbxx);
                if (StringUtils.isNotBlank(msg)){
                    logger.error("valid insert TbDpajSdryJbxx is error :"+msg);
                    resultMap.put("msg",msg);
                    return resultMap;
                }
                tbDpajSdryJbxxService.insert(tbDpajSdryJbxx);
            }else if (StringUtils.equalsIgnoreCase(curdType,"delete")){
                if(StringUtils.isNotBlank(pk)) {
                    tbDpajSdryJbxxService.delete(pk); //主键删除
                }else {
                    msg="没有要删除的数据";
                    logger.error("valid delete TbDpajSdryJbxx is error :"+msg);
                    resultMap.put("msg",msg);
                    return resultMap;
                }
            }else if (StringUtils.equalsIgnoreCase(curdType,"update")){
                msg = ValidateUtil.valid(tbDpajSdryJbxx);
                if (StringUtils.isNotBlank(msg)){
                    logger.error("valid update TbDpajSdryJbxx is error :"+msg);
                    resultMap.put("msg",msg);
                    return resultMap;
                }
                if (pk!=null&&!"".equals(pk)) {
                    tbDpajSdryJbxx.setJlbh(pk);
                    tbDpajSdryJbxxService.updateByPrimaryKey(tbDpajSdryJbxx);
                }else {
                    msg="主键为空";
                    logger.error(msg);
                }
            }
            msg = "操作成功";
        }else{
            msg="信息未填写";
            logger.error(msg);
        }
        resultMap.put("msg",msg);
        return resultMap;
    }

    @RequestMapping("/tbXdryJbxx/curd")
    @ResponseBody
    public Map<String,String> xdcurd(String curdType,String pk,TbXdryJbxx tbXdryJbxx) throws Exception {
        Map<String,String> resultMap = new HashMap<>();
        String msg = null;
        if (tbXdryJbxx!=null){
            String zjzl = tbXdryJbxx.getCyzjdm();
            String zjhm = tbXdryJbxx.getZjhm();
            if (!"111".equals(zjzl)){
                tbXdryJbxx.setSfzhm18("-");
                tbXdryJbxx.setZjhm(zjhm);
            }else {
                tbXdryJbxx.setSfzhm18(zjhm);
                tbXdryJbxx.setZjhm("-");
            }
            if (StringUtils.equalsIgnoreCase(curdType,"insert")){
                //valid校验需在TbXdryJbxx的字段上增加校验规则，规则与表单规则保持一致
                msg = ValidateUtil.valid(tbXdryJbxx);
                if (StringUtils.isNotBlank(msg)){
                    logger.error("valid insert TbXdryJbxx is error :"+msg);
                    resultMap.put("msg",msg);
                    return resultMap;
                }
                tbXdryJbxxService.insert(tbXdryJbxx);
            }else if (StringUtils.equalsIgnoreCase(curdType,"delete")){
                if(StringUtils.isNotBlank(pk)) {
                    tbXdryJbxxService.delete(pk); //主键删除
                }else {
                    msg="没有要删除的数据";
                    logger.error("valid delete TbXdryJbxx is error :"+msg);
                    resultMap.put("msg",msg);
                    return resultMap;
                }
            }else if (StringUtils.equalsIgnoreCase(curdType,"update")){
                msg = ValidateUtil.valid(tbXdryJbxx);
                if (StringUtils.isNotBlank(msg)){
                    logger.error("valid update TbXdryJbxx is error :"+msg);
                    resultMap.put("msg",msg);
                    return resultMap;
                }
                if (pk!=null&&!"".equals(pk)){
                    tbXdryJbxx.setRyxh(pk);
                    tbXdryJbxxService.updateByPrimaryKey(tbXdryJbxx);
                }else {
                    msg="主键为空";
                    logger.error(msg);
                }
            }
            msg = "操作成功";
        }else{
            msg="信息未填写";
            logger.error(msg);
        }
        resultMap.put("msg",msg);
        return resultMap;
    }
}
