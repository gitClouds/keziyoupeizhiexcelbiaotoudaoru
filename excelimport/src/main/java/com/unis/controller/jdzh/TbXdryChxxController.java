package com.unis.controller.jdzh;

import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.Map;

import com.unis.dto.ResultDto;
import com.unis.common.exception.http.RequestEntityTooLargeException;
import com.unis.common.excel.ExportBeanExcel;
import com.unis.common.util.ValidateUtil;

import com.unis.controller.BaseController;
import com.unis.model.jdzh.TbXdryChxx;
import com.unis.service.jdzh.TbXdryChxxService;

/**
 * @author gongjl
 * @version 1.0
 * @since 2020-03-10
 */
@Controller
@RequestMapping("/jdzh/tbXdryChxx")
public class TbXdryChxxController extends BaseController {
	private static final Logger logger = LoggerFactory.getLogger(TbXdryChxxController.class);

    @Autowired
    private TbXdryChxxService tbXdryChxxService;


    @RequestMapping("/page")
    public String forwordPage()  {
        return "jdzh/tbXdryChxx/tbXdryChxxPage";
    }

    @RequestMapping( "/list")
    @ResponseBody
    public ResultDto queryTbXdryChxxList(@RequestParam Map<?, ?> paramMap){
    	int pageSize = paramMap.get("pageSize")==null ? 10 : Integer.parseInt(paramMap.get("pageSize").toString());
        int pageNumber = paramMap.get("pageNumber")==null ? 1 : Integer.parseInt(paramMap.get("pageNumber").toString());
        try {
            ResultDto page = tbXdryChxxService.queryListByPage(paramMap,pageNumber,pageSize);
            return page;
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new RequestEntityTooLargeException(e.getMessage());
        }
    }

    @RequestMapping("/excel")
    public void exportTbXdryChxxList(HttpServletResponse response,@RequestParam Map<?, ?> paramMap){
        //导出默认1000行
        int pageSize = EXPORT_SIZE;
        int pageNumber = 1;
        OutputStream os = null;
        try {
            ResultDto page = tbXdryChxxService.queryListByPage(paramMap,pageNumber,pageSize);
            List list = page.getRows();

            response.setContentType("application/vnd.ms-excel");
            response.addHeader("Content-Disposition", "attachment;filename=exportTbXdryChxx"+ System.currentTimeMillis() + ".xls");
            os = response.getOutputStream();

            ExportBeanExcel<TbXdryChxx> export = new ExportBeanExcel<TbXdryChxx>();
            //根据需求选择导出字段，注意数据库中带有_等字符字段修改
            //导出表头
            String[] headers = {"查获序号--DEXXXXX--","人员编号--DE00038--编号规则为：R+XXXXXXXXXXXX(公安机关机构代码)+XXXXXX(年月)+XXXX(顺序号)              ","查获日期--DE01160--","  查获单位_行政区划代码--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码","  查获单位_区划内详细地址--DE00076--","  查获单位_单位名称--DE00065--","吸毒场所--DE01135--采用GA/TXXXX《吸毒场所种类代码》","本次滥用毒品种类--DEXXXXX--","毒品来源--DE00166--采用GA 332.3《禁毒信息管理代码 第3部分:毒品来源代码》","违法事实_简要案情--DE00100--","查获类型--DE01133--吸毒人员查获类型代码","查获来源--DE01132--采用GA/TXXXX《吸毒人员查获来源代码》","尿检结果--DE01137--尿检结果代码","吸毒方式--DE01136--采用GA/TXXXX《吸毒方式代码》","复吸次数----","娱乐场所查获序号----","娱乐场所名称----","是否特殊人群----","特殊人群种类----","阳性结果----","是否疾病----","疾病类型----","是否末次查获----","查获次数----","  填表人_姓名--DE00002--","  填表人_联系电话--DE00216--固定电话号码、移动电话号码","审核人_姓名--DE00002--","填表单位_单位名称--DE00065--","填表日期--DE01158--","录入人_姓名--DE00002--","录入单位_单位名称--DE00065--","录入时间--DE00739--","备注--DE00503--","人员标识--DEXXXXX--","修改部署单位--DEXXXXX--","归属单位--DEXXXXX--","更新时间戳--DEXXXXX--","操作标识--DEXXXXX--","内外网标志--DEXXXXX--","有效性--DEXXXXX--","更新时间--DEXXXXX--","  查获单位_单位代码--DE00065--","录入单位_单位代码--DE00065--","填表单位_单位代码--DE00065--","嫌疑人类别代码---XYRLB","注释","注释","注释","注释","注释","注释","吞食异物描述","是否吞食异物","对应的所有处置情况","对应的所有处置情况名称","本次查获之前的管控现状代码","来源(1：接口下发2：本地录入)"};
            //对应字段 headers 与cols一一对应
            String[] cols = {"chxh","xyrbh","chrq","chdw_xzqhdm","chdw_qhnxxdz","chdw_dwmc","xdcszldm","bclydpzl","dplydm","wfss_jyaq","xdrychlxdm","xdrychlydm","njjgdm","xdfsdm","xfcs","ylcschxh","ylcsmc","sftsrq","tsrqzl","yxjg","sfjb","jblx","sfmcch","chcs","tbr_xm","tbr_lxdh","shr_xm","tbdw_dwmc","tbrq","lrr_xm","lrdw_dwmc","lrsj","bz","rybs","xgbsdw","gsdw","gxsjc","czbs","inoutbz","yxx","gxsj","chdw_dwdm","lrdw_dwdm","tbdw_dwdm","xyrlb","fjmc","wlgjfjxh","chjclx","chjc_dwdm","chjc_dwmc","chsnl","tsywms","sftsyw","czqk","czqkmc","bczhzqgkxzdm","ly"};

            export.exportExcel("吸毒人员查获信息",headers,cols,list,os);

            os.flush();
            os.close();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new RequestEntityTooLargeException(e.getMessage());
        }
    }

    @RequestMapping("/curd")
    @ResponseBody
    public Map<String,String> curd(String curdType,TbXdryChxx tbXdryChxx) throws Exception {
    	Map<String,String> resultMap = new HashMap<>();
    	String msg = null;
        if (tbXdryChxx!=null){
        	if (StringUtils.equalsIgnoreCase(curdType,"insert")){
        		//valid校验需在TbXdryChxx的字段上增加校验规则，规则与表单规则保持一致
        		msg = ValidateUtil.valid(tbXdryChxx);
        		if (StringUtils.isNotBlank(msg)){
        			logger.error("valid insert TbXdryChxx is error :"+msg);
                     resultMap.put("msg",msg);
                     return resultMap;
                 }
                 tbXdryChxxService.insert(tbXdryChxx);
            }else if (StringUtils.equalsIgnoreCase(curdType,"delete")){
            	if(StringUtils.isNotBlank(tbXdryChxx.getChxh())) {
                    tbXdryChxxService.delete(tbXdryChxx.getChxh()); //主键删除
                }else {
                    msg="没有要删除的数据";
                    logger.error("valid delete TbXdryChxx is error :"+msg);
                    resultMap.put("msg",msg);
                    return resultMap;
                }
            }else if (StringUtils.equalsIgnoreCase(curdType,"update")){
            	msg = ValidateUtil.valid(tbXdryChxx);
        		if (StringUtils.isNotBlank(msg)){
        			 logger.error("valid update TbXdryChxx is error :"+msg);
                     resultMap.put("msg",msg);
                     return resultMap;
                 }
                tbXdryChxxService.updateByPrimaryKey(tbXdryChxx);
            }
            msg = "操作成功";
        }else{
            msg="信息未填写";
            logger.error(msg);
        }
    	resultMap.put("msg",msg);
        return resultMap;
    }

}
