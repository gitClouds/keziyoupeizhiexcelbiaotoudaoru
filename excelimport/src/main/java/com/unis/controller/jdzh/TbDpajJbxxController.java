package com.unis.controller.jdzh;

import java.util.HashMap;
import java.util.List;

import com.unis.common.excel.ExportBeanExcel;
import com.unis.model.jdzh.TbDpajJbxx;
import com.unis.service.jdzh.TbDpajJbxxService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.Map;

import com.unis.dto.ResultDto;
import com.unis.common.exception.http.RequestEntityTooLargeException;
import com.unis.common.util.ValidateUtil;

import com.unis.controller.BaseController;

/**
 * @author gongjl
 * @version 1.0
 * @since 2020-03-10
 */
@Controller
@RequestMapping("/Jdzh/tbDpajJbxx")
public class TbDpajJbxxController extends BaseController {
	private static final Logger logger = LoggerFactory.getLogger(TbDpajJbxxController.class);

    @Autowired
    private TbDpajJbxxService tbDpajJbxxService;


    @RequestMapping("/page")
    public String forwordPage()  {
        return "Jdzh/tbDpajJbxx/tbDpajJbxxPage";
    }

    @RequestMapping( "/list")
    @ResponseBody
    public ResultDto queryTbDpajJbxxList(@RequestParam Map<?, ?> paramMap){
    	int pageSize = paramMap.get("pageSize")==null ? 10 : Integer.parseInt(paramMap.get("pageSize").toString());
        int pageNumber = paramMap.get("pageNumber")==null ? 1 : Integer.parseInt(paramMap.get("pageNumber").toString());
        try {
            ResultDto page = tbDpajJbxxService.queryListByPage(paramMap,pageNumber,pageSize);
            return page;
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new RequestEntityTooLargeException(e.getMessage());
        }
    }

    @RequestMapping("/excel")
    public void exportTbDpajJbxxList(HttpServletResponse response,@RequestParam Map<?, ?> paramMap){
        //导出默认1000行
        int pageSize = EXPORT_SIZE;
        int pageNumber = 1;
        OutputStream os = null;
        try {
            ResultDto page = tbDpajJbxxService.queryListByPage(paramMap,pageNumber,pageSize);
            List list = page.getRows();

            response.setContentType("application/vnd.ms-excel");
            response.addHeader("Content-Disposition", "attachment;filename=exportTbDpajJbxx"+ System.currentTimeMillis() + ".xls");
            os = response.getOutputStream();

            ExportBeanExcel<TbDpajJbxx> export = new ExportBeanExcel<TbDpajJbxx>();
            //根据需求选择导出字段，注意数据库中带有_等字符字段修改
            //导出表头
            String[] headers = {"记录编号--DExxxxx--编号规则为“XXXXXXXXXXXX（公安机关机构代码）+YYYYMM（年月）+XXXXXXXXXXXXXX（顺序号）”                         ","立案编号--DE00092--编号规则为“A+XXXXXXXXXXXX（公安机关机构代码）+YYYYMM（年月）+XXXX（顺序号）”                         ","案件名称--DE00094--","案件类别--DE00093--采用GA 240.1《刑事犯罪信息管理代码 第1部分: 案件类别代码》","发现时间--DE01085、DE01121--","发案地点--DE00075--","发案地点详址--DE00076--","破案地区--DE00076--","立案单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码","立案/承办单位名称--DE00065--","承办人--DE00002--","承办人联系信息--DE00216--固定电话号码、移动电话号码","立案日期--DE00220--","破案单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码","破案单位名称--DE00065--","破案部门--DE00063--采用GA 300.15《看守所在押人员信息管理代码 第15部分: 办案单位类型代码》","破案日期--DE00223--","藏毒方式代码--DE00171--采用GA 332.10《禁毒信息管理代码 第10部分:藏毒方式代码》","贩运方式代码--DExxxxx--","是否团伙/集团作案--DExxxxx--","作案团伙/集团类型--DE00998--采用 GA/T XXX《犯罪团伙性质代码》","作案团伙/集团名称--DE00997--","案情--DE00100--","见证人提供情况--DE00521--","作案人情况分析--DE00521--","侦察措施--DE01009--采用 GA/T XXX《案事件侦查行为分类与代码》","破案方式代码--DE00097--采用GA 240.18《刑事犯罪信息管理代码 第18部分: 破案方式代码》","撤案原因--DE00099--采用GA 398.7《经济犯罪案件管理信息系统技术规范 第7部分：撤销案件原因代码》","撤案日期--DE01102--","查结情况--DE00521--","查结日期--DE00101--","公开方式--DExxxxx--","完全公开日期--DE00101--","可查状态--DE00596--采用GA/T 2000.40《公安信息代码 第40部分：使用状态代码》","查询单位名称--DE00065--","查询单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码","案件阶段--DE00096--采用GA 398.5《经济犯罪案件管理信息系统技术规范 第5部分：侦查工作阶段代码》","填报单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码","填报单位名称--DE00065--","登记人--DE00002--","登记日期--DE00524--","备注--DE00503--","审核单位名称--DE00065--","审核单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码","缴毒总量（克）--DExxxxx--","情报提供单位--DE00065--","情报提供单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码","贩运渠道说明--DE00521--","存在不合格--DE00742--判断标识","案件代号--DExxxxx--","案件性质--DExxxxx--","主侦单位名称--DE00060--","主侦单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码","指导单位名称--DE00060--","指导单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码","涉案国家--DE00069--采用GB/T 2659《世界各国和地区名称代码》中全部三位字母代码","涉案地区--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码","负责人--DE00002--","负责人电话--DE00216--固定电话号码、移动电话号码","线索来源----","线索来源单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码","线索来源单位名称--DE00060--","其它线索来源--DExxxxx--","目前侦查工作进展情况--DE01012--","侦查工作中遇到的主要问题--DE01012--","下一步工作建议--DE01012--","请示信息内容(DZWJNR)--DE01076--","督办级别--DE00091--采用GA 398.3《经济犯罪案件管理信息系统技术规范 第3部分：督办级别代码》0 部级  1省级  2市级。默认为空","破案状态--DExxxxx--","业务类别--DExxxxx--","确立时间--DE00101--","再次申报请示--DE01012--","申请联合请示--DE01012--","申请联合审批标识--DExxxxx--","联合办案--DExxxxx--","联合协查机构--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码","案件破案申请表--DE01012--","破案确立时间--DE00554--","撤销请示--DExxxxx--","立案/承办地区","当前审核结果","审核标志","更新时间","有效性","更新时间戳--DEXXXXX--","修改部署单位--DEXXXXX--","是否互联网案件--46","互联网案件类型--860","其他说明","是否涉嫌洗钱--46","涉毒洗钱方式--861","其他说明(洗钱)","是否黑社会性质--DExxxxx--","是否武装贩毒--DExxxxx--","目标案件操作级别： 1县建议。2县审核。3市建议。4市审核。5省建议。6省审核。7部建议。8部审核。默认为空","目标案件操作类别：  0申请目标案件。 1申请降级。2申请破案。3申请撤销。4已确立 。默认为空","案件类型 --DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码","部级目标案件编号--DExxxxx--编号规则为“XXXX（行政代码）+YYYY（年）+XXXX（顺序号）”","省级目标案件编号--DExxxxx--编号规则为“XXXX（行政代码）+YYYY（年）+XXXX（顺序号）”","市级目标案件编号--DExxxxx--编号规则为“XXXX（行政代码）+YYYY（年）+XXXX（顺序号）”","目标案件录入单位级别  0 部级  1省级  2市级  3县级   4派出所","宣传形式","是否宣传--46","是否群众举报--46","降级申报请示","上报单位名称--DE00060--","上报单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码","联系人","联系人电话","部级申报目标案件请示信息内容(DZWJNR)--DE01076--","省级申报目标案件请示信息内容(DZWJNR)--DE01076--","市级申报目标案件请示信息内容(DZWJNR)--DE01076--","县级申报目标案件请示信息内容(DZWJNR)--DE01076--","操作标识","转移清洗毒资方式","查缉站点编号","特情人员编号","情报产品编号","指示降级信息","部级申报目标案件请示信息时间","省级申报目标案件请示信息时间","市级申报目标案件请示信息时间","县级申报目标案件请示信息时间","报送项目类型","部级目标案件确立时间","省级目标案件确立时间","市级目标案件确立时间","传输状态。0:未传输。1:已传输（新老系统数据传输）","注释","注释","旧系统中破案方式代码--DE00097--采用GA 240.18《刑事犯罪信息管理代码 第18部分: 破案方式代码》","注释","注释","前期处置措施","种子来源","目标案件类别","目标案件序号","案件来源","禁种铲毒案件类型","来源(1：接口下发2：本地录入)"};
            //对应字段 headers 与cols一一对应
            String[] cols = {"jlbh","ajbh","ajmc","ajlbdm","fxsj","dzmc","fazdxz","padq","ladw_dwdm","ladw_dwmc","cbrxm","cbr_lxdh","larq","padw_dwdm","padw_dwmc","badwlxdm","parq","cdfsdm","fyfsdm","sfjt","fzthxzdm","fzthmc","jyaq","jzrtgqk","zarqkfx","asjzcxwlbdm","pafsdm","cxajyydm","cxajrq","cjqk","cjrq","gkfs","wqgkrq","syztdm","cxdw_dwmc","cxdw_dwdm","zcjddm","tbdw_dwdm","tbdw_dwmc","djr_xm","djrq","bz","shdw_dwmc","shdw_dwdm","jhdp","qbtgdw_dwmc","qbtgdw_dwdm","fyqdsm","pdbz","ajdh","ajxz","zzdw_dwmc","zzdw_dwdm","zddw_dwmc","zddw_dwdm","gjhdqdm","sadq_xzqh","fzrxm","fzr_lxdh","xsly","xsly_dwdm","xsly_dwmc","qyxsly","mqzcgzjzqk","zcgzzwt","xybgzjy","inforcontent","asjdbjbdm","pazt","ywlb","qlsj","zcsbqs","sqlhqs","sqlhspbs","lhba","lhzc_gajgjgdm","ajpasqb","paqlsj","cxqs","lacbdq","dqshjg","shbz","gxsj","yxx","gxsjc","xgbsdw","sfhlwaj","hlwajlx","qtsm","sfsxxq","sdxqfs","xqqtsm","sfhsh","sfwzfd","mbajzt","ajdj","ajlx","b_m_jlbh","s_m_jlbh","c_m_jlbh","mbajsbdj","xcxs","sfxc","sfqzjb","jjsqxx","sbdw_dwmc","sbdw_dwdm","lxr","lxr_dh","b_inforcontent","s_inforcontent","c_inforcontent","x_inforcontent","czbs","zyqxdzfs","cjzdbh","tqrybh","qbcpbh","zsjjsqxx","b_inforcontent_sqsj","s_inforcontent_sqsj","c_inforcontent_sqsj","x_inforcontent_sqsj","bsxm_lx","bm_qlsj","sm_qlsj","cm_qlsj","cszt","cbrxm2","cbr_lxdh2","pafsdm_old","n_yxh","sjly","qqczcs","zzly","mbajlb","mbajxh","ajly","jzcd_ajlx","ly"};

            export.exportExcel("毒品案件基本信息",headers,cols,list,os);

            os.flush();
            os.close();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new RequestEntityTooLargeException(e.getMessage());
        }
    }

    @RequestMapping("/curd")
    @ResponseBody
    public Map<String,String> curd(String curdType,TbDpajJbxx tbDpajJbxx) throws Exception {
    	Map<String,String> resultMap = new HashMap<>();
    	String msg = null;
        if (tbDpajJbxx!=null){
        	if (StringUtils.equalsIgnoreCase(curdType,"insert")){
        		//valid校验需在TbDpajJbxx的字段上增加校验规则，规则与表单规则保持一致
        		msg = ValidateUtil.valid(tbDpajJbxx);
        		if (StringUtils.isNotBlank(msg)){
        			logger.error("valid insert TbDpajJbxx is error :"+msg);
                     resultMap.put("msg",msg);
                     return resultMap;
                 }
                 tbDpajJbxxService.insert(tbDpajJbxx);
            }else if (StringUtils.equalsIgnoreCase(curdType,"delete")){
            	if(StringUtils.isNotBlank(tbDpajJbxx.getJlbh())) {
                    tbDpajJbxxService.delete(tbDpajJbxx.getJlbh()); //主键删除
                }else {
                    msg="没有要删除的数据";
                    logger.error("valid delete TbDpajJbxx is error :"+msg);
                    resultMap.put("msg",msg);
                    return resultMap;
                }
            }else if (StringUtils.equalsIgnoreCase(curdType,"update")){
            	msg = ValidateUtil.valid(tbDpajJbxx);
        		if (StringUtils.isNotBlank(msg)){
        			 logger.error("valid update TbDpajJbxx is error :"+msg);
                     resultMap.put("msg",msg);
                     return resultMap;
                 }
                tbDpajJbxxService.updateByPrimaryKey(tbDpajJbxx);
            }
            msg = "操作成功";
        }else{
            msg="信息未填写";
            logger.error(msg);
        }
    	resultMap.put("msg",msg);
        return resultMap;
    }

}
