package com.unis.controller.tool;

import java.util.HashMap;
import java.util.List;

import com.unis.logger.TargetLogger;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.Map;

import com.unis.dto.ResultDto;
import com.unis.common.exception.http.RequestEntityTooLargeException;
import com.unis.common.excel.ExportBeanExcel;
import com.unis.common.util.ValidateUtil;

import com.unis.controller.BaseController;
import com.unis.model.tool.PhoneAddr;
import com.unis.service.tool.PhoneAddrService;

/**
 * @author xuk
 * @version 1.0
 * @since 2019-04-29
 */
@Controller
@RequestMapping("/tool/phoneAddr")
public class PhoneAddrController extends BaseController {
	private static final Logger logger = LoggerFactory.getLogger(PhoneAddrController.class);

    @Autowired
    private PhoneAddrService phoneAddrService;
    
    
    @RequestMapping("/page")
    public String forwordPage()  {
        return "tool/phoneAddr/phoneAddrPage";
    }

    @TargetLogger("0701")
    @RequiresPermissions("ROLE_VIEW")
    @RequestMapping( "/list")
    @ResponseBody
    public ResultDto queryPhoneAddrList(@RequestParam Map<?, ?> paramMap){
    	int pageSize = paramMap.get("pageSize")==null ? 10 : Integer.parseInt(paramMap.get("pageSize").toString());
        int pageNumber = paramMap.get("pageNumber")==null ? 1 : Integer.parseInt(paramMap.get("pageNumber").toString());
        try {
            ResultDto page = phoneAddrService.queryListByPage(paramMap,pageNumber,pageSize);
            return page;
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new RequestEntityTooLargeException(e.getMessage());
        }
    }
    
}
