package com.unis.controller.tool;

import java.util.HashMap;
import java.util.List;

import com.unis.logger.TargetLogger;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.Map;

import com.unis.dto.ResultDto;
import com.unis.common.exception.http.RequestEntityTooLargeException;
import com.unis.common.excel.ExportBeanExcel;
import com.unis.common.util.ValidateUtil;

import com.unis.controller.BaseController;
import com.unis.model.tool.Lxmd;
import com.unis.service.tool.LxmdService;

/**
 * @author xuk
 * @version 1.0
 * @since 2019-04-26
 */
@Controller
@RequestMapping("/tool/lxmd")
public class LxmdController extends BaseController {
	private static final Logger logger = LoggerFactory.getLogger(LxmdController.class);

    @Autowired
    private LxmdService lxmdService;
    
    
    @RequestMapping("/page")
    public String forwordPage(String jtype, Model model)  {
        model.addAttribute("jtype",jtype);
        return "tool/lxmd/lxmdPage";
    }
    @TargetLogger("07030401")
    @RequiresPermissions("ROLE_VIEW")
    @RequestMapping( "/list")
    @ResponseBody
    public ResultDto queryLxmdList(@RequestParam Map<?, ?> paramMap){
    	int pageSize = paramMap.get("pageSize")==null ? 10 : Integer.parseInt(paramMap.get("pageSize").toString());
        int pageNumber = paramMap.get("pageNumber")==null ? 1 : Integer.parseInt(paramMap.get("pageNumber").toString());
        try {
            ResultDto page = lxmdService.queryListByPage(paramMap,pageNumber,pageSize);
            return page;
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new RequestEntityTooLargeException(e.getMessage());
        }
    }
    @TargetLogger("07030402")
    @RequiresPermissions("ROLE_VIEW")
    @RequestMapping("/excel")
    public void exportLxmdList(HttpServletResponse response,@RequestParam Map<?, ?> paramMap){
        //导出默认1000行
        int pageSize = EXPORT_SIZE;
        int pageNumber = 1;
        OutputStream os = null;
        try {
            ResultDto page = lxmdService.queryListByPage(paramMap,pageNumber,pageSize);
            List list = page.getRows();

            response.setContentType("application/vnd.ms-excel");
            response.addHeader("Content-Disposition", "attachment;filename=exportLxmd"+ System.currentTimeMillis() + ".xls");
            os = response.getOutputStream();

            ExportBeanExcel<Lxmd> export = new ExportBeanExcel<Lxmd>();
            //根据需求选择导出字段，注意数据库中带有_等字符字段修改
            //导出表头
            String[] headers = {"机构号","地区码","机构名称","调证手续","联系人","固话","手机号","邮箱","性质"};
            //对应字段 headers 与cols一一对应
            String[] cols = {"jgh","dqm","dwmc","dzsx","lxr","telephone","phone","email","xz"};
			
            export.exportExcel("sheet",headers,cols,list,os);

            os.flush();
            os.close();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new RequestEntityTooLargeException(e.getMessage());
        }
    }

    @TargetLogger("070301")
    @RequiresPermissions("ROLE_CURD")
    @RequestMapping("/curd")
    @ResponseBody
    public Map<String,String> curd(String curdType,Lxmd lxmd) throws Exception {
    	Map<String,String> resultMap = new HashMap<>();
    	String msg = null;
        if (lxmd!=null){
        	if (StringUtils.equalsIgnoreCase(curdType,"insert")){
        		//valid校验需在Lxmd的字段上增加校验规则，规则与表单规则保持一致
        		msg = ValidateUtil.valid(lxmd);
        		if (StringUtils.isNotBlank(msg)){
        			logger.error("valid insert Lxmd is error :"+msg);
                     resultMap.put("msg",msg);
                     return resultMap;
                 }
                 lxmdService.insert(lxmd);
            }else if (StringUtils.equalsIgnoreCase(curdType,"delete")){
            	if(StringUtils.isNotBlank(lxmd.getPk())) {
                    lxmdService.delete(lxmd.getPk()); //主键删除
                }else {
                    msg="没有要删除的数据";
                    logger.error("valid delete Lxmd is error :"+msg);
                    resultMap.put("msg",msg);
                    return resultMap;
                }
            }else if (StringUtils.equalsIgnoreCase(curdType,"update")){
            	msg = ValidateUtil.valid(lxmd);
        		if (StringUtils.isNotBlank(msg)){
        			 logger.error("valid update Lxmd is error :"+msg);
                     resultMap.put("msg",msg);
                     return resultMap;
                 }
                lxmdService.updateByPrimaryKey(lxmd);
            }
            msg = "操作成功";
        }else{
            msg="信息未填写";
            logger.error(msg);
        }
    	resultMap.put("msg",msg);
        return resultMap;
    }
    
}
