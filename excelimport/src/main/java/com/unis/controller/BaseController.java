package com.unis.controller;

import com.unis.common.secure.authc.UserInfo;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Value;

/**
 * Created by Administrator on 2018/12/29/029.
 */
public class BaseController {
    protected static final String REMIND_MSG = "msg";

    protected static final int EXPORT_SIZE = 1000;

    public UserInfo getUserInfo(){
        Subject currentUser = SecurityUtils.getSubject();
        UserInfo userInfo = (UserInfo) currentUser.getPrincipal();
        return userInfo;
    }
}
