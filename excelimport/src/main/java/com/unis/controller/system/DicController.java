package com.unis.controller.system;

import com.unis.common.exception.http.RequestEntityTooLargeException;
import com.unis.controller.BaseController;
import com.unis.dto.ResultDto;
import com.unis.service.system.CodeService;
import com.unis.service.system.DepartmentService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2019/1/2/002.
 */
@Controller
@RequestMapping("/dict")
public class DicController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(DicController.class);
    @Autowired
    private CodeService codeService;
    @Autowired
    private DepartmentService departmentService;

    /*@Value("${dict.label}")
    private String dictLabel;*/

    /*@RequestMapping("/list/{zddm}/")
    @ResponseBody
    public Object queryDicList(@PathVariable String zddm, @RequestParam Map<?, ?> paramMap) throws Exception {
        List<Map<String, String>> dictList = null;
        String mc = "";
        if(paramMap!=null){
            mc = paramMap.get("mc")==null?null:new String(paramMap.get("mc").toString().getBytes("ISO8859-1"),"UTF-8");
        }

        *//*List<Code> codeList = codeService.selectList(zddm,
                paramMap.get("dm")==null?null:paramMap.get("dm").toString(),
                mc,
                paramMap.get("yhm")==null?null:paramMap.get("yhm").toString(),
                paramMap.get("filterSql")==null?null:paramMap.get("filterSql").toString());*//*
        List<Code> codeList = codeService.selectList(zddm,
                mc,
                paramMap.get("filterSql")==null?null:paramMap.get("filterSql").toString());
        if (codeList!=null && codeList.size()>0){
            dictList = new ArrayList<Map<String, String>>();
            for (Code code : codeList){
                Map<String, String> dicMap = new HashMap<String, String>();
                dicMap.put("mc",code.getMc());
                dicMap.put("dm",code.getDm());
                //dicMap.put("label", MessageFormat.format(dictLabel, code.getDm(), code.getMc()));
                dicMap.put("label","<span>"+code.getDm()+"</span> | <span>"+code.getMc()+"</span>");

                dictList.add(dicMap);
            }
        }

        return dictList;
    }

    @RequestMapping("/list/{zddm}/{dm}")
    @ResponseBody
    public Object queryDicListByDm(@PathVariable String zddm, @PathVariable String dm) throws Exception {
        List<Map<String, String>> dictList = null;
        String mc = "";
        if(StringUtils.isNotBlank(dm)){
            List<Code> codeList = codeService.selectList(zddm,
                    dm,
                    null,
                    null,
                    null);
            if (codeList!=null && codeList.size()>0){
                dictList = new ArrayList<Map<String, String>>();
                for (Code code : codeList){
                    Map<String, String> dicMap = new HashMap<String, String>();
                    dicMap.put("mc",code.getMc());
                    dicMap.put("dm",code.getDm());
                    //dicMap.put("label", MessageFormat.format(dictLabel, code.getDm(), code.getMc()));
                    dicMap.put("label","<span>"+code.getDm()+"</span> | <span>"+code.getMc()+"</span>");

                    dictList.add(dicMap);
                }
            }

            return dictList;
        }else{
            return null;
        }
    }
    @RequestMapping("/list4Tree")
    @ResponseBody
    public Object queryTreeList(@RequestParam Map<?, ?> paramMap){
        if (paramMap!=null){
            String zddm = paramMap.get("zddm")==null?null : paramMap.get("zddm").toString();
            String dm = paramMap.get("dm")==null?null : paramMap.get("dm").toString();
            String sjdm = paramMap.get("sjdm")==null?null : paramMap.get("sjdm").toString();
            String mc = paramMap.get("mc")==null?null : paramMap.get("mc").toString();
            String yhm = paramMap.get("yhm")==null?null : paramMap.get("yhm").toString();
            String filterSql = paramMap.get("filterSql")==null?null : paramMap.get("filterSql").toString();

            return codeService.selectTreeList(zddm,dm,sjdm,mc,yhm,filterSql);
        }else{
            return null;
        }
    }*/
    @RequestMapping("/listTree")
    @ResponseBody
    public List queryTreeListByZddm(@RequestParam Map<?, ?> paramMap){
        String zddm="";
        String sjdm="";
        if (paramMap!=null && paramMap.get("zddm")!=null){
            zddm = paramMap.get("zddm")==null?null : paramMap.get("zddm").toString();

            if(StringUtils.isNotBlank(zddm)){
                if(StringUtils.equalsIgnoreCase(zddm,"dept")){
                    //sjdm为空查询一级菜单
                    sjdm = paramMap.get("sjdm")==null||StringUtils.isBlank(paramMap.get("sjdm").toString())?null : paramMap.get("sjdm").toString();
                    int deptLevel = StringUtils.isNotBlank(sjdm)?null:1;
                    String filterSql = paramMap==null ||paramMap.get("filterSql")==null || StringUtils.isNotBlank(paramMap.get("filterSql").toString())?null:paramMap.get("filterSql").toString();
                    List list = departmentService.selectTreeList(sjdm,filterSql,deptLevel);
                    return list;
                }else{
                    //目前仅有XZQH使用，sjdm为空查询一级菜单
                    sjdm = paramMap.get("sjdm")==null||StringUtils.isBlank(paramMap.get("sjdm").toString())?null : paramMap.get("sjdm").toString();
                    int codeLevel = StringUtils.isNotBlank(sjdm)?null:1;
                    List list = codeService.selectTreeListByZddm(zddm,sjdm,codeLevel);
                    return list;
                }
            }
        }

        return null;
    }
    @RequestMapping("/listCode/{zddm}")
    @ResponseBody
    public List queryListByZddm(@PathVariable String zddm,@RequestParam Map<?, ?> paramMap){
        if (StringUtils.isNotBlank(zddm)){
            String dm = paramMap==null ||paramMap.get("dm")==null || StringUtils.isNotBlank(paramMap.get("dm").toString())?null:paramMap.get("dm").toString();
            String filterSql = paramMap==null ||paramMap.get("filterSql")==null || StringUtils.isNotBlank(paramMap.get("filterSql").toString())?null:paramMap.get("filterSql").toString();
            List list = codeService.queryListByZddm(zddm,dm,filterSql);
            return list;
        }

        return null;
    }

    @RequestMapping( "/list")
    @ResponseBody
    public ResultDto queryDeptList(@RequestParam Map<?, ?> paramMap){
        int pageSize = paramMap.get("pageSize")==null ? 10 : Integer.parseInt(paramMap.get("pageSize").toString());
        int pageNumber = paramMap.get("pageNumber")==null ? 1 : Integer.parseInt(paramMap.get("pageNumber").toString());
        try {
            ResultDto page = departmentService.queryListByPage(paramMap,pageNumber,pageSize);
            return page;
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new RequestEntityTooLargeException(e.getMessage());
        }
    }

}
