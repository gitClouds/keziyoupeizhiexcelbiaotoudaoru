package com.unis.controller.system;

import java.util.HashMap;
import java.util.List;

import com.unis.logger.TargetLogger;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.Map;

import com.unis.dto.ResultDto;
import com.unis.common.exception.http.RequestEntityTooLargeException;
import com.unis.common.excel.ExportBeanExcel;
import com.unis.common.util.ValidateUtil;

import com.unis.controller.BaseController;
import com.unis.model.system.Role;
import com.unis.service.system.RoleService;

/**
 * Created by Administrator on 2019/1/8/008.
 */
@Controller
@RequestMapping("/system/role")
public class RoleController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(RoleController.class);

    @Autowired
    private RoleService roleService;

    @RequiresPermissions("SYS_ROLE")
    @RequestMapping("/page")
    public String forwordPage()  {
        return "system/role/rolePage";
    }

    @TargetLogger("020401")
    @RequiresPermissions("SYS_ROLE")
    @RequestMapping( "/list")
    @ResponseBody
    public ResultDto queryRoleList(@RequestParam Map<?, ?> paramMap){
        int pageSize = paramMap.get("pageSize")==null ? 10 : Integer.parseInt(paramMap.get("pageSize").toString());
        int pageNumber = paramMap.get("pageNumber")==null ? 1 : Integer.parseInt(paramMap.get("pageNumber").toString());
        try {
            ResultDto page = roleService.queryListByPage(paramMap,pageNumber,pageSize);
            return page;
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new RequestEntityTooLargeException(e.getMessage());
        }
    }

    @TargetLogger("020402")
    @RequiresPermissions("SYS_ROLE")
    @RequestMapping("/excel")
    public void exportRoleList(HttpServletResponse response, @RequestParam Map<?, ?> paramMap){
        //导出默认1000行
        int pageSize = EXPORT_SIZE;
        int pageNumber = 1;
        OutputStream os = null;
        try {
            ResultDto page = roleService.queryListByPage(paramMap,pageNumber,pageSize);
            List list = page.getRows();

            response.setContentType("application/vnd.ms-excel");
            response.addHeader("Content-Disposition", "attachment;filename=exportRole"+ System.currentTimeMillis() + ".xls");
            os = response.getOutputStream();

            ExportBeanExcel<Role> export = new ExportBeanExcel<Role>();
            //根据需求选择导出字段，注意数据库中带有_等字符字段修改
            //导出表头
            String[] headers = {"角色代码","角色名称","角色描述"};
            //对应字段 headers 与cols一一对应
            String[] cols = {"code","name","comments"};

            export.exportExcel("系统角色",headers,cols,list,os);

            os.flush();
            os.close();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new RequestEntityTooLargeException(e.getMessage());
        }
    }

    @TargetLogger("0201")
    @RequiresPermissions("SYS_ROLE")
    @RequestMapping("/curd")
    @ResponseBody
    public Map<String,String> curd(String curdType,Role role) throws Exception {
        Map<String,String> resultMap = new HashMap<>();
        String msg = null;
        if (role!=null){
            if (StringUtils.equalsIgnoreCase(curdType,"insert")){
                //valid校验需在Role的字段上增加校验规则，规则与表单规则保持一致
                msg = ValidateUtil.valid(role);
                if (StringUtils.isNotBlank(msg)){
                    logger.error("valid insert Role is error :"+msg);
                    resultMap.put("msg",msg);
                    return resultMap;
                }
                roleService.insert(role);
            }else if (StringUtils.equalsIgnoreCase(curdType,"delete")){
                if(StringUtils.isNotBlank(role.getPk())) {
                    roleService.delete(role.getPk()); //主键删除
                }else {
                    msg="没有要删除的数据";
                    logger.error("valid delete Role is error :"+msg);
                    resultMap.put("msg",msg);
                    return resultMap;
                }
            }else if (StringUtils.equalsIgnoreCase(curdType,"update")){
                msg = ValidateUtil.valid(role);
                if (StringUtils.isNotBlank(msg)){
                    logger.error("valid update Role is error :"+msg);
                    resultMap.put("msg",msg);
                    return resultMap;
                }
                roleService.updateByPrimaryKey(role);
            }
            msg = "操作成功";
        }else{
            msg="信息未填写";
            logger.error(msg);
        }
        resultMap.put("msg",msg);
        return resultMap;
    }
}
