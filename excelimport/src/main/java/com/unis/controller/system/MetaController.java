package com.unis.controller.system;

import java.util.HashMap;
import java.util.List;

import com.unis.logger.TargetLogger;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.Map;

import com.unis.dto.ResultDto;
import com.unis.common.exception.http.RequestEntityTooLargeException;
import com.unis.common.excel.ExportBeanExcel;
import com.unis.common.util.ValidateUtil;

import com.unis.controller.BaseController;
import com.unis.model.system.Meta;
import com.unis.service.system.MetaService;

/**
 * @author xuk
 * @version 1.0
 * @since 2019-03-13
 */
@Controller
@RequestMapping("/system/meta")
public class MetaController extends BaseController {
	private static final Logger logger = LoggerFactory.getLogger(MetaController.class);

    @Autowired
    private MetaService metaService;

    @TargetLogger("059901")
    @RequiresPermissions("SYS_ROLE")
    @RequestMapping("setUpPage")
    public String forwordSetUpPage(Model model){
        Meta meta = null;
        try {
            meta = metaService.queryMetaByPrimaryKey("1");
            model.addAttribute("meta",meta);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "system/meta/page";
    }

    @TargetLogger("059902")
    @RequiresPermissions("SYS_ROLE")
    @RequestMapping("/setUpMeta")
    @ResponseBody
    public Map<String,String> setUpMeta(Meta meta) throws Exception {
        Map<String,String> resultMap = new HashMap<>();
        String msg = null;
        if (meta!=null){
            msg = ValidateUtil.valid(meta);
            if (StringUtils.isNotEmpty(msg)){
                logger.error("valid update Meta is error :"+msg);
                resultMap.put("msg",msg);
                return resultMap;
            }
            meta.setXgsj(metaService.getDbDate());
            metaService.updateByPrimaryKey(meta);
            msg = "操作成功";
        }else{
            msg="信息未填写";
            logger.error(msg);
        }

        resultMap.put("msg",msg);
        return resultMap;
    }
}
