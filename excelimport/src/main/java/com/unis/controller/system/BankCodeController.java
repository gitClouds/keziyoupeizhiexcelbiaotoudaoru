package com.unis.controller.system;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.unis.common.exception.http.RequestEntityTooLargeException;

import com.unis.controller.BaseController;
import com.unis.model.system.BankCode;
import com.unis.service.system.BankCodeService;

/**
 * @author xuk
 * @version 1.0
 * @since 2019-02-14
 */
@Controller
@RequestMapping("/system/bankCode")
public class BankCodeController extends BaseController {
	private static final Logger logger = LoggerFactory.getLogger(BankCodeController.class);

    @Autowired
    private BankCodeService bankCodeService;
    @RequestMapping( "/getBank")
    @ResponseBody
    public BankCode getBankCode(String account){
        BankCode bank = null;
        if (StringUtils.isNotBlank(account)){
            try {
                bank = bankCodeService.queryBankByAccount(account);
            } catch (Exception e) {
                e.printStackTrace();
                logger.error(e.getMessage());
                throw new RequestEntityTooLargeException(e.getMessage());
            }
        }else{
            bank = new BankCode();
        }
        return bank;
    }
    
    /*@RequestMapping("/page")
    public String forwordPage()  {
        return "system/bankCode/bankCodePage";
    }
    
    @RequestMapping( "/list")
    @ResponseBody
    public ResultDto queryBankCodeList(@RequestParam Map<?, ?> paramMap){
    	int pageSize = paramMap.get("pageSize")==null ? 10 : Integer.parseInt(paramMap.get("pageSize").toString());
        int pageNumber = paramMap.get("pageNumber")==null ? 1 : Integer.parseInt(paramMap.get("pageNumber").toString());
        try {
            ResultDto page = bankCodeService.queryListByPage(paramMap,pageNumber,pageSize);
            return page;
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new RequestEntityTooLargeException(e.getMessage());
        }
    }
    
    @RequestMapping("/excel")
    public void exportBankCodeList(HttpServletResponse response,@RequestParam Map<?, ?> paramMap){
        //导出默认1000行
        int pageSize = EXPORT_SIZE;
        int pageNumber = 1;
        OutputStream os = null;
        try {
            ResultDto page = bankCodeService.queryListByPage(paramMap,pageNumber,pageSize);
            List list = page.getRows();

            response.setContentType("application/vnd.ms-excel");
            response.addHeader("Content-Disposition", "attachment;filename=exportBankCode"+ System.currentTimeMillis() + ".xls");
            os = response.getOutputStream();

            ExportBeanExcel<BankCode> export = new ExportBeanExcel<BankCode>();
            //根据需求选择导出字段，注意数据库中带有_等字符字段修改
            //导出表头
            String[] headers = {"注释","注释","注释","注释","注释","注释","注释"};
            //对应字段 headers 与cols一一对应
            String[] cols = {"pk","startaccount","accountlength","bankcode","bankname","isok","yxx"};
			
            export.exportExcel("sheet",headers,cols,list,os);

            os.flush();
            os.close();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new RequestEntityTooLargeException(e.getMessage());
        }
    }
    
    @RequestMapping("/curd")
    @ResponseBody
    public Map<String,String> curd(String curdType,BankCode bankCode) throws Exception {
    	Map<String,String> resultMap = new HashMap<>();
    	String msg = null;
        if (bankCode!=null){
        	if (StringUtils.equalsIgnoreCase(curdType,"insert")){
        		//valid校验需在BankCode的字段上增加校验规则，规则与表单规则保持一致
        		msg = ValidateUtil.valid(bankCode);
        		if (StringUtils.isNotBlank(msg)){
        			logger.error("valid insert BankCode is error :"+msg);
                     resultMap.put("msg",msg);
                     return resultMap;
                 }
                 bankCodeService.insert(bankCode);
            }else if (StringUtils.equalsIgnoreCase(curdType,"delete")){
            	if(StringUtils.isNotBlank(bankCode.getPk())) {
                    bankCodeService.delete(bankCode.getPk()); //主键删除
                }else {
                    msg="没有要删除的数据";
                    logger.error("valid delete BankCode is error :"+msg);
                    resultMap.put("msg",msg);
                    return resultMap;
                }
            }else if (StringUtils.equalsIgnoreCase(curdType,"update")){
            	msg = ValidateUtil.valid(bankCode);
        		if (StringUtils.isNotBlank(msg)){
        			 logger.error("valid update BankCode is error :"+msg);
                     resultMap.put("msg",msg);
                     return resultMap;
                 }
                bankCodeService.updateByPrimaryKey(bankCode);
            }
            msg = "操作成功";
        }else{
            msg="信息未填写";
            logger.error(msg);
        }
    	resultMap.put("msg",msg);
        return resultMap;
    }*/
    
}
