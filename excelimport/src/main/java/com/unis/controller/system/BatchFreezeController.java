package com.unis.controller.system;

import java.util.HashMap;
import java.util.List;

import com.unis.logger.TargetLogger;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.Map;

import com.unis.dto.ResultDto;
import com.unis.common.exception.http.RequestEntityTooLargeException;
import com.unis.common.excel.ExportBeanExcel;
import com.unis.common.util.ValidateUtil;

import com.unis.controller.BaseController;
import com.unis.model.system.BatchFreeze;
import com.unis.service.system.BatchFreezeService;

/**
 * @author xuk
 * @version 1.0
 * @since 2019-12-20
 */
@Controller
@RequestMapping("/system/batchFreeze")
public class BatchFreezeController extends BaseController {
	private static final Logger logger = LoggerFactory.getLogger(BatchFreezeController.class);

    @Autowired
    private BatchFreezeService batchFreezeService;

    @RequiresPermissions("SYS_ROLE")
    @RequestMapping("/page")
    public String forwordPage()  {
        return "system/batchFreeze/batchFreezePage";
    }

    @RequiresPermissions("SYS_ROLE")
    @RequestMapping( "/list")
    @ResponseBody
    public ResultDto queryBatchFreezeList(@RequestParam Map<?, ?> paramMap){
    	int pageSize = paramMap.get("pageSize")==null ? 10 : Integer.parseInt(paramMap.get("pageSize").toString());
        int pageNumber = paramMap.get("pageNumber")==null ? 1 : Integer.parseInt(paramMap.get("pageNumber").toString());
        try {
            ResultDto page = batchFreezeService.queryListByPage(paramMap,pageNumber,pageSize);
            return page;
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new RequestEntityTooLargeException(e.getMessage());
        }
    }

    @RequiresPermissions("SYS_ROLE")
    @RequestMapping("/excel")
    public void exportBatchFreezeList(HttpServletResponse response,@RequestParam Map<?, ?> paramMap){
        //导出默认1000行
        int pageSize = EXPORT_SIZE;
        int pageNumber = 1;
        OutputStream os = null;
        try {
            ResultDto page = batchFreezeService.queryListByPage(paramMap,pageNumber,pageSize);
            List list = page.getRows();

            response.setContentType("application/vnd.ms-excel");
            response.addHeader("Content-Disposition", "attachment;filename=exportBatchFreeze"+ System.currentTimeMillis() + ".xls");
            os = response.getOutputStream();

            ExportBeanExcel<BatchFreeze> export = new ExportBeanExcel<BatchFreeze>();
            //根据需求选择导出字段，注意数据库中带有_等字符字段修改
            //导出表头
            String[] headers = {"主键","类型：0-单位，1-个人","银行或者三方：0-银行，1-三方","注释","单位代码","单位名称","身份证号","注释","入库时间"};
            //对应字段 headers 与cols一一对应
            String[] cols = {"id","type","yhsf","yhxm","dwdm","dwmc","sfzh","yxx","rksj"};
			
            export.exportExcel("批量冻结配置表",headers,cols,list,os);

            os.flush();
            os.close();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new RequestEntityTooLargeException(e.getMessage());
        }
    }

    @RequiresPermissions("SYS_ROLE")
    @RequestMapping("/curd")
    @ResponseBody
    public Map<String,String> curd(String curdType, BatchFreeze batchFreeze) {
    	Map<String,String> resultMap = new HashMap<>();
    	String msg = null;
    	try{
            if (batchFreeze!=null){
                if (StringUtils.equalsIgnoreCase(curdType,"insert")){
                    //valid校验需在BatchFreeze的字段上增加校验规则，规则与表单规则保持一致
                    msg = ValidateUtil.valid(batchFreeze);
                    if (StringUtils.isNotBlank(msg)){
                        logger.error("valid insert BatchFreeze is error :"+msg);
                        resultMap.put("msg",msg);
                        return resultMap;
                    }
                    batchFreezeService.insert(batchFreeze);
                }else if (StringUtils.equalsIgnoreCase(curdType,"delete")){
                    if(StringUtils.isNotBlank(batchFreeze.getId())) {
                        batchFreezeService.delete(batchFreeze.getId()); //主键删除
                    }else {
                        msg="没有要删除的数据";
                        logger.error("valid delete BatchFreeze is error :"+msg);
                        resultMap.put("msg",msg);
                        return resultMap;
                    }
                }else if (StringUtils.equalsIgnoreCase(curdType,"update")){
                    msg = ValidateUtil.valid(batchFreeze);
                    if (StringUtils.isNotBlank(msg)){
                        logger.error("valid update BatchFreeze is error :"+msg);
                        resultMap.put("msg",msg);
                        return resultMap;
                    }
                    batchFreezeService.updateByPrimaryKey(batchFreeze);
                }
                msg = "操作成功";
            }else{
                msg="信息未填写";
                logger.error(msg);
            }
        }catch(Exception e){
    	    msg = e.getMessage();
    	    e.printStackTrace();
        }
    	resultMap.put("msg",msg);
        return resultMap;
    }
    
}
