package com.unis.controller.index;

import com.unis.controller.BaseController;
import com.unis.dto.ResultDto;
import com.unis.model.tool.TbXxInfo;
import com.unis.service.index.IndexService;
import com.unis.service.system.CodeService;
import com.unis.service.tool.TbXxInfoService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Administrator on 2018/12/28/028.
 */
@Controller
@RequestMapping("/index")
public class IndexController extends BaseController {
    @Autowired
    private IndexService indexService;
    @Autowired
    private CodeService codeService;
    @Autowired
    private TbXxInfoService tbXxInfoService;

    @RequestMapping("/indexPage")
    public String loginForm(HttpServletRequest request) throws Exception {
        String forword = "index/indexPage";
        return forword;
    }

    @RequestMapping("/cxtj")
    @ResponseBody
    public Map<String, Object> statisticsQuery() {
        Map map = indexService.statisticsQuery();
        return map;
    }

    @RequestMapping("/zftj")
    @ResponseBody
    public Map<String, Object> statisticsFrozen() {
        return indexService.statisticsFrozen();
    }

    @RequestMapping("/jjdtj")
    @ResponseBody
    public Map<String, Object> statisticsJjd() {
        return indexService.statisticsJjd();
    }

    @RequestMapping("/latj")
    @ResponseBody
    public Map<String, Object> statisticsJjdByAjlb() {
        Map<String, Object> resultMap = new HashMap<>();
        List<Map<String, String>> list = indexService.statisticsJjdByAjlb();
        list = codeService.translateListMap((List) list, "ajlb:010107");
        resultMap.put("list", list);
        return resultMap;
    }

    @RequestMapping("/xxInfo")
    @ResponseBody
    public Map<String, Object> xxInfo() {
        Map<String, Object> resultMap = new HashMap<>();
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("lrrjh", getUserInfo().getJgdm());
        paramMap.put("yxx", "1");
        List<TbXxInfo> list = null;
        try {
            list = tbXxInfoService.queryList(paramMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        resultMap.put("list", list);
        return resultMap;
    }


    @RequiresPermissions("SYS_ROLE")
    @RequestMapping("/xxtj")
    @ResponseBody
    public List<Map<String, String>> statisticsJjd(String rksj) {
        return indexService.statisticsJjd(rksj);
    }
}
