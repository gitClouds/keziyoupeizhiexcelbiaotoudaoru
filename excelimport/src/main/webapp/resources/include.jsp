<%@ page import="org.apache.shiro.SecurityUtils" %>
<%@ page import="org.apache.shiro.subject.Subject" %>
<%@ page import="com.unis.common.secure.authc.UserInfo" %>
<%@ page session="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
    String context = request.getContextPath();
    String scheme = request.getScheme();
    String host = request.getServerName();
    String basePath = scheme + "://" + host + ":" + request.getServerPort() + context + "/";
    String sslBasePath = "https://" + host + ":8443" + context;
    pageContext.setAttribute("ctx", context);
    pageContext.setAttribute("basePath", basePath);
    pageContext.setAttribute("sslBasePath", sslBasePath);
    pageContext.setAttribute("resource", context + "/resources");
    pageContext.setAttribute("plugin", context + "/jslib/plugins");
    Subject currentUser = SecurityUtils.getSubject();
    UserInfo userInfo = (UserInfo) currentUser.getPrincipal();
    pageContext.setAttribute("info", userInfo);
%>
