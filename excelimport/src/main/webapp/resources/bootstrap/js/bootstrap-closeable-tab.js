//子页面不用iframe，用div展示
var closableTab = {

    //添加tab
    addTab:function(tabItem){ //tabItem = {id,name,url,closable}

        var id = "tab_seed_" + tabItem.id;
        var container ="tab_container_" + tabItem.id;

        $("li[id^=tab_seed_]").removeClass("active");
        $("div[id^=tab_container_]").removeClass("active");

        if(!$('#'+id)[0]){
            var li_tab = '<li role="presentation" class="" id="'+id+'" style="height: 40px;line-height: 40px;"><a href="#'+container+'"  role="tab" data-toggle="tab" style="position: relative;padding:2px 23px 2px 15px;height: 40px;line-height: 40px;">'+tabItem.name;
            if(tabItem.closable){
                li_tab = li_tab + '<i class="glyphicon glyphicon-remove small" tabclose="'+id+'" style="position: absolute;right:6px;top: 6px;cursor:pointer;"  onclick="closableTab.closeTab(this)"></i></a></li> ';
            }else{
                li_tab = li_tab + '</a></li>';
            }

            var tabpanel = '<div role="tabpanel" class="tab-pane" id="'+container+'" style="width: 100%;">'+
                '正在加载...'+
                '</div>';


            $('.nav-tabs').append(li_tab);
            $('.tab-content').append(tabpanel);
            $('#'+container).load(tabItem.url,function(response,status,xhr){
                if(status=='error'){//status的值为success和error，如果error则显示一个错误页面
                    $(this).html(response);
                }
            });
        }
        $("#"+id).addClass("active");
        $("#"+container).addClass("active");
    },

    //关闭tab
    closeTab:function(item){
        var val = $(item).attr('tabclose');
        var containerId = "tab_container_"+val.substring(9);

        if($('#'+containerId).hasClass('active')){
            $('#'+val).prev().addClass('active');
            $('#'+containerId).prev().addClass('active');
        }


        $("#"+val).remove();
        $("#"+containerId).remove();
    }
}

var iframeTab = {
    addIframe:function (tabItem) {
        //{'id':'homePage','name':'首页','url':ctx+'/index/indexPage'}
        var id = "ifTab_seed_" + tabItem.id;//li id
        var iframeCont ="ifTab_container_" + tabItem.id;//iframe id
        var iId="ifTab_close_" + tabItem.id;//i标签关闭按钮ID
        $("li[id^=ifTab_seed_]").removeClass("active");//ifTab_seed_开头的所有元素去除active
        $("iframe[id^=ifTab_container_]").hide();//ifTab_container_开头的所有元素隐藏
        if(!$('#'+id)[0]){//没有此元素时增加
            //增加tab
            var li_tab = '<li id="'+id+'" ifrmId="'+iframeCont+'" onclick="iframeTab.tabClick(this);" style="height: 40px;line-height: 40px;"><a href="#" style="position: relative;padding:2px 23px 2px 15px;height: 40px;line-height: 40px;">'+tabItem.name+'<i id="'+iId+'" class="glyphicon glyphicon-remove small" style="position: absolute;right:6px;top: 6px;cursor:pointer;" tabClose="'+id+'" ifrmClose="'+iframeCont+'" onclick="iframeTab.closeIframe(event,this)"></i></a></li>';
            $('#ifTab_list').append(li_tab);
            //增加iframea
            var iframe_pro = '<iframe src="'+tabItem.url+'" style="display: block;" id="'+iframeCont+'" scrolling="auto" frameborder="0"></iframe>';
            $('#ifTab_center').append(iframe_pro);
        }
        //当前元素显示
        $("#"+id).addClass("active");
        $("#"+iframeCont).show();

        var tp_h = $(".navbar").height();
        var body_h = $(window).height();
        var tabs_h = $(".nav-tabs").height();
        $(".content-wrapper").css("height",body_h - tp_h);
        $(".tabs-cent").css("height",body_h - tp_h - tabs_h);
        $("#"+iframeCont).css("height",body_h - tp_h - tabs_h);
    },
    parentAddIframe:function (tabItem) {
        //{'id':'homePage','name':'首页','url':ctx+'/index/indexPage'}
        var id = "ifTab_seed_" + tabItem.id;//li id
        var iframeCont ="ifTab_container_" + tabItem.id;//iframe id
        var iId="ifTab_close_" + tabItem.id;//i标签关闭按钮ID
        window.parent.$("li[id^=ifTab_seed_]").removeClass("active");//ifTab_seed_开头的所有元素去除active
        window.parent.$("iframe[id^=ifTab_container_]").hide();//ifTab_container_开头的所有元素隐藏
        if(window.parent.$('#'+id)[0]){//此元素存在时先关闭
            window.parent.$('#'+iId).click();
        }
        if(! window.parent.$('#'+id)[0]){//没有此元素时增加
            //增加tab
            var li_tab = '<li id="'+id+'" ifrmId="'+iframeCont+'" onclick="iframeTab.tabClick(this);" style="height: 40px;line-height: 40px;"><a href="#" style="position: relative;padding:2px 23px 2px 15px;height: 40px;line-height: 40px;">'+tabItem.name+'<i id="'+iId+'" class="glyphicon glyphicon-remove small" style="position: absolute;right:6px;top: 6px;cursor:pointer;" tabClose="'+id+'" ifrmClose="'+iframeCont+'" onclick="iframeTab.closeIframe(event,this)"></i></a></li>';
            window.parent.$('#ifTab_list').append(li_tab);
            //增加iframe
            var iframe_pro = '<iframe src="'+tabItem.url+'" style="display: block;" id="'+iframeCont+'" scrolling="auto"  frameborder="0"></iframe>';
            window.parent.$('#ifTab_center').append(iframe_pro);
        }
        //当前元素显示
        window.parent.$("#"+id).addClass("active");
        window.parent.$("#"+iframeCont).show();

        var tp_h = window.parent.$(".navbar").height();
        var body_h = $(window.parent).height();
        var tabs_h = window.parent.$(".nav-tabs").height();
        window.parent.$(".content-wrapper").css("height",body_h - tp_h);
        window.parent.$(".tabs-cent").css("height",body_h - tp_h - tabs_h);
        window.parent.$("#"+iframeCont).css("height",body_h - tp_h - tabs_h);
    },
    parentTwoAddIframe:function (tabItem) {
        //{'id':'homePage','name':'首页','url':ctx+'/index/indexPage'}
        var id = "ifTab_seed_" + tabItem.id;//li id
        var iframeCont ="ifTab_container_" + tabItem.id;//iframe id
        var iId="ifTab_close_" + tabItem.id;//i标签关闭按钮ID
        window.parent.parent.$("li[id^=ifTab_seed_]").removeClass("active");//ifTab_seed_开头的所有元素去除active
        window.parent.parent.$("iframe[id^=ifTab_container_]").hide();//ifTab_container_开头的所有元素隐藏
        if(window.parent.parent.$('#'+id)[0]){//此元素存在时先关闭
            window.parent.parent.$('#'+iId).click();
        }
        if(! window.parent.parent.$('#'+id)[0]){//没有此元素时增加
            //增加tab
            var li_tab = '<li id="'+id+'" ifrmId="'+iframeCont+'" onclick="iframeTab.tabClick(this);" style="height: 40px;line-height: 40px;"><a href="#" style="position: relative;padding:2px 23px 2px 15px;height: 40px;line-height: 40px;">'+tabItem.name+'<i id="'+iId+'" class="glyphicon glyphicon-remove small" style="position: absolute;right:6px;top: 6px;cursor:pointer;" tabClose="'+id+'" ifrmClose="'+iframeCont+'" onclick="iframeTab.closeIframe(event,this)"></i></a></li>';
            window.parent.parent.$('#ifTab_list').append(li_tab);
            //增加iframe
            var iframe_pro = '<iframe src="'+tabItem.url+'" style="display: block;" id="'+iframeCont+'" scrolling="auto"  frameborder="0"></iframe>';
            window.parent.parent.$('#ifTab_center').append(iframe_pro);
        }
        //当前元素显示
        window.parent.parent.$("#"+id).addClass("active");
        window.parent.parent.$("#"+iframeCont).show();

        var tp_h = window.parent.parent.$(".navbar").height();
        var body_h = $(window.parent.parent).height();
        var tabs_h = window.parent.parent.$(".nav-tabs").height();
        window.parent.parent.$(".content-wrapper").css("height",body_h - tp_h);
        window.parent.parent.$(".tabs-cent").css("height",body_h - tp_h - tabs_h);
        window.parent.parent.$("#"+iframeCont).css("height",body_h - tp_h - tabs_h);
    },
    closeIframe:function (event,obj) {
        var evt = event ? event : (window.event ? window.event : null);
        evt.stopPropagation();
        var id = $(obj).attr('tabclose');
        var iframeCont = $(obj).attr('ifrmClose');

        if($('#'+id).hasClass('active')){
            $('#'+iframeCont).prev().show();
            $('#'+id).prev().addClass("active");
        }
        $("#"+id).remove();
        $("#"+iframeCont).remove();

    },
    tabClick:function (obj) {
        var id = $(obj).attr('id');
        var ifrmId = $(obj).attr('ifrmId');

        if($('#'+id).hasClass('active')){

        }else{
            $("li[id^=ifTab_seed_]").removeClass("active");//ifTab_seed_开头的所有元素去除active
            $("iframe[id^=ifTab_container_]").hide();//ifTab_container_开头的所有元素隐藏
            $('#'+id).addClass('active');
            $('#'+ifrmId).show();
        }
    },
    changeFrameHeight:function () {
        
    }
}


//初始化高度
function change_iframe_height() {

}