var fileUpload = {
    initImgInput:function (eff,ctrlName,subUrl) {
        var data = {"eff":eff};
        var control = $('#' + ctrlName);
        control.fileinput({
            maxFilesNum : 5,//上传最大的文件数量
            language: 'zh', //设置语言
            uploadUrl: subUrl, //上传的地址
            uploadExtraData:data,//上传文件时传递的额外参数
            allowedFileExtensions: ['jpg', 'jpeg', 'gif', 'zip', 'rar', 'png','doc','docx','pdf','ppt','pptx','txt'],//接收的文件后缀
            showUpload: false, //是否在选择按钮旁显示上传按钮
            showCaption: false,//是否显示 input中选中文件名称
            showRemove:true,//是否在选择按钮旁显示移除按钮
            browseClass: "btn btn-primary", //按钮样式
            dropZoneTitle:"拖拽文件到这里",//预览区域初始化显示文字
            browseOnZoneClick:true,//点击拖拽区域与点击选择按钮是同样效果
            //showBrowse:true,//是否显示选择按钮
            autoReplace:false,//覆盖上一个文件
            uploadAsync:false,//是否异步上传
            layoutTemplates :{
                actionDelete:'', //去除上传预览的缩略图中的 【删除】图标
                actionUpload:'',//去除上传预览缩略图中的 【上传】 图片；
                //actionZoom:''   //去除上传预览缩略图中的查看 【详情】 图标。
            },
            maxFileSize: 0,//单位为kb，如果为0表示不限制文件大小
            // dropZoneEnabled: true,//是否显示拖拽区域
            //minImageWidth: 50, //图片的最小宽度
            //minImageHeight: 50,//图片的最小高度
            //maxImageWidth: 1000,//图片的最大宽度
            //maxImageHeight: 1000,//图片的最大高度
            //minFileCount: 1,//表示允许同时上传的最小文件个数
            // maxFileCount: 3, //表示允许同时上传的最大文件个数,如果要上传多个，请在file元素上增加 multiple 属性
            enctype: 'multipart/form-data',
            validateInitialCount:true,
            previewFileIcon: "<i class='glyphicon glyphicon-file'></i>",
            // msgFilesTooMany: "选择上传的文件数量({n}) 超过允许的最大数值{m}！",
        }).on('fileerror', function(event, data, msg) {  //异步上传失败
            console.log('文件上传失败！'+data.id);
        }).on("filebatchuploadsuccess", function (event, data, previewId, index) {//同步上传成功
            setMongoPk(data);
        });
    },
    initPngInput:function (eff,ctrlName,subUrl) {
        var data = {"eff":eff};
        var control = $('#' + ctrlName);
        control.fileinput({
            maxFilesNum : 5,//上传最大的文件数量
            language: 'zh', //设置语言
            uploadUrl: subUrl, //上传的地址
            uploadExtraData:data,//上传文件时传递的额外参数
            allowedFileExtensions: ['jpg', 'jpeg', 'gif', 'zip', 'rar', 'png','doc','docx','pdf','ppt','pptx','txt'],//接收的文件后缀
            showUpload: false, //是否在选择按钮旁显示上传按钮
            showCaption: false,//是否显示 input中选中文件名称
            showRemove:true,//是否在选择按钮旁显示移除按钮
            browseClass: "btn btn-primary", //按钮样式
            dropZoneTitle:"拖拽文件到这里",//预览区域初始化显示文字
            browseOnZoneClick:true,//点击拖拽区域与点击选择按钮是同样效果
            //showBrowse:true,//是否显示选择按钮
            autoReplace:false,//覆盖上一个文件
            uploadAsync:false,//是否异步上传
            layoutTemplates :{
                actionDelete:'', //去除上传预览的缩略图中的 【删除】图标
                actionUpload:'',//去除上传预览缩略图中的 【上传】 图片；
                //actionZoom:''   //去除上传预览缩略图中的查看 【详情】 图标。
            },
            maxFileSize: 0,//单位为kb，如果为0表示不限制文件大小
            dropZoneEnabled: true,//是否显示拖拽区域
            //minImageWidth: 50, //图片的最小宽度
            //minImageHeight: 50,//图片的最小高度
            //maxImageWidth: 1000,//图片的最大宽度
            //maxImageHeight: 1000,//图片的最大高度
            //minFileCount: 1,//表示允许同时上传的最小文件个数
            // maxFileCount: 3, //表示允许同时上传的最大文件个数,如果要上传多个，请在file元素上增加 multiple 属性
            enctype: 'multipart/form-data',
            validateInitialCount:true,
            previewFileIcon: "<i class='glyphicon glyphicon-file'></i>",
            // msgFilesTooMany: "选择上传的文件数量({n}) 超过允许的最大数值{m}！",
        }).on('fileerror', function(event, data, msg) {  //异步上传失败
            console.log('文件上传失败！'+data.id);
        }).on("filebatchuploadsuccess", function (event, data, previewId, index) {//同步上传成功
            setMongoPk(data);
        });
    },
    showImgs:function (urls,ctrlName) {
        var btns = '<button type="button" class="btn btn-xs btn-default" onclick="fileUpload.fileDownLoad(this)" title="下载附件" data-key="{data}"><i class="fa fa-download"></i> </button>';
        $("#"+ctrlName).fileinput({
            'theme': 'explorer-fas',
            'uploadUrl': '#',
            layoutTemplates :{
                actionDelete:'', //去除上传预览的缩略图中的 【删除】图标
                actionUpload:'',//去除上传预览缩略图中的 【上传】 图片；
                //actionZoom:''   //去除上传预览缩略图中的查看 【详情】 图标。
            },
            language: 'zh', //设置语言
            otherActionButtons:btns,
            //browseClass:"hide",
            showBrowse:false,
            showUpload: false, //是否在选择按钮旁显示上传按钮
            showCaption: false,//是否显示 input中选中文件名称
            showRemove:false,//是否在选择按钮旁显示移除按钮
            overwriteInitial: false,
            initialPreviewAsData: true,
            initialPreview: urls
        });
    },
    editImgs:function (eff,ctrlName,subUrl,urls) {
        var btns = '<button type="button" class="btn btn-xs btn-default" onclick="fileUpload.fileDownLoad(this)" title="下载附件" data-key="{data}"><i class="fa fa-download"></i> </button>';
        var data = {"eff":eff};
        var control = $('#' + ctrlName);
        control.fileinput({
            language: 'zh', //设置语言
            uploadUrl: subUrl, //上传的地址
            uploadExtraData:data,//上传文件时传递的额外参数
            allowedFileExtensions: ['jpg', 'jpeg'],//接收的文件后缀
            showUpload: false, //是否在选择按钮旁显示上传按钮
            showCaption: false,//是否显示 input中选中文件名称
            showRemove:false,//是否在选择按钮旁显示移除按钮
            browseClass: "btn btn-primary", //按钮样式
            dropZoneTitle:"拖拽文件到这里",//预览区域初始化显示文字
            browseOnZoneClick:true,//点击拖拽区域与点击选择按钮是同样效果
            showBrowse:false,//是否显示选择按钮
            autoReplace:true,//覆盖上一个文件
            uploadAsync:false,//是否异步上传
            layoutTemplates :{
                actionDelete:'', //去除上传预览的缩略图中的 【删除】图标
                actionUpload:'',//去除上传预览缩略图中的 【上传】 图片；
                //actionZoom:''   //去除上传预览缩略图中的查看 【详情】 图标。
            },

            maxFileSize: 600,//单位为kb，如果为0表示不限制文件大小
            //dropZoneEnabled: false,//是否显示拖拽区域
            //minImageWidth: 50, //图片的最小宽度
            //minImageHeight: 50,//图片的最小高度
            //maxImageWidth: 1000,//图片的最大宽度
            //maxImageHeight: 1000,//图片的最大高度
            //minFileCount: 1,//表示允许同时上传的最小文件个数
            maxFileCount: 1, //表示允许同时上传的最大文件个数,如果要上传多个，请在file元素上增加 multiple 属性
            enctype: 'multipart/form-data',
            validateInitialCount:true,
            previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
            otherActionButtons:btns,
            initialPreviewAsData: true,
            initialPreview: urls
        }).on('fileerror', function(event, data, msg) {  //异步上传失败
            console.log('文件上传失败！'+data.id);
        }).on("filebatchuploadsuccess", function (event, data, previewId, index) {//同步上传成功
            setMongoPk(data);
        });
    },
    fileDownLoad:function (obj) {
        var $btn = $(obj),key = $btn.data('key');
        window.open(key);
    }

}