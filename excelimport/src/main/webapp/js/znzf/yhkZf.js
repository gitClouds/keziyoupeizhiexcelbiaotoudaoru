/**
 * Created by Administrator on 2019/2/20/020.
 */
$(function () {
    checkDzz("ifTab_close_addyhkZf");//验证调证字、经办人
    //getJjdZh("yhkZf",$("#xxType").val(),$("#jjdPk").val());//获取接警单相关的账号信息
    setDefaultData(0,0);//设置默认值
    formEditValidate('yhkZfQqEditForm');//表单验证
})

var setJjdZhToJsp = function (data) {
    if (data && data.length>0){
        for(var index =0;index<data.length;index++){
            if (index == 0){
                $("#bean_zhxm_0").val(data[index].zhxm==null?"":data[index].zhxm);
                $("#bean_zh_0").val(data[index].zh);
                $("#bean_zhjgdm_0").val(data[index].zhjgdm);
                $("#bean_zhjgmc_0").val(data[index].zhjgmc);
                //$("#bean_zhlx_0").val(data[index].zhlx);
                $("#bean_zcsj_0").val(data[index].zcsj);
                $("#bean_zcje_0").val(data[index].zcje);
                $("#bean_zzfs_0").val(data[index].zzfs);
                setDefaultData(0,0);
            }else{
                addZnzfYhkZf();
                $("#bean_zhxm_"+index).val(data[index].zhxm==null?"":data[index].zhxm);
                $("#bean_zh_"+index).val(data[index].zh);
                $("#bean_zhjgdm_"+index).val(data[index].zhjgdm);
                $("#bean_zhjgmc_"+index).val(data[index].zhjgmc);
                //$("#bean_zhlx_"+index).val(data[index].zhlx);
                $("#bean_zcsj_"+index).val(data[index].zcsj);
                $("#bean_zcje_"+index).val(data[index].zcje);
                $("#bean_zzfs_"+index).val(data[index].zzfs);
                setDefaultData(index-1,index);
            }
        }
    }
}

var addZnzfYhkZf = function () {
    var oldBeanNum = $(".beanData:last").attr("data-bean-num");
    addZnzfBean();
    setDefaultData(oldBeanNum,beanNum);
    updateFlwsParm(oldBeanNum,beanNum);
}

var setDefaultData = function (oldBeanNum,index) {
    $('#bean_zhlb_'+oldBeanNum).prop("checked","checked");
    //$('#bean_zhlx_'+(index)).val("银行卡");
}

function showFlwsFile(index) {
    var zhxm = $("#bean_zhxm_"+index).val();
    var zzfs = "银行卡";
    var zh = $("#bean_zh_"+index).val();
    var zhjgmc = $("#bean_zhjgmc_"+index).val();
    var zzje = $("#bean_zcje_"+index).val();
    var zzsj = $("#bean_zcsj_"+index).val();
    var zzsjDate = new Date(Date.parse(zzsj.replace(/-/g, "/")));
    zzsj = zzsjDate.Format('yyyyMMddhhmmss');
    var ajlb = $("#ajlb").val();
    if (zhjgmc && zhjgmc!=''){
    }else{
        alert("请选择所属银行");
        return false;
    }
    if (zh && zh!=''){
    }else{
        alert("请输入账号");
        return false;
    }

    var data={"index":index,"jgmc":zhjgmc,"zhxm":zhxm,"zzfs":zzfs,"zh":zh,"zzje":zzje,"ajlb":ajlb,"zzsj":zzsj};
    showZfFlws(data);
}

var yhkZfQqEditSubmit = function(btnObj){
    var flag = $('#yhkZfQqEditForm').validate('submitValidate');
    if (flag){
        //防止重复点击
        //$(btnObj).attr('disabled','disabled');
        var reqData = $('#yhkZfQqEditForm').serializeJsonObject();
        //console.log(reqData);
        //debugger
        reqData = setListData(reqData);
        //console.log(reqData);
        reqData = JSON.stringify(reqData);
        $.ajax({
            type: "POST",
            dataType: "json",
            contentType: 'application/json; charset=UTF-8',
            url: ctx+"/znzf/yhkZf/add" ,
            data: reqData,
            success : function(data) {
                if(data){
                    if(data.msg){
                        if(data.msg=='true'){
                            alert("操作成功");
                            window.parent.$("#ifTab_close_addyhkZf").click();
                        }else{
                            alert(data.msg);
                        }
                    }else{
                        alert("请求异常！");
                    }
                }
            },
            error : function() {
                alert("请求异常");
            }
        });
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}