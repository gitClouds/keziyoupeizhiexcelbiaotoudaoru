/**
 * Created by Administrator on 2019/3/28/028.
 */
$(function () {
    initSfJgActGrid();
})

function initSfJgActGrid(){
    $('#sfJgActGrid').bootstrapTable('destroy');

    $("#sfJgActGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/znzf/sfJgAct/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: querySfJgActParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 480,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
        showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
            {
                title : "序号",
                field : "Number",
                align : "center",
                width : "50px",
                formatter: function (value, row, index) {
                    var pageSize = $('#sfJgActGrid').bootstrapTable('getOptions').pageSize;     //通过table的#id 得到每页多少条
                    var pageNumber = $('#sfJgActGrid').bootstrapTable('getOptions').pageNumber; //通过table的#id 得到当前第几页
                    return pageSize * (pageNumber - 1) + index + 1;    // 返回每条的序号： 每页条数 *（当前页 - 1 ）+ 序号
                }
            },
            {
                title : "支付帐号",
                field : "zfzh",
                align : "center"
            },
            {
                title : "帐号类别",
                field : "zhlb",
                align : "center",
                visible: false,
                formatter:function (value, row, index) {
                    if (value=='01'){
                        return '个人';
                    }else if (value=='02'){
                        return '商户';
                    }
                }
            },
            {
                title : "姓名",
                field : "khxm",
                align : "center"
            },
            /*{
                title : "证件类型",
                field : "khzjlx",
                align : "center",
                visible: false
            },*/
            {
                title : "证件号",
                field : "khzjhm",
                align : "center"
            },
            {
                title : "证照有效期",
                field : "zjyxq",
                align : "center",
                visible: false
            },
            {
                title : "绑定手机号",
                field : "bdsjh",
                align : "center",
                visible: false
            },
            {
                title : "登录号",
                field : "dlhm",
                align : "center",
                visible: false
            },
            {
                title : "绑定微信号",
                field : "wx",
                align : "center"
            },
            {
                title : "绑定QQ号",
                field : "qq",
                align : "center",
                visible: false
            },
            {
                title : "IP地址",
                field : "ip",
                align : "center",
                visible: false
            },
            {
                title : "设备号",
                field : "sbhm",
                align : "center",
                visible: false
            },
            {
                title : "商户名称",
                field : "shmc",
                align : "center",
                visible: false
            },
            {
                title : "商户号",
                field : "shhm",
                align : "center",
                visible: false
            },
            {
                title : "POS机编号",
                field : "posbh",
                align : "center",
                visible: false
            },
            {
                title : "POS机注册地址",
                field : "posdz",
                align : "center",
                visible: false
            },
            {
                title : "POS机常用地址",
                field : "posdwdz",
                align : "center",
                visible: false
            }
            /*,
            {
                title:"操作",
                field:"pk",
                align:"center",
                valign : "middle",
                width : "120px",
                formatter : sfJgActOperationFormatter
            }*/
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}
function querySfJgActParams(params){
    var tmp = $("#testQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    tmp["qqpk"]=$("#pk").val();
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}

function exportSfJgAct(){
    var url = ctx+"/znzf/sfJgAct/excel?qqpk="+$("#pk").val();
    window.open(url);
}

function sfJgActOperationFormatter(value, row, index) {
    var result = "";
    result+='<span id="zfzhBtn" class="btn-sm btn-info" onclick="actCard(\''+row.pk+'\');">银行卡</span>';
    result+='&nbsp;<span id="csBtn" class="btn-sm btn-info" onclick="actMeta(\''+row.pk+'\');">措施</span>';
    return result;
}

function actCard(pk) {
    $("#showModalLabel").text("银行卡");
    $("#actShowIframe").attr("src",ctx+"/znzf/sfJgCard/pageFromAct?actpk="+pk);
    $("#showModal").modal();
}

function actMeta(pk) {
    $("#showModalLabel").text("措施");
    $("#actShowIframe").attr("src",ctx+"/znzf/sfJgMea/page?actpk="+pk);
    $("#showModal").modal();
}