/**
 * Created by Administrator on 2019/2/12/012.
 */
$(function () {
    checkDzz("ifTab_close_addyhkMx");//验证调证字、经办人
    var iszcygzs = $("#iszcygzs").val();
    if (iszcygzs >= 1) {
    } else {
        getJjdZh("yhkMx", $("#xxType").val(), $("#jjdPk").val());//获取接警单相关的账号信息
    }
    setDefaultData(0, 0);//设置默认值
    formEditValidate('yhkMxQqEditForm');//表单验证
})

var yhkMxQqEditSubmit = function (btnObj) {
    var flag = $('#yhkMxQqEditForm').validate('submitValidate');
    if (flag) {
        showLoading();
        var reqData = $('#yhkMxQqEditForm').serializeJsonObject();
        reqData = setListData(reqData);
        reqData = JSON.stringify(reqData);
        $.ajax({
            type: "POST",
            dataType: "json",
            contentType: 'application/json; charset=UTF-8',
            url: ctx + "/znzf/yhkMx/add",
            data: reqData,
            success: function (data) {
                hideLoading();
                if (data) {
                    if (data.msg) {
                        if (data.msg == 'true') {
                            alert("操作成功");
                            window.parent.document.getElementById('ifTab_container_zhxxlr').contentWindow.location.reload(true);
                            window.parent.$("#ifTab_close_addyhkMx").click();
                        } else {
                            alert(data.msg);
                        }
                    } else {
                        alert("请求异常！");
                    }
                }
            },
            error: function () {
                hideLoading();
                alert("请求异常");
            }
        });
    } else {
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}

var setJjdZhToJsp = function (data) {
    if (data && data.length > 0) {
        for (var index = 0; index < data.length; index++) {
            if (index == 0) {
                $("#bean_zhxm_0").val(data[index].zhxm == null ? "不详" : data[index].zhxm);
                $("#bean_zh_0").val(data[index].zh);
                $("#jjdPk_0").val(data[index].jjdpk);
//                $("#bean_zhlx_0").val(data[index].zhlx);
                $("#bean_zhlb_0").val(data[index].zhlb);
                $("#bean_zzsj_0").val(data[index].zcsj);
                $("#bean_zhjgdm_0").val(data[index].zhjgdm);
                $("#bean_zhjgmc_0").val(data[index].zhjgmc);
                $("#bean_cxnr_0").val(data[index].cxnr);
                setDefaultData(0, 0);
            } else {
                addZnzfYhkMx();
                $("#bean_zhxm_" + index).val(data[index].zhxm == null ? "不详" : data[index].zhxm);
                $("#bean_zh_" + index).val(data[index].zh);
                $("#jjdPk_" + index).val(data[index].jjdpk);
                $("#bean_zhjgdm_" + index).val(data[index].zhjgdm);
                $("#bean_zhjgmc_" + index).val(data[index].zhjgmc);
//                $("#bean_zhlx_"+index).val(data[index].zhlx);
                $("#bean_zhlb_" + index).val(data[index].zhlb);
                $("#bean_cxnr_" + index).val(data[index].cxnr);
                $("#bean_zzsj_" + index).val(data[index].zcsj);
                setDefaultData(index - 1, index);
            }
        }
        $("[id^=bean_zh_]").each(function () {
            getBank(this);
        })
    }
}
var setDefaultData = function (oldBeanNum, index) {
    var zzsj = $('#bean_zzsj_' + index).val();
    if (zzsj) {
        var zzsjDate = new Date(Date.parse(zzsj.replace(/-/g, "/")));
        zzsj = zzsjDate.Format("yyyy-MM-dd");
        var nDate = new Date();
        var endDate = nDate.Format("yyyy-MM-dd");
        var centDate = nDate.addDayNumByDate(-15, 'yyyy-MM-dd');
        if (zzsj < centDate) {
            endDate = zzsjDate.addDayNumByDate(15, 'yyyy-MM-dd');
        }
        $('#bean_endDate_' + index).val(endDate);
        var beginDate = new Date(Date.parse(endDate.replace(/-/g, "/"))).addDayNumByDate(-28, 'yyyy-MM-dd');
        $('#bean_startDate_' + index).val(beginDate);

    }

//    $('#bean_zhlb_'+oldBeanNum).prop("checked","checked");
//    $('#bean_cxnr_'+oldBeanNum).prop("checked","checked");

//    $('#bean_zhlx_'+(index)).val("银行卡");
}

var addZnzfYhkMx = function () {
    var oldBeanNum = $(".beanData:last").attr("data-bean-num");
    addZnzfBean();
    setDefaultData(oldBeanNum, beanNum);
    updateFlwsParm(oldBeanNum, beanNum);
}

function showFlwsFile(index) {
    var zhxm = $("#bean_zhxm_" + index).val();
    var zhlx = $("#bean_zhlx_" + index).val();
    var zh = $("#bean_zh_" + index).val();
    var zhjgmc = $("#bean_zhjgmc_" + index).val();
    if (zhjgmc && zhjgmc != '') {
    } else {
        alert("请选择所属银行");
        return false;
    }
    if (zh && zh != '') {
    } else {
        alert("请输入账号");
        return false;
    }
    var data = {"index": index, "jgmc": zhjgmc, "zhxm": zhxm, "zhlx": zhlx, "zh": zh}
    showCxFlws(data);
}
