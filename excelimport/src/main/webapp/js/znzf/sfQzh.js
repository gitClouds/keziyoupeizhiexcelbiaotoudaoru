/**
 * Created by Administrator on 2019/3/5/005.
 */
$(function () {
    checkDzz("ifTab_close_addsfQzh");//验证调证字、经办人
    var iszcygzs = $("#iszcygzs").val();
    if (iszcygzs>=1){}else {
        getJjdZh($("#fwType").val(), $("#xxType").val(), $("#jjdPk").val());//获取接警单相关的账号信息
    }
    setDefaultData(0,0);//设置默认值
    formEditValidate('sfQzhQqEditForm');//表单验证
})

var setDefaultData = function (oldBeanNum,index) {
    $('#bean_subjecttype_'+oldBeanNum).prop("checked","checked");
}

var showZhlb = function (obj) {
    var _obj = $(obj);
    var objId = _obj.attr("id");
    var index = objId.charAt(objId.length-1);
    var acctype = $("#bean_acctype_"+index).val();

    if(_obj && _obj.val()!="" && acctype && acctype=='02'){
        $("#div_accountype_"+index).removeClass("hide");
        $("#bean_accountype_"+index).val("01");
    }else{
        $("#bean_accountype_"+index).val("");
        if(!$("#div_accountype_"+index).hasClass("hide")){
            $("#div_accountype_"+index).addClass("hide");
        }
    }
}

var addZnzfsfQzh = function () {
    var oldBeanNum = $(".beanData:last").attr("data-bean-num");
    addZnzfBean();
    setDefaultData(oldBeanNum,beanNum);
    updateFlwsParm(oldBeanNum,beanNum);
    updateAccType(oldBeanNum,beanNum);
    showZhlb($("#bean_acctypeName_"+beanNum)[0]);
}

function showFlwsFile(index) {
    var zhlx = $("#bean_zzfs_"+index).val();
    var zh = $("#bean_accnumber_"+index).val();
    var jgmc = $("#bean_payname_"+index).val();
    if (jgmc && jgmc!=''){
    }else{
        alert("请选择所属机构");
        return false;
    }
    if (zh && zh!=''){
    }else{
        alert("请输入账号");
        return false;
    }
    if (zhlx && zhlx!=''){
    }else{
        zhlx = '支付账号';
    }

    var data = {"index":index,"jgmc":jgmc,"zhxm":$("#bean_accountname_"+index).val(),"zhlx":zhlx,"zh":zh};
    showCxFlws(data);
}

var updateAccType = function(oldBeanNum,index){
    var reg= new RegExp(oldBeanNum,'g');
    var div_accountype = $("#bean_accountype_"+index).parent().parent();
    var div_accountype_id = div_accountype.attr("id");
    div_accountype_id = div_accountype_id.replace(reg,index);
    div_accountype.attr("id",div_accountype_id);
}

var setJjdZhToJsp = function (data) {
    if (data && data.length>0){
        for(var index =0;index<data.length;index++){
            if (index == 0){
                $("#bean_zzfs_0").val(data[index].zhlx);//bean_acctype_0  bean_acctypeName_0

                $("#bean_accnumber_0").val(data[index].zh);
                $("#bean_paycode_0").val(data[index].zhjgdm);
                $("#bean_payname_0").val(data[index].zhjgmc);

                $("#bean_acctype_0").val(data[index].acctype);
                $("#bean_acctypeName_0").val(data[index].acctypeName);
                $("#bean_zzsj_0").val(data[index].zcsj);
                $("#bean_accountname_0").val(data[index].accountname);
                setDefaultData(0,0);
            }else{
                addZnzfsfQzh();
                $("#bean_zzfs_"+index).val(data[index].zhlx);

                $("#bean_accnumber_"+index).val(data[index].zh);
                $("#bean_paycode_"+index).val(data[index].zhjgdm);
                $("#bean_payname_"+index).val(data[index].zhjgmc);

                $("#bean_acctype_"+index).val(data[index].acctype);
                $("#bean_acctypeName_"+index).val(data[index].acctypeName);
                $("#bean_zzsj_"+index).val(data[index].zcsj);
                $("#bean_accountname_"+index).val(data[index].accountname);
                setDefaultData(index-1,index);
            }
            showZhlb($("#bean_acctypeName_"+index)[0]);
        }
    }
}

var sfQzhQqEditSubmit = function (btnObj) {
    var flag = $('#sfQzhQqEditForm').validate('submitValidate');
    if (flag){
        flag = checkForm();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
    if (flag){
        showLoading();
        var reqData = $('#sfQzhQqEditForm').serializeJsonObject();
        reqData = setListData(reqData);
        reqData = JSON.stringify(reqData);
        $.ajax({
            type: "POST",
            dataType: "json",
            contentType: 'application/json; charset=UTF-8',
            url: ctx+"/znzf/sfQzh/add" ,
            data: reqData,
            success : function(data) {
                hideLoading();
                if(data){
                    if(data.msg){
                        if(data.msg=='true'){
                            alert("操作成功");
                            window.parent.$("#ifTab_close_addsfQzh").click();
                        }else{
                            alert(data.msg);
                        }
                    }else{
                        alert("请求异常！");
                    }
                }
            },
            error : function() {
                hideLoading();
                alert("请求异常");
            }
        });
    }
}

var checkForm= function () {
    for (var i=0;i<=beanNum;i++){
        var acctype = $("#bean_acctype_"+i).val();
        if (acctype && acctype=='02'){
            var accountname = $("#bean_accountname_"+i).val();
            if (accountname && accountname!=''){
            }else{
                alert("姓名/商户不能为空");
                return false;
            }
        }
    }
    return true;
}