
$(function () {
    initZnzfSjscGrid();
    formEditValidate('znzfSjscEditForm');//表单验证
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#editModal').on('hidden.bs.modal', function (event) {
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").removeAttr('disabled');
        $("#editConfirmBtn").removeAttr('disabled','disabled');
        $("#editConfirmBtn").unbind();
        formEditValidateReset('znzfSjscEditForm');
    });
})

function initZnzfSjscGrid(){
    $('#znzfSjscGrid').bootstrapTable('destroy');

    $("#znzfSjscGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/znzf/sjsc/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [5,10,20,50,100],
        pageSize: 5,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryZnzfSjscParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 360,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
		showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
        //请根据需要修改/删除 需展示字段
            {
                title : "序号",
                field : "Number",
                align : "center",
                width : "50px",
                formatter: function (value, row, index) {
                    var pageSize = $('#znzfSjscGrid').bootstrapTable('getOptions').pageSize;     //通过table的#id 得到每页多少条
                    var pageNumber = $('#znzfSjscGrid').bootstrapTable('getOptions').pageNumber; //通过table的#id 得到当前第几页
                    return pageSize * (pageNumber - 1) + index + 1;    // 返回每条的序号： 每页条数 *（当前页 - 1 ）+ 序号
                }
            },
            {
                title : "文件名称",
                field : "filename",
                align : "center"
            },
            {
                title : "文件大小(KB)",
                field : "filesize",
                align : "center",
                formatter: function (value, row, index) {
                    return value?(Math.round(value/1024*100)/100):"-";
                }
            },
            {
                title : "录入单位",
                field : "lrdwmc",
                align : "center"
            },
            {
                title : "录入人",
                field : "lrrxm",
                align : "center"
            },
            {
                title : "录入时间",
                field : "rksj",
                align : "center",
                formatter: function (value, row, index) {
                    return value?new Date(value).Format("yyyy-MM-dd hh:mm:ss"):"";
                }
            },
            {
                title : "下载",
                field : "pk",
                align : "center",
                valign : "middle",
                width : "80px",
                events:znzfSjscOperateEvents,
                formatter : znzfSjscOperationFormatter
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function queryZnzfSjscParams(params){
    var tmp = $("#znzfSjscQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function znzfSjscOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' id='downloadBtn' class='btn' title='下载'><i class='fa fa-download fa-lg'></i></a>";
    result += "<a href='javascript:void(0);' id='delBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";

    return result;
}
//操作栏绑定事件
window.znzfSjscOperateEvents = {
    "click #downloadBtn":function (e,vale,row,index) {
        window.open(ctx+'/attachment/download?pk='+row.mongopk);
    },
    "click #delBtn":function (e,vale,row,index) {
        $("#tipsModalLabel").html("<i class='fa fa-warning fa-lg' style='color: #c87f0a'></i>警告");//提示标题
        $("#tipsMessage").text("确认删除么？");//设置提示信息
        $("#tipsConfirmBtn").text("删除");//按钮名称
        $("#tipsConfirmBtn").on("click",function(){
            deleteZnzfSjsc(row.pk);
        });
        $("#tipsModal").modal();
    }
}

function addZnzfSjsc() {
    $("#editModalLabel").text("数据上传");
    $("#edit_jjdpk").val($("#jjdpk").val());
    $("#edit_datatype").val($("#datatype").val());
    $("#dataFile").val('');

    $("#editConfirmBtn").on("click",function(){
        addSjsc();
    });

    $("#editModal").modal();
}

function deleteZnzfSjsc(id){
    var reqData={"curdType":"delete","pk":id};
    $("#tipsModal").modal('toggle');
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/znzf/sjsc/curd" ,
        data: reqData,
        success: function (result) {
            alert(result.msg);
            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function addSjsc() {
	//防止重复点击
    $("#editConfirmBtn").attr('disabled','disabled');
    //var reqData = $('#znzfSjscEditForm').serializeJsonObject();

    var formData = new FormData();
    var jjdpk = $('#edit_jjdpk').val();
    var datatype = $('#edit_datatype').val();
    var dataFile = $('#dataFile').get(0).files[0];
    formData.append("file",dataFile);
    formData.append("datatype", datatype);
    formData.append("jjdpk", jjdpk);

    $.ajax({
        type: "POST",
        //dataType: "json",
        contentType: false,
        processData: false,
        cache: false,
        url: ctx+"/znzf/sjsc/upload" ,
        enctype: 'multipart/form-data',
        data: formData,
        success : function(data) {
            alert(data.msg);
            $("#editModal").modal('toggle');

            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function search() {
    //initznzfSjscGrid();
    $("#znzfSjscGrid").bootstrapTable("refresh");
}


