/**
 * Created by Administrator on 2019/2/13/013.
 */
var getJbr = function (ifTableId) {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/system/jgz/getJgz" ,
        success: function (result) {
            if (result){
                if (result.pk){
                    $("#jbrzjhm").val(result.jbrzjhm);
                    $("#xcrzjhm").val(result.xcrzjhm);
                    $("#jbrxm").val(result.jbrxm);
                    $("#xcrxm").val(result.xcrxm);
                    $("#jbrdh").val(result.jbrdh);
                    $("#xcrdh").val(result.xcrdh);
                    fileUpload.showImgs([ctx+"/attachment/download?pk="+result.jbrmongo],'jbr_jgz');
                    fileUpload.showImgs([ctx+"/attachment/download?pk="+result.xcrmongo],'xcr_jgz');
                }else{
                    alert("您还未录入经办人信息，请先录入经办人信息");
                    var item = {'id':'jgzPage','name':'经办人	','url':ctx+'/system/jgz/page'};
                    iframeTab.parentAddIframe(item);
                    window.parent.$("#"+ifTableId).click();
                }
            }else{
                alert("您还未录入经办人信息，请先录入经办人信息");
                var item = {'id':'jgzPage','name':'经办人	','url':ctx+'/system/jgz/page'};
                iframeTab.parentAddIframe(item);
                //window.parent.$("#ifTab_close_addYhkMx").click();
                window.parent.$("#"+ifTableId).click();
            }
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function checkDzz(ifTableId){
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/system/dwDzz/checkDwDzz" ,
        async:false,
        success: function (result) {
            if(result==true){
                getJbr(ifTableId);
            }else{
                alert("您所在单位还未上传调证字相关电子印章，请联系管理员进行相关操作。");
                window.parent.$("#"+ifTableId).click();
            }
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function checkDzzNotIfrm(){
    var flag = false;
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/system/dwDzz/checkDwDzz" ,
        async:false,
        success: function (result) {
            if(result==true){
                flag = true;
            }else{
                alert("您所在单位还未上传调证字相关电子印章，请联系管理员进行相关操作。");
            }
        },
        error : function() {
            alert("请求异常");
        }
    });
    return flag;
}

function getJjdZh(czType,xxType,jjdPk){
    var reqData = {"czType":czType,"xxType":xxType,"jjdPk":jjdPk};
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/znzf/base/getJjdZh" ,
        data:reqData,
        async:false,
        success: function (result) {
            if(result){
                setJjdZhToJsp(result);
            }
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function addZnzfBean(){
    var oldBeanNum = $(".beanData:last").attr("data-bean-num");
    $(".beanData:last").after($(".beanData:last").clone());
    beanNum+=1;
    $(".beanData:last").attr("data-bean-num",beanNum);
    //遍历所有input
    $(".beanData:last input").each(function () {
        initAddParm(oldBeanNum,beanNum,this);
    })
    dicInit();
}

function removeZnzfBean(obj) {
    if($(".beanData").length>1){
        $(obj).parents(".beanData").remove();
        //$(".beanData:eq("+index+")").remove();
    }else{
        alert('最少保留一个');
    }
}

function getBank(obj){
    var zhId = $(obj).attr("id");

    var reg= new RegExp('bean_zh_','g');

    var zhjgdmId = zhId.replace(reg,'bean_zhjgdm_');
    var zhjgmcId = zhId.replace(reg,'bean_zhjgmc_');

    var zh = $(obj).val();
    if (zh && zh.trim().length>0){
        var reqData = {"account":zh};
        $.ajax({
            type: "POST",
            dataType: "json",
            url: ctx+"/system/bankCode/getBank" ,
            data:reqData,
            success: function (result) {
                if (result){
                    $("#"+zhjgdmId).val(result.bankcode);
                    $("#"+zhjgmcId).val(result.bankname);

                   /* var zhName = $(obj).attr("name");
                    var subName = zhName+'lb';
                    $("input[name='"+subName+"']").each(function () {
                        if ($(this).val() == result.subjecttype){
                            $(this).prop("checked","checked");
                        }
                    });*/
                }
            }
        });
    }
}

function setListData(json) {
    //获取相关账户信息
    json = getListParm("bean",json);
    return json;
}

var updateFlwsParm = function (oldBeanNum,index) {
    var reg= new RegExp(oldBeanNum,'g');

    var tdObj = $("#bean_mongo_"+index).next().find('[id^="edit_flws_"]');
    var tdId = tdObj.attr("id");
    tdId = tdId.replace(reg,index);
    tdObj.attr("id",tdId);
    tdObj.html('');

    var btn = $("#bean_mongo_"+index).next().find('button');
    var btnId = btn.attr("id");
    btnId = btnId.replace(reg,index);
    btn.attr("id",btnId);

    var btnOclc = btn.attr("onclick");
    btnOclc = btnOclc.replace(reg,index);
    btn.attr("onclick",btnOclc);

}

//data={"index":"","jgmc":"","zhxm":"","zzfs":"","zh":"","zzje":"","ajlb":"","zzsj":""}
var showZfFlws = function (data){
    var urls = [];

    $("#edit_flws_"+data.index).html('');
    $("#edit_flws_"+data.index).append('<input type="file" class="file"  id="flws_file_'+data.index+'" name="flws" >');

    urls = [ctx+"/attachment/zfws?yhjg="+data.jgmc+"&shr="+data.zhxm+"&zzfs="+data.zzfs+"&zh="+data.zh+"&zzje="+data.zzje+"&ajlb="+data.ajlb+"&zzsj_="+data.zzsj];

    //人工上传法律文书暂时不开放
    // fileUpload.editImgs('flws','flws_file_'+index,ctx+'/attachment/upload',urls);
    fileUpload.showImgs(urls,'flws_file_'+data.index);

}
//data={"index":"","jgmc":"","zhxm":"","zhlx":"","zh":""}
var showCxFlws = function (data) {
    var urls = [];

    $("#edit_flws_"+data.index).html('');
    $("#edit_flws_"+data.index).append('<input type="file" class="file"  id="flws_file_'+data.index+'" name="flws" >');

    urls = [ctx+"/attachment/cxws?yhjg="+data.jgmc+"&shr="+data.zhxm+"&zhlx="+data.zhlx+"&zh="+data.zh];
    //人工上传法律文书暂时不开放
    // fileUpload.editImgs('flws','flws_file_'+index,ctx+'/attachment/upload',urls);
    fileUpload.showImgs(urls,'flws_file_'+data.index);
}