/**
 * Created by Administrator on 2019/3/5/005.
 */
$(function () {
    checkDzz("ifTab_close_addsfZt");//验证调证字、经办人
    var iszcygzs = $("#iszcygzs").val();
    if (iszcygzs>=1){}else {
        getJjdZh("sfZt", $("#xxType").val(), $("#jjdPk").val());//获取接警单相关的账号信息
    }
    setDefaultData(0,0);//设置默认值
    formEditValidate('sfZtQqEditForm');//表单验证
})

var setDefaultData = function (oldBeanNum,index) {
    $('#bean_subjecttype_'+oldBeanNum).prop("checked","checked");
}

var showZtlb = function (obj) {
    var _obj = $(obj);
    var objId = _obj.attr("id");
    var index = objId.charAt(objId.length-1);
    var paycode = $("#bean_paycode_"+index).val();

    $("#bean_subjecttype_"+index).prop("checked","checked");
    if(_obj && _obj.val()!="" && paycode && paycode=='Z00444000013'){
            $("#div_subjecttype_"+index).removeClass("hide");
    }else{
        if(!$("#div_subjecttype_"+index).hasClass("hide")){
            $("#div_subjecttype_"+index).addClass("hide");
        }
    }
}

var addZnzfSfZt = function () {
    var oldBeanNum = $(".beanData:last").attr("data-bean-num");
    addZnzfBean();
    setDefaultData(oldBeanNum,beanNum);
    updateFlwsParm(oldBeanNum,beanNum);
    updateZtlb(oldBeanNum,beanNum);
    showZtlb($("#bean_payname_"+beanNum)[0]);
}

function showFlwsFile(index) {
    var zhlx = $("#bean_zzfs_"+index).val();
    var zh = $("#bean_accnumber_"+index).val();
    var jgmc = $("#bean_payname_"+index).val();
    if (jgmc && jgmc!=''){
    }else{
        alert("请选择所属机构");
        return false;
    }
    if (zh && zh!=''){
    }else{
        alert("请输入账号");
        return false;
    }
    if (zhlx && zhlx!=''){
    }else{
        zhlx = '支付账号';
    }

    var data = {"index":index,"jgmc":jgmc,"zhxm":"","zhlx":zhlx,"zh":zh};
    showCxFlws(data);
}

var setJjdZhToJsp = function (data) {
    if (data && data.length>0){
        for(var index =0;index<data.length;index++){
            if (index == 0){
                $("#bean_zzfs_0").val(data[index].zzfs);
                $("#bean_accnumber_0").val(data[index].zh);
                $("#bean_paycode_0").val(data[index].zhjgdm);
                $("#bean_payname_0").val(data[index].zhjgmc);
                $("#bean_zzsj_0").val(data[index].zcsj);
                setDefaultData(0,0);
            }else{
                addZnzfSfZt();
                $("#bean_zzfs_"+index).val(data[index].zzfs);
                $("#bean_accnumber_"+index).val(data[index].zh);
                $("#bean_paycode_"+index).val(data[index].zhjgdm);
                $("#bean_payname_"+index).val(data[index].zhjgmc);
                $("#bean_zzsj_"+index).val(data[index].zcsj);
                setDefaultData(index-1,index);
            }
            showZtlb($("#bean_payname_"+index)[0]);
        }
    }
}
var updateZtlb = function(oldBeanNum,index){
    var reg= new RegExp(oldBeanNum,'g');
    var div_ztlb = $("#bean_subjecttype_"+index).parent().parent();
    var div_ztlb_id = div_ztlb.attr("id");
    div_ztlb_id = div_ztlb_id.replace(reg,index);
    div_ztlb.attr("id",div_ztlb_id);
}

var sfZtQqEditSubmit = function (btnObj) {
    var flag = $('#sfZtQqEditForm').validate('submitValidate');
    if (flag){
        showLoading();
        var reqData = $('#sfZtQqEditForm').serializeJsonObject();
        reqData = setListData(reqData);
        reqData = JSON.stringify(reqData);
        $.ajax({
            type: "POST",
            dataType: "json",
            contentType: 'application/json; charset=UTF-8',
            url: ctx+"/znzf/sfZt/add" ,
            data: reqData,
            success : function(data) {
                hideLoading();
                if(data){
                    if(data.msg){
                        if(data.msg=='true'){
                            alert("操作成功");
                            window.parent.$("#ifTab_close_addsfZt").click();
                        }else{
                            alert(data.msg);
                        }
                    }else{
                        alert("请求异常！");
                    }
                }
            },
            error : function() {
                hideLoading();
                alert("请求异常");
            }
        });
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}