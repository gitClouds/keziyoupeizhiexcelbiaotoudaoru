/**
 * Created by Administrator on 2019/2/20/020.
 */
$(function () {
    checkDzz("ifTab_close_addyhkZt");//验证调证字、经办人
    var iszcygzs = $("#iszcygzs").val();
    if (iszcygzs>=1) {}else{
        getJjdZh("yhkZt", $("#xxType").val(), $("#jjdPk").val());//获取接警单相关的账号信息
    }
    setDefaultData(0,0);//设置默认值
    formEditValidate('yhkZtQqEditForm');//表单验证
})

var setJjdZhToJsp = function (data) {
    if (data && data.length>0){
        for(var index =0;index<data.length;index++){
            if (index == 0){
                $("#bean_zhxm_0").val(data[index].zhxm==null?"不详":data[index].zhxm);
                $("#bean_zh_0").val(data[index].zh);
                $("#bean_zhlx_0").val(data[index].zhlx);
                setDefaultData(0,0);
            }else{
                addZnzfYhkZt();
                $("#bean_zhxm_"+index).val(data[index].zhxm==null?"不详":data[index].zhxm);
                $("#bean_zh_"+index).val(data[index].zh);
                $("#bean_zhlx_"+index).val(data[index].zhlx);
                setDefaultData(index-1,index);
            }
        }
        $("[id^=bean_zh_]").each(function () {
            getBank(this);
        })
    }
}

var addZnzfYhkZt = function () {
    var oldBeanNum = $(".beanData:last").attr("data-bean-num");
    addZnzfBean();
    setDefaultData(oldBeanNum,beanNum);
    updateFlwsParm(oldBeanNum,beanNum);
}

var setDefaultData = function (oldBeanNum,index) {
    $('#bean_zhlb_'+oldBeanNum).prop("checked","checked");
    $('#bean_zhlx_'+(index)).val("银行卡");
}

function showFlwsFile(index) {
    var zhxm = $("#bean_zhxm_"+index).val();
    var zhlx = $("#bean_zhlx_"+index).val();
    var zh = $("#bean_zh_"+index).val();
    var zhjgmc = $("#bean_zhjgmc_"+index).val();
    if (zhjgmc && zhjgmc!=''){
    }else{
        alert("请选择所属银行");
        return false;
    }
    if (zh && zh!=''){
    }else{
        alert("请输入账号");
        return false;
    }
    var data={"index":index,"jgmc":zhjgmc,"zhxm":zhxm,"zhlx":zhlx,"zh":zh}
    showCxFlws(data);
}

var yhkZtQqEditSubmit = function(btnObj){
    var flag = $('#yhkZtQqEditForm').validate('submitValidate');
    if (flag){
        showLoading();
        var reqData = $('#yhkZtQqEditForm').serializeJsonObject();
        reqData = setListData(reqData);
        reqData = JSON.stringify(reqData);
        $.ajax({
            type: "POST",
            dataType: "json",
            contentType: 'application/json; charset=UTF-8',
            url: ctx+"/znzf/yhkZt/add" ,
            data: reqData,
            success : function(data) {
                hideLoading();
                if(data){
                    if(data.msg){
                        if(data.msg=='true'){
                            alert("操作成功");
                            window.parent.$("#ifTab_close_addyhkZt").click();
                        }else{
                            alert(data.msg);
                        }
                    }else{
                        alert("请求异常！");
                    }
                }
            },
            error : function() {
                hideLoading();
                alert("请求异常");
            }
        });
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}