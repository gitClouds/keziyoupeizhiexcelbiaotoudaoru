$(function () {
    initSfJgMeaGrid();
})

function initSfJgMeaGrid(){
    $('#sfJgMeaGrid').bootstrapTable('destroy');

    $("#sfJgMeaGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/znzf/sfJgMea/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: querySfJgMeaParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
        showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
            {
                title : "序号",
                field : "Number",
                align : "center",
                width : "50px",
                formatter: function (value, row, index) {
                    var pageSize = $('#sfJgMeaGrid').bootstrapTable('getOptions').pageSize;     //通过table的#id 得到每页多少条
                    var pageNumber = $('#sfJgMeaGrid').bootstrapTable('getOptions').pageNumber; //通过table的#id 得到当前第几页
                    return pageSize * (pageNumber - 1) + index + 1;    // 返回每条的序号： 每页条数 *（当前页 - 1 ）+ 序号
                }
            },
            {
                title : "支付账号",
                field : "zfzh",
                align : "center"
            },
            {
                title : "措施类型",
                field : "cslx",
                align : "center",
                formatter:function (value, row, index) {
                    if (value=='1'){
                        return '只收不付';
                    }else if (value=='2'){
                        return '不收不付';
                    }else if (value=='3'){
                        return '限额冻结';
                    }else if (value=='9'){
                        return '其他';
                    }
                }
            },
            {
                title : "开始日",
                field : "kssj",
                align : "center"
            },
            {
                title : "截止日",
                field : "jzsj",
                align : "center"
            },
            {
                title : "机关名称",
                field : "jgmc",
                align : "center"
            },
            {
                title : "执行金额",
                field : "zxje",
                align : "center"
            },
            {
                title : "备注",
                field : "bz",
                align : "center"
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function querySfJgMeaParams(params){
    var tmp = $("#testQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    tmp["actpk"]=$("#actpk").val();
    return tmp;
}

function exportSfJgMea(){
    var url = ctx+"/znzf/sfJgMea/excel?actpk="+$("#actpk").val();
    window.open(url);
}

function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}