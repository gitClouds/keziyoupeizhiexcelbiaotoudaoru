
/*$(function () {
    initCbMacGlGrid();
    //formEditValidate('cbMacGlEditForm');//表单验证
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
    /!*$('#editModal').on('hidden.bs.modal', function (event) {
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").removeAttr('disabled');
        $("#editConfirmBtn").removeAttr('disabled','disabled');
        $("#editConfirmBtn").unbind();
        formEditValidateReset('cbMacGlEditForm');
    });*!/
})*/
/*function initCbMacGlGrid(){
    $('#cbMacGlGrid').bootstrapTable('destroy');

    $("#cbMacGlGrid").bootstrapTable({
        method:"get",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        //contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/znzf/cbMacGl/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryCbMacGlParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
        showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
            //请根据需要修改/删除 需展示字段
            {
                title : "主键PK",
                field : "pk",
                align : "center"
            },
            {
                title : "串并编号",
                field : "cbbh",
                align : "center"
            },
            {
                title : "卡号所属类型：1-银行，2-三方",
                field : "sertype",
                align : "center"
            },
            {
                title : "请求PK",
                field : "qqpk",
                align : "center"
            },
            {
                title : "接警单PK",
                field : "jjdpk",
                align : "center"
            },
            {
                title : "MAC地址",
                field : "macdz",
                align : "center"
            },
            {
                title : "请求账号",
                field : "qqzh",
                align : "center"
            },
            {
                title : "请求账号姓名",
                field : "qqzhxm",
                align : "center"
            },
            {
                title : "请求账号机构代码",
                field : "qqzhjgdm",
                align : "center"
            },
            {
                title : "请求账号机构名称",
                field : "qqzhjgmc",
                align : "center"
            },
            {
                title : "接警单号码",
                field : "jjdhm",
                align : "center"
            },
            {
                title : "报案人姓名",
                field : "barxm",
                align : "center"
            },
            {
                title : "受理单位代码",
                field : "sldwdm",
                align : "center"
            },
            {
                title : "受理单位名称",
                field : "sldwmc",
                align : "center"
            },
            {
                title : "案发时间",
                field : "afsj",
                align : "center"
            },
            {
                title : "字段注释",
                field : "rksj",
                align : "center"
            },
            {
                title : "字段注释",
                field : "yxx",
                align : "center"
            },
            {
                title : "操作",
                field : "pk",
                align : "center",
                valign : "middle",
                width : "150px",
                events:cbMacGlOperateEvents,
                formatter : cbMacGlOperationFormatter
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}*/
function initCbMacGlGrid(){
    $('#cbMacGlGrid').bootstrapTable('destroy');

    $("#cbMacGlGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/znzf/cbMacGl/listMap',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [500,1000],
        pageSize: 500,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryCbMacGlParams,//参数
        /*showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮*/
        minimumCountColumns: 2,//最少允许的列数
        height: 330,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
		//showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
        //请根据需要修改/删除 需展示字段
            {
                title : "接警单号",
                field : "JJDHM",
                align : "center"
            },
            {
                title : "报案人姓名",
                field : "BARXM",
                align : "center"
            },
            {
                title : "受理单位名称",
                field : "SLDWMC",
                align : "center"
            },
            {
                title : "案发时间",
                field : "AFSJ",
                align : "center",
                formatter: function (value, row, index) {
                    return value?new Date(value).Format("yyyy-MM-dd hh:mm:ss"):"";
                }
            },
            /*{
                title : "帐/卡号",
                field : "QQPK",
                align : "center",
                formatter: function (value, row, index) {
                    var str = value.split(",");
                    if (str.length<=1) {
                        return value;
                    } else {
                        return str[0] + "<b class='text-red'>等</b>";
                    }
                }
            },*/
            {
                title : "MAC地址",
                field : "CBYJ",
                align : "center",
                formatter: function (value, row, index) {
                    var str = value.split(",");
                    if (str.length<=1) {
                        return value;
                    } else {
                        return str[0] + "<b class='text-red'>等</b>";
                    }
                }
            },
            {
                title : "操作",
                field : "pk",
                align : "center",
                valign : "middle",
                width : "150px",
                events:cbMacGlOperateEvents,
                formatter : cbMacGlOperationFormatter
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function queryCbMacGlParams(params){
    var tmp = $("#cbMacGlQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    tmp["jjdpk"]=$("#jjdpk").val();
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function cbMacGlOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' id='showBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";/*onclick=\"showCbMacGl('" + id + "', view='view')\"*/
   /* result += "<a href='javascript:void(0);' id='editBtn' class='btn' title='编辑'><span class='fa fa-edit fa-lg'></span></a>";/!*onclick=\"editCbMacGl('" + id + "')\"*!/
    result += "<a href='javascript:void(0);' id='delBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";/!*onclick=\"deleteCbMacGl('" + id + "')\"*!/*/

    return result;
}
// 以起始页码方式传入参数,params为table组装参数
function queryCbMacQqParams(params){
    var tmp = $("#cbMacQqQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    tmp["qqpk"]=$("#qqpk").val();
    return tmp;
}
function cbMacQqGabGrids(){
    $('#cbMacQqGabGrids').bootstrapTable('destroy');

    $("#cbMacQqGabGrids").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/znzf/cbMacGl/showCbMacQqPage',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [500,1000],
        pageSize: 500,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryCbMacQqParams,//参数
        //showColumns: true,//是否显示所有的列（选择显示的列）
        //showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 315,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        //toolbar: '#toolbar',              //工具按钮用哪个容器
        //toolbarAlign:"left",
        //showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
            //请根据需要修改/删除 需展示字段
            {
                title : "序号",
                field : "Number",
                align : "center",
                width : "5%",
                formatter: function (value, row, index) {
                    var pageSize = $('#cbMacQqGabGrids').bootstrapTable('getOptions').pageSize;     //通过table的#id 得到每页多少条
                    var pageNumber = $('#cbMacQqGabGrids').bootstrapTable('getOptions').pageNumber; //通过table的#id 得到当前第几页
                    return pageSize * (pageNumber - 1) + index + 1;    // 返回每条的序号： 每页条数 *（当前页 - 1 ）+ 序号
                }
            },
            {
                title : "账/卡号",
                field : "qqzh",
                align : "center"
            },
            {
                title : "账号姓名",
                field : "qqzhxm",
                align : "center"
            },
            {
                title : "所属机构",
                field : "qqzhjgmc",
                align : "center",
            },
            /*{
                title : "上级账号",
                field : "parentzh",
                align : "center"
            },*/
            {
                title : "MAC地址",
                field : "macdz",
                align : "center"
            },
            {
                title : "级别",
                field : "nlevel",
                align : "center"
                /* formatter: function (value, row, index) {
                 var str = value.split(",");
                 if (str.length<=1) {
                 return value;
                 } else {
                 return str[0] + "<b class='text-red'>等</b>";
                 }
                 }*/
            },
            {
                title : "串并账号",
                field : "pk",
                align : "center",
                valign : "middle",
                width : "100px",
                events:cbMacGlOperateEvents,
                formatter: function (value, row, index) {
                    var result= "<a href='javascript:void(0);' id='showXqBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";
                    return result;
                }
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}
function yaykXq(jjdpk) {
    var item = {'id':'yaykShowCb'+jjdpk,'name':'一案一库详情','url':ctx+'/yayk/show?pk='+jjdpk};
    iframeTab.parentAddIframe(item);
}
function selectQqzh(jjdpk,macdz) {
    var reqData = {"jjdpk":jjdpk,"macdz":macdz};
    $.ajax({
        type: "POST",
        dataType: "json",
        data:reqData,
        url: ctx+"/znzf/cbMacGl/selectCbZh" ,
        async:false,
        success: function (result) {
            $(".modal-body p").html(result.msg);
            $(".modal-dialog").stop().slideToggle(300);
        },
        error : function() {
            alert("请求异常");
        }
    });
}
//操作栏绑定事件
window.cbMacGlOperateEvents = {
    "click #showBtn":function (e,vale,row,index) {
        $("#showModalLabel").text("MAC串并详情").append('<span class="btn btn-info pull-right" onclick="yaykXq(\''+row.JJDPK+'\')" style="padding: 5px;">一案一库</span>');
        $("#zcShowIframe").attr("src",ctx+"/znzf/cbMacGl/showCbQq?qqpk="+row.QQPK+"&jjdpk="+$("#jjdpk").val());
        $("#showModal").modal();
    },
    "click #showXqBtn":function (e,vale,row,index) {
        selectQqzh($("#jjdpk").val(),row.macdz);
        //$(".modal-body p").html(row.qqpk);
        //$(".modal-dialog").stop().slideToggle(300);
    }
    /*"click #showBtn":function (e,vale,row,index) {
    	$("#showModalLabel").text("MAC串并详情");
    	//请根据需要修改/删除 需展示字段
        $("#show_pk").text(row.pk);
        $("#show_cbbh").text(row.cbbh);
        $("#show_sertype").text(row.sertype);
        $("#show_qqpk").text(row.qqpk);
        $("#show_jjdpk").text(row.jjdpk);
        $("#show_macdz").text(row.macdz);
        $("#show_qqzh").text(row.qqzh);
        $("#show_qqzhxm").text(row.qqzhxm);
        $("#show_qqzhjgdm").text(row.qqzhjgdm);
        $("#show_qqzhjgmc").text(row.qqzhjgmc);
        $("#show_jjdhm").text(row.jjdhm);
        $("#show_barxm").text(row.barxm);
        $("#show_sldwdm").text(row.sldwdm);
        $("#show_sldwmc").text(row.sldwmc);
        $("#show_afsj").text(row.afsj);
        $("#show_rksj").text(row.rksj);
        $("#show_yxx").text(row.yxx);

        $("#showModal").modal();
    },
    "click #editBtn":function (e,vale,row,index) {
    	//设置模态框标题
        $("#editModalLabel").text("**修改");
        //设置操作类型
        $("#edit_curdType").val("update");
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").attr('disabled','disabled');
        
        //请根据需要修改/删除 需展示字段
        //注意：字典类型数据需要dm和mc都进行设置
        $("#edit_pk").val(row.pk);
        $("#edit_cbbh").val(row.cbbh);
        $("#edit_sertype").val(row.sertype);
        $("#edit_qqpk").val(row.qqpk);
        $("#edit_jjdpk").val(row.jjdpk);
        $("#edit_macdz").val(row.macdz);
        $("#edit_qqzh").val(row.qqzh);
        $("#edit_qqzhxm").val(row.qqzhxm);
        $("#edit_qqzhjgdm").val(row.qqzhjgdm);
        $("#edit_qqzhjgmc").val(row.qqzhjgmc);
        $("#edit_jjdhm").val(row.jjdhm);
        $("#edit_barxm").val(row.barxm);
        $("#edit_sldwdm").val(row.sldwdm);
        $("#edit_sldwmc").val(row.sldwmc);
        $("#edit_afsj").val(row.afsj);
        $("#edit_rksj").val(row.rksj);
        $("#edit_yxx").val(row.yxx);

		//模态框的确认(保存)按钮绑定事件
        $("#editConfirmBtn").on("click",function(){
            //curdCbMacGl();
            cbMacGlEditSubmit();
        });
        $("#editModal").modal();
    },
    "click #delBtn":function (e,vale,row,index) {
        $("#tipsModalLabel").html("<i class='fa fa-warning fa-lg' style='color: #c87f0a'></i>警告");//提示标题
        $("#tipsMessage").text("确认删除么？");//设置提示信息
        $("#tipsConfirmBtn").text("删除");//按钮名称
        $("#tipsConfirmBtn").on("click",function(){
            deleteCbMacGl(row.pk);
        });
        $("#tipsModal").modal();
    }*/
}

function addCbMacGl() {
    $("#editModalLabel").text("**新增");
    $("#edit_curdType").val("insert");
    
    //请根据需要修改/删除 需展示字段
    //此处设置字段与修改时相同，是为清空点击修改时为元素的赋值
    //注意：字典类型数据需要dm和mc都进行设置
    $("#edit_pk").val('');
    $("#edit_cbbh").val('');
    $("#edit_sertype").val('');
    $("#edit_qqpk").val('');
    $("#edit_jjdpk").val('');
    $("#edit_macdz").val('');
    $("#edit_qqzh").val('');
    $("#edit_qqzhxm").val('');
    $("#edit_qqzhjgdm").val('');
    $("#edit_qqzhjgmc").val('');
    $("#edit_jjdhm").val('');
    $("#edit_barxm").val('');
    $("#edit_sldwdm").val('');
    $("#edit_sldwmc").val('');
    $("#edit_afsj").val('');
    $("#edit_rksj").val('');
    $("#edit_yxx").val('');
    
    $("#editConfirmBtn").on("click",function(){
        //curdCbMacGl();
        cbMacGlEditSubmit();
    });

    $("#editModal").modal();
}

function deleteCbMacGl(id){
    var reqData={"curdType":"delete","pk":id};
    $("#tipsModal").modal('toggle');
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/znzf/cbMacGl/curd" ,
        data: reqData,
        success: function (result) {
            alert(result.msg);
            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function curdCbMacGl() {
	//防止重复点击
    $("#editConfirmBtn").attr('disabled','disabled');
    var reqData = $('#cbMacGlEditForm').serializeJsonObject();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/znzf/cbMacGl/curd" ,
        data: reqData,
        success : function(data) {
            alert(data.msg);
            $("#editModal").modal('toggle');

            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function search() {
    //initcbMacGlGrid();
    $("#cbMacGlGrid").bootstrapTable("refresh");
}
function exportCbMacGl(){
    var form = document.cbMacGlQueryForm;
    var url = ctx+"/znzf/cbMacGl/excel";
    form.action=url;
    form.submit();
}

var cbMacGlEditSubmit = function(){
    var flag = $('#cbMacGlEditForm').validate('submitValidate');
    if (flag){
        curdCbMacGl();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}

