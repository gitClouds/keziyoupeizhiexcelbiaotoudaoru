/**
 * Created by Administrator on 2019/2/28/028.
 */
$(function () {
    checkDzz("ifTab_close_addsfZf");//验证调证字、经办人
    //getJjdZh("sfZf",$("#xxType").val(),$("#jjdPk").val());//获取接警单相关的账号信息
    setDefaultData(0,0);//设置默认值
    formEditValidate('sfZfQqEditForm');//表单验证
})

var showZffs = function (obj) {
    var _obj = $(obj);
    var objId = _obj.attr("id");
    var index = objId.charAt(objId.length-1);
    var paycode = $("#bean_paycode_"+index).val();

    $("#bean_zffs_"+index).prop("checked","checked");
    $("#bean_transferamount_"+index).val('');
    if(_obj && _obj.val()!="" && paycode && paycode=='Z00133000019'){
            $("#div_zffs_"+index).removeClass("hide");

            if(!$("#div_transferamount_"+index).hasClass("hide")){
                $("#div_transferamount_"+index).addClass("hide");
            }
    }else{
        if(!$("#div_zffs_"+index).hasClass("hide")){
            $("#div_zffs_"+index).addClass("hide");
        }
        if(!$("#div_transferamount_"+index).hasClass("hide")){
            $("#div_transferamount_"+index).addClass("hide");
        }
    }
}

var showTransferAmount = function(obj){
    var objId = $(obj).attr("id");
    var index = objId.charAt(objId.length-1);

    $("#bean_transferamount_"+index).val('');

    if ($(obj).val()=="01"){
        if($("#div_transferamount_"+index).hasClass("hide")){
            $("#div_transferamount_"+index).removeClass("hide");
            $('#bean_transferamount_'+index).attr("data-valid","isNonEmpty||onlyInt||between:1-9");
            $('#bean_transferamount_'+index).attr("data-error","不能为空||只能输入数字||长度为1-9位");
        }
    }else{
        if(!$("#div_transferamount_"+index).hasClass("hide")){
            $("#div_transferamount_"+index).addClass("hide");
            $('#bean_transferamount_'+index).attr("data-valid","onlyInt");
            $('#bean_transferamount_'+index).attr("data-error","");
        }
    }
}

var setDefaultData = function (oldBeanNum,index) {
    $('#bean_subjecttype_'+oldBeanNum).prop("checked","checked");
    $('#bean_zffs_'+oldBeanNum).prop("checked","checked");
}

var addZnzfSfZf = function () {
    var oldBeanNum = $(".beanData:last").attr("data-bean-num");
    addZnzfBean();
    setDefaultData(oldBeanNum,beanNum);
    updateFlwsParm(oldBeanNum,beanNum);
    updateZffs(oldBeanNum,beanNum);
    showZffs($("#bean_payname_"+beanNum)[0]);
}

var updateZffs = function(oldBeanNum,index){
    var reg= new RegExp(oldBeanNum,'g');
    var div_zzfs = $("#bean_zffs_"+index).parent().parent();
    var div_zzfs_id = div_zzfs.attr("id");
    div_zzfs_id = div_zzfs_id.replace(reg,index);
    div_zzfs.attr("id",div_zzfs_id);

    var div_transferamount = $("#bean_transferamount_"+index).parent().parent();
    var div_transferamount_id = div_transferamount.attr("id");
    div_transferamount_id = div_transferamount_id.replace(reg,index);
    div_transferamount.attr("id",div_transferamount_id);
}

function showFlwsFile(index) {
    var shr = $("#shr").val();
    var ajlb = $("#ajlb").val();
    var zhjgdm = $("#bean_paycode_"+index).val();
    var zzfs = "";
    if(zhjgdm=='Z00133000019'){
        zzfs = "支付宝";
    }else if(zhjgdm=='Z00444000013'){
        zzfs = "微信";
    }else{
        zzfs = "三方账号";
    }
    var zhjgmc = $("#bean_payname_"+index).val();
    var zh = $("#bean_accnumber_"+index).val();
    var zzje = $("#saje").val();
    var zzsj = new Date().initDateZeroTime();
    if (zhjgmc && zhjgmc!=''){
    }else{
        alert("请选择所属机构");
        return false;
    }
    if (zh && zh!=''){
    }else{
        alert("请输入账号");
        return false;
    }
    if (zzsj && zzsj!=''){
    }else{
        zzsj = $("#jjsj").val();
    }
    if (zzje && zzje!=''){
    }else{
        zzje = 0;
    }
    if (zzfs && zzfs!=''){
    }else{
        zzfs = '支付账号';
    }
    var data={"index":index,"jgmc":zhjgmc,"zhxm":shr,"zzfs":zzfs,"zh":zh,"zzje":zzje,"ajlb":ajlb,"zzsj":zzsj};
    showZfFlws(data);
}

var setJjdZhToJsp = function (data) {
    if (data && data.length>0){
        for(var index =0;index<data.length;index++){
            if (index == 0){
                $("#bean_zzfs_0").val(data[index].zzfs);
                $("#bean_accnumber_0").val(data[index].zh);
                $("#bean_paycode_0").val(data[index].zhjgdm);
                $("#bean_payname_0").val(data[index].zhjgmc);

                $("#bean_zzsj_0").val(data[index].zzsj);
                $("#bean_zzje_0").val(data[index].zzje);
                setDefaultData(0,0);
            }else{
                addZnzfSfZf();
                $("#bean_zzfs_"+index).val(data[index].zzfs);
                $("#bean_accnumber_"+index).val(data[index].zh);
                $("#bean_paycode_"+index).val(data[index].zhjgdm);
                $("#bean_payname_"+index).val(data[index].zhjgmc);

                $("#bean_zzsj_"+index).val(data[index].zzsj);
                $("#bean_zzje_"+index).val(data[index].zzje);
                setDefaultData(index-1,index);
            }
            showZffs($("#bean_payname_"+index)[0]);
        }
    }
}

var sfZfQqEditSubmit = function (btnObj) {
    var flag = $('#sfZfQqEditForm').validate('submitValidate');
    if (flag){
        //防止重复点击
        //$(btnObj).attr('disabled','disabled');
        var reqData = $('#sfZfQqEditForm').serializeJsonObject();
        reqData = setListData(reqData);
        reqData = JSON.stringify(reqData);
        $.ajax({
            type: "POST",
            dataType: "json",
            contentType: 'application/json; charset=UTF-8',
            url: ctx+"/znzf/sfZf/add" ,
            data: reqData,
            success : function(data) {
                if(data){
                    if(data.msg){
                        if(data.msg=='true'){
                            alert("操作成功");
                            window.parent.$("#ifTab_close_addsfZf").click();
                        }else{
                            alert(data.msg);
                        }
                    }else{
                        alert("请求异常！");
                    }
                }
            },
            error : function() {
                alert("请求异常");
            }
        });
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}