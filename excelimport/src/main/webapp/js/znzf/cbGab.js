
$(function () {
    //initCbGabGrid();
    //initCbGabGrids();
    //formEditValidate('cbGabEditForm');//表单验证
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
   /* $('#editModal').on('hidden.bs.modal', function (event) {
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").removeAttr('disabled');
        $("#editConfirmBtn").removeAttr('disabled','disabled');
        $("#editConfirmBtn").unbind();
        formEditValidateReset('cbGabEditForm');
    });*/
})
function initCbGabGrids(){
    $('#cbGabGrids').bootstrapTable('destroy');

    $("#cbGabGrids").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/znzf/cbGab/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [500,1000],
        pageSize: 500,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        //search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryCbGabParams,//参数
        //showColumns: true,//是否显示所有的列（选择显示的列）
        //showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 250,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        //clickToSelect: true,//是否启用点击选中行
        //smartDisplay: true,//智能显示分页或卡视图
        //toolbar: '#toolbar',              //工具按钮用哪个容器
        //toolbarAlign:"left",
        //showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
            //请根据需要修改/删除 需展示字段
            {
                title : "序号",
                field : "Number",
                align : "center",
                width : "5%",
                formatter: function (value, row, index) {
                    var pageSize = $('#cbGabGrids').bootstrapTable('getOptions').pageSize;     //通过table的#id 得到每页多少条
                    var pageNumber = $('#cbGabGrids').bootstrapTable('getOptions').pageNumber; //通过table的#id 得到当前第几页
                    return pageSize * (pageNumber - 1) + index + 1;    // 返回每条的序号： 每页条数 *（当前页 - 1 ）+ 序号
                }
            },
            {
                title : "账号",
                field : "cardnumber",
                width : "25%",
                align : "center"
            },
            {
                title : "账号姓名",
                field : "cardname",
                width : "15%",
                align : "center"
            },
            {
                title : "录入时间",
                field : "lrsj",
                width : "15%",
                align : "center",
                formatter: function (value, row, index) {
                    return value?new Date(value).Format("yyyy-MM-dd hh:mm:ss"):"";
                }
            },
            {
                title : "录入单位",
                field : "lrdwdm",
                width : "25%",
                align : "center"
            },
            {
                title : "数量",
                field : "count",
                width : "8%",
                align : "center"
            },
            /*{
                title : "简要案情",
                field : "sy",
                align : "center"
            },*/
            {
                title : "简要案情",
                field : "pk",
                align : "center",
                valign : "middle",
                width : "150px",
                events:cbGabOperateEvents,
                //formatter : cbGabOperationFormatter
                formatter: function (value, row, index) {
                    var result= "<a href='javascript:void(0);' id='showXqBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";
                    return result;
                }
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}
function initCbGabGrid(){
    $('#gabcb').bootstrapTable('destroy');

    $("#gabcb").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/znzf/cbGab/listcb',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [500,1000],
        pageSize: 500,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryCbGabParams,//参数
        //showColumns: true,//是否显示所有的列（选择显示的列）
        //showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 330,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
		//showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
        //请根据需要修改/删除 需展示字段
            {
                title : "序号",
                field : "Number",
                align : "center",
                width : "70px",
                formatter: function (value, row, index) {
                    var pageSize = $('#gabcb').bootstrapTable('getOptions').pageSize;     //通过table的#id 得到每页多少条
                    var pageNumber = $('#gabcb').bootstrapTable('getOptions').pageNumber; //通过table的#id 得到当前第几页
                    return pageSize * (pageNumber - 1) + index + 1;    // 返回每条的序号： 每页条数 *（当前页 - 1 ）+ 序号
                }
            },
            {
                title : "账号",
                field : "cardnumber",
                align : "center"
            },
            {
                title : "账号姓名",
                field : "cardname",
                align : "center"
            },
            {
                title : "数量",
                field : "count",
                align : "center"
            },
            {
                title : "操作",
                field : "pk",
                align : "center",
                valign : "middle",
                width : "150px",
                events:cbGabOperateEvents,
                formatter : cbGabOperationFormatter
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function queryCbGabParams(params){
    var tmp = $("#cbGabQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    tmp["jjdpk"]=$("#jjdpk").val();
    tmp["cardnumber"]=$("#cardnumber").val();
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function cbGabOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' id='showBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";/*onclick=\"showCbGab('" + id + "', view='view')\"*/
   /* result += "<a href='javascript:void(0);' id='editBtn' class='btn' title='编辑'><span class='fa fa-edit fa-lg'></span></a>";/!*onclick=\"editCbGab('" + id + "')\"*!/
    result += "<a href='javascript:void(0);' id='delBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";/!*onclick=\"deleteCbGab('" + id + "')\"*!/*/
    return result;
}
//操作栏绑定事件
window.cbGabOperateEvents = {

    "click #showBtn":function (e,vale,row,index) {

        var jjdpk = $("#jjdpk").val();
        $("#showModalLabel").text("公安部串并信息详情");
        $("#zcShowIframe").attr("src",ctx+"/znzf/cbGab/show?jjdpk="+jjdpk+"&cardnumber="+row.cardnumber);
        $("#showModal").modal();
    },
    "click #showXqBtn":function (e,vale,row,index) {
        $(".modal-body p").html(row.sy);
        $(".modal-dialog").stop().slideToggle(300);
    }
    /*"click #delBtn":function (e,vale,row,index) {
        $("#tipsModalLabel").html("<i class='fa fa-warning fa-lg' style='color: #c87f0a'></i>警告");//提示标题
        $("#tipsMessage").text("确认删除么？");//设置提示信息
        $("#tipsConfirmBtn").text("删除");//按钮名称
        $("#tipsConfirmBtn").on("click",function(){
            deleteCbGab(row.pk);
        });
        $("#tipsModal").modal();
    }*/
}

/*function addCbGab() {
    $("#editModalLabel").text("**新增");
    $("#edit_curdType").val("insert");
    
    //请根据需要修改/删除 需展示字段
    //此处设置字段与修改时相同，是为清空点击修改时为元素的赋值
    //注意：字典类型数据需要dm和mc都进行设置
    $("#edit_pk").val('');
    $("#edit_sertype").val('');
    $("#edit_jjdpk").val('');
    $("#edit_ywsqbh").val('');
    $("#edit_bankcode").val('');
    $("#edit_cardnumber").val('');
    $("#edit_cardname").val('');
    $("#edit_lrdwdm").val('');
    $("#edit_lrsj").val('');
    $("#edit_rksj").val('');
    $("#edit_yxx").val('');
    $("#edit_sy").val('');
    
    $("#editConfirmBtn").on("click",function(){
        //curdCbGab();
        cbGabEditSubmit();
    });

    $("#editModal").modal();
}

function deleteCbGab(id){
    var reqData={"curdType":"delete","pk":id};
    $("#tipsModal").modal('toggle');
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/znzf/cbGab/curd" ,
        data: reqData,
        success: function (result) {
            alert(result.msg);
            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function curdCbGab() {
	//防止重复点击
    $("#editConfirmBtn").attr('disabled','disabled');
    var reqData = $('#cbGabEditForm').serializeJsonObject();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/znzf/cbGab/curd" ,
        data: reqData,
        success : function(data) {
            alert(data.msg);
            $("#editModal").modal('toggle');

            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}*/

function search() {
    //initcbGabGrid();
    $("#cbGabGrid").bootstrapTable("refresh");
}
/*function exportCbGab(){
    var form = document.cbGabQueryForm;
    var url = ctx+"/znzf/cbGab/excel";
    form.action=url;
    form.submit();
}*/
function exportCbGab() {
    var url = ctx+"/znzf/cbGab/excel?jjdpk="+$("#jjdpk").val();
    window.open(url);
}

var cbGabEditSubmit = function(){
    var flag = $('#cbGabEditForm').validate('submitValidate');
    if (flag){
        curdCbGab();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}

