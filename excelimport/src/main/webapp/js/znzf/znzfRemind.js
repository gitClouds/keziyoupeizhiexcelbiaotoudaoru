
$(function () {
    initZnzfRemindGrid();
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
})

function initZnzfRemindGrid(){
    $('#znzfRemindGrid').bootstrapTable('destroy');

    $("#znzfRemindGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/znzf/znzfRemind/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [5,10,20,50,100],
        pageSize: 50,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryZnzfRemindParams,//参数
        showColumns: false,//是否显示所有的列（选择显示的列）
        showRefresh: false,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 300,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: false,//是否启用点击选中行
        smartDisplay: false,//智能显示分页或卡视图
		showToggle: false,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
            /*{
                title : "接警单pk",
                field : "jjdpk",
                align : "center"
            },
            {
                title : "账号",
                field : "cardnumber",
                align : "center"
            },*/
            /*{
                title : "类型",
                field : "remindtype",
                align : "center"
            },*/
            {
                title : "提醒信息",
                field : "remark",
                align : "left"
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}
// 以起始页码方式传入参数,params为table组装参数
function queryZnzfRemindParams(params){
    var tmp = $("#znzfRemindQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}

function search() {
    //initznzfRemindGrid();
    $("#znzfRemindGrid").bootstrapTable("refresh");
}
function exportZnzfRemind(){
    var form = document.znzfRemindQueryForm;
    var url = ctx+"/znzf/znzfRemind/excel";
    form.action=url;
    form.submit();
}

