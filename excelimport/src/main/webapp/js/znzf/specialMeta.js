
$(function () {
    initSpecialMetaGrid();
    formEditValidate('specialMetaEditForm');//表单验证
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
    $('#editModal').on('hidden.bs.modal', function (event) {
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").removeAttr('disabled');
        $("#editConfirmBtn").removeAttr('disabled','disabled');
        $("#editConfirmBtn").unbind();
        formEditValidateReset('specialMetaEditForm');
    });
})

function initSpecialMetaGrid(){
    $('#specialMetaGrid').bootstrapTable('destroy');

    $("#specialMetaGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/znzf/specialMeta/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: querySpecialMetaParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
		showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
        //请根据需要修改/删除 需展示字段
            {
                title : "序号",
                field : "Number",
                align : "center",
                width : "50px",
                formatter: function (value, row, index) {
                    var pageSize = $('#specialMetaGrid').bootstrapTable('getOptions').pageSize;     //通过table的#id 得到每页多少条
                    var pageNumber = $('#specialMetaGrid').bootstrapTable('getOptions').pageNumber; //通过table的#id 得到当前第几页
                    return pageSize * (pageNumber - 1) + index + 1;    // 返回每条的序号： 每页条数 *（当前页 - 1 ）+ 序号
                }
            },
            {
                title : "归属银行",
                field : "bankname",
                align : "center"
            },
            {
                title : "借贷标志",
                field : "jdbz",
                align : "center",
                formatter:function (value, row, index) {
                    if (value=='0'){
                        return '借';
                    }else if (value=='1'){
                        return '贷';
                    }
                }
            },
            {
                title : "验证字段",
                field : "checkfield",
                align : "center"
            },
            {
                title : "需验证值",
                field : "checkvalue",
                align : "center"
            },
            {
                title : "需请求类型",
                field : "sertype",
                align : "center",
                formatter:function (value, row, index) {
                    if (value=='S-19'){
                        return '流水';
                    }else if (value=='S-09'){
                        return '全账号';
                    }
                }
            },
            {
                title : "请求数据所在字段",
                field : "datafield",
                align : "center",
                visible: false
            },
            {
                title : "请求数据所在位置",
                field : "positionval",
                align : "center",
                visible: false,
                formatter:function (value, row, index) {
                    if (value=='1'){
                        return '开头';
                    }else if (value=='2'){
                        return '结尾';
                    }
                }
            },
            {
                title : "前一个字符",
                field : "startpfx",
                align : "center",
                visible: false,
            },
            {
                title : "后一个字符",
                field : "endpfx",
                align : "center",
                visible: false,
            },
            {
                title : "需请求三方机构",
                field : "payname",
                align : "center"
            },
            {
                title : "操作",
                field : "pk",
                align : "center",
                valign : "middle",
                width : "150px",
                events:specialMetaOperateEvents,
                formatter : specialMetaOperationFormatter
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function querySpecialMetaParams(params){
    var tmp = $("#specialMetaQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function specialMetaOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' id='showBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";/*onclick=\"showSpecialMeta('" + id + "', view='view')\"*/
    result += "<a href='javascript:void(0);' id='editBtn' class='btn' title='编辑'><span class='fa fa-edit fa-lg'></span></a>";/*onclick=\"editSpecialMeta('" + id + "')\"*/
    result += "<a href='javascript:void(0);' id='delBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";/*onclick=\"deleteSpecialMeta('" + id + "')\"*/

    return result;
}
//操作栏绑定事件
window.specialMetaOperateEvents = {
    "click #showBtn":function (e,vale,row,index) {
    	$("#showModalLabel").text("特殊分析规则详情");
    	//请根据需要修改/删除 需展示字段
        $("#show_pk").text(row.pk);
        $("#show_bankname").text(row.bankname);
        $("#show_jdbz").text(row.jdbz=='0'?'借':row.jdbz=='1'?'贷':row.jdbz);
        $("#show_checkfield").text(row.checkfield);
        $("#show_checkvalue").text(row.checkvalue);
        $("#show_sertype").text(row.sertype=='S-09'?'全账号':row.sertype=='S-19'?'流水':row.sertype);
        $("#show_datafield").text(row.datafield);
        $("#show_positionval").text(row.positionval);
        $("#show_startpfx").text(row.startpfx);
        $("#show_endpfx").text(row.endpfx);
        $("#show_payname").text(row.payname);

        $("#showModal").modal();
    },
    "click #editBtn":function (e,vale,row,index) {
    	//设置模态框标题
        $("#editModalLabel").text("特殊分析规则修改");
        //设置操作类型
        $("#edit_curdType").val("update");

        //请根据需要修改/删除 需展示字段
        //注意：字典类型数据需要dm和mc都进行设置
        $("#edit_pk").val(row.pk);
        $("#edit_bankcode").val(row.bankcode);
        $("#edit_jdbz").val(row.jdbz);
        $("#edit_checkfield").val(row.checkfield);
        $("#edit_checkvalue").val(row.checkvalue);
        $("#edit_sertype").val(row.sertype);
        $("#edit_datafield").val(row.datafield);
        $("#edit_positionval").val(row.positionval);
        $("#edit_startpfx").val(row.startpfx);
        $("#edit_endpfx").val(row.endpfx);
        $("#edit_paycode").val(row.paycode);
        $("#edit_payname").val(row.payname);
        $("#edit_yxx").val(row.yxx);
        $("#edit_rksj").val(row.rksj?new Date(row.rksj).Format("yyyy-MM-dd hh:mm:ss"):"");

		//模态框的确认(保存)按钮绑定事件
        $("#editConfirmBtn").on("click",function(){
            //curdSpecialMeta();
            specialMetaEditSubmit();
        });
        $("#editModal").modal();
    },
    "click #delBtn":function (e,vale,row,index) {
        $("#tipsModalLabel").html("<i class='fa fa-warning fa-lg' style='color: #c87f0a'></i>警告");//提示标题
        $("#tipsMessage").text("确认删除么？");//设置提示信息
        $("#tipsConfirmBtn").text("删除");//按钮名称
        $("#tipsConfirmBtn").on("click",function(){
            deleteSpecialMeta(row.pk);
        });
        $("#tipsModal").modal();
    }
}

function addSpecialMeta() {
    $("#editModalLabel").text("特殊分析规则新增");
    $("#edit_curdType").val("insert");
    
    //请根据需要修改/删除 需展示字段
    //此处设置字段与修改时相同，是为清空点击修改时为元素的赋值
    //注意：字典类型数据需要dm和mc都进行设置
    $("#edit_pk").val('');
    $("#edit_bankcode").val('');
    $("#edit_bankname").val('');
    $("#edit_jdbz").val('');
    $("#edit_checkfield").val('');
    $("#edit_checkvalue").val('');
    $("#edit_sertype").val('');
    $("#edit_datafield").val('');
    $("#edit_positionval").val('');
    $("#edit_startpfx").val('');
    $("#edit_endpfx").val('');
    $("#edit_paycode").val('');
    $("#edit_payname").val('');

    $("#editConfirmBtn").on("click",function(){
        //curdSpecialMeta();
        specialMetaEditSubmit();
    });

    $("#editModal").modal();
}

function deleteSpecialMeta(id){
    var reqData={"curdType":"delete","pk":id};
    $("#tipsModal").modal('toggle');
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/znzf/specialMeta/curd" ,
        data: reqData,
        success: function (result) {
            alert(result.msg);
            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function curdSpecialMeta() {
	//防止重复点击
    $("#editConfirmBtn").attr('disabled','disabled');
    var reqData = $('#specialMetaEditForm').serializeJsonObject();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/znzf/specialMeta/curd" ,
        data: reqData,
        success : function(data) {
            alert(data.msg);
            $("#editModal").modal('toggle');

            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function search() {
    //initspecialMetaGrid();
    $("#specialMetaGrid").bootstrapTable("refresh");
}
function exportSpecialMeta(){
    var form = document.specialMetaQueryForm;
    var url = ctx+"/znzf/specialMeta/excel";
    form.action=url;
    form.submit();
}

var specialMetaEditSubmit = function(){
    var flag = $('#specialMetaEditForm').validate('submitValidate');
    if (flag){
        curdSpecialMeta();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}

