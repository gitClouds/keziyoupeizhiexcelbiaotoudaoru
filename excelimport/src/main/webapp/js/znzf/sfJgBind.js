/**
 * Created by Administrator on 2019/3/27/027.
 */
$(function () {
    initSfJgBindGrid();
})


function initSfJgBindGrid(){
    $('#sfJgBindGrid').bootstrapTable('destroy');

    $("#sfJgBindGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/znzf/sfJgBind/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [5,10,20,50,100],
        pageSize: 5,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: querySfJgBindParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 300,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
        showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
            {
                title : "序号",
                field : "Number",
                align : "center",
                width : "50px",
                formatter: function (value, row, index) {
                    var pageSize = $('#sfJgBindGrid').bootstrapTable('getOptions').pageSize;     //通过table的#id 得到每页多少条
                    var pageNumber = $('#sfJgBindGrid').bootstrapTable('getOptions').pageNumber; //通过table的#id 得到当前第几页
                    return pageSize * (pageNumber - 1) + index + 1;    // 返回每条的序号： 每页条数 *（当前页 - 1 ）+ 序号
                }
            },
            {
                title : "数据类型",
                field : "datatype",
                align : "center",
                formatter : function(value, row, index) {
                    var result="";
                    if (value=='01'){
                        result="登录号";
                    }else if(value=='02'){
                        result="QQ号";
                    }else if(value=='03'){
                        result="微信号";
                    }else if(value=='04'){
                        result="邮箱";
                    }else if(value=='05'){
                        result="IP";
                    }else if(value=='06'){
                        result="设备号";
                    }
                    return result;
                }
            },
            {
                title : "绑定数据",
                field : "bindingdata",
                align : "center"
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}
function querySfJgBindParams(params){
    var tmp = $("#testQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    tmp["applicationid"]=$("#applicationid").val();
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}

function exportBind() {
    var url = ctx+"/znzf/sfJgBind/excel?applicationid="+$("#applicationid").val();
    window.open(url);
}