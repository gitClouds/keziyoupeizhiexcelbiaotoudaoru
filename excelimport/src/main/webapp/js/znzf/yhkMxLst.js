/**
 * Created by Administrator on 2019/3/26/026.
 */
$(function () {
    initMxListGrid();
})

function initMxListGrid() {
    $('#mxListGrid').bootstrapTable('destroy');

    $("#mxListGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/znzf/yhkMxJgLst/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [5,10,20,50,100],
        pageSize: 5,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 410,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
        showToggle: true,//是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
            {
                title : "序号",
                field : "Number",
                align : "center",
                width : "50px",
                formatter: function (value, row, index) {
                    var pageSize = $('#mxListGrid').bootstrapTable('getOptions').pageSize;     //通过table的#id 得到每页多少条
                    var pageNumber = $('#mxListGrid').bootstrapTable('getOptions').pageNumber; //通过table的#id 得到当前第几页
                    return pageSize * (pageNumber - 1) + index + 1;    // 返回每条的序号： 每页条数 *（当前页 - 1 ）+ 序号
                }
            },
            {
                title : "交易类型",
                field : "jylx",
                align : "center"
            },
            {
                title : "借贷标志",
                field : "jdbz",
                align : "center",
                formatter:function (value, row, index) {
                    if (value=='0'){
                        return '借';
                    }else if (value=='1'){
                        return '贷';
                    }
                }
            },
            {
                title : "交易金额",
                field : "jyje",
                align : "center"
            },
            {
                title : "余额",
                field : "jyye",
                align : "center"
            },
            {
                title : "交易时间",
                field : "jysj",
                align : "center",
                formatter: function (value, row, index) {
                    return value?new Date(value).Format("yyyy-MM-dd hh:mm:ss"):"";
                }
            },
            {
                title : "对方名称",
                field : "jydfmc",
                align : "center"
            },
            {
                title : "对方账卡号",
                field : "jydfzh",
                align : "center"
            },
            {
                title : "IP地址",
                field : "ipdz",
                align : "center",
                visible: false
            },
            {
                title : "MAC地址",
                field : "macdz",
                align : "center",
                visible: false
            },
            {
                title : "交易流水号",
                field : "jylsh",
                align : "center",
                visible: false
            },
            {
                title : "对方证件号码",
                field : "jydfzjhm",
                align : "center",
                visible: false
            },
            {
                title : "对方开户行",
                field : "jydfkhh",
                align : "center",
                visible: false
            },
            {
                title : "交易是否成功",
                field : "jysfcg",
                align : "center",
                visible: false,
                formatter:function (value, row, index) {
                    if (value=='00'){
                        return '成功';
                    }else if (value=='01'){
                        return '失败';
                    }
                }
            },
            {
                title : "现金标志",
                field : "xjbz",
                align : "center",
                visible: false,
                formatter:function (value, row, index) {
                    if (value=='00'){
                        return '其它';
                    }else if (value=='01'){
                        return '现金交易';
                    }
                }
            },
            {
                title : "交易网点",
                field : "jywdmc",
                align : "center",
                visible: false,
            },
            {
                title : "交易摘要",
                field : "jyzy",
                align : "center",
                visible: false,
            },
            {
                title : "日志号",
                field : "rzh",
                align : "center",
                visible: false,
            },
            {
                title : "传票号",
                field : "cph",
                align : "center",
                visible: false,
            },
            {
                title : "凭证号",
                field : "pzh",
                align : "center",
                visible: false,
            },
            {
                title : "终端号",
                field : "zdh",
                align : "center",
                visible: false,
            },
            {
                title : "柜员号",
                field : "jygyh",
                align : "center",
                visible: false,
            },
            {
                title : "商户名称",
                field : "shmc",
                align : "center",
                visible: false,
            },
            {
                title : "商户号",
                field : "shh",
                align : "center",
                visible: false,
            },
            {
                title : "币种",
                field : "rmbzl",
                align : "center",
                visible: false,
            },
            {
                title : "备注",
                field : "bz",
                align : "center",
                visible: false,
            },
            {
                title : "凭证种类",
                field : "pzzl",
                align : "center",
                visible: false,
            }

        ],
        responseHandler:responseHandler,//请求数据成功后，渲染表格前的方法

        onPostBody:function()
        {
            //重点就在这里，获取渲染后的数据列td的宽度赋值给对应头部的th,这样就表头和列就对齐了
            var header=$(".fixed-table-header table thead tr th");
            var body=$(".fixed-table-header table tbody tr td");
            var footer=$(".fixed-table-header table tr td");
            body.each(function(){
                header.width((this).width());
                footer.width((this).width());
            });
        }
    });
}

// 以起始页码方式传入参数,params为table组装参数
function queryParams(params){
    var tmp = $("#testQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    tmp["qqpk"]=$("#pk").val();
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}

function exportMxList() {
    var url = ctx+"/znzf/yhkMxJgLst/excel?qqpk="+$("#pk").val()+"&cardnumber="+$("#cardnumber").val();
    window.open(url);
}