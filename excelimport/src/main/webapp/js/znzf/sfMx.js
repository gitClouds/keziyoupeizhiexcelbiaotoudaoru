/**
 * Created by Administrator on 2019/3/4/004.
 */
$(function () {
    checkDzz("ifTab_close_addsfMx");//验证调证字、经办人
    var iszcygzs = $("#iszcygzs").val();
    if (iszcygzs >= 1) {
    } else {
        getJjdZh("sfMx", $("#xxType").val(), $("#jjdPk").val());//获取接警单相关的账号信息
    }
    setDefaultData(0, 0);//设置默认值
    formEditValidate('sfMxQqEditForm');//表单验证
})

var setDefaultData = function (oldBeanNum, index) {
    var zzsj = $('#bean_zzsj_' + index).val();
    if (zzsj) {
        var zzsjDate = new Date(Date.parse(zzsj.replace(/-/g, "/")));
        zzsj = zzsjDate.Format("yyyy-MM-dd");
        var nDate = new Date();
        var endDate = nDate.Format("yyyy-MM-dd");
        var centDate = nDate.addDayNumByDate(-15, 'yyyy-MM-dd');
        if (zzsj < centDate) {
            endDate = zzsjDate.addDayNumByDate(15, 'yyyy-MM-dd');
        }
        $('#bean_expiretime_' + index).val(endDate);
        var beginDate = new Date(Date.parse(endDate.replace(/-/g, "/"))).addDayNumByDate(-28, 'yyyy-MM-dd');
        $('#bean_starttime_' + index).val(beginDate);

    }
//    $('#bean_subjecttype_'+oldBeanNum).prop("checked","checked");
//    $('#bean_zhlx_'+index).val("01");
}

var addZnzfSfMx = function () {
    var oldBeanNum = $(".beanData:last").attr("data-bean-num");
    addZnzfBean();
    setDefaultData(oldBeanNum, beanNum);
    updateFlwsParm(oldBeanNum, beanNum);
}

function showFlwsFile(index) {
    var zhxm = $("#shr").val();
    var zhlx = $("#bean_zzfs_" + index).val();
    var zh = $("#bean_accnumber_" + index).val();
    var jgmc = $("#bean_payname_" + index).val();
    if (jgmc && jgmc != '') {
    } else {
        alert("请选择所属机构");
        return false;
    }
    if (zh && zh != '') {
    } else {
        alert("请输入账号");
        return false;
    }
    if (zhlx && zhlx != '') {
    } else {
        zhlx = '支付账号';
    }

    var data = {"index": index, "jgmc": jgmc, "zhxm": zhxm, "zhlx": zhlx, "zh": zh};
    showCxFlws(data);
}

var setJjdZhToJsp = function (data) {
    if (data && data.length > 0) {
        for (var index = 0; index < data.length; index++) {
            if (index == 0) {
//                $("#bean_zzfs_0").val(data[index].zzfs);
                $("#bean_accnumber_0").val(data[index].zh);
                $("#jjdPk_0").val(data[index].jjdpk);
                $("#bean_paycode_0").val(data[index].zhjgdm);
                $("#bean_payname_0").val(data[index].zhjgmc);
                $("#bean_zzsj_0").val(data[index].zcsj);
                //$("#bean_zzsj_0").addClass("hide");
                setDefaultData(0, 0);
            } else {
                addZnzfSfMx();
//                $("#bean_zzfs_"+index).val(data[index].zzfs);
                $("#bean_accnumber_" + index).val(data[index].zh);
                $("#jjdPk_" + index).val(data[index].jjdpk);
                $("#bean_paycode_" + index).val(data[index].zhjgdm);
                $("#bean_payname_" + index).val(data[index].zhjgmc);
                $("#bean_zzsj_" + index).val(data[index].zcsj);
                //$("#bean_zzsj_"+index).addClass("hide");
                setDefaultData(index - 1, index);
            }

        }
    }
}

var sfMxQqEditSubmit = function (btnObj) {
    var flag = $('#sfMxQqEditForm').validate('submitValidate');
    if (flag) {
        showLoading();
        var reqData = $('#sfMxQqEditForm').serializeJsonObject();
        reqData = setListData(reqData);
        reqData = JSON.stringify(reqData);
        $.ajax({
            type: "POST",
            dataType: "json",
            contentType: 'application/json; charset=UTF-8',
            url: ctx + "/znzf/sfMx/add",
            data: reqData,
            success: function (data) {
                hideLoading();
                if (data) {
                    if (data.msg) {
                        if (data.msg == 'true') {
                            alert("操作成功");
                            window.parent.document.getElementById('ifTab_container_zhxxlr').contentWindow.location.reload(true);
                            window.parent.$("#ifTab_close_addsfMx").click();
                        } else {
                            alert(data.msg);
                        }
                    } else {
                        alert("请求异常！");
                    }
                }
            },
            error: function () {
                hideLoading();
                alert("请求异常");
            }
        });
    } else {
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}