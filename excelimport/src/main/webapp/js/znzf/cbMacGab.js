
/*$(function () {
    initCbMacGabGrid();
    formEditValidate('cbMacGabEditForm');//表单验证
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
    $('#editModal').on('hidden.bs.modal', function (event) {
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").removeAttr('disabled');
        $("#editConfirmBtn").removeAttr('disabled','disabled');
        $("#editConfirmBtn").unbind();
        formEditValidateReset('cbMacGabEditForm');
    });
})*/
function initCbMacGabGridPage(){
    $('#initCbMacGabGridPage').bootstrapTable('destroy');

    $("#initCbMacGabGridPage").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/znzf/cbMacGab/listcb',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [500,1000],
        pageSize: 500,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryCbMacGabParam,//参数
        //showColumns: true,//是否显示所有的列（选择显示的列）
        //showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 315,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
        //showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
            //请根据需要修改/删除 需展示字段
            {
                title : "序号",
                field : "Number",
                align : "center",
                width : "5%",
                formatter: function (value, row, index) {
                    var pageSize = $('#initCbMacGabGridPage').bootstrapTable('getOptions').pageSize;     //通过table的#id 得到每页多少条
                    var pageNumber = $('#initCbMacGabGridPage').bootstrapTable('getOptions').pageNumber; //通过table的#id 得到当前第几页
                    return pageSize * (pageNumber - 1) + index + 1;    // 返回每条的序号： 每页条数 *（当前页 - 1 ）+ 序号
                }
            },
            {
                title : "账号",
                field : "qqzh",
                align : "center"
            },
            {
                title : "账号姓名",
                field : "qqzhxm",
                align : "center"
            },
            {
                title : "串并MAC依据",
                field : "macdz",
                align : "center",
                formatter: function (value, row, index) {
                    var str = value.split(",");
                    if (str.length<=1) {
                        return value;
                    } else {
                        return str[0] + "<b class='text-red'>等</b>";
                    }
                }
            },
            {
                title : "数量",
                field : "count",
                align : "center"
            },
            {
                title : "操作",
                field : "pk",
                align : "center",
                valign : "middle",
                width : "150px",
                events:cbMacGabOperateEvents,
                formatter : cbMacGabOperationFormatterPage
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}
function initCbMacGabGrid(){
    $('#cbMacGabGrid').bootstrapTable('destroy');

    $("#cbMacGabGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/znzf/cbMacGab/listcb',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [500,1000],
        pageSize: 500,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryCbMacGabParam,//参数
        //showColumns: true,//是否显示所有的列（选择显示的列）
        //showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 315,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
        //showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
            //请根据需要修改/删除 需展示字段
            {
                title : "序号",
                field : "Number",
                align : "center",
                width : "5%",
                formatter: function (value, row, index) {
                    var pageSize = $('#cbMacGabGrid').bootstrapTable('getOptions').pageSize;     //通过table的#id 得到每页多少条
                    var pageNumber = $('#cbMacGabGrid').bootstrapTable('getOptions').pageNumber; //通过table的#id 得到当前第几页
                    return pageSize * (pageNumber - 1) + index + 1;    // 返回每条的序号： 每页条数 *（当前页 - 1 ）+ 序号
                }
            },
            {
                title : "账号",
                field : "qqzh",
                align : "center"
            },
            {
                title : "账号姓名",
                field : "qqzhxm",
                align : "center"
            },
            {
                title : "串并MAC依据",
                field : "macdz",
                align : "center",
                formatter: function (value, row, index) {
                    var str = value.split(",");
                    if (str.length<=1) {
                        return value;
                    } else {
                        return str[0] + "<b class='text-red'>等</b>";
                    }
                }
            },
            {
                title : "数量",
                field : "count",
                align : "center"
            },
            {
                title : "操作",
                field : "pk",
                align : "center",
                valign : "middle",
                width : "150px",
                events:cbMacGabOperateEvents,
                formatter : cbMacGabOperationFormatter
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}
// 以起始页码方式传入参数,params为table组装参数
function queryCbMacGabParams(params){
    var tmp = $("#cbGabQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    tmp["jjdpk"]=$("#jjdpk").val();
    tmp["qqzh"]=$("#qqzh").val();
    return tmp;
}
function initCbMacGabGrids(){
    $('#cbMacGabGrids').bootstrapTable('destroy');

    $("#cbMacGabGrids").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/znzf/cbMacGab/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [500,1000],
        pageSize: 500,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryCbMacGabParams,//参数
        //showColumns: true,//是否显示所有的列（选择显示的列）
        //showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 315,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
		//showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
        //请根据需要修改/删除 需展示字段
            {
                title : "序号",
                field : "Number",
                align : "center",
                width : "5%",
                formatter: function (value, row, index) {
                    var pageSize = $('#cbMacGabGrids').bootstrapTable('getOptions').pageSize;     //通过table的#id 得到每页多少条
                    var pageNumber = $('#cbMacGabGrids').bootstrapTable('getOptions').pageNumber; //通过table的#id 得到当前第几页
                    return pageSize * (pageNumber - 1) + index + 1;    // 返回每条的序号： 每页条数 *（当前页 - 1 ）+ 序号
                }
            },
            {
                title : "账号",
                field : "qqzh",
                align : "center"
            },

            {
                title : "账号姓名",
                field : "qqzhxm",
                width : "80px",
                align : "center"
            },
            /*{
                title : "账号机构名称",
                field : "qqzhjgmc",
                align : "center"
            },*/
            {
                title : "录入单位名称",
                field : "qqlrdwmc",
                align : "center"
            },
            {
                title : "录入时间",
                field : "lrsj",
                align : "center",
                formatter: function (value, row, index) {
                    return value?new Date(value).Format("yyyy-MM-dd hh:mm:ss"):"";
                }
            },
            {
                title : "MAC地址",
                field : "macdz",
                width : "100px",
                align : "center"
            },
            {
                title : "数量",
                field : "count",
                width : "50px",
                align : "center"
            },
            {
                title : "串中接警单帐号",
                field : "jjdqqzh",
                align : "center"
            },
            {
                title : "简要案情",
                field : "pk",
                align : "center",
                valign : "middle",
                width : "80px",
                events:cbMacGabOperateEvents,
                formatter: function (value, row, index) {
                    var result= "<a href='javascript:void(0);' id='showXqBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";
                    return result;
                }
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}


function queryCbMacGabParam(params){
    var tmp = $("#cbMacGabQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    tmp["jjdpk"]=$("#jjdpk").val();
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
function cbMacGabOperationFormatterPage(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' id='showBtnPage' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";/*onclick=\"showCbMacGab('" + id + "', view='view')\"*/
    /* result += "<a href='javascript:void(0);' id='editBtn' class='btn' title='编辑'><span class='fa fa-edit fa-lg'></span></a>";/!*onclick=\"editCbMacGab('" + id + "')\"*!/
     result += "<a href='javascript:void(0);' id='delBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";/!*onclick=\"deleteCbMacGab('" + id + "')\"*!/*/
    return result;
}
//操作栏的格式化
function cbMacGabOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' id='showBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";/*onclick=\"showCbMacGab('" + id + "', view='view')\"*/
   /* result += "<a href='javascript:void(0);' id='editBtn' class='btn' title='编辑'><span class='fa fa-edit fa-lg'></span></a>";/!*onclick=\"editCbMacGab('" + id + "')\"*!/
    result += "<a href='javascript:void(0);' id='delBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";/!*onclick=\"deleteCbMacGab('" + id + "')\"*!/*/
    return result;
}
//操作栏绑定事件
window.cbMacGabOperateEvents = {
    "click #showBtn":function (e,vale,row,index) {
        var jjdpk = $("#jjdpk").val();
        $("#showModalLabel").text("公安部MAC串并信息详情");
        $("#zcShowIframe").attr("src",ctx+"/znzf/cbMacGab/show?jjdpk="+jjdpk+"&qqzh="+row.qqzh);
        $("#showModal").modal();
    },
    "click #showBtnPage":function (e,vale,row,index) {
        var jjdpk = $("#jjdpk").val();
        $("#showModalLabel").text("公安部MAC串并信息详情");
        $("#zcShowIframe").attr("src",ctx+"/znzf/cbMacGab/show?jjdpk="+jjdpk+"&qqzh="+row.qqzh);
        $("#showModal").modal();
    },
    "click #showXqBtn":function (e,vale,row,index) {
        $(".modal-body p").html(row.sy);
        $(".modal-dialog").stop().slideToggle(300);
    }
    /*"click #editBtn":function (e,vale,row,index) {
    	//设置模态框标题
        $("#editModalLabel").text("**修改");
        //设置操作类型
        $("#edit_curdType").val("update");
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").attr('disabled','disabled');
        
        //请根据需要修改/删除 需展示字段
        //注意：字典类型数据需要dm和mc都进行设置
        $("#edit_pk").val(row.pk);
        $("#edit_sertype").val(row.sertype);
        $("#edit_qqpk").val(row.qqpk);
        $("#edit_jjdpk").val(row.jjdpk);
        $("#edit_ywsqbh").val(row.ywsqbh);
        $("#edit_macdz").val(row.macdz);
        $("#edit_qqzh").val(row.qqzh);
        $("#edit_qqzhxm").val(row.qqzhxm);
        $("#edit_qqzhjgdm").val(row.qqzhjgdm);
        $("#edit_qqzhjgmc").val(row.qqzhjgmc);
        $("#edit_qqlrdwdm").val(row.qqlrdwdm);
        $("#edit_qqlrdwmc").val(row.qqlrdwmc);
        $("#edit_lrsj").val(row.lrsj);
        $("#edit_yxx").val(row.yxx);
        $("#edit_sy").val(row.sy);
        $("#edit_count").val(row.count);

		//模态框的确认(保存)按钮绑定事件
        $("#editConfirmBtn").on("click",function(){
            //curdCbMacGab();
            cbMacGabEditSubmit();
        });
        $("#editModal").modal();
    },
    "click #delBtn":function (e,vale,row,index) {
        $("#tipsModalLabel").html("<i class='fa fa-warning fa-lg' style='color: #c87f0a'></i>警告");//提示标题
        $("#tipsMessage").text("确认删除么？");//设置提示信息
        $("#tipsConfirmBtn").text("删除");//按钮名称
        $("#tipsConfirmBtn").on("click",function(){
            deleteCbMacGab(row.pk);
        });
        $("#tipsModal").modal();
    }*/
}

/*function addCbMacGab() {
    $("#editModalLabel").text("**新增");
    $("#edit_curdType").val("insert");
    
    //请根据需要修改/删除 需展示字段
    //此处设置字段与修改时相同，是为清空点击修改时为元素的赋值
    //注意：字典类型数据需要dm和mc都进行设置
    $("#edit_pk").val('');
    $("#edit_sertype").val('');
    $("#edit_qqpk").val('');
    $("#edit_jjdpk").val('');
    $("#edit_ywsqbh").val('');
    $("#edit_macdz").val('');
    $("#edit_qqzh").val('');
    $("#edit_qqzhxm").val('');
    $("#edit_qqzhjgdm").val('');
    $("#edit_qqzhjgmc").val('');
    $("#edit_qqlrdwdm").val('');
    $("#edit_qqlrdwmc").val('');
    $("#edit_lrsj").val('');
    $("#edit_yxx").val('');
    $("#edit_sy").val('');
    $("#edit_count").val('');
    
    $("#editConfirmBtn").on("click",function(){
        //curdCbMacGab();
        cbMacGabEditSubmit();
    });

    $("#editModal").modal();
}

function deleteCbMacGab(id){
    var reqData={"curdType":"delete","pk":id};
    $("#tipsModal").modal('toggle');
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/znzf/cbMacGab/curd" ,
        data: reqData,
        success: function (result) {
            alert(result.msg);
            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function curdCbMacGab() {
	//防止重复点击
    $("#editConfirmBtn").attr('disabled','disabled');
    var reqData = $('#cbMacGabEditForm').serializeJsonObject();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/znzf/cbMacGab/curd" ,
        data: reqData,
        success : function(data) {
            alert(data.msg);
            $("#editModal").modal('toggle');

            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}*/

function search() {
    //initcbMacGabGrid();
    $("#cbMacGabGrid").bootstrapTable("refresh");
}
function exportCbMacGab(){
    var form = document.cbMacGabQueryForm;
    var url = ctx+"/znzf/cbMacGab/excel";
    form.action=url;
    form.submit();
}
function exportCbMacGabXq(){
    var form = document.cbMacGabQueryFormXq;
    var url = ctx+"/znzf/cbMacGab/excel";
    form.action=url;
    form.submit();
}
var cbMacGabEditSubmit = function(){
    var flag = $('#cbMacGabEditForm').validate('submitValidate');
    if (flag){
        curdCbMacGab();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}

