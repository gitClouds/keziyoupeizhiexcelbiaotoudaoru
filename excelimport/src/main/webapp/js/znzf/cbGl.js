
/*$(function () {
    initCbGlGrid();
    formEditValidate('cbGlEditForm');//表单验证
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
    $('#editModal').on('hidden.bs.modal', function (event) {
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").removeAttr('disabled');
        $("#editConfirmBtn").removeAttr('disabled','disabled');
        $("#editConfirmBtn").unbind();
        formEditValidateReset('cbGlEditForm');
    });
})*/

function initCbGlGrid(){
    $('#xtncb').bootstrapTable('destroy');

    $("#xtncb").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/znzf/cbGl/listCb',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [500,1000],
        pageSize: 500,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryCbGlParams,//参数
        //showColumns: true,//是否显示所有的列（选择显示的列）
        //showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 315,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        //toolbar: '#toolbar',              //工具按钮用哪个容器
        //toolbarAlign:"left",
		//showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
        //请根据需要修改/删除 需展示字段
            {
                title : "序号",
                field : "Number",
                align : "center",
                width : "5%",
                formatter: function (value, row, index) {
                    var pageSize = $('#xtncb').bootstrapTable('getOptions').pageSize;     //通过table的#id 得到每页多少条
                    var pageNumber = $('#xtncb').bootstrapTable('getOptions').pageNumber; //通过table的#id 得到当前第几页
                    return pageSize * (pageNumber - 1) + index + 1;    // 返回每条的序号： 每页条数 *（当前页 - 1 ）+ 序号
                }
            },
            {
                title : "接警单号",
                field : "JJDHM",
                align : "center"
            },
            {
                title : "案发时间",
                field : "AFSJ",
                align : "center",
                formatter: function (value, row, index) {
                    return value?new Date(value).Format("yyyy-MM-dd hh:mm:ss"):"";
                }
            },
            {
                title : "报案人",
                field : "BARXM",
                align : "center"
            },
            {
                title : "受理单位",
                field : "SLDWMC",
                align : "center"
            },
            {
                title : "串并依据",
                field : "CBYJ",
                align : "center",
                formatter: function (value, row, index) {
                    var str = value.split(",");
                    if (str.length<=1) {
                        return value;
                    } else {
                        return str[0] + "<b class='text-red'>等</b>";
                    }
                }
            },
            {
                title : "操作",
                field : "JJDPK",
                align : "center",
                valign : "middle",
                width : "150px",
                events:cbGlOperateEvents,
                formatter : cbGlOperationFormatter
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}
// 以起始页码方式传入参数,params为table组装参数
function queryCbJltParams(params){
    var tmp = $("#cbJltQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    tmp["jjdpk"]=$("#jjdpk").val();
    tmp["cbyj"]=$("#cbyj").val();
    return tmp;
}
function cbGlJltGabGrids(){
    $('#cbGlJltGabGrids').bootstrapTable('destroy');

    $("#cbGlJltGabGrids").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/znzf/cbGl/showCbJltPage',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [500,1000],
        pageSize: 500,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryCbJltParams,//参数
        //showColumns: true,//是否显示所有的列（选择显示的列）
        //showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 315,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        //toolbar: '#toolbar',              //工具按钮用哪个容器
        //toolbarAlign:"left",
        //showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
            //请根据需要修改/删除 需展示字段
            {
                title : "序号",
                field : "Number",
                align : "center",
                width : "5%",
                formatter: function (value, row, index) {
                    var pageSize = $('#cbGlJltGabGrids').bootstrapTable('getOptions').pageSize;     //通过table的#id 得到每页多少条
                    var pageNumber = $('#cbGlJltGabGrids').bootstrapTable('getOptions').pageNumber; //通过table的#id 得到当前第几页
                    return pageSize * (pageNumber - 1) + index + 1;    // 返回每条的序号： 每页条数 *（当前页 - 1 ）+ 序号
                }
            },
            {
                title : "账/卡号",
                field : "cardnumber",
                align : "center"
            },
            {
                title : "账号姓名",
                field : "accountname",
                align : "center"
            },
            {
                title : "所属机构",
                field : "bankname",
                align : "center",
                /*formatter: function (value, row, index) {
                    return value?new Date(value).Format("yyyy-MM-dd hh:mm:ss"):"";
                }*/
            },

            {
                title : "上级账号",
                field : "parentzh",
                align : "center"
            },
            {
                title : "级别",
                field : "nlevel",
                align : "center",
               /* formatter: function (value, row, index) {
                    var str = value.split(",");
                    if (str.length<=1) {
                        return value;
                    } else {
                        return str[0] + "<b class='text-red'>等</b>";
                    }
                }*/
            }/*,
            {
                title : "操作",
                field : "JJDPK",
                align : "center",
                valign : "middle",
                width : "150px",
                events:cbGlOperateEvents,
                formatter : cbGlOperationFormatter
            }*/
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}
/*function initCbGlGrid(){
    $('#cbGlGrid').bootstrapTable('destroy');

    $("#cbGlGrid").bootstrapTable({
        method:"get",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        //contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/znzf/cbGl/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryCbGlParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
        showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
            //请根据需要修改/删除 需展示字段
            {
                title : "主键PK",
                field : "pk",
                align : "center"
            },
            {
                title : "串并编号",
                field : "cbbh",
                align : "center"
            },
            {
                title : "卡号所属类型：1-银行，2-三方",
                field : "sertype",
                align : "center"
            },
            {
                title : "接警单PK",
                field : "jjdpk",
                align : "center"
            },
            {
                title : "卡号所属机构代码",
                field : "bankcode",
                align : "center"
            },
            {
                title : "卡号",
                field : "cardnumber",
                align : "center"
            },
            {
                title : "录入人单位代码",
                field : "lrdwdm",
                align : "center"
            },
            {
                title : "录入人单位名称",
                field : "lrdwmc",
                align : "center"
            },
            {
                title : "录入人姓名",
                field : "lrrxm",
                align : "center"
            },
            {
                title : "录入人警号",
                field : "lrrjh",
                align : "center"
            },
            {
                title : "录入时间",
                field : "lrsj",
                align : "center"
            },
            {
                title : "字段注释",
                field : "rksj",
                align : "center"
            },
            {
                title : "字段注释",
                field : "yxx",
                align : "center"
            },
            {
                title : "字段注释",
                field : "jltpk",
                align : "center"
            },
            {
                title : "操作",
                field : "pk",
                align : "center",
                valign : "middle",
                width : "150px",
                events:cbGlOperateEvents,
                formatter : cbGlOperationFormatter
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}*/

// 以起始页码方式传入参数,params为table组装参数
function queryCbGlParams(params){
    var tmp = $("#cbGlQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    tmp["jjdpk"]=$("#jjdpk").val();
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function cbGlOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' id='showBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";/*onclick=\"showCbGl('" + id + "', view='view')\"*/
    /*result += "<a href='javascript:void(0);' id='editBtn' class='btn' title='编辑'><span class='fa fa-edit fa-lg'></span></a>";/!*onclick=\"editCbGl('" + id + "')\"*!/
    result += "<a href='javascript:void(0);' id='delBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";/!*onclick=\"deleteCbGl('" + id + "')\"*!/*/

    return result;
}
function yaykXq(jjdpk) {
    var item = {'id':'yaykShowCb'+jjdpk,'name':'一案一库详情','url':ctx+'/yayk/show?pk='+jjdpk};
    iframeTab.parentAddIframe(item);
}
//操作栏绑定事件
window.cbGlOperateEvents = {
    "click #showBtn":function (e,vale,row,index) {
        //var jjdpk = $("#jjdpk").val();
        $("#showModalLabel").text("系统内串并详情").append('<span class="btn btn-info pull-right" onclick="yaykXq(\''+row.JJDPK+'\')" style="padding: 5px;">一案一库</span>');
        $("#zcShowIframe").attr("src",ctx+"/znzf/cbGl/showCbJlt?jjdpk="+row.JJDPK+"&cbyj="+row.CBYJ);
        $("#showModal").modal();
        /*var item = {'id':'yaykShowCb'+row.jjdpk,'name':'一案一库详情','url':ctx+'/yayk/show?pk='+row.jjdpk};
        iframeTab.parentAddIframe(item);*/
    }
    /* "click #showBtn":function (e,vale,row,index) {
    	$("#showModalLabel").text("**详情");
    	//请根据需要修改/删除 需展示字段
        $("#show_pk").text(row.pk);
        $("#show_cbbh").text(row.cbbh);
        $("#show_sertype").text(row.sertype);
        $("#show_jjdpk").text(row.jjdpk);
        $("#show_bankcode").text(row.bankcode);
        $("#show_cardnumber").text(row.cardnumber);
        $("#show_lrdwdm").text(row.lrdwdm);
        $("#show_lrdwmc").text(row.lrdwmc);
        $("#show_lrrxm").text(row.lrrxm);
        $("#show_lrrjh").text(row.lrrjh);
        $("#show_lrsj").text(row.lrsj);
        $("#show_rksj").text(row.rksj);
        $("#show_yxx").text(row.yxx);
        $("#show_jltpk").text(row.jltpk);

        $("#showModal").modal();
    },
   "click #editBtn":function (e,vale,row,index) {
    	//设置模态框标题
        $("#editModalLabel").text("**修改");
        //设置操作类型
        $("#edit_curdType").val("update");
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").attr('disabled','disabled');

        //请根据需要修改/删除 需展示字段
        //注意：字典类型数据需要dm和mc都进行设置
        $("#edit_pk").val(row.pk);
        $("#edit_cbbh").val(row.cbbh);
        $("#edit_sertype").val(row.sertype);
        $("#edit_jjdpk").val(row.jjdpk);
        $("#edit_bankcode").val(row.bankcode);
        $("#edit_cardnumber").val(row.cardnumber);
        $("#edit_lrdwdm").val(row.lrdwdm);
        $("#edit_lrdwmc").val(row.lrdwmc);
        $("#edit_lrrxm").val(row.lrrxm);
        $("#edit_lrrjh").val(row.lrrjh);
        $("#edit_lrsj").val(row.lrsj);
        $("#edit_rksj").val(row.rksj);
        $("#edit_yxx").val(row.yxx);
        $("#edit_jltpk").val(row.jltpk);

		//模态框的确认(保存)按钮绑定事件
        $("#editConfirmBtn").on("click",function(){
            //curdCbGl();
            cbGlEditSubmit();
        });
        $("#editModal").modal();
    },
    "click #delBtn":function (e,vale,row,index) {
        $("#tipsModalLabel").html("<i class='fa fa-warning fa-lg' style='color: #c87f0a'></i>警告");//提示标题
        $("#tipsMessage").text("确认删除么？");//设置提示信息
        $("#tipsConfirmBtn").text("删除");//按钮名称
        $("#tipsConfirmBtn").on("click",function(){
            deleteCbGl(row.pk);
        });
        $("#tipsModal").modal();
    }*/
}

function addCbGl() {
    $("#editModalLabel").text("**新增");
    $("#edit_curdType").val("insert");
    
    //请根据需要修改/删除 需展示字段
    //此处设置字段与修改时相同，是为清空点击修改时为元素的赋值
    //注意：字典类型数据需要dm和mc都进行设置
    $("#edit_pk").val('');
    $("#edit_cbbh").val('');
    $("#edit_sertype").val('');
    $("#edit_jjdpk").val('');
    $("#edit_bankcode").val('');
    $("#edit_cardnumber").val('');
    $("#edit_lrdwdm").val('');
    $("#edit_lrdwmc").val('');
    $("#edit_lrrxm").val('');
    $("#edit_lrrjh").val('');
    $("#edit_lrsj").val('');
    $("#edit_rksj").val('');
    $("#edit_yxx").val('');
    $("#edit_jltpk").val('');
    
    $("#editConfirmBtn").on("click",function(){
        //curdCbGl();
        cbGlEditSubmit();
    });

    $("#editModal").modal();
}

function deleteCbGl(id){
    var reqData={"curdType":"delete","pk":id};
    $("#tipsModal").modal('toggle');
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/znzf/cbGl/curd" ,
        data: reqData,
        success: function (result) {
            alert(result.msg);
            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function curdCbGl() {
	//防止重复点击
    $("#editConfirmBtn").attr('disabled','disabled');
    var reqData = $('#cbGlEditForm').serializeJsonObject();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/znzf/cbGl/curd" ,
        data: reqData,
        success : function(data) {
            alert(data.msg);
            $("#editModal").modal('toggle');

            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function search() {
    //initcbGlGrid();
    $("#cbGlGrid").bootstrapTable("refresh");
}
function exportCbGl(){
    var form = document.cbGlQueryForm;
    var url = ctx+"/znzf/cbGl/excel";
    form.action=url;
    form.submit();
}

var cbGlEditSubmit = function(){
    var flag = $('#cbGlEditForm').validate('submitValidate');
    if (flag){
        curdCbGl();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}

