/**
 * Created by Administrator on 2019/3/6/006.
 */
$(function () {
    checkDzz("ifTab_close_addsfLs");//验证调证字、经办人
    var iszcygzs = $("#iszcygzs").val();
    if (iszcygzs>=1){}else {
        getJjdZh($("#fwType").val(), $("#xxType").val(), $("#jjdPk").val());//获取接警单相关的账号信息
    }
    formEditValidate('sfLsQqEditForm');//表单验证
})


var showZhlb = function (obj) {
    var _obj = $(obj);
    var objId = _obj.attr("id");
    var index = objId.charAt(objId.length-1);
    var acctype = $("#bean_acctype_"+index).val();

    if(_obj && _obj.val()!="" && acctype && acctype=='A1'){
        $("#div_bank_"+index).removeClass("hide");
    }else{
        if(!$("#div_bank_"+index).hasClass("hide")){
            $("#div_bank_"+index).addClass("hide");
        }
    }
}

var updateAccType = function(oldBeanNum,index){
    var reg= new RegExp(oldBeanNum,'g');
    var div_bank_ = $("#bean_bankid_"+index).parent().parent();
    var div_bank_id = div_bank_.attr("id");
    div_bank_id = div_bank_id.replace(reg,index);
    div_bank_.attr("id",div_bank_id);
}

var addZnzfsfLs = function () {
    var oldBeanNum = $(".beanData:last").attr("data-bean-num");
    addZnzfBean();
    updateFlwsParm(oldBeanNum,beanNum);
    updateAccType(oldBeanNum,beanNum);
    showZhlb($("#bean_acctype_"+beanNum)[0]);
}

function showFlwsFile(index) {
    var zhlx = $("#bean_accName_"+index).val();
    var zh = $("#bean_accnumber_"+index).val();
    var jgmc = $("#bean_payname_"+index).val();
    if (jgmc && jgmc!=''){
    }else{
        alert("请选择所属机构");
        return false;
    }
    if (zh && zh!=''){
    }else{
        alert("请输入账号");
        return false;
    }
    if (zhlx && zhlx!=''){
    }else{
        alert("请选择账号类型");
        return false;
    }

    var data = {"index":index,"jgmc":jgmc,"zhxm":"","zhlx":zhlx,"zh":zh};
    showCxFlws(data);
}

var setJjdZhToJsp = function (data) {
    if (data && data.length>0){
        for(var index =0;index<data.length;index++){
            if (index == 0){
                $("#bean_accnumber_0").val(data[index].zh);

                $("#bean_acctype_0").val(data[index].acctype);
                $("#bean_accName_0").val(data[index].accName);
                $("#bean_trandate_0").val(data[index].zcsj);
                $("#bean_zzsj_0").val(data[index].zcsj);
            }else{
                addZnzfsfLs();
                $("#bean_accnumber_"+index).val(data[index].zh);

                $("#bean_acctype_"+index).val(data[index].acctype);
                $("#bean_accName_"+index).val(data[index].accName);

                $("#bean_trandate_"+index).val(data[index].zcsj);
                $("#bean_zzsj_"+index).val(data[index].zcsj);
            }
            showZhlb($("#bean_accName_"+index)[0]);
        }
    }
}

var sfLsQqEditSubmit = function (btnObj) {
    var flag = $('#sfLsQqEditForm').validate('submitValidate');
    if (flag){
        flag = checkForm();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
    if (flag){
        showLoading();
        var reqData = $('#sfLsQqEditForm').serializeJsonObject();
        reqData = setListData(reqData);
        reqData = JSON.stringify(reqData);
        $.ajax({
            type: "POST",
            dataType: "json",
            contentType: 'application/json; charset=UTF-8',
            url: ctx+"/znzf/sfLs/add" ,
            data: reqData,
            success : function(data) {
                hideLoading();
                if(data){
                    if(data.msg){
                        if(data.msg=='true'){
                            alert("操作成功");
                            window.parent.$("#ifTab_close_addsfLs").click();
                        }else{
                            alert(data.msg);
                        }
                    }else{
                        alert("请求异常！");
                    }
                }
            },
            error : function() {
                hideLoading();
                alert("请求异常");
            }
        });
    }
}

var checkForm= function () {
    for (var i=0;i<=beanNum;i++){
        var acctype = $("#bean_acctype_"+i).val();
        if (acctype && acctype=='A1'){
            var bankid = $("#bean_bankid_"+i).val();
            if (bankid && bankid!=''){
            }else{
                alert("银行机构不能为空");
                return false;
            }
        }
    }
    return true;
}