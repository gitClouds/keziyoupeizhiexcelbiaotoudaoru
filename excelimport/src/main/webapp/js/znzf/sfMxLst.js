/**
 * Created by Administrator on 2019/3/27/027.
 */
$(function () {
    initSfMxJgLstGrid();
})

function initSfMxJgLstGrid() {
    $('#sfMxJgLstGrid').bootstrapTable('destroy');

    $("#sfMxJgLstGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url: ctx + '/znzf/sfMxJgLst/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [5,10, 20, 50, 100],
        pageSize: 5,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: querySfMxJgLstParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 410,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign: "left",
        showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns: [
            {
                title : "序号",
                field : "Number",
                align : "center",
                width : "50px",
                formatter: function (value, row, index) {
                    var pageSize = $('#sfMxJgLstGrid').bootstrapTable('getOptions').pageSize;     //通过table的#id 得到每页多少条
                    var pageNumber = $('#sfMxJgLstGrid').bootstrapTable('getOptions').pageNumber; //通过table的#id 得到当前第几页
                    return pageSize * (pageNumber - 1) + index + 1;    // 返回每条的序号： 每页条数 *（当前页 - 1 ）+ 序号
                }
            },
            {
                title: "出入账",
                field: "jdbz",
                align: "center",
                width : "80px",
                formatter:function (value, row, index) {
                    if (value=='0'){
                        return '出账';
                    }else if (value=='1'){
                        return '入账';
                    }
                }
            },
            {
                title: "交易时间",
                field: "jysj",
                align: "center",
                width : "200px",
            },
            {
                title: "交易金额",
                field: "jyje",
                align: "center",
                width : "100px",
            },
            {
                title: "收款支付帐号",
                field: "skzfzh",
                align: "center"
            },
            {
                title: "收款银行卡",
                field: "skyhzh",
                align: "center"
            },
            {
                title: "付款支付帐号",
                field: "fkzfzh",
                align: "center",
                visible: false
            },
            {
                title: "付款银行卡",
                field: "fkyhzh",
                align: "center",
            },
            {
                title: "余额",
                field: "ye",
                align: "center",
                visible: false
            },
            {
                title: "交易类型",
                field: "jylx",
                align: "center",
                visible: false
            },
            {
                title: "支付类型",
                field: "zflx",
                align: "center",
                visible: false
            },
            {
                title: "交易流水号",
                field: "jylsh",
                align: "center",
                visible: false
            },
            {
                title: "收款银行卡所属银行",
                field: "skyhjgmc",
                align: "center",
                visible: false
            },
            {
                title: "消费POS机",
                field: "possbh",
                align: "center",
                visible: false
            },
            {
                title: "收款商户号",
                field: "skshm",
                align: "center",
                visible: false
            },
            {
                title: "收款商户名称",
                field: "skfshmc",
                align: "center",
                visible: false
            },
            {
                title: "付款银行卡所属银行",
                field: "fkyhjgmc",
                align: "center",
                visible: false
            },
            {
                title: "设备类型",
                field: "jysblx",
                align: "center",
                visible: false,
                formatter:function (value, row, index) {
                    if (value=='1'){
                        return '电脑';
                    }else if (value=='2'){
                        return '手机';
                    }else{
                        return '其他';
                    }
                }
            },
            {
                title: "支付订单号",
                field: "zfddh",
                align: "center",
                visible: false
            },
            {
                title: "支付设备IP",
                field: "zfsbip",
                align: "center",
                visible: false
            },
            {
                title: "MAC地址",
                field: "macdz",
                align: "center",
                visible: false
            },
            {
                title: "交易经度",
                field: "jyddjd",
                align: "center",
                visible: false
            },
            {
                title: "交易纬度",
                field: "jyddwd",
                align: "center",
                visible: false
            },
            {
                title: "交易设备号",
                field: "jysbh",
                align: "center",
                visible: false
            },
            {
                title: "外部流水号",
                field: "yhwblsh",
                align: "center",
                visible: false
            },
            {
                title: "备注",
                field: "bz",
                align: "center",
                visible: false
            }
        ],
        responseHandler: responseHandler//请求数据成功后，渲染表格前的方法
    });
}
// 以起始页码方式传入参数,params为table组装参数
function querySfMxJgLstParams(params){
    var tmp = $("#testQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    tmp["qqpk"]=$("#pk").val();
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}

function exportSfMxJgLst() {
    var url = ctx+"/znzf/sfMxJgLst/excel?qqpk="+$("#pk").val()+"&accnumber="+$("#accnumber").val();
    window.open(url);
}