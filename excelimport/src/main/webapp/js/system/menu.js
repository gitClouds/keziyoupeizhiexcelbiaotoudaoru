
$(function () {
    initMenuGrid();
    formEditValidate('menuEditForm');
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
    $('#editModal').on('hidden.bs.modal', function (event) {
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").removeAttr('disabled');
        $("#editConfirmBtn").removeAttr('disabled','disabled');
        $("#editConfirmBtn").unbind();
        formEditValidateReset('menuEditForm');
    });
})

function initMenuGrid(){
    $('#menuGrid').bootstrapTable('destroy');

    $("#menuGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/system/menu/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryMenuParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
		showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
        //请根据需要修改/删除 需展示字段
            {
                title : "菜单代码",
                field : "code",
                align : "center"
            },
            {
                title : "菜单名称",
                field : "name",
                align : "center"
            },
            {
                title : "icon",
                field : "icon",
                align : "center",
                formatter:function (value, row, index) {
                    if (value!=''){
                        return '<i class="fa '+value+' fa-lg"></i>';
                    }else{
                        return '-';
                    }
                }
            },
            {
                title : "菜单ID",
                field : "pageId",
                align : "center"
            },
            {
                title : "是否包含子节点",
                field : "isparent",
                align : "center",
                formatter:function (value, row, index) {
                    if (value=='1'){
                        return '是';
                    }else{
                        return '否';
                    }
                }
            },
            {
                title : "URL路径",
                field : "url",
                align : "center"
            },
            {
                title : "上级代码",
                field : "parentCode",
                align : "center"
            },
            {
                title : "菜单级别",
                field : "codeLevel",
                align : "center"
            },
            {
                title : "排序序号",
                field : "px",
                align : "center"
            },
            {
                title : "菜单描述",
                field : "comments",
                align : "center"
            },
            {
                title : "操作",
                field : "pk",
                align : "center",
                valign : "middle",
                width : "150px",
                events:menuOperateEvents,
                formatter : menuOperationFormatter
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function queryMenuParams(params){
    var tmp = $("#menuQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function menuOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' id='showBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";/*onclick=\"showMenu('" + id + "', view='view')\"*/
    result += "<a href='javascript:void(0);' id='editBtn' class='btn' title='编辑'><span class='fa fa-edit fa-lg'></span></a>";/*onclick=\"editMenu('" + id + "')\"*/
    result += "<a href='javascript:void(0);' id='delBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";/*onclick=\"deleteMenu('" + id + "')\"*/

    return result;
}
//操作栏绑定事件
window.menuOperateEvents = {
    "click #showBtn":function (e,vale,row,index) {
    	//请根据需要修改/删除 需展示字段
        $("#show_pk").text(row.pk);
        $("#show_code").text(row.code);
        $("#show_name").text(row.name);
        $("#show_icon").text(row.icon);
        $("#show_pageId").text(row.pageId);
        $("#show_isparent").text(row.isparent);
        $("#show_url").text(row.url);
        $("#show_parentCode").text(row.parentCode);
        $("#show_codeLevel").text(row.codeLevel);
        $("#show_px").text(row.px);
        $("#show_comments").text(row.comments);

        $("#showModalLabel").text("菜单详情");
        $("#showModal").modal();
    },
    "click #editBtn":function (e,vale,row,index) {
    	//设置模态框标题
        $("#editModalLabel").text("菜单修改");
        //设置操作类型
        $("#edit_curdType").val("update");
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").attr('disabled','disabled');
        
        //请根据需要修改/删除 需展示字段
        //注意：字典类型数据需要dm和mc都进行设置
        $("#edit_pk").val(row.pk);
        $("#edit_code").val(row.code);
        $("#edit_name").val(row.name);
        $("#edit_icon").val(row.icon);
        $("#edit_pageId").val(row.pageId);
        $("#edit_isparent").val(row.isparent);
        $("#edit_url").val(row.url);
        $("#edit_parentCode").val(row.parentCode);
        $("#edit_codeLevel").val(row.codeLevel);
        $("#edit_px").val(row.px);
        $("#edit_comments").val(row.comments);

		//模态框的确认(提交)按钮绑定事件
        $("#editConfirmBtn").on("click",function(){
            //curdMenu();
            menuEditSubmit();
        });
        $("#editModal").modal();
    },
    "click #delBtn":function (e,vale,row,index) {
        $("#tipsModalLabel").html("<i class='fa fa-warning fa-lg' style='color: #c87f0a'></i>警告");//提示标题
        $("#tipsMessage").text("确认删除么？");//设置提示信息
        $("#tipsConfirmBtn").text("删除");//按钮名称
        $("#tipsConfirmBtn").on("click",function(){
            deleteMenu(row.pk);
        });
        $("#tipsModal").modal();
    }
}

function addMenu() {
    $("#editModalLabel").text("菜单新增");
    $("#edit_curdType").val("insert");
    
    //请根据需要修改/删除 需展示字段
    //此处设置字段与修改时相同，是为清空点击修改时为元素的赋值
    //注意：字典类型数据需要dm和mc都进行设置
    $("#edit_pk").val('');
    $("#edit_code").val('');
    $("#edit_name").val('');
    $("#edit_icon").val('');
    $("#edit_pageId").val('');
    $("#edit_isparent").val('');
    $("#edit_url").val('');
    $("#edit_parentCode").val('');
    $("#edit_codeLevel").val('');
    $("#edit_px").val('');
    $("#edit_comments").val('');

    $("#editConfirmBtn").on("click",function(){
        //curdMenu();
        menuEditSubmit();
    });

    $("#editModal").modal();
}

function deleteMenu(id){
    var reqData={"curdType":"delete","pk":id};
    $("#tipsModal").modal('toggle');
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/system/menu/curd" ,
        data: reqData,
        success: function (result) {
            alert(result.msg);
            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function curdMenu() {
	//防止重复点击
    $("#editConfirmBtn").attr('disabled','disabled');
    var reqData = $('#menuEditForm').serializeJsonObject();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/system/menu/curd" ,
        data: reqData,
        success : function(data) {
            alert(data.msg);
            $("#editModal").modal('toggle');

            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function search() {
    //initmenuGrid();
    $("#menuGrid").bootstrapTable("refresh");
}
function exportMenu(){
    var form = document.menuQueryForm;
    var url = ctx+"/system/menu/excel";
    form.action=url;
    form.submit();
}

var menuEditSubmit = function(){
    var flag = $('#menuEditForm').validate('submitValidate');
    if (flag){
        curdMenu();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}


