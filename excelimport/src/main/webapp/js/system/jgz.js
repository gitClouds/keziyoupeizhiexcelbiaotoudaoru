
$(function () {
    initJgzGrid();
    formEditValidate('jgzEditForm');
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
    $('#editModal').on('hidden.bs.modal', function (event) {
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").removeAttr('disabled');
        $("#editConfirmBtn").removeAttr('disabled','disabled');
        $("#editConfirmBtn").unbind();
        $("#edit_jbr").html('');
        $("#jbr_mongo").val('');
        $("#edit_xcr").html('');
        $("#xcr_mongo").val('');
        formEditValidateReset('jgzEditForm');
    });
})

function initJgzGrid(){
    $('#jgzGrid').bootstrapTable('destroy');

    $("#jgzGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/system/jgz/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryJgzParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
		showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
        //请根据需要修改/删除 需展示字段
            {
                title : "用户姓名",
                field : "username",
                align : "center"
            },
            {
                title : "单位名称",
                field : "dwmc",
                align : "center"
            },
            {
                title : "经办人警号",
                field : "jbrzjhm",
                align : "center"
            },
            {
                title : "经办人姓名",
                field : "jbrxm",
                align : "center"
            },
            {
                title : "经办人电话",
                field : "jbrdh",
                align : "center"
            },
            {
                title : "协查人警号",
                field : "xcrzjhm",
                align : "center"
            },
            {
                title : "协查人姓名",
                field : "xcrxm",
                align : "center"
            },
            {
                title : "协查人电话",
                field : "xcrdh",
                align : "center"
            },
            {
                title : "入库时间",
                field : "rksj",
                align : "center",
                formatter: function (value, row, index) {
                    return value?new Date(value).Format("yyyy-MM-dd hh:mm:ss"):"";
                }
            },
            {
                title : "录入单位",
                field : "lrdwmc",
                align : "center"
            },
            {
                title : "操作",
                field : "pk",
                align : "center",
                valign : "middle",
                width : "150px",
                events:jgzOperateEvents,
                formatter : jgzOperationFormatter
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function queryJgzParams(params){
    var tmp = $("#jgzQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function jgzOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' id='showBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";/*onclick=\"showJgz('" + id + "', view='view')\"*/
    //result += "<a href='javascript:void(0);' id='editBtn' class='btn' title='编辑'><span class='fa fa-edit fa-lg'></span></a>";/*onclick=\"editJgz('" + id + "')\"*/
    result += "<a href='javascript:void(0);' id='delBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";/*onclick=\"deleteJgz('" + id + "')\"*/

    return result;
}
//操作栏绑定事件
window.jgzOperateEvents = {
    "click #showBtn":function (e,vale,row,index) {
    	$("#showModalLabel").text("经办人详情");
    	//请根据需要修改/删除 需展示字段
        $("#show_pk").text(row.pk);
        $("#show_username").text(row.username);
        $("#show_dwmc").text(row.dwmc);
        $("#show_jbrzjhm").text(row.jbrzjhm);
        $("#show_jbrxm").text(row.jbrxm);
        $("#show_jbrdh").text(row.jbrdh);
        $("#show_xcrzjhm").text(row.xcrzjhm);
        $("#show_xcrxm").text(row.xcrxm);
        $("#show_xcrdh").text(row.xcrdh);
        $("#show_rksj").text(row.rksj?new Date(row.rksj).Format("yyyy-MM-dd hh:mm:ss"):"");
        $("#show_lrdwmc").text(row.lrdwmc);

        fileUpload.showImgs([ctx+"/attachment/download?pk="+row.jbrmongo],'show_jbr');

        fileUpload.showImgs([ctx+"/attachment/download?pk="+row.xcrmongo],'show_xcr');

        $("#showModal").modal();
    },
    /*"click #editBtn":function (e,vale,row,index) {
    	//设置模态框标题
        $("#editModalLabel").text("经办人修改");
        //设置操作类型
        $("#edit_curdType").val("update");
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").attr('disabled','disabled');
        
        //请根据需要修改/删除 需展示字段
        //注意：字典类型数据需要dm和mc都进行设置
        $("#edit_pk").val(row.pk);
        $("#edit_userpk").val(row.userpk);
        $("#edit_username").val(row.username);
        $("#edit_dwdm").val(row.dwdm);
        $("#edit_dwmc").val(row.dwmc);
        $("#edit_jbrzjhm").val(row.jbrzjhm);
        $("#edit_jbrxm").val(row.jbrxm);
        $("#edit_jbrdh").val(row.jbrdh);
        $("#edit_jbrmongo").val(row.jbrmongo);
        $("#edit_xcrzjhm").val(row.xcrzjhm);
        $("#edit_xcrxm").val(row.xcrxm);
        $("#edit_xcrdh").val(row.xcrdh);
        $("#edit_xcrmongo").val(row.xcrmongo);

		//模态框的确认(提交)按钮绑定事件
        $("#editConfirmBtn").on("click",function(){
            //curdJgz();
            jgzEditSubmit();
        });
        $("#editModal").modal();
    },*/
    "click #delBtn":function (e,vale,row,index) {
        $("#tipsModalLabel").html("<i class='fa fa-warning fa-lg' style='color: #c87f0a'></i>警告");//提示标题
        $("#tipsMessage").text("确认删除么？");//设置提示信息
        $("#tipsConfirmBtn").text("删除");//按钮名称
        $("#tipsConfirmBtn").on("click",function(){
            deleteJgz(row.pk);
        });
        $("#tipsModal").modal();
    }
}

function addJgz() {
    $("#editModalLabel").text("经办人新增");
    $("#edit_curdType").val("insert");
    
    //请根据需要修改/删除 需展示字段
    //此处设置字段与修改时相同，是为清空点击修改时为元素的赋值
    //注意：字典类型数据需要dm和mc都进行设置
    $("#edit_pk").val('');
    $("#edit_dwdm").val('');
    $("#edit_dwmc").val('');
    $("#edit_jbrzjhm").val('');
    $("#edit_jbrxm").val('');
    $("#edit_jbrdh").val('');
    $("#edit_jbrmongo").val('');
    $("#edit_xcrzjhm").val('');
    $("#edit_xcrxm").val('');
    $("#edit_xcrdh").val('');
    $("#edit_xcrmongo").val('');

    $("#edit_jbr").append('<input type="file" class="file"  id="jbr_file" name="jbr" >');
    fileUpload.initImgInput('jbr','jbr_file',ctx+'/attachment/upload');

    $("#edit_xcr").append('<input type="file" class="file"  id="xcr_file" name="xcr" >');
    fileUpload.initImgInput('xcr','xcr_file',ctx+'/attachment/upload');

    $("#editConfirmBtn").on("click",function(){
        //curdJgz();
        //jgzEditSubmit();
        var dwdm=$("#edit_dwdm").val();
        checkJgz(dwdm);
    });

    $("#editModal").modal();
}
function checkJgz(dwdm){
    var reqData={"dwdm":dwdm};
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/system/jgz/checkJgz" ,
        data: reqData,
        async:false,
        success: function (result) {
            if(result==true){
                if(dwdm){
                    alert("当前单位的经办人、协查人已录入，请勿重复录入");
                }else{
                    alert("您已录入经办人、协查人警官证，请勿重复录入");
                }

            }else{
                jgzEditSubmit();
            }
        },
        error : function() {
            alert("请求异常");
        }
    });
}
function deleteJgz(id){
    var reqData={"curdType":"delete","pk":id};
    $("#tipsModal").modal('toggle');
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/system/jgz/curd" ,
        data: reqData,
        success: function (result) {
            alert(result.msg);
            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function curdJgz() {
	//防止重复点击
    $("#editConfirmBtn").attr('disabled','disabled');
    var reqData = $('#jgzEditForm').serializeJsonObject();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/system/jgz/curd" ,
        data: reqData,
        success : function(data) {
            alert(data.msg);
            $("#editModal").modal('toggle');

            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function search() {
    //initjgzGrid();
    $("#jgzGrid").bootstrapTable("refresh");
}
function exportJgz(){
    var form = document.jgzQueryForm;
    var url = ctx+"/system/jgz/excel";
    form.action=url;
    form.submit();
}

var jgzEditSubmit = function(){
    var flag = $('#jgzEditForm').validate('submitValidate');
    if (flag){
        $('#jbr_file').fileinput('upload');
        $('#xcr_file').fileinput('upload');
        //curdJgz();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}
var fileSucc = 0;
var setMongoPk = function(data){
    fileSucc+=1;
    if(data.extra.eff=='jbr'){
        $('#jbr_mongo').val(data.response.jbr_mongo);
    }
    if(data.extra.eff=='xcr'){
        $('#xcr_mongo').val(data.response.xcr_mongo);
    }
    //
    if(fileSucc==2){
        fileSucc = 0;
        curdJgz();
    }
}
