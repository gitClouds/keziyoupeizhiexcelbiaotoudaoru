
$(function () {
    initRoleGrid();
    formEditValidate('roleEditForm');//表单验证
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsRoleModal').on('hidden.bs.modal', function (event) {
        $("#tipsRoleConfirmBtn").unbind();
    });
    $('#editRoleModal').on('hidden.bs.modal', function (event) {
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").removeAttr('disabled');
        $("#editRoleConfirmBtn").removeAttr('disabled','disabled');
        $("#editRoleConfirmBtn").unbind();
        formEditValidateReset('roleEditForm');
    });
})

function initRoleGrid(){
    $('#roleGrid').bootstrapTable('destroy');

    $("#roleGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/system/role/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryRoleParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#roleToolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
        showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
        //请根据需要修改/删除 需展示字段
            {
                title : "角色代码",
                field : "code",
                align : "center"
            },
            {
                title : "角色名称",
                field : "name",
                align : "center"
            },
            {
                title : "角色描述",
                field : "comments",
                align : "center"
            },
            {
                title : "操作",
                field : "pk",
                align : "center",
                valign : "middle",
                width : "150px",
                events:roleOperateEvents,
                formatter : roleOperationFormatter
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function queryRoleParams(params){
    var tmp = $("#roleQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function roleOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' id='showRoleBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";/*onclick=\"showRole('" + id + "', view='view')\"*/
    result += "<a href='javascript:void(0);' id='editRoleBtn' class='btn' title='编辑'><span class='fa fa-edit fa-lg'></span></a>";/*onclick=\"editRole('" + id + "')\"*/
    result += "<a href='javascript:void(0);' id='delRoleBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";/*onclick=\"deleteRole('" + id + "')\"*/

    return result;
}
//操作栏绑定事件
window.roleOperateEvents = {
    "click #showRoleBtn":function (e,vale,row,index) {
    	//请根据需要修改/删除 需展示字段
        $("#show_role_code").text(row.code);
        $("#show_role_name").text(row.name);
        $("#show_role_comments").text(row.comments);

        $("#showRoleModalLabel").text("角色详情");
        $("#showRoleModal").modal();
    },
    "click #editRoleBtn":function (e,vale,row,index) {
    	//设置模态框标题
        $("#editRoleModalLabel").text("角色修改");
        //设置操作类型
        $("#edit_role_curdType").val("update");
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").attr('disabled','disabled');
        
        //请根据需要修改/删除 需展示字段
        //注意：字典类型数据需要dm和mc都进行设置
        $("#edit_role_pk").val(row.pk);
        $("#edit_role_code").val(row.code);
        $("#edit_role_name").val(row.name);
        $("#edit_role_comments").val(row.comments);

		//模态框的确认(提交)按钮绑定事件
        $("#editRoleConfirmBtn").on("click",function(){
            //curdRole();
            roleEditSubmit();
        });
        $("#editRoleModal").modal();
    },
    "click #delRoleBtn":function (e,vale,row,index) {
        $("#tipsRoleModalLabel").html("<i class='fa fa-warning fa-lg' style='color: #c87f0a'></i>警告");//提示标题
        $("#tipsRoleMessage").text("确认删除么？");//设置提示信息
        $("#tipsRoleConfirmBtn").text("删除");//按钮名称
        $("#tipsRoleConfirmBtn").on("click",function(){
            deleteRole(row.pk);
        });
        $("#tipsRoleModal").modal();
    }
}

function addRole() {
    $("#editRoleModalLabel").text("角色新增");
    $("#edit_role_curdType").val("insert");
    
    //请根据需要修改/删除 需展示字段
    //此处设置字段与修改时相同，是为清空点击修改时为元素的赋值
    //注意：字典类型数据需要dm和mc都进行设置
    $("#edit_role_pk").val('');
    $("#edit_role_code").val('');
    $("#edit_role_name").val('');
    $("#edit_role_comments").val('');

    $("#editRoleConfirmBtn").on("click",function(){
        //curdRole();
        roleEditSubmit();
    });

    $("#editRoleModal").modal();
}

function deleteRole(id){
    var reqData={"curdType":"delete","pk":id};
    $("#tipsRoleModal").modal('toggle');
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/system/role/curd" ,
        data: reqData,
        success: function (result) {
            alert(result.msg);
            searchRole();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function curdRole() {
	//防止重复点击
    $("#editRoleConfirmBtn").attr('disabled','disabled');
    var reqData = $('#roleEditForm').serializeJsonObject();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/system/role/curd" ,
        data: reqData,
        success : function(data) {
            alert(data.msg);
            $("#editRoleModal").modal('toggle');

            searchRole();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function searchRole() {
    $("#roleGrid").bootstrapTable("refresh");
}
function exportRole(){
    var form = document.roleQueryForm;
    var url = ctx+"/system/role/excel";
    form.action=url;
    form.submit();
}

var roleEditSubmit = function(){
    var flag = $('#roleEditForm').validate('submitValidate');
    if (flag){
        curdRole();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}