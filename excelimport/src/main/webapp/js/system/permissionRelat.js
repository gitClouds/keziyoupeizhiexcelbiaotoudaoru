var modalInt = 0;
$(function () {

    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#editModal').on('hidden.bs.modal', function (event) {
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").removeAttr('disabled');
        $("#editConfirmBtn").removeAttr('disabled','disabled');
        $("#editConfirmBtn").unbind();
    });
})

function initPermissionRelatGrid(){
    $('#permissionRelatGrid').bootstrapTable('destroy');

    $("#permissionRelatGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/system/permission/relatList',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [100,200,500],
        pageSize: 100,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'client',//设置为服务器端分页
        queryParams: queryPermissionParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 500,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        //toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
        showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        checkbox: true,
        //得到查询的参数
        columns :[
            //请根据需要修改/删除 需展示字段
            {
                align:"center",
                checkbox:true,
                formatter:stateFormatter
            },
            {
                title : "权限代码",
                field : "code",
                align : "center"
            },
            {
                title : "权限名称",
                field : "name",
                align : "center"
            },
            {
                title : "权限描述",
                field : "comments",
                align : "center"
            }
        ]
        //responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });

    //绑定保存按钮事件
}
function stateFormatter(value, row, index) {
    var ret = {checked:false,disabled:false};
    if (row.ckd === 'TRUE') {
        ret.checked = true;
    }else{
        ret.checked = false;
    }
    if (row.dis === 'TRUE') {
        ret.disabled = true;
    }else{
        ret.disabled = false;
    }

    return ret;
}


// 以起始页码方式传入参数,params为table组装参数
function queryPermissionParams(params){
    var tmp = $("#permissionRelatForm").serializeJsonObject();
    return tmp;
}

function searchPermRelat() {
    if (modalInt==0){
        modalInt+=1;
        initPermissionRelatGrid();
    }else {
        $("#permissionRelatGrid").bootstrapTable("refresh");
    }
}

function grantPerm(type,code,permType) {
    if (permType==='0'){
        $("#editModalLabel").text("禁止权限");
    }else{
        $("#editModalLabel").text("授权");
    }

    $("#forKey").val(code);
    $("#relatType").val(type);
    $("#relatCheckedCode").val('');
    $("#permType").val(permType);

    $("#editConfirmBtn").on("click",function(){
        grantPermSubmit();
    });

    searchPermRelat();

    $("#editModal").modal();

}

function grantPermSubmit() {
    $("#editConfirmBtn").attr('disabled','disabled');
    //获取选中内容
    var checkedRow= $("#permissionRelatGrid").bootstrapTable('getSelections');

    var relatCheckedCode = '';

    if (checkedRow.length>0){
        for (var i=0;i<checkedRow.length;i++){
            relatCheckedCode+=checkedRow[i]['code']+',';
        }
        relatCheckedCode = relatCheckedCode.substring(0,relatCheckedCode.length-1);
    }
    $("#relatCheckedCode").val(relatCheckedCode);

    var reqData =  $('#permissionRelatForm').serializeJsonObject();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/system/permission/relatPerm" ,
        data: reqData,
        success: function (result) {
            alert(result.msg);
            $("#editModal").modal('toggle');
        },
        error : function() {
            alert("请求异常");
        }
    });

}
