$(function () {
    initUserGrid();
    formEditValidate('userEditForm');
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
    $('#editModal').on('hidden.bs.modal', function (event) {
        $("#edit_password").removeAttr('disabled');
        $("#editConfirmBtn").removeAttr('disabled','disabled');
        $("#editConfirmBtn").unbind();
        formEditValidateReset('userEditForm');
    });
    $('#grantRoleModal').on('hidden.bs.modal', function (event) {
        $("#grantRoleConfirmBtn").removeAttr('disabled');
        $("#grantRoleConfirmBtn").unbind();
    });
})

function initUserGrid(){
    $('#userGrid').bootstrapTable('destroy');

    $("#userGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/system/user/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryUserParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#userToolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
        showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
            {
                title : "账号",
                field : "username",
                align : "center"
            },
            {
                title : "姓名",
                field : "xm",
                align : "center"
            },
            {
                title : "身份证号",
                field : "idno",
                align : "center"
            },
            {
                title : "警号",
                field : "jh",
                align : "center"
            },
            {
                title : "联系方式",
                field : "lxfs",
                align : "center"
            },
            {
                title : "所属机构",
                field : "jgmc",
                align : "center"
            },
            {
                title : "操作",
                field : "pk",
                align : "center",
                valign : "middle",
                width : "160px",
                events:userOperateEvents,
                formatter : userOperationFormatter
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}
// 以起始页码方式传入参数,params为table组装参数
function queryUserParams(params){
    /*var tmp = {
        "pageSize": params.limit,
        "pageNumber": params.offset+1,
        "username":$("#username").val(),
        "xm":$("#xm").val(),
        "deptCode":$("#deptCode").val(),
        "lxfs":$("#lxfs").val()
    }*/
    var tmp = $("#userQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}

//操作栏的格式化
function userOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' id='showBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";/*onclick=\"showUser('" + id + "', view='view')\"*/
    result += "<a href='javascript:void(0);' id='editBtn' class='btn' title='编辑'><span class='fa fa-edit fa-lg'></span></a>";/*onclick=\"editUser('" + id + "')\"*/
    result += "<a href='javascript:void(0);' id='delBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";/*onclick=\"deleteUser('" + id + "')\"*/
    result += "<a href='javascript:void(0);' id='setRoleBtn' class='btn' title='分配角色'><span class='fa fa-id-card fa-lg'></span></a>";/*onclick=\"deleteUser('" + id + "')\"*/

    return result;
}
window.userOperateEvents = {
    "click #showBtn":function (e,vale,row,index) {
        //showUser(row.pk);
        $("#show_username").text(row.username);
        $("#show_xm").text(row.xm);
        $("#show_idno").text(row.idno);
        $("#show_jgmc").text(row.jgmc);
        $("#show_jgdm").text(row.jgdm);
        $("#show_jh").text(row.jh);
        $("#show_lxfs").text(row.lxfs);
        $("#show_outtime").text(row.outtime?new Date(row.outtime).Format("yyyy-MM-dd hh:mm:ss"):"");

        $("#showModal").modal();
    },
    "click #editBtn":function (e,vale,row,index) {
        //editUser(row);
        $("#editModalLabel").text("修改用户");
        $("#edit_curdType").val("update");

        $("#edit_username").val(row.username);
        $("#edit_password").attr('disabled','disabled');
        $("#edit_xm").val(row.xm);
        $("#edit_idno").val(row.idno);
        $("#edit_jh").val(row.jh);
        $("#edit_lxfs").val(row.lxfs);
        $("#edit_jgdm").val(row.jgdm);
        $("#edit_jgmc").val(row.jgmc);
        $("#edit_pk").val(row.pk);
        $("#edit_outtime").val(row.outtime?new Date(row.outtime).Format("yyyy-MM-dd hh:mm:ss"):row.outtime);

        $("#editConfirmBtn").on("click",function(){
            userEditSubmit();
        });
        $("#editModal").modal();
    },
    "click #delBtn":function (e,vale,row,index) {
        $("#tipsModalLabel").html("<i class='fa fa-warning fa-lg' style='color: #c87f0a'></i>警告");//提示标题
        $("#tipsMessage").text("确认删除： " +row.xm + " ?");//设置提示信息
        $("#tipsConfirmBtn").text("删除");//按钮名称
        $("#tipsConfirmBtn").on("click",function(){
            deleteUser(row.pk);
        });
        $("#tipsModal").modal();
    },
    "click #setRoleBtn":function (e,value,row,index) {
        $("#userPk").val(row.pk);

        $("#grantRoleConfirmBtn").on("click",function(){
            grantRoleToUser();
        });
        searchUserRole();
        $("#grantRoleModal").modal();
    }
}

function addUser() {
    $("#editModalLabel").text("新增用户");
    $("#edit_curdType").val("insert");
    $("#edit_username").val('');
    $("#edit_password").val('');
    $("#edit_xm").val('');
    $("#edit_idno").val('');
    $("#edit_jh").val('');
    $("#edit_outtime").val('');
    $("#edit_lxfs").val('');
    $("#edit_jgdm").val('');
    $("#edit_jgmc").val('');
    $("#edit_pk").val('');
    $("#editConfirmBtn").on("click",function(){
        //curdUser();
        userEditSubmit();
    });

    $("#editModal").modal();
}

function deleteUser(id){
    var reqData={"curdType":"delete","pk":id};
    $("#tipsModal").modal('toggle');
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/system/user/curd" ,
        data: reqData,
        success: function (result) {
            alert(result.msg);
            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function curdUser() {
    $("#editConfirmBtn").attr('disabled','disabled');
    var reqData = $('#userEditForm').serializeJsonObject();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/system/user/curd" ,
        data: reqData,
        success : function(data) {
            alert(data.msg);
            $("#editModal").modal('toggle');

            search();
            /*var json = JSON.parse(data);
            $("#jrjq").html(json.jqxx.jrjq);
            $("#czjq").html(json.jqxx.czjq);
            $(".runNumA").html(json.jqxx.ssje);
            $(".runNumB").html(json.jqxx.zfje);
            $(".runNumC").html(json.jqxx.djje);*/
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function search() {
    $("#userGrid").bootstrapTable("refresh");
}

function exportUser(){
    var form = document.userQueryForm;
    var url = ctx+"/system/user/excel";
    form.action=url;
    form.submit();
}

var userEditSubmit = function(){
    var flag = $('#userEditForm').validate('submitValidate');
    if (flag){
        curdUser();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}
var roleModalInt = 0;
function initUserRoleGrid(){
    $('#grantRoleGrid').bootstrapTable('destroy');

    $("#grantRoleGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/system/user/roleList',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [100,200],
        pageSize: 100,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'client',//设置为服务器端分页
        queryParams: queryUserRoleParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 300,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        //toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
        showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        checkbox: true,
        //得到查询的参数
        columns :[
            //请根据需要修改/删除 需展示字段
            {
                align:"center",
                checkbox:true,
                formatter:stateRoleFormatter
            },
            {
                title : "角色代码",
                field : "code",
                align : "center"
            },
            {
                title : "角色名称",
                field : "name",
                align : "center"
            },
            {
                title : "角色描述",
                field : "comments",
                align : "center"
            }
        ]
        //responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });

    //绑定保存按钮事件
}
function stateRoleFormatter(value, row, index) {
    var ret = {checked:false,disabled:false};
    if (row.ckd === 'TRUE') {
        ret.checked = true;
    }else{
        ret.checked = false;
    }
    return ret;
}
// 以起始页码方式传入参数,params为table组装参数
function queryUserRoleParams(params){
    var tmp = $("#grantRoleForm").serializeJsonObject();
    return tmp;
}

var searchUserRole = function () {
    if (roleModalInt==0){
        roleModalInt+=1;
        initUserRoleGrid();
    }else {
        $("#grantRoleGrid").bootstrapTable("refresh");
    }
}

var grantRoleToUser = function () {
    $("#grantRoleConfirmBtn").attr('disabled','disabled');

    //获取选中内容
    var checkedRow= $("#grantRoleGrid").bootstrapTable('getSelections');

    var relatCheckedCode = '';

    if (checkedRow.length>0){
        for (var i=0;i<checkedRow.length;i++){
            relatCheckedCode+=checkedRow[i]['pk']+',';
        }
        relatCheckedCode = relatCheckedCode.substring(0,relatCheckedCode.length-1);
    }
    $("#checkedRoles").val(relatCheckedCode);
    var reqData = $('#grantRoleForm').serializeJsonObject();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/system/user/grantRole" ,
        data: reqData,
        success: function (result) {
            alert(result.msg);
            $("#grantRoleModal").modal('toggle');
        },
        error : function() {
            alert("请求异常");
        }
    });

}