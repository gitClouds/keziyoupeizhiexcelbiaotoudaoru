$(function () {
    $('#tipModal').on('hidden.bs.modal', function (event) {
        $("#tipConfirmBtn").unbind();
    });
    $('#cModal').on('hidden.bs.modal', function (event) {
        $("#cConfirmBtn").unbind();
    });
})
function resetPwd() {
    $("#tipModal").modal('toggle');
    $("#tipModalLabel").html("<i class='fa fa-warning fa-lg' style='color: #c87f0a'></i>警告");//提示标题
    $("#tipMessage").text("您确认重置密码么？重置后密码还原为初始密码。");//设置提示信息
    $("#tipConfirmBtn").on("click",function(){
        resetUserPwd();
    });
    $("#tipModal").modal();
}
function resetUserPwd() {
    $("#tipModal").modal('toggle');
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/system/user/resetPwd" ,
        success: function (result) {
            alert(result.msg+"\n"+"请重新登录。");
            logout();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function showChangePwdModal() {
    $("#cModal").modal('toggle');
    $("#cConfirmBtn").on("click",function(){
        changePwdSubmit();
    });
    $("#cModal").modal();
}

function changePwdSubmit() {
    var flag = $('#changePwdForm').validate('submitValidate');
    if (flag){
        var old_password = $('#old_password').val();
        var password = $('#password').val();
        var conf_password = $('#conf_password').val();
        if (old_password===password){
            alert("新密码与旧密码相同。");
        }else{
            if (!(conf_password===password)){
                alert("两次输入密码不一致，请重新输入。");
            }else{
                changePwd();
            }
        }
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}

function changePwd() {
    var reqData = $('#changePwdForm').serializeJsonObject();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/system/user/changePwd" ,
        data: reqData,
        success: function (result) {
            alert(result.msg+"\n"+"请重新登录。");
            $("#cModal").modal('toggle');
            logout();
        },
        error : function() {
            alert("请求异常");
        }
    });
}
