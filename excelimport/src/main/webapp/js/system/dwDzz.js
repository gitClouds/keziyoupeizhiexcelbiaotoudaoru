
$(function () {
    initDwDzzGrid();
    formEditValidate('dwDzzEditForm');
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
    $('#editModal').on('hidden.bs.modal', function (event) {
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").removeAttr('disabled');
        $("#editConfirmBtn").removeAttr('disabled','disabled');
        $("#editConfirmBtn").unbind();
        //清除fileupload
        $("#edit_cxz").html('');
        $("#cxz_mongo").val('');
        $("#edit_zfz").html('');
        $("#zfz_mongo").val('');
        formEditValidateReset('dwDzzEditForm');
    });
})

function initDwDzzGrid(){
    $('#dwDzzGrid').bootstrapTable('destroy');

    $("#dwDzzGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/system/dwDzz/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryDwDzzParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
		showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
        //请根据需要修改/删除 需展示字段
            {
                title : "单位代码",
                field : "dwdm",
                align : "center"
            },
            {
                title : "单位名称",
                field : "dwmc",
                align : "center"
            },
            {
                title : "单位简称",
                field : "dwjc",
                align : "center"
            },
            {
                title : "调证年份",
                field : "dzyear",
                align : "center"
            },
            {
                title : "调证字号",
                field : "dzz",
                align : "center"
            },
            {
                title : "录入单位",
                field : "lrdwmc",
                align : "center"
            },
            {
                title : "录入时间",
                field : "rksj",
                align : "center",
                formatter: function (value, row, index) {
                    return value?new Date(value).Format("yyyy-MM-dd hh:mm:ss"):"";
                }
            },
            {
                title : "修改时间",
                field : "xgsj",
                align : "center",
                formatter: function (value, row, index) {
                    return value?new Date(value).Format("yyyy-MM-dd hh:mm:ss"):"";
                }
            },
            {
                title : "操作",
                field : "pk",
                align : "center",
                valign : "middle",
                width : "150px",
                events:dwDzzOperateEvents,
                formatter : dwDzzOperationFormatter
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function queryDwDzzParams(params){
    var tmp = $("#dwDzzQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function dwDzzOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' id='showBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";/*onclick=\"showDwDzz('" + id + "', view='view')\"*/
    result += "<a href='javascript:void(0);' id='delBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";/*onclick=\"deleteDwDzz('" + id + "')\"*/

    return result;
}
//操作栏绑定事件
window.dwDzzOperateEvents = {
    "click #showBtn":function (e,vale,row,index) {
    	$("#showModalLabel").text("调证字详情");
    	//请根据需要修改/删除 需展示字段
        $("#show_dwmc").text(row.dwmc);
        $("#show_dwjc").text(row.dwjc);
        $("#show_dzyear").text(row.dzyear);
        $("#show_dzz").text(row.dzz);

        $("#show_lrdwmc").text(row.lrdwmc);
        $("#show_rksj").text(row.rksj?new Date(row.rksj).Format("yyyy-MM-dd hh:mm:ss"):"");
        $("#show_xgdwmc").text(row.xgdwmc);
        $("#show_xgsj").text(row.xgsj?new Date(row.xgsj).Format("yyyy-MM-dd hh:mm:ss"):"");

        fileUpload.showImgs([ctx+"/attachment/download?pk="+row.cxz],'show_cxz');

        fileUpload.showImgs([ctx+"/attachment/download?pk="+row.zfz],'show_zfz');

        $("#showModal").modal();
    },
    "click #delBtn":function (e,vale,row,index) {
        $("#tipsModalLabel").html("<i class='fa fa-warning fa-lg' style='color: #c87f0a'></i>警告");//提示标题
        $("#tipsMessage").text("确认删除么？");//设置提示信息
        $("#tipsConfirmBtn").text("删除");//按钮名称
        $("#tipsConfirmBtn").on("click",function(){
            deleteDwDzz(row.pk);
        });
        $("#tipsModal").modal();
    }
}

function addDwDzz() {
    $("#editModalLabel").text("调证字新增");
    $("#edit_curdType").val("insert");
    
    //请根据需要修改/删除 需展示字段
    //此处设置字段与修改时相同，是为清空点击修改时为元素的赋值
    //注意：字典类型数据需要dm和mc都进行设置
    $("#edit_pk").val('');
    $("#edit_dwdm").val('');
    $("#edit_dwmc").val('');
    $("#edit_dwjc").val('');
    $("#edit_cxz").val('');
    $("#edit_zfz").val('');

    $("#edit_cxz").append('<input type="file" class="file"  id="cxz_file" name="cxz_m" >');
    fileUpload.initPngInput('cxz','cxz_file',ctx+'/attachment/upload');

/*    $("#edit_zfz").append('<input type="file" class="file"  id="zfz_file" name="zfz_m" >');
    fileUpload.initPngInput('zfz','zfz_file',ctx+'/attachment/upload');*/
    $("#editConfirmBtn").on("click",function(){
        //curdDwDzz();
        checkDwDzz($("#edit_dwdm").val());
    });

    $("#editModal").modal();
}
function checkDwDzz(dwdm){
    var reqData={"dwdm":dwdm};
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/system/dwDzz/checkDwDzz" ,
        data: reqData,
        async:false,
        success: function (result) {
            if(result==true){
                alert("此机构调证字已存在，请勿重复录入");
            }else{
                dwDzzEditSubmit();
            }
        },
        error : function() {
            alert("请求异常");
        }
    });
}
function deleteDwDzz(id){
    var reqData={"curdType":"delete","pk":id};
    $("#tipsModal").modal('toggle');
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/system/dwDzz/curd" ,
        data: reqData,
        success: function (result) {
            alert(result.msg);
            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function curdDwDzz() {
	//防止重复点击
    $("#editConfirmBtn").attr('disabled','disabled');
    var reqData = $('#dwDzzEditForm').serializeJsonObject();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/system/dwDzz/curd" ,
        data: reqData,
        success : function(data) {
            alert(data.msg);
            $("#editModal").modal('toggle');

            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function search() {
    //initdwDzzGrid();
    $("#dwDzzGrid").bootstrapTable("refresh");
}
function exportDwDzz(){
    var form = document.dwDzzQueryForm;
    var url = ctx+"/system/dwDzz/excel";
    form.action=url;
    form.submit();
}

var dwDzzEditSubmit = function(){
    var flag = $('#dwDzzEditForm').validate('submitValidate');
    if (flag){
        $('#cxz_file').fileinput('upload');
        // $('#zfz_file').fileinput('upload');
        // curdDwDzz();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}

var fileSucc = 0;
var setMongoPk = function(data){
    fileSucc+=1;
    if(data.extra.eff=='cxz'){
        $('#cxz_mongo').val(data.response.cxz_m_mongo);
    }
   /* if(data.extra.eff=='zfz'){
        $('#zfz_mongo').val(data.response.zfz_m_mongo);
    }*/
    //
    if(fileSucc==1){
        fileSucc = 0;
        curdDwDzz();
    }
}
