/**
 * Created by Administrator on 2019/3/20/020.
 */
$(function () {
    formEditValidate('metaEditForm');//表单验证
})

var metaEditSubmit = function(obj){
    var flag = $('#metaEditForm').validate('submitValidate');
    if (flag){
        curdMeta(obj);
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}

function curdMeta(obj) {
	var jysjb=$("#jysjb").val();
	var jysje=$("#jysje").val();
	if(jysjb >= jysje){
		 alert('交易时间段:前段时间不能大于等于后端时间！');
		 return;
	}
    //防止重复点击
    $(obj).attr('disabled','disabled');
    var reqData = $('#metaEditForm').serializeJsonObject();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/system/meta/setUpMeta" ,
        data: reqData,
        success : function(data) {
            alert(data.msg);
            window.parent.$("#ifTab_close_znzfMeta").click();
        },
        error : function() {
            $(obj).attr('disabled','');
            alert("请求异常");
        }
    });
}