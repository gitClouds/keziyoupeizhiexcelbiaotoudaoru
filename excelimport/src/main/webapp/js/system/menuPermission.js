
$(function () {
    initMenuGrid();
})

function initMenuGrid(){
    $('#menuPremGrid').bootstrapTable('destroy');

    $("#menuPremGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/system/menu/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryMenuParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        //toolbar: '#roleToolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
        showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
            //请根据需要修改/删除 需展示字段
            {
                title : "菜单代码",
                field : "code",
                align : "center"
            },
            {
                title : "菜单名称",
                field : "name",
                align : "center"
            },
            {
                title : "icon",
                field : "icon",
                align : "center",
                formatter:function (value, row, index) {
                    if (value!=''){
                        return '<i class="fa '+value+' fa-lg"></i>';
                    }else{
                        return '-';
                    }
                }
            },
            {
                title : "菜单ID",
                field : "pageId",
                align : "center"
            },
            {
                title : "是否包含子节点",
                field : "isparent",
                align : "center",
                formatter:function (value, row, index) {
                    if (value=='1'){
                        return '是';
                    }else{
                        return '否';
                    }
                }
            },
            {
                title : "URL路径",
                field : "url",
                align : "center"
            },
            {
                title : "上级代码",
                field : "parentCode",
                align : "center"
            },
            {
                title : "菜单级别",
                field : "codeLevel",
                align : "center"
            },
            {
                title : "排序序号",
                field : "px",
                align : "center"
            },
            {
                title : "菜单描述",
                field : "comments",
                align : "center"
            },
            {
                title : "操作",
                field : "pk",
                align : "center",
                valign : "middle",
                width : "150px",
                formatter : menuPermOperationFormatter
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function queryMenuParams(params){
    var tmp = $("#menuPermQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function menuPermOperationFormatter(value, row, index) {
    var result = "";
    result += "<a href='javascript:void(0);' onclick=\"grantPerm('MENU','" + row.code + "','1')\" class='btn' title='授权'><span class='fa fa-unlock fa-lg'></span></a>";/*onclick=\"editRole('" + id + "')\"*/

    return result;
}

function search() {
    $("#menuPremGrid").bootstrapTable("refresh");
}


