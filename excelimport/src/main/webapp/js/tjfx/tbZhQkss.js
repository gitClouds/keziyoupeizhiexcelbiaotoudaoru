
$(function () {
    initTbZhQkssGrid();
    formEditValidate('tbZhQkssEditForm');//表单验证
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
    $('#editModal').on('hidden.bs.modal', function (event) {
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").removeAttr('disabled');
        $("#editConfirmBtn").removeAttr('disabled','disabled');
        $("#editConfirmBtn").unbind();
        formEditValidateReset('tbZhQkssEditForm');
    });
})

function initTbZhQkssGrid(){
    $('#tbZhQkssGrid').bootstrapTable('destroy');

    $("#tbZhQkssGrid").bootstrapTable({
        method:"get",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        //contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/tjfx/tbZhQkss/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryTbZhQkssParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
		showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
        //请根据需要修改/删除 需展示字段
            {
                title : "账号",
                field : "zh",
                align : "center"
            },
            {
                title : "姓名",
                field : "xm",
                align : "center"
            },
            {
                title : "证件号码",
                field : "zjhm",
                align : "center"
            },
            {
                title : "证件类型",
                field : "zjlx",
                align : "center"
            },
            {
                title : "手机号",
                field : "sjh",
                align : "center"
            },
            {
                title : "账户机构名称",
                field : "jgmc",
                align : "center"
            },
            {
                title : "账号类型",
                field : "zhlx",
                align : "center"
            },
            {
                title : "业务类型",
                field : "ywlx",
                align : "center",
                formatter:function (value, row, index) {
                    var str = '-';
                    if (value=='yhkMx'||value=='sfMx'){
                        str = '明细查询';
                    }else if (value=='yhkZt'||value=='sfZt'){
                        str = '主体查询';
                    }else if (value=='sfQzh') {
                        str = '全账户查询';
                    }
                    return str;
                }
            },
            {
                title : "录入人姓名",
                field : "ywfsrxm",
                align : "center"
            },
            {
                title : "录入人单位名称",
                field : "ywfsrdwmc",
                align : "center"
            },
            {
                title : "详情",
                align : "center",
                valign : "middle",
                width : "80px",
                formatter : tbZhQkssOperationFormatter
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function queryTbZhQkssParams(params){
    var tmp = $("#tbZhQkssQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function tbZhQkssOperationFormatter(value, row, index) {
    var pk = row.qqpk;
    var ywlx = row.ywlx;
    var zh = row.zh;
    var resflag = row.resflag;
    var result = "";
    if (resflag==0){
        result = '暂未反馈';
    }else if(resflag==1 || resflag==2){
        result += "<a href='javascript:void(0);' id='showBtn' class='btn' onclick='queryxq(\""+pk+"\",\""+ywlx+"\",\""+zh+"\")' title='详情'><i class='fa fa-search fa-lg'></i></a>";
    }
    return result;
}
//操作栏绑定事件
function queryxq(pk,ywlx,zh){
    if (ywlx=='yhkMx'||ywlx=='sfMx'){
        $("#showModalLabel").text("明细信息");
        $("#zcShowIframe").attr("src",ctx+"/znzf/base/show?zcType="+ywlx+"&pk="+pk+"&zh="+zh);
        $("#showModal").modal();
    }else if (ywlx=='yhkZt'||ywlx=='sfZt'){
        $("#showModalLabel").text("主体信息");
        $("#zcShowIframe").attr("src",ctx+"/znzf/base/show?zcType="+ywlx+"&pk="+pk+"&zh="+zh);
        $("#showModal").modal();
    }else if (ywlx=='sfQzh') {
        $("#showModalLabel").text("全账号信息");
        $("#zcShowIframe").attr("src",ctx+"/znzf/base/show?zcType=sfQzh&pk="+pk+"&zh="+zh);
        $("#showModal").modal();
    }
}

function addTbZhQkss() {
    $("#editModalLabel").text("**新增");
    $("#edit_curdType").val("insert");
    
    //请根据需要修改/删除 需展示字段
    //此处设置字段与修改时相同，是为清空点击修改时为元素的赋值
    //注意：字典类型数据需要dm和mc都进行设置
    $("#edit_zh").val('');
    $("#edit_xm").val('');
    $("#edit_zjhm").val('');
    $("#edit_zjlx").val('');
    $("#edit_jgdm").val('');
    $("#edit_jgmc").val('');
    $("#edit_zhlx").val('');
    $("#edit_ywlx").val('');
    $("#edit_ywfsrxm").val('');
    $("#edit_ywfsrdwmc").val('');
    
    $("#editConfirmBtn").on("click",function(){
        //curdTbZhQkss();
        tbZhQkssEditSubmit();
    });

    $("#editModal").modal();
}


function curdTbZhQkss() {
	//防止重复点击
    $("#editConfirmBtn").attr('disabled','disabled');
    var reqData = $('#tbZhQkssEditForm').serializeJsonObject();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/tjfx/tbZhQkss/curd" ,
        data: reqData,
        success : function(data) {
            alert(data.msg);
            $("#editModal").modal('toggle');

            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function search() {
    //inittbZhQkssGrid();
    var zh = $("#zh").val();
    var zjhm = $("#zjhm").val().toUpperCase();
    var xm = $("#xm").val();
    var sjh = $("#sjh").val();

    if ((zh==null||zh=='')&&(zjhm==null||zjhm=='')&&(xm==null||xm=='')&&(sjh==null||sjh=='')){
        alert("请输入查询条件");
        return;
    }else {
        if (zh!=null&&zh!=''){
            var zhpatten = /^[a-zA-Z0-9\.\_\-\@]+$/ ;
            if(zhpatten.test(zh) == false){
                alert("请输入正确的账号");
                return;
            }
        }else if(zjhm!=null&&zjhm!=''){
            var zjpatten = /^[0-9X]+$/;
            if(zjpatten.test(zjhm) == false){
                alert("请输入正确的证件号码");
                return;
            }
        }else if(sjh!=null&&sjh!=''){
            var sjhpatten = /^[0-9]+$/;
            if(sjhpatten.test(sjh) == false){
                alert("请输入正确的手机号码");
                return;
            }
        }
    }
    $("#tbZhQkssGrid").bootstrapTable("refresh");
}
function exportTbZhQkss(){
    var form = document.tbZhQkssQueryForm;
    var url = ctx+"/tjfx/tbZhQkss/excel";
    form.action=url;
    form.submit();
}

var tbZhQkssEditSubmit = function(){
    var flag = $('#tbZhQkssEditForm').validate('submitValidate');
    if (flag){
        curdTbZhQkss();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}

