
$(function () {
    initxskGrid();
    formEditValidate('xskEditForm');//表单验证
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
    $('#editModal').on('hidden.bs.modal', function (event) {
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").removeAttr('disabled');
        $("#editConfirmBtn").removeAttr('disabled','disabled');
        $("#editConfirmBtn").unbind();
        formEditValidateReset('xskEditForm');
    });
    initcookie();
})

function initxskGrid(){
    $('#xskGrid').bootstrapTable('destroy');

    $("#xskGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/xsk/xsk/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryxskParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
		showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
        //请根据需要修改/删除 需展示字段
            /*{
                title : "主键",
                field : "pk",
                align : "center",
                visible : false
            },*/
            {
                title : "账号",
                field : "zh",
                align : "center",
                events:xskYaykOperateEvents,
                formatter: xskYaykOperateFormatter
            },
            {
                title : "姓名",
                field : "xm",
                align : "center",
                events: xskryEvents,
                formatter: function (value, row, index) {
                    var xdrypk = row.xdrypk;
                    var sdrypk = row.sdrypk;
                    if ((sdrypk!=null&&sdrypk!=''&&sdrypk!='1') || (xdrypk!=null&&xdrypk!=''&&xdrypk!='1')){
                        return '<a href=\'javascript:void(0);\' class=\'xskry\' title=\'详情\'>' + value + '(前科)</a>';
                    }else {
                        return value;
                    }
                }
            },
            {
                title : "身份证号",
                field : "sfzh",
                align : "center"
            },
            {
                title : "账号机构名称",
                field : "zhjgmc",
                align : "center"
            },
            {
                title : "入库时间",
                field : "rksj",
                align : "center",
                formatter: function (value, row, index) {
                    return value?new Date(value).Format("yyyy-MM-dd hh:mm:ss"):"-";
                }
            },
            {
                title : "录入单位",
                field : "lrdwmc",
                align : "center"
            },
            {
                title : "积分",
                field : "jf",
                align : "center",
                formatter: function (value, row, index) {
                    var str;
                    if (value<3&&value>=0){
                        str = "<b>"+value+"</b>";
                    }else if (6>value&&value>=3){
                        str = "<b style='color: orange'>"+value+"</b>";
                    }else if(value>=6){
                        str = "<b style='color: red'>"+value+"</b>";
                    }
                    return str;
                }
            },
            {
                title : "积分描述",
                field : "jfms",
                align : "center",
                width: 100,
                events:xskjltEvents,
                formatter: function (value, row, index) {
                    var str = '';
                    var gxr = row.gxr;
                    var sdgxr = row.sdgxr;
                    var zje = row.zje;
                    var sdje = row.sdje;
                    var fxf = row.fxf;
                    if (fxf>0){
                        str += "复吸犯+<a href='javascript:void(0);' class='xskjlt' title='详情'>"+2+"</a></br>";
                    }
                    if (sdgxr>0){
                        str += "涉毒关系人+<a href='javascript:void(0);' class='xskjlt' title='详情'>"+2+"</a></br>";
                    }
                    if (sdje>0){
                        str += "涉毒金额+<a href='javascript:void(0);' class='xskjlt' title='详情'>"+2+"</a></br>";
                    }
                    if (gxr>0){
                        str += "多关系人+<a href='javascript:void(0);' class='xskjlt' title='详情'>"+1+"</a></br>";
                    }
                    if (zje>0){
                        str += "转出金额+<a href='javascript:void(0);' class='xskjlt' title='详情'>"+1+"</a>";
                    }
                    return str!=''?str:'-';
                }
            },
            {
                title : "总关系人/涉毒关系人",
                field : "gxr",
                align : "center",
                events: xskgxtEvents,
                formatter: function (value, row, index) {
                    var gxr = row.gxr;
                    var sdgxr = row.sdgxr;
                    var str = "<a href='javascript:void(0);' class='xskgxt' title='详情'>"+gxr+"</a>(人)"+
                        "/<a href='javascript:void(0);' class='xskgxt' title='详情'>"+sdgxr+"</a>(人)";
                    return str;
                }

            },
            {
                title : "总金额/涉毒金额",
                field : "zje",
                align : "center",
                events: xskmxEvents,
                formatter: function (value, row, index) {
                    var zje = row.zje;
                    var sdje = row.sdje;
                    var str = "<a href='javascript:void(0);' class='xskmx' title='详情'>"+zje+"</a>(元)" +
                        "/<a href='javascript:void(0);' class='xskmx' title='详情'>"+sdje+"</a>(元)";
                    return str;
                }
            },
            {
                title : "复吸犯",
                field : "fxf",
                align : "center",
                formatter: function (value, row, index) {
                    var str = '';
                    if (value==0){
                        str = '否';
                    }else if(value==1){
                        str = '是';
                    }
                    return str;
                }
            },
            /*{
                title : "抓获单位",
                field : "zhdwmc",
                align : "center"
            },
            {
                title : "有效性",
                field : "yxx",
                align : "center",
                visible : false
            },
            {
                title : "录入单位代码",
                field : "lrdwdm",
                align : "center",
                visible : false
            },
            {
                title : "抓获单位代码",
                field : "zhdwdm",
                align : "center",
                visible : false
            },
            {
                title : "操作",
                field : "pk",
                align : "center",
                valign : "middle",
                width : "150px",
                events:xskOperateEvents,
                formatter : xskOperationFormatter
            }*/
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}
function xskYaykOperateFormatter(value, row, index) {
    return "<a href='javascript:void(0);' class='xskYayk' title='详情'>"+value+"</a>";
}

window.xskmxEvents = {
    "click .xskmx":function (e,vale,row,index) {
        $("#showModalLabel").text("明细信息");
        var zcType = "";
        var sertype = row.sertype;
        if (sertype != null && sertype != '') {
            if (sertype == 1) {
                zcType = "yhkMx";
            }
            if (sertype == 2) {
                zcType = "sfMx";
            }
        }
        $("#zcShowIframe").attr("src", ctx + "/znzf/base/show?zcType=" + zcType + "&pk=" + row.mxqqpk + "&zh=" + row.zh);
        $("#showModal").modal();
    }
}

window.xskryEvents = {
    "click .xskry":function (e,vale,row,index) {
        var pks='';
        var xdrypk = row.xdrypk;
        var sdrypk = row.sdrypk;
        if (xdrypk!='1'&&xdrypk!=null&&xdrypk!=''){
            pks = xdrypk;
            if (sdrypk!='1'&&sdrypk!=null&&sdrypk!=''){
                pks += ','+sdrypk;
            }
            var item = {'id':'xdryxx','name':'吸毒人员信息','url':ctx+'/jdzh/xdpage?lx=2&pks='+xdrypk};
        }else{
            if (sdrypk!='1'&&sdrypk!=null&&sdrypk!=''){
                pks = sdrypk;
                var item = {'id':'sdryxx','name':'涉毒人员信息','url':ctx+'/jdzh/sdpage?lx=1&pks='+sdrypk};
            }
        }
        iframeTab.parentAddIframe(item);
    }
}

window.xskjltEvents = {
    "click .xskjlt":function (e,vale,row,index) {
        var jjdpk = row.zhpk;
        window.open(ctx + '/znzf/jlt/mindPage.html?jjdpk=' + jjdpk, 'jltWin', '');
    }
}

window.xskgxtEvents = {
    "click .xskgxt":function (e,vale,row,index) {
        var jltpk = row.jltpk;
        var item = {'id':'jdrygxt','name':'人员关系图','url':ctx+'/xsk/xsk/jdrygxt?jltpk='+jltpk};
        iframeTab.parentAddIframe(item);
    }
}

window.xskYaykOperateEvents = {
    "click .xskYayk":function (e,vale,row,index) {
        var zhpk = row.zhpk;
        var nl = row.nlevel;
        var item = {'id':'yaykShow','name':'一案一库详情','url':ctx+'/yayk/show?pk='+zhpk+'#zcGrid'+nl};
        iframeTab.parentAddIframe(item);
    }
}

// 以起始页码方式传入参数,params为table组装参数
function queryxskParams(params){
    var tmp = $("#xskQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function xskOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' id='showBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";/*onclick=\"showxsk('" + id + "', view='view')\"*/
    result += "<a href='javascript:void(0);' id='editBtn' class='btn' title='编辑'><span class='fa fa-edit fa-lg'></span></a>";/*onclick=\"editxsk('" + id + "')\"*/
    result += "<a href='javascript:void(0);' id='delBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";/*onclick=\"deletexsk('" + id + "')\"*/

    return result;
}
//操作栏绑定事件
window.xskOperateEvents = {
    "click #showBtn":function (e,vale,row,index) {
    	$("#showModalLabel").text("**详情");
    	//请根据需要修改/删除 需展示字段
        $("#show_pk").text(row.pk);
        $("#show_zh").text(row.zh);
        $("#show_xm").text(row.xm);
        $("#show_sfzh").text(row.sfzh);
        $("#show_nc").text(row.nc);
        $("#show_rksj").text(row.rksj);
        $("#show_lrdw").text(row.lrdw);
        $("#show_jf").text(row.jf);
        $("#show_jfms").text(row.jfms);
        $("#show_gxr").text(row.gxr);
        $("#show_zje").text(row.zje);
        $("#show_fxf").text(row.fxf);
        $("#show_zhdw").text(row.zhdw);
        $("#show_yxx").text(row.yxx);

        $("#showModal").modal();
    },
    "click #editBtn":function (e,vale,row,index) {
    	//设置模态框标题
        $("#editModalLabel").text("**修改");
        //设置操作类型
        $("#edit_curdType").val("update");
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").attr('disabled','disabled');

        //请根据需要修改/删除 需展示字段
        //注意：字典类型数据需要dm和mc都进行设置
        $("#edit_pk").val(row.pk);
        $("#edit_zh").val(row.zh);
        $("#edit_xm").val(row.xm);
        $("#edit_sfzh").val(row.sfzh);
        $("#edit_nc").val(row.nc);
        $("#edit_rksj").val(row.rksj);
        $("#edit_lrdw").val(row.lrdw);
        $("#edit_jf").val(row.jf);
        $("#edit_jfms").val(row.jfms);
        $("#edit_gxr").val(row.gxr);
        $("#edit_zje").val(row.zje);
        $("#edit_fxf").val(row.fxf);
        $("#edit_zhdw").val(row.zhdw);
        $("#edit_yxx").val(row.yxx);

		//模态框的确认(保存)按钮绑定事件
        $("#editConfirmBtn").on("click",function(){
            //curdxsk();
            xskEditSubmit();
        });
        $("#editModal").modal();
    },
    "click #delBtn":function (e,vale,row,index) {
        $("#tipsModalLabel").html("<i class='fa fa-warning fa-lg' style='color: #c87f0a'></i>警告");//提示标题
        $("#tipsMessage").text("确认删除么？");//设置提示信息
        $("#tipsConfirmBtn").text("删除");//按钮名称
        $("#tipsConfirmBtn").on("click",function(){
            deletexsk(row.pk);
        });
        $("#tipsModal").modal();
    }
}

function addxsk() {
    $("#editModalLabel").text("**新增");
    $("#edit_curdType").val("insert");
    
    //请根据需要修改/删除 需展示字段
    //此处设置字段与修改时相同，是为清空点击修改时为元素的赋值
    //注意：字典类型数据需要dm和mc都进行设置
    $("#edit_pk").val('');
    $("#edit_zh").val('');
    $("#edit_xm").val('');
    $("#edit_sfzh").val('');
    $("#edit_nc").val('');
    $("#edit_rksj").val('');
    $("#edit_lrdw").val('');
    $("#edit_jf").val('');
    $("#edit_jfms").val('');
    $("#edit_gxr").val('');
    $("#edit_zje").val('');
    $("#edit_fxf").val('');
    $("#edit_zhdw").val('');
    $("#edit_yxx").val('');
    
    $("#editConfirmBtn").on("click",function(){
        //curdxsk();
        xskEditSubmit();
    });

    $("#editModal").modal();
}

function deletexsk(id){
    var reqData={"curdType":"delete","pk":id};
    $("#tipsModal").modal('toggle');
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/xsk/xsk/curd" ,
        data: reqData,
        success: function (result) {
            alert(result.msg);
            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function curdxsk() {
	//防止重复点击
    $("#editConfirmBtn").attr('disabled','disabled');
    var reqData = $('#xskEditForm').serializeJsonObject();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/xsk/xsk/curd" ,
        data: reqData,
        success : function(data) {
            alert(data.msg);
            $("#editModal").modal('toggle');

            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function initcookie() {
    var cookie = localStorage.getItem("xskzhs");
    if (cookie!=null&&cookie!=''){
        var str = '';
        var r = cookie.split(",");
        for (var i=0;i<r.length;i++){
            str += "<a style='cursor:pointer;' onclick='jl(\""+r[i]+"\")'>"+r[i]+"</a>&nbsp;";
        }
        $("#zjss").html(str);
    }else {
        localStorage.setItem("xskzhs","");
    }
}

function setitem() {
    var a = $("#zh").val();
    var cookie = localStorage.getItem("xskzhs");
    var html = $("#zjss").html();
    if (a!=null&&a!='') {
        if (cookie.length===0){
            cookie += a;
            html = "<a style='cursor:pointer;' onclick='jl(\""+a+"\")'>"+a+"</a>&nbsp;";
        }else{
            var r = cookie.split(",");
            if (r.indexOf(a)>-1){
                return;
            }
            if (r.length==5){
                cookie = cookie.substring(0,cookie.lastIndexOf(","));
                html = "<a style='cursor:pointer;' onclick='jl(\""+a+"\")'>"+a+"</a>&nbsp;"+html.substring(0,html.lastIndexOf("<a"));
            }else {
                html = "<a style='cursor:pointer;' onclick='jl(\""+a+"\")'>"+a+"</a>&nbsp;"+html;
            }
            cookie = a+","+cookie;
        }
        localStorage.setItem("xskzhs",cookie);
        $("#zjss").html(html);
    }
}

function jl(a) {
    $("#zh").val(a);
    search();
}

function search() {
    //initxskGrid();
    setitem();
    if ($("#jf").val()!=null&&$("#jf").val()!=''&&$("#jf").val()!=undefined){
        var jy = /^(-)?[0-9]*$/;
        if (!jy.test($("#jf").val())) {
            alert("积分只能输入数字")
            return;
        }else{
            if ($("#jf").val()>8 || $("#jf").val()<0){
                alert("积分最多为8,最少为0");
                return;
            }
        }
    }
    $("#xskGrid").bootstrapTable("refresh");
}
function exportxsk(){
    var form = document.xskQueryForm;
    var url = ctx+"/xsk/xsk/excel";
    form.action=url;
    form.submit();
}

var xskEditSubmit = function(){
    var flag = $('#xskEditForm').validate('submitValidate');
    if (flag){
        curdxsk();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}

