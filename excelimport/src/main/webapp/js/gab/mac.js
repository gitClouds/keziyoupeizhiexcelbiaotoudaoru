/**
 * Created by Administrator on 2019/6/5.
 */

$(function () {
    initGabMacGrid();
})
function initGabMacGrid(){
    $('#gabMacGrid').bootstrapTable('destroy');

    $("#gabMacGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/gab/mac/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryGabMacParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
            //请根据需要修改/删除 需展示字段
            {
                title : "账/卡号",
                field : "zh",
                align : "center"
            },
            {
                title : "姓名",
                field : "zhxm",
                align : "center"
            },
            {
                title : "所属银行",
                field : "zhjgdm",
                align : "center"
            },
            {
                title : "查询单位",
                field : "lrrdwdm",
                align : "center"
            },
            {
                title : "查询时间",
                field : "rksj",
                align : "center",
                formatter: function (value, row, index) {
                    return value?new Date(value).Format("yyyy-MM-dd hh:mm:ss"):"";
                }
            },
            {
                title : "事由",
                field : "sy",
                align : "center"
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function queryGabMacParams(params){
    var tmp = $("#gabYhkMacQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}

function search() {
    $("#gabMacGrid").bootstrapTable("refresh");
}