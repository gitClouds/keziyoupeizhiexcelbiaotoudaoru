/**
 * Created by Administrator on 2019/6/10.
 */

function ajxxShow(ajid) {
    var reqData={"ajid":ajid};
    $.ajax({
        type: "POST",
        dataType: "json",
        async:false,
        url: ctx+"/gab/ajxx/show" ,
        data: reqData,
        success: function (result) {
            showAjxx(result);
        },
        error : function() {
            alert("请求异常");
        }
    });
}
function showAjxx(data) {
    $("#ajbh").val(data.ajbh);
    $("#ladwmc").val(data.ladwmc);
    $("#lasj").val(data.lasj?new Date(data.lasj).Format("yyyy-MM-dd"):"");
    $("#lxr").val(data.lxr+' '+(data.lxrsj?data.lxrsj:"")+' '+(data.lxdh?data.lxdh:""));
    $("#bar").val(data.bar);
    $("#barsfzh").val(data.barsfzh);
    $("#barlxdh").val(data.barlxdh);
    $("#shrdwmc").val(data.shrdwmc);
    $("#ajlb").val(data.ajlb);
    $("#ajlbzl").val(data.ajlbzl);
    $("#sajz").val(data.sajz);
    $("#fasj").val((data.bfasj?new Date(data.bfasj).Format("yyyy-MM-dd"):"")+" 至 "+(data.afasj?new Date(data.afasj).Format("yyyy-MM-dd"):""));
    $("#fadxc").val((data.fadmc?data.fadmc:"")+"-"+(data.fadxc?data.fadxc:""));
    $("#jyaq").text(data.jyaq);

    $("#fbr").val(data.fbr);
    $("#fbdw").val(data.fbdwmc);
    $("#fbsj").val(data.fbsj?new Date(data.fbsj).Format("yyyy-MM-dd"):"");

    $("#shr").val(data.shr);
    $("#shdwmc").val(data.shdwmc);
    $("#shsj").val(data.shsj?new Date(data.shsj).Format("yyyy-MM-dd"):"");
    $("#shzt").val(data.shzt?data.shzt==='1'?"审核通过": data.shzt==='2'?"审核不通过": data.shzt==='3'?"上报待审核":"" :"");
    $("#shyj").val(data.shyj);
}

function dataShow(ajid) {
    var reqData={"ajid":ajid};
    $.ajax({
        type: "POST",
        dataType: "json",
        async:false,
        url: ctx+"/gab/ajxx/showData" ,
        data: reqData,
        success: function (result) {
            showFj(result);
            showData(result);
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function showFj(data) {
    var sadjbs="";
    if (data.sadjb && data.sadjb.length > 0){
        for (var i=0;i<data.sadjb.length;i++){
            sadjbs+='<i class="btn" onclick="fileUpload.fileDownLoad(this)" data-key="'+ctx+'/attachment/download?pk='+data.sadjb[i].fj+'"><i class="fa fa-download fa-lg"></i></i>&nbsp;&nbsp;&nbsp;&nbsp;';
        }
    }
    $("#sadjb").html(sadjbs);

    var xwbls="";
    if (data.xwbl && data.xwbl.length > 0){
        for (var i=0;i<data.xwbl.length;i++){
            xwbls+='<i class="btn" onclick="fileUpload.fileDownLoad(this)" data-key="'+ctx+'/attachment/download?pk='+data.xwbl[i].fj+'"><i class="fa fa-download fa-lg"></i></i>&nbsp;&nbsp;&nbsp;&nbsp;';
        }
    }
    $("#xwbl").html(xwbls);

    var lajdss="";
    if (data.lajds && data.lajds.length > 0){
        for (var i=0;i<data.lajds.length;i++){
            lajdss+='<i class="btn" onclick="fileUpload.fileDownLoad(this)" data-key="'+ctx+'/attachment/download?pk='+data.lajds[i].fj+'"><i class="fa fa-download fa-lg"></i></i>&nbsp;&nbsp;&nbsp;&nbsp;';
        }
    }
    $("#lajds").html(lajdss);

    var ltnrs="";
    if (data.ltnr && data.ltnr.length > 0){
        for (var i=0;i<data.ltnr.length;i++){
            ltnrs+='<i class="btn" onclick="fileUpload.fileDownLoad(this)" data-key="'+ctx+'/attachment/download?pk='+data.ltnr[i].fj+'"><i class="fa fa-download fa-lg"></i></i>&nbsp;&nbsp;&nbsp;&nbsp;';
        }
    }
    $("#ltnr").html(ltnrs);
}

function showData(data) {
    if (data.sayhkList && data.sayhkList.length > 0){
        for (var i=0;i<data.sayhkList.length;i++){
            if (i==0){
                showSazhVal(data.sayhkList[i]);
            }else{
                addSazh();
                showSazhVal(data.sayhkList[i]);
            }
        }
    }
    if (data.dhList && data.dhList.length > 0){
        for (var i=0;i<data.dhList.length;i++){
            if (i==0){
                showDhVal(data.dhList[i]);
            }else{
                addDh();
                showDhVal(data.dhList[i]);
            }
        }
    }
    if (data.qqList && data.qqList.length > 0){
        for (var i=0;i<data.qqList.length;i++){
            if (i==0){
                showQqVal(data.qqList[i]);
            }else{
                addQq();
                showQqVal(data.qqList[i]);
            }
        }
    }
    if (data.wlList && data.wlList.length > 0){
        for (var i=0;i<data.wlList.length;i++){
            if (i==0){
                showWlVal(data.wlList[i]);
            }else{
                addWl();
                showWlVal(data.wlList[i]);
            }
        }
    }
}
function addSazh(){
    var oldBeanNum = $(".sazhData:last").attr("data-bean-num");
    $(".sazhData:last").after($(".sazhData:last").clone());
    sazhNum+=1;
    $(".sazhData:last").attr("data-bean-num",sazhNum);
    //遍历所有input
    $(".sazhData:last input").each(function () {
        initAddParm(oldBeanNum,sazhNum,this);
    })
}
function addDh() {
    var oldBeanNum = $(".dhData:last").attr("data-bean-num");
    $(".dhData:last").after($(".dhData:last").clone());
    dhNum+=1;
    $(".dhData:last").attr("data-bean-num",dhNum);
    //遍历所有input
    $(".dhData:last input").each(function () {
        initAddParm(oldBeanNum,dhNum,this);
    })
}
function addQq() {
    var oldBeanNum = $(".qqData:last").attr("data-bean-num");
    $(".qqData:last").after($(".qqData:last").clone());
    qqNum+=1;
    $(".qqData:last").attr("data-bean-num",qqNum);
    //遍历所有input
    $(".qqData:last input").each(function () {
        initAddParm(oldBeanNum,qqNum,this);
    })
}
function addWl() {
    var oldBeanNum = $(".wlData:last").attr("data-bean-num");
    $(".wlData:last").after($(".wlData:last").clone());
    wlNum+=1;
    $(".wlData:last").attr("data-bean-num",wlNum);
    //遍历所有input
    $(".wlData:last input").each(function () {
        initAddParm(oldBeanNum,wlNum,this);
    })
}
function showSazhVal(data){
    $("#show_zzfs_"+sazhNum).val(data.zzfs);
    $("#show_zzsj_"+sazhNum).val(data.zzsj?new Date(data.zzsj).Format("yyyy-MM-dd hh:mm:ss"):"");
    $("#show_shrzhlx_"+sazhNum).val(data.shrzhlx);
    $("#show_shrzh_"+sazhNum).val(data.shrzh);
    $("#show_xyrzhlx_"+sazhNum).val(data.xyrzhlx);
    $("#show_xyrzh_"+sazhNum).val(data.xyrzh);
    $("#show_fkdd_"+sazhNum).val(data.fkdd);
    if (data.cXh){
        $("#show_zzhkpz_"+sazhNum).html('<i class="btn" onclick="fileUpload.fileDownLoad(this)" data-key="'+ctx+'/attachment/download?pk='+data.cXh+'"><i class="fa fa-download fa-lg"></i></i>');
    }
}
function showDhVal(data) {
    $("#show_shrdh_"+dhNum).val(data.shrdh);
    $("#show_sadhhm_"+dhNum).val(data.sadhhm);
    $("#show_yys_"+dhNum).val(data.yys);
    $("#show_dhgsdmc_"+dhNum).val(data.dhgsdmc);
    $("#show_thsj_"+dhNum).val((data.kssj?new Date(data.kssj).Format("yyyy-MM-dd hh:mm:ss"):"")+" 至 "+(data.jssj?new Date(data.jssj).Format("yyyy-MM-dd hh:mm:ss"):""));
    $("#show_thnr_"+dhNum).text(data.thnr);
}
function showQqVal(data) {
    $("#show_sjgjmc_"+qqNum).val(data.sjgjmc);
    $("#show_pzqq_"+qqNum).val(data.pzqq);
    $("#show_qqip_"+qqNum).val(data.qqip);
    $("#show_qqdlsj_"+qqNum).val(data.qqdlsj);
    if (data.cXh) {
        $("#show_dlrz_" + qqNum).html('<i class="btn" onclick="fileUpload.fileDownLoad(this)" data-key="' + ctx + '/attachment/download?pk=' + data.cXh + '"><i class="fa fa-download fa-lg"></i></i>');
    }
}
function showWlVal(data) {
    $("#show_sawz_"+wlNum).val(data.sawz);
}


function initGabAjHgGrid(){
    $('#gzrzGrid').bootstrapTable('destroy');

    $("#gzrzGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/gab/ajxx/hgList',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryGabAjHgParams,//参数
        showColumns: false,//是否显示所有的列（选择显示的列）
        showRefresh: false,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 410,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        //得到查询的参数
        columns :[
            {
                title : "序号",
                field : "Number",
                align : "center",
                width : "50px",
                formatter: function (value, row, index) {
                    var pageSize = $('#gzrzGrid').bootstrapTable('getOptions').pageSize;     //通过table的#id 得到每页多少条
                    var pageNumber = $('#gzrzGrid').bootstrapTable('getOptions').pageNumber; //通过table的#id 得到当前第几页
                    return pageSize * (pageNumber - 1) + index + 1;    // 返回每条的序号： 每页条数 *（当前页 - 1 ）+ 序号
                }
            },
            {
                title : "发案单位",
                field : "fadwmc",
                align : "center"
            },
            {
                title : "进度描述",
                field : "jdms",
                align : "center"
            },
            {
                title : "进展类别",
                field : "jzlb",
                align : "center"
            },
            {
                title : "进展时间",
                field : "jzczsj",
                align : "center",
                formatter: function (value, row, index) {
                    return value?new Date(value).Format("yyyy-MM-dd hh:mm:ss"):"";
                }
            },
            {
                title : "进度操作单位",
                field : "jzczdwmc",
                align : "center"
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}
// 以起始页码方式传入参数,params为table组装参数
function queryGabAjHgParams(params){
    var tmp = $("#gabAjHgQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    tmp["ajid"]=$("#ajid").val();
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}