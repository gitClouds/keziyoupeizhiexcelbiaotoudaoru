
$(function () {
    initTbXdryChxxGrid();
    formEditValidate('tbXdryChxxEditForm');//表单验证
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
    $('#editModal').on('hidden.bs.modal', function (event) {
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").removeAttr('disabled');
        $("#editConfirmBtn").removeAttr('disabled','disabled');
        $("#editConfirmBtn").unbind();
        formEditValidateReset('tbXdryChxxEditForm');
    });
})

function initTbXdryChxxGrid(){
    $('#tbXdryChxxGrid').bootstrapTable('destroy');

    $("#tbXdryChxxGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/jdzh/tbXdryChxx/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryTbXdryChxxParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
		showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
        //请根据需要修改/删除 需展示字段
            {
                title : "查获序号--DEXXXXX--",
                field : "chxh",
                align : "center"
            },
            {
                title : "人员编号--DE00038--编号规则为：R+XXXXXXXXXXXX(公安机关机构代码)+XXXXXX(年月)+XXXX(顺序号)              ",
                field : "xyrbh",
                align : "center"
            },
            {
                title : "查获日期--DE01160--",
                field : "chrq",
                align : "center"
            },
            {
                title : "  查获单位_行政区划代码--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码",
                field : "chdw_xzqhdm",
                align : "center"
            },
            {
                title : "  查获单位_区划内详细地址--DE00076--",
                field : "chdw_qhnxxdz",
                align : "center"
            },
            {
                title : "  查获单位_单位名称--DE00065--",
                field : "chdw_dwmc",
                align : "center"
            },
            {
                title : "吸毒场所--DE01135--采用GA/TXXXX《吸毒场所种类代码》",
                field : "xdcszldm",
                align : "center"
            },
            {
                title : "本次滥用毒品种类--DEXXXXX--",
                field : "bclydpzl",
                align : "center"
            },
            {
                title : "毒品来源--DE00166--采用GA 332.3《禁毒信息管理代码 第3部分:毒品来源代码》",
                field : "dplydm",
                align : "center"
            },
            {
                title : "违法事实_简要案情--DE00100--",
                field : "wfss_jyaq",
                align : "center"
            },
            {
                title : "查获类型--DE01133--吸毒人员查获类型代码",
                field : "xdrychlxdm",
                align : "center"
            },
            {
                title : "查获来源--DE01132--采用GA/TXXXX《吸毒人员查获来源代码》",
                field : "xdrychlydm",
                align : "center"
            },
            {
                title : "尿检结果--DE01137--尿检结果代码",
                field : "njjgdm",
                align : "center"
            },
            {
                title : "吸毒方式--DE01136--采用GA/TXXXX《吸毒方式代码》",
                field : "xdfsdm",
                align : "center"
            },
            {
                title : "复吸次数----",
                field : "xfcs",
                align : "center"
            },
            {
                title : "娱乐场所查获序号----",
                field : "ylcschxh",
                align : "center"
            },
            {
                title : "娱乐场所名称----",
                field : "ylcsmc",
                align : "center"
            },
            {
                title : "是否特殊人群----",
                field : "sftsrq",
                align : "center"
            },
            {
                title : "特殊人群种类----",
                field : "tsrqzl",
                align : "center"
            },
            {
                title : "阳性结果----",
                field : "yxjg",
                align : "center"
            },
            {
                title : "是否疾病----",
                field : "sfjb",
                align : "center"
            },
            {
                title : "疾病类型----",
                field : "jblx",
                align : "center"
            },
            {
                title : "是否末次查获----",
                field : "sfmcch",
                align : "center"
            },
            {
                title : "查获次数----",
                field : "chcs",
                align : "center"
            },
            {
                title : "  填表人_姓名--DE00002--",
                field : "tbr_xm",
                align : "center"
            },
            {
                title : "  填表人_联系电话--DE00216--固定电话号码、移动电话号码",
                field : "tbr_lxdh",
                align : "center"
            },
            {
                title : "审核人_姓名--DE00002--",
                field : "shr_xm",
                align : "center"
            },
            {
                title : "填表单位_单位名称--DE00065--",
                field : "tbdw_dwmc",
                align : "center"
            },
            {
                title : "填表日期--DE01158--",
                field : "tbrq",
                align : "center"
            },
            {
                title : "录入人_姓名--DE00002--",
                field : "lrr_xm",
                align : "center"
            },
            {
                title : "录入单位_单位名称--DE00065--",
                field : "lrdw_dwmc",
                align : "center"
            },
            {
                title : "录入时间--DE00739--",
                field : "lrsj",
                align : "center"
            },
            {
                title : "备注--DE00503--",
                field : "bz",
                align : "center"
            },
            {
                title : "人员标识--DEXXXXX--",
                field : "rybs",
                align : "center"
            },
            {
                title : "修改部署单位--DEXXXXX--",
                field : "xgbsdw",
                align : "center"
            },
            {
                title : "归属单位--DEXXXXX--",
                field : "gsdw",
                align : "center"
            },
            {
                title : "更新时间戳--DEXXXXX--",
                field : "gxsjc",
                align : "center"
            },
            {
                title : "操作标识--DEXXXXX--",
                field : "czbs",
                align : "center"
            },
            {
                title : "内外网标志--DEXXXXX--",
                field : "inoutbz",
                align : "center"
            },
            {
                title : "有效性--DEXXXXX--",
                field : "yxx",
                align : "center"
            },
            {
                title : "更新时间--DEXXXXX--",
                field : "gxsj",
                align : "center"
            },
            {
                title : "  查获单位_单位代码--DE00065--",
                field : "chdw_dwdm",
                align : "center"
            },
            {
                title : "录入单位_单位代码--DE00065--",
                field : "lrdw_dwdm",
                align : "center"
            },
            {
                title : "填表单位_单位代码--DE00065--",
                field : "tbdw_dwdm",
                align : "center"
            },
            {
                title : "嫌疑人类别代码---XYRLB",
                field : "xyrlb",
                align : "center"
            },
            {
                title : "字段注释",
                field : "fjmc",
                align : "center"
            },
            {
                title : "字段注释",
                field : "wlgjfjxh",
                align : "center"
            },
            {
                title : "字段注释",
                field : "chjclx",
                align : "center"
            },
            {
                title : "字段注释",
                field : "chjc_dwdm",
                align : "center"
            },
            {
                title : "字段注释",
                field : "chjc_dwmc",
                align : "center"
            },
            {
                title : "字段注释",
                field : "chsnl",
                align : "center"
            },
            {
                title : "吞食异物描述",
                field : "tsywms",
                align : "center"
            },
            {
                title : "是否吞食异物",
                field : "sftsyw",
                align : "center"
            },
            {
                title : "对应的所有处置情况",
                field : "czqk",
                align : "center"
            },
            {
                title : "对应的所有处置情况名称",
                field : "czqkmc",
                align : "center"
            },
            {
                title : "本次查获之前的管控现状代码",
                field : "bczhzqgkxzdm",
                align : "center"
            },
            {
                title : "来源(1：接口下发2：本地录入)",
                field : "ly",
                align : "center"
            },
            {
                title : "操作",
                field : "chxh",
                align : "center",
                valign : "middle",
                width : "150px",
                events:tbXdryChxxOperateEvents,
                formatter : tbXdryChxxOperationFormatter
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function queryTbXdryChxxParams(params){
    var tmp = $("#tbXdryChxxQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function tbXdryChxxOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' id='showBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";/*onclick=\"showTbXdryChxx('" + id + "', view='view')\"*/
    result += "<a href='javascript:void(0);' id='editBtn' class='btn' title='编辑'><span class='fa fa-edit fa-lg'></span></a>";/*onclick=\"editTbXdryChxx('" + id + "')\"*/
    result += "<a href='javascript:void(0);' id='delBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";/*onclick=\"deleteTbXdryChxx('" + id + "')\"*/

    return result;
}
//操作栏绑定事件
window.tbXdryChxxOperateEvents = {
    "click #showBtn":function (e,vale,row,index) {
    	$("#showModalLabel").text("**详情");
    	//请根据需要修改/删除 需展示字段
        $("#show_chxh").text(row.chxh);
        $("#show_xyrbh").text(row.xyrbh);
        $("#show_chrq").text(row.chrq);
        $("#show_chdw_xzqhdm").text(row.chdw_xzqhdm);
        $("#show_chdw_qhnxxdz").text(row.chdw_qhnxxdz);
        $("#show_chdw_dwmc").text(row.chdw_dwmc);
        $("#show_xdcszldm").text(row.xdcszldm);
        $("#show_bclydpzl").text(row.bclydpzl);
        $("#show_dplydm").text(row.dplydm);
        $("#show_wfss_jyaq").text(row.wfss_jyaq);
        $("#show_xdrychlxdm").text(row.xdrychlxdm);
        $("#show_xdrychlydm").text(row.xdrychlydm);
        $("#show_njjgdm").text(row.njjgdm);
        $("#show_xdfsdm").text(row.xdfsdm);
        $("#show_xfcs").text(row.xfcs);
        $("#show_ylcschxh").text(row.ylcschxh);
        $("#show_ylcsmc").text(row.ylcsmc);
        $("#show_sftsrq").text(row.sftsrq);
        $("#show_tsrqzl").text(row.tsrqzl);
        $("#show_yxjg").text(row.yxjg);
        $("#show_sfjb").text(row.sfjb);
        $("#show_jblx").text(row.jblx);
        $("#show_sfmcch").text(row.sfmcch);
        $("#show_chcs").text(row.chcs);
        $("#show_tbr_xm").text(row.tbr_xm);
        $("#show_tbr_lxdh").text(row.tbr_lxdh);
        $("#show_shr_xm").text(row.shr_xm);
        $("#show_tbdw_dwmc").text(row.tbdw_dwmc);
        $("#show_tbrq").text(row.tbrq);
        $("#show_lrr_xm").text(row.lrr_xm);
        $("#show_lrdw_dwmc").text(row.lrdw_dwmc);
        $("#show_lrsj").text(row.lrsj);
        $("#show_bz").text(row.bz);
        $("#show_rybs").text(row.rybs);
        $("#show_xgbsdw").text(row.xgbsdw);
        $("#show_gsdw").text(row.gsdw);
        $("#show_gxsjc").text(row.gxsjc);
        $("#show_czbs").text(row.czbs);
        $("#show_inoutbz").text(row.inoutbz);
        $("#show_yxx").text(row.yxx);
        $("#show_gxsj").text(row.gxsj);
        $("#show_chdw_dwdm").text(row.chdw_dwdm);
        $("#show_lrdw_dwdm").text(row.lrdw_dwdm);
        $("#show_tbdw_dwdm").text(row.tbdw_dwdm);
        $("#show_xyrlb").text(row.xyrlb);
        $("#show_fjmc").text(row.fjmc);
        $("#show_wlgjfjxh").text(row.wlgjfjxh);
        $("#show_chjclx").text(row.chjclx);
        $("#show_chjc_dwdm").text(row.chjc_dwdm);
        $("#show_chjc_dwmc").text(row.chjc_dwmc);
        $("#show_chsnl").text(row.chsnl);
        $("#show_tsywms").text(row.tsywms);
        $("#show_sftsyw").text(row.sftsyw);
        $("#show_czqk").text(row.czqk);
        $("#show_czqkmc").text(row.czqkmc);
        $("#show_bczhzqgkxzdm").text(row.bczhzqgkxzdm);
        $("#show_ly").text(row.ly);

        $("#showModal").modal();
    },
    "click #editBtn":function (e,vale,row,index) {
    	//设置模态框标题
        $("#editModalLabel").text("**修改");
        //设置操作类型
        $("#edit_curdType").val("update");
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").attr('disabled','disabled');
        
        //请根据需要修改/删除 需展示字段
        //注意：字典类型数据需要dm和mc都进行设置
        $("#edit_chxh").val(row.chxh);
        $("#edit_xyrbh").val(row.xyrbh);
        $("#edit_chrq").val(row.chrq);
        $("#edit_chdw_xzqhdm").val(row.chdw_xzqhdm);
        $("#edit_chdw_qhnxxdz").val(row.chdw_qhnxxdz);
        $("#edit_chdw_dwmc").val(row.chdw_dwmc);
        $("#edit_xdcszldm").val(row.xdcszldm);
        $("#edit_bclydpzl").val(row.bclydpzl);
        $("#edit_dplydm").val(row.dplydm);
        $("#edit_wfss_jyaq").val(row.wfss_jyaq);
        $("#edit_xdrychlxdm").val(row.xdrychlxdm);
        $("#edit_xdrychlydm").val(row.xdrychlydm);
        $("#edit_njjgdm").val(row.njjgdm);
        $("#edit_xdfsdm").val(row.xdfsdm);
        $("#edit_xfcs").val(row.xfcs);
        $("#edit_ylcschxh").val(row.ylcschxh);
        $("#edit_ylcsmc").val(row.ylcsmc);
        $("#edit_sftsrq").val(row.sftsrq);
        $("#edit_tsrqzl").val(row.tsrqzl);
        $("#edit_yxjg").val(row.yxjg);
        $("#edit_sfjb").val(row.sfjb);
        $("#edit_jblx").val(row.jblx);
        $("#edit_sfmcch").val(row.sfmcch);
        $("#edit_chcs").val(row.chcs);
        $("#edit_tbr_xm").val(row.tbr_xm);
        $("#edit_tbr_lxdh").val(row.tbr_lxdh);
        $("#edit_shr_xm").val(row.shr_xm);
        $("#edit_tbdw_dwmc").val(row.tbdw_dwmc);
        $("#edit_tbrq").val(row.tbrq);
        $("#edit_lrr_xm").val(row.lrr_xm);
        $("#edit_lrdw_dwmc").val(row.lrdw_dwmc);
        $("#edit_lrsj").val(row.lrsj);
        $("#edit_bz").val(row.bz);
        $("#edit_rybs").val(row.rybs);
        $("#edit_xgbsdw").val(row.xgbsdw);
        $("#edit_gsdw").val(row.gsdw);
        $("#edit_gxsjc").val(row.gxsjc);
        $("#edit_czbs").val(row.czbs);
        $("#edit_inoutbz").val(row.inoutbz);
        $("#edit_yxx").val(row.yxx);
        $("#edit_gxsj").val(row.gxsj);
        $("#edit_chdw_dwdm").val(row.chdw_dwdm);
        $("#edit_lrdw_dwdm").val(row.lrdw_dwdm);
        $("#edit_tbdw_dwdm").val(row.tbdw_dwdm);
        $("#edit_xyrlb").val(row.xyrlb);
        $("#edit_fjmc").val(row.fjmc);
        $("#edit_wlgjfjxh").val(row.wlgjfjxh);
        $("#edit_chjclx").val(row.chjclx);
        $("#edit_chjc_dwdm").val(row.chjc_dwdm);
        $("#edit_chjc_dwmc").val(row.chjc_dwmc);
        $("#edit_chsnl").val(row.chsnl);
        $("#edit_tsywms").val(row.tsywms);
        $("#edit_sftsyw").val(row.sftsyw);
        $("#edit_czqk").val(row.czqk);
        $("#edit_czqkmc").val(row.czqkmc);
        $("#edit_bczhzqgkxzdm").val(row.bczhzqgkxzdm);
        $("#edit_ly").val(row.ly);

		//模态框的确认(保存)按钮绑定事件
        $("#editConfirmBtn").on("click",function(){
            //curdTbXdryChxx();
            tbXdryChxxEditSubmit();
        });
        $("#editModal").modal();
    },
    "click #delBtn":function (e,vale,row,index) {
        $("#tipsModalLabel").html("<i class='fa fa-warning fa-lg' style='color: #c87f0a'></i>警告");//提示标题
        $("#tipsMessage").text("确认删除么？");//设置提示信息
        $("#tipsConfirmBtn").text("删除");//按钮名称
        $("#tipsConfirmBtn").on("click",function(){
            deleteTbXdryChxx(row.chxh);
        });
        $("#tipsModal").modal();
    }
}

function addTbXdryChxx() {
    $("#editModalLabel").text("**新增");
    $("#edit_curdType").val("insert");
    
    //请根据需要修改/删除 需展示字段
    //此处设置字段与修改时相同，是为清空点击修改时为元素的赋值
    //注意：字典类型数据需要dm和mc都进行设置
    $("#edit_chxh").val('');
    $("#edit_xyrbh").val('');
    $("#edit_chrq").val('');
    $("#edit_chdw_xzqhdm").val('');
    $("#edit_chdw_qhnxxdz").val('');
    $("#edit_chdw_dwmc").val('');
    $("#edit_xdcszldm").val('');
    $("#edit_bclydpzl").val('');
    $("#edit_dplydm").val('');
    $("#edit_wfss_jyaq").val('');
    $("#edit_xdrychlxdm").val('');
    $("#edit_xdrychlydm").val('');
    $("#edit_njjgdm").val('');
    $("#edit_xdfsdm").val('');
    $("#edit_xfcs").val('');
    $("#edit_ylcschxh").val('');
    $("#edit_ylcsmc").val('');
    $("#edit_sftsrq").val('');
    $("#edit_tsrqzl").val('');
    $("#edit_yxjg").val('');
    $("#edit_sfjb").val('');
    $("#edit_jblx").val('');
    $("#edit_sfmcch").val('');
    $("#edit_chcs").val('');
    $("#edit_tbr_xm").val('');
    $("#edit_tbr_lxdh").val('');
    $("#edit_shr_xm").val('');
    $("#edit_tbdw_dwmc").val('');
    $("#edit_tbrq").val('');
    $("#edit_lrr_xm").val('');
    $("#edit_lrdw_dwmc").val('');
    $("#edit_lrsj").val('');
    $("#edit_bz").val('');
    $("#edit_rybs").val('');
    $("#edit_xgbsdw").val('');
    $("#edit_gsdw").val('');
    $("#edit_gxsjc").val('');
    $("#edit_czbs").val('');
    $("#edit_inoutbz").val('');
    $("#edit_yxx").val('');
    $("#edit_gxsj").val('');
    $("#edit_chdw_dwdm").val('');
    $("#edit_lrdw_dwdm").val('');
    $("#edit_tbdw_dwdm").val('');
    $("#edit_xyrlb").val('');
    $("#edit_fjmc").val('');
    $("#edit_wlgjfjxh").val('');
    $("#edit_chjclx").val('');
    $("#edit_chjc_dwdm").val('');
    $("#edit_chjc_dwmc").val('');
    $("#edit_chsnl").val('');
    $("#edit_tsywms").val('');
    $("#edit_sftsyw").val('');
    $("#edit_czqk").val('');
    $("#edit_czqkmc").val('');
    $("#edit_bczhzqgkxzdm").val('');
    $("#edit_ly").val('');
    
    $("#editConfirmBtn").on("click",function(){
        //curdTbXdryChxx();
        tbXdryChxxEditSubmit();
    });

    $("#editModal").modal();
}

function deleteTbXdryChxx(id){
    var reqData={"curdType":"delete","chxh":id};
    $("#tipsModal").modal('toggle');
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/jdzh/tbXdryChxx/curd" ,
        data: reqData,
        success: function (result) {
            alert(result.msg);
            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function curdTbXdryChxx() {
	//防止重复点击
    $("#editConfirmBtn").attr('disabled','disabled');
    var reqData = $('#tbXdryChxxEditForm').serializeJsonObject();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/jdzh/tbXdryChxx/curd" ,
        data: reqData,
        success : function(data) {
            alert(data.msg);
            $("#editModal").modal('toggle');

            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function search() {
    //inittbXdryChxxGrid();
    $("#tbXdryChxxGrid").bootstrapTable("refresh");
}
function exportTbXdryChxx(){
    var form = document.tbXdryChxxQueryForm;
    var url = ctx+"/jdzh/tbXdryChxx/excel";
    form.action=url;
    form.submit();
}

var tbXdryChxxEditSubmit = function(){
    var flag = $('#tbXdryChxxEditForm').validate('submitValidate');
    if (flag){
        curdTbXdryChxx();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}

