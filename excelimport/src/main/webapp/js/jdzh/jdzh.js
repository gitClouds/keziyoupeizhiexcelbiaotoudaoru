var tbXdryJbxxEditSubmit = function(){
    var flag = $('#tbXdryJbxxEditForm').validate('submitValidate');
    if (flag){
        curdTbXdryJbxx();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}
var tbDpajSdryJbxxEditSubmit = function(){
    var flag = $('#tbDpajSdryJbxxEditForm').validate('submitValidate');
    if (flag){
        curdTbDpajSdryJbxx();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}
function curdTbDpajSdryJbxx() {
    var curdType = $("#type").val();
    //防止重复点击
    $("#editConfirmBtn").attr('disabled','disabled');
    var reqData = $('#tbDpajSdryJbxxEditForm').serializeJsonObject();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/jdzh/tbDpajSdryJbxx/curd" ,
        data: reqData,
        success : function(data) {
            alert(data.msg);
            window.parent.$("#ifTab_container_jdzhxx")[0].contentWindow.search();
            if (curdType=='insert'){
                window.parent.$("#ifTab_close_rycl").click();
            }else if(curdType=='update'){
                $("#editConfirmBtn").removeAttr('disabled');
            }
        },
        error : function() {
            alert("请求异常");
        }
    });
}
function curdTbXdryJbxx() {
    var curdType = $("#type").val();
    //防止重复点击
    $("#editConfirmBtn").attr('disabled','disabled');
    var reqData = $('#tbXdryJbxxEditForm').serializeJsonObject();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/jdzh/tbXdryJbxx/curd" ,
        data: reqData,
        success : function(data) {
            alert(data.msg);
            window.parent.$("#ifTab_container_jdzhxx")[0].contentWindow.search();
            if (curdType=='insert'){
                window.parent.$("#ifTab_close_rycl").click();
            }else if(curdType=='update'){
                $("#editConfirmBtn").removeAttr('disabled');
            }
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function check(a,lx){
    var xyrbh = $(a).val();
    if (xyrbh!=null&&xyrbh!=''&&xyrbh!=undefined){
        $.ajax({
            type: "POST",
            url: ctx+"/jdzh/check",
            data: "xyrbh="+xyrbh+"&lx="+lx,
            contentType: "application/x-www-form-urlencoded; charset=utf-8",
            success: function(msg){
                if (msg!=''&&msg!=null){
                    $("#edit_xyrbh").val("");
                    alert(msg);
                }
            }
        });
    }
}