
$(function () {
    initTbDpajJbxxGrid();
    formEditValidate('tbDpajJbxxEditForm');//表单验证
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
    $('#editModal').on('hidden.bs.modal', function (event) {
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").removeAttr('disabled');
        $("#editConfirmBtn").removeAttr('disabled','disabled');
        $("#editConfirmBtn").unbind();
        formEditValidateReset('tbDpajJbxxEditForm');
    });
})

function initTbDpajJbxxGrid(){
    $('#tbDpajJbxxGrid').bootstrapTable('destroy');

    $("#tbDpajJbxxGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/Jdzh/tbDpajJbxx/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryTbDpajJbxxParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
		showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
        //请根据需要修改/删除 需展示字段
            {
                title : "记录编号--DExxxxx--编号规则为“XXXXXXXXXXXX（公安机关机构代码）+YYYYMM（年月）+XXXXXXXXXXXXXX（顺序号）”                         ",
                field : "jlbh",
                align : "center"
            },
            {
                title : "立案编号--DE00092--编号规则为“A+XXXXXXXXXXXX（公安机关机构代码）+YYYYMM（年月）+XXXX（顺序号）”                         ",
                field : "ajbh",
                align : "center"
            },
            {
                title : "案件名称--DE00094--",
                field : "ajmc",
                align : "center"
            },
            {
                title : "案件类别--DE00093--采用GA 240.1《刑事犯罪信息管理代码 第1部分: 案件类别代码》",
                field : "ajlbdm",
                align : "center"
            },
            {
                title : "发现时间--DE01085、DE01121--",
                field : "fxsj",
                align : "center"
            },
            {
                title : "发案地点--DE00075--",
                field : "dzmc",
                align : "center"
            },
            {
                title : "发案地点详址--DE00076--",
                field : "fazdxz",
                align : "center"
            },
            {
                title : "破案地区--DE00076--",
                field : "padq",
                align : "center"
            },
            {
                title : "立案单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码",
                field : "ladw_dwdm",
                align : "center"
            },
            {
                title : "立案/承办单位名称--DE00065--",
                field : "ladw_dwmc",
                align : "center"
            },
            {
                title : "承办人--DE00002--",
                field : "cbrxm",
                align : "center"
            },
            {
                title : "承办人联系信息--DE00216--固定电话号码、移动电话号码",
                field : "cbr_lxdh",
                align : "center"
            },
            {
                title : "立案日期--DE00220--",
                field : "larq",
                align : "center"
            },
            {
                title : "破案单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码",
                field : "padw_dwdm",
                align : "center"
            },
            {
                title : "破案单位名称--DE00065--",
                field : "padw_dwmc",
                align : "center"
            },
            {
                title : "破案部门--DE00063--采用GA 300.15《看守所在押人员信息管理代码 第15部分: 办案单位类型代码》",
                field : "badwlxdm",
                align : "center"
            },
            {
                title : "破案日期--DE00223--",
                field : "parq",
                align : "center"
            },
            {
                title : "藏毒方式代码--DE00171--采用GA 332.10《禁毒信息管理代码 第10部分:藏毒方式代码》",
                field : "cdfsdm",
                align : "center"
            },
            {
                title : "贩运方式代码--DExxxxx--",
                field : "fyfsdm",
                align : "center"
            },
            {
                title : "是否团伙/集团作案--DExxxxx--",
                field : "sfjt",
                align : "center"
            },
            {
                title : "作案团伙/集团类型--DE00998--采用 GA/T XXX《犯罪团伙性质代码》",
                field : "fzthxzdm",
                align : "center"
            },
            {
                title : "作案团伙/集团名称--DE00997--",
                field : "fzthmc",
                align : "center"
            },
            {
                title : "案情--DE00100--",
                field : "jyaq",
                align : "center"
            },
            {
                title : "见证人提供情况--DE00521--",
                field : "jzrtgqk",
                align : "center"
            },
            {
                title : "作案人情况分析--DE00521--",
                field : "zarqkfx",
                align : "center"
            },
            {
                title : "侦察措施--DE01009--采用 GA/T XXX《案事件侦查行为分类与代码》",
                field : "asjzcxwlbdm",
                align : "center"
            },
            {
                title : "破案方式代码--DE00097--采用GA 240.18《刑事犯罪信息管理代码 第18部分: 破案方式代码》",
                field : "pafsdm",
                align : "center"
            },
            {
                title : "撤案原因--DE00099--采用GA 398.7《经济犯罪案件管理信息系统技术规范 第7部分：撤销案件原因代码》",
                field : "cxajyydm",
                align : "center"
            },
            {
                title : "撤案日期--DE01102--",
                field : "cxajrq",
                align : "center"
            },
            {
                title : "查结情况--DE00521--",
                field : "cjqk",
                align : "center"
            },
            {
                title : "查结日期--DE00101--",
                field : "cjrq",
                align : "center"
            },
            {
                title : "公开方式--DExxxxx--",
                field : "gkfs",
                align : "center"
            },
            {
                title : "完全公开日期--DE00101--",
                field : "wqgkrq",
                align : "center"
            },
            {
                title : "可查状态--DE00596--采用GA/T 2000.40《公安信息代码 第40部分：使用状态代码》",
                field : "syztdm",
                align : "center"
            },
            {
                title : "查询单位名称--DE00065--",
                field : "cxdw_dwmc",
                align : "center"
            },
            {
                title : "查询单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码",
                field : "cxdw_dwdm",
                align : "center"
            },
            {
                title : "案件阶段--DE00096--采用GA 398.5《经济犯罪案件管理信息系统技术规范 第5部分：侦查工作阶段代码》",
                field : "zcjddm",
                align : "center"
            },
            {
                title : "填报单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码",
                field : "tbdw_dwdm",
                align : "center"
            },
            {
                title : "填报单位名称--DE00065--",
                field : "tbdw_dwmc",
                align : "center"
            },
            {
                title : "登记人--DE00002--",
                field : "djr_xm",
                align : "center"
            },
            {
                title : "登记日期--DE00524--",
                field : "djrq",
                align : "center"
            },
            {
                title : "备注--DE00503--",
                field : "bz",
                align : "center"
            },
            {
                title : "审核单位名称--DE00065--",
                field : "shdw_dwmc",
                align : "center"
            },
            {
                title : "审核单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码",
                field : "shdw_dwdm",
                align : "center"
            },
            {
                title : "缴毒总量（克）--DExxxxx--",
                field : "jhdp",
                align : "center"
            },
            {
                title : "情报提供单位--DE00065--",
                field : "qbtgdw_dwmc",
                align : "center"
            },
            {
                title : "情报提供单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码",
                field : "qbtgdw_dwdm",
                align : "center"
            },
            {
                title : "贩运渠道说明--DE00521--",
                field : "fyqdsm",
                align : "center"
            },
            {
                title : "存在不合格--DE00742--判断标识",
                field : "pdbz",
                align : "center"
            },
            {
                title : "案件代号--DExxxxx--",
                field : "ajdh",
                align : "center"
            },
            {
                title : "案件性质--DExxxxx--",
                field : "ajxz",
                align : "center"
            },
            {
                title : "主侦单位名称--DE00060--",
                field : "zzdw_dwmc",
                align : "center"
            },
            {
                title : "主侦单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码",
                field : "zzdw_dwdm",
                align : "center"
            },
            {
                title : "指导单位名称--DE00060--",
                field : "zddw_dwmc",
                align : "center"
            },
            {
                title : "指导单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码",
                field : "zddw_dwdm",
                align : "center"
            },
            {
                title : "涉案国家--DE00069--采用GB/T 2659《世界各国和地区名称代码》中全部三位字母代码",
                field : "gjhdqdm",
                align : "center"
            },
            {
                title : "涉案地区--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码",
                field : "sadq_xzqh",
                align : "center"
            },
            {
                title : "负责人--DE00002--",
                field : "fzrxm",
                align : "center"
            },
            {
                title : "负责人电话--DE00216--固定电话号码、移动电话号码",
                field : "fzr_lxdh",
                align : "center"
            },
            {
                title : "线索来源----",
                field : "xsly",
                align : "center"
            },
            {
                title : "线索来源单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码",
                field : "xsly_dwdm",
                align : "center"
            },
            {
                title : "线索来源单位名称--DE00060--",
                field : "xsly_dwmc",
                align : "center"
            },
            {
                title : "其它线索来源--DExxxxx--",
                field : "qyxsly",
                align : "center"
            },
            {
                title : "目前侦查工作进展情况--DE01012--",
                field : "mqzcgzjzqk",
                align : "center"
            },
            {
                title : "侦查工作中遇到的主要问题--DE01012--",
                field : "zcgzzwt",
                align : "center"
            },
            {
                title : "下一步工作建议--DE01012--",
                field : "xybgzjy",
                align : "center"
            },
            {
                title : "请示信息内容(DZWJNR)--DE01076--",
                field : "inforcontent",
                align : "center"
            },
            {
                title : "督办级别--DE00091--采用GA 398.3《经济犯罪案件管理信息系统技术规范 第3部分：督办级别代码》0 部级  1省级  2市级。默认为空",
                field : "asjdbjbdm",
                align : "center"
            },
            {
                title : "破案状态--DExxxxx--",
                field : "pazt",
                align : "center"
            },
            {
                title : "业务类别--DExxxxx--",
                field : "ywlb",
                align : "center"
            },
            {
                title : "确立时间--DE00101--",
                field : "qlsj",
                align : "center"
            },
            {
                title : "再次申报请示--DE01012--",
                field : "zcsbqs",
                align : "center"
            },
            {
                title : "申请联合请示--DE01012--",
                field : "sqlhqs",
                align : "center"
            },
            {
                title : "申请联合审批标识--DExxxxx--",
                field : "sqlhspbs",
                align : "center"
            },
            {
                title : "联合办案--DExxxxx--",
                field : "lhba",
                align : "center"
            },
            {
                title : "联合协查机构--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码",
                field : "lhzc_gajgjgdm",
                align : "center"
            },
            {
                title : "案件破案申请表--DE01012--",
                field : "ajpasqb",
                align : "center"
            },
            {
                title : "破案确立时间--DE00554--",
                field : "paqlsj",
                align : "center"
            },
            {
                title : "撤销请示--DExxxxx--",
                field : "cxqs",
                align : "center"
            },
            {
                title : "立案/承办地区",
                field : "lacbdq",
                align : "center"
            },
            {
                title : "当前审核结果",
                field : "dqshjg",
                align : "center"
            },
            {
                title : "审核标志",
                field : "shbz",
                align : "center"
            },
            {
                title : "更新时间",
                field : "gxsj",
                align : "center"
            },
            {
                title : "有效性",
                field : "yxx",
                align : "center"
            },
            {
                title : "更新时间戳--DEXXXXX--",
                field : "gxsjc",
                align : "center"
            },
            {
                title : "修改部署单位--DEXXXXX--",
                field : "xgbsdw",
                align : "center"
            },
            {
                title : "是否互联网案件--46",
                field : "sfhlwaj",
                align : "center"
            },
            {
                title : "互联网案件类型--860",
                field : "hlwajlx",
                align : "center"
            },
            {
                title : "其他说明",
                field : "qtsm",
                align : "center"
            },
            {
                title : "是否涉嫌洗钱--46",
                field : "sfsxxq",
                align : "center"
            },
            {
                title : "涉毒洗钱方式--861",
                field : "sdxqfs",
                align : "center"
            },
            {
                title : "其他说明(洗钱)",
                field : "xqqtsm",
                align : "center"
            },
            {
                title : "是否黑社会性质--DExxxxx--",
                field : "sfhsh",
                align : "center"
            },
            {
                title : "是否武装贩毒--DExxxxx--",
                field : "sfwzfd",
                align : "center"
            },
            {
                title : "目标案件操作级别： 1县建议。2县审核。3市建议。4市审核。5省建议。6省审核。7部建议。8部审核。默认为空",
                field : "mbajzt",
                align : "center"
            },
            {
                title : "目标案件操作类别：  0申请目标案件。 1申请降级。2申请破案。3申请撤销。4已确立 。默认为空",
                field : "ajdj",
                align : "center"
            },
            {
                title : "案件类型 --DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码",
                field : "ajlx",
                align : "center"
            },
            {
                title : "部级目标案件编号--DExxxxx--编号规则为“XXXX（行政代码）+YYYY（年）+XXXX（顺序号）”",
                field : "b_m_jlbh",
                align : "center"
            },
            {
                title : "省级目标案件编号--DExxxxx--编号规则为“XXXX（行政代码）+YYYY（年）+XXXX（顺序号）”",
                field : "s_m_jlbh",
                align : "center"
            },
            {
                title : "市级目标案件编号--DExxxxx--编号规则为“XXXX（行政代码）+YYYY（年）+XXXX（顺序号）”",
                field : "c_m_jlbh",
                align : "center"
            },
            {
                title : "目标案件录入单位级别  0 部级  1省级  2市级  3县级   4派出所",
                field : "mbajsbdj",
                align : "center"
            },
            {
                title : "宣传形式",
                field : "xcxs",
                align : "center"
            },
            {
                title : "是否宣传--46",
                field : "sfxc",
                align : "center"
            },
            {
                title : "是否群众举报--46",
                field : "sfqzjb",
                align : "center"
            },
            {
                title : "降级申报请示",
                field : "jjsqxx",
                align : "center"
            },
            {
                title : "上报单位名称--DE00060--",
                field : "sbdw_dwmc",
                align : "center"
            },
            {
                title : "上报单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码",
                field : "sbdw_dwdm",
                align : "center"
            },
            {
                title : "联系人",
                field : "lxr",
                align : "center"
            },
            {
                title : "联系人电话",
                field : "lxr_dh",
                align : "center"
            },
            {
                title : "部级申报目标案件请示信息内容(DZWJNR)--DE01076--",
                field : "b_inforcontent",
                align : "center"
            },
            {
                title : "省级申报目标案件请示信息内容(DZWJNR)--DE01076--",
                field : "s_inforcontent",
                align : "center"
            },
            {
                title : "市级申报目标案件请示信息内容(DZWJNR)--DE01076--",
                field : "c_inforcontent",
                align : "center"
            },
            {
                title : "县级申报目标案件请示信息内容(DZWJNR)--DE01076--",
                field : "x_inforcontent",
                align : "center"
            },
            {
                title : "操作标识",
                field : "czbs",
                align : "center"
            },
            {
                title : "转移清洗毒资方式",
                field : "zyqxdzfs",
                align : "center"
            },
            {
                title : "查缉站点编号",
                field : "cjzdbh",
                align : "center"
            },
            {
                title : "特情人员编号",
                field : "tqrybh",
                align : "center"
            },
            {
                title : "情报产品编号",
                field : "qbcpbh",
                align : "center"
            },
            {
                title : "指示降级信息",
                field : "zsjjsqxx",
                align : "center"
            },
            {
                title : "部级申报目标案件请示信息时间",
                field : "b_inforcontent_sqsj",
                align : "center"
            },
            {
                title : "省级申报目标案件请示信息时间",
                field : "s_inforcontent_sqsj",
                align : "center"
            },
            {
                title : "市级申报目标案件请示信息时间",
                field : "c_inforcontent_sqsj",
                align : "center"
            },
            {
                title : "县级申报目标案件请示信息时间",
                field : "x_inforcontent_sqsj",
                align : "center"
            },
            {
                title : "报送项目类型",
                field : "bsxm_lx",
                align : "center"
            },
            {
                title : "部级目标案件确立时间",
                field : "bm_qlsj",
                align : "center"
            },
            {
                title : "省级目标案件确立时间",
                field : "sm_qlsj",
                align : "center"
            },
            {
                title : "市级目标案件确立时间",
                field : "cm_qlsj",
                align : "center"
            },
            {
                title : "传输状态。0:未传输。1:已传输（新老系统数据传输）",
                field : "cszt",
                align : "center"
            },
            {
                title : "字段注释",
                field : "cbrxm2",
                align : "center"
            },
            {
                title : "字段注释",
                field : "cbr_lxdh2",
                align : "center"
            },
            {
                title : "旧系统中破案方式代码--DE00097--采用GA 240.18《刑事犯罪信息管理代码 第18部分: 破案方式代码》",
                field : "pafsdm_old",
                align : "center"
            },
            {
                title : "字段注释",
                field : "n_yxh",
                align : "center"
            },
            {
                title : "字段注释",
                field : "sjly",
                align : "center"
            },
            {
                title : "前期处置措施",
                field : "qqczcs",
                align : "center"
            },
            {
                title : "种子来源",
                field : "zzly",
                align : "center"
            },
            {
                title : "目标案件类别",
                field : "mbajlb",
                align : "center"
            },
            {
                title : "目标案件序号",
                field : "mbajxh",
                align : "center"
            },
            {
                title : "案件来源",
                field : "ajly",
                align : "center"
            },
            {
                title : "禁种铲毒案件类型",
                field : "jzcd_ajlx",
                align : "center"
            },
            {
                title : "来源(1：接口下发2：本地录入)",
                field : "ly",
                align : "center"
            },
            {
                title : "操作",
                field : "jlbh",
                align : "center",
                valign : "middle",
                width : "150px",
                events:tbDpajJbxxOperateEvents,
                formatter : tbDpajJbxxOperationFormatter
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function queryTbDpajJbxxParams(params){
    var tmp = $("#tbDpajJbxxQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function tbDpajJbxxOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' id='showBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";/*onclick=\"showTbDpajJbxx('" + id + "', view='view')\"*/
    result += "<a href='javascript:void(0);' id='editBtn' class='btn' title='编辑'><span class='fa fa-edit fa-lg'></span></a>";/*onclick=\"editTbDpajJbxx('" + id + "')\"*/
    result += "<a href='javascript:void(0);' id='delBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";/*onclick=\"deleteTbDpajJbxx('" + id + "')\"*/

    return result;
}
//操作栏绑定事件
window.tbDpajJbxxOperateEvents = {
    "click #showBtn":function (e,vale,row,index) {
    	$("#showModalLabel").text("**详情");
    	//请根据需要修改/删除 需展示字段
        $("#show_jlbh").text(row.jlbh);
        $("#show_ajbh").text(row.ajbh);
        $("#show_ajmc").text(row.ajmc);
        $("#show_ajlbdm").text(row.ajlbdm);
        $("#show_fxsj").text(row.fxsj);
        $("#show_dzmc").text(row.dzmc);
        $("#show_fazdxz").text(row.fazdxz);
        $("#show_padq").text(row.padq);
        $("#show_ladw_dwdm").text(row.ladw_dwdm);
        $("#show_ladw_dwmc").text(row.ladw_dwmc);
        $("#show_cbrxm").text(row.cbrxm);
        $("#show_cbr_lxdh").text(row.cbr_lxdh);
        $("#show_larq").text(row.larq);
        $("#show_padw_dwdm").text(row.padw_dwdm);
        $("#show_padw_dwmc").text(row.padw_dwmc);
        $("#show_badwlxdm").text(row.badwlxdm);
        $("#show_parq").text(row.parq);
        $("#show_cdfsdm").text(row.cdfsdm);
        $("#show_fyfsdm").text(row.fyfsdm);
        $("#show_sfjt").text(row.sfjt);
        $("#show_fzthxzdm").text(row.fzthxzdm);
        $("#show_fzthmc").text(row.fzthmc);
        $("#show_jyaq").text(row.jyaq);
        $("#show_jzrtgqk").text(row.jzrtgqk);
        $("#show_zarqkfx").text(row.zarqkfx);
        $("#show_asjzcxwlbdm").text(row.asjzcxwlbdm);
        $("#show_pafsdm").text(row.pafsdm);
        $("#show_cxajyydm").text(row.cxajyydm);
        $("#show_cxajrq").text(row.cxajrq);
        $("#show_cjqk").text(row.cjqk);
        $("#show_cjrq").text(row.cjrq);
        $("#show_gkfs").text(row.gkfs);
        $("#show_wqgkrq").text(row.wqgkrq);
        $("#show_syztdm").text(row.syztdm);
        $("#show_cxdw_dwmc").text(row.cxdw_dwmc);
        $("#show_cxdw_dwdm").text(row.cxdw_dwdm);
        $("#show_zcjddm").text(row.zcjddm);
        $("#show_tbdw_dwdm").text(row.tbdw_dwdm);
        $("#show_tbdw_dwmc").text(row.tbdw_dwmc);
        $("#show_djr_xm").text(row.djr_xm);
        $("#show_djrq").text(row.djrq);
        $("#show_bz").text(row.bz);
        $("#show_shdw_dwmc").text(row.shdw_dwmc);
        $("#show_shdw_dwdm").text(row.shdw_dwdm);
        $("#show_jhdp").text(row.jhdp);
        $("#show_qbtgdw_dwmc").text(row.qbtgdw_dwmc);
        $("#show_qbtgdw_dwdm").text(row.qbtgdw_dwdm);
        $("#show_fyqdsm").text(row.fyqdsm);
        $("#show_pdbz").text(row.pdbz);
        $("#show_ajdh").text(row.ajdh);
        $("#show_ajxz").text(row.ajxz);
        $("#show_zzdw_dwmc").text(row.zzdw_dwmc);
        $("#show_zzdw_dwdm").text(row.zzdw_dwdm);
        $("#show_zddw_dwmc").text(row.zddw_dwmc);
        $("#show_zddw_dwdm").text(row.zddw_dwdm);
        $("#show_gjhdqdm").text(row.gjhdqdm);
        $("#show_sadq_xzqh").text(row.sadq_xzqh);
        $("#show_fzrxm").text(row.fzrxm);
        $("#show_fzr_lxdh").text(row.fzr_lxdh);
        $("#show_xsly").text(row.xsly);
        $("#show_xsly_dwdm").text(row.xsly_dwdm);
        $("#show_xsly_dwmc").text(row.xsly_dwmc);
        $("#show_qyxsly").text(row.qyxsly);
        $("#show_mqzcgzjzqk").text(row.mqzcgzjzqk);
        $("#show_zcgzzwt").text(row.zcgzzwt);
        $("#show_xybgzjy").text(row.xybgzjy);
        $("#show_inforcontent").text(row.inforcontent);
        $("#show_asjdbjbdm").text(row.asjdbjbdm);
        $("#show_pazt").text(row.pazt);
        $("#show_ywlb").text(row.ywlb);
        $("#show_qlsj").text(row.qlsj);
        $("#show_zcsbqs").text(row.zcsbqs);
        $("#show_sqlhqs").text(row.sqlhqs);
        $("#show_sqlhspbs").text(row.sqlhspbs);
        $("#show_lhba").text(row.lhba);
        $("#show_lhzc_gajgjgdm").text(row.lhzc_gajgjgdm);
        $("#show_ajpasqb").text(row.ajpasqb);
        $("#show_paqlsj").text(row.paqlsj);
        $("#show_cxqs").text(row.cxqs);
        $("#show_lacbdq").text(row.lacbdq);
        $("#show_dqshjg").text(row.dqshjg);
        $("#show_shbz").text(row.shbz);
        $("#show_gxsj").text(row.gxsj);
        $("#show_yxx").text(row.yxx);
        $("#show_gxsjc").text(row.gxsjc);
        $("#show_xgbsdw").text(row.xgbsdw);
        $("#show_sfhlwaj").text(row.sfhlwaj);
        $("#show_hlwajlx").text(row.hlwajlx);
        $("#show_qtsm").text(row.qtsm);
        $("#show_sfsxxq").text(row.sfsxxq);
        $("#show_sdxqfs").text(row.sdxqfs);
        $("#show_xqqtsm").text(row.xqqtsm);
        $("#show_sfhsh").text(row.sfhsh);
        $("#show_sfwzfd").text(row.sfwzfd);
        $("#show_mbajzt").text(row.mbajzt);
        $("#show_ajdj").text(row.ajdj);
        $("#show_ajlx").text(row.ajlx);
        $("#show_b_m_jlbh").text(row.b_m_jlbh);
        $("#show_s_m_jlbh").text(row.s_m_jlbh);
        $("#show_c_m_jlbh").text(row.c_m_jlbh);
        $("#show_mbajsbdj").text(row.mbajsbdj);
        $("#show_xcxs").text(row.xcxs);
        $("#show_sfxc").text(row.sfxc);
        $("#show_sfqzjb").text(row.sfqzjb);
        $("#show_jjsqxx").text(row.jjsqxx);
        $("#show_sbdw_dwmc").text(row.sbdw_dwmc);
        $("#show_sbdw_dwdm").text(row.sbdw_dwdm);
        $("#show_lxr").text(row.lxr);
        $("#show_lxr_dh").text(row.lxr_dh);
        $("#show_b_inforcontent").text(row.b_inforcontent);
        $("#show_s_inforcontent").text(row.s_inforcontent);
        $("#show_c_inforcontent").text(row.c_inforcontent);
        $("#show_x_inforcontent").text(row.x_inforcontent);
        $("#show_czbs").text(row.czbs);
        $("#show_zyqxdzfs").text(row.zyqxdzfs);
        $("#show_cjzdbh").text(row.cjzdbh);
        $("#show_tqrybh").text(row.tqrybh);
        $("#show_qbcpbh").text(row.qbcpbh);
        $("#show_zsjjsqxx").text(row.zsjjsqxx);
        $("#show_b_inforcontent_sqsj").text(row.b_inforcontent_sqsj);
        $("#show_s_inforcontent_sqsj").text(row.s_inforcontent_sqsj);
        $("#show_c_inforcontent_sqsj").text(row.c_inforcontent_sqsj);
        $("#show_x_inforcontent_sqsj").text(row.x_inforcontent_sqsj);
        $("#show_bsxm_lx").text(row.bsxm_lx);
        $("#show_bm_qlsj").text(row.bm_qlsj);
        $("#show_sm_qlsj").text(row.sm_qlsj);
        $("#show_cm_qlsj").text(row.cm_qlsj);
        $("#show_cszt").text(row.cszt);
        $("#show_cbrxm2").text(row.cbrxm2);
        $("#show_cbr_lxdh2").text(row.cbr_lxdh2);
        $("#show_pafsdm_old").text(row.pafsdm_old);
        $("#show_n_yxh").text(row.n_yxh);
        $("#show_sjly").text(row.sjly);
        $("#show_qqczcs").text(row.qqczcs);
        $("#show_zzly").text(row.zzly);
        $("#show_mbajlb").text(row.mbajlb);
        $("#show_mbajxh").text(row.mbajxh);
        $("#show_ajly").text(row.ajly);
        $("#show_jzcd_ajlx").text(row.jzcd_ajlx);
        $("#show_ly").text(row.ly);

        $("#showModal").modal();
    },
    "click #editBtn":function (e,vale,row,index) {
    	//设置模态框标题
        $("#editModalLabel").text("**修改");
        //设置操作类型
        $("#edit_curdType").val("update");
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").attr('disabled','disabled');
        
        //请根据需要修改/删除 需展示字段
        //注意：字典类型数据需要dm和mc都进行设置
        $("#edit_jlbh").val(row.jlbh);
        $("#edit_ajbh").val(row.ajbh);
        $("#edit_ajmc").val(row.ajmc);
        $("#edit_ajlbdm").val(row.ajlbdm);
        $("#edit_fxsj").val(row.fxsj);
        $("#edit_dzmc").val(row.dzmc);
        $("#edit_fazdxz").val(row.fazdxz);
        $("#edit_padq").val(row.padq);
        $("#edit_ladw_dwdm").val(row.ladw_dwdm);
        $("#edit_ladw_dwmc").val(row.ladw_dwmc);
        $("#edit_cbrxm").val(row.cbrxm);
        $("#edit_cbr_lxdh").val(row.cbr_lxdh);
        $("#edit_larq").val(row.larq);
        $("#edit_padw_dwdm").val(row.padw_dwdm);
        $("#edit_padw_dwmc").val(row.padw_dwmc);
        $("#edit_badwlxdm").val(row.badwlxdm);
        $("#edit_parq").val(row.parq);
        $("#edit_cdfsdm").val(row.cdfsdm);
        $("#edit_fyfsdm").val(row.fyfsdm);
        $("#edit_sfjt").val(row.sfjt);
        $("#edit_fzthxzdm").val(row.fzthxzdm);
        $("#edit_fzthmc").val(row.fzthmc);
        $("#edit_jyaq").val(row.jyaq);
        $("#edit_jzrtgqk").val(row.jzrtgqk);
        $("#edit_zarqkfx").val(row.zarqkfx);
        $("#edit_asjzcxwlbdm").val(row.asjzcxwlbdm);
        $("#edit_pafsdm").val(row.pafsdm);
        $("#edit_cxajyydm").val(row.cxajyydm);
        $("#edit_cxajrq").val(row.cxajrq);
        $("#edit_cjqk").val(row.cjqk);
        $("#edit_cjrq").val(row.cjrq);
        $("#edit_gkfs").val(row.gkfs);
        $("#edit_wqgkrq").val(row.wqgkrq);
        $("#edit_syztdm").val(row.syztdm);
        $("#edit_cxdw_dwmc").val(row.cxdw_dwmc);
        $("#edit_cxdw_dwdm").val(row.cxdw_dwdm);
        $("#edit_zcjddm").val(row.zcjddm);
        $("#edit_tbdw_dwdm").val(row.tbdw_dwdm);
        $("#edit_tbdw_dwmc").val(row.tbdw_dwmc);
        $("#edit_djr_xm").val(row.djr_xm);
        $("#edit_djrq").val(row.djrq);
        $("#edit_bz").val(row.bz);
        $("#edit_shdw_dwmc").val(row.shdw_dwmc);
        $("#edit_shdw_dwdm").val(row.shdw_dwdm);
        $("#edit_jhdp").val(row.jhdp);
        $("#edit_qbtgdw_dwmc").val(row.qbtgdw_dwmc);
        $("#edit_qbtgdw_dwdm").val(row.qbtgdw_dwdm);
        $("#edit_fyqdsm").val(row.fyqdsm);
        $("#edit_pdbz").val(row.pdbz);
        $("#edit_ajdh").val(row.ajdh);
        $("#edit_ajxz").val(row.ajxz);
        $("#edit_zzdw_dwmc").val(row.zzdw_dwmc);
        $("#edit_zzdw_dwdm").val(row.zzdw_dwdm);
        $("#edit_zddw_dwmc").val(row.zddw_dwmc);
        $("#edit_zddw_dwdm").val(row.zddw_dwdm);
        $("#edit_gjhdqdm").val(row.gjhdqdm);
        $("#edit_sadq_xzqh").val(row.sadq_xzqh);
        $("#edit_fzrxm").val(row.fzrxm);
        $("#edit_fzr_lxdh").val(row.fzr_lxdh);
        $("#edit_xsly").val(row.xsly);
        $("#edit_xsly_dwdm").val(row.xsly_dwdm);
        $("#edit_xsly_dwmc").val(row.xsly_dwmc);
        $("#edit_qyxsly").val(row.qyxsly);
        $("#edit_mqzcgzjzqk").val(row.mqzcgzjzqk);
        $("#edit_zcgzzwt").val(row.zcgzzwt);
        $("#edit_xybgzjy").val(row.xybgzjy);
        $("#edit_inforcontent").val(row.inforcontent);
        $("#edit_asjdbjbdm").val(row.asjdbjbdm);
        $("#edit_pazt").val(row.pazt);
        $("#edit_ywlb").val(row.ywlb);
        $("#edit_qlsj").val(row.qlsj);
        $("#edit_zcsbqs").val(row.zcsbqs);
        $("#edit_sqlhqs").val(row.sqlhqs);
        $("#edit_sqlhspbs").val(row.sqlhspbs);
        $("#edit_lhba").val(row.lhba);
        $("#edit_lhzc_gajgjgdm").val(row.lhzc_gajgjgdm);
        $("#edit_ajpasqb").val(row.ajpasqb);
        $("#edit_paqlsj").val(row.paqlsj);
        $("#edit_cxqs").val(row.cxqs);
        $("#edit_lacbdq").val(row.lacbdq);
        $("#edit_dqshjg").val(row.dqshjg);
        $("#edit_shbz").val(row.shbz);
        $("#edit_gxsj").val(row.gxsj);
        $("#edit_yxx").val(row.yxx);
        $("#edit_gxsjc").val(row.gxsjc);
        $("#edit_xgbsdw").val(row.xgbsdw);
        $("#edit_sfhlwaj").val(row.sfhlwaj);
        $("#edit_hlwajlx").val(row.hlwajlx);
        $("#edit_qtsm").val(row.qtsm);
        $("#edit_sfsxxq").val(row.sfsxxq);
        $("#edit_sdxqfs").val(row.sdxqfs);
        $("#edit_xqqtsm").val(row.xqqtsm);
        $("#edit_sfhsh").val(row.sfhsh);
        $("#edit_sfwzfd").val(row.sfwzfd);
        $("#edit_mbajzt").val(row.mbajzt);
        $("#edit_ajdj").val(row.ajdj);
        $("#edit_ajlx").val(row.ajlx);
        $("#edit_b_m_jlbh").val(row.b_m_jlbh);
        $("#edit_s_m_jlbh").val(row.s_m_jlbh);
        $("#edit_c_m_jlbh").val(row.c_m_jlbh);
        $("#edit_mbajsbdj").val(row.mbajsbdj);
        $("#edit_xcxs").val(row.xcxs);
        $("#edit_sfxc").val(row.sfxc);
        $("#edit_sfqzjb").val(row.sfqzjb);
        $("#edit_jjsqxx").val(row.jjsqxx);
        $("#edit_sbdw_dwmc").val(row.sbdw_dwmc);
        $("#edit_sbdw_dwdm").val(row.sbdw_dwdm);
        $("#edit_lxr").val(row.lxr);
        $("#edit_lxr_dh").val(row.lxr_dh);
        $("#edit_b_inforcontent").val(row.b_inforcontent);
        $("#edit_s_inforcontent").val(row.s_inforcontent);
        $("#edit_c_inforcontent").val(row.c_inforcontent);
        $("#edit_x_inforcontent").val(row.x_inforcontent);
        $("#edit_czbs").val(row.czbs);
        $("#edit_zyqxdzfs").val(row.zyqxdzfs);
        $("#edit_cjzdbh").val(row.cjzdbh);
        $("#edit_tqrybh").val(row.tqrybh);
        $("#edit_qbcpbh").val(row.qbcpbh);
        $("#edit_zsjjsqxx").val(row.zsjjsqxx);
        $("#edit_b_inforcontent_sqsj").val(row.b_inforcontent_sqsj);
        $("#edit_s_inforcontent_sqsj").val(row.s_inforcontent_sqsj);
        $("#edit_c_inforcontent_sqsj").val(row.c_inforcontent_sqsj);
        $("#edit_x_inforcontent_sqsj").val(row.x_inforcontent_sqsj);
        $("#edit_bsxm_lx").val(row.bsxm_lx);
        $("#edit_bm_qlsj").val(row.bm_qlsj);
        $("#edit_sm_qlsj").val(row.sm_qlsj);
        $("#edit_cm_qlsj").val(row.cm_qlsj);
        $("#edit_cszt").val(row.cszt);
        $("#edit_cbrxm2").val(row.cbrxm2);
        $("#edit_cbr_lxdh2").val(row.cbr_lxdh2);
        $("#edit_pafsdm_old").val(row.pafsdm_old);
        $("#edit_n_yxh").val(row.n_yxh);
        $("#edit_sjly").val(row.sjly);
        $("#edit_qqczcs").val(row.qqczcs);
        $("#edit_zzly").val(row.zzly);
        $("#edit_mbajlb").val(row.mbajlb);
        $("#edit_mbajxh").val(row.mbajxh);
        $("#edit_ajly").val(row.ajly);
        $("#edit_jzcd_ajlx").val(row.jzcd_ajlx);
        $("#edit_ly").val(row.ly);

		//模态框的确认(保存)按钮绑定事件
        $("#editConfirmBtn").on("click",function(){
            //curdTbDpajJbxx();
            tbDpajJbxxEditSubmit();
        });
        $("#editModal").modal();
    },
    "click #delBtn":function (e,vale,row,index) {
        $("#tipsModalLabel").html("<i class='fa fa-warning fa-lg' style='color: #c87f0a'></i>警告");//提示标题
        $("#tipsMessage").text("确认删除么？");//设置提示信息
        $("#tipsConfirmBtn").text("删除");//按钮名称
        $("#tipsConfirmBtn").on("click",function(){
            deleteTbDpajJbxx(row.jlbh);
        });
        $("#tipsModal").modal();
    }
}

function addTbDpajJbxx() {
    $("#editModalLabel").text("**新增");
    $("#edit_curdType").val("insert");
    
    //请根据需要修改/删除 需展示字段
    //此处设置字段与修改时相同，是为清空点击修改时为元素的赋值
    //注意：字典类型数据需要dm和mc都进行设置
    $("#edit_jlbh").val('');
    $("#edit_ajbh").val('');
    $("#edit_ajmc").val('');
    $("#edit_ajlbdm").val('');
    $("#edit_fxsj").val('');
    $("#edit_dzmc").val('');
    $("#edit_fazdxz").val('');
    $("#edit_padq").val('');
    $("#edit_ladw_dwdm").val('');
    $("#edit_ladw_dwmc").val('');
    $("#edit_cbrxm").val('');
    $("#edit_cbr_lxdh").val('');
    $("#edit_larq").val('');
    $("#edit_padw_dwdm").val('');
    $("#edit_padw_dwmc").val('');
    $("#edit_badwlxdm").val('');
    $("#edit_parq").val('');
    $("#edit_cdfsdm").val('');
    $("#edit_fyfsdm").val('');
    $("#edit_sfjt").val('');
    $("#edit_fzthxzdm").val('');
    $("#edit_fzthmc").val('');
    $("#edit_jyaq").val('');
    $("#edit_jzrtgqk").val('');
    $("#edit_zarqkfx").val('');
    $("#edit_asjzcxwlbdm").val('');
    $("#edit_pafsdm").val('');
    $("#edit_cxajyydm").val('');
    $("#edit_cxajrq").val('');
    $("#edit_cjqk").val('');
    $("#edit_cjrq").val('');
    $("#edit_gkfs").val('');
    $("#edit_wqgkrq").val('');
    $("#edit_syztdm").val('');
    $("#edit_cxdw_dwmc").val('');
    $("#edit_cxdw_dwdm").val('');
    $("#edit_zcjddm").val('');
    $("#edit_tbdw_dwdm").val('');
    $("#edit_tbdw_dwmc").val('');
    $("#edit_djr_xm").val('');
    $("#edit_djrq").val('');
    $("#edit_bz").val('');
    $("#edit_shdw_dwmc").val('');
    $("#edit_shdw_dwdm").val('');
    $("#edit_jhdp").val('');
    $("#edit_qbtgdw_dwmc").val('');
    $("#edit_qbtgdw_dwdm").val('');
    $("#edit_fyqdsm").val('');
    $("#edit_pdbz").val('');
    $("#edit_ajdh").val('');
    $("#edit_ajxz").val('');
    $("#edit_zzdw_dwmc").val('');
    $("#edit_zzdw_dwdm").val('');
    $("#edit_zddw_dwmc").val('');
    $("#edit_zddw_dwdm").val('');
    $("#edit_gjhdqdm").val('');
    $("#edit_sadq_xzqh").val('');
    $("#edit_fzrxm").val('');
    $("#edit_fzr_lxdh").val('');
    $("#edit_xsly").val('');
    $("#edit_xsly_dwdm").val('');
    $("#edit_xsly_dwmc").val('');
    $("#edit_qyxsly").val('');
    $("#edit_mqzcgzjzqk").val('');
    $("#edit_zcgzzwt").val('');
    $("#edit_xybgzjy").val('');
    $("#edit_inforcontent").val('');
    $("#edit_asjdbjbdm").val('');
    $("#edit_pazt").val('');
    $("#edit_ywlb").val('');
    $("#edit_qlsj").val('');
    $("#edit_zcsbqs").val('');
    $("#edit_sqlhqs").val('');
    $("#edit_sqlhspbs").val('');
    $("#edit_lhba").val('');
    $("#edit_lhzc_gajgjgdm").val('');
    $("#edit_ajpasqb").val('');
    $("#edit_paqlsj").val('');
    $("#edit_cxqs").val('');
    $("#edit_lacbdq").val('');
    $("#edit_dqshjg").val('');
    $("#edit_shbz").val('');
    $("#edit_gxsj").val('');
    $("#edit_yxx").val('');
    $("#edit_gxsjc").val('');
    $("#edit_xgbsdw").val('');
    $("#edit_sfhlwaj").val('');
    $("#edit_hlwajlx").val('');
    $("#edit_qtsm").val('');
    $("#edit_sfsxxq").val('');
    $("#edit_sdxqfs").val('');
    $("#edit_xqqtsm").val('');
    $("#edit_sfhsh").val('');
    $("#edit_sfwzfd").val('');
    $("#edit_mbajzt").val('');
    $("#edit_ajdj").val('');
    $("#edit_ajlx").val('');
    $("#edit_b_m_jlbh").val('');
    $("#edit_s_m_jlbh").val('');
    $("#edit_c_m_jlbh").val('');
    $("#edit_mbajsbdj").val('');
    $("#edit_xcxs").val('');
    $("#edit_sfxc").val('');
    $("#edit_sfqzjb").val('');
    $("#edit_jjsqxx").val('');
    $("#edit_sbdw_dwmc").val('');
    $("#edit_sbdw_dwdm").val('');
    $("#edit_lxr").val('');
    $("#edit_lxr_dh").val('');
    $("#edit_b_inforcontent").val('');
    $("#edit_s_inforcontent").val('');
    $("#edit_c_inforcontent").val('');
    $("#edit_x_inforcontent").val('');
    $("#edit_czbs").val('');
    $("#edit_zyqxdzfs").val('');
    $("#edit_cjzdbh").val('');
    $("#edit_tqrybh").val('');
    $("#edit_qbcpbh").val('');
    $("#edit_zsjjsqxx").val('');
    $("#edit_b_inforcontent_sqsj").val('');
    $("#edit_s_inforcontent_sqsj").val('');
    $("#edit_c_inforcontent_sqsj").val('');
    $("#edit_x_inforcontent_sqsj").val('');
    $("#edit_bsxm_lx").val('');
    $("#edit_bm_qlsj").val('');
    $("#edit_sm_qlsj").val('');
    $("#edit_cm_qlsj").val('');
    $("#edit_cszt").val('');
    $("#edit_cbrxm2").val('');
    $("#edit_cbr_lxdh2").val('');
    $("#edit_pafsdm_old").val('');
    $("#edit_n_yxh").val('');
    $("#edit_sjly").val('');
    $("#edit_qqczcs").val('');
    $("#edit_zzly").val('');
    $("#edit_mbajlb").val('');
    $("#edit_mbajxh").val('');
    $("#edit_ajly").val('');
    $("#edit_jzcd_ajlx").val('');
    $("#edit_ly").val('');
    
    $("#editConfirmBtn").on("click",function(){
        //curdTbDpajJbxx();
        tbDpajJbxxEditSubmit();
    });

    $("#editModal").modal();
}

function deleteTbDpajJbxx(id){
    var reqData={"curdType":"delete","jlbh":id};
    $("#tipsModal").modal('toggle');
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/Jdzh/tbDpajJbxx/curd" ,
        data: reqData,
        success: function (result) {
            alert(result.msg);
            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function curdTbDpajJbxx() {
	//防止重复点击
    $("#editConfirmBtn").attr('disabled','disabled');
    var reqData = $('#tbDpajJbxxEditForm').serializeJsonObject();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/Jdzh/tbDpajJbxx/curd" ,
        data: reqData,
        success : function(data) {
            alert(data.msg);
            $("#editModal").modal('toggle');

            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function search() {
    //inittbDpajJbxxGrid();
    $("#tbDpajJbxxGrid").bootstrapTable("refresh");
}
function exportTbDpajJbxx(){
    var form = document.tbDpajJbxxQueryForm;
    var url = ctx+"/Jdzh/tbDpajJbxx/excel";
    form.action=url;
    form.submit();
}

var tbDpajJbxxEditSubmit = function(){
    var flag = $('#tbDpajJbxxEditForm').validate('submitValidate');
    if (flag){
        curdTbDpajJbxx();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}

