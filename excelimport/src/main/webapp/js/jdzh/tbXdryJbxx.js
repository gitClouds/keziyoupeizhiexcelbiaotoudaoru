
$(function () {
    initTbXdryJbxxGrid();
    formEditValidate('tbXdryJbxxEditForm');//表单验证
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
    $('#editModal').on('hidden.bs.modal', function (event) {
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").removeAttr('disabled');
        $("#editConfirmBtn").removeAttr('disabled','disabled');
        $("#editConfirmBtn").unbind();
        formEditValidateReset('tbXdryJbxxEditForm');
    });
})

function initTbXdryJbxxGrid(){
    $('#tbXdryJbxxGrid').bootstrapTable('destroy');

    $("#tbXdryJbxxGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/jdzh/xdlist?pks='+pks+'&yayklx='+lx,//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryTbXdryJbxxParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
		showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
        //请根据需要修改/删除 需展示字段
            {
                title : "所属发起账号",
                field : "sszh",
                align : "center",
                formatter: function (value, row, index) {
                    var zhpk = row.zhpk;
                    var cj = row.sscj;
                    if (zhpk!=null && zhpk!=''){
                        return "<a href='javascript:void(0);' onclick='glyayk(\""+zhpk+"\")'>"+value+"("+cj+"级)</a>";
                    }else{
                        return "-";
                    }
                }
            },/*
            {
                title : "所属发起层级",
                field : "sscj",
                align : "center"
            },*/
            {
                title : "嫌疑人编号",
                field : "xyrbh",
                align : "center"
            },
            {
                title : "姓名",
                field : "xm",
                align : "center"
            },
            {
                title : "性别",
                field : "xbdm",
                align : "center",
                formatter: function (value, row, index) {
                    return dicMap["XB"][value];
                }
            },
            {
                title : "出生日期",
                field : "csrq",
                align : "center",
                formatter: function (value, row, index) {
                    return value?new Date(value).Format("yyyy-MM-dd"):"-";
                }
            },
            {
                title : "证件种类",
                field : "cyzjdm",
                align : "center",
                formatter: function (value, row, index) {
                    return dicMap["CYZJDM"][value];
                }
            },
            {
                title : "证件号码",
                field : "sfzhm18",
                align : "center",
                formatter: function (value, row, index) {
                    var zl = row.cyzjdm;
                    var zjhm = row.zjhm;
                    if (zl!=null&&zl!=''&&zl=='111'){
                        return value;
                    }else if(zl!=null&&zl!=''&&zl!='111'){
                        return zjhm;
                    }
                }
            },
            {
                title : "户籍地址",
                field : "hjdz_dzmc",
                align : "center"
            },
            {
                title : "现居住地地址",
                field : "sjjzd_dzmc",
                align : "center"
            },
            {
                title : "类型",
                field : "lx",
                align : "center",
                formatter: function (value, row, index) {
                    if (value==1){
                        return "涉毒"
                    }else if(value==2){
                        return "吸毒"
                    }
                }
            },
            {
                title : "来源",
                field : "ly",
                align : "center",
                formatter: function (value, row, index) {
                    if (value==1){
                        return "禁毒库"
                    }else if(value==2){
                        return "本地录入"
                    }
                }
            },
            {
                title : "操作",
                field : "ryxh",
                align : "center",
                valign : "middle",
                width : "150px",
                events:tbXdryJbxxOperateEvents,
                formatter : tbXdryJbxxOperationFormatter
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}

function glyayk(zhpk) {
    var item = {'id':'yaykShow','name':'一案一库详情','url':ctx+'/yayk/show?pk='+zhpk};
    iframeTab.parentAddIframe(item);
}

//操作栏的格式化
function tbXdryJbxxOperationFormatter(value, row, index) {
    var lx = row.lx;
    var pk = row.ryxh;
    var ly = row.ly;
    var result = "";
    result += "<a href='javascript:void(0);' onclick='ryxq(\""+lx+"\",\""+pk+"\")' class='btn' title='详情'><i class='fa fa-search fa-lg'></i></a>";
    if (pks==null || pks==""){
        if (ly==2) {
            result += "<a href='javascript:void(0);' onclick='addNew(" + lx + ",\"update\",\"" + pk + "\")' class='btn' title='编辑'><span class='fa fa-edit fa-lg'></span></a>";
        }
    }
    return result;
}

function ryxq(lx,pk){
    var name = "禁毒综合人员详情";
    if (lx==1){
        name = "涉毒人员详情";
    }else if(lx==2){
        name = "吸毒人员详情";
    }
    var item = {'id':'jdzhryxq','name':name,'url':ctx+'/jdzh/jdzhryxq?pk='+pk+'&lx='+lx};
    iframeTab.parentAddIframe(item);
}

function addNew(lx,type,pk){
    var name;
    if (type=='insert'){
        if (lx==1){
            name = '新增涉毒人员信息';
        }else if (lx==2){
            name = '新增吸毒人员信息';
        }
        pk = '';
    }else if(type=='update'){
        if (lx==1){
            name = '修改涉毒人员信息';
        }else if (lx==2){
            name = '修改吸毒人员信息';
        }
    }
    var item = {'id':'rycl','name':name,'url':ctx+'/jdzh/addeditPage?lx='+lx+'&type='+type+'&pk='+pk};
    iframeTab.parentAddIframe(item);
}
// 以起始页码方式传入参数,params为table组装参数
function queryTbXdryJbxxParams(params){
    var tmp = $("#tbXdryJbxxQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
/*function tbXdryJbxxOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' id='showBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";/!*onclick=\"showTbXdryJbxx('" + id + "', view='view')\"*!/
    result += "<a href='javascript:void(0);' id='editBtn' class='btn' title='编辑'><span class='fa fa-edit fa-lg'></span></a>";/!*onclick=\"editTbXdryJbxx('" + id + "')\"*!/
    result += "<a href='javascript:void(0);' id='delBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";/!*onclick=\"deleteTbXdryJbxx('" + id + "')\"*!/

    return result;
}*/
//操作栏绑定事件
window.tbXdryJbxxOperateEvents = {
    "click #showBtn":function (e,vale,row,index) {
    	$("#showModalLabel").text("**详情");
    	//请根据需要修改/删除 需展示字段
        $("#show_ryxh").text(row.ryxh);
        $("#show_xdryly").text(row.xdryly);
        $("#show_ywlx").text(row.ywlx);
        $("#show_ywxh").text(row.ywxh);
        $("#show_xyrbh").text(row.xyrbh);
        $("#show_xm").text(row.xm);
        $("#show_xdrydjlydm").text(row.xdrydjlydm);
        $("#show_cyzjdm").text(row.cyzjdm);
        $("#show_sfzhm18").text(row.sfzhm18);
        $("#show_gmsfhm").text(row.gmsfhm);
        $("#show_zjhm").text(row.zjhm);
        $("#show_bmch").text(row.bmch);
        $("#show_xbdm").text(row.xbdm);
        $("#show_csrq").text(row.csrq);
        $("#show_mzdm").text(row.mzdm);
        $("#show_sg").text(row.sg);
        $("#show_jggjdqdm").text(row.jggjdqdm);
        $("#show_xldm").text(row.xldm);
        $("#show_grsfdm").text(row.grsfdm);
        $("#show_hyzkdm").text(row.hyzkdm);
        $("#show_jyqk").text(row.jyqk);
        $("#show_gzdw_dwmc").text(row.gzdw_dwmc);
        $("#show_szzwbh").text(row.szzwbh);
        $("#show_rydnabh").text(row.rydnabh);
        $("#show_hjdz_xzqhmc").text(row.hjdz_xzqhmc);
        $("#show_hjdz_dzmc").text(row.hjdz_dzmc);
        $("#show_hjdz_gajgmc").text(row.hjdz_gajgmc);
        $("#show_sjjzd_xzqhmc").text(row.sjjzd_xzqhmc);
        $("#show_sjjzd_dzmc").text(row.sjjzd_dzmc);
        $("#show_sjjzd_gajgmc").text(row.sjjzd_gajgmc);
        $("#show_lrr_xm").text(row.lrr_xm);
        $("#show_lrdw_dwmc").text(row.lrdw_dwmc);
        $("#show_lrsj").text(row.lrsj);
        $("#show_sf828k").text(row.sf828k);
        $("#show_sdrybh").text(row.sdrybh);
        $("#show_yxh").text(row.yxh);
        $("#show_inoutbz").text(row.inoutbz);
        $("#show_gsdw").text(row.gsdw);
        $("#show_cyzk").text(row.cyzk);
        $("#show_rybs").text(row.rybs);
        $("#show_nccbz").text(row.nccbz);
        $("#show_czbs").text(row.czbs);
        $("#show_gxsjc").text(row.gxsjc);
        $("#show_xgbsdw").text(row.xgbsdw);
        $("#show_ccbz").text(row.ccbz);
        $("#show_yxx").text(row.yxx);
        $("#show_gxsj").text(row.gxsj);
        $("#show_lrdw_dwdm").text(row.lrdw_dwdm);
        $("#show_hjdz_xzqhdm").text(row.hjdz_xzqhdm);
        $("#show_hjdz_gajgdm").text(row.hjdz_gajgdm);
        $("#show_sjjzd_xzqhdm").text(row.sjjzd_xzqhdm);
        $("#show_sjjzd_gajgdm").text(row.sjjzd_gajgdm);
        $("#show_xp").text(row.xp);
        $("#show_fjxh").text(row.fjxh);
        $("#show_xxmc").text(row.xxmc);
        $("#show_ryxh").text(row.ryxh);
        $("#show_xdryly").text(row.xdryly);
        $("#show_ywlx").text(row.ywlx);
        $("#show_ywxh").text(row.ywxh);
        $("#show_xyrbh").text(row.xyrbh);
        $("#show_xm").text(row.xm);
        $("#show_xdrydjlydm").text(row.xdrydjlydm);
        $("#show_cyzjdm").text(row.cyzjdm);
        $("#show_sfzhm18").text(row.sfzhm18);
        $("#show_gmsfhm").text(row.gmsfhm);
        $("#show_zjhm").text(row.zjhm);
        $("#show_bmch").text(row.bmch);
        $("#show_xbdm").text(row.xbdm);
        $("#show_csrq").text(row.csrq);
        $("#show_mzdm").text(row.mzdm);
        $("#show_sg").text(row.sg);
        $("#show_jggjdqdm").text(row.jggjdqdm);
        $("#show_xldm").text(row.xldm);
        $("#show_grsfdm").text(row.grsfdm);
        $("#show_hyzkdm").text(row.hyzkdm);
        $("#show_jyqk").text(row.jyqk);
        $("#show_gzdw_dwmc").text(row.gzdw_dwmc);
        $("#show_szzwbh").text(row.szzwbh);
        $("#show_rydnabh").text(row.rydnabh);
        $("#show_hjdz_xzqhmc").text(row.hjdz_xzqhmc);
        $("#show_hjdz_dzmc").text(row.hjdz_dzmc);
        $("#show_hjdz_gajgmc").text(row.hjdz_gajgmc);
        $("#show_sjjzd_xzqhmc").text(row.sjjzd_xzqhmc);
        $("#show_sjjzd_dzmc").text(row.sjjzd_dzmc);
        $("#show_sjjzd_gajgmc").text(row.sjjzd_gajgmc);
        $("#show_lrr_xm").text(row.lrr_xm);
        $("#show_lrdw_dwmc").text(row.lrdw_dwmc);
        $("#show_lrsj").text(row.lrsj);
        $("#show_sf828k").text(row.sf828k);
        $("#show_sdrybh").text(row.sdrybh);
        $("#show_yxh").text(row.yxh);
        $("#show_inoutbz").text(row.inoutbz);
        $("#show_gsdw").text(row.gsdw);
        $("#show_cyzk").text(row.cyzk);
        $("#show_rybs").text(row.rybs);
        $("#show_nccbz").text(row.nccbz);
        $("#show_czbs").text(row.czbs);
        $("#show_gxsjc").text(row.gxsjc);
        $("#show_xgbsdw").text(row.xgbsdw);
        $("#show_ccbz").text(row.ccbz);
        $("#show_yxx").text(row.yxx);
        $("#show_gxsj").text(row.gxsj);
        $("#show_lrdw_dwdm").text(row.lrdw_dwdm);
        $("#show_hjdz_xzqhdm").text(row.hjdz_xzqhdm);
        $("#show_hjdz_gajgdm").text(row.hjdz_gajgdm);
        $("#show_sjjzd_xzqhdm").text(row.sjjzd_xzqhdm);
        $("#show_sjjzd_gajgdm").text(row.sjjzd_gajgdm);
        $("#show_xp").text(row.xp);
        $("#show_fjxh").text(row.fjxh);
        $("#show_xxmc").text(row.xxmc);

        $("#showModal").modal();
    },
    "click #editBtn":function (e,vale,row,index) {
    	//设置模态框标题
        $("#editModalLabel").text("**修改");
        //设置操作类型
        $("#edit_curdType").val("update");
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").attr('disabled','disabled');
        
        //请根据需要修改/删除 需展示字段
        //注意：字典类型数据需要dm和mc都进行设置
        $("#edit_ryxh").val(row.ryxh);
        $("#edit_xdryly").val(row.xdryly);
        $("#edit_ywlx").val(row.ywlx);
        $("#edit_ywxh").val(row.ywxh);
        $("#edit_xyrbh").val(row.xyrbh);
        $("#edit_xm").val(row.xm);
        $("#edit_xdrydjlydm").val(row.xdrydjlydm);
        $("#edit_cyzjdm").val(row.cyzjdm);
        $("#edit_sfzhm18").val(row.sfzhm18);
        $("#edit_gmsfhm").val(row.gmsfhm);
        $("#edit_zjhm").val(row.zjhm);
        $("#edit_bmch").val(row.bmch);
        $("#edit_xbdm").val(row.xbdm);
        $("#edit_csrq").val(row.csrq);
        $("#edit_mzdm").val(row.mzdm);
        $("#edit_sg").val(row.sg);
        $("#edit_jggjdqdm").val(row.jggjdqdm);
        $("#edit_xldm").val(row.xldm);
        $("#edit_grsfdm").val(row.grsfdm);
        $("#edit_hyzkdm").val(row.hyzkdm);
        $("#edit_jyqk").val(row.jyqk);
        $("#edit_gzdw_dwmc").val(row.gzdw_dwmc);
        $("#edit_szzwbh").val(row.szzwbh);
        $("#edit_rydnabh").val(row.rydnabh);
        $("#edit_hjdz_xzqhmc").val(row.hjdz_xzqhmc);
        $("#edit_hjdz_dzmc").val(row.hjdz_dzmc);
        $("#edit_hjdz_gajgmc").val(row.hjdz_gajgmc);
        $("#edit_sjjzd_xzqhmc").val(row.sjjzd_xzqhmc);
        $("#edit_sjjzd_dzmc").val(row.sjjzd_dzmc);
        $("#edit_sjjzd_gajgmc").val(row.sjjzd_gajgmc);
        $("#edit_lrr_xm").val(row.lrr_xm);
        $("#edit_lrdw_dwmc").val(row.lrdw_dwmc);
        $("#edit_lrsj").val(row.lrsj);
        $("#edit_sf828k").val(row.sf828k);
        $("#edit_sdrybh").val(row.sdrybh);
        $("#edit_yxh").val(row.yxh);
        $("#edit_inoutbz").val(row.inoutbz);
        $("#edit_gsdw").val(row.gsdw);
        $("#edit_cyzk").val(row.cyzk);
        $("#edit_rybs").val(row.rybs);
        $("#edit_nccbz").val(row.nccbz);
        $("#edit_czbs").val(row.czbs);
        $("#edit_gxsjc").val(row.gxsjc);
        $("#edit_xgbsdw").val(row.xgbsdw);
        $("#edit_ccbz").val(row.ccbz);
        $("#edit_yxx").val(row.yxx);
        $("#edit_gxsj").val(row.gxsj);
        $("#edit_lrdw_dwdm").val(row.lrdw_dwdm);
        $("#edit_hjdz_xzqhdm").val(row.hjdz_xzqhdm);
        $("#edit_hjdz_gajgdm").val(row.hjdz_gajgdm);
        $("#edit_sjjzd_xzqhdm").val(row.sjjzd_xzqhdm);
        $("#edit_sjjzd_gajgdm").val(row.sjjzd_gajgdm);
        $("#edit_xp").val(row.xp);
        $("#edit_fjxh").val(row.fjxh);
        $("#edit_xxmc").val(row.xxmc);
        $("#edit_ryxh").val(row.ryxh);
        $("#edit_xdryly").val(row.xdryly);
        $("#edit_ywlx").val(row.ywlx);
        $("#edit_ywxh").val(row.ywxh);
        $("#edit_xyrbh").val(row.xyrbh);
        $("#edit_xm").val(row.xm);
        $("#edit_xdrydjlydm").val(row.xdrydjlydm);
        $("#edit_cyzjdm").val(row.cyzjdm);
        $("#edit_sfzhm18").val(row.sfzhm18);
        $("#edit_gmsfhm").val(row.gmsfhm);
        $("#edit_zjhm").val(row.zjhm);
        $("#edit_bmch").val(row.bmch);
        $("#edit_xbdm").val(row.xbdm);
        $("#edit_csrq").val(row.csrq);
        $("#edit_mzdm").val(row.mzdm);
        $("#edit_sg").val(row.sg);
        $("#edit_jggjdqdm").val(row.jggjdqdm);
        $("#edit_xldm").val(row.xldm);
        $("#edit_grsfdm").val(row.grsfdm);
        $("#edit_hyzkdm").val(row.hyzkdm);
        $("#edit_jyqk").val(row.jyqk);
        $("#edit_gzdw_dwmc").val(row.gzdw_dwmc);
        $("#edit_szzwbh").val(row.szzwbh);
        $("#edit_rydnabh").val(row.rydnabh);
        $("#edit_hjdz_xzqhmc").val(row.hjdz_xzqhmc);
        $("#edit_hjdz_dzmc").val(row.hjdz_dzmc);
        $("#edit_hjdz_gajgmc").val(row.hjdz_gajgmc);
        $("#edit_sjjzd_xzqhmc").val(row.sjjzd_xzqhmc);
        $("#edit_sjjzd_dzmc").val(row.sjjzd_dzmc);
        $("#edit_sjjzd_gajgmc").val(row.sjjzd_gajgmc);
        $("#edit_lrr_xm").val(row.lrr_xm);
        $("#edit_lrdw_dwmc").val(row.lrdw_dwmc);
        $("#edit_lrsj").val(row.lrsj);
        $("#edit_sf828k").val(row.sf828k);
        $("#edit_sdrybh").val(row.sdrybh);
        $("#edit_yxh").val(row.yxh);
        $("#edit_inoutbz").val(row.inoutbz);
        $("#edit_gsdw").val(row.gsdw);
        $("#edit_cyzk").val(row.cyzk);
        $("#edit_rybs").val(row.rybs);
        $("#edit_nccbz").val(row.nccbz);
        $("#edit_czbs").val(row.czbs);
        $("#edit_gxsjc").val(row.gxsjc);
        $("#edit_xgbsdw").val(row.xgbsdw);
        $("#edit_ccbz").val(row.ccbz);
        $("#edit_yxx").val(row.yxx);
        $("#edit_gxsj").val(row.gxsj);
        $("#edit_lrdw_dwdm").val(row.lrdw_dwdm);
        $("#edit_hjdz_xzqhdm").val(row.hjdz_xzqhdm);
        $("#edit_hjdz_gajgdm").val(row.hjdz_gajgdm);
        $("#edit_sjjzd_xzqhdm").val(row.sjjzd_xzqhdm);
        $("#edit_sjjzd_gajgdm").val(row.sjjzd_gajgdm);
        $("#edit_xp").val(row.xp);
        $("#edit_fjxh").val(row.fjxh);
        $("#edit_xxmc").val(row.xxmc);

		//模态框的确认(保存)按钮绑定事件
        $("#editConfirmBtn").on("click",function(){
            //curdTbXdryJbxx();
            tbXdryJbxxEditSubmit();
        });
        $("#editModal").modal();
    },
    "click #delBtn":function (e,vale,row,index) {
        $("#tipsModalLabel").html("<i class='fa fa-warning fa-lg' style='color: #c87f0a'></i>警告");//提示标题
        $("#tipsMessage").text("确认删除么？");//设置提示信息
        $("#tipsConfirmBtn").text("删除");//按钮名称
        $("#tipsConfirmBtn").on("click",function(){
            deleteTbXdryJbxx(row.ryxh);
        });
        $("#tipsModal").modal();
    }
}

function addTbXdryJbxx() {
    $("#editModalLabel").text("**新增");
    $("#edit_curdType").val("insert");
    
    //请根据需要修改/删除 需展示字段
    //此处设置字段与修改时相同，是为清空点击修改时为元素的赋值
    //注意：字典类型数据需要dm和mc都进行设置
    $("#edit_ryxh").val('');
    $("#edit_xdryly").val('');
    $("#edit_ywlx").val('');
    $("#edit_ywxh").val('');
    $("#edit_xyrbh").val('');
    $("#edit_xm").val('');
    $("#edit_xdrydjlydm").val('');
    $("#edit_cyzjdm").val('');
    $("#edit_sfzhm18").val('');
    $("#edit_gmsfhm").val('');
    $("#edit_zjhm").val('');
    $("#edit_bmch").val('');
    $("#edit_xbdm").val('');
    $("#edit_csrq").val('');
    $("#edit_mzdm").val('');
    $("#edit_sg").val('');
    $("#edit_jggjdqdm").val('');
    $("#edit_xldm").val('');
    $("#edit_grsfdm").val('');
    $("#edit_hyzkdm").val('');
    $("#edit_jyqk").val('');
    $("#edit_gzdw_dwmc").val('');
    $("#edit_szzwbh").val('');
    $("#edit_rydnabh").val('');
    $("#edit_hjdz_xzqhmc").val('');
    $("#edit_hjdz_dzmc").val('');
    $("#edit_hjdz_gajgmc").val('');
    $("#edit_sjjzd_xzqhmc").val('');
    $("#edit_sjjzd_dzmc").val('');
    $("#edit_sjjzd_gajgmc").val('');
    $("#edit_lrr_xm").val('');
    $("#edit_lrdw_dwmc").val('');
    $("#edit_lrsj").val('');
    $("#edit_sf828k").val('');
    $("#edit_sdrybh").val('');
    $("#edit_yxh").val('');
    $("#edit_inoutbz").val('');
    $("#edit_gsdw").val('');
    $("#edit_cyzk").val('');
    $("#edit_rybs").val('');
    $("#edit_nccbz").val('');
    $("#edit_czbs").val('');
    $("#edit_gxsjc").val('');
    $("#edit_xgbsdw").val('');
    $("#edit_ccbz").val('');
    $("#edit_yxx").val('');
    $("#edit_gxsj").val('');
    $("#edit_lrdw_dwdm").val('');
    $("#edit_hjdz_xzqhdm").val('');
    $("#edit_hjdz_gajgdm").val('');
    $("#edit_sjjzd_xzqhdm").val('');
    $("#edit_sjjzd_gajgdm").val('');
    $("#edit_xp").val('');
    $("#edit_fjxh").val('');
    $("#edit_xxmc").val('');
    $("#edit_ryxh").val('');
    $("#edit_xdryly").val('');
    $("#edit_ywlx").val('');
    $("#edit_ywxh").val('');
    $("#edit_xyrbh").val('');
    $("#edit_xm").val('');
    $("#edit_xdrydjlydm").val('');
    $("#edit_cyzjdm").val('');
    $("#edit_sfzhm18").val('');
    $("#edit_gmsfhm").val('');
    $("#edit_zjhm").val('');
    $("#edit_bmch").val('');
    $("#edit_xbdm").val('');
    $("#edit_csrq").val('');
    $("#edit_mzdm").val('');
    $("#edit_sg").val('');
    $("#edit_jggjdqdm").val('');
    $("#edit_xldm").val('');
    $("#edit_grsfdm").val('');
    $("#edit_hyzkdm").val('');
    $("#edit_jyqk").val('');
    $("#edit_gzdw_dwmc").val('');
    $("#edit_szzwbh").val('');
    $("#edit_rydnabh").val('');
    $("#edit_hjdz_xzqhmc").val('');
    $("#edit_hjdz_dzmc").val('');
    $("#edit_hjdz_gajgmc").val('');
    $("#edit_sjjzd_xzqhmc").val('');
    $("#edit_sjjzd_dzmc").val('');
    $("#edit_sjjzd_gajgmc").val('');
    $("#edit_lrr_xm").val('');
    $("#edit_lrdw_dwmc").val('');
    $("#edit_lrsj").val('');
    $("#edit_sf828k").val('');
    $("#edit_sdrybh").val('');
    $("#edit_yxh").val('');
    $("#edit_inoutbz").val('');
    $("#edit_gsdw").val('');
    $("#edit_cyzk").val('');
    $("#edit_rybs").val('');
    $("#edit_nccbz").val('');
    $("#edit_czbs").val('');
    $("#edit_gxsjc").val('');
    $("#edit_xgbsdw").val('');
    $("#edit_ccbz").val('');
    $("#edit_yxx").val('');
    $("#edit_gxsj").val('');
    $("#edit_lrdw_dwdm").val('');
    $("#edit_hjdz_xzqhdm").val('');
    $("#edit_hjdz_gajgdm").val('');
    $("#edit_sjjzd_xzqhdm").val('');
    $("#edit_sjjzd_gajgdm").val('');
    $("#edit_xp").val('');
    $("#edit_fjxh").val('');
    $("#edit_xxmc").val('');
    
    $("#editConfirmBtn").on("click",function(){
        //curdTbXdryJbxx();
        tbXdryJbxxEditSubmit();
    });

    $("#editModal").modal();
}

function deleteTbXdryJbxx(id){
    var reqData={"curdType":"delete","ryxh":id};
    $("#tipsModal").modal('toggle');
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/Jdzh/tbXdryJbxx/curd" ,
        data: reqData,
        success: function (result) {
            alert(result.msg);
            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function curdTbXdryJbxx() {
	//防止重复点击
    $("#editConfirmBtn").attr('disabled','disabled');
    var reqData = $('#tbXdryJbxxEditForm').serializeJsonObject();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/Jdzh/tbXdryJbxx/curd" ,
        data: reqData,
        success : function(data) {
            alert(data.msg);
            $("#editModal").modal('toggle');

            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function search() {
    //inittbXdryJbxxGrid();
    $("#tbXdryJbxxGrid").bootstrapTable("refresh");
}
function exportTbXdryJbxx(){
    var form = document.tbXdryJbxxQueryForm;
    var url = ctx+"/Jdzh/tbXdryJbxx/excel";
    form.action=url;
    form.submit();
}

var tbXdryJbxxEditSubmit = function(){
    var flag = $('#tbXdryJbxxEditForm').validate('submitValidate');
    if (flag){
        curdTbXdryJbxx();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}

