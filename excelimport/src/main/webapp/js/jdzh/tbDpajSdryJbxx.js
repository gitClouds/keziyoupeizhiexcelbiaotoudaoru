
$(function () {
    initTbDpajSdryJbxxGrid();
    formEditValidate('tbDpajSdryJbxxEditForm');//表单验证
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
    $('#editModal').on('hidden.bs.modal', function (event) {
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").removeAttr('disabled');
        $("#editConfirmBtn").removeAttr('disabled','disabled');
        $("#editConfirmBtn").unbind();
        formEditValidateReset('tbDpajSdryJbxxEditForm');
    });
    initcookie();
})

function initTbDpajSdryJbxxGrid(){
    $('#tbDpajSdryJbxxGrid').bootstrapTable('destroy');

    $("#tbDpajSdryJbxxGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/jdzh/sdlist?pks='+pks+'&yayklx='+lx,//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryTbDpajSdryJbxxParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
		showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
        //请根据需要修改/删除 需展示字段
            {
                title : "所属发起账号",
                field : "sszh",
                align : "center",
                formatter: function (value, row, index) {
                    var zhpk = row.zhpk;
                    var cj = row.sscj;
                    if (zhpk!=null && zhpk!=''){
                        return "<a href='javascript:void(0);' onclick='glyayk(\""+zhpk+"\")'>"+value+"("+cj+"级)</a>";
                    }else{
                        return "-";
                    }
                }
            },/*
            {
                title : "所属发起层级",
                field : "sscj",
                align : "center"
            },*/
            {
                title : "嫌疑人编号",
                field : "xyrbh",
                align : "center"
            },
            {
                title : "姓名",
                field : "xm",
                align : "center"
            },
            {
                title : "性别",
                field : "xbdm",
                align : "center",
                formatter: function (value, row, index) {
                    return dicMap["XB"][value];
                }
            },
            {
                title : "出生日期",
                field : "csrq",
                align : "center",
                formatter: function (value, row, index) {
                    return value?new Date(value).Format("yyyy-MM-dd"):"-";
                }
            },
            {
                title : "证件种类",
                field : "cyzjdm",
                align : "center",
                formatter: function (value, row, index) {
                    return dicMap["CYZJDM"][value];
                }
            },
            {
                title : "证件号码",
                field : "zjhm",
                align : "center",
                formatter: function (value, row, index) {
                    var zl = row.cyzjdm;
                    var qtzjhm = row.qtzjhm;
                    if (zl!=null&&zl!=''&&zl=='111'){
                        return value;
                    }else if(zl!=null&&zl!=''&&zl!='111'){
                        return qtzjhm;
                    }
                }
            },
            {
                title : "户籍地址",
                field : "hjdxz",
                align : "center"
            },
            {
                title : "现居住地地址",
                field : "sjjzdxz",
                align : "center"
            },
            {
                title : "类型",
                field : "lx",
                align : "center",
                formatter: function (value, row, index) {
                    if (value==1){
                        return "涉毒"
                    }else if(value==2){
                        return "吸毒"
                    }
                }
            },
            {
                title : "来源",
                field : "ly",
                align : "center",
                formatter: function (value, row, index) {
                    if (value==1){
                        return "禁毒库"
                    }else if(value==2){
                        return "本地录入"
                    }
                }
            },
            {
                title : "操作",
                field : "jlbh",
                align : "center",
                valign : "middle",
                width : "150px",
                events:tbDpajSdryJbxxOperateEvents,
                formatter : tbDpajSdryJbxxOperationFormatter
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}
function glyayk(zhpk) {
    var item = {'id':'yaykShow','name':'一案一库详情','url':ctx+'/yayk/show?pk='+zhpk};
    iframeTab.parentAddIframe(item);
}

// 以起始页码方式传入参数,params为table组装参数
function queryTbDpajSdryJbxxParams(params){
    var tmp = $("#tbDpajSdryJbxxQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function tbDpajSdryJbxxOperationFormatter(value, row, index) {
    var lx = row.lx;
    var pk = row.jlbh;
    var ly = row.ly;
    var result = "";
    result += "<a href='javascript:void(0);' onclick='ryxq(\""+lx+"\",\""+pk+"\")' class='btn' title='详情'><i class='fa fa-search fa-lg'></i></a>";
    if (pks==null || pks==""){
        if (ly==2) {
            result += "<a href='javascript:void(0);' onclick='addNew(" + lx + ",\"update\",\"" + pk + "\")' class='btn' title='编辑'><span class='fa fa-edit fa-lg'></span></a>";
        }
    }
    return result;
}

function ryxq(lx,pk){
    var name = "禁毒综合人员详情";
    if (lx==1){
        name = "涉毒人员详情";
    }else if(lx==2){
        name = "吸毒人员详情";
    }
    var item = {'id':'jdzhryxq','name':name,'url':ctx+'/jdzh/jdzhryxq?pk='+pk+'&lx='+lx};
    iframeTab.parentAddIframe(item);
}

function addNew(lx,type,pk){
    var name;
    if (type=='insert'){
        if (lx==1){
            name = '新增涉毒人员信息';
        }else if (lx==2){
            name = '新增吸毒人员信息';
        }
        pk = '';
    }else if(type=='update'){
        if (lx==1){
            name = '修改涉毒人员信息';
        }else if (lx==2){
            name = '修改吸毒人员信息';
        }
    }
    var item = {'id':'rycl','name':name,'url':ctx+'/jdzh/addeditPage?lx='+lx+'&type='+type+'&pk='+pk};
    iframeTab.parentAddIframe(item);
}

//操作栏绑定事件
window.tbDpajSdryJbxxOperateEvents = {
    /*"click #showBtn":function (e,vale,row,index) {
        //请根据需要修改/删除 需展示字段
        $("#show_jlbh").text(row.jlbh);
        $("#show_xyrbh").text(row.xyrbh);
        $("#show_tbdw_dwmc").text(row.tbdw_dwmc);
        $("#show_djr_xm").text(row.djr_xm);
        $("#show_xm").text(row.xm);
        $("#show_cym").text(row.cym);
        $("#show_cyzjdm").text(row.cyzjdm);
        $("#show_zjhm").text(row.zjhm);
        $("#show_qtzjhm").text(row.qtzjhm);
        $("#show_qq").text(row.qq);
        $("#show_hjd_xzqh").text(row.hjd_xzqh);
        $("#show_hjdxz").text(row.hjdxz);
        $("#show_hjd_dwdm").text(row.hjd_dwdm);
        $("#show_hjd_dwmc").text(row.hjd_dwmc);
        $("#show_sjjzd_xzqh").text(row.sjjzd_xzqh);
        $("#show_sjjzdxz").text(row.sjjzdxz);
        $("#show_sjjzd_dwmc").text(row.sjjzd_dwmc);
        $("#show_sjjzd_dwdm").text(row.sjjzd_dwdm);
        $("#show_qtdz_xzqh").text(row.qtdz_xzqh);
        $("#show_qtzzxz").text(row.qtzzxz);
        $("#show_bmch").text(row.bmch);
        $("#show_xbdm").text(row.xbdm);
        $("#show_csrq").text(row.csrq);
        $("#show_csdssxdm").text(row.csdssxdm);
        $("#show_grsfdm").text(row.grsfdm);
        $("#show_xldm").text(row.xldm);
        $("#show_mzdm").text(row.mzdm);
        $("#show_fwcs").text(row.fwcs);
        $("#show_sg").text(row.sg);
        $("#show_tmtzms").text(row.tmtzms);
        $("#show_br_lxdh").text(row.br_lxdh);
        $("#show_lxrxx").text(row.lxrxx);
        $("#show_gjdm").text(row.gjdm);
        $("#show_hyzkdm").text(row.hyzkdm);
        $("#show_ryxz").text(row.ryxz);
        $("#show_xzdjrq").text(row.xzdjrq);
        $("#show_ccxdrq").text(row.ccxdrq);
        $("#show_dpfzxyrlx").text(row.dpfzxyrlx);
        $("#show_bz").text(row.bz);
        $("#show_djrq").text(row.djrq);
        $("#show_sdrylx").text(row.sdrylx);
        $("#show_cxdw_dwmc").text(row.cxdw_dwmc);
        $("#show_xmhypy").text(row.xmhypy);
        $("#show_tbdq_xzqh").text(row.tbdq_xzqh);
        $("#show_ztrylxdm").text(row.ztrylxdm);
        $("#show_wsbh").text(row.wsbh);
        $("#show_yzdxxxyrlrly").text(row.yzdxxxyrlrly);
        $("#show_yqtxqx").text(row.yqtxqx);
        $("#show_qtzt").text(row.qtzt);
        $("#show_rylb").text(row.rylb);
        $("#show_ryjsdm").text(row.ryjsdm);
        $("#show_qtjsdm").text(row.qtjsdm);
        $("#show_nl").text(row.nl);
        $("#show_jgssxdm").text(row.jgssxdm);
        $("#show_jl").text(row.jl);
        $("#show_jwdz").text(row.jwdz);
        $("#show_txdz").text(row.txdz);
        $("#show_wwm").text(row.wwm);
        $("#show_wwx").text(row.wwx);
        $("#show_xxdm").text(row.xxdm);
        $("#show_zjxydm").text(row.zjxydm);
        $("#show_zy").text(row.zy);
        $("#show_zylbdm").text(row.zylbdm);
        $("#show_zzmmdm").text(row.zzmmdm);
        $("#show_hykydm").text(row.hykydm);
        $("#show_sfsw").text(row.sfsw);
        $("#show_swsj").text(row.swsj);
        $("#show_xdjlhcjgdm").text(row.xdjlhcjgdm);
        $("#show_yz").text(row.yz);
        $("#show_saryzcdm").text(row.saryzcdm);
        $("#show_fzzlx").text(row.fzzlx);
        $("#show_qyfwdm").text(row.qyfwdm);
        $("#show_bkzt").text(row.bkzt);
        $("#show_fzztlxdm").text(row.fzztlxdm);
        $("#show_tbdw_dwdm").text(row.tbdw_dwdm);
        $("#show_tbdq").text(row.tbdq);
        $("#show_jkzk").text(row.jkzk);
        $("#show_czbz").text(row.czbz);
        $("#show_wlsfxx").text(row.wlsfxx);
        $("#show_gjsj").text(row.gjsj);
        $("#show_yxx").text(row.yxx);
        $("#show_gxsj").text(row.gxsj);
        $("#show_gxsjc").text(row.gxsjc);
        $("#show_xgbsdw").text(row.xgbsdw);
        $("#show_sjch").text(row.sjch);
        $("#show_sjxh").text(row.sjxh);
        $("#show_gh").text(row.gh);
        $("#show_email").text(row.email);
        $("#show_xnsf").text(row.xnsf);
        $("#show_wx").text(row.wx);
        $("#show_ry_dwmc").text(row.ry_dwmc);
        $("#show_tsbj").text(row.tsbj);
        $("#show_sczt").text(row.sczt);
        $("#show_sjhgsd").text(row.sjhgsd);
        $("#show_ryzp").text(row.ryzp);
        $("#show_fzlx").text(row.fzlx);
        $("#show_ryly").text(row.ryly);
        $("#show_czbs").text(row.czbs);
        $("#show_cszt").text(row.cszt);
        $("#show_yqtxqxbf").text(row.yqtxqxbf);
        $("#show_csdssxdmbf").text(row.csdssxdmbf);
        $("#show_jgssxdmbf").text(row.jgssxdmbf);
        $("#show_sgbf").text(row.sgbf);
        $("#show_bhsbh").text(row.bhsbh);
        $("#show_xp").text(row.xp);
        $("#show_n_yxh").text(row.n_yxh);
        $("#show_lxrlxfs").text(row.lxrlxfs);
        $("#show_lxrgx").text(row.lxrgx);
        $("#show_sjly").text(row.sjly);
        $("#showModal").modal();*!/
    },*/
    "click #editBtn":function (e,vale,row,index) {
    	//设置模态框标题
        $("#editModalLabel").text("**修改");
        //设置操作类型
        $("#edit_curdType").val("update");
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").attr('disabled','disabled');
        
        //请根据需要修改/删除 需展示字段
        //注意：字典类型数据需要dm和mc都进行设置
        $("#edit_jlbh").val(row.jlbh);
        $("#edit_xyrbh").val(row.xyrbh);
        $("#edit_tbdw_dwmc").val(row.tbdw_dwmc);
        $("#edit_djr_xm").val(row.djr_xm);
        $("#edit_xm").val(row.xm);
        $("#edit_cym").val(row.cym);
        $("#edit_cyzjdm").val(row.cyzjdm);
        $("#edit_zjhm").val(row.zjhm);
        $("#edit_qtzjhm").val(row.qtzjhm);
        $("#edit_qq").val(row.qq);
        $("#edit_hjd_xzqh").val(row.hjd_xzqh);
        $("#edit_hjdxz").val(row.hjdxz);
        $("#edit_hjd_dwdm").val(row.hjd_dwdm);
        $("#edit_hjd_dwmc").val(row.hjd_dwmc);
        $("#edit_sjjzd_xzqh").val(row.sjjzd_xzqh);
        $("#edit_sjjzdxz").val(row.sjjzdxz);
        $("#edit_sjjzd_dwmc").val(row.sjjzd_dwmc);
        $("#edit_sjjzd_dwdm").val(row.sjjzd_dwdm);
        $("#edit_qtdz_xzqh").val(row.qtdz_xzqh);
        $("#edit_qtzzxz").val(row.qtzzxz);
        $("#edit_bmch").val(row.bmch);
        $("#edit_xbdm").val(row.xbdm);
        $("#edit_csrq").val(row.csrq);
        $("#edit_csdssxdm").val(row.csdssxdm);
        $("#edit_grsfdm").val(row.grsfdm);
        $("#edit_xldm").val(row.xldm);
        $("#edit_mzdm").val(row.mzdm);
        $("#edit_fwcs").val(row.fwcs);
        $("#edit_sg").val(row.sg);
        $("#edit_tmtzms").val(row.tmtzms);
        $("#edit_br_lxdh").val(row.br_lxdh);
        $("#edit_lxrxx").val(row.lxrxx);
        $("#edit_gjdm").val(row.gjdm);
        $("#edit_hyzkdm").val(row.hyzkdm);
        $("#edit_ryxz").val(row.ryxz);
        $("#edit_xzdjrq").val(row.xzdjrq);
        $("#edit_ccxdrq").val(row.ccxdrq);
        $("#edit_dpfzxyrlx").val(row.dpfzxyrlx);
        $("#edit_bz").val(row.bz);
        $("#edit_djrq").val(row.djrq);
        $("#edit_sdrylx").val(row.sdrylx);
        $("#edit_cxdw_dwmc").val(row.cxdw_dwmc);
        $("#edit_xmhypy").val(row.xmhypy);
        $("#edit_tbdq_xzqh").val(row.tbdq_xzqh);
        $("#edit_ztrylxdm").val(row.ztrylxdm);
        $("#edit_wsbh").val(row.wsbh);
        $("#edit_yzdxxxyrlrly").val(row.yzdxxxyrlrly);
        $("#edit_yqtxqx").val(row.yqtxqx);
        $("#edit_qtzt").val(row.qtzt);
        $("#edit_rylb").val(row.rylb);
        $("#edit_ryjsdm").val(row.ryjsdm);
        $("#edit_qtjsdm").val(row.qtjsdm);
        $("#edit_nl").val(row.nl);
        $("#edit_jgssxdm").val(row.jgssxdm);
        $("#edit_jl").val(row.jl);
        $("#edit_jwdz").val(row.jwdz);
        $("#edit_txdz").val(row.txdz);
        $("#edit_wwm").val(row.wwm);
        $("#edit_wwx").val(row.wwx);
        $("#edit_xxdm").val(row.xxdm);
        $("#edit_zjxydm").val(row.zjxydm);
        $("#edit_zy").val(row.zy);
        $("#edit_zylbdm").val(row.zylbdm);
        $("#edit_zzmmdm").val(row.zzmmdm);
        $("#edit_hykydm").val(row.hykydm);
        $("#edit_sfsw").val(row.sfsw);
        $("#edit_swsj").val(row.swsj);
        $("#edit_xdjlhcjgdm").val(row.xdjlhcjgdm);
        $("#edit_yz").val(row.yz);
        $("#edit_saryzcdm").val(row.saryzcdm);
        $("#edit_fzzlx").val(row.fzzlx);
        $("#edit_qyfwdm").val(row.qyfwdm);
        $("#edit_bkzt").val(row.bkzt);
        $("#edit_fzztlxdm").val(row.fzztlxdm);
        $("#edit_tbdw_dwdm").val(row.tbdw_dwdm);
        $("#edit_tbdq").val(row.tbdq);
        $("#edit_jkzk").val(row.jkzk);
        $("#edit_czbz").val(row.czbz);
        $("#edit_wlsfxx").val(row.wlsfxx);
        $("#edit_gjsj").val(row.gjsj);
        $("#edit_yxx").val(row.yxx);
        $("#edit_gxsj").val(row.gxsj);
        $("#edit_gxsjc").val(row.gxsjc);
        $("#edit_xgbsdw").val(row.xgbsdw);
        $("#edit_sjch").val(row.sjch);
        $("#edit_sjxh").val(row.sjxh);
        $("#edit_gh").val(row.gh);
        $("#edit_email").val(row.email);
        $("#edit_xnsf").val(row.xnsf);
        $("#edit_wx").val(row.wx);
        $("#edit_ry_dwmc").val(row.ry_dwmc);
        $("#edit_tsbj").val(row.tsbj);
        $("#edit_sczt").val(row.sczt);
        $("#edit_sjhgsd").val(row.sjhgsd);
        $("#edit_ryzp").val(row.ryzp);
        $("#edit_fzlx").val(row.fzlx);
        $("#edit_ryly").val(row.ryly);
        $("#edit_czbs").val(row.czbs);
        $("#edit_cszt").val(row.cszt);
        $("#edit_yqtxqxbf").val(row.yqtxqxbf);
        $("#edit_csdssxdmbf").val(row.csdssxdmbf);
        $("#edit_jgssxdmbf").val(row.jgssxdmbf);
        $("#edit_sgbf").val(row.sgbf);
        $("#edit_bhsbh").val(row.bhsbh);
        $("#edit_xp").val(row.xp);
        $("#edit_n_yxh").val(row.n_yxh);
        $("#edit_lxrlxfs").val(row.lxrlxfs);
        $("#edit_lxrgx").val(row.lxrgx);
        $("#edit_sjly").val(row.sjly);

		//模态框的确认(保存)按钮绑定事件
        $("#editConfirmBtn").on("click",function(){
            //curdTbDpajSdryJbxx();
            tbDpajSdryJbxxEditSubmit();
        });
        $("#editModal").modal();
    },
    "click #delBtn":function (e,vale,row,index) {
        $("#tipsModalLabel").html("<i class='fa fa-warning fa-lg' style='color: #c87f0a'></i>警告");//提示标题
        $("#tipsMessage").text("确认删除么？");//设置提示信息
        $("#tipsConfirmBtn").text("删除");//按钮名称
        $("#tipsConfirmBtn").on("click",function(){
            deleteTbDpajSdryJbxx(row.jlbh);
        });
        $("#tipsModal").modal();
    }
}

function addTbDpajSdryJbxx() {
    $("#editModalLabel").text("涉毒新增");
    $("#edit_curdType").val("insert");
    
    //请根据需要修改/删除 需展示字段
    //此处设置字段与修改时相同，是为清空点击修改时为元素的赋值
    //注意：字典类型数据需要dm和mc都进行设置
    $("#edit_jlbh").val('');
    $("#edit_xyrbh").val('');
    $("#edit_tbdw_dwmc").val('');
    $("#edit_djr_xm").val('');
    $("#edit_xm").val('');
    $("#edit_cym").val('');
    $("#edit_cyzjdm").val('');
    $("#edit_zjhm").val('');
    $("#edit_qtzjhm").val('');
    $("#edit_qq").val('');
    $("#edit_hjd_xzqh").val('');
    $("#edit_hjdxz").val('');
    $("#edit_hjd_dwdm").val('');
    $("#edit_hjd_dwmc").val('');
    $("#edit_sjjzd_xzqh").val('');
    $("#edit_sjjzdxz").val('');
    $("#edit_sjjzd_dwmc").val('');
    $("#edit_sjjzd_dwdm").val('');
    $("#edit_qtdz_xzqh").val('');
    $("#edit_qtzzxz").val('');
    $("#edit_bmch").val('');
    $("#edit_xbdm").val('');
    $("#edit_csrq").val('');
    $("#edit_csdssxdm").val('');
    $("#edit_grsfdm").val('');
    $("#edit_xldm").val('');
    $("#edit_mzdm").val('');
    $("#edit_fwcs").val('');
    $("#edit_sg").val('');
    $("#edit_tmtzms").val('');
    $("#edit_br_lxdh").val('');
    $("#edit_lxrxx").val('');
    $("#edit_gjdm").val('');
    $("#edit_hyzkdm").val('');
    $("#edit_ryxz").val('');
    $("#edit_xzdjrq").val('');
    $("#edit_ccxdrq").val('');
    $("#edit_dpfzxyrlx").val('');
    $("#edit_bz").val('');
    $("#edit_djrq").val('');
    $("#edit_sdrylx").val('');
    $("#edit_cxdw_dwmc").val('');
    $("#edit_xmhypy").val('');
    $("#edit_tbdq_xzqh").val('');
    $("#edit_ztrylxdm").val('');
    $("#edit_wsbh").val('');
    $("#edit_yzdxxxyrlrly").val('');
    $("#edit_yqtxqx").val('');
    $("#edit_qtzt").val('');
    $("#edit_rylb").val('');
    $("#edit_ryjsdm").val('');
    $("#edit_qtjsdm").val('');
    $("#edit_nl").val('');
    $("#edit_jgssxdm").val('');
    $("#edit_jl").val('');
    $("#edit_jwdz").val('');
    $("#edit_txdz").val('');
    $("#edit_wwm").val('');
    $("#edit_wwx").val('');
    $("#edit_xxdm").val('');
    $("#edit_zjxydm").val('');
    $("#edit_zy").val('');
    $("#edit_zylbdm").val('');
    $("#edit_zzmmdm").val('');
    $("#edit_hykydm").val('');
    $("#edit_sfsw").val('');
    $("#edit_swsj").val('');
    $("#edit_xdjlhcjgdm").val('');
    $("#edit_yz").val('');
    $("#edit_saryzcdm").val('');
    $("#edit_fzzlx").val('');
    $("#edit_qyfwdm").val('');
    $("#edit_bkzt").val('');
    $("#edit_fzztlxdm").val('');
    $("#edit_tbdw_dwdm").val('');
    $("#edit_tbdq").val('');
    $("#edit_jkzk").val('');
    $("#edit_czbz").val('');
    $("#edit_wlsfxx").val('');
    $("#edit_gjsj").val('');
    $("#edit_yxx").val('');
    $("#edit_gxsj").val('');
    $("#edit_gxsjc").val('');
    $("#edit_xgbsdw").val('');
    $("#edit_sjch").val('');
    $("#edit_sjxh").val('');
    $("#edit_gh").val('');
    $("#edit_email").val('');
    $("#edit_xnsf").val('');
    $("#edit_wx").val('');
    $("#edit_ry_dwmc").val('');
    $("#edit_tsbj").val('');
    $("#edit_sczt").val('');
    $("#edit_sjhgsd").val('');
    $("#edit_ryzp").val('');
    $("#edit_fzlx").val('');
    $("#edit_ryly").val('');
    $("#edit_czbs").val('');
    $("#edit_cszt").val('');
    $("#edit_yqtxqxbf").val('');
    $("#edit_csdssxdmbf").val('');
    $("#edit_jgssxdmbf").val('');
    $("#edit_sgbf").val('');
    $("#edit_bhsbh").val('');
    $("#edit_xp").val('');
    $("#edit_n_yxh").val('');
    $("#edit_lxrlxfs").val('');
    $("#edit_lxrgx").val('');
    $("#edit_sjly").val('');

    $("#editConfirmBtn").on("click",function(){
        //curdTbDpajSdryJbxx();
        tbDpajSdryJbxxEditSubmit();
    });

    $("#editModal").modal();
}

function deleteTbDpajSdryJbxx(id){
    var reqData={"curdType":"delete","jlbh":id};
    $("#tipsModal").modal('toggle');
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/Jdzh/tbDpajSdryJbxx/curd" ,
        data: reqData,
        success: function (result) {
            alert(result.msg);
            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function curdTbDpajSdryJbxx() {
	//防止重复点击
    $("#editConfirmBtn").attr('disabled','disabled');
    var reqData = $('#tbDpajSdryJbxxEditForm').serializeJsonObject();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/Jdzh/tbDpajSdryJbxx/curd" ,
        data: reqData,
        success : function(data) {
            alert(data.msg);
            $("#editModal").modal('toggle');

            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function initcookie() {
    var cookie = localStorage.getItem("jdzhs");
    if (cookie!=null&&cookie!=''){
        var str = '';
        var r = cookie.split(",");
        for (var i=0;i<r.length;i++){
            str += "<a style='cursor:pointer;' onclick='jl(\""+r[i]+"\")'>"+r[i]+"</a>&nbsp;";
        }
        $("#zjss").html(str);
    }else {
        localStorage.setItem("jdzhs","");
    }
}

function setitem() {
    var a = $("#sszh").val();
    var cookie = localStorage.getItem("jdzhs");
    var html = $("#zjss").html();
    if (a!=null&&a!='') {
        if (cookie.length===0){
            cookie += a;
            html = "<a style='cursor:pointer;' onclick='jl(\""+a+"\")'>"+a+"</a>&nbsp;";
        }else{
            var r = cookie.split(",");
            if (r.indexOf(a)>-1){
                return;
            }
            if (r.length==5){
                cookie = cookie.substring(0,cookie.lastIndexOf(","));
                html = "<a style='cursor:pointer;' onclick='jl(\""+a+"\")'>"+a+"</a>&nbsp;"+html.substring(0,html.lastIndexOf("<a"));
            }else {
                html = "<a style='cursor:pointer;' onclick='jl(\""+a+"\")'>"+a+"</a>&nbsp;"+html;
            }
            cookie = a+","+cookie;
        }
        localStorage.setItem("jdzhs",cookie);
        $("#zjss").html(html);
    }
}

function jl(a) {
    $("#sszh").val(a);
    search();
}

function search() {
    //inittbDpajSdryJbxxGrid();
    setitem();
    $("#tbDpajSdryJbxxGrid").bootstrapTable("refresh");
}
function exportTbDpajSdryJbxx(){
    var form = document.tbDpajSdryJbxxQueryForm;
    var url = ctx+"/Jdzh/tbDpajSdryJbxx/excel";
    form.action=url;
    form.submit();
}

var tbDpajSdryJbxxEditSubmit = function(){
    var flag = $('#tbDpajSdryJbxxEditForm').validate('submitValidate');
    if (flag){
        curdTbDpajSdryJbxx();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}

