
$(function () {
    initTbDpajSdryChxxGrid();
    formEditValidate('tbDpajSdryChxxEditForm');//表单验证
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
    $('#editModal').on('hidden.bs.modal', function (event) {
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").removeAttr('disabled');
        $("#editConfirmBtn").removeAttr('disabled','disabled');
        $("#editConfirmBtn").unbind();
        formEditValidateReset('tbDpajSdryChxxEditForm');
    });
})

function initTbDpajSdryChxxGrid(){
    $('#tbDpajSdryChxxGrid').bootstrapTable('destroy');

    $("#tbDpajSdryChxxGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/jdzh/tbDpajSdryChxx/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryTbDpajSdryChxxParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
		showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
        //请根据需要修改/删除 需展示字段
            {
                title : "记录编号--DExxxxx--编号规则为“XXXXXXXXXXXX（公安机关机构代码）+YYYYMM（年月）+XXXXXXXXXXXXXX（顺序号）”                         ",
                field : "jlbh",
                align : "center"
            },
            {
                title : "立案编号--DE00092--编号规则为“A+XXXXXXXXXXXX（公安机关机构代码）+YYYYMM（年月）+XXXX（顺序号）”                         ",
                field : "ajbh",
                align : "center"
            },
            {
                title : "涉毒人员编号--DE00038--编号规则为：R+XXXXXXXXXXXX(公安机关机构代码)+XXXXXX(年月)+XXXX(顺序号)              ",
                field : "xyrbh",
                align : "center"
            },
            {
                title : "填报单位名称--DE00065--",
                field : "tbdw_dwmc",
                align : "center"
            },
            {
                title : "登记人--DE00002--",
                field : "djr_xm",
                align : "center"
            },
            {
                title : "登记日期--DE00554--",
                field : "djrq",
                align : "center"
            },
            {
                title : "涉毒类型--DExxxxx--",
                field : "sdlx",
                align : "center"
            },
            {
                title : "查获日期--DE00554--",
                field : "chrq",
                align : "center"
            },
            {
                title : "查获地点--DE00768--",
                field : "ddmc",
                align : "center"
            },
            {
                title : "活动区域--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码",
                field : "hdqh_xzqh",
                align : "center"
            },
            {
                title : "查获单位--DE00065--",
                field : "chdw_dwmc",
                align : "center"
            },
            {
                title : "处置情况--DExxxxx--",
                field : "czqk",
                align : "center"
            },
            {
                title : "查获地区--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码",
                field : "chd_xzqh",
                align : "center"
            },
            {
                title : "处置日期--DE00554--",
                field : "czrq",
                align : "center"
            },
            {
                title : "违法犯罪事实--DE00100--",
                field : "jyaq",
                align : "center"
            },
            {
                title : "银行卡发卡银行--DE00243--",
                field : "khyhmc",
                align : "center"
            },
            {
                title : "银行卡号--DExxxxx--",
                field : "xykh",
                align : "center"
            },
            {
                title : "银行账号--DE00241--",
                field : "yhzh",
                align : "center"
            },
            {
                title : "车牌号--DE00307--",
                field : "jdchphm",
                align : "center"
            },
            {
                title : "是否吸毒--DE00838--采用GA/T XXXXX-XXXX《吸毒记录核查结果代码》",
                field : "xdjlhcjgdm",
                align : "center"
            },
            {
                title : "在逃人员编号--DExxxxx、DE00618--",
                field : "ztrybh",
                align : "center"
            },
            {
                title : "立案地--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码",
                field : "lad_xzqh",
                align : "center"
            },
            {
                title : "上网追逃日期--DE00554--",
                field : "swzjrq",
                align : "center"
            },
            {
                title : "抓获地--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码",
                field : "zhd_xzqh",
                align : "center"
            },
            {
                title : "通缉级别--DE00232--采用GA240.54-2003《通缉级别代码》",
                field : "tjjbdm",
                align : "center"
            },
            {
                title : "抓获方式--DE01017--采用 GA/T XXX《抓获方式代码》",
                field : "zhfsdm",
                align : "center"
            },
            {
                title : "抓获线索来源--DE00170--采用GA 332.9 《禁毒信息管理代码 第9部分:线索提供方式代码》",
                field : "xstgfsdm",
                align : "center"
            },
            {
                title : "抓获经过--DE01012--",
                field : "zcxwnrms",
                align : "center"
            },
            {
                title : "户籍地--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码",
                field : "hjd_xzqh",
                align : "center"
            },
            {
                title : "是否逃跑--DExxxxx--",
                field : "sftp",
                align : "center"
            },
            {
                title : "审判后执行情况--DExxxxx--",
                field : "sphzxqk",
                align : "center"
            },
            {
                title : "移交地区--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码",
                field : "yjd_xzqh",
                align : "center"
            },
            {
                title : "结伙作案中作用--DExxxxx--",
                field : "jhzazzy",
                align : "center"
            },
            {
                title : "是/否特殊人群--DExxxxx--",
                field : "sftsrq",
                align : "center"
            },
            {
                title : "是/否高危预警人员--DExxxxx--",
                field : "sfgwyjry",
                align : "center"
            },
            {
                title : "逮捕证号--DExxxxx--",
                field : "dbzh",
                align : "center"
            },
            {
                title : "查获单位代码",
                field : "chdwdm",
                align : "center"
            },
            {
                title : "填报单位",
                field : "tbdw_dwdm",
                align : "center"
            },
            {
                title : "刑期-年",
                field : "xqn",
                align : "center"
            },
            {
                title : "刑期-月",
                field : "xqy",
                align : "center"
            },
            {
                title : "执行截止日期",
                field : "zxqzrq",
                align : "center"
            },
            {
                title : "更新时间",
                field : "gxsj",
                align : "center"
            },
            {
                title : "有效性",
                field : "yxx",
                align : "center"
            },
            {
                title : "更新时间戳--DEXXXXX--",
                field : "gxsjc",
                align : "center"
            },
            {
                title : "修改部署单位--DEXXXXX--",
                field : "xgbsdw",
                align : "center"
            },
            {
                title : "法律文书编号",
                field : "flwsbh",
                align : "center"
            },
            {
                title : "是否主犯",
                field : "sfzf",
                align : "center"
            },
            {
                title : "是否在逃",
                field : "sfzt",
                align : "center"
            },
            {
                title : "字段注释",
                field : "sftsrqlx",
                align : "center"
            },
            {
                title : "字段注释",
                field : "sfjhgk",
                align : "center"
            },
            {
                title : "字段注释",
                field : "zhsj",
                align : "center"
            },
            {
                title : "字段注释",
                field : "zhd",
                align : "center"
            },
            {
                title : "字段注释",
                field : "zhd_xxdq",
                align : "center"
            },
            {
                title : "字段注释",
                field : "zhd_xxdz",
                align : "center"
            },
            {
                title : "字段注释",
                field : "fzqk",
                align : "center"
            },
            {
                title : "字段注释",
                field : "fzlx",
                align : "center"
            },
            {
                title : "操作标识",
                field : "czbs",
                align : "center"
            },
            {
                title : "字段注释",
                field : "ryly",
                align : "center"
            },
            {
                title : "传输状态。0:未传输。1:已传输（新老系统数据传输）",
                field : "cszt",
                align : "center"
            },
            {
                title : "字段注释",
                field : "gjdm",
                align : "center"
            },
            {
                title : "字段注释",
                field : "sdryjlbh",
                align : "center"
            },
            {
                title : "字段注释",
                field : "chdwxc",
                align : "center"
            },
            {
                title : "旧系统特殊人群代码",
                field : "sftsrq_old",
                align : "center"
            },
            {
                title : "字段注释",
                field : "sjly",
                align : "center"
            },
            {
                title : "来源(1：接口下发2：本地录入)",
                field : "ly",
                align : "center"
            },
            {
                title : "操作",
                field : "jlbh",
                align : "center",
                valign : "middle",
                width : "150px",
                events:tbDpajSdryChxxOperateEvents,
                formatter : tbDpajSdryChxxOperationFormatter
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function queryTbDpajSdryChxxParams(params){
    var tmp = $("#tbDpajSdryChxxQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function tbDpajSdryChxxOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' id='showBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";/*onclick=\"showTbDpajSdryChxx('" + id + "', view='view')\"*/
    result += "<a href='javascript:void(0);' id='editBtn' class='btn' title='编辑'><span class='fa fa-edit fa-lg'></span></a>";/*onclick=\"editTbDpajSdryChxx('" + id + "')\"*/
    result += "<a href='javascript:void(0);' id='delBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";/*onclick=\"deleteTbDpajSdryChxx('" + id + "')\"*/

    return result;
}
//操作栏绑定事件
window.tbDpajSdryChxxOperateEvents = {
    "click #showBtn":function (e,vale,row,index) {
    	$("#showModalLabel").text("**详情");
    	//请根据需要修改/删除 需展示字段
        $("#show_jlbh").text(row.jlbh);
        $("#show_ajbh").text(row.ajbh);
        $("#show_xyrbh").text(row.xyrbh);
        $("#show_tbdw_dwmc").text(row.tbdw_dwmc);
        $("#show_djr_xm").text(row.djr_xm);
        $("#show_djrq").text(row.djrq);
        $("#show_sdlx").text(row.sdlx);
        $("#show_chrq").text(row.chrq);
        $("#show_ddmc").text(row.ddmc);
        $("#show_hdqh_xzqh").text(row.hdqh_xzqh);
        $("#show_chdw_dwmc").text(row.chdw_dwmc);
        $("#show_czqk").text(row.czqk);
        $("#show_chd_xzqh").text(row.chd_xzqh);
        $("#show_czrq").text(row.czrq);
        $("#show_jyaq").text(row.jyaq);
        $("#show_khyhmc").text(row.khyhmc);
        $("#show_xykh").text(row.xykh);
        $("#show_yhzh").text(row.yhzh);
        $("#show_jdchphm").text(row.jdchphm);
        $("#show_xdjlhcjgdm").text(row.xdjlhcjgdm);
        $("#show_ztrybh").text(row.ztrybh);
        $("#show_lad_xzqh").text(row.lad_xzqh);
        $("#show_swzjrq").text(row.swzjrq);
        $("#show_zhd_xzqh").text(row.zhd_xzqh);
        $("#show_tjjbdm").text(row.tjjbdm);
        $("#show_zhfsdm").text(row.zhfsdm);
        $("#show_xstgfsdm").text(row.xstgfsdm);
        $("#show_zcxwnrms").text(row.zcxwnrms);
        $("#show_hjd_xzqh").text(row.hjd_xzqh);
        $("#show_sftp").text(row.sftp);
        $("#show_sphzxqk").text(row.sphzxqk);
        $("#show_yjd_xzqh").text(row.yjd_xzqh);
        $("#show_jhzazzy").text(row.jhzazzy);
        $("#show_sftsrq").text(row.sftsrq);
        $("#show_sfgwyjry").text(row.sfgwyjry);
        $("#show_dbzh").text(row.dbzh);
        $("#show_chdwdm").text(row.chdwdm);
        $("#show_tbdw_dwdm").text(row.tbdw_dwdm);
        $("#show_xqn").text(row.xqn);
        $("#show_xqy").text(row.xqy);
        $("#show_zxqzrq").text(row.zxqzrq);
        $("#show_gxsj").text(row.gxsj);
        $("#show_yxx").text(row.yxx);
        $("#show_gxsjc").text(row.gxsjc);
        $("#show_xgbsdw").text(row.xgbsdw);
        $("#show_flwsbh").text(row.flwsbh);
        $("#show_sfzf").text(row.sfzf);
        $("#show_sfzt").text(row.sfzt);
        $("#show_sftsrqlx").text(row.sftsrqlx);
        $("#show_sfjhgk").text(row.sfjhgk);
        $("#show_zhsj").text(row.zhsj);
        $("#show_zhd").text(row.zhd);
        $("#show_zhd_xxdq").text(row.zhd_xxdq);
        $("#show_zhd_xxdz").text(row.zhd_xxdz);
        $("#show_fzqk").text(row.fzqk);
        $("#show_fzlx").text(row.fzlx);
        $("#show_czbs").text(row.czbs);
        $("#show_ryly").text(row.ryly);
        $("#show_cszt").text(row.cszt);
        $("#show_gjdm").text(row.gjdm);
        $("#show_sdryjlbh").text(row.sdryjlbh);
        $("#show_chdwxc").text(row.chdwxc);
        $("#show_sftsrq_old").text(row.sftsrq_old);
        $("#show_sjly").text(row.sjly);
        $("#show_ly").text(row.ly);

        $("#showModal").modal();
    },
    "click #editBtn":function (e,vale,row,index) {
    	//设置模态框标题
        $("#editModalLabel").text("**修改");
        //设置操作类型
        $("#edit_curdType").val("update");
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").attr('disabled','disabled');
        
        //请根据需要修改/删除 需展示字段
        //注意：字典类型数据需要dm和mc都进行设置
        $("#edit_jlbh").val(row.jlbh);
        $("#edit_ajbh").val(row.ajbh);
        $("#edit_xyrbh").val(row.xyrbh);
        $("#edit_tbdw_dwmc").val(row.tbdw_dwmc);
        $("#edit_djr_xm").val(row.djr_xm);
        $("#edit_djrq").val(row.djrq);
        $("#edit_sdlx").val(row.sdlx);
        $("#edit_chrq").val(row.chrq);
        $("#edit_ddmc").val(row.ddmc);
        $("#edit_hdqh_xzqh").val(row.hdqh_xzqh);
        $("#edit_chdw_dwmc").val(row.chdw_dwmc);
        $("#edit_czqk").val(row.czqk);
        $("#edit_chd_xzqh").val(row.chd_xzqh);
        $("#edit_czrq").val(row.czrq);
        $("#edit_jyaq").val(row.jyaq);
        $("#edit_khyhmc").val(row.khyhmc);
        $("#edit_xykh").val(row.xykh);
        $("#edit_yhzh").val(row.yhzh);
        $("#edit_jdchphm").val(row.jdchphm);
        $("#edit_xdjlhcjgdm").val(row.xdjlhcjgdm);
        $("#edit_ztrybh").val(row.ztrybh);
        $("#edit_lad_xzqh").val(row.lad_xzqh);
        $("#edit_swzjrq").val(row.swzjrq);
        $("#edit_zhd_xzqh").val(row.zhd_xzqh);
        $("#edit_tjjbdm").val(row.tjjbdm);
        $("#edit_zhfsdm").val(row.zhfsdm);
        $("#edit_xstgfsdm").val(row.xstgfsdm);
        $("#edit_zcxwnrms").val(row.zcxwnrms);
        $("#edit_hjd_xzqh").val(row.hjd_xzqh);
        $("#edit_sftp").val(row.sftp);
        $("#edit_sphzxqk").val(row.sphzxqk);
        $("#edit_yjd_xzqh").val(row.yjd_xzqh);
        $("#edit_jhzazzy").val(row.jhzazzy);
        $("#edit_sftsrq").val(row.sftsrq);
        $("#edit_sfgwyjry").val(row.sfgwyjry);
        $("#edit_dbzh").val(row.dbzh);
        $("#edit_chdwdm").val(row.chdwdm);
        $("#edit_tbdw_dwdm").val(row.tbdw_dwdm);
        $("#edit_xqn").val(row.xqn);
        $("#edit_xqy").val(row.xqy);
        $("#edit_zxqzrq").val(row.zxqzrq);
        $("#edit_gxsj").val(row.gxsj);
        $("#edit_yxx").val(row.yxx);
        $("#edit_gxsjc").val(row.gxsjc);
        $("#edit_xgbsdw").val(row.xgbsdw);
        $("#edit_flwsbh").val(row.flwsbh);
        $("#edit_sfzf").val(row.sfzf);
        $("#edit_sfzt").val(row.sfzt);
        $("#edit_sftsrqlx").val(row.sftsrqlx);
        $("#edit_sfjhgk").val(row.sfjhgk);
        $("#edit_zhsj").val(row.zhsj);
        $("#edit_zhd").val(row.zhd);
        $("#edit_zhd_xxdq").val(row.zhd_xxdq);
        $("#edit_zhd_xxdz").val(row.zhd_xxdz);
        $("#edit_fzqk").val(row.fzqk);
        $("#edit_fzlx").val(row.fzlx);
        $("#edit_czbs").val(row.czbs);
        $("#edit_ryly").val(row.ryly);
        $("#edit_cszt").val(row.cszt);
        $("#edit_gjdm").val(row.gjdm);
        $("#edit_sdryjlbh").val(row.sdryjlbh);
        $("#edit_chdwxc").val(row.chdwxc);
        $("#edit_sftsrq_old").val(row.sftsrq_old);
        $("#edit_sjly").val(row.sjly);
        $("#edit_ly").val(row.ly);

		//模态框的确认(保存)按钮绑定事件
        $("#editConfirmBtn").on("click",function(){
            //curdTbDpajSdryChxx();
            tbDpajSdryChxxEditSubmit();
        });
        $("#editModal").modal();
    },
    "click #delBtn":function (e,vale,row,index) {
        $("#tipsModalLabel").html("<i class='fa fa-warning fa-lg' style='color: #c87f0a'></i>警告");//提示标题
        $("#tipsMessage").text("确认删除么？");//设置提示信息
        $("#tipsConfirmBtn").text("删除");//按钮名称
        $("#tipsConfirmBtn").on("click",function(){
            deleteTbDpajSdryChxx(row.jlbh);
        });
        $("#tipsModal").modal();
    }
}

function addTbDpajSdryChxx() {
    $("#editModalLabel").text("**新增");
    $("#edit_curdType").val("insert");
    
    //请根据需要修改/删除 需展示字段
    //此处设置字段与修改时相同，是为清空点击修改时为元素的赋值
    //注意：字典类型数据需要dm和mc都进行设置
    $("#edit_jlbh").val('');
    $("#edit_ajbh").val('');
    $("#edit_xyrbh").val('');
    $("#edit_tbdw_dwmc").val('');
    $("#edit_djr_xm").val('');
    $("#edit_djrq").val('');
    $("#edit_sdlx").val('');
    $("#edit_chrq").val('');
    $("#edit_ddmc").val('');
    $("#edit_hdqh_xzqh").val('');
    $("#edit_chdw_dwmc").val('');
    $("#edit_czqk").val('');
    $("#edit_chd_xzqh").val('');
    $("#edit_czrq").val('');
    $("#edit_jyaq").val('');
    $("#edit_khyhmc").val('');
    $("#edit_xykh").val('');
    $("#edit_yhzh").val('');
    $("#edit_jdchphm").val('');
    $("#edit_xdjlhcjgdm").val('');
    $("#edit_ztrybh").val('');
    $("#edit_lad_xzqh").val('');
    $("#edit_swzjrq").val('');
    $("#edit_zhd_xzqh").val('');
    $("#edit_tjjbdm").val('');
    $("#edit_zhfsdm").val('');
    $("#edit_xstgfsdm").val('');
    $("#edit_zcxwnrms").val('');
    $("#edit_hjd_xzqh").val('');
    $("#edit_sftp").val('');
    $("#edit_sphzxqk").val('');
    $("#edit_yjd_xzqh").val('');
    $("#edit_jhzazzy").val('');
    $("#edit_sftsrq").val('');
    $("#edit_sfgwyjry").val('');
    $("#edit_dbzh").val('');
    $("#edit_chdwdm").val('');
    $("#edit_tbdw_dwdm").val('');
    $("#edit_xqn").val('');
    $("#edit_xqy").val('');
    $("#edit_zxqzrq").val('');
    $("#edit_gxsj").val('');
    $("#edit_yxx").val('');
    $("#edit_gxsjc").val('');
    $("#edit_xgbsdw").val('');
    $("#edit_flwsbh").val('');
    $("#edit_sfzf").val('');
    $("#edit_sfzt").val('');
    $("#edit_sftsrqlx").val('');
    $("#edit_sfjhgk").val('');
    $("#edit_zhsj").val('');
    $("#edit_zhd").val('');
    $("#edit_zhd_xxdq").val('');
    $("#edit_zhd_xxdz").val('');
    $("#edit_fzqk").val('');
    $("#edit_fzlx").val('');
    $("#edit_czbs").val('');
    $("#edit_ryly").val('');
    $("#edit_cszt").val('');
    $("#edit_gjdm").val('');
    $("#edit_sdryjlbh").val('');
    $("#edit_chdwxc").val('');
    $("#edit_sftsrq_old").val('');
    $("#edit_sjly").val('');
    $("#edit_ly").val('');
    
    $("#editConfirmBtn").on("click",function(){
        //curdTbDpajSdryChxx();
        tbDpajSdryChxxEditSubmit();
    });

    $("#editModal").modal();
}

function deleteTbDpajSdryChxx(id){
    var reqData={"curdType":"delete","jlbh":id};
    $("#tipsModal").modal('toggle');
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/jdzh/tbDpajSdryChxx/curd" ,
        data: reqData,
        success: function (result) {
            alert(result.msg);
            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function curdTbDpajSdryChxx() {
	//防止重复点击
    $("#editConfirmBtn").attr('disabled','disabled');
    var reqData = $('#tbDpajSdryChxxEditForm').serializeJsonObject();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/jdzh/tbDpajSdryChxx/curd" ,
        data: reqData,
        success : function(data) {
            alert(data.msg);
            $("#editModal").modal('toggle');

            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function search() {
    //inittbDpajSdryChxxGrid();
    $("#tbDpajSdryChxxGrid").bootstrapTable("refresh");
}
function exportTbDpajSdryChxx(){
    var form = document.tbDpajSdryChxxQueryForm;
    var url = ctx+"/jdzh/tbDpajSdryChxx/excel";
    form.action=url;
    form.submit();
}

var tbDpajSdryChxxEditSubmit = function(){
    var flag = $('#tbDpajSdryChxxEditForm').validate('submitValidate');
    if (flag){
        curdTbDpajSdryChxx();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}

