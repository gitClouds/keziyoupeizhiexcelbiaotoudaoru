function initJjdYpbgGrid(){
    $('#ypbgGrid').bootstrapTable('destroy');

    $("#ypbgGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/fzzx/ypbg/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [5,10,20,50,100],
        pageSize: 5,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryJjdYpbgParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 250,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
        showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
            {
                title : "文件名",
                field : "filename",
                align : "center"
            },
            {
                title : "生成单位",
                field : "lrdwmc",
                align : "center"
            },
            {
                title : "生成人",
                field : "lrrxm",
                align : "center",
                width : "80px"
            },
            {
                title : "生成时间",
                field : "rksj",
                align : "center",
                width : "120px",
                formatter: function (value, row, index) {
                    return value?new Date(value).Format("yyyy-MM-dd hh:mm:ss"):"";
                }
            },
            {
                title : "下载",
                field : "mongopk",
                align : "center",
                valign : "middle",
                width : "80px",
                formatter : jjdYpbgOperationFormatter
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function queryJjdYpbgParams(params){
    var tmp = $("#jjdYpbgQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    tmp["jjdpk"]=$("#jjdpk").val();
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function jjdYpbgOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' onclick='fileUpload.fileDownLoad(this)' data-key='"+ctx+"/attachment/download?pk="+value+"' class='btn' title='下载'><i class='fa fa-download fa-lg'></i></a>";
    return result;
}

function generatYpbg() {
    var reqData = {"jjdpk":$("#jjdpk").val()};
    $.ajax({
        type: "POST",
        dataType: "json",
        data:reqData,
        url: ctx+"/fzzx/ypbg/generat" ,
        async:false,
        success: function (result) {
            if (result.msg=="true"){
                alert("生成成功！");
                $('#ypbgGrid').bootstrapTable("refresh");
            }else{
                alert(result.msg);
            }
        },
        error : function() {
            alert("请求异常");
        }
    });
}