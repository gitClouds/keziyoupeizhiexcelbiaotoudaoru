
$(function () {
    initJjdGrid();
    formEditValidate('jjdEditForm');
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
})

function initJjdGrid(){
    $('#jjdGrid').bootstrapTable('destroy');

    $("#jjdGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/fzzx/jjd/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryJjdParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
		showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
        //请根据需要修改/删除 需展示字段
            {
                title : "序号",
                field : "Number",
                align : "center",
                width : "50px",
                formatter: function (value, row, index) {
                    var pageSize = $('#jjdGrid').bootstrapTable('getOptions').pageSize;     //通过table的#id 得到每页多少条
                    var pageNumber = $('#jjdGrid').bootstrapTable('getOptions').pageNumber; //通过table的#id 得到当前第几页
                    return pageSize * (pageNumber - 1) + index + 1;    // 返回每条的序号： 每页条数 *（当前页 - 1 ）+ 序号
                }
            },
            {
                title : "接警单号",
                field : "jjdhm",
                align : "center"
            },
            {
                title : "案发时间",
                field : "afsj",
                align : "center",
                formatter: function (value, row, index) {
                    return value?new Date(value).Format("yyyy-MM-dd hh:mm:ss"):"";
                }
            },
            {
                title : "案发地点",
                field : "afdd",
                align : "center"
            },
            {
                title : "报案人",
                field : "barxm",
                align : "center"
            },
            {
                title : "联系电话",
                field : "barlxdh",
                align : "center"
            },
            {
                title : "涉案金额",
                field : "saje",
                align : "center"
            },
            {
                title : "受理单位",
                field : "sldwmc",
                align : "center"
            },
            {
                title : "录入单位",
                field : "lrdwmc",
                align : "center"
            },
            {
                title : "操作",
                field : "pk",
                align : "center",
                valign : "middle",
                width : "150px",
                events:jjdOperateEvents,
                formatter : jjdOperationFormatter
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function queryJjdParams(params){
    var tmp = $("#jjdQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function jjdOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' id='showBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";/*onclick=\"showJjd('" + id + "', view='view')\"*/
    result += "<a href='javascript:void(0);' id='editBtn' class='btn' title='编辑'><span class='fa fa-edit fa-lg'></span></a>";/*onclick=\"editJjd('" + id + "')\"*/
    result += "<a href='javascript:void(0);' id='delBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";/*onclick=\"deleteJjd('" + id + "')\"*/
    if(fxfsPerm && row.accuratetype==1){
        result += "<a href='javascript:void(0);' id='accBtn' class='btn' title='修改分析方式'><span class='fa fa-check-circle-o fa-lg'></span></a>";
    }

    return result;
}
//操作栏绑定事件
window.jjdOperateEvents = {
    "click #showBtn":function (e,vale,row,index) {
    	$("#showModalLabel").text("接警单详情");
        if (shrNum>0){
            for (var i=shrNum;i>0;i--){
                $(".shrData:last").remove();
                shrNum--;
            }
        }

        if (xyrNum>0){
            for (var i=xyrNum;i>0;i--){
                $(".xyrData:last").remove();
                xyrNum--;
            }
        }
        if (shxxNum>0){
            for (var i=shxxNum;i>0;i--){
                $(".shxxData:last").remove();
                shxxNum--;
            }
        }
        jjdShow(row.pk);
        $("#showModal").modal();
    },
    "click #editBtn":function (e,vale,row,index) {
        var item = {'id':'updateJjd','name':'接警单修改','url':ctx+'/fzzx/jjd/jjdForm?curdType=update&pk='+row.pk};
        iframeTab.parentAddIframe(item);
    },
    "click #delBtn":function (e,vale,row,index) {
        $("#tipsModalLabel").html("<i class='fa fa-warning fa-lg' style='color: #c87f0a'></i>警告");//提示标题
        $("#tipsMessage").text("确认删除么？");//设置提示信息
        $("#tipsConfirmBtn").text("删除");//按钮名称
        $("#tipsConfirmBtn").on("click",function(){
            deleteJjd(row.pk);
        });
        $("#tipsModal").modal();
    },
    "click #accBtn":function (e,vale,row,index) {
        $("#tipsModalLabel").html("<i class='fa fa-warning fa-lg' style='color: #c87f0a'></i>警告");//提示标题
        $("#tipsMessage").text("确认修改分析方式么？修改后按照时间进行分析...");//设置提示信息
        $("#tipsConfirmBtn").text("修改");//按钮名称
        $("#tipsConfirmBtn").on("click",function(){
            updateAccuratetype(row.pk);
        });
        $("#tipsModal").modal();
    }
}

function jjdShow(pk) {
    var reqData={"pk":pk};
    $.ajax({
        type: "POST",
        dataType: "json",
        async:false,
        url: ctx+"/fzzx/jjd/jjdShow" ,
        data: reqData,
        success: function (result) {
            showJjdVal(result);
        },
        error : function() {
            alert("请求异常");
        }
    });
}
function showJjdVal(data){
    $("#show_pk").val(data.pk);

    $("#show_jjdhm").val(data.jjdhm);
    $("#show_zfje").val(data.zfje);
    $("#show_sldwmc").val(data.sldwmc);
    $("#show_jdxz").val(data.dicMap['010101'][data.jdxz]);
    $("#show_jjsj").val(data.jjsj?new Date(data.jjsj).Format("yyyy-MM-dd hh:mm:ss"):"");

    $("#show_barxm").val(data.barxm);
    $("#show_barsfzh").val(data.barsfzh);
    $("#show_barlxdh").val(data.barlxdh);
    $("#show_jjfs").val(data.dicMap['010102'][data.jjfs]);

    $("#show_ajlx").val(data.dicMap['010103'][data.ajlx]);
    $("#show_ajlb").val(data.dicMap['010107'][data.ajlb]);
    $("#show_afsj").val(data.afsj?new Date(data.afsj).Format("yyyy-MM-dd hh:mm:ss"):"");
    $("#show_saje").val(data.saje);
    $("#show_afdd").val(data.afdd);
    $("#show_jyaq").text(data.jyaq);
    $("#show_bz").text(data.bz);

    $("#show_sawz").val(data.sawz);

    if (data.shrList && data.shrList.length > 0){
        for (var i=0;i<data.shrList.length;i++){
            if (i==0){
                showJjdShrVal(data.shrList[i],data.dicMap);
            }else{
                addShr();
                showJjdShrVal(data.shrList[i],data.dicMap);
            }
        }
    }
    if (data.xyrList && data.xyrList.length > 0){
        for (var i=0;i<data.xyrList.length;i++){
            if (i==0){
                showJjdXyrVal(data.xyrList[i],data.dicMap);
            }else{
                addXyr();
                showJjdXyrVal(data.xyrList[i],data.dicMap);
            }
        }
    }
    if (data.shxxList && data.shxxList.length > 0){
        for (var i=0;i<data.shxxList.length;i++){
            if (i==0){
                showJjdShxxVal(data.shxxList[i]);
            }else{
                addShxx();
                showJjdShxxVal(data.shxxList[i]);
            }
        }
    }
}
function showJjdShrVal(data,dicMap){
    $("#show_shr_yhk_"+shrNum).val(data.yhk);
    $("#show_shr_ckr_"+shrNum).val(data.ckr);
    $("#show_shr_sfzh_"+shrNum).val(data.sfzh);
    $("#show_shr_zzfs_"+shrNum).val(dicMap['010104'][data.zzfs]);
    $("#show_shr_zzsj_" + shrNum).val(data.zzsj?new Date(data.zzsj).Format("yyyy-MM-dd hh:mm:ss"):"");
    $("#show_shr_zzje_"+shrNum).val(data.zzje);
    $("#show_shr_zfb_"+shrNum).val(data.zfb);
    $("#show_shr_cft_"+shrNum).val(data.cft);
    $("#show_shr_wx_"+shrNum).val(data.wx);
    $("#show_shr_dh_"+shrNum).val(data.dh);
    $("#show_shr_jydh_"+shrNum).val(data.jydh);
    $("#show_shr_qq_"+shrNum).val(data.qq);
}
function showJjdXyrVal(data,dicMap){
    $("#show_xyr_yhk_"+xyrNum).val(data.yhk);
    $("#show_xyr_ckr_"+xyrNum).val(data.ckr);
    $("#show_xyr_sfzh_"+xyrNum).val(data.sfzh);
    $("#show_xyr_zzfs_"+xyrNum).val(dicMap['010104'][data.zzfs]);
    $("#show_xyr_zzsj_"+xyrNum).val(data.zzsj?new Date(data.zzsj).Format("yyyy-MM-dd hh:mm:ss"):"");

    $("#show_xyr_zzje_"+xyrNum).val(data.zzje);
    $("#show_xyr_zfb_"+xyrNum).val(data.zfb);
    $("#show_xyr_cft_"+xyrNum).val(data.cft);
    $("#show_xyr_wx_"+xyrNum).val(data.wx);
    $("#show_xyr_dh_"+xyrNum).val(data.dh);
    $("#show_xyr_jydh_"+xyrNum).val(data.jydh);
    $("#show_xyr_qq_"+xyrNum).val(data.qq);
}
function showJjdShxxVal(data) {
    $("#show_shxx_shmc_"+shxxNum).val(data.shmc);
    $("#show_shxx_lxfs_"+shxxNum).val(data.lxfs);
    $("#show_shxx_shh_"+shxxNum).val(data.shh);
    $("#show_shxx_jydh_"+shxxNum).val(data.jydh);
}

function addJjd() {
    var item = {'id':'addJjd','name':'接警单新增','url':ctx+'/fzzx/jjd/jjdForm?curdType=insert'};
    iframeTab.parentAddIframe(item);
}
function updateAccuratetype(id){
    var reqData={"pk":id};
    $("#tipsModal").modal('toggle');
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/fzzx/jjd/updateAccuratetype" ,
        data: reqData,
        success: function (result) {
            alert(result.msg);
            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}
function deleteJjd(id){
    var reqData={"pk":id};
    $("#tipsModal").modal('toggle');
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/fzzx/jjd/delete" ,
        data: reqData,
        success: function (result) {
            alert(result.msg);
            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function curdJjd() {
	//防止重复点击
    showLoading();
    var reqData = $('#jjdEditForm').serializeJsonObject();
    var curdType = $("#curdType").val();
    reqData = setListData(reqData);
    reqData = JSON.stringify(reqData);
    $.ajax({
        type: "POST",
        dataType: "json",
        contentType: 'application/json; charset=UTF-8',
        url: ctx+"/fzzx/jjd/curd" ,
        data: reqData,
        success : function(data) {
            alert(data.msg);
            hideLoading();
            window.parent.$("#ifTab_container_fzzxJjdPage")[0].contentWindow.search();
            if (curdType=='insert'){
                window.parent.$("#ifTab_close_addJjd").click();
            }
            if(curdType == 'update'){
                window.parent.$("#ifTab_close_updateJjd").click();
            }

        },
        error : function() {
            alert("请求异常");
            hideLoading();
        }
    });
}
function setListData(json) {
    //获取受害人
    json = getListParm("shr",json);
    //获取嫌疑人
    json = getListParm("xyr",json);
    //获取商户信息
    json = getListParm("shxx",json);
    return json;
}

function search() {
    //initjjdGrid();
    $("#jjdGrid").bootstrapTable("refresh");
}
function exportJjd(){
    var form = document.jjdQueryForm;
    var url = ctx+"/fzzx/jjd/excel";
    form.action=url;
    form.submit();
}
var jjdEditSubmit = function(){
    var flag = $('#jjdEditForm').validate('submitValidate');
    if (flag){
        curdJjd();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}

function addShr(){
    var oldBeanNum = $(".shrData:last").attr("data-bean-num");
    $(".shrData:last").after($(".shrData:last").clone());
    shrNum+=1;
    $(".shrData:last").attr("data-bean-num",shrNum);
    //遍历所有input
    $(".shrData:last input").each(function () {
        initAddParm(oldBeanNum,shrNum,this);
    })
    dicInit();
}

function removeShr(obj) {
    if($(".shrData").length>1){
        $(obj).parents(".shrData").remove();
    }else{
        alert('最少保留一个');
    }
}

function addXyr(){
    var oldBeanNum = $(".xyrData:last").attr("data-bean-num");
    $(".xyrData:last").after($(".xyrData:last").clone());
    xyrNum+=1;
    $(".xyrData:last").attr("data-bean-num",xyrNum);
    //遍历所有input
    $(".xyrData:last input").each(function () {
        initAddParm(oldBeanNum,xyrNum,this);
    })
    dicInit();
}

function removeXyr(obj) {
    if($(".xyrData").length>1){
        $(obj).parents(".xyrData").remove();
    }else{
        alert('最少保留一个');
    }
}

function addShxx(){
    var oldBeanNum = $(".shxxData:last").attr("data-bean-num");
    $(".shxxData:last").after($(".shxxData:last").clone());
    shxxNum+=1;
    $(".shxxData:last").attr("data-bean-num",shxxNum);
    //遍历所有input
    $(".shxxData:last input").each(function () {
        initAddParm(oldBeanNum,shxxNum,this);
    })
    dicInit();
}

function removeShxx(obj) {
    if($(".shxxData").length>1){
        $(obj).parents(".shxxData").remove();
    }else{
        alert('最少保留一个');
    }
}

