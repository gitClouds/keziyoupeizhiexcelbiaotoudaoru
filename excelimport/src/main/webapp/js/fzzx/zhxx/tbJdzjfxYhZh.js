
$(function () {
    initTbJdzjfxYhZhGrid();
    formEditValidate('tbJdzjfxYhZhEditForm');//表单验证
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
    $('#editModal').on('hidden.bs.modal', function (event) {
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").removeAttr('disabled');
        $("#editConfirmBtn").removeAttr('disabled','disabled');
        $("#editConfirmBtn").unbind();
        formEditValidateReset('tbJdzjfxYhZhEditForm');
    });
})

function initTbJdzjfxYhZhGrid(){
    $('#tbJdzjfxYhZhGrid').bootstrapTable('destroy');

    $("#tbJdzjfxYhZhGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/zhxx/tbJdzjfxYhZh/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryTbJdzjfxYhZhParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
		showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
        //请根据需要修改/删除 需展示字段
            {
                title : "警情银行卡ID",
                field : "id",
                align : "center"
            },
            {
                title : "银行账号",
                field : "zh",
                align : "center"
            },
            {
                title : "户主姓名",
                field : "xm",
                align : "center"
            },
            {
                title : "户主证件类型",
                field : "zjlx",
                align : "center"
            },
            {
                title : "户主的证件号码",
                field : "zjhm",
                align : "center"
            },
            {
                title : "账号所属银行名称",
                field : "jgmc",
                align : "center"
            },
            {
                title : "账户所属银行代码",
                field : "jgdm",
                align : "center"
            },
            {
                title : "转账时间",
                field : "zzsj",
                align : "center"
            },
            {
                title : "转账金额",
                field : "zzje",
                align : "center"
            },
            {
                title : "录入人",
                field : "lrr",
                align : "center"
            },
            {
                title : "录入时间",
                field : "lrsj",
                align : "center"
            },
            {
                title : "所属关系，1：受害人，2：嫌疑人",
                field : "ssgx",
                align : "center"
            },
            {
                title : "账户类型 01-个人 02：对公",
                field : "zhlb",
                align : "center"
            },
            {
                title : "录入人姓名",
                field : "lrrxm",
                align : "center"
            },
            {
                title : "录入人单位",
                field : "lrdw",
                align : "center"
            },
            {
                title : "录入单位名称",
                field : "lrrdwmc",
                align : "center"
            },
            {
                title : "修改人姓名",
                field : "xgrxm",
                align : "center"
            },
            {
                title : "修改人",
                field : "xgr",
                align : "center"
            },
            {
                title : "修改人单位代码",
                field : "xgrdwdm",
                align : "center"
            },
            {
                title : "修改人单位名称",
                field : "xgrdwmc",
                align : "center"
            },
            {
                title : "修改时间",
                field : "xgsj",
                align : "center"
            },
            {
                title : "有效性",
                field : "yxx",
                align : "center"
            },
            {
                title : "转账方式",
                field : "zzfs",
                align : "center"
            },
            {
                title : "是否已止付 0：未止付；1：已止付",
                field : "sfyzf",
                align : "center"
            },
            {
                title : "是否已冻结 0：未冻结； 1：已冻结",
                field : "sfydj",
                align : "center"
            },
            {
                title : "余额",
                field : "ye",
                align : "center"
            },
            {
                title : "层级",
                field : "cj",
                align : "center"
            },
            {
                title : "是否为延伸卡号 0：否； 1：是",
                field : "sfys",
                align : "center"
            },
            {
                title : "是否资金返还 0：否 1：是",
                field : "zjfh",
                align : "center"
            },
            {
                title : "账号类型：1：银行卡 3：pos机",
                field : "zhlx",
                align : "center"
            },
            {
                title : "返还金额",
                field : "fhje",
                align : "center"
            },
            {
                title : "是否下发",
                field : "sfxf",
                align : "center"
            },
            {
                title : "实际返还金额",
                field : "sfje",
                align : "center"
            },
            {
                title : "资金返还状态 0：未下发 1：已下发 2：已完成",
                field : "zjfhzt",
                align : "center"
            },
            {
                title : "字段注释",
                field : "yhksqbh",
                align : "center"
            },
            {
                title : "字段注释",
                field : "kszfbs",
                align : "center"
            },
            {
                title : "字段注释",
                field : "jjbh",
                align : "center"
            },
            {
                title : "操作",
                field : "id",
                align : "center",
                valign : "middle",
                width : "150px",
                events:tbJdzjfxYhZhOperateEvents,
                formatter : tbJdzjfxYhZhOperationFormatter
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function queryTbJdzjfxYhZhParams(params){
    var tmp = $("#tbJdzjfxYhZhQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function tbJdzjfxYhZhOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' id='showBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";/*onclick=\"showTbJdzjfxYhZh('" + id + "', view='view')\"*/
    result += "<a href='javascript:void(0);' id='editBtn' class='btn' title='编辑'><span class='fa fa-edit fa-lg'></span></a>";/*onclick=\"editTbJdzjfxYhZh('" + id + "')\"*/
    result += "<a href='javascript:void(0);' id='delBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";/*onclick=\"deleteTbJdzjfxYhZh('" + id + "')\"*/

    return result;
}
//操作栏绑定事件
window.tbJdzjfxYhZhOperateEvents = {
    "click #showBtn":function (e,vale,row,index) {
    	$("#showModalLabel").text("**详情");
    	//请根据需要修改/删除 需展示字段
        $("#show_id").text(row.id);
        $("#show_zh").text(row.zh);
        $("#show_xm").text(row.xm);
        $("#show_zjlx").text(row.zjlx);
        $("#show_zjhm").text(row.zjhm);
        $("#show_jgmc").text(row.jgmc);
        $("#show_jgdm").text(row.jgdm);
        $("#show_zzsj").text(row.zzsj);
        $("#show_zzje").text(row.zzje);
        $("#show_lrr").text(row.lrr);
        $("#show_lrsj").text(row.lrsj);
        $("#show_ssgx").text(row.ssgx);
        $("#show_zhlb").text(row.zhlb);
        $("#show_lrrxm").text(row.lrrxm);
        $("#show_lrdw").text(row.lrdw);
        $("#show_lrrdwmc").text(row.lrrdwmc);
        $("#show_xgrxm").text(row.xgrxm);
        $("#show_xgr").text(row.xgr);
        $("#show_xgrdwdm").text(row.xgrdwdm);
        $("#show_xgrdwmc").text(row.xgrdwmc);
        $("#show_xgsj").text(row.xgsj);
        $("#show_yxx").text(row.yxx);
        $("#show_zzfs").text(row.zzfs);
        $("#show_sfyzf").text(row.sfyzf);
        $("#show_sfydj").text(row.sfydj);
        $("#show_ye").text(row.ye);
        $("#show_cj").text(row.cj);
        $("#show_sfys").text(row.sfys);
        $("#show_zjfh").text(row.zjfh);
        $("#show_zhlx").text(row.zhlx);
        $("#show_fhje").text(row.fhje);
        $("#show_sfxf").text(row.sfxf);
        $("#show_sfje").text(row.sfje);
        $("#show_zjfhzt").text(row.zjfhzt);
        $("#show_yhksqbh").text(row.yhksqbh);
        $("#show_kszfbs").text(row.kszfbs);
        $("#show_jjbh").text(row.jjbh);

        $("#showModal").modal();
    },
    "click #editBtn":function (e,vale,row,index) {
    	//设置模态框标题
        $("#editModalLabel").text("**修改");
        //设置操作类型
        $("#edit_curdType").val("update");
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").attr('disabled','disabled');
        
        //请根据需要修改/删除 需展示字段
        //注意：字典类型数据需要dm和mc都进行设置
        $("#edit_id").val(row.id);
        $("#edit_zh").val(row.zh);
        $("#edit_xm").val(row.xm);
        $("#edit_zjlx").val(row.zjlx);
        $("#edit_zjhm").val(row.zjhm);
        $("#edit_jgmc").val(row.jgmc);
        $("#edit_jgdm").val(row.jgdm);
        $("#edit_zzsj").val(row.zzsj);
        $("#edit_zzje").val(row.zzje);
        $("#edit_lrr").val(row.lrr);
        $("#edit_lrsj").val(row.lrsj);
        $("#edit_ssgx").val(row.ssgx);
        $("#edit_zhlb").val(row.zhlb);
        $("#edit_lrrxm").val(row.lrrxm);
        $("#edit_lrdw").val(row.lrdw);
        $("#edit_lrrdwmc").val(row.lrrdwmc);
        $("#edit_xgrxm").val(row.xgrxm);
        $("#edit_xgr").val(row.xgr);
        $("#edit_xgrdwdm").val(row.xgrdwdm);
        $("#edit_xgrdwmc").val(row.xgrdwmc);
        $("#edit_xgsj").val(row.xgsj);
        $("#edit_yxx").val(row.yxx);
        $("#edit_zzfs").val(row.zzfs);
        $("#edit_sfyzf").val(row.sfyzf);
        $("#edit_sfydj").val(row.sfydj);
        $("#edit_ye").val(row.ye);
        $("#edit_cj").val(row.cj);
        $("#edit_sfys").val(row.sfys);
        $("#edit_zjfh").val(row.zjfh);
        $("#edit_zhlx").val(row.zhlx);
        $("#edit_fhje").val(row.fhje);
        $("#edit_sfxf").val(row.sfxf);
        $("#edit_sfje").val(row.sfje);
        $("#edit_zjfhzt").val(row.zjfhzt);
        $("#edit_yhksqbh").val(row.yhksqbh);
        $("#edit_kszfbs").val(row.kszfbs);
        $("#edit_jjbh").val(row.jjbh);

		//模态框的确认(保存)按钮绑定事件
        $("#editConfirmBtn").on("click",function(){
            //curdTbJdzjfxYhZh();
            tbJdzjfxYhZhEditSubmit();
        });
        $("#editModal").modal();
    },
    "click #delBtn":function (e,vale,row,index) {
        $("#tipsModalLabel").html("<i class='fa fa-warning fa-lg' style='color: #c87f0a'></i>警告");//提示标题
        $("#tipsMessage").text("确认删除么？");//设置提示信息
        $("#tipsConfirmBtn").text("删除");//按钮名称
        $("#tipsConfirmBtn").on("click",function(){
            deleteTbJdzjfxYhZh(row.id);
        });
        $("#tipsModal").modal();
    }
}

function addTbJdzjfxYhZh() {
    $("#editModalLabel").text("**新增");
    $("#edit_curdType").val("insert");
    
    //请根据需要修改/删除 需展示字段
    //此处设置字段与修改时相同，是为清空点击修改时为元素的赋值
    //注意：字典类型数据需要dm和mc都进行设置
    $("#edit_id").val('');
    $("#edit_zh").val('');
    $("#edit_xm").val('');
    $("#edit_zjlx").val('');
    $("#edit_zjhm").val('');
    $("#edit_jgmc").val('');
    $("#edit_jgdm").val('');
    $("#edit_zzsj").val('');
    $("#edit_zzje").val('');
    $("#edit_lrr").val('');
    $("#edit_lrsj").val('');
    $("#edit_ssgx").val('');
    $("#edit_zhlb").val('');
    $("#edit_lrrxm").val('');
    $("#edit_lrdw").val('');
    $("#edit_lrrdwmc").val('');
    $("#edit_xgrxm").val('');
    $("#edit_xgr").val('');
    $("#edit_xgrdwdm").val('');
    $("#edit_xgrdwmc").val('');
    $("#edit_xgsj").val('');
    $("#edit_yxx").val('');
    $("#edit_zzfs").val('');
    $("#edit_sfyzf").val('');
    $("#edit_sfydj").val('');
    $("#edit_ye").val('');
    $("#edit_cj").val('');
    $("#edit_sfys").val('');
    $("#edit_zjfh").val('');
    $("#edit_zhlx").val('');
    $("#edit_fhje").val('');
    $("#edit_sfxf").val('');
    $("#edit_sfje").val('');
    $("#edit_zjfhzt").val('');
    $("#edit_yhksqbh").val('');
    $("#edit_kszfbs").val('');
    $("#edit_jjbh").val('');
    
    $("#editConfirmBtn").on("click",function(){
        //curdTbJdzjfxYhZh();
        tbJdzjfxYhZhEditSubmit();
    });

    $("#editModal").modal();
}

function deleteTbJdzjfxYhZh(id){
    var reqData={"curdType":"delete","id":id};
    $("#tipsModal").modal('toggle');
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/zhxx/tbJdzjfxYhZh/curd" ,
        data: reqData,
        success: function (result) {
            alert(result.msg);
            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function curdTbJdzjfxYhZh() {
	//防止重复点击
    $("#editConfirmBtn").attr('disabled','disabled');
    var reqData = $('#tbJdzjfxYhZhEditForm').serializeJsonObject();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/zhxx/tbJdzjfxYhZh/curd" ,
        data: reqData,
        success : function(data) {
            alert(data.msg);
            $("#editModal").modal('toggle');

            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function search() {
    //inittbJdzjfxYhZhGrid();
    $("#tbJdzjfxYhZhGrid").bootstrapTable("refresh");
}
function exportTbJdzjfxYhZh(){
    var form = document.tbJdzjfxYhZhQueryForm;
    var url = ctx+"/zhxx/tbJdzjfxYhZh/excel";
    form.action=url;
    form.submit();
}

var tbJdzjfxYhZhEditSubmit = function(){
    var flag = $('#tbJdzjfxYhZhEditForm').validate('submitValidate');
    if (flag){
        curdTbJdzjfxYhZh();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}

