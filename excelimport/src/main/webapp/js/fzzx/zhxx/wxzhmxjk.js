$(function () {
    initTbJdzjfxSfZhGrid();
    formEditValidate('tbJdzjfxSfZhEditForm');//表单验证
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
    $('#editModal').on('hidden.bs.modal', function (event) {
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").removeAttr('disabled');
        $("#editConfirmBtn").removeAttr('disabled', 'disabled');
        $("#editConfirmBtn").unbind();
        formEditValidateReset('tbJdzjfxSfZhEditForm');
    });
})

function initTbJdzjfxSfZhGrid() {
    $('#tbJdzjfxSfZhGrid').bootstrapTable('destroy');

    $("#tbJdzjfxSfZhGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url: ctx + '/zhxx/tbJdzjfxSfZh/list?sjly=1',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10, 20, 50, 100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryTbJdzjfxSfZhParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign: "left",
        showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns: [
            //请根据需要修改/删除 需展示字段
//            {
//                field: 'checked',
//                checkbox: true,
//                align: 'center',
//                valign: 'middle',
//                formatter: function (value, row, index) {//设置满足条件的行可以使用复选框
//                    if (row.fxbs != 1) {
//                        return {
//                            disabled: true
//                        }
//                    }
//                }
//            },
            {
                title: "序号",
                field: "Number",
                align: "center",
                width: "50px",
                formatter: function (value, row, index) {
                    var pageSize = $('#tbJdzjfxSfZhGrid').bootstrapTable('getOptions').pageSize;     //通过table的#id 得到每页多少条
                    var pageNumber = $('#tbJdzjfxSfZhGrid').bootstrapTable('getOptions').pageNumber; //通过table的#id 得到当前第几页
                    return pageSize * (pageNumber - 1) + index + 1;    // 返回每条的序号： 每页条数 *（当前页 - 1 ）+ 序号
                }
            },
            {
                title: "账号",
                field: "zh",
                align: "center"
            },
            {
                title: "身份证号",
                field: "zjhm",
                align: "center"
            },
//            {
//                title: "户主姓名",
//                field: "xm",
//                align: "center"
//            },
            {
                title: "账号所属机构名称",
                field: "jgmc",
                align: "center"
            },
            /*{
                title: "转账时间",
                field: "zzsj",
                align: "center",
                formatter: function (value, row, index) {
                    return value ? new Date(value).Format("yyyy-MM-dd hh:mm:ss") : "";
                }
            },
            {
                title: "转账金额",
                field: "zzje",
                align: "center"
            },*/
            {
                title: "短信发送电话",
                field: "lxfs",
                align: "center"
            },
            {
            	title: "备注",
            	field: "jjbh",
            	align: "center"
            },
            {
                title: "录入人姓名",
                field: "lrrxm",
                align: "center"
            },
            {
                title: "录入时间",
                field: "lrsj",
                align: "center",
                formatter: function (value, row, index) {
                    return value ? new Date(value).Format("yyyy-MM-dd hh:mm:ss") : "";
                }
            },
            {
            	title: "查询标志",
            	field: "www",
            	align: "center",
            	formatter: function (value, row, index) {
            		if(row.cxkg == '1'){
            			return "查询进行中";
            		}else if(row.cxkg == '2'){
            			return "查询已停止";
            		}else if(row.cxkg == '0'){
            			return "查询未启动";
            		}
            	}
            },
            {
                title: "操作",
                field: "id",
                align: "center",
                valign: "middle",
                width: "100px",
                events: tbJdzjfxSfZhOperateEvents,
                formatter: tbJdzjfxSfZhOperationFormatter
            }
        ],
        responseHandler: responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function queryTbJdzjfxSfZhParams(params) {
    var tmp = $("#tbJdzjfxSfZhQueryForm").serializeJsonObject();
    tmp["pageSize"] = params.limit;
    tmp["pageNumber"] = params.offset / params.limit + 1;
    return tmp;
}

function responseHandler(result) {
    //如果没有错误则返回数据，渲染表格
    return {
        total: result.total, //总页数,前面的key必须为"total"
        rows: result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}

//操作栏的格式化
function tbJdzjfxSfZhOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    if (row.fxbs == '1') {
    	if (row.lrr == username || row.lrr == 'thunis'){
    		if (row.cxkg=='0') {
    			result += "<a href='javascript:void(0);' id='fxBtn' class='btn' title='明细查询启动'><i class='glyphicon glyphicon-play-circle'></i></a>";
    			result += "<a href='javascript:void(0);' id='editBtn' class='btn' title='编辑'><span class='fa fa-edit fa-lg'></span></a>";/*onclick=\"editTbJdzjfxSfZh('" + id + "')\"*/
    			result += "<a href='javascript:void(0);' id='delBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";/*onclick=\"deleteTbJdzjfxSfZh('" + id + "')\"*/
    		}
    	}
    }else{
    	if (row.lrr == username || row.lrr == 'thunis'){
    		if(row.cxkg=='1'){
    			result += "<a href='javascript:void(0);' id='tzcxBtn' class='btn' title='停止查询'><i class='glyphicon glyphicon-remove'></i></a>";
    		}else if(row.cxkg=='2'){
    			result += "<a href='javascript:void(0);' id='jxcxBtn' class='btn' title='继续查询'><i class='fa fa-check-circle-o fa-lg'></i></a>";
    		}
    	}
        result += "<a href='javascript:void(0);' id='showBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";/*onclick=\"showTbJdzjfxSfZh('" + id + "', view='view')\"*/
    }
    return result;
}

function plFx() {
    var ids = "";//得到用户选择的数据的ID
    var rows = $("#tbJdzjfxSfZhGrid").bootstrapTable('getSelections');
    for (var i = 0; i < rows.length; i++) {
        ids += rows[i].id + ',';
    }
    ids = ids.substring(0, ids.length - 1);
    if (ids == '') {
        alert("请选择数据!");
        return;
    }

    var lx = $("#lx").val();
    var url = ctx + '/znzf/base/form?fwType=sfMx&xxType=xyr&jjdPk=' + ids;
    var item = {'id': "addsfMx", 'name': "三方请求单", 'url': url};
    if (lx == 2) {
        url = ctx + '/znzf/base/form?fwType=yhkMx&xxType=xyr&jjdPk=' + ids;
        item = {'id': "addyhkMx", 'name': "银行请求单", 'url': url};
    }
    iframeTab.parentAddIframe(item);
}

//操作栏绑定事件
window.tbJdzjfxSfZhOperateEvents = {
    "click #showBtn": function (e, vale, row, index) {
    	var item = {
            'id': 'screenData',
            'name': '微信账号明细数据',
            'url': ctx + '/zhxx/tbJdzjfxSfZh/wxzhmxjkDetailPage?id=' + row.id
        };
        iframeTab.parentAddIframe(item);
    },
    "click #editBtn": function (e, vale, row, index) {
        //设置模态框标题
        $("#editModalLabel").text("微信账号信息修改");
        //设置操作类型
        $("#edit_curdType").val("update");
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").attr('disabled','disabled');

        //请根据需要修改/删除 需展示字段
        //注意：字典类型数据需要dm和mc都进行设置
        $("#edit_id").val(row.id);
        $("#edit_zh").val(row.zh);
        $("#edit_xm").val(row.xm);
        $("#zjlx").val(row.zjlx);
        $("#edit_zjhm").val(row.zjhm);
        $("#jgmc").val(row.jgmc);
        $("#jgdm").val(row.jgdm);
        $("#zzsj").val(row.zzsj);
        $("#edit_zzje").val(row.zzje);
        $("#edit_lrr").val(row.lrr);
        $("#edit_lrsj").val(row.lrsj);
        $("#edit_ssgx").val(row.ssgx);
        $("#edit_zhlb").val(row.zhlb);
        if (row.zhlb){
            $("#edit_zhlb").find("option:contains(row.zhlb)").attr("selected",true);
        }
        $("#edit_lrrxm").val(row.lrrxm);
        $("#edit_lrdw").val(row.lrdw);
        $("#edit_lrrdwmc").val(row.lrrdwmc);
        $("#edit_xgrxm").val(row.xgrxm);
        $("#edit_xgr").val(row.xgr);
        $("#edit_xgrdwdm").val(row.xgrdwdm);
        $("#edit_xgrdwmc").val(row.xgrdwmc);
        $("#edit_xgsj").val(row.xgsj);
        $("#edit_yxx").val(row.yxx);
        $("#edit_zzfs").val(row.zzfs);
        $("#edit_sfyzf").val(row.sfyzf);
        $("#edit_sfydj").val(row.sfydj);
        $("#edit_ye").val(row.ye);
        $("#edit_cj").val(row.cj);
        $("#edit_sfys").val(row.sfys);
        $("#edit_zjfh").val(row.zjfh);
        $("#edit_zhlx").val(row.zhlx);
        $("#edit_fhje").val(row.fhje);
        $("#edit_sfxf").val(row.sfxf);
        $("#edit_sfje").val(row.sfje);
        $("#edit_zjfhzt").val(row.zjfhzt);
        $("#edit_yhksqbh").val(row.yhksqbh);
        $("#edit_kszfbs").val(row.kszfbs);
        $("#edit_jjbh").val(row.jjbh);
        $("#edit_lxfs").val(row.lxfs);
        if (row.rylx){
            $("#edit_rylx").find("option:contains(row.rylx)").attr("selected",true);
        }
        //模态框的确认(保存)按钮绑定事件
        $("#editConfirmBtn").on("click", function () {
            //curdTbJdzjfxSfZh();
            tbJdzjfxSfZhEditSubmit();
        });
        $("#editModal").modal();
    },
    "click #delBtn": function (e, vale, row, index) {
        $("#tipsModalLabel").html("<i class='fa fa-warning fa-lg' style='color: #c87f0a'></i>警告");//提示标题
        $("#tipsMessage").text("确认删除么？");//设置提示信息
        $("#tipsConfirmBtn").text("删除");//按钮名称
        $("#tipsConfirmBtn").on("click", function () {
            deleteTbJdzjfxSfZh(row.id);
        });
        $("#tipsModal").modal();
    },
    "click #fxBtn": function (e, vale, row, index) {
    	 $.ajax({
    	        type: "POST",
    	        dataType: "json",
    	        data:{"id":row.id},
    	        url: ctx+"/zhxx/tbJdzjfxSfZh/mxfx",
    	        beforeSend: function () {
                    loading("明细查询启动中，请稍后......");
                },
    	        success: function (data) {
    	        	top.layer.close(indexLoading);
    	            if(data.success){
    	            	top.layer.alert("明细查询启动成功！", {icon: 1, title: '提示'});
    	            	search();
    	            }else{
        				top.layer.alert("停止查询操作失败：请求异常", {icon: 2, title: '提示'});
        			}
    	        },
                error : function() {
                	top.layer.alert("明细查询启动失败：请求异常", {icon: 2, title: '提示'});
                }
    	    });
    },
    "click #tzcxBtn": function (e, vale, row, index) {
    	$.ajax({
    		type: "POST",
    		dataType: "json",
    		data:{"id":row.id,"cxkg":"2"},
    		url: ctx+"/zhxx/tbJdzjfxSfZh/tzjxcx",
    		beforeSend: function () {
    			loading("停止查询操作中，请稍后......");
    		},
    		success: function (data) {
    			top.layer.close(indexLoading);
    			if(data.success){
    				top.layer.alert("停止查询操作成功！", {icon: 1, title: '提示'});
    				search();
    			}else{
    				top.layer.alert("停止查询操作失败：请求异常", {icon: 2, title: '提示'});
    			}
    		},
    		error : function() {
    			top.layer.alert("停止查询操作失败：请求异常", {icon: 2, title: '提示'});
    		}
    	});
    },
    "click #jxcxBtn": function (e, vale, row, index) {
    	$.ajax({
    		type: "POST",
    		dataType: "json",
    		data:{"id":row.id,"cxkg":"1"},
    		url: ctx+"/zhxx/tbJdzjfxSfZh/tzjxcx",
    		beforeSend: function () {
    			loading("继续查询操作中，请稍后......");
    		},
    		success: function (data) {
    			top.layer.close(indexLoading);
    			if(data.success){
    				top.layer.alert("继续查询操作成功！", {icon: 1, title: '提示'});
    				search();
    			}else{
    				top.layer.alert("停止查询操作失败：请求异常", {icon: 2, title: '提示'});
    			}
    		},
    		error : function() {
    			top.layer.alert("继续查询操作失败：请求异常", {icon: 2, title: '提示'});
    		}
    	});
    },
    "click #hdBtn": function (e, vale, row, index) {
        var url = ctx + '//hd/tbHdInfo/openHdJg?zh_id=' + row.id;
        var item = {'id': "ifTab_seed_listHdDetailInfo", 'name': "话单明细信息", 'url': url};
        iframeTab.parentAddIframe(item);
    }
}

function loading(msg) {
	indexLoading = top.layer.msg(msg, {
        icon: 16,
        shade: [0.1, '#666666'],
        time: false  //不自动关闭
    })
}

function addTbJdzjfxSfZh() {
    $("#editModalLabel").text("微信账号信息新增");
    $("#edit_curdType").val("insert");
    //请根据需要修改/删除 需展示字段
    //此处设置字段与修改时相同，是为清空点击修改时为元素的赋值
    //注意：字典类型数据需要dm和mc都进行设置
    $("#edit_id").val('');
    $("#edit_zh").val('');
    $("#edit_xm").val('');
    $("#zjlx").val('');
    $("#edit_zjhm").val('');
    $("#jgdm").val('');
    $("#jgmc").val('');
    $("#zzsj").val('');
    $("#edit_zzje").val('');
    $("#edit_lrr").val('');
    $("#edit_lrsj").val('');
    $("#edit_ssgx").val('');
    $("#edit_zhlb").val('');
    $("#edit_lrrxm").val('');
    $("#edit_lrdw").val('');
    $("#edit_lrrdwmc").val('');
    $("#edit_xgrxm").val('');
    $("#edit_xgr").val('');
    $("#edit_xgrdwdm").val('');
    $("#edit_xgrdwmc").val('');
    $("#edit_xgsj").val('');
    $("#edit_yxx").val('');
    $("#edit_zzfs").val('');
    $("#edit_sfyzf").val('');
    $("#edit_sfydj").val('');
    $("#edit_ye").val('');
    $("#edit_cj").val('');
    $("#edit_sfys").val('');
    $("#edit_zjfh").val('');
    $("#edit_zhlx").val('');
    $("#edit_fhje").val('');
    $("#edit_sfxf").val('');
    $("#edit_sfje").val('');
    $("#edit_zjfhzt").val('');
    $("#edit_yhksqbh").val('');
    $("#edit_kszfbs").val('');
    $("#edit_jjbh").val('');
    $("#edit_lxfs").val('');
    
   
    $("#editConfirmBtn").on("click", function () {
        tbJdzjfxSfZhEditSubmit();
    });

    $("#editModal").modal();
}

function deleteTbJdzjfxSfZh(id) {
    var reqData = {"id": id};
    $("#tipsModal").modal('toggle');
    $.ajax({
        type: "POST",
        dataType: "json",
        contentType: 'application/json; charset=UTF-8',
        url: ctx + "/zhxx/tbJdzjfxSfZh/curd?curdType=delete&lx=1",
        data: JSON.stringify(reqData),
        success: function (result) {
            alert(result.msg);
            search();
        },
        error: function () {
            alert("请求异常");
        }
    });
}

function curdTbJdzjfxSfZh() {
    //防止重复点击
    $("#editConfirmBtn").attr('disabled', 'disabled');
    var reqData = $('#tbJdzjfxSfZhEditForm').serializeJsonObject();
    reqData = JSON.stringify(reqData);
    var curdType = $("#edit_curdType").val();
    var lx = "1";
    $.ajax({
        type: "POST",
        dataType: "json",
        contentType: 'application/json; charset=UTF-8',
        url: ctx + "/zhxx/tbJdzjfxSfZh/curd?curdType=" + curdType + "&lx=" + lx,
        data: reqData,
        success: function (data) {
            alert(data.msg);
            $("#editModal").modal('toggle');
            search();
        },
        error: function () {
            alert("请求异常");
        }
    });
}

function search() {
    $("#tbJdzjfxSfZhGrid").bootstrapTable("refresh");
}

function exportTbJdzjfxSfZh() {
    var form = document.tbJdzjfxSfZhQueryForm;
    var url = ctx + "/zhxx/tbJdzjfxSfZh/excel";
    form.action = url;
    form.submit();
}

var tbJdzjfxSfZhEditSubmit = function () {
    var flag = $('#tbJdzjfxSfZhEditForm').validate('submitValidate');
    if (flag) {
    	var zh=$("#edit_zh").val();
    	var zjhm=$("#edit_zjhm").val();
    	if(zh == '' && zjhm == ''){
    		alert("微信账号和身份证号必须填一个！");
    		return;
    	}
    	var flag=true;
    	var lxfs=$("#edit_lxfs").val().replace('，',',');
    	var arr=lxfs.split(",");
    	if(/^[\,]+$/.test(arr)){ 
    		flag=false;
    	}
    	$.each(arr,function(i,v){
    		if(v != '' && !(/^1[3456789]\d{9}$/.test(v))){ 
        		flag=false;
        	}
    	})
    	$("#edit_lxfs").val(lxfs);
    	if(!flag){
    		alert("短信发送电话号码格式不对！");
    		return;
    	}
        curdTbJdzjfxSfZh();
    } else {
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}

function importTbJdzjfxSfZh() {
    $("#fileUploadModalLabel").text("数据上传");
    $(".progress-bar-success").attr('style', 'width:0%;');
    /*$("#edit_jjdpk").val($("#jjdpk").val());
    $("#edit_datatype").val($("#datatype").val());*/
    $("#dataFile").val('');

    $("#fileUploadConfirmBtn").on("click", function () {
        addSjsc();
    });

    $("#fileUploadModal").modal();
}

function persent() {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: ctx + "/zhxx/tbJdzjfxSfZh/percent",
        success: function (data) {
            if (data != 0) {
                $("#process").show();
                $(".progress-bar-success").attr('style', 'width:' + data + '%;');
            }
        },
        error: function () {
            console.info("进度数据返回异常")
        }
    });
}


function addSjsc() {
    var timePercent = setInterval('persent()', 100);
    //防止重复点击
    // $("#fileUploadConfirmBtn").attr('disabled', 'disabled');
    //var reqData = $('#znzfSjscEditForm').serializeJsonObject();
    var lx = $("#imp_lx").val();
    var formData = new FormData();
    var dataFile = $('#dataFile').get(0).files[0];
    formData.append("file", dataFile);


    $.ajax({
        type: "POST",
        //dataType: "json",
        contentType: false,
        processData: false,
        cache: false,
        url: ctx + "/zhxx/tbJdzjfxSfZh/upload?lx=" + lx,
        enctype: 'multipart/form-data',
        data: formData,
        success: function (data) {
            window.clearInterval(timePercent);
            alert(data.msg);
            $("#fileUploadModal").modal('toggle');

            search();
        },
        error: function () {
            alert("请求异常");
        }
    });
}

function exportTbJdzjfxSfZhTemplate() {
    var a = document.createElement('a'); // 创建a标签
    a.setAttribute('download', '');// download属性
    a.setAttribute('href', ctx + '/resources/common/template/SfzfCardModel.xlsx');// href链接
    a.click();// 自执行点击事件
    a.remove();

}

function exportTbJdzjfxYhZhTemplate() {
    var a = document.createElement('a'); // 创建a标签
    a.setAttribute('download', '');// download属性
    a.setAttribute('href', ctx + '/resources/common/template/BankCardModel.xlsx');// href链接
    a.click();// 自执行点击事件
    a.remove();

}

$(function () {
    $("#add_i").hover(function () {
        $("#add").text("新增");

    }, function () {
        $("#add").text("");
    });

    $("#exp_i").hover(function () {
        $("#exp").text("导出");

    }, function () {
        $("#exp").text("");
    });

    $("#imp_i").hover(function () {
        $("#imp").text("导入");

    }, function () {
        $("#imp").text("");
    });
    $("#plfx_i").hover(function () {
        $("#plfx").text("批量分析");

    }, function () {
        $("#plfx").text("");
    });

    $("#template_i").hover(function () {
        $("#templageDownload").text("三方模板");

    }, function () {
        $("#templageDownload").text("");
    });

    $("#templateyh_i").hover(function () {
        $("#templageYhDownload").text("银行模板");

    }, function () {
        $("#templageYhDownload").text("");
    });

    $("#tabs1 button").click(function () {
        $("#tabs1 button").eq($(this).index()).addClass("btn-info").siblings().removeClass('btn-info');
        var lx = $(this).attr("value");
        if (lx == "1") {
            $("#template_i").show();
            $("#templateyh_i").hide();
            $("#jgmc").attr("data-jzd-code", "542");
        }
        if (lx == "2") {
            $("#template_i").hide();
            $("#templateyh_i").show();
            $("#jgmc").attr("data-jzd-code", "5390");
        }
        $("#lx").val(lx);
        $("#edit_lx").val(lx);
        $("#imp_lx").val(lx);
        search();
    });

})

function valiZh(){
	var zh=$("#edit_zh").val();
	var jgdm=$("input[name='jgdm']").val();
	vali('zh',zh,jgdm);
}
function valiZjhm(){
	var zjhm=$("#edit_zjhm").val();
	var jgdm=$("input[name='jgdm']").val();
	vali('zjhm',zjhm,jgdm);

}
function valiZhZjhm(){
	var zh=$("#edit_zh").val();
	if(zh != ''){
		var jgdm=$("input[name='jgdm']").val();
		vali('zh',zh,jgdm);
	}
	
	var zjhm=$("#edit_zjhm").val();
	if(zjhm != ''){
		var jgdm=$("input[name='jgdm']").val();
		vali('zjhm',zjhm,jgdm);
	}
}

function vali(type,val,jgdm){
	$.ajax({
        type: "GET",
        dataType: "json",
        url: ctx + "/zhxx/tbJdzjfxSfZh/valiZhZjhm?type="+type+"&val="+val+"&jgdm="+jgdm,
        success: function (data) {
            if (!data.success) {
               alert(data.msg);
               if(type == 'zh'){
            	   $("#edit_zh").val('');
               }else if(type == 'zjhm'){
            	   $("#edit_zjhm").val('');
               }
            }
        },
        error: function () {
        	alert("请求异常");
        }
    });
}




