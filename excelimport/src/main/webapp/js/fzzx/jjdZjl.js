/**
 * Created by Administrator on 2019/2/12/012.
 */
function checkXxType(){
    var xxType = $('input:radio[name="xxType"]:checked').val();
    if(xxType==null){
        alert("请选择受害人/嫌疑人!");
        return false;
    }else{
        return xxType;
    }
}

/*function addYhkMx() {
    var xxType = checkXxType();
    if(xxType){
        //var url = ctx+'/znzf/yhkMx/form?xxType='+xxType+'&jjdPk='+$("#show_pk").val();
        var url = ctx+'/znzf/base/form?xxType='+xxType+'&jjdPk='+$("#show_pk").val()+'&fwType=yhkMx';
        var item = {'id':'addYhkMx','name':'银行卡明细请求','url':url};
        iframeTab.parentAddIframe(item);
    }
}*/

function addBase(fwType,name) {
    var xxType = checkXxType();
    var nlevel = "";
    if(xxType=="shr"){
        nlevel = 0;
    }else if(xxType=="xyr"){
        nlevel = 1;
    }
    if(xxType){
        //var url = ctx+'/znzf/yhkMx/form?xxType='+xxType+'&jjdPk='+$("#show_pk").val();show_ajlb
        var url = ctx+'/znzf/base/form?xxType='+xxType+'&jjdPk='+$("#show_pk").val()+'&fwType='+fwType+"&nlevel="+nlevel+"&iszcygzs="+$("#iszcygzs").val();
        var item = {'id':'add'+fwType,'name':name,'url':url};
        if($("#iszcygzs").val()>=1){
            iframeTab.parentTwoAddIframe(item);
        }else{
            iframeTab.parentAddIframe(item);
        }
    }
}

function addTypeBase(idName,type,name) {
    var xxType = checkXxType();
    var nlevel = "";
    if(xxType=="shr"){
        nlevel = 0;
    }else if(xxType=="xyr"){
        nlevel = 1;
    }
    if(xxType){
        //var url = ctx+'/znzf/yhkMx/form?xxType='+xxType+'&jjdPk='+$("#show_pk").val();show_ajlb
        var url = ctx+'/znzf/base/form?xxType='+xxType+'&jjdPk='+$("#show_pk").val()+'&fwType='+idName+'_'+type+"&nlevel="+nlevel+"&iszcygzs="+$("#iszcygzs").val();
        var item = {'id':'add'+idName,'name':name,'url':url};
        if($("#iszcygzs").val()>=1){
            iframeTab.parentTwoAddIframe(item);
        }else{
            iframeTab.parentAddIframe(item);
        }
    }
}

function setUpNlevel(tableId,pk,cardnumber) {
    //editUser(row);
    $("#editModalLabel").text("等级设置");

    $("#jltPk").val(pk);
    $("#jlt_cardnumber").val(cardnumber);

    $("#editConfirmBtn").on("click",function(){
        setUpNlevelEditSubmit(tableId);//
    });
    $("#editModal").modal();
}

function setUpNlevelEditSubmit(tableId) {
    var flag = $('#setUpNlevelEditForm').validate('submitValidate');
    if (flag){
        showLoading();
        var reqData = $('#setUpNlevelEditForm').serializeJsonObject();
        $.ajax({
            type: "POST",
            dataType: "json",
            data:reqData,
            url: ctx+"/znzf/jlt/setUpNlevel" ,
            async:false,
            success: function (result) {
                hideLoading();
                if (result.msg=="true"){
                    alert("设置成功");
                    window.parent.$("#zcxx_zjl").click();
                    window.parent.$("#zcygzs_zjfx").click();
                    //$('#'+tableId).bootstrapTable("refresh");
                }else{
                    alert(result.msg);
                }
                $("#editModal").modal('toggle');
            },
            error : function() {
                hideLoading();
                alert("请求异常");
            }
        });
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}