
$(function () {
    initAjxxGrid();
    formEditValidate('ajxxEditForm');//表单验证
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
    $('#editModal').on('hidden.bs.modal', function (event) {
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").removeAttr('disabled');
        $("#editConfirmBtn").removeAttr('disabled','disabled');
        $("#editConfirmBtn").unbind();
        formEditValidateReset('ajxxEditForm');
    });
})

function initAjxxGrid(){
    $('#ajxxGrid').bootstrapTable('destroy');

    $("#ajxxGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/fzzx/ajxx/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryAjxxParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
		showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
        //请根据需要修改/删除 需展示字段
            {
                title : "序号",
                field : "Number",
                align : "center",
                width : "50px",
                formatter: function (value, row, index) {
                    var pageSize = $('#ajxxGrid').bootstrapTable('getOptions').pageSize;     //通过table的#id 得到每页多少条
                    var pageNumber = $('#ajxxGrid').bootstrapTable('getOptions').pageNumber; //通过table的#id 得到当前第几页
                    return pageSize * (pageNumber - 1) + index + 1;    // 返回每条的序号： 每页条数 *（当前页 - 1 ）+ 序号
                }
            },
            {
                title : "接警单号",
                field : "jjdhm",
                align : "center"
            },
            {
                title : "案件编号",
                field : "ajbh",
                align : "center"
            },
           /* {
                title : "案件ID（预留字段）",
                field : "ajid",
                align : "center"
            },
            {
                title : "止付金额",
                field : "zfje",
                align : "center"
            },
            {
                title : "警单性质",
                field : "jdxz",
                align : "center"
            },*/
            {
                title : "受理单位",
                field : "sldw",
                align : "center"
            },
           /* {
                title : "受理单位名称",
                field : "sldwmc",
                align : "center"
            },
            {
                title : "接警时间",
                field : "jjsj",
                align : "center"
            },
            {
                title : "报案人姓名",
                field : "barxm",
                align : "center"
            },
            {
                title : "报案人身份证",
                field : "barsfzh",
                align : "center"
            },
            {
                title : "报案人联系电话",
                field : "barlxdh",
                align : "center"
            },
            {
                title : "接警方式",
                field : "jjfs",
                align : "center"
            },
            {
                title : "案件类型",
                field : "ajlx",
                align : "center"
            },*/
            {
                title : "案件类别",
                field : "ajlb",
                align : "center"
            },
            {
                title : "案发时间",
                field : "afsj",
                align : "center"
            },
            {
                title : "涉案金额",
                field : "saje",
                align : "center"
            },
            /*{
                title : "案发地点",
                field : "afdd",
                align : "center"
            },*/
            {
                title : "简要案情",
                field : "jyaq",
                align : "center"
            },
            /*{
                title : "涉案网址",
                field : "sawz",
                align : "center"
            },
            {
                title : "备注",
                field : "bz",
                align : "center"
            },
            {
                title : "状态",
                field : "zt",
                align : "center"
            },*/
            {
                title : "录入时间",
                field : "cjsj",
                align : "center"
            },
            /*{
                title : "修改时间",
                field : "xgsj",
                align : "center"
            },
            {
                title : "区域",
                field : "area",
                align : "center"
            },
            {
                title : "录入单位代码",
                field : "lrdwdm",
                align : "center"
            },
            {
                title : "录入单位",
                field : "lrdwmc",
                align : "center"
            },
            {
                title : "录入人姓名",
                field : "lrrxm",
                align : "center"
            },
            {
                title : "录入人警号",
                field : "lrrjh",
                align : "center"
            },*/
            {
                title : "立案单位代码",
                field : "ladwdm",
                align : "center"
            },
            {
                title : "立案单位名称",
                field : "ladwmc",
                align : "center"
            },
            {
                title : "立案时间",
                field : "lasj",
                align : "center"
            },
            /*{
                title : "联系人（立案）",
                field : "lxr",
                align : "center"
            },
            {
                title : "联系电话（立案）",
                field : "lxdh",
                align : "center"
            },
            {
                title : "联系人手机号码",
                field : "lxrsj",
                align : "center"
            },
            {
                title : " 案件类别子类",
                field : "ajlbzl",
                align : "center"
            },*/
            {
                title : "操作",
                field : "pk",
                align : "center",
                valign : "middle",
                width : "150px",
                events:ajxxOperateEvents,
                formatter : ajxxOperationFormatter
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function queryAjxxParams(params){
    var tmp = $("#ajxxQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function ajxxOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' id='showBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";/*onclick=\"showAjxx('" + id + "', view='view')\"*/
    result += "<a href='javascript:void(0);' id='editBtn' class='btn' title='编辑'><span class='fa fa-edit fa-lg'></span></a>";/*onclick=\"editAjxx('" + id + "')\"*/
    result += "<a href='javascript:void(0);' id='delBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";/*onclick=\"deleteAjxx('" + id + "')\"*/

    return result;
}
//操作栏绑定事件
window.ajxxOperateEvents = {
    "click #showBtn":function (e,vale,row,index) {
    	$("#showModalLabel").text("**详情");
    	//请根据需要修改/删除 需展示字段
        $("#show_pk").text(row.pk);
        $("#show_jjdhm").text(row.jjdhm);
        $("#show_ajbh").text(row.ajbh);
        $("#show_ajid").text(row.ajid);
        $("#show_zfje").text(row.zfje);
        $("#show_jdxz").text(row.jdxz);
        $("#show_sldw").text(row.sldw);
        $("#show_sldwmc").text(row.sldwmc);
        $("#show_jjsj").text(row.jjsj);
        $("#show_barxm").text(row.barxm);
        $("#show_barsfzh").text(row.barsfzh);
        $("#show_barlxdh").text(row.barlxdh);
        $("#show_jjfs").text(row.jjfs);
        $("#show_ajlx").text(row.ajlx);
        $("#show_ajlb").text(row.ajlb);
        $("#show_afsj").text(row.afsj);
        $("#show_saje").text(row.saje);
        $("#show_afdd").text(row.afdd);
        $("#show_jyaq").text(row.jyaq);
        $("#show_sawz").text(row.sawz);
        $("#show_bz").text(row.bz);
        $("#show_zt").text(row.zt);
        $("#show_cjsj").text(row.cjsj);
        $("#show_xgsj").text(row.xgsj);
        $("#show_area").text(row.area);
        $("#show_lrdwdm").text(row.lrdwdm);
        $("#show_lrdwmc").text(row.lrdwmc);
        $("#show_lrrxm").text(row.lrrxm);
        $("#show_lrrjh").text(row.lrrjh);
        $("#show_ladwdm").text(row.ladwdm);
        $("#show_ladwmc").text(row.ladwmc);
        $("#show_lasj").text(row.lasj);
        $("#show_lxr").text(row.lxr);
        $("#show_lxdh").text(row.lxdh);
        $("#show_lxrsj").text(row.lxrsj);
        $("#show_ajlbzl").text(row.ajlbzl);

        $("#showModal").modal();
    },
    "click #editBtn":function (e,vale,row,index) {
    	//设置模态框标题
        $("#editModalLabel").text("**修改");
        //设置操作类型
        $("#edit_curdType").val("update");
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").attr('disabled','disabled');
        
        //请根据需要修改/删除 需展示字段
        //注意：字典类型数据需要dm和mc都进行设置
        $("#edit_pk").val(row.pk);
        $("#edit_jjdhm").val(row.jjdhm);
        $("#edit_ajbh").val(row.ajbh);
        $("#edit_ajid").val(row.ajid);
        $("#edit_zfje").val(row.zfje);
        $("#edit_jdxz").val(row.jdxz);
        $("#edit_sldw").val(row.sldw);
        $("#edit_sldwmc").val(row.sldwmc);
        $("#edit_jjsj").val(row.jjsj);
        $("#edit_barxm").val(row.barxm);
        $("#edit_barsfzh").val(row.barsfzh);
        $("#edit_barlxdh").val(row.barlxdh);
        $("#edit_jjfs").val(row.jjfs);
        $("#edit_ajlx").val(row.ajlx);
        $("#edit_ajlb").val(row.ajlb);
        $("#edit_afsj").val(row.afsj);
        $("#edit_saje").val(row.saje);
        $("#edit_afdd").val(row.afdd);
        $("#edit_jyaq").val(row.jyaq);
        $("#edit_sawz").val(row.sawz);
        $("#edit_bz").val(row.bz);
        $("#edit_zt").val(row.zt);
        $("#edit_cjsj").val(row.cjsj);
        $("#edit_xgsj").val(row.xgsj);
        $("#edit_area").val(row.area);
        $("#edit_lrdwdm").val(row.lrdwdm);
        $("#edit_lrdwmc").val(row.lrdwmc);
        $("#edit_lrrxm").val(row.lrrxm);
        $("#edit_lrrjh").val(row.lrrjh);
        $("#edit_ladwdm").val(row.ladwdm);
        $("#edit_ladwmc").val(row.ladwmc);
        $("#edit_lasj").val(row.lasj);
        $("#edit_lxr").val(row.lxr);
        $("#edit_lxdh").val(row.lxdh);
        $("#edit_lxrsj").val(row.lxrsj);
        $("#edit_ajlbzl").val(row.ajlbzl);

		//模态框的确认(保存)按钮绑定事件
        $("#editConfirmBtn").on("click",function(){
            //curdAjxx();
            ajxxEditSubmit();
        });
        $("#editModal").modal();
    },
    "click #delBtn":function (e,vale,row,index) {
        $("#tipsModalLabel").html("<i class='fa fa-warning fa-lg' style='color: #c87f0a'></i>警告");//提示标题
        $("#tipsMessage").text("确认删除么？");//设置提示信息
        $("#tipsConfirmBtn").text("删除");//按钮名称
        $("#tipsConfirmBtn").on("click",function(){
            deleteAjxx(row.pk);
        });
        $("#tipsModal").modal();
    }
}
function addAjxx() {
    var item = {'id':'addAjxx','name':'案件新增','url':ctx+'/fzzx/ajxx/ajxxForm?curdType=insert'};
    iframeTab.parentAddIframe(item);
}
/*function addAjxx() {
    $("#editModalLabel").text("**新增");
    $("#edit_curdType").val("insert");
    
    //请根据需要修改/删除 需展示字段
    //此处设置字段与修改时相同，是为清空点击修改时为元素的赋值
    //注意：字典类型数据需要dm和mc都进行设置
    $("#edit_pk").val('');
    $("#edit_jjdhm").val('');
    $("#edit_ajbh").val('');
    $("#edit_ajid").val('');
    $("#edit_zfje").val('');
    $("#edit_jdxz").val('');
    $("#edit_sldw").val('');
    $("#edit_sldwmc").val('');
    $("#edit_jjsj").val('');
    $("#edit_barxm").val('');
    $("#edit_barsfzh").val('');
    $("#edit_barlxdh").val('');
    $("#edit_jjfs").val('');
    $("#edit_ajlx").val('');
    $("#edit_ajlb").val('');
    $("#edit_afsj").val('');
    $("#edit_saje").val('');
    $("#edit_afdd").val('');
    $("#edit_jyaq").val('');
    $("#edit_sawz").val('');
    $("#edit_bz").val('');
    $("#edit_zt").val('');
    $("#edit_cjsj").val('');
    $("#edit_xgsj").val('');
    $("#edit_area").val('');
    $("#edit_lrdwdm").val('');
    $("#edit_lrdwmc").val('');
    $("#edit_lrrxm").val('');
    $("#edit_lrrjh").val('');
    $("#edit_ladwdm").val('');
    $("#edit_ladwmc").val('');
    $("#edit_lasj").val('');
    $("#edit_lxr").val('');
    $("#edit_lxdh").val('');
    $("#edit_lxrsj").val('');
    $("#edit_ajlbzl").val('');
    
    $("#editConfirmBtn").on("click",function(){
        //curdAjxx();
        ajxxEditSubmit();
    });

    $("#editModal").modal();
}*/

function deleteAjxx(id){
    var reqData={"curdType":"delete","pk":id};
    $("#tipsModal").modal('toggle');
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/fzzx/ajxx/curd" ,
        data: reqData,
        success: function (result) {
            alert(result.msg);
            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function curdAjxx() {
	//防止重复点击
    $("#editConfirmBtn").attr('disabled','disabled');
    var reqData = $('#ajxxEditForm').serializeJsonObject();
    var curdType = $("#curdType").val();

    reqData = JSON.stringify(reqData);
    $.ajax({
        type: "POST",
        dataType: "json",
        contentType: 'application/json; charset=UTF-8',
        url: ctx+"/fzzx/ajxx/curd" ,
        data: reqData,
        success : function(data) {
            alert(data.msg);
            window.parent.$("#ifTab_container_fzzxAjxxPage")[0].contentWindow.search();
            if (curdType=='insert'){
                window.parent.$("#ifTab_close_addAjxx").click();
            }
            /*$("#editModal").modal('toggle');
            search();*/
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function search() {
    //initajxxGrid();
    $("#ajxxGrid").bootstrapTable("refresh");
}
function exportAjxx(){
    var form = document.ajxxQueryForm;
    var url = ctx+"/fzzx/ajxx/excel";
    form.action=url;
    form.submit();
}

var ajxxEditSubmit = function(){
    var flag = $('#ajxxEditForm').validate('submitValidate');
    if (flag){
        curdAjxx();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}

