
$(function () {
    initLxmdGrid();
    formEditValidate('lxmdEditForm');//表单验证
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
    $('#editModal').on('hidden.bs.modal', function (event) {
        $("#editConfirmBtn").unbind();
        formEditValidateReset('lxmdEditForm');
    });
})

function initLxmdGrid(){
    $('#lxmdGrid').bootstrapTable('destroy');

    $("#lxmdGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/tool/lxmd/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryLxmdParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
		showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
        //请根据需要修改/删除 需展示字段
            {
                title : "机构号",
                field : "jgh",
                align : "center"
            },
            {
                title : "地区码",
                field : "dqm",
                align : "center"
            },
            {
                title : "机构名称",
                field : "dwmc",
                align : "center"
            },
            {
                title : "联系人",
                field : "lxr",
                align : "center"
            },
            {
                title : "固话",
                field : "telephone",
                align : "center"
            },
            {
                title : "手机",
                field : "phone",
                align : "center"
            },
            {
                title : "邮箱",
                field : "email",
                align : "center"
            },
            {
                title : "性质",
                field : "xz",
                align : "center",
                visible: false
            },
            {
                title : "录入时间",
                field : "rksj",
                align : "center",
                formatter:function (value, row, index) {
                    return value?new Date(value).Format("yyyy-MM-dd hh:mm:ss"):"-";
                },
                visible: false
            },
            {
                title : "调证手续",
                field : "dzsx",
                align : "center",
                visible: false
            },
            {
                title : "操作",
                field : "pk",
                align : "center",
                valign : "middle",
                width : "150px",
                events:lxmdOperateEvents,
                formatter : lxmdOperationFormatter
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function queryLxmdParams(params){
    var tmp = $("#lxmdQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function lxmdOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' id='showBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";/*onclick=\"showLxmd('" + id + "', view='view')\"*/
    result += "<a href='javascript:void(0);' id='editBtn' class='btn' title='编辑'><span class='fa fa-edit fa-lg'></span></a>";/*onclick=\"editLxmd('" + id + "')\"*/
    result += "<a href='javascript:void(0);' id='delBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";/*onclick=\"deleteLxmd('" + id + "')\"*/

    return result;
}
//操作栏绑定事件
window.lxmdOperateEvents = {
    "click #showBtn":function (e,vale,row,index) {
    	$("#showModalLabel").text("联系名单详情");
    	//请根据需要修改/删除 需展示字段
        $("#show_jgh").text(row.jgh);
        $("#show_dqm").text(row.dqm);
        $("#show_dwmc").text(row.dwmc);
        $("#show_dzsx").text(row.dzsx);
        $("#show_lxr").text(row.lxr);
        $("#show_telephone").text(row.telephone);
        $("#show_phone").text(row.phone);
        $("#show_email").text(row.email);
        $("#show_xz").text(row.xz);
        $("#show_rksj").text(row.rksj?new Date(row.rksj).Format("yyyy-MM-dd hh:mm:ss"):"");
        $("#showModal").modal();
    },
    "click #editBtn":function (e,vale,row,index) {
    	//设置模态框标题
        $("#editModalLabel").text("联系名单修改");
        //设置操作类型
        $("#edit_curdType").val("update");
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").attr('disabled','disabled');
        
        //请根据需要修改/删除 需展示字段
        //注意：字典类型数据需要dm和mc都进行设置
        $("#edit_pk").val(row.pk);
        $("#edit_jtype").val(row.jtype);
        $("#edit_jgh").val(row.jgh);
        $("#edit_dqm").val(row.dqm);
        $("#edit_dwmc").val(row.dwmc);
        $("#edit_dzsx").val(row.dzsx);
        $("#edit_lxr").val(row.lxr);
        $("#edit_telephone").val(row.telephone);
        $("#edit_phone").val(row.phone);
        $("#edit_email").val(row.email);
        $("#edit_xz").val(row.xz);

		//模态框的确认(保存)按钮绑定事件
        $("#editConfirmBtn").on("click",function(){
            //curdLxmd();
            lxmdEditSubmit();
        });
        $("#editModal").modal();
    },
    "click #delBtn":function (e,vale,row,index) {
        $("#tipsModalLabel").html("<i class='fa fa-warning fa-lg' style='color: #c87f0a'></i>警告");//提示标题
        $("#tipsMessage").text("确认删除么？");//设置提示信息
        $("#tipsConfirmBtn").text("删除");//按钮名称
        $("#tipsConfirmBtn").on("click",function(){
            deleteLxmd(row.pk);
        });
        $("#tipsModal").modal();
    }
}

function addLxmd() {
    $("#editModalLabel").text("联系名单新增");
    $("#edit_curdType").val("insert");
    
    //请根据需要修改/删除 需展示字段
    //此处设置字段与修改时相同，是为清空点击修改时为元素的赋值
    //注意：字典类型数据需要dm和mc都进行设置
    $("#edit_pk").val('');
    $("#edit_jtype").val('');
    $("#edit_jgh").val('');
    $("#edit_dqm").val('');
    $("#edit_dwmc").val('');
    $("#edit_dzsx").val('');
    $("#edit_lxr").val('');
    $("#edit_telephone").val('');
    $("#edit_phone").val('');
    $("#edit_email").val('');
    $("#edit_xz").val('');

    $("#editConfirmBtn").on("click",function(){
        //curdLxmd();
        lxmdEditSubmit();
    });

    $("#editModal").modal();
}

function deleteLxmd(id){
    var reqData={"curdType":"delete","pk":id};
    $("#tipsModal").modal('toggle');
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/tool/lxmd/curd" ,
        data: reqData,
        success: function (result) {
            alert(result.msg);
            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function curdLxmd() {
	//防止重复点击
    showLoading();
    var reqData = $('#lxmdEditForm').serializeJsonObject();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/tool/lxmd/curd" ,
        data: reqData,
        success : function(data) {
            hideLoading();
            alert(data.msg);
            $("#editModal").modal('toggle');
            search();
        },
        error : function() {
            hideLoading();
            alert("请求异常");
        }
    });
}

function search() {
    //initlxmdGrid();
    $("#lxmdGrid").bootstrapTable("refresh");
}
function exportLxmd(){
    var form = document.lxmdQueryForm;
    var url = ctx+"/tool/lxmd/excel";
    form.action=url;
    form.submit();
}

var lxmdEditSubmit = function(){
    var flag = $('#lxmdEditForm').validate('submitValidate');
    if (flag){
        curdLxmd();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}

