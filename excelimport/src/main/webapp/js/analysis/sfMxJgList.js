$(function () {
    initSfMxJgListGrid();
})

function initSfMxJgListGrid() {
    $('#sfMxJgListGrid').bootstrapTable('destroy');

    $("#sfMxJgListGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url: urlSf,//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10, 20, 50, 100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: querySfMxJgListParams,//参数
        showColumns: false,//是否显示所有的列（选择显示的列）
        showRefresh: false,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbarSf',              //工具按钮用哪个容器
        toolbarAlign: "left",
        showToggle: false,                //是否显示详细视图和列表视图的切换按钮
        rowStyle: function (row, index) {
            if (row.sjly == '1' && row.disab == 1) {
                return {css: {"color": "red", "cursor": "not-allowed", "background-color": "antiquewhite"}};
            }
            if (row.disab == 1) {
                return {css: {"color": "red", "cursor": "not-allowed"}};
            }
            if (row.sjly == '1') {
                return {css: {"background-color": "antiquewhite"}};
            }

            return {};
        },
        //得到查询的参数
        columns: [
            //请根据需要修改/删除 需展示字段
            {
                title: "序号",
                field: "Number",
                align: "center",
                width: "50px",
                formatter: function (value, row, index) {
                    var pageSize = $('#sfMxJgListGrid').bootstrapTable('getOptions').pageSize;     //通过table的#id 得到每页多少条
                    var pageNumber = $('#sfMxJgListGrid').bootstrapTable('getOptions').pageNumber; //通过table的#id 得到当前第几页
                    return pageSize * (pageNumber - 1) + index + 1;    // 返回每条的序号： 每页条数 *（当前页 - 1 ）+ 序号
                }
            },
            {
                title: "是否涉毒人员",
                field: "isxdsd",
                align: "center",
                events: isXdSdRyEvents,
                formatter: isXdSdRyFormatter
            },
            {
                title: "主体详情",
                field: "ztxq",
                align: "center",
                events: yhAndSfShowOperateEvents,
                formatter: yhAndSfOperationFormatter
            },
//            {
//                title : "主键",
//                field : "pk",
//                align : "center"
//            },
//            {
//                title : "接警单",
//                field : "jjdpk",
//                align : "center"
//            },
//            {
//                title : "业务申请编号",
//                field : "applicationid",
//                align : "center"
//            },
//            {
//                title : "支付订单号(支付机构内部)",
//                field : "zfddh",
//                align : "center"
//            },
//            {
//                title : "交易类型",
//                field : "jylx",
//                align : "center",
//                formatter: function (value, row, index) {
//                	if(value == "1"){
//                		return "支付账户充值";
//                	}else if(value == "2"){
//                		return "支付账户对支付账户转账";
//                	}else if(value == "3"){
//                		return "支付账户提现/转账至银行卡";
//                	}else if(value == "4"){
//                		return "支付账户消费";
//                	}else if(value == "5"){
//                		return "代收";
//                	}else if(value == "6"){
//                		return "代付";
//                	}else if(value == "9"){
//                		return "其他";
//                	}else{
//                		return "";
//                	}
//                }
//            },
//            {
//                title : "支付类型",
//                field : "zflx",
//                align : "center"
//            },
//            {
//                title : "出入账标识",
//                field : "jdbz",
//                align : "center",
//                formatter: function (value, row, index) {
//                	if(value == "0"){
//                		return "出账";
//                	}else if(value == "1"){
//                		return "入账";
//                	}else{
//                		return "";
//                	}
//                }
//            },
            {
                title: "交易时间",
                field: "jysj",
                align: "center"
            },
//            {
//                title : "币种(CNY或RMB人民币、USD美元、EUR欧元…)",
//                field : "rmbzl",
//                align : "center"
//            },
            {
                title: "交易金额",
                field: "jyje",
                align: "center"
            },
//            {
//                title : "交易流水号(支付机构内部流水号)",
//                field : "jylsh",
//                align : "center"
//            },
//            {
//                title : "交易余额",
//                field : "ye",
//                align : "center"
//            },
//            {
//                title : "收款方银行卡所属银行机构编码",
//                field : "skyhjgdm",
//                align : "center"
//            },
//            {
//                title : "收款方银行卡所属银行名称",
//                field : "skyhjgmc",
//                align : "center"
//            },
            {
                title: "收款方银行卡所属银行卡号",
                field: "skyhzh",
                align: "center"
            },
            {
                title: "收款方的支付帐号",
                field: "skzfzh",
                align: "center"
            },
//            {
//                title : "消费POS机编号",
//                field : "possbh",
//                align : "center"
//            },
//            {
//                title : "收款方的商户号",
//                field : "skshm",
//                align : "center"
//            },
//            {
//                title : "付款方银行卡所属银行机构编码(<OnlinePayCompanyType>=02,必填)",
//                field : "fkyhjgdm",
//                align : "center"
//            },
//            {
//                title : "付款方银行卡所属银行名称",
//                field : "fkyhjgmc",
//                align : "center"
//            },
            {
                title: "付款方银行卡所属银行卡号",
                field: "fkyhzh",
                align: "center"
            },
            {
                title: "是否标记",
                field: "PK",
                align: "center",
                valign: "middle",
                width: "100px",
                formatter: function (value, row, index) {
                    var result = '<a href="javascript:void(0);" class="btn-sm btn-info" onclick="disabRow(\'sfMxJgListGrid\',\'' + row.pk + '\');">';
                    if (row.disab == 1) {
                        result += "取消标记";
                    } else {
                        result += "标记";
                    }
                    result += "</a>";
                    return result;
                }
            },
//            {
//                title : "付款方的支付帐号",
//                field : "fkzfzh",
//                align : "center"
//            },
//            {
//                title : "交易设备类型(1-电脑;2-手机;3-其他)",
//                field : "jysblx",
//                align : "center"
//            },
//            {
//                title : "交易支付设备IP",
//                field : "zfsbip",
//                align : "center"
//            },
//            {
//                title : "MAC地址",
//                field : "macdz",
//                align : "center"
//            },
//            {
//                title : "交易地点经度(WGS84坐标系，单位度)",
//                field : "jyddjd",
//                align : "center"
//            },
//            {
//                title : "交易地点纬度(WGS84坐标系，单位度)",
//                field : "jyddwd",
//                align : "center"
//            },
//            {
//                title : "交易设备号",
//                field : "jysbh",
//                align : "center"
//            },
//            {
//                title : "银行外部渠道交易流水号",
//                field : "yhwblsh",
//                align : "center"
//            },
//            {
//                title : "备注",
//                field : "bz",
//                align : "center"
//            },
//            {
//                title : "入库时间",
//                field : "rksj",
//                align : "center"
//            },
//            {
//                title : "有效性",
//                field : "yxx",
//                align : "center"
//            },
//            {
//                title : "IP归属地",
//                field : "ipxz",
//                align : "center"
//            },
//            {
//                title : "收款方商户名称",
//                field : "skfshmc",
//                align : "center"
//            },
//            {
//                title : "字段注释",
//                field : "qqpk",
//                align : "center"
//            },
//            {
//                title : "操作",
//                field : "pk",
//                align : "center",
//                valign : "middle",
//                width : "150px",
//            }
        ],
        responseHandler: responseHandler//请求数据成功后，渲染表格前的方法
    });
}


// 以起始页码方式传入参数,params为table组装参数
function querySfMxJgListParams(params) {
    var tmp = $("#mxQueryForm").serializeJsonObject();
    tmp["pageSize"] = params.limit;
    tmp["pageNumber"] = params.offset / params.limit + 1;
    return tmp;
}

function responseHandler(result) {
    //如果没有错误则返回数据，渲染表格
    return {
        total: result.total, //总页数,前面的key必须为"total"
        rows: result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}

function exportSfMxJgList() {
    var form = document.mxQueryForm;
    var url = ctx + "/analysis/manulAnalysis/sfMxJgList/excel";
    form.action = url;
    form.submit();
}


