
$(function () {
    initYhkMxJgListGrid();
})

function initYhkMxJgListGrid(){
    $('#yhkMxJgListGrid').bootstrapTable('destroy');

    $("#yhkMxJgListGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:urlYhk,//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryYhkMxJgListParams,//参数
        showColumns: false,//是否显示所有的列（选择显示的列）
        showRefresh: false,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbarYhk',              //工具按钮用哪个容器
        toolbarAlign:"left",
		showToggle: false,     //是否显示详细视图和列表视图的切换按钮
		rowStyle:function (row, index) {
            if (row.disab==1) {
                return {css:{"color":"red","cursor":"not-allowed"}};
            }
            return {};
        },
        //得到查询的参数
        columns :[
        //请根据需要修改/删除 需展示字段
        	{
                title: "序号",
                field: "Number",
                align: "center",
                width: "50px",
                formatter: function (value, row, index) {
                    var pageSize = $('#yhkMxJgListGrid').bootstrapTable('getOptions').pageSize;     //通过table的#id 得到每页多少条
                    var pageNumber = $('#yhkMxJgListGrid').bootstrapTable('getOptions').pageNumber; //通过table的#id 得到当前第几页
                    return pageSize * (pageNumber - 1) + index + 1;    // 返回每条的序号： 每页条数 *（当前页 - 1 ）+ 序号
                }
            },
            {
                title : "是否禁毒人员",
                field : "isxdsd",
                align : "center",
                events:isXdSdRyEvents,
                formatter : isXdSdRyFormatter
            },
            {
            	title : "主体详情",
            	field : "ztxq",
            	align : "center",
            	events:yhAndSfShowOperateEvents,
                formatter : yhAndSfOperationFormatter
            },
//            {
//                title : "主键",
//                field : "pk",
//                align : "center"
//            },
//            {
//                title : "接警单",
//                field : "jjdpk",
//                align : "center"
//            },
//            {
//                title : "业务申请编号",
//                field : "applicationid",
//                align : "center"
//            },
//            {
//                title : "交易类型",
//                field : "jylx",
//                align : "center"
//            },
//            {
//                title : "出入账标识",
//                field : "jdbz",
//                align : "center",
//                formatter: function (value, row, index) {
//                	if(value == "0"){
//                		return "出账";
//                	}else if(value == "1"){
//                		return "入账";
//                	}else{
//                		return "";
//                	}
//                }
//            },
            {
                title : "交易金额",
                field : "jyje",
                align : "center"
            },
//            {
//                title : "交易余额",
//                field : "jyye",
//                align : "center"
//            },
            {
                title : "交易时间",
                field : "jysj",
                align : "center",
                formatter: function (value, row, index) {
                    return value ? new Date(value).Format("yyyy-MM-dd hh:mm:ss") : "";
                }
            },
//            {
//                title : "交易流水号",
//                field : "jylsh",
//                align : "center"
//            },
//            {
//                title : "交易对方名称",
//                field : "jydfmc",
//                align : "center"
//            },
            {
                title : "交易对方账卡号",
                field : "jydfzh",
                align : "center"
            },
            {
                title : "是否标记",
                field : "PK",
                align : "center",
                valign : "middle",
                width : "100px",
                formatter : function(value, row, index) {
                    var result = '<a href="javascript:void(0);" class="btn-sm btn-info" onclick="disabRow(\'yhkMxJgListGrid\',\''+row.pk+'\');">';
                    if (row.disab==1){
                        result+="取消标记";
                    }else{
                        result+="标记";
                    }
                    result+="</a>";
                    return result;
                }
            },
//            {
//                title : "交易对方证件号码",
//                field : "jydfzjhm",
//                align : "center"
//            },
//            {
//                title : "交易对方账号开户行",
//                field : "jydfkhh",
//                align : "center"
//            },
//            {
//                title : "交易是否成功(00-成功;01-失败)",
//                field : "jysfcg",
//                align : "center"
//            },
//            {
//                title : "现金标志(00-其它;01-现金交易)",
//                field : "xjbz",
//                align : "center"
//            },
//            {
//                title : "交易网点名称",
//                field : "jywdmc",
//                align : "center"
//            },
//            {
//                title : "交易网点代码",
//                field : "jywddm",
//                align : "center"
//            },
//            {
//                title : "交易摘要",
//                field : "jyzy",
//                align : "center"
//            },
//            {
//                title : "IP地址",
//                field : "ipdz",
//                align : "center"
//            },
//            {
//                title : "MAC地址",
//                field : "macdz",
//                align : "center"
//            },
//            {
//                title : "日志号",
//                field : "rzh",
//                align : "center"
//            },
//            {
//                title : "传票号",
//                field : "cph",
//                align : "center"
//            },
//            {
//                title : "凭证号",
//                field : "pzh",
//                align : "center"
//            },
//            {
//                title : "终端号",
//                field : "zdh",
//                align : "center"
//            },
//            {
//                title : "交易柜员号",
//                field : "jygyh",
//                align : "center"
//            },
//            {
//                title : "商户名称",
//                field : "shmc",
//                align : "center"
//            },
//            {
//                title : "商户号",
//                field : "shh",
//                align : "center"
//            },
//            {
//                title : "币种(CNY人民币、USD美元、EUR欧元…)",
//                field : "rmbzl",
//                align : "center"
//            },
//            {
//                title : "备注(网关交易时需要反馈外部流水号)",
//                field : "bz",
//                align : "center"
//            },
//            {
//                title : "凭证种类",
//                field : "pzzl",
//                align : "center"
//            },
//            {
//                title : "入库时间",
//                field : "rksj",
//                align : "center"
//            },
//            {
//                title : "有效性",
//                field : "yxx",
//                align : "center"
//            },
//            {
//                title : "ip归属地",
//                field : "ipxz",
//                align : "center"
//            },
//            {
//                title : "字段注释",
//                field : "qqpk",
//                align : "center"
//            },
//            {
//                title : "操作",
//                field : "pk",
//                align : "center",
//                valign : "middle",
//                width : "150px",
//            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}


// 以起始页码方式传入参数,params为table组装参数
function queryYhkMxJgListParams(params){
    var tmp = $("#mxQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}

function exportYhkMxJgList(){
    var form = document.mxQueryForm;
    var url = ctx+"/analysis/manulAnalysis/yhkMxJgList/excel";
    form.action=url;
    form.submit();
}
