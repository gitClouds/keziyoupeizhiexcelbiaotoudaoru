$(function () {
    initManulAnalysisGrid();
    formEditValidate('manulAnalysisEditForm');//表单验证
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
    $('#editModal').on('hidden.bs.modal', function (event) {
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").removeAttr('disabled');
        $("#editConfirmBtn").removeAttr('disabled', 'disabled');
        $("#editConfirmBtn").unbind();
        formEditValidateReset('manulAnalysisEditForm');
    });
})

function initManulAnalysisGrid() {
    $('#manulAnalysisGrid').bootstrapTable('destroy');

    $("#manulAnalysisGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url: ctx + '/analysis/manulAnalysis/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10, 20, 50, 100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryManulAnalysisParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign: "left",
        showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns: [
            //请根据需要修改/删除 需展示字段
//            {
//                title : "字段注释",
//                field : "pk",
//                align : "center"
//            },
            {
                title: "创建时间",
                field: "create_date",
                align: "center",
                formatter: function (value, row, index) {
                    return value ? new Date(value).Format("yyyy-MM-dd hh:mm:ss") : "";
                }
            },
//            {
//                title : "创建人",
//                field : "createor_id",
//                align : "center"
//            },
            {
                title: "创建人",
                field: "createor_name",
                align: "center"
            },
//            {
//                title : "筛选条件",
//                field : "conditions",
//                align : "center"
//            },
            {
                title: "操作",
                field: "pk",
                align: "center",
                valign: "middle",
                width: "150px",
                events: manulAnalysisOperateEvents,
                formatter: manulAnalysisOperationFormatter
            }
        ],
        responseHandler: responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function queryManulAnalysisParams(params) {
    var tmp = $("#manulAnalysisQueryForm").serializeJsonObject();
    tmp["pageSize"] = params.limit;
    tmp["pageNumber"] = params.offset / params.limit + 1;
    return tmp;
}

function responseHandler(result) {
    //如果没有错误则返回数据，渲染表格
    return {
        total: result.total, //总页数,前面的key必须为"total"
        rows: result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}

//操作栏的格式化
function manulAnalysisOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' id='showBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";/*onclick=\"showManulAnalysis('" + id + "', view='view')\"*/
    if(userpk == row.createor_id){
    	result += "<a href='javascript:void(0);' id='delBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";/*onclick=\"deleteManulAnalysis('" + id + "')\"*/
    }
    result += "<a href='javascript:void(0);' id='bgBtn' class='btn' title='分析报告'><span class='glyphicon glyphicon-file'></span></a>";
    return result;
}

//操作栏绑定事件
window.manulAnalysisOperateEvents = {
    "click #showBtn": function (e, vale, row, index) {
        var item = {
            'id': 'screenData',
            'name': '筛选的数据',
            'url': ctx + '/analysis/manulAnalysis/screenDataPage?pk=' + row.pk
        };
        iframeTab.parentAddIframe(item);

    },
    "click #editBtn": function (e, vale, row, index) {
        //设置模态框标题
        $("#editModalLabel").text("**修改");
        //设置操作类型
        $("#edit_curdType").val("update");
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").attr('disabled','disabled');

        //请根据需要修改/删除 需展示字段
        //注意：字典类型数据需要dm和mc都进行设置
        $("#edit_pk").val(row.pk);
        $("#edit_create_date").val(row.create_date);
        $("#edit_createor_id").val(row.createor_id);
        $("#edit_createor_name").val(row.createor_name);
        $("#edit_conditions").val(row.conditions);

        //模态框的确认(保存)按钮绑定事件
        $("#editConfirmBtn").on("click", function () {
            //curdManulAnalysis();
            manulAnalysisEditSubmit();
        });
        $("#editModal").modal();
    },
    "click #delBtn": function (e, vale, row, index) {
        $("#tipsModalLabel").html("<i class='fa fa-warning fa-lg' style='color: #c87f0a'></i>警告");//提示标题
        $("#tipsMessage").text("确认删除么？");//设置提示信息
        $("#tipsConfirmBtn").text("删除");//按钮名称
        $("#tipsConfirmBtn").on("click", function () {
            deleteManulAnalysis(row.pk);
        });
        $("#tipsModal").modal();
    },
    "click #bgBtn": function (e, vale, row, index) {
        var item = {'id': 'bgInfo', 'name': '分析报告', 'url': ctx + "/analysis/manulAnalysis/bgInfo?pk=" + row.pk};
        iframeTab.parentAddIframe(item);
    }
}

function addManulAnalysis() {
    $("#editModalLabel").text("**新增");
    $("#edit_curdType").val("insert");

    //请根据需要修改/删除 需展示字段
    //此处设置字段与修改时相同，是为清空点击修改时为元素的赋值
    //注意：字典类型数据需要dm和mc都进行设置
    $("#edit_pk").val('');
    $("#edit_create_date").val('');
    $("#edit_createor_id").val('');
    $("#edit_createor_name").val('');
    $("#edit_conditions").val('');

    $("#editConfirmBtn").on("click", function () {
        //curdManulAnalysis();
        manulAnalysisEditSubmit();
    });

    $("#editModal").modal();
}

function deleteManulAnalysis(id) {
    var reqData = {"curdType": "delete", "pk": id};
    $("#tipsModal").modal('toggle');
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx + "/analysis/manulAnalysis/curd",
        data: reqData,
        success: function (result) {
            alert(result.msg);
            search();
        },
        error: function () {
            alert("请求异常");
        }
    });
}

function curdManulAnalysis() {
    //防止重复点击
    $("#editConfirmBtn").attr('disabled', 'disabled');
    var reqData = $('#manulAnalysisEditForm').serializeJsonObject();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx + "/analysis/manulAnalysis/curd",
        data: reqData,
        success: function (data) {
            alert(data.msg);
            $("#editModal").modal('toggle');

            search();
        },
        error: function () {
            alert("请求异常");
        }
    });
}

function search() {
    //initmanulAnalysisGrid();
    $("#manulAnalysisGrid").bootstrapTable("refresh");
}

function exportManulAnalysis() {
    var form = document.manulAnalysisQueryForm;
    var url = ctx + "/analysis/manulAnalysis/excel";
    form.action = url;
    form.submit();
}

var manulAnalysisEditSubmit = function () {
    var flag = $('#manulAnalysisEditForm').validate('submitValidate');
    if (flag) {
        curdManulAnalysis();
    } else {
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}

