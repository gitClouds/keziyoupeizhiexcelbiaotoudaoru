
$(function () {
    initVZhQzfzhv1Grid();
    formEditValidate('vZhQzfzhv1EditForm');//表单验证
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
    $('#editModal').on('hidden.bs.modal', function (event) {
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").removeAttr('disabled');
        $("#editConfirmBtn").removeAttr('disabled','disabled');
        $("#editConfirmBtn").unbind();
        formEditValidateReset('vZhQzfzhv1EditForm');
    });
})

function initVZhQzfzhv1Grid(){
    $('#vZhQzfzhv1Grid').bootstrapTable('destroy');

    $("#vZhQzfzhv1Grid").bootstrapTable({
        method:"get",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        //contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/wxjyyjmx/vZhQzfzhv1/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryVZhQzfzhv1Params,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
		showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
        //请根据需要修改/删除 需展示字段
            {
                title : "登录号",
                field : "dlh",
                align : "center"
            },
            {
                title : "姓名/企业名称",
                field : "xm_qymc",
                align : "center"
            },
            {
                title : "银行卡认证状态",
                field : "yhkrzzt",
                align : "center"
            },
            {
                title : "微信号",
                field : "wxh",
                align : "center"
            },
            {
                title : "证件类型",
                field : "zjlx",
                align : "center"
            },
            {
                title : "绑定手机号",
                field : "bdsjh",
                align : "center"
            },
            {
                title : "查询账号",
                field : "cxzh",
                align : "center"
            },
            {
                title : "支付账号",
                field : "zfzh",
                align : "center"
            },
            {
                title : "证件号",
                field : "zjh",
                align : "center"
            },
            {
                title : "账号类别",
                field : "zhlb",
                align : "center"
            },
            {
                title : "银行卡类型",
                field : "yhklx",
                align : "center"
            },
            {
                title : "证照有效期",
                field : "zzyxq",
                align : "center"
            },
            {
                title : "去重字段",
                field : "qczd",
                align : "center"
            },
            {
                title : "银行卡所属银行名称",
                field : "yhkssyhmc",
                align : "center"
            },
            {
                title : "银行卡有效期",
                field : "yhkyxq",
                align : "center"
            },
            {
                title : "时间",
                field : "sj",
                align : "center"
            },
            {
                title : "银行卡其他信息",
                field : "yhkqtxx",
                align : "center"
            },
            {
                title : "银行卡卡号",
                field : "yhkkh",
                align : "center"
            },
            {
                title : "QQ号",
                field : "qqh",
                align : "center"
            },
            {
                title : "银行卡所属银行机构编码",
                field : "yhkssyhjgbm",
                align : "center"
            },
            {
                title : "绑定手机号《微信主体》",
                field : "bdsjh_wxzt",
                align : "center"
            },
            {
                title : "绑定银行卡《微信主体》",
                field : "bdyhk_wxzt",
                align : "center"
            },
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function queryVZhQzfzhv1Params(params){
    var tmp = $("#vZhQzfzhv1QueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function vZhQzfzhv1OperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' id='showBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";/*onclick=\"showVZhQzfzhv1('" + id + "', view='view')\"*/
    result += "<a href='javascript:void(0);' id='editBtn' class='btn' title='编辑'><span class='fa fa-edit fa-lg'></span></a>";/*onclick=\"editVZhQzfzhv1('" + id + "')\"*/
    result += "<a href='javascript:void(0);' id='delBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";/*onclick=\"deleteVZhQzfzhv1('" + id + "')\"*/

    return result;
}
//操作栏绑定事件

function addVZhQzfzhv1() {
    $("#editModalLabel").text("**新增");
    $("#edit_curdType").val("insert");
    
    //请根据需要修改/删除 需展示字段
    //此处设置字段与修改时相同，是为清空点击修改时为元素的赋值
    //注意：字典类型数据需要dm和mc都进行设置
    $("#edit_dlh").val('');
    $("#edit_xm_qymc").val('');
    $("#edit_yhkrzzt").val('');
    $("#edit_wxh").val('');
    $("#edit_zjlx").val('');
    $("#edit_bdsjh").val('');
    $("#edit_cxzh").val('');
    $("#edit_zfzh").val('');
    $("#edit_zjh").val('');
    $("#edit_zhlb").val('');
    $("#edit_yhklx").val('');
    $("#edit_zzyxq").val('');
    $("#edit_qczd").val('');
    $("#edit_yhkssyhmc").val('');
    $("#edit_yhkyxq").val('');
    $("#edit_sj").val('');
    $("#edit_yhkqtxx").val('');
    $("#edit_yhkkh").val('');
    $("#edit_qqh").val('');
    $("#edit_yhkssyhjgbm").val('');
    $("#edit_bdsjh_wxzt").val('');
    $("#edit_bdyhk_wxzt").val('');
    
    $("#editConfirmBtn").on("click",function(){
        //curdVZhQzfzhv1();
        vZhQzfzhv1EditSubmit();
    });

    $("#editModal").modal();
}


function curdVZhQzfzhv1() {
	//防止重复点击
    $("#editConfirmBtn").attr('disabled','disabled');
    var reqData = $('#vZhQzfzhv1EditForm').serializeJsonObject();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/wxjyyjmx/vZhQzfzhv1/curd" ,
        data: reqData,
        success : function(data) {
            alert(data.msg);
            $("#editModal").modal('toggle');

            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function search() {
    //initvZhQzfzhv1Grid();
    $("#vZhQzfzhv1Grid").bootstrapTable("refresh");
}
function exportVZhQzfzhv1(){
    var form = document.vZhQzfzhv1QueryForm;
    var url = ctx+"/wxjyyjmx/vZhQzfzhv1/excel";
    form.action=url;
    form.submit();
}

var vZhQzfzhv1EditSubmit = function(){
    var flag = $('#vZhQzfzhv1EditForm').validate('submitValidate');
    if (flag){
        curdVZhQzfzhv1();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}

