

$(function () {
    initVZhZfzhdwdtj1Grid();
    formEditValidate('vZhZfzhdwdtj1EditForm');//表单验证
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
    $('#editModal').on('hidden.bs.modal', function (event) {
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").removeAttr('disabled');
        $("#editConfirmBtn").removeAttr('disabled','disabled');
        $("#editConfirmBtn").unbind();
        formEditValidateReset('vZhZfzhdwdtj1EditForm');
    });
})

function initVZhZfzhdwdtj1Grid(){
    $('#vZhZfzhdwdtj1Grid').bootstrapTable('destroy');

    $("#vZhZfzhdwdtj1Grid").bootstrapTable({
        method:"get",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        //contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/wxjyyjmx/vZhZfzhdwdtj1/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryVZhZfzhdwdtj1Params,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
		showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
        //请根据需要修改/删除 需展示字段
            {
                title : "支付账号",
                field : "cftzh",
                align : "center"
            },
            {
                title : "<我方>入账涉毒人数 ",
                field : "wf_rz_sdrs",
                align : "center"
            },
            {
                title : " 最后交易时间",
                field : "zhjysj",
                align : "center"
            },
            {
                title : " 最后交易情况",
                field : "zhjyqk",
                align : "center"
            },
            {
                title : "明细 ",
                field : "mx",
                align : "center"
            },
            {
                title : "绑定手机号新 ",
                field : "bdsjhmx",
                align : "center"
            },
            {
                title : "开户证件",
                field : "khzj",
                align : "center"
            },
            {
                title : "开户人 ",
                field : "khr",
                align : "center"
            },
            {
                title : "涉毒资金关系人数 ",
                field : "sdzjgxrs",
                align : "center"
            },
            {
                title : "入账人数 ",
                field : "rz_rs",
                align : "center"
            },
            {
                title : "入账涉毒人数 ",
                field : "rz_sdrs",
                align : "center"
            },
            {
                title : "入账金额 ",
                field : "rz_je",
                align : "center"
            },
            {
                title : "入账次数 ",
                field : "rz_cs",
                align : "center"
            },
            {
                title : "出账人数 ",
                field : "cz_cs",
                align : "center"
            },
            {
                title : "出账涉毒人数 ",
                field : "cz_sdrs",
                align : "center"
            },
            {
                title : "出账金额 ",
                field : "cz_je",
                align : "center"
            },
            {
                title : "已调资金关系人数 ",
                field : "ydzjgxrs",
                align : "center"
            },
            {
                title : "微信号 ",
                field : "wxh",
                align : "center"
            },
            {
                title : "下家",
                field : "xjx",
                align : "center"
            },
            {
                title : "上家 ",
                field : "sjx",
                align : "center"
            },
            {
                title : "涉毒好友",
                field : "sdhy",
                align : "center"
            },
            {
                title : "微信通联积分描述",
                field : "wxtljfmx",
                align : "center"
            },
            {
                title : "微信通联积分",
                field : "wxtljf",
                align : "center"
            },
            {
                title : "微信昵称",
                field : "wxnc",
                align : "center"
            },
            {
                title : "开户资料 ",
                field : "khzl",
                align : "center"
            },
            {
                title : "研判报告 ",
                field : "ypbg",
                align : "center"
            },
            {
                title : "经销商积分描述",
                field : "jxsjfms",
                align : "center"
            },
            {
                title : "经销商积分 ",
                field : "jxsjf",
                align : "center"
            },
            {
                title : "来源案件",
                field : "lyaj",
                align : "center"
            },
            {
                title : "实名手机",
                field : "smsj",
                align : "center"
            },
            {
                title : "最新参考位置",
                field : "zxckwz",
                align : "center"
            },
            {
                title : "夜间参考位置",
                field : "yjckwz",
                align : "center"
            },
            {
                title : "收件地址",
                field : "sjdz",
                align : "center"
            },
            {
                title : "阿里标签数",
                field : "albqs",
                align : "center"
            },
            {
                title : "收款规律积分(我方) ",
                field : "skgljf_wf",
                align : "center"
            },
            {
                title : "收款规律积分(对方)",
                field : "skgljf_df",
                align : "center"
            },
        /*    {
                title : "字段注释",
                field : "skgljf_max",
                align : "center"
            },*/
            {
                title : "积分描述新",
                field : "jfmsx",
                align : "center"
            },
            {
                title : "积分新",
                field : "jfx",
                align : "center"
            },
            {
                title : "研判报告1.0",
                field : "ypbg1",
                align : "center"
            },

        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function queryVZhZfzhdwdtj1Params(params){
    var tmp = $("#vZhZfzhdwdtj1QueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function vZhZfzhdwdtj1OperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' id='showBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";/*onclick=\"showVZhZfzhdwdtj1('" + id + "', view='view')\"*/
    result += "<a href='javascript:void(0);' id='editBtn' class='btn' title='编辑'><span class='fa fa-edit fa-lg'></span></a>";/*onclick=\"editVZhZfzhdwdtj1('" + id + "')\"*/
    result += "<a href='javascript:void(0);' id='delBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";/*onclick=\"deleteVZhZfzhdwdtj1('" + id + "')\"*/

    return result;
}

function addVZhZfzhdwdtj1() {
    $("#editModalLabel").text("**新增");
    $("#edit_curdType").val("insert");
    
    //请根据需要修改/删除 需展示字段
    //此处设置字段与修改时相同，是为清空点击修改时为元素的赋值
    //注意：字典类型数据需要dm和mc都进行设置
    $("#edit_cftzh").val('');
    $("#edit_wf_rz_sdrs").val('');
    $("#edit_zhjysj").val('');
    $("#edit_zhjyqk").val('');
    $("#edit_mx").val('');
    $("#edit_bdsjhmx").val('');
    $("#edit_khzj").val('');
    $("#edit_khr").val('');
    $("#edit_sdzjgxrs").val('');
    $("#edit_rz_rs").val('');
    $("#edit_rz_sdrs").val('');
    $("#edit_rz_je").val('');
    $("#edit_rz_cs").val('');
    $("#edit_cz_cs").val('');
    $("#edit_cz_sdrs").val('');
    $("#edit_cz_je").val('');
    $("#edit_ydzjgxrs").val('');
    $("#edit_wxh").val('');
    $("#edit_xjx").val('');
    $("#edit_sjx").val('');
    $("#edit_sdhy").val('');
    $("#edit_wxtljfmx").val('');
    $("#edit_wxtljf").val('');
    $("#edit_wxnc").val('');
    $("#edit_khzl").val('');
    $("#edit_ypbg").val('');
    $("#edit_jxsjfms").val('');
    $("#edit_jxsjf").val('');
    $("#edit_lyaj").val('');
    $("#edit_smsj").val('');
    $("#edit_zxckwz").val('');
    $("#edit_yjckwz").val('');
    $("#edit_sjdz").val('');
    $("#edit_albqs").val('');
    $("#edit_skgljf_wf").val('');
    $("#edit_skgljf_df").val('');
    $("#edit_skgljf_max").val('');
    $("#edit_jfmsx").val('');
    $("#edit_jfx").val('');
    $("#edit_ypbg1").val('');
    
    $("#editConfirmBtn").on("click",function(){
        //curdVZhZfzhdwdtj1();
        vZhZfzhdwdtj1EditSubmit();
    });

    $("#editModal").modal();
}


function search() {
    //initvZhZfzhdwdtj1Grid();
    $("#vZhZfzhdwdtj1Grid").bootstrapTable("refresh");
}
function exportVZhZfzhdwdtj1(){
    var form = document.vZhZfzhdwdtj1QueryForm;
    var url = ctx+"/wxjyyjmx/vZhZfzhdwdtj1/excel";
    form.action=url;
    form.submit();
}

var vZhZfzhdwdtj1EditSubmit = function(){
    var flag = $('#vZhZfzhdwdtj1EditForm').validate('submitValidate');
    if (flag){
        curdVZhZfzhdwdtj1();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}

