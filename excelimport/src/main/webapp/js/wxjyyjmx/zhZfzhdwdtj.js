

$(function () {
    initZhZfzhdwdtjGrid();
    formEditValidate('zhZfzhdwdtjEditForm');//表单验证
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
    $('#editModal').on('hidden.bs.modal', function (event) {
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").removeAttr('disabled');
        $("#editConfirmBtn").removeAttr('disabled','disabled');
        $("#editConfirmBtn").unbind();
        formEditValidateReset('zhZfzhdwdtjEditForm');
    });
})

function initZhZfzhdwdtjGrid(){
    $('#zhZfzhdwdtjGrid').bootstrapTable('destroy');

    $("#zhZfzhdwdtjGrid").bootstrapTable({
        method:"get",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        //contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/wxjyyjmx/zhZfzhdwdtj/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryZhZfzhdwdtjParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
		showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
        //请根据需要修改/删除 需展示字段
            {
                title : "最后交易时间日期形",
                field : "zhjysjrqx",
                align : "center"
            },
            {
                title : "已调明细",
                field : "ydmx",
                align : "center"
            },
            {
                title : "昆明到港次数《证件关联》",
                field : "kmdgcs_zjgl",
                align : "center"
            },
            {
                title : "对方支付账号交易次数",
                field : "dfzfzhjycs",
                align : "center"
            },
            {
                title : "《对方》最后交易时间",
                field : "dfzhjysj",
                align : "center"
            },
            {
                title : "《对方》最后交易情况",
                field : "dfzhjyqk",
                align : "center"
            },
            {
                title : "《对方》最早交易时间",
                field : "dfzzjysj",
                align : "center"
            },
            {
                title : "《对方》最早交易情况",
                field : "dfzzjyqk",
                align : "center"
            },
            {
                title : "《我方》最后交易余额",
                field : "wfzhjyye",
                align : "center"
            },
            {
                title : "《我方》最后来源案件",
                field : "wfzhlyaj",
                align : "center"
            },
            {
                title : "《我方》最后交易时间1",
                field : "wfzhjysj1",
                align : "center"
            },
            {
                title : "《我方》最后交易情况1",
                field : "wfzhjyqk1",
                align : "center"
            },
            {
                title : "全部前科信息",
                field : "qbqkxx",
                align : "center"
            },
            {
                title : "最近抓获时间",
                field : "zjzhsj",
                align : "center"
            },
            {
                title : "最近抓获数据来源",
                field : "zjzhsjly",
                align : "center"
            },
            {
                title : "前科人员户籍属地",
                field : "qkryhjsd",
                align : "center"
            },
            {
                title : "最近抓获人员类别中文",
                field : "zjzhrylbzw",
                align : "center"
            },
            {
                title : "前科受理单位",
                field : "qksldw",
                align : "center"
            },
            {
                title : "最近抓获情况",
                field : "zjzhqk",
                align : "center"
            },
            {
                title : "绑定银行卡《微信账号关联》",
                field : "bdyhk_wxzhgl",
                align : "center"
            },
            {
                title : "开户主体证件号或证照号《微信账号关联》",
                field : "khztzjhm_wxzhgl",
                align : "center"
            },
            {
                title : "开户主体姓名或企业名称《微信账号关联》",
                field : "khztxm_wxzhgl",
                align : "center"
            },
            {
                title : "绑定手机号《微信账号关联》",
                field : "bdsjh_wxzhgl",
                align : "center"
            },
            {
                title : "昆明到港次数《微信关联》",
                field : "kmdgcs_wxgl",
                align : "center"
            },
            {
                title : "对方微信号",
                field : "dfwxh",
                align : "center"
            },
            {
                title : "已调明细2",
                field : "ydmx2",
                align : "center"
            },
            {
                title : "《我方》最后交易时间",
                field : "wfzhjysj",
                align : "center"
            },
            {
                title : "《我方》最后交易情况",
                field : "wfzhjyqk",
                align : "center"
            },
            {
                title : "我方支付账号交易次数",
                field : "wfzfzhjycs",
                align : "center"
            },
            {
                title : "《我方》最早交易时间",
                field : "wfzzjysj",
                align : "center"
            },
            {
                title : "《我方》最早交易情况",
                field : "wfzzjyqk",
                align : "center"
            },
            {
                title : "绑定银行卡《我方支付》",
                field : "bdyhk_wfzf",
                align : "center"
            },
            {
                title : "证件号《我方支付》",
                field : "zjh_wfzf",
                align : "center"
            },
            {
                title : "开户姓名《我方支付》",
                field : "khxm_wfzf",
                align : "center"
            },
            {
                title : "绑定手机号《我方支付》",
                field : "bdsjh_wfzf",
                align : "center"
            },
            {
                title : "高位户籍《我方支付》",
                field : "gwhj_wfzf",
                align : "center"
            },
            {
                title : "微信号新《我方支付》",
                field : "wxzh_x_wfzf",
                align : "center"
            },
            {
                title : "昆明到港次数（我方证件）",
                field : "kmdgcs_wfzj",
                align : "center"
            },
            {
                title : "财付通账号",
                field : "cftzh",
                align : "center"
            },
            {
                title : "微信号",
                field : "wxh",
                align : "center"
            },
            {
                title : "财付通开户人",
                field : "cftkhr",
                align : "center"
            },
            {
                title : "财付通开户证件",
                field : "cftkhzj",
                align : "center"
            },
            {
                title : "财付通绑定手机",
                field : "cftbdsjh",
                align : "center"
            },
            {
                title : "《对方》高危户籍",
                field : "df_gwhj",
                align : "center"
            },
            {
                title : "出账次数",
                field : "czcs",
                align : "center"
            },
            {
                title : "出账金额",
                field : "czje",
                align : "center"
            },
            {
                title : "入账次数",
                field : "rzcs",
                align : "center"
            },
            {
                title : "入账金额",
                field : "rzje",
                align : "center"
            },
            {
                title : "资金关系人数",
                field : "zjgxrs",
                align : "center"
            },
            {
                title : "涉毒资金关系人数",
                field : "sdzjgxrs",
                align : "center"
            },
            {
                title : "对方账号入账人数",
                field : "dfzhrzrs",
                align : "center"
            },
            {
                title : "对方账号出账人数",
                field : "dfzhczrs",
                align : "center"
            },
            {
                title : "对方入账涉毒人数",
                field : "dfrzsdrs",
                align : "center"
            },
            {
                title : "对方出账涉毒人数",
                field : "dfczsdrs",
                align : "center"
            },
            {
                title : "敏感时段占比%",
                field : "mfsdzb",
                align : "center"
            },
            {
                title : "整数交易占比%",
                field : "zsjyzb",
                align : "center"
            },
            {
                title : "是否本地户籍人员对手",
                field : "sfbdhjryds",
                align : "center"
            },
            {
                title : "最后交易时间",
                field : "zhjysj",
                align : "center"
            },
            {
                title : "最后交易情况",
                field : "zhjyqk",
                align : "center"
            },
            {
                title : "《我方》微信号新1",
                field : "rz_wf_wxzh_x1",
                align : "center"
            },
            {
                title : "《我方》入账交易对手数",
                field : "rz_jydss",
                align : "center"
            },
            {
                title : "《我方》入账交易次数",
                field : "rz_jycs",
                align : "center"
            },
            {
                title : "《我方》入账交易金额",
                field : "rz_jyje",
                align : "center"
            },
            {
                title : "《我方》已调入账交易对手数",
                field : "rz_yd_jydss",
                align : "center"
            },
            {
                title : "《我方》入账涉毒对手《人员表》",
                field : "rz_df_sdzjhm_ryb",
                align : "center"
            },
            {
                title : "《我方》入账涉赌人数",
                field : "rz_sdrs",
                align : "center"
            },
            {
                title : "《我方》入账涉毒对手《微信关联人》",
                field : "rz_df_zjhm_wxglr",
                align : "center"
            },
            {
                title : "《我方》入账敏感时段率",
                field : "rz_mgsjl",
                align : "center"
            },
            {
                title : "《我方》入账整数交易率",
                field : "rz_zsjyl",
                align : "center"
            },
            {
                title : "《我方》入账涉毒前科金额",
                field : "rz_sdqkje",
                align : "center"
            },
            {
                title : "《我方》入账涉赌前科金额",
                field : "rz_sdqkje1",
                align : "center"
            },
            {
                title : "《我方》出账交易对手数",
                field : "cz_jydss",
                align : "center"
            },
            {
                title : "《我方》出账交易次数",
                field : "cz_jycs",
                align : "center"
            },
            {
                title : "《我方》出账交易金额",
                field : "cz_jyje",
                align : "center"
            },
            {
                title : "《我方》出账已调交易对手数",
                field : "cz_yd_jydss",
                align : "center"
            },
            {
                title : "《我方》出账涉毒对手《人员表》",
                field : "cz_df_sdzjhm_ryb",
                align : "center"
            },
            {
                title : "《我方》出账涉赌人数",
                field : "cz_sdrs",
                align : "center"
            },
            {
                title : "《我方》出账涉毒对手《微信关联人》",
                field : "cz_df_zjhm_wxglr",
                align : "center"
            },
            {
                title : "《我方》出账敏感时段率",
                field : "cz_mgsjl",
                align : "center"
            },
            {
                title : "《我方》出账整数交易率",
                field : "cz_zsjyl",
                align : "center"
            },
            {
                title : "《我方》出账涉毒前科金额",
                field : "cz_sdqkje",
                align : "center"
            },
            {
                title : "《我方》出账涉赌前科金额",
                field : "cz_sdqkje1",
                align : "center"
            },
            {
                title : "《我方》总交易次数",
                field : "zjycs",
                align : "center"
            },
            {
                title : "《我方》总交易对手",
                field : "zjyds",
                align : "center"
            },
            {
                title : "《我方》总交易金额",
                field : "zjyje",
                align : "center"
            },
            {
                title : "《我方》已调交易对手",
                field : "ydjyds",
                align : "center"
            },
            {
                title : "《我方》涉毒对手《人员表》",
                field : "df_sdzjhm_ryb",
                align : "center"
            },
            {
                title : "《我方》涉毒对手《微信关联人》",
                field : "df_zjhm_wxglr",
                align : "center"
            },
            {
                title : "《我方》微信账号新2",
                field : "wf_wxzh_x1",
                align : "center"
            },
            {
                title : "《我方》高危户籍",
                field : "wf_gwhj",
                align : "center"
            },
            {
                title : "上家",
                field : "sj",
                align : "center"
            },
            {
                title : "下家",
                field : "xj",
                align : "center"
            },
            {
                title : "我方上家",
                field : "wfsj",
                align : "center"
            },
            {
                title : "我方下家",
                field : "wfxj",
                align : "center"
            },
            {
                title : "敏感时段率新",
                field : "mgsdlx",
                align : "center"
            },
            {
                title : "整数交易率新",
                field : "zsjylx",
                align : "center"
            },
            {
                title : "最早交易时间(新)",
                field : "zzjysjx",
                align : "center"
            },
            {
                title : "最后交易情况新",
                field : "zhjyqkx",
                align : "center"
            },
            {
                title : "昆明到港次数新",
                field : "kmdgcsx",
                align : "center"
            },
            {
                title : "是否调取明细A",
                field : "sfdqmxa",
                align : "center"
            },
            {
                title : "绑定银行卡新A",
                field : "bdyhkxa",
                align : "center"
            },
            {
                title : "最后交易时间新",
                field : "zhjysjx",
                align : "center"
            },
            {
                title : "绑定手机号码新",
                field : "bdsjhmx",
                align : "center"
            },
            {
                title : "财富通开户证件（新）",
                field : "cftkhzjx",
                align : "center"
            },
            {
                title : "财富通开户人（新）",
                field : "cftkhrx",
                align : "center"
            },
            {
                title : "出账涉毒人数占比",
                field : "czsdrhzb",
                align : "center"
            },
            {
                title : "入账涉毒人数占比",
                field : "rzsdrszb",
                align : "center"
            },
            {
                title : "最后交易时间日期格式",
                field : "zhjysjrqgs",
                align : "center"
            },
            {
                title : "高危户籍新",
                field : "gwhjx",
                align : "center"
            },
            {
                title : "涉毒资金关系人数A",
                field : "sdzigxrsa",
                align : "center"
            },
            {
                title : "入账人数新A",
                field : "rzrsxa",
                align : "center"
            },
            {
                title : "入账涉毒人数新A",
                field : "rzsdrsxa",
                align : "center"
            },
            {
                title : "入账金额新A",
                field : "rzjexa",
                align : "center"
            },
            {
                title : "入账次数新A",
                field : "rzcsxa",
                align : "center"
            },
            {
                title : "出账人数新A",
                field : "czrsxa",
                align : "center"
            },
            {
                title : "出账涉毒人数新A",
                field : "czsdrsxa",
                align : "center"
            },
            {
                title : "出账金额新A",
                field : "czjexa",
                align : "center"
            },
            {
                title : "已调资金关系人数",
                field : "ydzjgxrs",
                align : "center"
            },
            {
                title : "微信号新",
                field : "wxhx",
                align : "center"
            },
            {
                title : "《我方》敏感时段率",
                field : "wfmgsdl",
                align : "center"
            },
            {
                title : "《我方》整数交易率",
                field : "wfzsjyl",
                align : "center"
            },
            {
                title : "开户资料",
                field : "khzl",
                align : "center"
            },
            {
                title : "经销商积分描述新A",
                field : "jxsdfmsxa",
                align : "center"
            },
            {
                title : "经销商积分新A",
                field : "jxsdfxa",
                align : "center"
            },
            {
                title : "研判报告",
                field : "ypbg",
                align : "center"
            },
            {
                title : "涉毒好友",
                field : "sdhy",
                align : "center"
            },
            {
                title : "证件号码《微信通联》新",
                field : "zjhm_wxlt_x",
                align : "center"
            },
            {
                title : "姓名《微信通联》新",
                field : "xm_wxlt_x",
                align : "center"
            },
            {
                title : "微信通联积分描述新",
                field : "wxtldfmsx",
                align : "center"
            },
            {
                title : "微信通联积分新",
                field : "wxtldfx",
                align : "center"
            },
            {
                title : "微信昵称新",
                field : "wxncx",
                align : "center"
            },
            {
                title : "吸毒仔积分描述",
                field : "xdzdfms",
                align : "center"
            },
            {
                title : "积分《吸毒仔》",
                field : "df_xdz",
                align : "center"
            },
            {
                title : "积分《分销商》旧",
                field : "df_fxs_j",
                align : "center"
            },
            {
                title : "经销商积分描述",
                field : "jxsdfms",
                align : "center"
            },
            {
                title : "上家新",
                field : "sjx",
                align : "center"
            },
            {
                title : "下家新",
                field : "xjx",
                align : "center"
            },

        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function queryZhZfzhdwdtjParams(params){
    var tmp = $("#zhZfzhdwdtjQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function zhZfzhdwdtjOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' id='showBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";/*onclick=\"showZhZfzhdwdtj('" + id + "', view='view')\"*/
    result += "<a href='javascript:void(0);' id='editBtn' class='btn' title='编辑'><span class='fa fa-edit fa-lg'></span></a>";/*onclick=\"editZhZfzhdwdtj('" + id + "')\"*/
    result += "<a href='javascript:void(0);' id='delBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";/*onclick=\"deleteZhZfzhdwdtj('" + id + "')\"*/

    return result;
}

function addZhZfzhdwdtj() {
    $("#editModalLabel").text("**新增");
    $("#edit_curdType").val("insert");
    
    //请根据需要修改/删除 需展示字段
    //此处设置字段与修改时相同，是为清空点击修改时为元素的赋值
    //注意：字典类型数据需要dm和mc都进行设置
    $("#edit_zhjysjrqx").val('');
    $("#edit_ydmx").val('');
    $("#edit_kmdgcs_zjgl").val('');
    $("#edit_dfzfzhjycs").val('');
    $("#edit_dfzhjysj").val('');
    $("#edit_dfzhjyqk").val('');
    $("#edit_dfzzjysj").val('');
    $("#edit_dfzzjyqk").val('');
    $("#edit_wfzhjyye").val('');
    $("#edit_wfzhlyaj").val('');
    $("#edit_wfzhjysj1").val('');
    $("#edit_wfzhjyqk1").val('');
    $("#edit_qbqkxx").val('');
    $("#edit_zjzhsj").val('');
    $("#edit_zjzhsjly").val('');
    $("#edit_qkryhjsd").val('');
    $("#edit_zjzhrylbzw").val('');
    $("#edit_qksldw").val('');
    $("#edit_zjzhqk").val('');
    $("#edit_bdyhk_wxzhgl").val('');
    $("#edit_khztzjhm_wxzhgl").val('');
    $("#edit_khztxm_wxzhgl").val('');
    $("#edit_bdsjh_wxzhgl").val('');
    $("#edit_kmdgcs_wxgl").val('');
    $("#edit_dfwxh").val('');
    $("#edit_ydmx2").val('');
    $("#edit_wfzhjysj").val('');
    $("#edit_wfzhjyqk").val('');
    $("#edit_wfzfzhjycs").val('');
    $("#edit_wfzzjysj").val('');
    $("#edit_wfzzjyqk").val('');
    $("#edit_bdyhk_wfzf").val('');
    $("#edit_zjh_wfzf").val('');
    $("#edit_khxm_wfzf").val('');
    $("#edit_bdsjh_wfzf").val('');
    $("#edit_gwhj_wfzf").val('');
    $("#edit_wxzh_x_wfzf").val('');
    $("#edit_kmdgcs_wfzj").val('');
    $("#edit_cftzh").val('');
    $("#edit_wxh").val('');
    $("#edit_cftkhr").val('');
    $("#edit_cftkhzj").val('');
    $("#edit_cftbdsjh").val('');
    $("#edit_df_gwhj").val('');
    $("#edit_czcs").val('');
    $("#edit_czje").val('');
    $("#edit_rzcs").val('');
    $("#edit_rzje").val('');
    $("#edit_zjgxrs").val('');
    $("#edit_sdzjgxrs").val('');
    $("#edit_dfzhrzrs").val('');
    $("#edit_dfzhczrs").val('');
    $("#edit_dfrzsdrs").val('');
    $("#edit_dfczsdrs").val('');
    $("#edit_mfsdzb").val('');
    $("#edit_zsjyzb").val('');
    $("#edit_sfbdhjryds").val('');
    $("#edit_zhjysj").val('');
    $("#edit_zhjyqk").val('');
    $("#edit_rz_wf_wxzh_x1").val('');
    $("#edit_rz_jydss").val('');
    $("#edit_rz_jycs").val('');
    $("#edit_rz_jyje").val('');
    $("#edit_rz_yd_jydss").val('');
    $("#edit_rz_df_sdzjhm_ryb").val('');
    $("#edit_rz_sdrs").val('');
    $("#edit_rz_df_zjhm_wxglr").val('');
    $("#edit_rz_mgsjl").val('');
    $("#edit_rz_zsjyl").val('');
    $("#edit_rz_sdqkje").val('');
    $("#edit_rz_sdqkje1").val('');
    $("#edit_cz_jydss").val('');
    $("#edit_cz_jycs").val('');
    $("#edit_cz_jyje").val('');
    $("#edit_cz_yd_jydss").val('');
    $("#edit_cz_df_sdzjhm_ryb").val('');
    $("#edit_cz_sdrs").val('');
    $("#edit_cz_df_zjhm_wxglr").val('');
    $("#edit_cz_mgsjl").val('');
    $("#edit_cz_zsjyl").val('');
    $("#edit_cz_sdqkje").val('');
    $("#edit_cz_sdqkje1").val('');
    $("#edit_zjycs").val('');
    $("#edit_zjyds").val('');
    $("#edit_zjyje").val('');
    $("#edit_ydjyds").val('');
    $("#edit_df_sdzjhm_ryb").val('');
    $("#edit_df_zjhm_wxglr").val('');
    $("#edit_wf_wxzh_x1").val('');
    $("#edit_wf_gwhj").val('');
    $("#edit_sj").val('');
    $("#edit_xj").val('');
    $("#edit_wfsj").val('');
    $("#edit_wfxj").val('');
    $("#edit_mgsdlx").val('');
    $("#edit_zsjylx").val('');
    $("#edit_zzjysjx").val('');
    $("#edit_zhjyqkx").val('');
    $("#edit_kmdgcsx").val('');
    $("#edit_sfdqmxa").val('');
    $("#edit_bdyhkxa").val('');
    $("#edit_zhjysjx").val('');
    $("#edit_bdsjhmx").val('');
    $("#edit_cftkhzjx").val('');
    $("#edit_cftkhrx").val('');
    $("#edit_czsdrhzb").val('');
    $("#edit_rzsdrszb").val('');
    $("#edit_zhjysjrqgs").val('');
    $("#edit_gwhjx").val('');
    $("#edit_sdzigxrsa").val('');
    $("#edit_rzrsxa").val('');
    $("#edit_rzsdrsxa").val('');
    $("#edit_rzjexa").val('');
    $("#edit_rzcsxa").val('');
    $("#edit_czrsxa").val('');
    $("#edit_czsdrsxa").val('');
    $("#edit_czjexa").val('');
    $("#edit_ydzjgxrs").val('');
    $("#edit_wxhx").val('');
    $("#edit_wfmgsdl").val('');
    $("#edit_wfzsjyl").val('');
    $("#edit_khzl").val('');
    $("#edit_jxsdfmsxa").val('');
    $("#edit_jxsdfxa").val('');
    $("#edit_ypbg").val('');
    $("#edit_sdhy").val('');
    $("#edit_zjhm_wxlt_x").val('');
    $("#edit_xm_wxlt_x").val('');
    $("#edit_wxtldfmsx").val('');
    $("#edit_wxtldfx").val('');
    $("#edit_wxncx").val('');
    $("#edit_xdzdfms").val('');
    $("#edit_df_xdz").val('');
    $("#edit_df_fxs_j").val('');
    $("#edit_jxsdfms").val('');
    $("#edit_sjx").val('');
    $("#edit_xjx").val('');
    
    $("#editConfirmBtn").on("click",function(){
        //curdZhZfzhdwdtj();
        zhZfzhdwdtjEditSubmit();
    });

    $("#editModal").modal();
}


function search() {
    //initzhZfzhdwdtjGrid();
    $("#zhZfzhdwdtjGrid").bootstrapTable("refresh");
}
function exportZhZfzhdwdtj(){
    var form = document.zhZfzhdwdtjQueryForm;
    var url = ctx+"/wxjyyjmx/zhZfzhdwdtj/excel";
    form.action=url;
    form.submit();
}

var zhZfzhdwdtjEditSubmit = function(){
    var flag = $('#zhZfzhdwdtjEditForm').validate('submitValidate');
    if (flag){
        curdZhZfzhdwdtj();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}

