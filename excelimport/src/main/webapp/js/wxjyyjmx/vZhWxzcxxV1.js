

$(function () {
    initVZhWxzcxxV1Grid();
    formEditValidate('vZhWxzcxxV1EditForm');//表单验证
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
    $('#editModal').on('hidden.bs.modal', function (event) {
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").removeAttr('disabled');
        $("#editConfirmBtn").removeAttr('disabled','disabled');
        $("#editConfirmBtn").unbind();
        formEditValidateReset('vZhWxzcxxV1EditForm');
    });
})

function initVZhWxzcxxV1Grid(){
    $('#vZhWxzcxxV1Grid').bootstrapTable('destroy');

    $("#vZhWxzcxxV1Grid").bootstrapTable({
        method:"get",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        //contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/wxjyyjmx/vZhWxzcxxV1/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryVZhWxzcxxV1Params,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
		showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
        //请根据需要修改/删除 需展示字段
            {
                title : "绑定银行卡",
                field : "bdyhk",
                align : "center"
            },
            {
                title : "备注",
                field : "bz",
                align : "center"
            },
            {
                title : "开户主体或证件号码",
                field : "khztzjhm",
                align : "center"
            },
            {
                title : "证件有效期",
                field : "zjyxq",
                align : "center"
            },
            {
                title : "查询反馈说明",
                field : "cxfksm",
                align : "center"
            },
            {
                title : "反馈机构名称",
                field : "fkjgmc",
                align : "center"
            },
            {
                title : "查询账号",
                field : "cxzh",
                align : "center"
            },
            {
                title : "微信号",
                field : "wxh",
                align : "center"
            },
            {
                title : "商户号码",
                field : "shhm",
                align : "center"
            },
            {
                title : "来源",
                field : "ly",
                align : "center"
            },
            {
                title : "来源分局",
                field : "lyfj",
                align : "center"
            },
            {
                title : "开户主体姓名",
                field : "khztxm",
                align : "center"
            },
            {
                title : "商户名称",
                field : "shmc",
                align : "center"
            },
            {
                title : "开户主体证件类型",
                field : "khztzjlx",
                align : "center"
            },
            {
                title : "qq号",
                field : "qqh",
                align : "center"
            },
            {
                title : "绑定手机号",
                field : "bdsjh",
                align : "center"
            },
            {
                title : "时间",
                field : "sj",
                align : "center"
            },
            {
                title : "注册信息录入时间",
                field : "zcxxlrsj",
                align : "center"
            },
            {
                title : "查询账号是否是身份证",
                field : "cxzh_sfssfz",
                align : "center"
            },
            {
                title : "微信账号新",
                field : "wxzh_x",
                align : "center"
            },
            {
                title : "去重字段新",
                field : "qczd_x",
                align : "center"
            },
            {
                title : "身份证件前6位",
                field : "sfzj6",
                align : "center"
            },
            {
                title : "高危户籍",
                field : "gwhj",
                align : "center"
            },

        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function queryVZhWxzcxxV1Params(params){
    var tmp = $("#vZhWxzcxxV1QueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function vZhWxzcxxV1OperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' id='showBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";/*onclick=\"showVZhWxzcxxV1('" + id + "', view='view')\"*/
    result += "<a href='javascript:void(0);' id='editBtn' class='btn' title='编辑'><span class='fa fa-edit fa-lg'></span></a>";/*onclick=\"editVZhWxzcxxV1('" + id + "')\"*/
    result += "<a href='javascript:void(0);' id='delBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";/*onclick=\"deleteVZhWxzcxxV1('" + id + "')\"*/

    return result;
}

function addVZhWxzcxxV1() {
    $("#editModalLabel").text("**新增");
    $("#edit_curdType").val("insert");
    
    //请根据需要修改/删除 需展示字段
    //此处设置字段与修改时相同，是为清空点击修改时为元素的赋值
    //注意：字典类型数据需要dm和mc都进行设置
    $("#edit_bdyhk").val('');
    $("#edit_bz").val('');
    $("#edit_khztzjhm").val('');
    $("#edit_zjyxq").val('');
    $("#edit_cxfksm").val('');
    $("#edit_fkjgmc").val('');
    $("#edit_cxzh").val('');
    $("#edit_wxh").val('');
    $("#edit_shhm").val('');
    $("#edit_ly").val('');
    $("#edit_lyfj").val('');
    $("#edit_khztxm").val('');
    $("#edit_shmc").val('');
    $("#edit_khztzjlx").val('');
    $("#edit_qqh").val('');
    $("#edit_bdsjh").val('');
    $("#edit_sj").val('');
    $("#edit_zcxxlrsj").val('');
    $("#edit_cxzh_sfssfz").val('');
    $("#edit_wxzh_x").val('');
    $("#edit_qczd_x").val('');
    $("#edit_sfzj6").val('');
    $("#edit_gwhj").val('');
    
    $("#editConfirmBtn").on("click",function(){
        //curdVZhWxzcxxV1();
        vZhWxzcxxV1EditSubmit();
    });

    $("#editModal").modal();
}


function search() {
    //initvZhWxzcxxV1Grid();
    $("#vZhWxzcxxV1Grid").bootstrapTable("refresh");
}
function exportVZhWxzcxxV1(){
    var form = document.vZhWxzcxxV1QueryForm;
    var url = ctx+"/wxjyyjmx/vZhWxzcxxV1/excel";
    form.action=url;
    form.submit();
}

var vZhWxzcxxV1EditSubmit = function(){
    var flag = $('#vZhWxzcxxV1EditForm').validate('submitValidate');
    if (flag){
        curdVZhWxzcxxV1();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}

