

$(function () {
    initVZhDfzfzhZzjysjGrid();
    formEditValidate('vZhDfzfzhZzjysjEditForm');//表单验证
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
    $('#editModal').on('hidden.bs.modal', function (event) {
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").removeAttr('disabled');
        $("#editConfirmBtn").removeAttr('disabled','disabled');
        $("#editConfirmBtn").unbind();
        formEditValidateReset('vZhDfzfzhZzjysjEditForm');
    });
})

function initVZhDfzfzhZzjysjGrid(){
    $('#vZhDfzfzhZzjysjGrid').bootstrapTable('destroy');

    $("#vZhDfzfzhZzjysjGrid").bootstrapTable({
        method:"get",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        //contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/wxjyyjmx/vZhDfzfzhZzjysj/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryVZhDfzfzhZzjysjParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
		showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
        //请根据需要修改/删除 需展示字段
            {
                title : "我方支付账号",
                field : "wfzfzh",
                align : "center"
            },
            {
                title : "交易主体的出入账标识",
                field : "jyztdcrzbs",
                align : "center"
            },
            {
                title : "对方支付账号",
                field : "dfzfzh",
                align : "center"
            },
            {
                title : "交易类型",
                field : "jylx",
                align : "center"
            },
            {
                title : "交易时间",
                field : "jysj",
                align : "center"
            },
            {
                title : "交易金额",
                field : "jyje",
                align : "center"
            },
            {
                title : "对方支付账号最早交易情况",
                field : "dfzfzhzzjyqk",
                align : "center"
            },

        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function queryVZhDfzfzhZzjysjParams(params){
    var tmp = $("#vZhDfzfzhZzjysjQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function vZhDfzfzhZzjysjOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' id='showBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";/*onclick=\"showVZhDfzfzhZzjysj('" + id + "', view='view')\"*/
    result += "<a href='javascript:void(0);' id='editBtn' class='btn' title='编辑'><span class='fa fa-edit fa-lg'></span></a>";/*onclick=\"editVZhDfzfzhZzjysj('" + id + "')\"*/
    result += "<a href='javascript:void(0);' id='delBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";/*onclick=\"deleteVZhDfzfzhZzjysj('" + id + "')\"*/

    return result;
}

function addVZhDfzfzhZzjysj() {
    $("#editModalLabel").text("**新增");
    $("#edit_curdType").val("insert");
    
    //请根据需要修改/删除 需展示字段
    //此处设置字段与修改时相同，是为清空点击修改时为元素的赋值
    //注意：字典类型数据需要dm和mc都进行设置
    $("#edit_wfzfzh").val('');
    $("#edit_jyztdcrzbs").val('');
    $("#edit_dfzfzh").val('');
    $("#edit_jylx").val('');
    $("#edit_jysj").val('');
    $("#edit_jyje").val('');
    $("#edit_dfzfzhzzjyqk").val('');
    
    $("#editConfirmBtn").on("click",function(){
        //curdVZhDfzfzhZzjysj();
        vZhDfzfzhZzjysjEditSubmit();
    });

    $("#editModal").modal();
}


function search() {
    //initvZhDfzfzhZzjysjGrid();
    $("#vZhDfzfzhZzjysjGrid").bootstrapTable("refresh");
}
function exportVZhDfzfzhZzjysj(){
    var form = document.vZhDfzfzhZzjysjQueryForm;
    var url = ctx+"/wxjyyjmx/vZhDfzfzhZzjysj/excel";
    form.action=url;
    form.submit();
}

var vZhDfzfzhZzjysjEditSubmit = function(){
    var flag = $('#vZhDfzfzhZzjysjEditForm').validate('submitValidate');
    if (flag){
        curdVZhDfzfzhZzjysj();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}

