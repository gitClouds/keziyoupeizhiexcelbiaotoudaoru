

$(function () {
    initVZhWfCrzjhTjGrid();
    formEditValidate('vZhWfCrzjhTjEditForm');//表单验证
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
    $('#editModal').on('hidden.bs.modal', function (event) {
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").removeAttr('disabled');
        $("#editConfirmBtn").removeAttr('disabled','disabled');
        $("#editConfirmBtn").unbind();
        formEditValidateReset('vZhWfCrzjhTjEditForm');
    });
})

function initVZhWfCrzjhTjGrid(){
    $('#vZhWfCrzjhTjGrid').bootstrapTable('destroy');

    $("#vZhWfCrzjhTjGrid").bootstrapTable({
        method:"get",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        //contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/wxjyyjmx/vZhWfCrzjhTj/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryVZhWfCrzjhTjParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
		showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
        //请根据需要修改/删除 需展示字段
            {
                title : "我方支付账户",
                field : "wfzfzh",
                align : "center"
            },
            {
                title : "《入账》我方微信号",
                field : "rz_wf_wxh",
                align : "center"
            },
            {
                title : "《入账》我方微信账号新1",
                field : "rz_wf_wxzh_x1",
                align : "center"
            },
            {
                title : "《入账》我方高危户籍",
                field : "rz_wf_gwhj",
                align : "center"
            },
            {
                title : "《入账》交易对手数",
                field : "rz_jydss",
                align : "center"
            },
            {
                title : "《入账》交易次数",
                field : "rz_jycs",
                align : "center"
            },
            {
                title : "《入账》交易金额",
                field : "rz_jyje",
                align : "center"
            },
            {
                title : "《入账》涉毒前科金额",
                field : "rz_sdqkje",
                align : "center"
            },
            {
                title : "《入账》涉赌前科金额",
                field : "rz_sdqkje1",
                align : "center"
            },
            {
                title : "《入账》已调交易对手数",
                field : "rz_yd_jydss",
                align : "center"
            },
            {
                title : "《入账》对方涉毒证件号码人员表",
                field : "rz_df_sdzjhm_ryb",
                align : "center"
            },
            {
                title : "《入账》敏感时间段",
                field : "rz_mgsjl",
                align : "center"
            },
            {
                title : "《入账》整数交易率",
                field : "rz_zsjyl",
                align : "center"
            },
            {
                title : "《入账》涉毒人数",
                field : "rz_sdrs",
                align : "center"
            },
            {
                title : "《入账》对方证件号码微信关联人",
                field : "rz_df_zjhm_wxglr",
                align : "center"
            },
            {
                title : "《出账》我方微信账号新1",
                field : "cz_wf_wxzh_x1",
                align : "center"
            },
            {
                title : "《出账》我方高危户籍",
                field : "cz_wf_gwhj",
                align : "center"
            },
            {
                title : "《出账》交易对手数",
                field : "cz_jydss",
                align : "center"
            },
            {
                title : "《出账》交易次数",
                field : "cz_jycs",
                align : "center"
            },
            {
                title : "《出账》交易金额",
                field : "cz_jyje",
                align : "center"
            },
            {
                title : "《出账》涉毒前科金额",
                field : "cz_sdqkje",
                align : "center"
            },
            {
                title : "《出账》涉赌前科金额",
                field : "cz_sdqkje1",
                align : "center"
            },
            {
                title : "《出账》已调交易对手数",
                field : "cz_yd_jydss",
                align : "center"
            },
            {
                title : "《出账》对方涉毒证件号码人员表",
                field : "cz_df_sdzjhm_ryb",
                align : "center"
            },
            {
                title : "《出账》敏感时间段",
                field : "cz_mgsjl",
                align : "center"
            },
            {
                title : "《出账》整数交易率",
                field : "cz_zsjyl",
                align : "center"
            },
            {
                title : "《出账》涉毒人数",
                field : "cz_sdrs",
                align : "center"
            },
            {
                title : "《出账》对方证件号码微信关联人",
                field : "cz_df_zjhm_wxglr",
                align : "center"
            },
            {
                title : "总交易次数",
                field : "zjycs",
                align : "center"
            },
            {
                title : "总交易对手",
                field : "zjyds",
                align : "center"
            },
            {
                title : "总交易金额",
                field : "zjyje",
                align : "center"
            },
            {
                title : "已调交易对手",
                field : "ydjyds",
                align : "center"
            },
            {
                title : "对方涉毒证件号码_人员表",
                field : "df_sdzjhm_ryb",
                align : "center"
            },
            {
                title : "对方证件号码_微信关联人",
                field : "df_zjhm_wxglr",
                align : "center"
            },
            {
                title : "我方微信账号新1",
                field : "wf_wxzh_x1",
                align : "center"
            },
            {
                title : "我方高危户籍",
                field : "wf_gwhj",
                align : "center"
            },

        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function queryVZhWfCrzjhTjParams(params){
    var tmp = $("#vZhWfCrzjhTjQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function vZhWfCrzjhTjOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' id='showBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";/*onclick=\"showVZhWfCrzjhTj('" + id + "', view='view')\"*/
    result += "<a href='javascript:void(0);' id='editBtn' class='btn' title='编辑'><span class='fa fa-edit fa-lg'></span></a>";/*onclick=\"editVZhWfCrzjhTj('" + id + "')\"*/
    result += "<a href='javascript:void(0);' id='delBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";/*onclick=\"deleteVZhWfCrzjhTj('" + id + "')\"*/

    return result;
}

function addVZhWfCrzjhTj() {
    $("#editModalLabel").text("**新增");
    $("#edit_curdType").val("insert");
    
    //请根据需要修改/删除 需展示字段
    //此处设置字段与修改时相同，是为清空点击修改时为元素的赋值
    //注意：字典类型数据需要dm和mc都进行设置
    $("#edit_wfzfzh").val('');
    $("#edit_rz_wf_wxh").val('');
    $("#edit_rz_wf_wxzh_x1").val('');
    $("#edit_rz_wf_gwhj").val('');
    $("#edit_rz_jydss").val('');
    $("#edit_rz_jycs").val('');
    $("#edit_rz_jyje").val('');
    $("#edit_rz_sdqkje").val('');
    $("#edit_rz_sdqkje1").val('');
    $("#edit_rz_yd_jydss").val('');
    $("#edit_rz_df_sdzjhm_ryb").val('');
    $("#edit_rz_mgsjl").val('');
    $("#edit_rz_zsjyl").val('');
    $("#edit_rz_sdrs").val('');
    $("#edit_rz_df_zjhm_wxglr").val('');
    $("#edit_cz_wf_wxzh_x1").val('');
    $("#edit_cz_wf_gwhj").val('');
    $("#edit_cz_jydss").val('');
    $("#edit_cz_jycs").val('');
    $("#edit_cz_jyje").val('');
    $("#edit_cz_sdqkje").val('');
    $("#edit_cz_sdqkje1").val('');
    $("#edit_cz_yd_jydss").val('');
    $("#edit_cz_df_sdzjhm_ryb").val('');
    $("#edit_cz_mgsjl").val('');
    $("#edit_cz_zsjyl").val('');
    $("#edit_cz_sdrs").val('');
    $("#edit_cz_df_zjhm_wxglr").val('');
    $("#edit_zjycs").val('');
    $("#edit_zjyds").val('');
    $("#edit_zjyje").val('');
    $("#edit_ydjyds").val('');
    $("#edit_df_sdzjhm_ryb").val('');
    $("#edit_df_zjhm_wxglr").val('');
    $("#edit_wf_wxzh_x1").val('');
    $("#edit_wf_gwhj").val('');
    
    $("#editConfirmBtn").on("click",function(){
        //curdVZhWfCrzjhTj();
        vZhWfCrzjhTjEditSubmit();
    });

    $("#editModal").modal();
}


function search() {
    //initvZhWfCrzjhTjGrid();
    $("#vZhWfCrzjhTjGrid").bootstrapTable("refresh");
}
function exportVZhWfCrzjhTj(){
    var form = document.vZhWfCrzjhTjQueryForm;
    var url = ctx+"/wxjyyjmx/vZhWfCrzjhTj/excel";
    form.action=url;
    form.submit();
}

var vZhWfCrzjhTjEditSubmit = function(){
    var flag = $('#vZhWfCrzjhTjEditForm').validate('submitValidate');
    if (flag){
        curdVZhWfCrzjhTj();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}

