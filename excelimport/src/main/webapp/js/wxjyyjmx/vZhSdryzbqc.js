

$(function () {
    initVZhSdryzbqcGrid();
    formEditValidate('vZhSdryzbqcEditForm');//表单验证
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
    $('#editModal').on('hidden.bs.modal', function (event) {
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").removeAttr('disabled');
        $("#editConfirmBtn").removeAttr('disabled','disabled');
        $("#editConfirmBtn").unbind();
        formEditValidateReset('vZhSdryzbqcEditForm');
    });
})

function initVZhSdryzbqcGrid(){
    $('#vZhSdryzbqcGrid').bootstrapTable('destroy');

    $("#vZhSdryzbqcGrid").bootstrapTable({
        method:"get",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        //contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/wxjyyjmx/vZhSdryzbqc/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryVZhSdryzbqcParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
		showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
        //请根据需要修改/删除 需展示字段
            {
                title : "姓名",
                field : "xm",
                align : "center"
            },
            {
                title : "证件号码优化",
                field : "zjhmyh",
                align : "center"
            },
            {
                title : "嫌疑人电话",
                field : "xyrdh",
                align : "center"
            },
            {
                title : "审批时间",
                field : "spsj",
                align : "center"
            },
            {
                title : "人员户籍属地",
                field : "ryhjsd",
                align : "center"
            },
            {
                title : "人员地址",
                field : "rydz",
                align : "center"
            },
            {
                title : "受理单位",
                field : "sldw",
                align : "center"
            },
            {
                title : "备注（案情）",
                field : "aq",
                align : "center"
            },
            {
                title : "案件编号证件号码姓名",
                field : "ajbhzjhm",
                align : "center"
            },
            {
                title : "数据来源",
                field : "sjly",
                align : "center"
            },
            {
                title : "年龄",
                field : "ll",
                align : "center"
            },
            {
                title : "是否港台人员",
                field : "sfgtry",
                align : "center"
            },
            {
                title : "绰号/别名",
                field : "ch",
                align : "center"
            },
            {
                title : "出生日期",
                field : "csrq",
                align : "center"
            },
            {
                title : "QQ",
                field : "qq",
                align : "center"
            },
            {
                title : "国籍",
                field : "gj",
                align : "center"
            },
            {
                title : "收否台湾",
                field : "sftw",
                align : "center"
            },
            {
                title : "证件种类",
                field : "zjzl",
                align : "center"
            },
            {
                title : "户籍地详址",
                field : "hjdxz",
                align : "center"
            },
            {
                title : "微信",
                field : "wx",
                align : "center"
            },
            {
                title : "其他证件级号码",
                field : "qtzjhm",
                align : "center"
            },
            {
                title : "姓名别称",
                field : "xmbc",
                align : "center"
            },
            {
                title : "人员类别中午",
                field : "rylbzw",
                align : "center"
            },
            {
                title : "联系方式电话",
                field : "lxfsdh",
                align : "center"
            },
            {
                title : "是否澳门",
                field : "sfam",
                align : "center"
            },
            {
                title : "是否香港",
                field : "sfxg",
                align : "center"
            },
            {
                title : "最新抓获去重（计算）",
                field : "zxghqc",
                align : "center"
            },
            {
                title : "是否近三年（计算）",
                field : "sfjsn",
                align : "center"
            },
            {
                title : "是否本地（计算）",
                field : "sfbd",
                align : "center"
            },
            {
                title : "人员户籍归属新（计算）",
                field : "ryhjgsx",
                align : "center"
            },
            {
                title : "户籍地详细地址新（计算）",
                field : "hjdxxdzx",
                align : "center"
            },
            {
                title : "港澳台和其他人员标签",
                field : "gathqtrybq",
                align : "center"
            },
            {
                title : "最近抓获情况",
                field : "zjzhqk",
                align : "center"
            },
            {
                title : "是否刑事（计算）",
                field : "sfxs",
                align : "center"
            },

        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function queryVZhSdryzbqcParams(params){
    var tmp = $("#vZhSdryzbqcQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function vZhSdryzbqcOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' id='showBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";/*onclick=\"showVZhSdryzbqc('" + id + "', view='view')\"*/
    result += "<a href='javascript:void(0);' id='editBtn' class='btn' title='编辑'><span class='fa fa-edit fa-lg'></span></a>";/*onclick=\"editVZhSdryzbqc('" + id + "')\"*/
    result += "<a href='javascript:void(0);' id='delBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";/*onclick=\"deleteVZhSdryzbqc('" + id + "')\"*/

    return result;
}

function addVZhSdryzbqc() {
    $("#editModalLabel").text("**新增");
    $("#edit_curdType").val("insert");
    
    //请根据需要修改/删除 需展示字段
    //此处设置字段与修改时相同，是为清空点击修改时为元素的赋值
    //注意：字典类型数据需要dm和mc都进行设置
    $("#edit_xm").val('');
    $("#edit_zjhmyh").val('');
    $("#edit_xyrdh").val('');
    $("#edit_spsj").val('');
    $("#edit_ryhjsd").val('');
    $("#edit_rydz").val('');
    $("#edit_sldw").val('');
    $("#edit_aq").val('');
    $("#edit_ajbhzjhm").val('');
    $("#edit_sjly").val('');
    $("#edit_ll").val('');
    $("#edit_sfgtry").val('');
    $("#edit_ch").val('');
    $("#edit_csrq").val('');
    $("#edit_qq").val('');
    $("#edit_gj").val('');
    $("#edit_sftw").val('');
    $("#edit_zjzl").val('');
    $("#edit_hjdxz").val('');
    $("#edit_wx").val('');
    $("#edit_qtzjhm").val('');
    $("#edit_xmbc").val('');
    $("#edit_rylbzw").val('');
    $("#edit_lxfsdh").val('');
    $("#edit_sfam").val('');
    $("#edit_sfxg").val('');
    $("#edit_zxghqc").val('');
    $("#edit_sfjsn").val('');
    $("#edit_sfbd").val('');
    $("#edit_ryhjgsx").val('');
    $("#edit_hjdxxdzx").val('');
    $("#edit_gathqtrybq").val('');
    $("#edit_zjzhqk").val('');
    $("#edit_sfxs").val('');
    
    $("#editConfirmBtn").on("click",function(){
        //curdVZhSdryzbqc();
        vZhSdryzbqcEditSubmit();
    });

    $("#editModal").modal();
}


function search() {
    //initvZhSdryzbqcGrid();
    $("#vZhSdryzbqcGrid").bootstrapTable("refresh");
}
function exportVZhSdryzbqc(){
    var form = document.vZhSdryzbqcQueryForm;
    var url = ctx+"/wxjyyjmx/vZhSdryzbqc/excel";
    form.action=url;
    form.submit();
}

var vZhSdryzbqcEditSubmit = function(){
    var flag = $('#vZhSdryzbqcEditForm').validate('submitValidate');
    if (flag){
        curdVZhSdryzbqc();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}

