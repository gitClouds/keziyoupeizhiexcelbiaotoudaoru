/**
 * Created by Administrator on 2019/3/20/020.
 */
$(function () {
    initqwjsGrid();
})

function initqwjsGrid() {
    $('#qwjsGrid').bootstrapTable('destroy');

    $("#qwjsGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/qwjs/base/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryqwjsParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
            //请根据需要修改/删除 需展示字段
            {
                title : "序号",
                field : "Number",
                align : "center",
                width : "50px",
                formatter: function (value, row, index) {
                    var pageSize = $('#qwjsGrid').bootstrapTable('getOptions').pageSize;     //通过table的#id 得到每页多少条
                    var pageNumber = $('#qwjsGrid').bootstrapTable('getOptions').pageNumber; //通过table的#id 得到当前第几页
                    return pageSize * (pageNumber - 1) + index + 1;    // 返回每条的序号： 每页条数 *（当前页 - 1 ）+ 序号
                }
            },
            {
                title : "内容",
                field : "nr",
                align : "left"
            },
            {
                title : "操作",
                field : "PK",
                align : "center",
                valign : "middle",
                width : "80px",
                events:qwjsOperateEvents,
                formatter : qwjsOperationFormatter
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function queryqwjsParams(params){
    var tmp = $("#qwjsQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function qwjsOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' id='showBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";
    return result;
}
window.qwjsOperateEvents = {
    "click #showBtn":function (e,vale,row,index) {
        if (row.ly && row.ly ==='wx'){
            var item = {'id':'yaykShowQw'+row.ajid,'name':'一案一库详情','url':ctx+'/yayk/show?pk='+row.ajid};
        }else{
            var item = {'id':'ajxxShow','name':'案件详情','url':ctx+'/gab/ajxx/showPage?pk='+row.ajid};
        }
        iframeTab.parentAddIframe(item);
    }
}

function search() {
	var nr = $("#nr").val().trim();
	if('' == nr){
		alert("请输入需要查询的内容！");
		return;
	}
    $("#qwjsGrid").bootstrapTable("refresh");
}



