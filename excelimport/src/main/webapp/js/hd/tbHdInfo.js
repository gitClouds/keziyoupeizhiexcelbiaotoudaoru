
$(function () {
    initTbHdInfoGrid();
    formEditValidate('tbHdInfoEditForm');//表单验证
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
    $('#editModal').on('hidden.bs.modal', function (event) {
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").removeAttr('disabled');
        $("#editConfirmBtn").removeAttr('disabled','disabled');
        $("#editConfirmBtn").unbind();
        formEditValidateReset('tbHdInfoEditForm');
    });
})

function initTbHdInfoGrid(){
    $('#tbHdInfoGrid').bootstrapTable('destroy');

    $("#tbHdInfoGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/hd/tbHdInfo/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryTbHdInfoParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
		showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
        //请根据需要修改/删除 需展示字段
            {
                title: "序号",
                field: "Number",
                align: "center",
                width: "50px",
                formatter: function (value, row, index) {
                    var pageSize = $('#tbHdInfoGrid').bootstrapTable('getOptions').pageSize;     //通过table的#id 得到每页多少条
                    var pageNumber = $('#tbHdInfoGrid').bootstrapTable('getOptions').pageNumber; //通过table的#id 得到当前第几页
                    return pageSize * (pageNumber - 1) + index + 1;    // 返回每条的序号： 每页条数 *（当前页 - 1 ）+ 序号
                }
            },
            {
                title : "手机号/身份证号",
                field : "hd_hm",
                align : "center"
            },
            {
                title : "户主",
                field : "hd_hz",
                align : "center"
            },
            {
                title : "话单来源",
                field : "hd_ly",
                align : "center",
                formatter : hdlyTransform
            },
            {
                title : "录入人姓名",
                field : "hd_lrr_xm",
                align : "center"
            },
            {
                title : "录入人代码",
                field : "hd_lrr_dm",
                align : "center",
                visible:false
            },
            {
                title : "录入人单位名称",
                field : "hd_lrr_dw_mc",
                align : "center"
            },
            {
                title : "录入人单位代码",
                field : "hd_lrr_dw_dm",
                align : "center",
                visible:false
            },
            {
                title : "录入时间",
                field : "hd_lrsj",
                align : "center"
            },
            {
                title : "操作",
                field : "hd_id",
                align : "center",
                valign : "middle",
                width : "150px",
                events:tbHdInfoOperateEvents,
                formatter : tbHdInfoOperationFormatter
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function queryTbHdInfoParams(params){
    var tmp = $("#tbHdInfoQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function tbHdInfoOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    /*href='"+ctx+"/hd/tbHdJgInfo/page?hd_id="+row.hd_id+"'*/
    result += "<a href='javascript:void(0);' id='showBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";/*onclick=\"showTbHdInfo('" + id + "', view='view')\"*/
    if (row.hd_lrr_dm == jh){
        result += "<a href='javascript:void(0);' id='editBtn' class='btn' title='编辑'><span class='fa fa-edit fa-lg'></span></a>";/*onclick=\"editTbHdInfo('" + id + "')\"*/
        result += "<a href='javascript:void(0);' id='delBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";/*onclick=\"deleteTbHdInfo('" + id + "')\"*/
    }
    return result;
}

function hdlyTransform(value, row, index){
    if (value){
        if(value == "1"){
            return "系统录入";
        }else {
            return "账号查询";
        }
    }else {
        return "";
    }
}

//操作栏绑定事件
window.tbHdInfoOperateEvents = {
    "click #showBtn":function (e,vale,row,index) {
        var item = {'id':'listHdDetailInfo','name':'话单明细信息','url':ctx+"/hd/tbHdJgInfo/page?hd_id="+row.hd_id};
        iframeTab.parentAddIframe(item);
    	/*$("#showModalLabel").text("话单信息详情");
    	//请根据需要修改/删除 需展示字段
        $("#show_hd_id").text(row.hd_id);
        $("#show_hd_hm").text(row.hd_hm);
        $("#show_hd_hz").text(row.hd_hz);
        $("#show_hd_ly").text(row.hd_ly);
        $("#show_hd_lrr_xm").text(row.hd_lrr_xm);
        $("#show_hd_lrr_dm").text(row.hd_lrr_dm);
        $("#show_hd_lrr_dw_mc").text(row.hd_lrr_dw_mc);
        $("#show_hd_lrr_dw_dm").text(row.hd_lrr_dw_dm);
        $("#show_hd_yxx").text(row.hd_yxx);
        $("#show_hd_lrsj").text(row.hd_lrsj);
        $("#show_hd_phone_type").text(row.hd_phone_type);
        $("#show_hd_cx").text(row.hd_cx);
        $("#show_hd_last_cxsj").text(row.hd_last_cxsj);

        $("#showModal").modal();*/
    },
    "click #editBtn":function (e,vale,row,index) {
    	//设置模态框标题
        $("#editModalLabel").text("话单信息修改");
        //设置操作类型
        $("#edit_curdType").val("update");
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").attr('disabled','disabled');

        //请根据需要修改/删除 需展示字段
        //注意：字典类型数据需要dm和mc都进行设置
        $("#edit_hd_id").val(row.hd_id);
        $("#edit_hd_hm").val(row.hd_hm);
        $("#edit_hd_hz").val(row.hd_hz);
        $("#edit_hd_ly").val(row.hd_ly);
        $("#edit_hd_lrr_xm").val(row.hd_lrr_xm);
        $("#edit_hd_lrr_dm").val(row.hd_lrr_dm);
        $("#edit_hd_lrr_dw_mc").val(row.hd_lrr_dw_mc);
        $("#edit_hd_lrr_dw_dm").val(row.hd_lrr_dw_dm);
        $("#edit_hd_yxx").val(row.hd_yxx);
        $("#edit_hd_lrsj").val(row.hd_lrsj);
        $("#edit_hd_phone_type").val(row.hd_phone_type);
        $("#edit_hd_cx").val(row.hd_cx);
        $("#edit_hd_last_cxsj").val(row.hd_last_cxsj);

		//模态框的确认(保存)按钮绑定事件
        $("#editConfirmBtn").on("click",function(){
            //curdTbHdInfo();
            tbHdInfoEditSubmit();
        });
        $("#editModal").modal();
    },
    "click #delBtn":function (e,vale,row,index) {
        $("#tipsModalLabel").html("<i class='fa fa-warning fa-lg' style='color: #c87f0a'></i>警告");//提示标题
        $("#tipsMessage").text("确认删除么？");//设置提示信息
        $("#tipsConfirmBtn").text("删除");//按钮名称
        $("#tipsConfirmBtn").on("click",function(){
            deleteTbHdInfo(row.hd_id);
        });
        $("#tipsModal").modal();
    }
}


function addTbHdInfo() {
    $("#editModalLabel").text("话单信息新增");
    $("#edit_curdType").val("insert");
    
    //请根据需要修改/删除 需展示字段
    //此处设置字段与修改时相同，是为清空点击修改时为元素的赋值
    //注意：字典类型数据需要dm和mc都进行设置
    $("#edit_hd_id").val('');
    $("#edit_hd_hm").val('');
    $("#edit_hd_hz").val('');
    $("#edit_hd_ly").val('');
    $("#edit_hd_lrr_xm").val('');
    $("#edit_hd_lrr_dm").val('');
    $("#edit_hd_lrr_dw_mc").val('');
    $("#edit_hd_lrr_dw_dm").val('');
    $("#edit_hd_yxx").val('');
    $("#edit_hd_lrsj").val('');
    $("#edit_hd_phone_type").val('');
    $("#edit_hd_cx").val('');
    $("#edit_hd_last_cxsj").val('');
    
    $("#editConfirmBtn").on("click",function(){
        //curdTbHdInfo();
        tbHdInfoEditSubmit();
    });

    $("#editModal").modal();
}

function deleteTbHdInfo(id){
    var reqData={"curdType":"delete","hd_id":id};
    $("#tipsModal").modal('toggle');
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/hd/tbHdInfo/curd" ,
        data: reqData,
        success: function (result) {
            alert(result.msg);
            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function curdTbHdInfo() {
	//防止重复点击
    $("#editConfirmBtn").attr('disabled','disabled');
    var reqData = $('#tbHdInfoEditForm').serializeJsonObject();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/hd/tbHdInfo/curd" ,
        data: reqData,
        success : function(data) {
            alert(data.msg);
            $("#editModal").modal('toggle');

            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function search() {
    //inittbHdInfoGrid();
    $("#tbHdInfoGrid").bootstrapTable("refresh");
}
function exportTbHdInfo(){
    var form = document.tbHdInfoQueryForm;
    var url = ctx+"/hd/tbHdInfo/excel";
    form.action=url;
    form.submit();
}

var tbHdInfoEditSubmit = function(){
    var flag = $('#tbHdInfoEditForm').validate('submitValidate');
    if (flag){
        curdTbHdInfo();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}
/*var fileSucc = 0;
var setMongoPk = function(data){
    fileSucc+=1;
    if(data.extra.eff=='fj'){
        $('#fj_mongo').val(data.response.fj_mongo);
    }
    if(fileSucc==2){
        fileSucc = 0;
        curdJgz();
    }
}
function curdJgz() {
    //防止重复点击
    $("#editConfirmBtn").attr('disabled','disabled');
    var reqData = $('#jgzEditForm').serializeJsonObject();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/system/jgz/curd" ,
        data: reqData,
        success : function(data) {
            alert(data.msg);
            $("#editModal").modal('toggle');

            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}*/

