
$(function () {
    initTbHdJgInfoGrid();
    formEditValidate('tbHdJgInfoEditForm');//表单验证
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
    $('#editModal').on('hidden.bs.modal', function (event) {
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").removeAttr('disabled');
        $("#editConfirmBtn").removeAttr('disabled','disabled');
        $("#editConfirmBtn").unbind();
        $("#edit_hd").html('');
        $("#hd_mongo").val('');
        formEditValidateReset('tbHdJgInfoEditForm');
    });
})

function initTbHdJgInfoGrid(){
    $('#tbHdJgInfoGrid').bootstrapTable('destroy');

    $("#tbHdJgInfoGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/hd/tbHdJgInfo/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryTbHdJgInfoParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
		showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
        //请根据需要修改/删除 需展示字段
            {
                title: "序号",
                field: "Number",
                align: "center",
                width: "50px",
                formatter: function (value, row, index) {
                    var pageSize = $('#tbHdJgInfoGrid').bootstrapTable('getOptions').pageSize;     //通过table的#id 得到每页多少条
                    var pageNumber = $('#tbHdJgInfoGrid').bootstrapTable('getOptions').pageNumber; //通过table的#id 得到当前第几页
                    return pageSize * (pageNumber - 1) + index + 1;    // 返回每条的序号： 每页条数 *（当前页 - 1 ）+ 序号
                }
            },
            {
                title : "身份证号/电话号码",
                field : "jg_hm",
                align : "center"
            },
            {
                title : "号码类别",
                field : "jg_hmlb",
                align : "center",
                formatter: function (value, row, index) {
                    if(value){
                        if (value == "01") {
                            return "电话号码";
                        }else {
                            return "身份证号";
                        }
                    }else {
                        return "";
                    }
                }
            },
            {
                title : "运营商类型",
                field : "jg_yyslx",
                align : "center",
                formatter: function (value, row, index) {
                    if(value){
                        if (value == "01") {
                            return "移动";
                        }else if(value == "02"){
                            return "联通";
                        }else if (value == "03"){
                            return "电信";
                        }else {
                            return "广电";
                        }
                    }else {
                        return "";
                    }
                }
            },
            {
                title : "创建时间",
                field : "jg_createtime",
                align : "center"
            },
            {
                title : "文件名称",
                field : "jg_title",
                align : "center"
            },

            {
                title : "录入时间",
                field : "jg_lrsj",
                align : "center"
            },
            {
                title : "操作",
                field : "jg_id",
                align : "center",
                valign : "middle",
                width : "150px",
                events:tbHdJgInfoOperateEvents,
                formatter : tbHdJgInfoOperationFormatter
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function queryTbHdJgInfoParams(params){
    var tmp = $("#tbHdJgInfoQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function tbHdJgInfoOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' id='showBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";/*onclick=\"showTbHdJgInfo('" + id + "', view='view')\"*/
    result += "<a href='javascript:void(0);' style=\"display: none\" id='editBtn' class='btn' title='编辑'><span class='fa fa-edit fa-lg'></span></a>";/*onclick=\"editTbHdJgInfo('" + id + "')\"*/
    result += "<a href='javascript:void(0);' style=\"display: none\" id='delBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";/*onclick=\"deleteTbHdJgInfo('" + id + "')\"*/

    return result;
}
//操作栏绑定事件
window.tbHdJgInfoOperateEvents = {
    "click #showBtn":function (e,vale,row,index) {
    	$("#showModalLabel").text("话单明细详情");
    	//请根据需要修改/删除 需展示字段
        $("#show_jg_id").text(row.jg_id);
        $("#show_hd_id").text(row.hd_id);
        $("#show_jg_hm").text(row.jg_hm);
        $("#show_jg_hmlb").text(row.jg_hmlb);
        $("#show_jg_yyslx").text(row.jg_yyslx);
        if (row.jg_yyslx == '01'){
            $("#show_jg_yyslx").text('移动');
        }else if (row.jg_yyslx == '02') {
            $("#show_jg_yyslx").text('联通');
        }else if (row.jg_yyslx == '03') {
            $("#show_jg_yyslx").text('电信');
        }else {
            $("#show_jg_yyslx").text('广电');
        }
        if (row.jg_hmlb == '01'){
            $("#show_jg_hmlb").text('电话号码');
        } else{
            $("#show_jg_hmlb").text('身份证号');
        }
        $("#show_jg_createtime").text(row.jg_createtime);
        $("#show_jg_evidendesc").text(row.jg_evidendesc);
        $("#show_jg_title").text(row.jg_title);
        $("#show_jg_file_dir").text(row.jg_file_dir);
        $("#show_jg_filepassword").text(row.jg_filepassword);
        $("#show_jg_lrsj").text(row.jg_lrsj);
        $("#show_fj_mong_id").text(row.fj_mong_id);
        if (row.fj_mong_id){
            fileUpload.showImgs([ctx+"/attachment/download?pk="+row.fj_mong_id],'show_hd');
            $(".kv-file-content").remove();
            $(".fileinput-remove").remove();
            $(".btn-outline-secondary").remove();
            $(".file-size-info").css("width", "300px");
            $(".file-size-info").text("文件名称 " +  row.jg_title);
        }
        $("#showModal").modal();
    },
    "click #editBtn":function (e,vale,row,index) {
    	//设置模态框标题
        $("#editModalLabel").text("话单明细修改");
        //设置操作类型
        $("#edit_curdType").val("update");
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").attr('disabled','disabled');
        
        //请根据需要修改/删除 需展示字段
        //注意：字典类型数据需要dm和mc都进行设置
        $("#edit_jg_id").val(row.jg_id);
        $("#edit_hd_id").val(row.hd_id);
        $("#edit_jg_hm").val(row.jg_hm);
        $("#edit_jg_hmlb").val(row.jg_hmlb);
        $("#edit_jg_yyslx").val(row.jg_yyslx);
        $("#edit_jg_createtime").val(row.jg_createtime);
        $("#edit_jg_evidendesc").val(row.jg_evidendesc);
        $("#edit_jg_title").val(row.jg_title);
        $("#edit_jg_file_dir").val(row.jg_file_dir);
        $("#edit_jg_filepassword").val(row.jg_filepassword);
        $("#edit_jg_lrsj").val(row.jg_lrsj);
        $("#edit_fj_mong_id").val(row.fj_mong_id);

		//模态框的确认(保存)按钮绑定事件
        $("#editConfirmBtn").on("click",function(){
            //curdTbHdJgInfo();
            tbHdJgInfoEditSubmit();
        });
        $("#editModal").modal();
    },
    "click #delBtn":function (e,vale,row,index) {
        $("#tipsModalLabel").html("<i class='fa fa-warning fa-lg' style='color: #c87f0a'></i>警告");//提示标题
        $("#tipsMessage").text("确认删除么？");//设置提示信息
        $("#tipsConfirmBtn").text("删除");//按钮名称
        $("#tipsConfirmBtn").on("click",function(){
            deleteTbHdJgInfo(row.jg_id);
        });
        $("#tipsModal").modal();
    }
}

function addTbHdJgInfo() {
    $("#editModalLabel").text("话单明细新增");
    $("#edit_curdType").val("insert");
    //请根据需要修改/删除 需展示字段
    //此处设置字段与修改时相同，是为清空点击修改时为元素的赋值
    //注意：字典类型数据需要dm和mc都进行设置
    $("#edit_jg_id").val('');
    $("#edit_hd_id").val('');
    $("#edit_jg_hm").val('');
    $("#edit_jg_hmlb").val('');
    $("#edit_jg_yyslx").val('');
    $("#edit_jg_createtime").val('');
    $("#edit_jg_evidendesc").val('');
    $("#edit_jg_title").val('');
    $("#edit_jg_file_dir").val('');
    $("#edit_jg_filepassword").val('');
    $("#edit_jg_lrsj").val('');
    $("#edit_fj_mong_id").val('');
    $("#edit_hd").append('<input type="file" class="file" data-show-caption="true" id="hd_file" name="hd" >');
    fileUpload.initImgInput('hd','hd_file',ctx+'/attachment/upload');
    $("#editConfirmBtn").on("click",function(){
        //curdTbHdJgInfo();
        tbHdJgInfoEditSubmit();
    });

    $("#editModal").modal();
}

function uploadFile() {
    $('#hd_file').fileinput('upload');
}

function deleteTbHdJgInfo(id){
    var reqData={"curdType":"delete","jg_id":id};
    $("#tipsModal").modal('toggle');
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/hd/tbHdJgInfo/curd" ,
        data: reqData,
        success: function (result) {
            alert(result.msg);
            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function curdTbHdJgInfo() {
	//防止重复点击
    $("#editConfirmBtn").attr('disabled','disabled');
    var reqData = $('#tbHdJgInfoEditForm').serializeJsonObject();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/hd/tbHdJgInfo/curd" ,
        data: reqData,
        success : function(data) {
            alert(data.msg);
            $("#editModal").modal('toggle');

            search();
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function search() {
    //inittbHdJgInfoGrid();
    $("#tbHdJgInfoGrid").bootstrapTable("refresh");
}
function exportTbHdJgInfo(){
    var form = document.tbHdJgInfoQueryForm;
    var url = ctx+"/hd/tbHdJgInfo/excel";
    form.action=url;
    form.submit();
}

var tbHdJgInfoEditSubmit = function(){
    var flag = $('#tbHdJgInfoEditForm').validate('submitValidate');
    if (flag){
        uploadFile();
        $("#jg_title").val($(".file-caption-name").attr("title"));
        // curdTbHdJgInfo();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}

var fileSucc = 0;
var setMongoPk = function(data){
    fileSucc+=1;
    if(data.extra.eff=='hd'){
        $('#hd_mongo').val(data.response.hd_mongo);
    }
    if(fileSucc==1){
        fileSucc = 0;
        curdTbHdJgInfo();
    }
}
