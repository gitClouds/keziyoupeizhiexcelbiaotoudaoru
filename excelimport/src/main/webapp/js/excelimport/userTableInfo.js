var insertNewRowData = null;

$(function () {
    /*initUserTableInfoGrid();*/
    formEditValidate('userTableInfoEditForm');//表单验证
    //当模态框隐去的时候取消绑定的事件，下一次再动态绑定
    $('#tipsModal').on('hidden.bs.modal', function (event) {
        $("#tipsConfirmBtn").unbind();
    });
    $('#editModal').on('hidden.bs.modal', function (event) {
        //修改时，若有不让修改的字段，请在edit时加入disabled属性，并在modal消失时去除此disabled属性
        //$("#edit_password").removeAttr('disabled');
        $("#editConfirmBtn").removeAttr('disabled','disabled');
        $("#editConfirmBtn").unbind();
        formEditValidateReset('userTableInfoEditForm');
    });
})

function initUserTableInfoGrid(objCol){
    $('#userTableInfoGrid').bootstrapTable('destroy');

    $("#userTableInfoGrid").bootstrapTable({
        method:"get",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        //contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/excelimport/userTableInfo/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 20,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryUserTableInfoParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
		showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :objCol,
        responseHandler:responseHandler,//请求数据成功后，渲染表格前的方法
        /*onLoadSuccess:onLoadOptionEvent,*/
        /*rowStyle:rowStyle,//通过自定义函数设置行样式*/
        onEditableSave:editableSaveData
    });
}
function bootstrapCOls(result) {
    var colArray = [];
    var headerJsonBox = {};
    var headerJsonOrder = {};
    headerJsonBox.checkbox=true;
    headerJsonBox.visible=true;
    headerJsonBox.align='center';
    headerJsonOrder.title = "序号";
    headerJsonOrder.field = "Number";
    headerJsonOrder.align = "center";
    headerJsonOrder.formatter = xhPageOrder;
    colArray.push(headerJsonBox);
    colArray.push(headerJsonOrder);
    $.each(result, function (index, item) {
        var colJson = {};
        var num = (index+1)+'.';
        var col_name = "";
        if (item.COMMENTS){
            col_name = item.COMMENTS+"("+item.COLUMN_NAME+")";
            /*col_name = item.COMMENTS;*/
        } else{
            col_name = item.COLUMN_NAME;
        }
        /*判断哪些字段需要隐藏*/
        /*if (item.COLUMN_NAME == "ID") {
            colJson.visible = false;
        }
        if (item.COLUMN_NAME == "FIELD_KIND") {
            colJson.visible = false;
        }
        if (item.COLUMN_NAME == "STATE") {
            colJson.visible = false;
        }
        if (item.COLUMN_NAME == "CREATE_TIME") {
            colJson.visible = false;
        }*/
        colJson.title=col_name;
        colJson.field=item.COLUMN_NAME;
        colJson.align="center";
        colJson.cellStyle = setGridCss;
        colJson.formatter = deleteHGNull;
        /*colJson.formatter = gridOption;*/
        colArray.push(colJson);
    });
    return colArray;
}
// 以起始页码方式传入参数,params为table组装参数
function queryUserTableInfoParams(params){
    var tmp = $("#userTableInfoQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    tmp["table_name"]=$("#tableName").val();
    tmp["sqlType"]=sql_statement_type;
    tmp["function_type"]=$("#saveFunctionType").val();
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function userTableInfoOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    result += "<a href='javascript:void(0);' id='showBtn' class='btn' title='查看'><i class='fa fa-search fa-lg'></i></a>";/*onclick=\"showUserTableInfo('" + id + "', view='view')\"*/
    result += "<a href='javascript:void(0);' id='editBtn' class='btn' title='编辑'><span class='fa fa-edit fa-lg'></span></a>";/*onclick=\"editUserTableInfo('" + id + "')\"*/
    result += "<a href='javascript:void(0);' id='delBtn' class='btn' title='删除'><span class='fa fa-trash-o fa-lg'></span></a>";/*onclick=\"deleteUserTableInfo('" + id + "')\"*/

    return result;
}

function addUserTableInfo() {
    $("#editModalLabel").text("**新增");
    $("#edit_curdType").val("insert");
    
    //请根据需要修改/删除 需展示字段
    //此处设置字段与修改时相同，是为清空点击修改时为元素的赋值
    //注意：字典类型数据需要dm和mc都进行设置
    $("#edit_id").val('');
    $("#edit_table_name").val('');
    $("#edit_table_type").val('');
    $("#edit_comments").val('');
    
    $("#editConfirmBtn").on("click",function(){
        //curdUserTableInfo();
        userTableInfoEditSubmit();
    });

    $("#editModal").modal();
}


function search() {
    //inituserTableInfoGrid();
    $("#userTableInfoGrid").bootstrapTable("refresh");
}
function exportUserTableInfo(){
    var form = document.userTableInfoQueryForm;
    var url = ctx+"/excelimport/userTableInfo/excel";
    form.action=url;
    form.submit();
}

var userTableInfoEditSubmit = function(){
    var flag = $('#userTableInfoEditForm').validate('submitValidate');
    if (flag){
        curdUserTableInfo();
    }else{
        alert('信息校验不通过，请根据提示填写相应内容');
    }
}

var xhPageOrder = function (value, row, index) {
    var pageSize = $('#userTableInfoGrid').bootstrapTable('getOptions').pageSize;     //通过table的#id 得到每页多少条
    var pageNumber = $('#userTableInfoGrid').bootstrapTable('getOptions').pageNumber; //通过table的#id 得到当前第几页
    return pageSize * (pageNumber - 1) + index + 1;    // 返回每条的序号： 每页条数 *（当前页 - 1 ）+ 序号
}
var setGridCss = function (value,row,index,field) {
    if (value && value.length > 60){
        return {
            css:{
                "overflow": "hidden",
                "text-overflow": "ellipsis",
                "white-space": "nowrap",
                "max-width": "150px"
            }
        }
    }
    if(field == "TASK_SCHEDULE"){
        if (value == "1"){
            return {
                css:{
                    "background-color": "#2ccaff"
                }
            }
        }
        if (value == "2"){
            return {
                css:{
                    "background-color": "#38f737"
                }
            }
        }
        if (value == "3"){
            return {
                css:{
                    "background-color": "#ff5659"
                }
            }
        }
    }
    return {};
}

/*function rowStyle(row, index) {
    var style = {};
    style={css:{'color':'#ed5565'}};
    return style;
}*/

/*
var gridOption = function (value, row, index) {
    var eleContent = null;
    if (value && value.length > 60){
        eleContent = '<span data-toggle="tooltip" data-placement="right" title="'+value+'">'+value+'</span>';
    }else {
        eleContent = value;
    }
    return eleContent;
}
*/

function deleteHGNull(value, row, index) {
    if (value){
        return value;
    } else {
        return "";
    }
}

/*最终让内容全显示*/
/*function onLoadOptionEvent() {
    var count = getAllRowsData().length;
    for (let i = 0; i < count; i++) {
        var tdObjs = $('#userTableInfoGrid tr[data-index="'+i+'"] td');
        for (let j = 0; j < tdObjs.length ; j++) {
            var value = $(tdObjs[j]).text();
            /!*启动行内编辑功能*!/
            editCell(value, $(tdObjs[j]));
            /!*下面是内容太多全显示内容*!/
            /!*if (value.length > 60){
                var tdObj = tdObjs[j];
                $(tdObj).attr("data-toggle","tooltip");
                $(tdObj).attr("data-placement","right");
                $(tdObj).attr("title",value);
            }*!/
        }
    }
}*/

/*获取所有行数据*/
function getAllRowsData() {
    var allRowsData = $('#userTableInfoGrid').bootstrapTable('getData');
    return allRowsData;
}
function insertRowData(sub,v,fileName){
    var tableHeadObjs = $('#userTableInfoGrid thead tr th');
    tableHeadObjs.splice(0, 2);
    var trObjs = $('#userTableInfoGrid tbody tr');
    var updateRow = trObjs.length - 1;
    var lsttrObj = trObjs[updateRow];
    var tdObjs = $(lsttrObj).children("td");
    tdObjs.splice(0,2);
    var rowsV = {}
    /*解决有复选框，有序号时新增行时清空上一行问题*/
    for (var i = 0; i < tdObjs.length; i++) {
        var newTdValue = $(tdObjs[i]).text();
        for (var j = 0; j < tableHeadObjs.length; j++) {
            var col_name = $(tableHeadObjs[j]).attr("data-field");
            if (i==j){
                rowsV[col_name] = newTdValue;
            }
        }
    }
    if (rowsV) {
        $('#userTableInfoGrid').bootstrapTable('updateRow', {
            index: updateRow,
            row: rowsV
        })
    }
    var orderRow = getAllRowsData().length + 1;
    var inserNewKeyValue = getOracleColumnName(sub,v,fileName);
    if (orderRow && inserNewKeyValue){
        $('#userTableInfoGrid').bootstrapTable('insertRow', {
            index: orderRow,
            row: inserNewKeyValue
        })
    }

}

/*从数据库中获取表头字段名*/
function getOracleColumnName(sub,v,fileName){
    var rows = {};
    if (column_names && column_names.length > 0) {
        $.each(column_names, function (index, item) {
            var col_name = item.COLUMN_NAME;
            if (v){
                if (col_name == "TABLE_NAME") {
                    rows[col_name] = pinyin.getCamelChars(fileName).substring(0,pinyin.getCamelChars(fileName).lastIndexOf("."));
                }
                if (col_name == "TABLE_CHINA_NAME") {
                    rows[col_name] = fileName;
                }
                if (col_name == "FIELD_NAME"){
                    rows[col_name] = pinyin.getCamelChars(v) + "_" + sub;
                }
                if (col_name == "FIELD_CHINA_NAME"){
                    rows[col_name] = v;
                }
                if (col_name == "FIELD_TYPE") {
                    rows[col_name] = "VARCHAR2";
                }
                if (col_name == "FIELD_LENTH"){
                    rows[col_name] = "200";
                }
            }else {
                rows[col_name] = "";
            }
        })
    }
    return rows;
}

function deleteRowData(){
    var trObjs = $('#userTableInfoGrid tbody tr[class="selected"]');
    if (trObjs.length > 0){
        for (let i = 0; i < trObjs.length; i++) {
            $(trObjs[i]).remove();
        }
    }
    /*var rowAllArray = $('#userTableInfoGrid').bootstrapTable('getData');*/
    /*var rowArray = $('#userTableInfoGrid').bootstrapTable('getSelections');
    var ids = [];
    for (let i = 0; i < rowArray.length; i++) {
        var id = rowArray[i][0];
        ids.push(id);
    }
    $('#userTableInfoGrid').bootstrapTable('remove', {
        field: '0',
        values: ids
    })*/
}

function deleteAllRowData() {
    var trObjs = $('#userTableInfoGrid tbody tr');
    if (trObjs.length > 0){
        for (let i = 0; i < trObjs.length ; i++) {
            $(trObjs[i]).remove();
        }
    }
    /*$('#userTableInfoGrid').bootstrapTable('removeAll')*/
}
function editableSaveData(field, row, oldValue, $element) {
    if (row){
        $.ajax({
            type: "post",
            url: ctx+"/excelimport/userTableInfo/save_edit_data",
            data: row,
            dataType: 'JSON',
            success: function (data, status) {
                if (status == "success") {
                    alert('提交数据成功');
                }
            },
            error: function () {
                alert('编辑失败');
            },
            complete: function () {

            }

        });
    }
}

/*
function editCell(value, $element) {
    var newValue = "";
    var rowsValues = {};
    if (value && value.length > 60) {
        newValue = "textarea";
    }else {
        newValue = "text";
    }
    $element.editable({
        type: newValue,                //编辑框的类型。支持text|textarea|select|date|checklist等
        title: "用户名",              //编辑框的标题
        disabled: false,             //是否禁用编辑
        emptytext: "",              //空值的默认文本
        mode: "inline"             //编辑框的模式：支持popup和inline两种模式，默认是popup
        /!*validate: function (value) { //字段验证
            if (!$.trim(value)) {
                return '不能为空';
            }
        }*!/
    });
}
*/
function getAllData() {
    var tableHeadObjs = $('#userTableInfoGrid thead tr th');
    tableHeadObjs.splice(0, 2);
    var trObjs = $('#userTableInfoGrid tbody tr');
    var saverows = [];
    if (trObjs.length > 0) {
        for (let i = 0; i < trObjs.length ; i++) {
            var tdObjs = $(trObjs[i]).children("td");
            tdObjs.splice(0, 2);
            var rowTr = {};
            for (let j = 0; j <tdObjs.length ; j++) {
                for (let k = 0; k < tableHeadObjs.length ; k++) {
                    if (j == k) {
                        rowTr[$(tableHeadObjs[k]).attr("data-field")]=$(tdObjs[j]).text();
                    }
                }
            }
            saverows.push(rowTr)
        }
    }
    return saverows;
}
function getSeletionData() {
    var tableHeadObjs = $('#userTableInfoGrid thead tr th');
    tableHeadObjs.splice(0, 2);
    var trObjs = $('#userTableInfoGrid tbody tr[class="selected"]');
    var saveselectedrows = [];
    if (trObjs.length){
        for (let i = 0; i < trObjs.length ; i++) {
            var tdObjs = $(trObjs[i]).children("td");
            tdObjs.splice(0, 2);
            var rowTr = {};
            for (let j = 0; j <tdObjs.length ; j++) {
                for (let k = 0; k < tableHeadObjs.length ; k++) {
                    if (j == k) {
                        rowTr[$(tableHeadObjs[k]).attr("data-field")]=$(tdObjs[j]).text();
                    }
                }
            }
            saveselectedrows.push(rowTr)
        }
    }
    return saveselectedrows;
}
function getAllDataTo() {
    var data = getAllData();
    if (data.length > 0){
        var table_name = "";
        if (data[0].TABLE_NAME) {
            table_name = data[0].TABLE_NAME;
        }
        var table_china_name = "";
        if (data[0].TABLE_CHINA_NAME) {
            table_china_name = data[0].TABLE_CHINA_NAME;
        }
        var own_tablespace = "";
        if (data[0].OWN_TABLESPACE){
            own_tablespace = data[0].OWN_TABLESPACE;
        }
        $.each(data, function (index, item) {
            item.TABLE_NAME = table_name;
            item.TABLE_CHINA_NAME = table_china_name;
            item.OWN_TABLESPACE = own_tablespace;
            var field_order = index+1;
            item.FIELD_ORDER = field_order;
        })
    }
    return data;
}
function saveAllRowData() {
    var saverows = getAllData();
    var table_name_one = $("#tableName").val();
    if (table_name_one == "FIELD_PROPERTY_INFO_TB") {
        saverows = getAllDataTo();
    }
    if (saverows.length > 0){
        $.ajax({
            type: "post",
            url: ctx+"/excelimport/userTableInfo/save_edit_data",
            data: {"saverows":JSON.stringify(saverows), "table_name": $("#tableName").val()},
            dataType: 'JSON',
            success: function (data) {
                if (data.code == "1") {
                    search();
                    alert('提交数据成功');
                }else {
                    alert('提交数据失败，原因:'+data.msg);
                }
                $("#createTab").show();
                queryTableInfo();
                querySqlBatchTypeNameInfo();
            },
            error: function () {
                alert('编辑失败');
            },
            complete: function () {

            }

        });
    }
}

function deleteSelectionRowData(){
    var saverows = getSeletionData();

    if (saverows.length > 0){
        $.ajax({
            type: "post",
            url: ctx+"/excelimport/userTableInfo/delete_tb_data",
            data: {"saverows":JSON.stringify(saverows), "table_name": $("#tableName").val()},
            dataType: 'JSON',
            success: function (data) {
                if (data.code == "1") {
                    search();
                    alert('删除数据成功');
                }else {
                    alert('删除数据失败，原因:'+data.msg);
                }
            },
            error: function () {
                alert('删除失败');
            },
            complete: function () {

            }

        });
    }
}

function getFieldDicValue(dic_type){
    var result = null;
    if (dic_type){
        $.ajax({
            type: "POST",
            url: ctx+"/excelimport/userTableInfo/get_field_property_dic",
            data: {"dic_type":dic_type},
            async: false,
            dataType: 'JSON',
            success: function (data) {
                if (data.length > 0){
                    result = data;
                }
            }
        });
    }
   return result;
}

function getRandom() {
    var charactors="AB1CD2EF3GH4IJ5KL6MN7OPQ8RST9UVW0XYZ";
    var value='',i;
    for(j=1;j<=4;j++){
        i = parseInt(35*Math.random());
        value = value + charactors.charAt(i);
    }
    return value;
}
function getCreateTableData() {
    var objs = {};
    var data = getAllData();
    var sql = "";
    if (data && data.length > 0){
        var data = getAllData();
        var table_name = data[0].TABLE_NAME.toLocaleUpperCase();
        var table_space = data[0].OWN_TABLESPACE;
        var table_china_name = data[0].TABLE_CHINA_NAME;
        var field_names = "";
        var field_china_name_sql = "";
        var pks_sql = "";
        var indexs_sql = "";
        $.each(data, function (index, item) {
            var field_length = "";
            if (item.FIELD_LENTH) {
                field_length = "("+item.FIELD_LENTH+")";
            }
            field_names += item.FIELD_NAME + " " + item.FIELD_TYPE + field_length + " " + item.ISNULL + ",";
            if (item.IS_PK && item.IS_PK == "是") {
                pks_sql = " alter table "+table_name+" add constraint PK_"+getRandom()+" primary key ("+item.FIELD_NAME+");";
            }
            if (item.IS_INDEX && item.IS_INDEX == "是"){
                indexs_sql += "create index INDEX_"+getRandom()+" on "+table_name+" ("+item.FIELD_NAME+") tablespace "+table_space+" pctfree 10 initrans 2 maxtrans 255 storage ( initial 64K next 1M minextents 1 maxextents unlimited );";
            }
            if (item.FIELD_CHINA_NAME) {
                field_china_name_sql += "comment on column "+table_name+"."+item.FIELD_NAME+" is '"+item.FIELD_CHINA_NAME+"';";
            }

        })
        var field_names_sql = "("+field_names.substring(0,field_names.length-1)+")";
        var table_space_sql = ";";
        if (table_space) {
            table_space_sql = "tablespace "+table_space+" pctfree 10 initrans 1 maxtrans 255 storage ( initial 64K next 1M minextents 1 maxextents unlimited );";
        }
        var table_china_name_sql = "";
        if (table_china_name){
            table_china_name_sql = "comment on table "+table_name+" is '"+table_china_name+"';";
        }
        sql = " create table "+table_name+field_names_sql+table_space_sql+table_china_name_sql+field_china_name_sql+pks_sql+indexs_sql;

    }
    objs.sqlKey = sql;
    objs.table_nameKey = table_name;
    return objs;
}

function createTableExcute(sql,table_name,isnull) {
    $.ajax({
        type: "post",
        url: ctx+"/excelimport/userTableInfo/create_table",
        data: {"sql":sql,"table_name":table_name,"isnull":isnull},
        dataType: 'JSON',
        success: function (data) {
            if (data.code == "1") {
                alert('创建成功');
            }else {
                alert('创建失败，原因:'+data.msg);
            }
            $("#createTab").hide();
        },
        error: function () {
            alert('删除失败');
        },
        complete: function () {
        }

    });
}
function createTab(){
    var objs = getCreateTableData();
    var sql = objs.sqlKey;
    var table_name = objs.table_nameKey;
    if (sql){
        $.ajax({
            type: "post",
            url: ctx+"/excelimport/userTableInfo/query_create_name_isnull",
            data: {"table_name":table_name},
            dataType: 'JSON',
            success: function (data) {
                var isnull = data.code;
                if (isnull == "1") {
                    var flag=confirm("表已存在，是否删除原表继续创建？");
                    if (flag){
                        createTableExcute(sql,table_name,isnull);
                    }else {
                        return;
                    }
                }else {
                    createTableExcute(sql,table_name,isnull);
                }
            },
            error: function () {
                alert('查询失败');
            },
            complete: function () {
            }

        });

    }
}

function queryTableInfo(){
    $("#selection_edit_table").empty();
    $.ajax({
        type: "post",
        url: ctx+"/excelimport/userTableInfo/query_create_name_info",
        dataType: 'JSON',
        success: function (data) {
            var list = data.list;
            if (list){
                var op = '<option value="null" selected="selected">请选择表</option>';
                $.each(list, function (index, item) {
                    var comment_name_c = "";
                    if (item.TABLE_CHINA_NAME){
                        comment_name_c = item.TABLE_CHINA_NAME+"||"+item.TABLE_NAME;
                    }else {
                        comment_name_c = item.TABLE_NAME;
                    }
                    op += '<option value="'+item.TABLE_NAME+'">'+comment_name_c+'</option>'
                });
                $("#selection_edit_table").append(op);
            }
        },
        error: function () {
            alert('查询失败');
        },
        complete: function () {
        }

    });
}

function querySqlBatchTypeNameInfo(){
    $("#selection_task_type").empty();
    $.ajax({
        type: "post",
        url: ctx+"/excelimport/userTableInfo/query_sql_batch_type_name_info",
        dataType: 'JSON',
        success: function (data) {
            var list = data.list;
            if (list){
                var op = '<option value="" selected="selected">请选择批次/任务种类</option>';
                $.each(list, function (index, item) {
                    var comment_name_c = "";
                    if (item.TASK_TYPE_NAME){
                        comment_name_c = item.TASK_BATCH+"||"+item.TASK_TYPE_NAME;
                    }else {
                        comment_name_c = item.TASK_BATCH;
                    }
                    op += '<option value="'+item.TASK_BATCH+'">'+comment_name_c+'</option>'
                });
                $("#selection_task_type").append(op);
            }
        },
        error: function () {
            alert('查询失败');
        },
        complete: function () {
        }

    });
}

function queryCreateTablePropetry(v){
    if (v){
        search();
    }
}
function querySqlInfo(v) {
        search();
}
function getSelection() {
    var area = document.getElementById("runSqlArea");
    var selstart, selend, seltext;
    if (area.selectionStart) {
        selstart = area.selectionStart;
        selend = area.selectionEnd;
        seltext = area.value.substring(selstart, selend);
    } else {
        var range1 = document.selection.createRange();
        if (range1.parentElement() != area) return;
        var range2 = range1.duplicate();
        range2.moveToElementText(area);
        range2.setEndPoint('EndToEnd', range1);
        selstart = range2.text.length - range1.text.length;
        selend = selstart + range1.text.length;
        seltext = range1.text;
    }
    return {
        selstart: selstart,
        selend: selend,
        seltext: seltext
    };
}

function runSql(){
    if (username != "thunis"){
        $("#resultSqlArea").text("无此权限");
        return;
    }
    var sql = "";
    var sqlObj = getSelection();
    sql = sqlObj.seltext;
    if (!sql) {
        sql = $("#runSqlArea").val();
    }
    if (sql){
        $.ajax({
            type: "post",
            url: ctx+"/excelimport/userTableInfo/run_sql",
            data: {"sql":sql},
            dataType: 'JSON',
            success: function (data) {
                if (data.code == "1"){
                    if (data.msg){
                        $("#resultSqlArea").text(data.msg);
                    }else {
                        $("#resultSqlArea").text("sql执行成功");
                    }
                } else {
                    $("#resultSqlArea").text("执行失败,失败原因:" + data.msg);
                }
            },
            error: function () {
                alert('执行出错');
            },
            complete: function () {
            }

        });
    }
}
$(function () {
    $('#userTableInfoGrid').on('click-cell.bs.table', function (e,field, value,  row, $element) {
        var table_name = $("#tableName").val();
        if (table_name == "FIELD_PROPERTY_INFO_TB" || table_name == "SQL_SCRIPT_INFO_TAB" || table_name == "FIELD_PROPERTY_DIC_TAB"){
            var newValue = "";
            var flag = false;
            var emptytext = "";
            var dic_type = "";
            if (field == "FIELD_TYPE") {
                dic_type = "1";
            }
            if (field == "ISNULL"){
                dic_type = "2";
            }
            if (field == "OWN_TABLESPACE"){
                dic_type = "3";
            }
            var result = null;
            if (field == "FIELD_TYPE" || field == "ISNULL" || field == "OWN_TABLESPACE"){
                result = getFieldDicValue(dic_type);
                result.unshift({value: '', text: ''});
            }
            if(field == "IS_PK" || field == "IS_INDEX"){
                result = [{value: '', text: ''},{value: '是', text: '是'}];
            }
            var rowsValues = {};
            if (value && value.length > 60) {
                newValue = "textarea";
            }else {
                newValue = "text";
            }
            if (field == "SQL_STATEMENT") {
                newValue = "textarea";
            }
            if (field == "Number"){
                flag = true;
            }
            if (field == "ID"){
                flag = true;
            }
            if (field == "STATE"){
                flag = true;
            }
            if (field == "CREATE_TIME"){
                flag = true;
            }
            if (field == "FIELD_TYPE") {
                emptytext = ""
            }
            if (field == "FIELD_TYPE" || field == "ISNULL" || field == "OWN_TABLESPACE" || field == "IS_PK" || field == "IS_INDEX"){
                newValue = "select";
            }
            $element.editable({
                type: newValue,  //编辑框的类型。支持text|textarea|select|date|checklist等
                source: result,
                title: field,              //编辑框的标题
                disabled: flag,             //是否禁用编辑
                emptytext: emptytext,              //空值的默认文本
                mode: "inline"             //编辑框的模式：支持popup和inline两种模式，默认是popup
                /*validate: function (value) { //字段验证
                    if (!$.trim(value)) {
                        return '不能为空';
                    }
                }*/
            });
        }
    })
})