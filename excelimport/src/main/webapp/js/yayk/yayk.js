/**
 * Created by Administrator on 2019/3/20/020.
 */
$(function () {
    initYaykGrid();
})

function initYaykGrid() {
    $('#yaykGrid').bootstrapTable('destroy');

    $("#yaykGrid").bootstrapTable({
        method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/yayk/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [10,20,50,100],
        pageSize: 10,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryYaykParams,//参数
        showColumns: true,//是否显示所有的列（选择显示的列）
        showRefresh: true,//是否显示刷新按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 590,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        toolbar: '#toolbar',              //工具按钮用哪个容器
        toolbarAlign:"left",
        showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
        //得到查询的参数
        columns :[
            //请根据需要修改/删除 需展示字段
            {
                title : "序号",
                field : "Number",
                align : "center",
                width : "50px",
                formatter: function (value, row, index) {
                    var pageSize = $('#yaykGrid').bootstrapTable('getOptions').pageSize;     //通过table的#id 得到每页多少条
                    var pageNumber = $('#yaykGrid').bootstrapTable('getOptions').pageNumber; //通过table的#id 得到当前第几页
                    return pageSize * (pageNumber - 1) + index + 1;    // 返回每条的序号： 每页条数 *（当前页 - 1 ）+ 序号
                }
            },
            {
                title : "账号",
                field : "zh",
                align : "center"
            },
            {
                title : "户主姓名",
                field : "xm",
                align : "center"
            },
            {
                title : "账号所属机构名称",
                field : "jgmc",
                align : "center"
            },
            {
                title : "转账时间",
                field : "zzsj",
                align : "center",
                formatter: function (value, row, index) {
                    return value?new Date(value).Format("yyyy-MM-dd hh:mm:ss"):"";
                }
            },
            {
                title : "转账金额",
                field : "zzje",
                align : "center"
            },
            {
                title : "录入人姓名",
                field : "lrrxm",
                align : "center"
            },
            {
                title : "分析时间",
                field : "fxsj",
                align : "center",
                formatter: function (value, row, index) {
                    return value?new Date(value).Format("yyyy-MM-dd hh:mm:ss"):"";
                }
            },
            {
            	title : "层级",
            	field : "cj",
            	align : "center"
            },
            {
                title : "操作",
                field : "PK",
                align : "center",
                valign : "middle",
                width : "150px",
                events:yaykOperateEvents,
                formatter : yaykOperationFormatter
            }
        ],
        responseHandler:responseHandler//请求数据成功后，渲染表格前的方法
    });
}

// 以起始页码方式传入参数,params为table组装参数
function queryYaykParams(params){
    var tmp = $("#yaykQueryForm").serializeJsonObject();
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    return tmp;
}
function responseHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//操作栏的格式化
function yaykOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    if (row.cj>=0){
        result += "<a href='javascript:void(0);' id='showBtn' class='btn' title='详情'><i class='fa fa-search fa-lg'></i></a>";/*onclick=\"showJjd('" + id + "', view='view')\"*/
    }
    return result;
}
window.yaykOperateEvents = {
    "click #showBtn":function (e,vale,row,index) {
        var item = {'id':'yaykShow','name':'一案一库详情','url':ctx+'/yayk/show?pk='+row.id};
        iframeTab.parentAddIframe(item);
    }
}

function search() {
    $("#yaykGrid").bootstrapTable("refresh");
}

function exportYayk(){
    var form = document.yaykQueryForm;
    var url = ctx+"/yayk/excel";
    form.action=url;
    form.submit();
}
function switchZfCxDjTab(cType) {
    $("#cxType").val(cType);
    zfcxdjGrid();
}
function zfcxdjGrid() {

    $('#zfcxdjGrid').bootstrapTable('destroy');

    $("#zfcxdjGrid").bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/znzf/base/list',//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [5,10,20,50,100,200,500,1000],
        pageSize: 500,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryYhAndSfParams,//参数
        showColumns: false,//是否显示所有的列（选择显示的列）
        showRefresh: false,//是否显示刷新按钮
        showToggle: false,//是否显示详细视图和列表视图的切换按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 250,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: true,//是否启用点击选中行
        smartDisplay: true,//智能显示分页或卡视图
        //toolbar: '#toolbar',//工具按钮用哪个容器
        toolbarAlign:"left",
        //得到查询的参数
        columns :[
            //请根据需要修改/删除 需展示字段
            {
                title : "序号",
                field : "Number",
                align : "center",
                width : "50px",
                formatter: function (value, row, index) {
                    var pageSize = $('#zfcxdjGrid').bootstrapTable('getOptions').pageSize;     //通过table的#id 得到每页多少条
                    var pageNumber = $('#zfcxdjGrid').bootstrapTable('getOptions').pageNumber; //通过table的#id 得到当前第几页
                    return pageSize * (pageNumber - 1) + index + 1;    // 返回每条的序号： 每页条数 *（当前页 - 1 ）+ 序号
                }
            },
            {
                title : "时间",
                field : "REQDATE",
                align : "center",
                formatter: function (value, row, index) {
                    return value?new Date(value).Format("yyyy-MM-dd hh:mm:ss"):"";
                }
            },
            {
                title : "姓名",
                field : "ACCOUNTNAME",
                align : "center"
            },
            {
                title : "银行/机构",
                field : "BANKNAME",
                align : "center"
            },
            {
                title : "账/卡号",
                field : "CARDNUMBER",
                align : "center"
            },
            {
                title : "余额",
                field : "YE",
                align : "center"
            },
            {
                title : "请求类型",
                field : "QQLX",
                align : "center"
            },
            {
                title : "是否反馈",
                field : "RESFLAG",
                align : "center",
                formatter:function (value, row, index) {
                    if (value=='1'){
                        return '已反馈';
                    }else if(value=='2'){
                        return '反馈失败';
                    }else{
                        return '未反馈';
                    }
                }
            },
            {
                title : "反馈结果",
                field : "RESULTCODE",
                align : "center"
            },
            {
                title : "录入单位",
                field : "LRDWMC",
                align : "center"
            }
        ],
        responseHandler:responseYhAndSfHandler//请求数据成功后，渲染表格前的方法
    });
}
// 以起始页码方式传入参数,params为table组装参数
function queryYhAndSfParams(params){
    var tmp = $("#yaykQueryForm1").serializeJsonObject() ;
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    tmp["cType"]=$("#cxType").val();
    tmp["jjdpk"]=$("#jjdpk").val();
    return tmp;
}

function responseYhAndSfHandler(result){
    //如果没有错误则返回数据，渲染表格
    return {
        total : result.total, //总页数,前面的key必须为"total"
        rows : result.rows //行数据，前面的key要与之前设置的dataField的值一致.
    };
}
//type：0为资金；1为人员
//iszcygzs:1为侦查员工作室
function searchLevel(type,jjdpk,iszcygzs){
    $('#addTable').html('');
    $.ajax({
        type:"GET",
        url:ctx+'/znzf/jlt/getJltLevel?jjdPk='+jjdpk+"&iszcygzs="+iszcygzs,
        async:false,
        dataType:"text",
        success:function (data) {
            if (iszcygzs>=1){
                for (var i=1;i<=data;i++){
                    //console.log(i)
                    var title="";
                    title = '步骤'+convertToChinese(i);

                    var str="<h5 style=\"color:#00c0ef;padding: 10px;\">"+title+"</h5><table id=\"zcGrid"+i+ "\" class=\"table table-bordered table-striped table-condensed\"> ";
                    $("#addTable").append(str);
                    zcZjlGrids(type,"zcGrid"+(parseInt(iszcygzs)+i-1),i,(parseInt(iszcygzs)+i-1));
                }
            }else{
                for (var i=1;i<=data;i++){
                    //console.log(i)
                    var title="";
                    if (i==0){
                        title = '受害人';
                    }else{
                        title = convertToChinese(i)+'级账号';
                    }

                    var str="<h5 style=\"color:#00c0ef;padding: 10px;\">"+title+"</h5><table id=\"zcGrid"+i+ "\" class=\"table table-bordered table-striped table-condensed\"> ";
                    $("#addTable").append(str);
                    zcZjlGrids(type,"zcGrid"+i,i,iszcygzs);
                }
            }
        }
    });
    if(iszcygzs>=1){
        changeZjfxIframeHeight('zcygzsIframe');
    }
}
function zcZjlGrids(zcType,tableId,level,iszcygzs) {
    zcZjlGrid(zcType,tableId,level,iszcygzs);
}
function zcZjlGrid(zcType,tableId,level,iszcygzs) {

    $('#'+tableId).bootstrapTable('destroy');

    $('#'+tableId).bootstrapTable({
    	method:"post",//请求方式（*）
        //极为重要，缺失无法执行queryParams，传递page参数
        contentType : "application/x-www-form-urlencoded",//post请求的话就加上这个句话
        url:ctx+'/znzf/jlt/list?nlevel='+level+'&iszcygzs='+iszcygzs,//请求后台的URL（*）
        cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped: true,
        pagination: true,//是否显示分页（*）
        pageList: [5,10,20,50,100,200,500,1000],
        pageSize: 500,
        pageNumber: 1,
        paginationPreText: '上一页',//指定分页条中上一页按钮的图标或文字
        paginationNextText: '下一页',//指定分页条中下一页按钮的图标或文字
        search: false,
        sidePagination: 'server',//设置为服务器端分页
        queryParams: queryParamsWthJjd,//参数
        showColumns: false,//是否显示所有的列（选择显示的列）
        showRefresh: false,//是否显示刷新按钮
        showToggle: false,//是否显示详细视图和列表视图的切换按钮
        minimumCountColumns: 2,//最少允许的列数
        height: 250,//行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        clickToSelect: false,//是否启用点击选中行
        smartDisplay: false,//智能显示分页或卡视图
        //toolbar: '#toolbar',//工具按钮用哪个容器
        //toolbarAlign:"left",
        checkboxHeader: false,
        rowStyle:function (row, index) {
            if (row.disab==1) {
                return {css:{"color":"red","cursor":"not-allowed"}};
            }
            return {};
        },
        //得到查询的参数
        columns :getZcColumn(zcType,tableId,iszcygzs),
        responseHandler:responseYhAndSfHandler//请求数据成功后，渲染表格前的方法
    });
}
function getZcColumn(zcType,tableId,iszcygzs) {
    var clms = [];
    if (zcType==0){
        clms = [
            {
                align:"center",
                checkbox:true,
                formatter:function (value, row, index) {
                    var ret = {checked:false,disabled:false};
                    if (row.keepon === 0) {
                        ret.disabled = true;
                    }else{
                        ret.disabled = false;
                    }
                    return ret;
                }
            },
            {
                title : "序号",
                field : "Number",
                align : "center",
                width : "50px",
                formatter: function (value, row, index) {
                    var pageSize = $('#'+tableId).bootstrapTable('getOptions').pageSize;     //通过table的#id 得到每页多少条
                    var pageNumber = $('#'+tableId).bootstrapTable('getOptions').pageNumber; //通过table的#id 得到当前第几页
                    return pageSize * (pageNumber - 1) + index + 1;    // 返回每条的序号： 每页条数 *（当前页 - 1 ）+ 序号
                }
            },
            {
                title : "所属机构",
                field : "bankname",
                align : "center"
            },
            {
                title : "姓名",
                field : "accountname",
                align : "center",
                formatter: function (value, row, index) {
                    if (row.sertype=='3'){
                        return '-';
                    }else{
                        return value;
                    }
                }
            },
            {
                title : "账/卡号",
                field : "cardnumber",
                align : "center"
            },
            {
                title : "上级账号",
                field : "parentzh",
                align : "center"
            },
            {
                title : "详情",
                field : "PK",
                align : "center",
                valign : "middle",
                width : "200px",
                events:yhAndSfShowOperateEvents,
                formatter : yhAndSfOperationFormatter
            },
            {
                title : "是否禁毒人员",
                align : "center",
                valign : "middle",
                width : "140px",
                events:isXdSdRyEvents,
                formatter : isXdSdRyFormatter
            },
        /*    {
                title : "处置",
                field : "PK",
                align : "center",
                valign : "middle",
                width : "80px",
                formatter : function(value, row, index){
                    var id = value;
                    var result = "";
                    if (row.nlevel>=0 &&row.sertype!=3){
                        if(row.zfpk){
                            result+='<span id="zfBtn" class="btn-sm btn-info" onclick="zfCardNumber(this,\''+row.pk+'\',\''+row.zfpk+'\',\''+row.sertype+'\',\''+row.cardnumber+'\',\''+row.jjdpk+'\',\''+tableId+'\');">止付</span>';
                        }else{
                            if(zfPerm){
                                result+='<span id="zfBtn" class="btn-sm btn-warning" onclick="zfCardNumber(this,\''+row.pk+'\',\''+row.zfpk+'\',\''+row.sertype+'\',\''+row.cardnumber+'\',\''+row.jjdpk+'\',\''+tableId+'\');">止付</span>';
                            }
                        }
                        /!*result+='&nbsp;&nbsp;<span id="djBtn" class="btn-sm btn-danger " onclick="djCardNumber(this,\''+row.pk+'\');">冻结</span>';*!/
                    }
                    if (row.bz){
                        result += '&nbsp;<span class="btn-sm btn-danger" onclick="showJltRemind(\''+row.jjdpk+'\',\''+row.pk+'\',\''+row.cardnumber+'\');">提醒</span>';
                    }
                    return result;
                }
            },*/
            {
                title : "下级分析状态",
                field : "PK",
                align : "center",
                valign : "middle",
                width : "100px",
                formatter : function(value, row, index) {
                    if(row.keepon === 1){
                    	return "还未分析";
                    }else  if(row.keepon === 0 && row.fxbs === 1){
                    	return "正在分析";
                    }else  if(row.fxbs === 2){
                    	return "分析完毕";
                    }
                }
            },
            {
                title : "标记",
                field : "PK",
                align : "center",
                valign : "middle",
                width : "50px",
                formatter : function(value, row, index) {
                    var result = '<span class="btn-sm btn-info" onclick="disabRow(\''+tableId+'\',\''+row.pk+'\');">';
                    if (row.disab==1){
                        result+="取消标记";
                    }else{
                        result+="标记";
                    }
                    result+="</span>";
                    if(iszcygzs==1 && row.isshow==0){
                        result += '&nbsp;<span class="btn-sm btn-info" onclick="setUpNlevel(\''+tableId+'\',\''+row.pk+'\',\''+row.cardnumber+'\');">等级</span>';
                    }
                    return result;
                }
            }
        ]
    }else if(zcType==1){
        clms = [
            //请根据需要修改/删除 需展示字段
            {
                title : "序号",
                field : "Number",
                align : "center",
                width : "50px",
                formatter: function (value, row, index) {
                    var pageSize = $('#'+tableId).bootstrapTable('getOptions').pageSize;     //通过table的#id 得到每页多少条
                    var pageNumber = $('#'+tableId).bootstrapTable('getOptions').pageNumber; //通过table的#id 得到当前第几页
                    return pageSize * (pageNumber - 1) + index + 1;    // 返回每条的序号： 每页条数 *（当前页 - 1 ）+ 序号
                }
            },
            {
                title : "所属机构",
                field : "bankname",
                align : "center"
            },
            {
                title : "姓名",
                field : "accountname",
                align : "center"
            },
            {
                title : "账/卡号",
                field : "cardnumber",
                align : "center"
            },
            {
                title : "上级账号",
                field : "parentzh",
                align : "center"
            },
            {
                title : "证件号码",
                field : "cardid",
                align : "center"
            },
            {
                title : "联系方式",
                field : "phone",
                align : "center"
            }
        ]
    }
    return clms;
}

function queryParamsWthJjd(params) {
    var tmp = $("#yaykQueryForm1").serializeJsonObject() ;
    tmp["pageSize"]=params.limit;
    tmp["pageNumber"]=params.offset/params.limit+1;
    tmp["jjdpk"]=$("#jjdpk").val();
    return tmp;
}

function isXdSdRyFormatter(value, row, index){
	if(row.xdrypk == null && row.sdrypk == null){
		return "未分析";
	}else if(row.xdrypk == "1" && row.sdrypk == "1"){
		return "否";
	}else{
		var result = "";
		if(row.xdrypk != null && row.xdrypk.length > 1 ){
			result += "<a href='javascript:void(0);' id='xdBtn' class='btn-sm btn-info' title='吸毒人员'>吸毒人员</a>";
		}
		if(row.sdrypk != null && row.sdrypk.length > 1 ){
			result += "&nbsp;<a href='javascript:void(0);' id='sdBtn' class='btn-sm btn-info' title='涉毒人员'>涉毒人员</a>";
		}
		return result;
	}
		
}

window.isXdSdRyEvents= {
	 "click #xdBtn":function (e,vale,row,index) {
		 var item = {'id':'xdryxx','name':'吸毒人员信息','url':ctx+'/jdzh/xdpage?pks='+row.xdrypk+'&lx=2'};
		 iframeTab.parentAddIframe(item);
	 },
	 "click #sdBtn":function (e,vale,row,index) {
		 var item = {'id':'sdryxx','name':'涉毒人员信息','url':ctx+'/jdzh/sdpage?pks='+row.sdrypk+'&lx=1'};
		 iframeTab.parentAddIframe(item);
	 }

}

function relationMap(pk){
    var item = {'id':'relationMap','name':'人员关系图','url':ctx+'/znzf/jlt/relation_map_page?nlevel=0&pk='+pk};
    iframeTab.parentAddIframe(item);
}
//操作栏的格式化
function yhAndSfOperationFormatter(value, row, index) {
    var id = value;
    var result = "";
    if(row.sertype && row.sertype==3){
        result += "<a href='javascript:void(0);' id='lsBtn' class='btn-sm btn-info' title='流水查询'>流水查询</a>";
    }else{
        result += "<a href='javascript:void(0);' id='mxBtn' class='btn-sm btn-info' title='明细'>明细</a>";
        result += "&nbsp;<a href='javascript:void(0);' id='ztBtn' class='btn-sm btn-info' title='主体'>主体</a>";
        if(row.qzhpk!=null){
            result += "&nbsp;<a href='javascript:void(0);' id='qzhBtn' class='btn-sm btn-info' title='全账号'>全账号</a>";
        }
    }
    if ((row.xdrypk != null && row.xdrypk != '1') || (row.sdrypk != null && row.sdrypk != '1')) {
        result += "&nbsp;</pre><a href='javascript:void(0);' id='relationMapBtn' class='btn-sm btn-info' title='人员关系图'>人员关系图</a>";
    }
    result += "<br><div style='padding-top: 10px;'><a href='javascript:void(0);' id='xdsdrycx' class='btn-sm btn-info'  title='禁毒人员查询'>禁毒人员查询</a>";
    result += "&nbsp;<a href='javascript:void(0);' id='ztQqSend' class='btn-sm btn-info'  title='主体请求重发'>主体请求重发</a></div>";
    return result;
}
window.yhAndSfShowOperateEvents= {
    "click #mxBtn":function (e,vale,row,index) {
        $("#showModalLabel").text("明细信息");
        var zcType = "";
        if(row.sertype && row.sertype==1){
            zcType="yhkMx";
        }
        if(row.sertype && row.sertype==2){
            zcType="sfMx";
        }
        $("#zcShowIframe").attr("src",ctx+"/znzf/base/show?zcType="+zcType+"&pk="+row.mxqqpk+"&zh="+row.cardnumber);
        $("#showModal").modal();
    },
    "click #ztBtn":function (e,vale,row,index) {
        $("#showModalLabel").text("主体信息");
        var zcType = "";
        if(row.sertype && row.sertype==1){
            zcType="yhkZt";
        }
        if(row.sertype && row.sertype==2){
            zcType="sfZt";
        }
        $("#zcShowIframe").attr("src",ctx+"/znzf/base/show?zcType="+zcType+"&pk="+row.ztqqpk+"&zh="+row.cardnumber);
        $("#showModal").modal();
    },
    "click #qzhBtn":function (e,vale,row,index) {
        $("#showModalLabel").text("全账号信息");
        $("#zcShowIframe").attr("src",ctx+"/znzf/base/show?zcType=sfQzh&pk="+row.qzhpk+"&zh="+row.cardnumber);
        $("#showModal").modal();
        // var item = {'id':'showSfJgAct','name':'全账号详情','url':ctx+"/znzf/base/show?zcType=sfQzh&pk="+row.jjdpk+"&zh="+row.cardnumber};
        //var item = {'id':'showSfJgAct','name':'全账号详情','url':ctx+"/znzf/base/show?zcType=sfQzh&pk="+row.qzhpk};
        //iframeTab.parentAddIframe(item);
    },
    "click #lsBtn":function (e,vale,row,index) {
        $("#showModalLabel").text("流水查询信息");
         $("#zcShowIframe").attr("src",ctx+"/znzf/base/show?zcType=sfLs&pk="+row.jjdpk+"&zh="+row.cardnumber);
         $("#showModal").modal();
    },
    "click #relationMapBtn":function (e,vale,row,index) {
        relationMap(row.cardnumber);
    },
    "click #xdsdrycx":function (e,vale,row,index) {
    	$.ajax({
            type: "get",
            dataType: "json",
            data:{ztpk:row.ztqqpk,sertype:row.sertype,pk:row.pk},
            url: ctx+"/yayk/xdSdRyCx" ,
            beforeSend: function () {
                loading("查询中，请稍后......");
            },
            success: function (data) {
            	top.layer.close(indexLoading);
                if (data.success){
                	top.layer.alert("查询成功："+data.msg, {icon: 1, title: '提示'});
                	zcZjlGrids("0","zcGrid"+row.nlevel,row.nlevel,"0");
                }else{
                	if(data.code =='1'){
                		top.layer.alert("查询失败："+data.msg, {icon: 2, title: '提示'});
                	}else if(data.code =='2'){
                		top.layer.confirm("查询失败："+data.msg+'需要提供身份证号号才能查询，确认继续吗？', {icon: 2, title: '提示'},function(r){
                			top.layer.prompt({title: '请输入身份证号码，点确认查询', formType: 0}, function(text, index){
                				$.ajax({
                		            type: "get",
                		            dataType: "json",
                		            data:{sfzh:text,pk:row.pk},
                		            url: ctx+"/yayk/xdSdRyCx2" ,
                		            beforeSend: function () {
                                        loading("查询中，请稍后......");
                                    },
                		            success: function (data) {
                		            	 top.layer.close(indexLoading);
                		            	 if (data.success){
                		            		 top.layer.alert("查询成功："+data.msg, {icon: 1, title: '提示'});
                		                 	 zcZjlGrids("0","zcGrid"+row.nlevel,row.nlevel,"0");
                		                 	 top.layer.close(index);
                		            	 }else{
                		            		 top.layer.alert("查询失败：请求异常", {icon: 2, title: '提示'});
                		            	 }
                		            },
                		            error : function() {
                		            	top.layer.alert("查询失败：请求异常", {icon: 2, title: '提示'});
                		            }
                				});
                			 });
                		});
                	}else{
                		top.layer.alert("查询失败：请求异常", {icon: 2, title: '提示'});
                    }
               }
            },
            error : function() {
            	top.layer.alert("查询失败：请求异常", {icon: 2, title: '提示'});
            }
        });
    },
    "click #ztQqSend":function (e,vale,row,index) {
    	$.ajax({
            type: "get",
            dataType: "json",
            data:{ztpk:row.ztqqpk,sertype:row.sertype},
            url: ctx+"/yayk/ztQqSend" ,
            success: function (data) {
            	 if (data.success){
           		 	 top.layer.alert("重发成功："+data.msg, {icon: 1, title: '提示'});
	           	 }else{
	           		if(data.code =='0' || data.code =='1'){
	           			top.layer.alert("重发失败："+data.msg, {icon: 2, title: '提示'});
	           		}else{
	           			top.layer.alert("重发失败：请求异常", {icon: 2, title: '提示'});
	           		}
	           	 }
            },
            error : function() {
            	top.layer.alert("重发失败：请求异常", {icon: 2, title: '提示'});
            }
    	 });
    },

}
function loading(msg) {
	indexLoading = top.layer.msg(msg, {
        icon: 16,
        shade: [0.1, '#666666'],
        time: false  //不自动关闭
    })
}

function showJltRemind(jjdpk,pk,cardnumber) {
    $("#showModalLabel").text(cardnumber+"提醒信息");
    $("#zcShowIframe").attr("src",ctx+"/znzf/znzfRemind/page?jjdpk="+jjdpk+"&cardnumber="+cardnumber+"&jltpk="+pk);
    $("#showModal").modal();
}
function disabRow(tableId,pk) {
    var reqData = {"pk":pk};
    $.ajax({
        type: "POST",
        dataType: "json",
        data:reqData,
        url: ctx+"/znzf/jlt/disab" ,
        async:false,
        success: function (result) {
            if (result.msg=="true"){
                $('#'+tableId).bootstrapTable("refresh");
            }else{
                alert(result.msg);
            }
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function zfCardNumber(obj,jltPk,zfpk,sertype,cardnumber,jjdpk,tableId) {
    $(obj).attr('disabled','disabled');
    var zcType = "";
    if(sertype && sertype==1){
        zcType="yhkZf";
    }
    if(sertype && sertype==2){
        zcType="sfZf";
    }
    var iszcygzs = $("#iszcygzs").val();

    if(zfpk && zfpk!=null && zfpk!='null'){
        $("#showModalLabel").text("止付信息");
        $("#zcShowIframe").attr("src",ctx+"/znzf/base/show?zcType="+zcType+"&pk="+jjdpk+"&zh="+cardnumber);
        $("#showModal").modal();

        $(obj).removeAttr('disabled','disabled');
    }else{
        $.ajax({
            type: "POST",
            dataType: "json",
            data:{"pk":jltPk},
            url: ctx+"/znzf/jlt/selectJltZfPk" ,
            async:false,
            success: function (result) {
                if (result.resultPk){
                    $("#showModalLabel").text("止付信息");
                    $("#zcShowIframe").attr("src",ctx+"/znzf/base/show?zcType="+zcType+"&pk="+jjdpk+"&zh="+cardnumber);
                    $("#showModal").modal();

                    $("#"+tableId).bootstrapTable("refresh");
                    $(obj).removeAttr('disabled','disabled');
                }else{
                    var url = ctx+"/znzf/jlt/selectJltAndJjd?qqType=01&pk="+jltPk;
                    var item = {'id':'add'+zcType,'name':'止付请求','url':url};
                    if(iszcygzs&& iszcygzs==1){
                        iframeTab.parentTwoAddIframe(item);
                    }else{
                        iframeTab.parentAddIframe(item);
                    }
                }

            },
            error : function() {
                alert("请求异常");
            }
        });
    }
}

function switchZcygzsTab(tabType,datatype,jjdpk) {
    if (tabType=='ypsl'){
        $("#zcygzsIframe").attr('src',ctx+'/znzf/ypsl/page.htm?jjdpk='+jjdpk);
    }else if (tabType=='zjfx'){
        $("#zcygzsIframe").attr('src',ctx+'/znzf/zjfx/page.html?jjdpk='+jjdpk);
    }else if(tabType=='swdt'){
        //$("#zcygzsIframe").attr('src',ctx+'/znzf/jlt/mindPage.html?jjdpk='+jjdpk);
        window.open(ctx+'/znzf/jlt/mindPage.html?jjdpk='+jjdpk,'jltWin','');
    }else if (tabType=='sjsc'){
        $("#zcygzsIframe").attr('src',ctx+'/znzf/sjsc/page.html?jjdpk='+jjdpk+'&datatype='+datatype);
    }
}
function changeZjfxIframeHeight(iframeId) {
    setTimeout(function () {
        window.parent.document.getElementById(iframeId).style.height=$("#addTable").height()+300+"px";
    },2500);
}
function keepOnAnalysis() {
    showLoading();
    var nlevels=[];
    var checkJltPk = new Array();
    var $grids = $("[id^='zcGrid']");
    if ($grids && $grids.length>0){
        for(var i=0;i<$grids.length;i++){
            var grid = $grids[i];
            var checkObj = $(grid).bootstrapTable('getSelections');
            if (checkObj&& checkObj.length>0){
                checkObj.forEach(function (x,index,a) {
                    checkJltPk.push(x);
                    if(nlevels.indexOf(x.nlevel) < 0){
                    	nlevels.push(x.nlevel);
                    }
                });
            }
        }
    }
    if (checkJltPk && checkJltPk.length>0){
        /*if (checkJltPk.length>20){
            alert("继续分析每次不能选择超过20条");
            hideLoading();
        }else{*/
        if(confirm('确定要对选中账/卡号进行继续分析吗')){
            $.ajax({
                type: "POST",
                dataType: "json",
                contentType: 'application/json; charset=UTF-8',
                url: ctx+"/znzf/jlt/keepOn" ,
                data: JSON.stringify(checkJltPk),
                async: false,
                success : function(data) {
                    alert(data.msg);
                    $.each(nlevels,function(i,v){
                    	zcZjlGrids("0","zcGrid"+v,v,"0");
                    })
                    hideLoading();
                },
                error : function() {
                    alert("请求异常");
                    hideLoading();
                }
            });
        }else{
            hideLoading();
        }
        //}
    }else{
        alert("请选择需要继续分析的账/卡号");
        hideLoading();
    }
}
function showSfCbMacGab() {
    var jjdpk = $("#jjdpk").val();
    $.ajax({
        type: "POST",
        dataType: "json",
        data:{"jjdpk":jjdpk},
        url: ctx+"/fzzx/jjd/showSfCbMacGab",
        async:false,
        success: function (result) {
            if(result){
                $("#macCbGab").addClass("btn-info");
                $("#macCbGab").attr("data-type","3");
            }else{
                $("#macCbGab").addClass("btn-warning");
                $("#macCbGab").attr("data-type","2");
            }
        }
    });
}
function macCbGab(obj) {
    var jjdpk = $("#jjdpk").val();
    var dType=$(obj).attr("data-type");

    if (dType==="2"){
        $.ajax({
            type: "POST",
            dataType: "json",
            data:{"jjdpk":jjdpk,"curdType":dType},
            url: ctx+"/fzzx/jjd/curdCbGab",
            async:false,
            success: function (result) {
                if (result.flag){
                    alert("操作成功");
                    $(obj).attr("data-type","3");
                    $(obj).removeClass("btn-warning");
                    $(obj).addClass("btn-info");
                }else{
                    alert(result.msg);
                }
            },
            error : function() {
                alert("请求异常");
            }
        });
    }else{
        var item = {'id':'macCbPage','name':'部MAC串并	','url':ctx+'/znzf/cbMac/page?jjdpk='+jjdpk};
        iframeTab.parentAddIframe(item);
    }
}
function userFocus(obj) {
    var jjdpk = $("#jjdpk").val();
    var dType=$(obj).attr("data-type");
    $.ajax({
        type: "POST",
        dataType: "json",
        data:{"jjdpk":jjdpk,"curdType":dType},
        url: ctx+"/fzzx/userFocus/curd",
        async:false,
        success: function (result) {
            if (result.flag){
                if(dType==="0"){
                    $(obj).text("取消关注");
                    $(obj).attr("data-type","1");
                }else if(dType==="1"){
                    $(obj).text("关注");
                    $(obj).attr("data-type","0");
                }
            }else{
                alert(result.msg);
            }
        },
        error : function() {
            alert("请求异常");
        }
    });
}
function showFocus() {
    var jjdpk = $("#jjdpk").val();
    $.ajax({
        type: "POST",
        dataType: "json",
        data:{"jjdpk":jjdpk},
        url: ctx+"/fzzx/userFocus/show",
        async:false,
        success: function (result) {
            if(result){
                $("#myFocus").text("取消关注");
                $("#myFocus").attr("data-type","1");
            }else{
                $("#myFocus").text("关注");
                $("#myFocus").attr("data-type","0");
            }
        }
    });
}
