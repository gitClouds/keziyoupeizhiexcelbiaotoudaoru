var xxlr_week,xxlr_month;
var zjck_cx_week,zjck_cx_month,zjck_cx_day,zjck_zf_week,zjck_zf_month,zjck_zf_day;
var latj_list,latj_show_type=[],latj_show_week=[],latj_show_month=[];
function jjdtj() {
    $.ajax({
        type: "get",
        dataType: "json",
        async:false,
        url: ctx+"/index/jjdtj" ,
        success: function (result) {
            if(result){
                xxlr_week = result.week;
                xxlr_month = result.month;
            }
        },
        error : function() {
            alert("请求异常");
        }
    });
}
function latj() {
    $.ajax({
        type: "get",
        dataType: "json",
        async:false,
        url: ctx+"/index/latj" ,
        success: function (result) {
            if(result.list){
                latj_list = result.list;
                for(var i = 0; i < latj_list.length; i++) {
                    latj_show_type[latj_show_type.length] = latj_list[i]['ajlb'];
                    latj_show_week[latj_show_week.length] = latj_list[i]['week'];
                    latj_show_month[latj_show_month.length] = latj_list[i]['month'];
                }
            }
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function cxtj() {
    $.ajax({
        type: "get",
        async:false,
        dataType: "json",
        url: ctx+"/index/cxtj" ,
        success: function (result) {
            if(result){
                zjck_cx_day = result.today;
                zjck_cx_week = result.week;
                zjck_cx_month = result.month;
            }
        },
        error : function() {
            alert("请求异常");
        }
    });
}
function zftj() {
    $.ajax({
        type: "get",
        async:false,
        dataType: "json",
        url: ctx+"/index/zftj" ,
        success: function (result) {
            zjck_zf_day = result.today;
            zjck_zf_week = result.week;
            zjck_zf_month = result.month;
        },
        error : function() {
            alert("请求异常");
        }
    });
}

function latjShow(type) {
    if(latj_list){}else {
        latj();
    }
    if (type==1){//月
        latj_option(latj_show_month, 'line_latj');
    }else {
        latj_option(latj_show_week, 'line_latj');
    }
}

function jjdTjShow(type){
    if(xxlr_week){}else{
        jjdtj();
    }
    if(type==1){
        xxlr_option(xxlr_month, 'line_xxlr');
    }else{
        xxlr_option(xxlr_week, 'line_xxlr');
    }
}

function xxlrTj() {
    var reqData={"rksj":$("#rksj").val()};
    $.ajax({
        type: "get",
        data: reqData,
        dataType: "json",
        contentType:'application/json;charset=utf-8',
        url: ctx+"/index/xxtj" ,
        success: function (result) {
            xxlr_tj_option(result,'xxlr_tj');
        },
        error : function() {
            //alert("请求异常");
        }
    });

}

function zjckTjShow(type){
    if(type==1){
        if(zjck_zf_day){}else{
            zftj();
        }
        zjck_option(zjck_zf_day, 'circle_zjcx_day', '当日');   //当日
        zjck_option(zjck_zf_week, 'circle_zjcx__week', '本周');   //本周
        zjck_option(zjck_zf_month, 'circle_zjcx_mouth', '本月');   //本月
    }else{
        if(zjck_cx_day){}else{
            cxtj();
        }
        zjck_option(zjck_cx_day, 'circle_zjcx_day', '当日');   //当日
        zjck_option(zjck_cx_week, 'circle_zjcx__week', '本周');   //本周
        zjck_option(zjck_cx_month, 'circle_zjcx_mouth', '本月');   //本月
    }
}

//资金查询图表封装
function zjck_option(data, dom, title) {
    var xAxis_data = [];
    var qqs_data = [];
    var fkcgs_data = [];
    var fksbs_data = [];
    var wfks_data = [];

    for(var i = 0; i < data.length; i++) {
        xAxis_data[xAxis_data.length] = data[i]['name'];
        qqs_data[qqs_data.length] = data[i]['value'];
        fkcgs_data[fkcgs_data.length] = data[i]['succ'];
        fksbs_data[fksbs_data.length] = data[i]['nsucc'];
        wfks_data[wfks_data.length] = data[i]['noresp'];
    }

    var circle_chart = echarts.init(document.getElementById(dom));
    var circle_option =  {
        color: ['#00c0ef', '#5cb85c', '#dd4b39', '#f0ad4e'],
        title: {
            text: title,
            textAlign: 'center',
            x: '50%'
        },
        grid: {
            'left': '15%',
            'right': '2%',
            'bottom': '8%'
        },
        tooltip: {
            trigger: 'item',
            axisPointer: {
                type: 'shadow'
            }
        },
        xAxis: {
            type: 'category',
            axisTick: {show: false},
            data: xAxis_data
        },
        yAxis: {
            type: 'value',
            minInterval : 1
        },
        series: [
            {
                name: '请求数',
                type: 'bar',
                barWidth: '15%',
                barGap: '6%',
                data: qqs_data
            },
            {
                name: '反馈成功数',
                type: 'bar',
                barWidth: '15%',
                data: fkcgs_data
            },
            {
                name: '反馈失败数',
                type: 'bar',
                barWidth: '15%',
                data: fksbs_data
            },
            {
                name: '未反馈数',
                type: 'bar',
                barWidth: '15%',
                data: wfks_data
            }
        ]
    };
    circle_chart.setOption(circle_option);

}

//信息录入图表封装
function xxlr_option(data, dom) {
    var xAxis_data = [];
    var jjds_data = [];
    var wjxcx_data = [];
    var jlt_data = [];
    var saje_data = [];

    for(var i= 0; i < data.length; i++) {
        xAxis_data[xAxis_data.length] = data[i]['name'];
        jjds_data[jjds_data.length] = data[i]['value'];
        wjxcx_data[wjxcx_data.length] = data[i]['noinit'];
        jlt_data[jlt_data.length] = data[i]['issucc'];
        saje_data[saje_data.length] = data[i]['saje'];
    };

    var line_chart = echarts.init(document.getElementById(dom));
    var line_option = {
        color: ['#00c0ef', '#dd4b39', '#5cb85c','#F0AD4E'],
        tooltip: {
            trigger: 'axis',
        },
        legend: {
            left: 'center',
            data:  [ '未进行嫌疑人查询', '金流图5级以上','涉案金额'],
            selected: {
                '涉案金额': false
            }
        },
        xAxis: {
            type: 'category',
            name: '时间',
            splitLine: {show: false},
            axisLabel:{
                interval:1,//0：全部显示，1：间隔为1显示对应类目，2：依次类推，（简单试一下就明白了，这样说是不是有点抽象）
                rotate:-45,//倾斜显示，-：顺时针旋转，+或不写：逆时针旋转
            },
            data: xAxis_data
        },
        grid: {
            left: '2%',
            right: '6%',
            bottom: '8%',
            containLabel: true
        },
        yAxis: {
            type: 'value',
            name: '数量',
            minInterval : 1
        },
        series: [
           /* {
                name: '接警单数',
                type: 'line',
                data: jjds_data
            },*/
            {
                name: '未进行嫌疑人查询',
                type: 'line',
                data: wjxcx_data
            },
            {
                name: '金流图5级以上',
                type: 'line',
                data: jlt_data
            },
            {
                name: '涉案金额',
                type: 'line',
                data: saje_data
            }
        ]
    };
    line_chart.setOption(line_option);
}

function xxlr_tj_option(data, dom) {
    var xAxis_data = [];
    var count_data = [];

    for(var i = 0; i < data.length; i++) {
        xAxis_data[xAxis_data.length] = data[i]['name'];
        count_data[count_data.length] = data[i]['value'];
    }

    var circle_chart = echarts.init(document.getElementById(dom));
    var circle_option =  {
        color: ['#00c0ef'],
        grid: {
            'left': '2%',
            'right': '6%',
            'bottom': '20%'
        },
        tooltip: {
            trigger: 'item',
            axisPointer: {
                type: 'shadow'
            }
        },
        xAxis: {
            type: 'category',
            axisTick: {show: false},
            axisLabel:{
                interval:0,//0：全部显示，1：间隔为1显示对应类目，2：依次类推，（简单试一下就明白了，这样说是不是有点抽象）
                rotate:-45,//倾斜显示，-：顺时针旋转，+或不写：逆时针旋转
            },
            data: xAxis_data
        },
        yAxis: {
            type: 'value',
            minInterval : 1
        },
        series: [
            {
                name: '接警单录入数',
                type: 'bar',
                barWidth: '15%',
                barGap: '6%',
                data: count_data
            }
        ]
    };
    circle_chart.setOption(circle_option);
}

function latj_option(data,dom) {
    var circle_chart = echarts.init(document.getElementById(dom));
    var circle_option =  {
        color: ['#00c0ef'],
        grid: {
            'left': '3%',
            'right': '6%',
            'bottom': '20%'
        },
        tooltip: {
            trigger: 'item',
            axisPointer: {
                type: 'shadow'
            }
        },
        xAxis: {
            type: 'category',
            axisTick: {show: false},
            axisLabel:{
                interval:0,//0：全部显示，1：间隔为1显示对应类目，2：依次类推，（简单试一下就明白了，这样说是不是有点抽象）
                rotate:-45,//倾斜显示，-：顺时针旋转，+或不写：逆时针旋转
            },
            data: latj_show_type
        },
        yAxis: {
            type: 'value',
            minInterval : 1
        },
        series: [
            {
                name: '类案录入数',
                type: 'bar',
                barWidth: '15%',
                barGap: '6%',
                data: data
            }
        ]
    };
    circle_chart.setOption(circle_option);
}