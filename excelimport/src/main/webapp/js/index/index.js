function getLevelMenu(level, parentCode, obj) {
    var menus = '';
    if (level > 1) {
        menus = '<ul class="treeview-menu">';
    }
    var dataShow = $(obj).attr("data-sync-show");
    if (level == 1 || dataShow == 0) {
        var reqData = {"codeLevel": level, "parentCode": parentCode};
        $.ajax({
            url: ctx + "/system/menu/levelList",
            type: "post",
            data: reqData,
            dataType: "json",
            async: false,
            success: function (data) {
                $.each(data, function (index) {
                    if (data[index].isparent && data[index].isparent == 1) {
                        menus += getParentCode(data[index].icon, data[index].name, data[index].level, data[index].code);
                    } else {
                        menus += getCode(data[index].pageId, data[index].url, data[index].icon, data[index].name);
                    }
                });
                if (level > 1) {
                    menus += '</ul>';
                }
                if (level == 1) {
                    menus += "<li class=\"login_out\"><a href=\"javascript:logout();\"><i class=\"fa fa-power-off text-red\"></i><span>注销</span></a></li>";
                } else {
                    $(obj).attr("data-sync-show", "1");
                }
                $(obj).append(menus);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log(XMLHttpRequest.status);
                console.log(XMLHttpRequest.readyState);
                console.log(textStatus);
            }
        });
    }
}

function getParentCode(icon, name, level, parentCode) {
    return "<li id='" + parentCode + "' class=\"treeview\" data-sync-show=\"0\" onclick=\"getLevelMenu(" + (level + 1) + ",'" + parentCode + "',this)\"><a href=\"#\"><i class=\"fa " + icon + "\"></i><span>" + name + "</span><span class=\"pull-right-container\"><i class=\"fa fa-angle-left pull-right\"></i></span></a></li>";
}

function getCode(pageId, ul, icon, name) {
    return "<li id='" + name + "' class=\"tree_form\" onclick=\"cret_tab('" + pageId + "','" + name + "','" + ul + "')\"><a href=\"javascript:void(0);\"><i class=\"fa " + icon + "\"></i>" + name + "</a></li>";
}

function logout() {
    window.location = ctx + "/logout"
}

function initTab() {
    //首页暂时屏蔽，进入首页默认进入接警单界面
    var item = {'id': 'homePage', 'name': '首页', 'url': ctx + '/index/indexPage'};
    iframeTab.addIframe(item);
    /*var item = {'id':'fzzxJjdPage','name':'接警单','url':ctx+'/fzzx/jjd/page'};
    iframeTab.addIframe(item);*/
}

function qsjs() {
    window.open(ctx + "/es/es/pageNew.html");
}

function cret_tab(id, name, url) {
    var item = {'id': id, 'name': name, 'url': ctx + url};
    iframeTab.addIframe(item);
};