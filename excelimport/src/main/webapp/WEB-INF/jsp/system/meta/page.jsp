<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
    <style>
        .valid_message{
            color: red;
        }
    </style>
</head>
<body>
<!-- 主体内容 -->
<section class="content">
    <!--查询项表单 begin-->
    <div class="row">
        <div class="col-xs-12">
            <ul class="box clearfix">
                <form class="form_chag form-inline" id="metaEditForm" name="metaEditForm">
                    <input type="hidden" id="edit_curdType" name="curdType" />
                    <input type="hidden" id="pk" name="pk" value="${meta.pk}" />

                    <div class="form-group" style="width: 60%">
                        <label for="nlevel">最大层级</label>
                        <input type="text" style="width: 15%!important" value="${meta.nlevel}" class="required form-control" id="nlevel" name="nlevel" data-valid="isNonEmpty||onlyInt||between:1-1||maxIntVal:5||minIntVal:1" data-error="不能为空||只能输入数字||长度为1||不能大于5||不能小于1" >
                        <b>层</b>
                    </div>
                    <div class="form-group" style="width: 60%">
                        <label for="ts">下级账号</label>
                        <input type="text" style="width: 15%!important" value="${meta.ts}" class="required form-control" id="ts" name="ts" data-valid="isNonEmpty||onlyInt||between:1-2||maxIntVal:20||minIntVal:1" data-error="不能为空||只能输入数字||长度为1-2||不能大于20||不能小于1"  >
                        <b>个</b>
                    </div>
                    <div class="form-group" style="width: 60%">
                        <label for="minje">最小金额</label>
                        <input type="text" style="width: 15%!important" value="${meta.minje}" class="required form-control" id="minje" name="minje" data-valid="isNonEmpty||onlyInt||between:3-4" data-error="不能为空||只能输入数字||长度为3-4" >
                        <b>元</b>
                    </div>
                    <div class="form-group" style="width: 60%">
                        <label for="maxhour">转账时间后</label>
                        <input type="text" value="${meta.maxhour}" style="width: 15%!important" class="required form-control" id="maxhour" name="maxhour" data-valid="isNonEmpty||onlyInt||between:1-3||maxIntVal:168||minIntVal:1" data-error="不能为空||只能输入数字||长度为1-3||不能大于168||不能小于1" >
                        <b>小时的流水</b>
                    </div>
                    <div class="form-group" style="width: 60%">
                        <label for="ts">交易时间段</label>
                        <input type="text" style="width: 15%!important" value="${meta.jysjb}" class="required form-control" id="jysjb" name="jysjb" data-valid="isNonEmpty||onlyInt||between:1-2||maxIntVal:24" data-error="不能为空||只能输入数字||长度为1-2||不能大于24"  >
                        -
                        <input type="text" style="width: 15%!important" value="${meta.jysje}" class="required form-control" id="jysje" name="jysje" data-valid="isNonEmpty||onlyInt||between:1-2||maxIntVal:24" data-error="不能为空||只能输入数字||长度为1-2||不能大于24"  >
                        <b>点</b>
                    </div>
                    <div class="form-group" style="width: 100%">
                        <label style="width: 18%;" for="phone">指定电话号码(多个号码用英文逗号隔开)</label>
                        <textarea  type="text" value="${meta.phone}" style="width: 50%!important" class="form-control" id="phone" name="phone" rows="5">${meta.phone}</textarea>
                    </div>
                    <div class="form-group " style="width: 10%;float: right">
                        <button type="button" class="btn btn-info" onclick="metaEditSubmit(this);">提交</button>
                    </div>
                    <%--<div class="form-group">
                        <label for="jdgx">借贷关系</label>
                        <input type="text" class="form-control" id="jdgx" name="jdgx" >
                    </div>--%>

                </form>
            </ul>
        </div>
    </div>
    <!--查询项表单 end-->
</section>
<script>
    var ctx = "${ctx}";
</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/system/meta.js"></script>
</body>
</html>