<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/1/10/010
  Time: 13:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>用户详情</title>
</head>
<body>
<table class="table table-bordered">
    <tr>
        <td class="sec_tit">账号</td>
        <td id="show_username"></td>
        <td class="sec_tit">姓名</td>
        <td id="show_xm"></td>
    </tr>
    <tr>
        <td class="sec_tit">身份证号</td>
        <td id="show_idno"></td>
        <td class="sec_tit">警号</td>
        <td id="show_jh"></td>
    </tr>
    <tr>
        <td class="sec_tit">所属机构</td>
        <td id="show_jgmc"></td>
        <td class="sec_tit">机构代码</td>
        <td id="show_jgdm"></td>
    </tr>
    <tr>
        <td class="sec_tit">联系方式</td>
        <td id="show_lxfs"></td>
        <td class="sec_tit">过期时间</td>
        <td id="show_outtime"></td>
    </tr>
</table>
</body>
</html>
