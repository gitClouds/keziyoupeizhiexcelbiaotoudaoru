<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>用户新增/修改</title>
</head>
<body>
    <form class="form_chag" id="userEditForm" name="userEditForm">
        <input type="hidden" id="edit_curdType" name="curdType" >
            <input type="hidden" id="edit_pk" name="pk" value="" >
            <table class="table table-bordered">
                <tr>
                    <td class="sec_tit">
                        <label for="edit_username">账号</label>
                    </td>
                    <td>
                        <div class="form_controls">
                            <input type="text" class="required form-control" id="edit_username" name="username" data-tip="" data-valid="isNonEmpty||onlyEnAndInt||between:6-16" data-error="账号不能为空||只能输入数字和英文字符||账号长度为6-16位" >
                        </div>
                    </td>
                    <td class="sec_tit">
                        <label for="edit_password">密码</label>
                    </td>
                    <td>
                        <div class="form_controls">
                            <input type="password" class="required form-control" id="edit_password" name="password" data-tip="" data-valid="onlyEnAndInt||between:4-18" data-error="只能输入数字和英文字符||密码长度为4-18位" >
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="sec_tit">
                        <label for="edit_xm">姓名</label>
                    </td>
                    <td>
                        <div class="form_controls">
                            <input class="required form-control" id="edit_xm" type="text" name="xm" data-tip="" data-valid="isNonEmpty||onlyZh||between:2-10" data-error="姓名不能为空||只能输入中文||姓名长度为2-10位">
                        </div>
                    </td>
                    <td class="sec_tit">
                        <label for="edit_idno">身份证号</label>
                    </td>
                    <td>
                        <div class="form_controls">
                            <input type="text" class="required form-control" id="edit_idno" name="idno" data-tip="" data-valid="isNonEmpty||isCardNo" data-error="身份证号不能为空||请输入正确的身份证号码" >
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="sec_tit">
                        <label for="edit_jh">警号</label>
                    </td>
                    <td>
                        <div class="form_controls">
                            <input type="text" class="required form-control" id="edit_jh" name="jh" data-tip="" data-valid="isNonEmpty||onlyInt||between:6-7" data-error="警号不能为空||只能输入数字||警号长度为6-7位" >
                        </div>
                    </td>
                    <td class="sec_tit">
                        <label for="edit_jgdm">所属机构</label>
                    </td>
                    <td>
                        <div class="form-group has-feedback form_controls" style="width: 100%;">
                            <input type="hidden" id="edit_jgdm" name="jgdm" />
                            <input type="text" class="required form-control" id="edit_jgmc" name="jgmc" data-jzd-type="tree" data-jzd-code="DEPT" data-jzd-filter="" data-jzd-dm="edit_jgdm" data-jzd-mc="edit_jgmc" readonly data-tip="" data-valid="isNonEmpty" data-error="所属机构不能为空" >
                            <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_jgdm');clearInput('edit_jgmc');"></span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="sec_tit">
                        <label for="edit_lxfs">联系方式</label>
                    </td>
                    <td>
                        <div class="form_controls">
                            <input type="text" class="required form-control" id="edit_lxfs" name="lxfs" data-tip="" data-valid="isNonEmpty||onlyInt||between:10-12" data-error="联系方式不能为空||只能输入数字||联系方式长度为10-12位" >
                        </div>
                    </td>
                    <td class="sec_tit">
                        <label for="edit_outtime">过期时间</label>
                    </td>
                    <td>
                        <div class="form_controls">
                            <input type="text" value="" class="Wdate required form-control" id="edit_outtime" name="outtime" onFocus="WdatePicker({lang:'zh-cn',isShowClear:false,dateFmt:'yyyy-MM-dd HH:mm:ss'})" data-valid="isNonEmpty" data-error="过期时间不能为空" >
                        </div>
                    </td>
                </tr>
            </table>
    </form>
</body>
</html>
