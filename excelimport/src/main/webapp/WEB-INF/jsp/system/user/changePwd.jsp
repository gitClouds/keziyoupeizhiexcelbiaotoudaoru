<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>密码修改</title>
    <style>
        .sec_tit{
            width: 20%;
        }
    </style>
</head>
<body>
    <form class="form_chag" id="changePwdForm" name="changePwdForm">
            <table class="table table-bordered">
                <tr>
                    <td class="sec_tit">
                        <label for="old_password">原始密码</label>
                    </td>
                    <td>
                        <div class="form_controls">
                            <input type="password" class="required form-control" id="old_password" name="old_password" data-tip="" data-valid="onlyEnAndInt||between:6-18" data-error="只能输入数字和英文字符||密码长度为6-18位" >
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="sec_tit">
                        <label for="password">新密码</label>
                    </td>
                    <td>
                        <div class="form_controls">
                            <input type="password" class="required form-control" id="password" name="password" data-tip="6-18位的数字和英文字符组合" data-valid="onlyEnAndInt||between:6-18" data-error="只能输入数字和英文字符||密码长度为6-18位" >
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="sec_tit">
                        <label for="conf_password">确认密码</label>
                    </td>
                    <td>
                        <div class="form_controls">
                            <input type="password" class="required form-control" id="conf_password" name="conf_password" data-tip="" data-valid="onlyEnAndInt||between:6-18" data-error="只能输入数字和英文字符||密码长度为6-18位" >
                        </div>
                    </td>
                </tr>
            </table>
    </form>
</body>
</html>
