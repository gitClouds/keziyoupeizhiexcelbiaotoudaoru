<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
</head>
<body>
<!-- 主体内容 -->
<section class="content">
    <!--查询项表单 begin-->
    <div class="row">
        <div class="col-xs-12">
            <ul class="box clearfix">
                <form class="form_chag form-inline" name="userPermQueryForm" id="userPermQueryForm">
                    <!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
                    <!-- 请根据需要修改 查询检索项 -->
                    <div class="form-group">
                        <label for="username">账号</label>
                        <input type="text" class="form-control" id="username" name="username" >
                    </div>
                    <div class="form-group">
                        <label for="idno">身份证</label>
                        <input type="text" class="form-control" id="idno" name="idno" >
                    </div>
                    <div class="form-group">
                        <label for="xm">姓名</label>
                        <input type="text" class="form-control" id="xm" name="xm" >
                    </div>
                    <div class="form-group">
                        <label for="jh">警号</label>
                        <input type="text" class="form-control" id="jh" name="jh" >
                    </div>
                    <div class="form-group">
                        <label for="lxfs">联系方式</label>
                        <input type="text" class="form-control" id="lxfs" name="lxfs" >
                    </div>
                    <div class="form-group has-feedback">
                        <label for="jgmc">单位</label>
                        <div class="dic_ipt">
                            <input type="hidden" id="jgdm" name="jgdm" />
                            <input type="text" class="form-control" id="jgmc" name="jgmc" data-jzd-type="tree" data-jzd-code="DEPT" data-jzd-filter="" data-jzd-dm="jgdm" data-jzd-mc="jgmc" readonly >
                            <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('jgdm');clearInput('jgmc');"></span>
                        </div>
                    </div>

                    <div class="form-group ">
                        <button type="button" style="margin-left: 8%" class="btn btn-info" onclick="search();">检索</button>
                    </div>
                </form>
            </ul>
        </div>
    </div>
    <!--查询项表单 end-->
    <!--结果展示 begin-->
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="userPermGrid" class="table table-bordered table-striped table-condensed">

                    </table>
                </div>
            </div>
        </div>
    </div>
    <!--结果展示 end-->
    <!-- 编辑模态框（Modal） -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="editModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="editMessage">
                    <jsp:include page="permissionRelatForm.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" id="editConfirmBtn">提交</button>
                </div>
            </div>
        </div>
    </div>
    <div id="treeDiv">
        <input type="text" class="form-control" id="searchTree"  placeholder="支持文本检索">
        <div id="tree"></div>
    </div>
</section>
<script>
    var ctx = "${ctx}";
</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/system/userPermission.js"></script>
<script src="${ctx}/js/system/permissionRelat.js"></script>
</body>
</html>