<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">主键UUID</td>
            <td id="show_pk"></td>
    		<td class="sec_tit">权限代码</td>
            <td id="show_code"></td>
		</tr>
		<tr>
    		<td class="sec_tit">权限名称</td>
            <td id="show_name"></td>
    		<td class="sec_tit">权限描述</td>
            <td id="show_comments"></td>
		</tr>
    </table>
</body>
</html>
