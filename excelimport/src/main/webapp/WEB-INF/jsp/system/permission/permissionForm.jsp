<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <form class="form_chag" id="permissionEditForm" name="permissionEditForm">
        <input type="hidden" id="edit_curdType" name="curdType" >
        <input type="hidden" id="edit_pk" name="pk" >
        <table class="table table-bordered">
        	<!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
            <!-- 请根据需要修改 可编辑项、hidden等 -->
            <!-- 表单校验的字段需要在input外加一层 <div class="form_controls"> -->
            <!-- 表单校验的input classes + required -->
            <!-- 表单校验的input data-tip="" 为进入时提醒 -->
            <!-- 表单校验的input data-valid="" 为验证规则，多个可使用‘||’分割；具体参照jquery-validate.js-->
            <!-- 表单校验的input data-error="" 为验证不通过时的提醒，需与 data-valid一一对应 -->
            <!-- 校验需自己填写 -->
			<tr>
        		<td class="sec_tit">
                    <label for="edit_code">权限代码</label>
                </td>
                <td>
                    <input type="text" class="form-control" id="edit_code" name="code" >
                </td>
        		<td class="sec_tit">
                    <label for="edit_name">权限名称</label>
                </td>
                <td>
                    <input type="text" class="form-control" id="edit_name" name="name" >
                </td>
            </tr>
            <tr>
        		<td class="sec_tit">
                    <label for="edit_comments">权限描述</label>
                </td>
                <td>
                    <input type="text" class="form-control" id="edit_comments" name="comments" >
                </td>
			</tr>
        </table>
    </form>
</body>
</html>
