<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">用户姓名</td>
            <td id="show_username"></td>
			<td class="sec_tit">单位名称</td>
			<td id="show_dwmc"></td>
		</tr>
		<tr>
    		<td class="sec_tit">经办人警号</td>
            <td id="show_jbrzjhm"></td>
			<td class="sec_tit">协查人警号</td>
			<td id="show_xcrzjhm"></td>
		</tr>
		<tr>
			<td class="sec_tit">经办人姓名</td>
			<td id="show_jbrxm"></td>
			<td class="sec_tit">协查人姓名</td>
			<td id="show_xcrxm"></td>
		</tr>
		<tr>
			<td class="sec_tit">经办人电话</td>
			<td id="show_jbrdh"></td>
			<td class="sec_tit">协查人电话</td>
			<td id="show_xcrdh"></td>
		</tr>

		<tr>
    		<td class="sec_tit">入库时间</td>
            <td id="show_rksj"></td>
			<td class="sec_tit">录入单位</td>
			<td id="show_lrdwmc"></td>
		</tr>
		<tr>
			<td class="sec_tit">经办人警官证</td>
			<td>
				<input id="show_jbr" type="file" >
			</td>
			<td class="sec_tit">协查人警官证</td>
			<td>
				<input id="show_xcr" type="file" >
			</td>
		</tr>
    </table>
</body>
</html>
