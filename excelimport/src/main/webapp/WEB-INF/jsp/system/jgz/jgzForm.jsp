<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <form class="form_chag" id="jgzEditForm" name="jgzEditForm">
        <input type="hidden" id="edit_curdType" name="curdType" >
        <input type="hidden" id="edit_pk" name="pk" >
        <table class="table table-bordered">
        	<!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
            <!-- 请根据需要修改 可编辑项、hidden等 -->
            <!-- 表单校验的字段需要在input外加一层 <div class="form_controls"> -->
            <!-- 表单校验的input classes + required -->
            <!-- 表单校验的input data-tip="" 为进入时提醒 -->
            <!-- 表单校验的input data-valid="" 为验证规则，多个可使用‘||’分割；具体参照jquery-validate.js-->
            <!-- 表单校验的input data-error="" 为验证不通过时的提醒，需与 data-valid一一对应 -->
            <!-- 校验需自己填写 -->

            <tr id="xxType" >
        		<td class="sec_tit">
                    <label>类型</label>
                </td>
                <td>
                    <label class="radio-inline">
                        <input type="radio" name="xxType" value="yh" checked />用户
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="xxType" value="dw" />单位
                    </label>
                </td>
        		<td class="sec_tit">
                    <label for="edit_dwdm">单位</label>
                </td>
                <td>
                    <div class="form-group has-feedback form_controls" style="width: 100%;">
                        <input type="hidden" id="edit_dwdm" name="dwdm" />
                        <input type="text" class="form-control" id="edit_dwmc" name="dwmc" data-jzd-type="tree" data-jzd-code="DEPT" data-jzd-filter="" data-jzd-dm="edit_dwdm" data-jzd-mc="edit_dwmc" readonly  >
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_dwdm');clearInput('edit_dwmc');"></span>
                    </div>
                </td>
			</tr>

			<tr>
        		<td class="sec_tit">
                    <label for="edit_jbrzjhm">经办人警号</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_jbrzjhm" name="jbrzjhm" data-valid="isNonEmpty||onlyInt||between:6-7" data-error="不能为空||只能输入数字||长度为6-7位" >
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_xcrzjhm">协查人警号</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" class="required form-control" id="edit_xcrzjhm" name="xcrzjhm" data-valid="isNonEmpty||onlyInt||between:6-7" data-error="不能为空||只能输入数字||长度为6-7位" >
                    </div>
                </td>

			</tr>
			<tr>
                <td class="sec_tit">
                    <label for="edit_jbrxm">经办人姓名</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" class="required form-control" id="edit_jbrxm" name="jbrxm" data-valid="isNonEmpty||onlyZh||between:2-10" data-error="不能为空||只能输入中文||长度为2-10个汉字" >
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_xcrxm">协查人姓名</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" class="required form-control" id="edit_xcrxm" name="xcrxm" data-valid="isNonEmpty||onlyZh||between:2-10" data-error="不能为空||只能输入中文||长度为2-10个汉字" >
                    </div>
                </td>
			</tr>
			<tr>
                <td class="sec_tit">
                    <label for="edit_jbrdh">经办人电话</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" class="required form-control" id="edit_jbrdh" name="jbrdh" data-valid="isNonEmpty||onlyInt||between:10-12" data-error="不能为空||只能输入数字||长度为10-12位" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_xcrdh">协查人电话</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_xcrdh" name="xcrdh" data-valid="isNonEmpty||onlyInt||between:10-12" data-error="不能为空||只能输入数字||长度为10-12位" >
                    </div>
                </td>
			</tr>
			<tr>
                <td class="sec_tit">
                    <label for="edit_jbr">经办人警官证</label>
                </td>
                <td>
                    <input type="hidden" name="jbrmongo" id="jbr_mongo" value="">
                    <div class="form_controls" id="edit_jbr">

                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_xcr">协查人警官证</label>
                </td>
                <td>
                    <input type="hidden" name="xcrmongo" id="xcr_mongo" value="">
                    <div class="form_controls" id="edit_xcr">

                    </div>
                </td>
			</tr>

        </table>
    </form>
</body>
</html>
