<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <form class="form_chag" id="roleEditForm" name="roleEditForm">
        <input type="hidden" id="edit_role_curdType" name="curdType" >
        <input type="hidden" id="edit_role_pk" name="pk" >
        <table class="table table-bordered">
        	<!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
            <!-- 请根据需要修改 可编辑项、hidden等 -->
            <!-- 表单校验的字段需要在input外加一层 <div class="form_controls"> -->
            <!-- 表单校验的input classes + required -->
            <!-- 表单校验的input data-tip="" 为进入时提醒 -->
            <!-- 表单校验的input data-valid="" 为验证规则，多个可使用‘||’分割；具体参照jquery-validate.js-->
            <!-- 表单校验的input data-error="" 为验证不通过时的提醒，需与 data-valid一一对应 -->
            <!-- 校验需自己填写 -->
			<tr>
        		<td class="sec_tit">
                    <label for="edit_role_code">角色代码</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_role_code" name="code" data-tip="" data-valid="isNonEmpty||onlyEn||between:4-12" data-error="不能为空||只能输入字母||4-12位之间" >
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_role_name">角色名称</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" class="required form-control" id="edit_role_name" name="name" data-tip="" data-valid="isNonEmpty||onlyZh||between:3-8" data-error="不能为空||只能输入中文||3-8位之间" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_role_comments">角色描述</label>
                </td>
                <td colspan="3">
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_role_comments" name="comments" data-tip="" data-valid="isNonEmpty||onlyZh||between:2-20" data-error="不能为空||只能输入中文||2-20位之间" >
                    </div>
                </td>
			</tr>
        </table>
    </form>
</body>
</html>
