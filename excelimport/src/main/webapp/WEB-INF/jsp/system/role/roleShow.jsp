<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">角色代码</td>
            <td id="show_role_code"></td>
			<td class="sec_tit">角色名称</td>
			<td id="show_role_name"></td>
		</tr>
		<tr>
    		<td class="sec_tit">角色描述</td>
            <td id="show_role_comments"></td>
		</tr>
    </table>
</body>
</html>
