<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <form class="form_chag" id="menuEditForm" name="menuEditForm">
        <input type="hidden" id="edit_curdType" name="curdType" >
        <input type="hidden" id="edit_pk" name="pk" >
        <table class="table table-bordered">
        	<!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
            <!-- 请根据需要修改 可编辑项、hidden等 -->
            <!-- 表单校验的字段需要在input外加一层 <div class="form_controls"> -->
            <!-- 表单校验的input classes + required -->
            <!-- 表单校验的input data-tip="" 为进入时提醒 -->
            <!-- 表单校验的input data-valid="" 为验证规则，多个可使用‘||’分割；具体参照jquery-validate.js-->
            <!-- 表单校验的input data-error="" 为验证不通过时的提醒，需与 data-valid一一对应 -->
            <!-- 校验需自己填写 -->
			<tr>
        		<td class="sec_tit">
                    <label for="edit_code">菜单代码</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_code" name="code" data-tip="" data-valid="isNonEmpty||onlyInt||between:8-8" data-error="不能为空||只能输入数字||8位数字" >
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_name">菜单名称</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" class="required form-control" id="edit_name" name="name" data-tip="" data-valid="isNonEmpty||between:3-20" data-error="不能为空||3-20位中文" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_icon">icon标签</label>
                </td>
                <td>
                    <input type="text" class="form-control" id="edit_icon" name="icon"  >
                </td>
                <td class="sec_tit">
                    <label for="edit_pageId">菜单ID</label>
                </td>
                <td>
                    <input type="text" class="form-control" id="edit_pageId" name="pageId"  >
                </td>
			</tr>
            <tr>
                <td class="sec_tit">
                    <label for="edit_url">URL路径</label>
                </td>
                <td>
                    <input type="text" class="form-control" id="edit_url" name="url" >
                </td>
                <td class="sec_tit">
                    <label for="edit_parentCode">上级代码</label>
                </td>
                <td>
                    <input type="text" class="form-control" id="edit_parentCode" name="parentCode" >
                </td>
            </tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_isparent">是否包含子节点</label>
                </td>
                <td>
                    <input type="text" class="form-control" id="edit_isparent" name="isparent"  >
                </td>
        		<td class="sec_tit">
                    <label for="edit_codeLevel">菜单级别</label>
                </td>
                <td>
                	<div class="form_controls">
                        <select class="required form-control" id="edit_codeLevel" name="codeLevel" data-tip="" data-valid="isNonEmpty" data-error="必须选择">
                            <option></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>
                    </div>
                </td>

			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_px">排序号码</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_px" name="px" data-tip="" data-valid="isNonEmpty||onlyInt||between:1-6" data-error="不能为空||只能输入数字||1-6位数字" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_comments">菜单描述</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_comments" name="comments" data-tip="" data-valid="isNonEmpty||between:6-20" data-error="不能为空||6-20位中文" >
                    </div>
                </td>
			</tr>
        </table>
    </form>
</body>
</html>
