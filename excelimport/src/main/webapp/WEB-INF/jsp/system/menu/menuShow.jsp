<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">主键UUID</td>
            <td id="show_pk"></td>
    		<td class="sec_tit">菜单代码</td>
            <td id="show_code"></td>
		</tr>
		<tr>
    		<td class="sec_tit">菜单名称</td>
            <td id="show_name"></td>
    		<td class="sec_tit">icon名称</td>
            <td id="show_icon"></td>
		</tr>
		<tr>
    		<td class="sec_tit">菜单ID</td>
            <td id="show_pageId"></td>
    		<td class="sec_tit">是否包含子节点</td>
            <td id="show_isparent"></td>
		</tr>
		<tr>
    		<td class="sec_tit">URL路径</td>
            <td id="show_url"></td>
    		<td class="sec_tit">上级代码</td>
            <td id="show_parentCode"></td>
		</tr>
		<tr>
    		<td class="sec_tit">菜单级别</td>
            <td id="show_codeLevel"></td>
			<td class="sec_tit">排序序号</td>
			<td id="show_px"></td>
		</tr>
		<tr>
    		<td class="sec_tit">菜单描述</td>
            <td id="show_comments"></td>
		</tr>
    </table>
</body>
</html>
