<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <form class="form_chag" id="dwDzzEditForm" name="dwDzzEditForm">
        <input type="hidden" id="edit_curdType" name="curdType" >
        <input type="hidden" id="edit_pk" name="pk" >
        <table class="table table-bordered">
        	<!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
            <!-- 请根据需要修改 可编辑项、hidden等 -->
            <!-- 表单校验的字段需要在input外加一层 <div class="form_controls"> -->
            <!-- 表单校验的input classes + required -->
            <!-- 表单校验的input data-tip="" 为进入时提醒 -->
            <!-- 表单校验的input data-valid="" 为验证规则，多个可使用‘||’分割；具体参照jquery-validate.js-->
            <!-- 表单校验的input data-error="" 为验证不通过时的提醒，需与 data-valid一一对应 -->
            <!-- 校验需自己填写 -->
			<tr>
        		<td class="sec_tit">
                    <label for="edit_dwdm">所属机构</label>
                </td>
                <td>
                    <div class="form-group has-feedback form_controls" style="width: 100%;">
                        <input type="hidden" id="edit_dwdm" name="dwdm" />
                        <input type="text" class="required form-control" id="edit_dwmc" name="dwmc" data-jzd-type="tree" data-jzd-code="DEPT" data-jzd-filter="" data-jzd-dm="edit_dwdm" data-jzd-mc="edit_dwmc" readonly data-valid="isNonEmpty" data-error="所属机构不能为空" >
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_dwdm');clearInput('edit_dwmc');"></span>
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_dwjc">单位简称</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" class="required form-control" id="edit_dwjc" name="dwjc" data-tip="例：惠公(刑)" data-valid="isNonEmpty||between:5-10" data-error="不能为空||长度为10位" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_cxz">查询章</label>
                </td>
                <td colspan="3">
                    <input type="hidden" name="cxz" id="cxz_mongo" value="">
                    <div class="form_controls" id="edit_cxz">

                    </div>
                </td>
        		<%--<td class="sec_tit">
                    <label for="edit_zfz">止付章</label>
                </td>
                <td>
                    <input type="hidden" name="zfz" id="zfz_mongo" value="">
                    <div class="form_controls" id="edit_zfz">

                    </div>
                </td>--%>
			</tr>
        </table>
    </form>
</body>
</html>
