<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
			<td class="sec_tit">所属机构</td>
			<td id="show_dwmc"></td>
			<td class="sec_tit">单位简称</td>
			<td id="show_dwjc"></td>
		</tr>
		<tr>
    		<td class="sec_tit">调证年份</td>
            <td id="show_dzyear"></td>
    		<td class="sec_tit">调证字号</td>
            <td id="show_dzz"></td>
		</tr>
		<tr>
    		<td class="sec_tit">录入单位</td>
            <td id="show_lrdwmc"></td>
			<td class="sec_tit">录入时间</td>
			<td id="show_rksj"></td>
		</tr>
		<tr>
    		<td class="sec_tit">修改单位</td>
            <td id="show_xgdwmc"></td>
    		<td class="sec_tit">修改时间</td>
            <td id="show_xgsj"></td>
		</tr>
		<tr>
			<td class="sec_tit">查询章</td>
			<td colspan="3">
				<input id="show_cxz" type="file" >
			</td>
			<%--<td class="sec_tit">止付章</td>
			<td>
				<input id="show_zfz" type="file" >
			</td>--%>
		</tr>
    </table>
</body>
</html>
