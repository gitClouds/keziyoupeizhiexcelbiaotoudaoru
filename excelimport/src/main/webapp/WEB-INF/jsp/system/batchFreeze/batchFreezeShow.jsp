<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
			<td class="sec_tit">用户姓名</td>
			<td id="show_yhxm"></td>
    		<td class="sec_tit">0单位1个人</td>
            <td id="show_type"></td>
		</tr>
		<tr>
    		<td class="sec_tit">0银行1三方</td>
            <td id="show_yhsf"></td>
			<td class="sec_tit">身份证号</td>
			<td id="show_sfzh"></td>
		</tr>
		<tr>
    		<td class="sec_tit">单位代码</td>
            <td id="show_dwdm"></td>
    		<td class="sec_tit">单位名称</td>
            <td id="show_dwmc"></td>
		</tr>
		<tr>
			<td class="sec_tit">冻结到期天数起</td>
			<td id="show_min"></td>
			<td class="sec_tit">冻结到期天数止</td>
			<td id="show_max"></td>
		</tr>
    </table>
</body>
</html>
