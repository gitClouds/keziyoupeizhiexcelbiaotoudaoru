<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <form class="form_chag" id="batchFreezeEditForm" name="batchFreezeEditForm">
        <input type="hidden" id="edit_curdType" name="curdType" >
        <input type="hidden" id="edit_id" name="id">
        <table class="table table-bordered">
        	<!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
            <!-- 请根据需要修改 可编辑项、hidden等 -->
            <!-- 表单校验的字段需要在input外加一层 <div class="form_controls"> -->
            <!-- 表单校验的input classes + required -->
            <!-- 表单校验的input data-tip="" 为进入时提醒 -->
            <!-- 表单校验的input data-valid="" 为验证规则，多个可使用‘||’分割；具体参照jquery-validate.js-->
            <!-- 表单校验的input data-error="" 为验证不通过时的提醒，需与 data-valid一一对应 -->
            <!-- 校验需自己填写 -->
			<tr>
                <td class="sec_tit">
                    <label for="edit_yhxm">用户姓名</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" class="form-control" id="edit_yhxm" name="yhxm" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_type">0单位1个人</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_type" name="type" data-tip="" data-valid="isNonEmpty||onlyInt||between:1" data-error="不能为空||只能是1位数字" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_yhsf">0银行1三方</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_yhsf" name="yhsf" data-tip="" data-valid="isNonEmpty||onlyInt||between:1" data-error="不能为空||只能是1位数字" >
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_sfzh">身份证号</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" class="required form-control" id="edit_sfzh" name="sfzh" data-tip="" data-valid="onlyInt" data-error="只能是数字" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_dwdm">单位代码</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_dwdm" name="dwdm" data-tip="" data-valid="isNonEmpty" data-error="不能为空" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_dwmc">单位名称</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_dwmc" name="dwmc" data-tip="" data-valid="isNonEmpty" data-error="不能为空" >
                    </div>
                </td>
			</tr>
			<tr>

        		<td class="sec_tit">
                    <label for="edit_min">冻结到期天数起</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_min" name="min" data-tip="" data-valid="isNonEmpty||onlyInt||between:2" data-error="不能为空||只能是1,2位数字" >
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_max">冻结到期天数止</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" class="required form-control" id="edit_max" name="max" data-tip="" data-valid="isNonEmpty||onlyInt||between:2" data-error="不能为空||只能是1,2位数字" >
                    </div>
                </td>
			</tr>

        </table>
    </form>
</body>
</html>
