<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="../../css/es/page.css">
<script src="../../resources/bootstrap/js/jquery.min.js"></script>
<script>


    var gjz = '${gjz}';//获取你输入的关键字；
    var tab = '${tab}';//获取你输入的关键字；
    var total = '${total}';//获取你输入的关键字；
    var page = '${page}';//获取你输入的关键字；
    var gjzsz = gjz.split(":");
    var keyword = "";
    if (gjzsz.length > 1) {
        keyword = gjzsz[0].trim();
    }

    function ghgjz() {

        if (keyword) {
            var keyword_regExp = new RegExp(keyword, "g");
            $("#bdkjqxx").find(".btxx").each(function () {
                var html = $(this).html();
                var newHtml = html.replace(keyword_regExp, "<span style='color:orange' >" + keyword + "</span>");//将找到的关键字替换，加上highlight属性；
                $(this).html(newHtml);
                if (html != newHtml) {
                    var gjz_html = $(this).next().html();
                    var gjz_newHtml = gjz_html.replace(gjz_html, "<span style='color:orange' >" + gjz_html + "</span>");//将找到的关键字替换，加上highlight属性；
                    $(this).next().html(gjz_newHtml);
                }

            });
            return;
        }
        if (gjz) {
            var regExp = new RegExp(gjz, "g");
            $("#bdkjqxx").find(".deatil").each(function () {
                var html = $(this).html();
                var newHtml = html.replace(regExp, "<span style='color:orange' >" + gjz + "</span>");//将找到的关键字替换，加上highlight属性；
                $(this).html(newHtml);

            });
        }

    }


</script>
<style>

    ul.pagination {
        display: inline-block;
        padding: 0;
        margin: 0;
    }

    ul.pagination li {display: inline;}

    ul.pagination li a {
        color: black;
        float: left;
        padding: 8px 16px;
        text-decoration: none;
    }

    ul.pagination li a.active {
        background-color: #1492FF;
        color: white;
    }

    ul.pagination li a:hover:not(.active) {background-color: #ddd;}

    .btxx {
        color: #939292;
        font-size: 14px;

    }

    .btxx_text {
        color: #939292;
        font-size: 14px;

    }

    .deatil {
        color: #666666;
        font-size: 12px;
        padding-right: 22px;
    }

    .deatil_text {
        margin: 10px;
        color: #666666;
        font-size: 12px;
        padding-right: 22px;
    }
</style>
<div class="detailSearch"><a>条件：<span id="tiaojian">${gjz==''?'无':gjz}</span></a>
    <a>命中：<span id="mingzhong">${total}</span></a>
    <a>资源：<span>${zyname}</span></a></div>
<div id="bdkjqxx">
    <div class="detail_content">
        <c:forEach items="${esData}" var="d">${d}</c:forEach>
    </div>
    <ul class="pagination" style="padding: 20px;">
        <li><a href="#" onclick="goPage(1)">首页</a></li>
        <li><a href="#" onclick="goPage(${page-1})">«</a></li>
        <c:forEach var="p" begin="${pageStart}" end="${pageEnd}">
            <li><a class="${page==p?'active':''}" href="#" onclick="goPage(${p})">${p}</a></li>
        </c:forEach>
        <li><a href="#" onclick="goPage(${page+1})">»</a></li>
<%--
        <li><a href="#" onclick="goPage(${pageNum})">最后一页</a></li>
--%>
    </ul>
    <script>
        ghgjz();
    </script>
</div>