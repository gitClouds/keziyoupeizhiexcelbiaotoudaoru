<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>全文检索</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
    <script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
    <style type="text/css">
        .main_tit h5 {
            color: #1492FF;
            font-size: 18px;
            line-height: 18px;
            text-indent: 10px;
            font-weight: 400;
            margin-top: 20px;
        }

        .main_content p {
            height: 30px;
            line-height: 20px;
            color: gray;
            cursor: pointer;
        }

        .main_tp {
            padding: 0 20px 20px;
            background: #FFF;
            margin-top: 10px;
            box-shadow: 0 0 8px #B9B9C8;
        }

        .nav_right {
            height: 100%;
            width: 79%;
            float: right;
            margin-top: 10px;
            margin-right: 10px;
        }

        .nav_left {
            height: 100%;
            width: 19%;
            float: left;
            margin-top: 10px;
            margin-left: 10px;
        }

        .main_tp {
            padding: 0 20px 20px;
            background: #FFF;
            margin-top: 10px;
            box-shadow: 0 0 8px #B9B9C8;
        }

        .main_tit {
            height: 60px;
            line-height: 60px;
            border-bottom: 1px solid #EDF0F6;
        }

        .main_tit {
            height: 60px;
            line-height: 60px;
            border-bottom: 1px solid #EDF0F6;
        }

        .fl {
            float: left;
        }

        .search input {
            width: 700px;
            height: 50px;
            font-size: 14px;
            line-height: 20px;
            color: #333;
            padding: 15px 20px;
            -moz-appearance: none;
            -webkit-appearance: none;
            border: 1px solid #108CEE;
        }

        .search .search-btn {
            display: inline-block;
            margin-left: -6px;
            cursor: pointer;
            color: #FFF;
            background-color: #108CEE;
            width: 115px;
            font-size: 14px;
            line-height: 50px;
            text-align: center;
            border: 1px solid #108CEE;
        }

        .content_left {
            height: 100%;
            width: 100%;
            float: left;
            margin-top: 10px;
        }

        .detailSearch {
            padding: 10px;
            border-bottom: 1px dashed #BBBBBB;
        }

        .detail_content {
            border: 1px solid #BBBBBB;
            clear: both;
            margin: 20px 0;
            color: #BBBBBB;
        }

        .btxx {
            color: #666666;
            font-size: 14px;
        }

        .tit_num {
            padding: 0 12px;
            height: 28px;
            display: inline-block;
            background-color: #1492FF;
            color: #FFF;
            text-align: center;
            font: 400 12px/28px "Microsoft YaHei";
        }

        .btxx {
            color: #666666;
            font-size: 14px;
        }

        .deatil {
            color: #222222;
            font-size: 12px;
            margin: 10px;
            line-height: 20px;
        }

        .button_num {
            padding: 0 7px;
            height: 28px;
            display: inline-block;
            background-color: #BBBBBB;
            color: #FFF;
            text-align: center;
            font: 400 12px/28px "Microsoft YaHei";
            border-radius: 10px;
            cursor: pointer;
            /*  margin-left: 20px;*/
            width: 20%;
            float: right;
        }

        .main_content p {
            height: 30px;
            line-height: 20px;
            color: gray;
            cursor: pointer;
        }

        .detailSearch {
            padding: 10px;
            border-bottom: 1px dashed #BBBBBB;
        }

        .detailSearch a {
            width: 30%;
            display: inline-block;
            text-decoration: none;
            color: #BBBBBB;
        }

        .detailSearch span {
            color: orange;
        }


        .colorblue {
            color: blue !important;
        }
    </style>
    <script>
        var gjz = '${gjz}';
        var tab = '${tab}';
        var ctx = '${ctx}';


        function loacl_search() {

            var sgjz = $("#gjz").val();
            var stab = $(".colorblue").attr("id");
            window.location.replace(ctx + "/es/es/searchResult.html?gjz=" + sgjz + "&tab=" + stab);
        }

        $(document).ready(function () {
            $(".fzzylb").find("p").click(function () {
                $(".fzzylb").find("p").each(function () {
                    $(this).removeClass("colorblue");
                });
                $(this).addClass("colorblue");
            })

            $("#gjz").keydown(function(event){
                if (event.keyCode == 13) {
                    loacl_search();
                }
            });

        })
    </script>
    <script src="${ctx}/js/es/qwjs.js"></script>
</head>
<body>
<div id="qwjsGz">
    <div class="nav_left">
        <div class="main_tp clearfix zylb">
            <div class="main_tit">
                <h5 class="fl">资源列表</h5>
            </div>
            <div class="resource_content fzzylb">
                <div class="main_content">
                    <c:forEach var="esTab" items="${esTabList}" varStatus="">
                        <p id="${esTab.id}">${esTab.name}<span class="button_num fr">${esTab.num}</span></p>
                    </c:forEach>
                </div>
            </div>
        </div>
    </div>
    <div id="test" class="nav_right">
        <div class="main_tp clearfix">
            <div class="main_tit">
                <h5 class="fl">搜索结果</h5>
            </div>
            <div class="search fl" style="margin-bottom: 15px;">
                <input type="text" id="gjz" placeholder="请输入" value="${gjz}">
                <span class="search-btn" onclick="loacl_search()">搜索</span>
            </div>
            <div class="resource_content" id="cxym">
            </div>
        </div>
        <div id="zyfl">
        </div>
    </div>
</div>
</body>
</html>