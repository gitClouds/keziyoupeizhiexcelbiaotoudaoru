<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>全文检索</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
    <script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
    <style type="text/css">
        .wrap {
            position: relative;
            z-index: 2;
        }

        .main {
            position: absolute;
            left: 50%;
            top: 30%;
            z-index: 90;
            width: 100%;
            margin-left: -555px;
        }

        .search-box {
            margin-top: 10%;
            width: 80%;
        }

        .inp-search {
            height: 64px;
            background: rgba(255, 255, 255, .8);
            border: 0;
            -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 10px;
            width: 100%;
            padding: 8px 0;
            font-size: 20px;
            font-family: '微软雅黑';
            text-indent: 12px;

        }


        .btn-search {
            display: block;
            width: 84px;
            height: 84px;
            background: url('../../resources/img/es/search.png') no-repeat center center;
            position: absolute;
            right: 15px;
            top: -7px;
            cursor: pointer;
            margin-top: 10%;
            width: 46%;
        }

        .wrap-bg {
            position: absolute;
            left: 0;
            top: 0;
            bottom: 0;
            right: 0;
            z-index: -1;
        }
    </style>
    <script>
        function loacl_search() {
            var gjz = $("#gjz").val();
            window.location.replace('${ctx}/es/es/searchResult.html?gjz=' + gjz);
        }

        $(document).ready(function () {
            $("#gjz").keydown(function (event) {
                if (event.keyCode == 13) {
                    loacl_search();
                }
            });
        })
    </script>
</head>
<body>
<div class="wrap">
    <div class="main">
        <div class="search-box">
            <input type="text" name="gjz" id="gjz" placeholder="请输入你需要查询的内容" class="inp-search">
            <span class="btn-search" onclick="loacl_search()">	</span>
        </div>
    </div>
    <div class="wrap-bg">
        <img style="width: 100%;background-repeat:no-repeat;background-size:cover" src="${ctx}/resources/img/es/img_bg.jpg">
    </div>
</div>
</body>
</html>