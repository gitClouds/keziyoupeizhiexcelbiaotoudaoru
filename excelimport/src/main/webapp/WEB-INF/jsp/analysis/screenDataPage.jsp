<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
    <style>
        .valid_message{
            color: red;
        }
    </style>
</head>
<body>
<!-- 主体内容 -->
<section class="content">
    <!--查询项表单 begin-->
    <c:if test="${empty analysis_pk}">
    <div class="row">
        <div class="col-xs-12">
            <ul class="box clearfix">
                <form class="form_chag form-inline" id="mxQueryForm" name="mxQueryForm">
                    <div class="form-group" style="width: 100%">
                        <label for="minje">交易金额</label>
                        <input type="text" style="width: 15%!important" value="200" class="required form-control" id="jyjebs" name="jyjebs" data-valid="isNonEmpty||onlyInt" data-error="不能为空||只能输入数字" >
                        <b>元的倍数</b>
                        <label for="ts">交易时间</label>
                         <input type="text" value="" id="jysjBegin" name="jysjBegin" style="width: 30%!important" onFocus="WdatePicker({lang:'zh-cn',isShowClear:false,dateFmt:'yyyyMMdd',maxDate:'%y-%M-%d'})" class="Wdate required form-control" data-valid="isNonEmpty" data-error="不能为空" >
						 <b>至</b>
						 <input type="text" value="" id="jysjEnd" name="jysjEnd"  style="width: 30%!important" onFocus="WdatePicker({lang:'zh-cn',isShowClear:false,dateFmt:'yyyyMMdd',maxDate:'%y-%M-%d'})" class="Wdate required form-control" data-valid="isNonEmpty" data-error="不能为空" >
                    </div>
                     <div class="form-group" style="width: 60%">
                        <label for="ts">交易时间段</label>
                        <input type="text" style="width: 15%!important" value="0" class="required form-control" id="jysjb" name="jysjb" data-valid="isNonEmpty||onlyInt||between:1-2||maxIntVal:24" data-error="不能为空||只能输入数字||长度为1-2||不能大于24"  >
                        -
                        <input type="text" style="width: 15%!important" value="9" class="required form-control" id="jysje" name="jysje" data-valid="isNonEmpty||onlyInt||between:1-2||maxIntVal:24" data-error="不能为空||只能输入数字||长度为1-2||不能大于24"  >
                        <b>点</b>
                        <label for="nlevel">涉毒人员</label>
                        <select class="form-control" id="isxdsd" name="isxdsd" style="width: 30%" >
                            <option value=""></option>
                            <option value="0">否</option>
                            <option value="1">是</option>
                            <option value="2">未知</option>
                        </select>
                     </div>
                         <div class="form-group" style="width: 60%">
                         <label for="ts">账号</label>
                         <input type="text" style="width: 30%!important"  class="form-control" id="cxzh" name="cxzh"    >

                     </div>
                    <div class="form-group " style="width: 10%;float: right">
                        <button type="button" class="btn btn-info" onclick="fxData(this);">分析数据</button>
                    </div>
                    <div class="form-group " style="width: 10%;float: right">
                        <button type="button" class="btn btn-info" onclick="search();">筛选数据</button>
                    </div>
                </form>
            </ul>
        </div>
    </div>
    </c:if>
    <c:if test="${not empty analysis_pk}">
    	<div class="col-xs-12">
            <ul class="box clearfix">
            	<form class="form_chag form-inline" id="mxQueryForm" name="mxQueryForm">
                 <div class="form-group" style="width: 100%">
                     <label for="minje">交易金额</label>
                     <input type="text" style="width: 15%!important" value="${conditions.jyjebs }" readonly="readonly" class="required form-control" id="jyjebs" name="jyjebs" >
                     <b>元的倍数</b>
                     <label for="ts">交易时间</label>
                      <input type="text" value="${conditions.jysjBegin }" id="jysjBegin" name="jysjBegin" readonly="readonly" style="width: 30%!important" class="Wdate required form-control" >
					 <b>至</b>
					 <input type="text" value="${conditions.jysjEnd }" id="jysjEnd" name="jysjEnd" readonly="readonly" style="width: 30%!important" class="Wdate required form-control" >
                 </div>
                  <div class="form-group" style="width: 60%">
                     <label for="ts">交易时间段</label>
                     <input type="text" style="width: 15%!important" value="${conditions.jysjb }" readonly="readonly" class="required form-control" id="jysjb" name="jysjb">
                     -
                     <input type="text" style="width: 15%!important" value="${conditions.jysje }" readonly="readonly" class="required form-control" id="jysje" name="jysje" >
                     <b>点</b>
                 </div>
                </form>
            </ul>
        </div>
    </c:if>
    <div><h3>三方明细</h3></div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="sfMxJgListGrid" class="table table-bordered table-striped table-condensed">

                    </table>
                    <!--toolbar -->
                    <c:if test="${empty analysis_pk}">
	                     <div class="btn-group" id="toolbarSf">
	                        <i class="btn" onclick="exportSfMxJgList();">
	                            <i class="fa fa-cloud-download fa-2x"></i>
	                        </i>
	                    </div>
                    </c:if>
                </div>
            </div>
        </div>
    </div>
    <div><h3>银行卡明细</h3></div>
	<div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="yhkMxJgListGrid" class="table table-bordered table-striped table-condensed">

                    </table>
                    <!--toolbar -->
                    <c:if test="${empty analysis_pk}">
	                    <div class="btn-group" id="toolbarYhk">
	                        <i class="btn" onclick="exportYhkMxJgList();">
	                            <i class="fa fa-cloud-download fa-2x"></i>
	                        </i>
	                    </div>
                    </c:if>
                </div>
            </div>
        </div>
    </div>
    <!-- 详情模态框（Modal） -->
    <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="showModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="showMessage">
                    <%--<jsp:include page="${ctx}/znzf/base/show"/>--%>
                    <iframe src="" id="zcShowIframe" width="100%" scrolling="no" frameborder="0"></iframe>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    var ctx = "${ctx}";
    var urlYhk = ctx+'/analysis/manulAnalysis/yhkMxJgList/list';
    var urlSf = ctx+'/analysis/manulAnalysis/sfMxJgList/list';
    if("${analysis_pk}" != ""){
    	urlYhk = urlYhk+'?analysis_pk=${analysis_pk}';
    	urlSf = urlSf+'?analysis_pk=${analysis_pk}';
    }
</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-closeable-tab.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/analysis/sfMxJgList.js"></script>
<script src="${ctx}/js/analysis/yhkMxJgList.js"></script>
<script>
function search() {
	var result = valiData();
	if(!result.flag){
		alert(result.msg);
		return;
	}
	$("#sfMxJgListGrid").bootstrapTable("refresh");
	$("#yhkMxJgListGrid").bootstrapTable("refresh");
}
function fxData(obj){
	var result = valiData();
	if(!result.flag){
		alert(result.msg);
		return;
	}
    
    var reqData = $('#mxQueryForm').serializeJsonObject();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: ctx+"/analysis/manulAnalysis/fxData" ,
        data: reqData,
        beforeSend: function () {
            loading("分析数据中，请稍后......");
        },
        success : function(data) {
        	top.layer.close(indexLoading);
        	if(data.result){
        		top.layer.alert("分析数据功能操作成功！可在分析结果查看菜单查看分析信息", {icon: 1, title: '提示'});
                var item = {'id':'fxjg','name':'分析结果查看','url':ctx+'/analysis/manulAnalysis/page'};
                iframeTab.parentAddIframe(item);
        	}else{
        		if(data.code == '1'){
        			top.layer.alert("分析数据失败：没有筛选到数据！", {icon: 2, title: '提示'});
        		}else{
        			top.layer.alert("分析数据失败：请求异常！", {icon: 2, title: '提示'});
        		}
        	}
        },
        error : function() {
        	top.layer.close(indexLoading);
        	top.layer.alert("分析数据失败：请求异常！", {icon: 2, title: '提示'});
        }
    });
}

function loading(msg) {
	indexLoading = top.layer.msg(msg, {
        icon: 16,
        shade: [0.1, '#666666'],
        time: false  //不自动关闭
    })
}
function valiData(){
	var result={};
	var flag = $('#mxQueryForm').validate('submitValidate');
    if (!flag){
    	result.flag=false;
    	result.msg='信息校验不通过，请根据提示填写相应内容';
        return result;
    }
    var jysjBegin=$("#jysjBegin").val();
	var jysjEnd=$("#jysjEnd").val();
	if(parseInt(jysjBegin) >= parseInt(jysjEnd)){
		result.flag=false;
	    result.msg='交易时间：前段时间不能大于等于后端时间！';
	    return result;
	}
	var jysjb=$("#jysjb").val();
	var jysje=$("#jysje").val();
	if(parseInt(jysjb) >= parseInt(jysje)){
		result.flag=false;
	    result.msg='交易时间段：前段时间不能大于等于后端时间！';
	    return result;
	}
	result.flag=true;
	return result;
}
function isXdSdRyFormatter(value, row, index){
	if(row.xdrypks == '0' && row.sdrypks == '0'){
		return "分析中";
	}else if(row.xdrypks == '1' && row.sdrypks == '1'){
		return "否";
	}else{
		var result = "";
		if(row.xdrypks != null && row.xdrypks.length > 1 ){
			result += "<a href='javascript:void(0);' id='xdBtn' class='btn-sm btn-info' title='吸毒人员'>吸毒人员</a>";
		}
		if(row.sdrypks != null && row.sdrypks.length > 1){
			result += "&nbsp;<a href='javascript:void(0);' id='sdBtn' class='btn-sm btn-info' title='涉毒人员'>涉毒人员</a>";
		}
		return result;
	}
		
}

function yhAndSfOperationFormatter(value, row, index) {
	if(row.ztqqpk != null){
		return "<a href='javascript:void(0);' id='ztBtn' class='btn-sm btn-info' title='主体'>主体</a>";
	}
}
window.yhAndSfShowOperateEvents= {
	 "click #ztBtn":function (e,vale,row,index) {
	        $("#showModalLabel").text("主体信息");
	        if(row.ztqqtype == '1'){
	        	$("#zcShowIframe").attr("src",ctx+"/znzf/base/show?zcType=yhkZt&pk="+row.ztqqpk);
	        }else if(row.ztqqtype == '2'){
	        	$("#zcShowIframe").attr("src",ctx+"/znzf/base/show?zcType=sfZt&pk="+row.ztqqpk);
	        }
	        $("#showModal").modal(); 
     },
}

window.isXdSdRyEvents= {
	 "click #xdBtn":function (e,vale,row,index) {
		 ryxq(2,row.xdrypks);
	 },
	 "click #sdBtn":function (e,vale,row,index) {
		 ryxq(1,row.sdrypks);
	 },
}

function ryxq(lx,pks){
    var item = {'id':'jdzhxxx','name':'禁毒综合信息','url':ctx+'/jdzh/page?pks='+pks+'&lx='+lx};
    iframeTab.parentAddIframe(item);
}

function disabRow(tableId,pk) {
    var reqData = {"pk":pk};
    var url="";
    if(tableId == "sfMxJgListGrid"){
    	url = ctx+'/analysis/manulAnalysis/sfMxJgList/disab';
    }else if(tableId == "yhkMxJgListGrid"){
    	url = ctx+'/analysis/manulAnalysis/yhkMxJgList/disab';
    }
    $.ajax({
        type: "POST",
        dataType: "json",
        data:reqData,
        url:url,
        async:false,
        success: function (result) {
            if (result.msg=="true"){
                $('#'+tableId).bootstrapTable("refresh");
            }else{
                alert(result.msg);
            }
        },
        error : function() {
            alert("请求异常");
        }
    });
}
</script>
</body>
</html>