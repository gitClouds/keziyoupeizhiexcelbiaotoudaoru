<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">字段注释</td>
            <td id="show_pk"></td>
    		<td class="sec_tit">创建时间</td>
            <td id="show_create_date"></td>
		</tr>
		<tr>
    		<td class="sec_tit">创建人</td>
            <td id="show_createor_id"></td>
    		<td class="sec_tit">创建人</td>
            <td id="show_createor_name"></td>
		</tr>
		<tr>
    		<td class="sec_tit">筛选条件</td>
            <td id="show_conditions"></td>
		</tr>
    </table>
</body>
</html>
