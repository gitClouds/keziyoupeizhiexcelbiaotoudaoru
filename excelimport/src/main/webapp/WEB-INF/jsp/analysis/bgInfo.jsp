<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
    <style type="text/css">
        .jumbotron {
            padding-top: 5px;padding-bottom: 5px;background-color: #ffffff;
        }
    </style>
</head>
<body>
<!-- 主体内容 -->
<section class="content">

    <div class="jumbotron">
        <h4 class="display-4">
            ${cxTjInfo}
            <a href="${ctx}/analysis/manulAnalysis/bgInfo?pk=${pk}&lx=dc">
                <i class="btn" style="float: right" title="导出">
                    <i class="fa fa-cloud-download fa-2x"></i>
                </i>
            </a>
            <br/>
            ${cxZsInfo}
        </h4>
        <p class="lead">
            ${sfsdInfo}
            <br> ${sfxdInfo}
            <br> ${yhksdInfo}
            <br> ${yhkxdInfo}
        </p>
    </div>
    <div class="jumbotron">
        <h4 class="display-4">
            三方账号存在涉毒人员的数据如下:
        </h4>
        <table class="table">
            <thead>
            <tr>
                <td>账号</td>
                <td>查询账户</td>
                <td>交易时间</td>
                <td>交易类型</td>
                <td>交易金额</td>
                <td>人员姓名</td>
                <td>人员关系网图</td>
                <td>人员号码</td>
                <td>出现次数</td>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${sfSdList}" var="sf">
                <tr style="background-color: ${sf.ZJHM_NUM>1?'#f5c4c4':''};">
                    <td><span style="cursor: pointer;color: blue;" onclick="openZt('${sf.ZTQQTYPE}','${sf.ZTQQPK}')">${sf.ZH}</span>
                    </td>
                    <td>${sf.QQZH} </td>
                    <td>${sf.JYSJ} </td>
                    <td>${sf.JDBZ} </td>
                    <td>${sf.JYJE} </td>
                    <td>${sf.XM}</td>
                    <td>
                        <a target="_blank" href="${ctx}/analysis/manulAnalysis/person_relationMap_page?zh=${sf.ZH}&rypks=${sf.SDRYPKS}&lx=1">人员关系</a>
                    </td>
                        <%--<td>${sf.ZJHM} </td>--%>
                    <td>
                        <span style="cursor: pointer;color: blue;" onclick="ryxq('1','${sf.SDRYPKS}');">
                                ${sf.ZJHM}</span>
                    </td>

                    <td>${sf.ZJHM_NUM} </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <div class="jumbotron">
        <h4 class="display-4">
            三方账号存在吸毒人员的数据如下:
        </h4>
        <table class="table">
            <thead>
            <tr>
                <td>账号</td>
                <td>查询账户</td>
                <td>交易时间</td>
                <td>交易类型</td>
                <td>交易金额</td>
                <td>人员姓名</td>
                <td>人员关系网图</td>
                <td>人员号码</td>
                <td>出现次数</td>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${sfXdList}" var="sf">
                <tr style="background-color: ${sf.ZJHM_NUM>1?'#f5c4c4':''};">
                    <td><span style="cursor: pointer;color: blue;" onclick="openZt('${sf.ZTQQTYPE}','${sf.ZTQQPK}')">${sf.ZH}</span>
                    </td>
                    <td>${sf.QQZH} </td>
                    <td>${sf.JYSJ} </td>
                    <td>${sf.JDBZ} </td>
                    <td>${sf.JYJE} </td>
                    <td>${sf.XM} </td>
                    <td>
                        <a target="_blank" href="${ctx}/analysis/manulAnalysis/person_relationMap_page?zh=${sf.ZH}&rypks=${sf.XDRYPKS}&lx=2">人员关系</a>
                    </td>
                    <td>${sf.ZJHM} </td>
                    <td><span style="cursor: pointer;color: blue;" onclick="ryxq('2','${sf.XDRYPKS}')">${sf.ZJHM}</span></td>

                    <td>${sf.ZJHM_NUM} </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <div class="jumbotron">
        <h4 class="display-4">
            银行账号存在涉毒人员的数据如下:</h4>
        <table class="table">
            <thead>
            <tr>
                <td>账号</td>
                <td>查询账户</td>
                <td>交易时间</td>
                <td>交易类型</td>

                <td>交易金额</td>

                <td>人员姓名</td>
                <td>人员关系网图</td>
                <td>人员号码</td>
                <td>出现次数</td>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${yhkSdList}" var="yhk">
                <tr style="background-color: ${yhk.ZJHM_NUM>1?'#f5c4c4':''};">
                    <td>
                        <span style="cursor: pointer;color: blue;" onclick="openZt('${yhk.ZTQQTYPE}','${yhk.ZTQQPK}')">${yhk.ZH}</span>
                    </td>
                    <td>${yhk.QQZH} </td>
                    <td>${yhk.JYSJ} </td>
                    <td>${yhk.JDBZ} </td>
                    <td>${yhk.JYJE} </td>
                    <td>${yhk.XM} </td>
                    <td>
                        <a target="_blank" href="${ctx}/analysis/manulAnalysis/person_relationMap_page?zh=${yhk.ZH}&rypks=${yhk.SDRYPKS}&lx=3">人员关系</a>
                    </td>
                        <%--  <td>${yhk.ZJHM} </td>--%>
                    <td><span style="cursor: pointer;color: blue;" onclick="ryxq('1','${yhk.SDRYPKS}');">${yhk.ZJHM}</span></td>

                    <td>${yhk.ZJHM_NUM} </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <div class="jumbotron">
        <h4 class="display-4">
            银行账号存在吸毒人员的数据如下:</h4>
        <table class="table">
            <thead>
            <tr>
                <td>账号</td>
                <td>查询账户</td>
                <td>交易时间</td>
                <td>交易类型</td>
                <td>交易金额</td>
                <td>人员姓名</td>
                <td>人员关系网图</td>
                <td>人员号码</td>
                <td>出现次数</td>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${yhkXdList}" var="yhk">
                <tr style="background-color: ${yhk.ZJHM_NUM>1?'#f5c4c4':''};">
                    <td>
                        <span style="cursor: pointer;color: blue;" onclick="openZt('${yhk.ZTQQTYPE}','${yhk.ZTQQPK}')">${yhk.ZH}</span>
                    </td>
                    <td>${yhk.QQZH} </td>
                    <td>${yhk.JYSJ} </td>
                    <td>${yhk.JDBZ} </td>
                    <td>${yhk.JYJE} </td>
                    <td>${yhk.XM} </td>
                    <td>
                        <a target="_blank" href="${ctx}/analysis/manulAnalysis/person_relationMap_page?zh=${yhk.ZH}&rypks=${yhk.XDRYPKS}&lx=4">人员关系</a>
                    </td>
                    <td><span style="cursor: pointer;color: blue;" onclick="ryxq('2','${yhk.XDRYPKS}');">${yhk.ZJHM}</span></td>
                    <td>${yhk.ZJHM_NUM} </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>


    <!-- 详情模态框（Modal） -->
    <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="showModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="showMessage">
                    <%--<jsp:include page="${ctx}/znzf/base/show"/>--%>
                    <iframe src="" id="zcShowIframe" width="100%" scrolling="no" frameborder="0"></iframe>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div>
        </div>
    </div>

</section>


<script>
    var ctx = "${ctx}";

    function openZt(ztqqtype, ztqqpk) {

        $("#showModalLabel").text("主体信息");
        if (ztqqtype == '1') {
            $("#zcShowIframe").attr("src", ctx + "/znzf/base/show?zcType=yhkZt&pk=" + ztqqpk);
        } else if (ztqqtype == '2') {
            $("#zcShowIframe").attr("src", ctx + "/znzf/base/show?zcType=sfZt&pk=" + ztqqpk);
        }
        $("#showModal").modal();

    }


    function ryxq(lx, pks) {
        var url = "";
        if (lx == '1') { //涉毒
            url = ctx + '/jdzh/sdpage?pks=' + pks + '&lx=' + lx;
        } else {//吸毒
            url = ctx + '/jdzh/xdpage?pks=' + pks + '&lx=' + lx;
        }
        var item = {'id': 'jdzhxxx', 'name': '禁毒综合信息', 'url': url};
        iframeTab.parentAddIframe(item);
    }

</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-closeable-tab.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/js/common.js"></script>
</body>
</html>