<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <script type="text/javascript" src="${ctx}/resources/group/js/plugins/d3/d3.v3.min.js"></script>
    <script type="text/javascript" src="${ctx}/resources/group/js/plugins/jquery/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="${ctx}/resources/layer/layer.js" charset="utf-8"></script>
    <link rel="stylesheet" href="${ctx}/resources/group/css/demo.css"/>
    <script>
        //iframe真正解决高度自适应
        function changeFrameHeight(iframeId){
            var ifm= document.getElementById(iframeId);
            ifm.height=document.documentElement.clientHeight;

        }

        window.onresize=function(){
            changeFrameHeight();

        }
    </script>
</head>
<body>
<jsp:include page="../loading.jsp"/>
<!-- 模态框（Modal） -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:90%;height: auto;">
        <div class="modal-content" style="border-radius:5px" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    明细信息
                </h4>
            </div>
            <div class="modal-body">
                <%--<iframe src="${ctx}/znzf/jlt/echarts_page" id="echartsShow" width="100%" scrolling="no" frameborder="0"></iframe>--%>
                <%--<jsp:include page="echarts.jsp"/>--%>
                <iframe src="" id="zcShowIframe" width="100%" scrolling="no" frameborder="0"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>
<h1 id="msg" style="margin-left: 49%;margin-top: 45%;display: none;">未查询到数据</h1>
<div style="margin-top: 10px;margin-left: 10px;" class="form-inline">
    <label>选择首次交易日期:</label>
    <select onclick="selectOp()" class="form-control" id="selection_date"></select>
</div>
<iframe src="${ctx}/znzf/jlt/relation_map_page?pk=${list[0]["PK"]}" style="margin-top: 30px;" id="rygxtb" width="100%" height="100%" scrolling="no" frameborder="0"></iframe>
<script type="text/javascript">
    $(function () {
        var list = '${list}';
        var data = JSON.parse(list);
        if (data.length <= 0){
            $("#msg").show();
            return;
        }
        var op = "";
        $.each(data, function (index, item) {
            op+='<option value="'+item.PK+'">'+item.FIRSTDATE+'</option>'
        });
        $("#selection_date").append(op);

    })
    function selectOp() {
        var pk = $('#selection_date option:selected').val();
        if(pk){
            window.location.href="${ctx}/analysis/manulAnalysis/person_relationMap_page?zh=${zh}&rypks=${rypks}&lx=${lx}";
        }
    }

</script>
<script>
    function showModel(d) {
        console.info(d);
        var info = d;
        var zhlx = info.source.zhlx;
        var mxqqpk = info.source.mxqqpk;
        var zh = info.source.cardnumber;
        var xjzh = info.xjzh;
        var zcType = '';
        if(zhlx && zhlx=='1'){
            zcType="yhkMx";
        }
        if(zhlx && zhlx=='2'){
            zcType="sfMx";
        }
        $("#zcShowIframe").attr("src","${ctx}/znzf/base/show?zcType="+zcType+"&pk="+mxqqpk+"&zh="+zh+"&xjzh="+xjzh);
        $("#myModal").modal();
    }
    function flashMap(){
        document.getElementById('rygxtb').contentWindow.location.reload(true);
    }
</script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>