<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">我方账号新1</td>
            <td id="show_wf_wxzh_x1"></td>
    		<td class="sec_tit">我方开户证件</td>
            <td id="show_wf_khzj"></td>
		</tr>
		<tr>
    		<td class="sec_tit">我方支付帐号</td>
            <td id="show_wfzfzh"></td>
    		<td class="sec_tit">我方开户姓名</td>
            <td id="show_wf_khxm"></td>
		</tr>
		<tr>
    		<td class="sec_tit">交易类型</td>
            <td id="show_jylx"></td>
    		<td class="sec_tit">对方支付帐号</td>
            <td id="show_dfzfzh"></td>
		</tr>
		<tr>
    		<td class="sec_tit">交易次数</td>
            <td id="show_jycs"></td>
    		<td class="sec_tit">交易金额</td>
            <td id="show_jyje"></td>
		</tr>
		<tr>
    		<td class="sec_tit">是否本地户籍人员对手</td>
            <td id="show_sfbdhjryds"></td>
		</tr>
    </table>
</body>
</html>
