<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
</head>
<body>
<!-- 主体内容 -->
<section class="content">
	<!--查询项表单 begin-->
    <div class="row">
        <div class="col-xs-12">
            <ul class="box clearfix">
                <form class="form_chag form-inline" name="vZhSdryzbqcQueryForm" id="vZhSdryzbqcQueryForm">
                	<!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
                	<!-- 请根据需要修改 查询检索项 -->
                    	<div class="form-group">
	                        <label for="xm">姓名</label>
	                        <input type="text" class="form-control" id="xm" name="xm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zjhmyh">证件号码优化</label>
	                        <input type="text" class="form-control" id="zjhmyh" name="zjhmyh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="xyrdh">嫌疑人电话</label>
	                        <input type="text" class="form-control" id="xyrdh" name="xyrdh" >
	                    </div>
                    	<!-- <div class="form-group">
	                        <label for="spsj">审批时间</label>
	                        <input type="text" class="form-control" id="spsj" name="spsj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ryhjsd">人员户籍属地</label>
	                        <input type="text" class="form-control" id="ryhjsd" name="ryhjsd" >
	                    </div>
                    	<div class="form-group">
	                        <label for="rydz">人员地址</label>
	                        <input type="text" class="form-control" id="rydz" name="rydz" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sldw">受理单位</label>
	                        <input type="text" class="form-control" id="sldw" name="sldw" >
	                    </div>
                    	<div class="form-group">
	                        <label for="aq">备注（案情）</label>
	                        <input type="text" class="form-control" id="aq" name="aq" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ajbhzjhm">案件编号证件号码姓名</label>
	                        <input type="text" class="form-control" id="ajbhzjhm" name="ajbhzjhm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sjly">数据来源</label>
	                        <input type="text" class="form-control" id="sjly" name="sjly" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ll">年龄</label>
	                        <input type="text" class="form-control" id="ll" name="ll" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sfgtry">是否港台人员</label>
	                        <input type="text" class="form-control" id="sfgtry" name="sfgtry" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ch">绰号/别名</label>
	                        <input type="text" class="form-control" id="ch" name="ch" >
	                    </div>
                    	<div class="form-group">
	                        <label for="csrq">出生日期</label>
	                        <input type="text" class="form-control" id="csrq" name="csrq" >
	                    </div>
                    	<div class="form-group">
	                        <label for="qq">QQ</label>
	                        <input type="text" class="form-control" id="qq" name="qq" >
	                    </div>
                    	<div class="form-group">
	                        <label for="gj">国籍</label>
	                        <input type="text" class="form-control" id="gj" name="gj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sftw">收否台湾</label>
	                        <input type="text" class="form-control" id="sftw" name="sftw" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zjzl">证件种类</label>
	                        <input type="text" class="form-control" id="zjzl" name="zjzl" >
	                    </div>
                    	<div class="form-group">
	                        <label for="hjdxz">户籍地详址</label>
	                        <input type="text" class="form-control" id="hjdxz" name="hjdxz" >
	                    </div>
                    	<div class="form-group">
	                        <label for="wx">微信</label>
	                        <input type="text" class="form-control" id="wx" name="wx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="qtzjhm">其他证件级号码</label>
	                        <input type="text" class="form-control" id="qtzjhm" name="qtzjhm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="xmbc">姓名别称</label>
	                        <input type="text" class="form-control" id="xmbc" name="xmbc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="rylbzw">人员类别中午</label>
	                        <input type="text" class="form-control" id="rylbzw" name="rylbzw" >
	                    </div>
                    	<div class="form-group">
	                        <label for="lxfsdh">联系方式电话</label>
	                        <input type="text" class="form-control" id="lxfsdh" name="lxfsdh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sfam">是否澳门</label>
	                        <input type="text" class="form-control" id="sfam" name="sfam" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sfxg">是否香港</label>
	                        <input type="text" class="form-control" id="sfxg" name="sfxg" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zxghqc">最新抓获去重（计算）</label>
	                        <input type="text" class="form-control" id="zxghqc" name="zxghqc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sfjsn">是否近三年（计算）</label>
	                        <input type="text" class="form-control" id="sfjsn" name="sfjsn" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sfbd">是否本地（计算）</label>
	                        <input type="text" class="form-control" id="sfbd" name="sfbd" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ryhjgsx">人员户籍归属新（计算）</label>
	                        <input type="text" class="form-control" id="ryhjgsx" name="ryhjgsx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="hjdxxdzx">户籍地详细地址新（计算）</label>
	                        <input type="text" class="form-control" id="hjdxxdzx" name="hjdxxdzx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="gathqtrybq">港澳台和其他人员标签</label>
	                        <input type="text" class="form-control" id="gathqtrybq" name="gathqtrybq" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zjzhqk">最近抓获情况</label>
	                        <input type="text" class="form-control" id="zjzhqk" name="zjzhqk" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sfxs">是否刑事（计算）</label>
	                        <input type="text" class="form-control" id="sfxs" name="sfxs" >
	                    </div> -->
                                        
                    <div class="form-group ">
                        <button type="button" style="margin-left: 8%" class="btn btn-info" onclick="search();">检索</button>
                    </div>
                </form>
            </ul>
        </div>
    </div>
	<!--查询项表单 end-->
	<!--结果展示 begin-->
	<div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="vZhSdryzbqcGrid" class="table table-bordered table-striped table-condensed">

                    </table>
                    <!--toolbar -->
                    <div class="btn-group" id="toolbar">
                        <i class="btn" onclick="exportVZhSdryzbqc();">
                            <i class="fa fa-cloud-download fa-2x"></i>
                        </i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--结果展示 end-->
    <!-- 提示模态框（Modal） -->
    <div class="modal fade" id="tipsModal" tabindex="-1" role="dialog" aria-labelledby="tipsModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="tipsModalLabel">提示</h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="tipsMessage"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" id="tipsConfirmBtn">确定</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 详情模态框（Modal） -->
    <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="showModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="showMessage">
                    <jsp:include page="vZhSdryzbqcShow.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 编辑模态框（Modal） -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="editModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="editMessage">
                    <jsp:include page="vZhSdryzbqcForm.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" id="editConfirmBtn">保存</button>
                </div>
            </div>
        </div>
    </div>
    
    <!-- 字典、树形字典展示框。如果page、form均无字典项，可无需此div -->
    <div id="treeDiv">
        <input type="text" class="form-control" id="searchTree"  placeholder="支持文本检索">
        <div id="tree"></div>
    </div>
</section>
<script>
    var ctx = "${ctx}";
</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/wxjyyjmx/vZhSdryzbqc.js"></script>
</body>
</html>