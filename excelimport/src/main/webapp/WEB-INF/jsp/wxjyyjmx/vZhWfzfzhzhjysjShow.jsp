<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">我方支付账号</td>
            <td id="show_wfzfzh"></td>
    		<td class="sec_tit">交易余额</td>
            <td id="show_jyye"></td>
		</tr>
		<tr>
    		<td class="sec_tit">交易主体的出入账标识</td>
            <td id="show_jyztdcrzbs"></td>
    		<td class="sec_tit">对方支付账号</td>
            <td id="show_dfzfzh"></td>
		</tr>
		<tr>
    		<td class="sec_tit">交易类型</td>
            <td id="show_jylx"></td>
    		<td class="sec_tit">交易时间</td>
            <td id="show_jysj"></td>
		</tr>
		<tr>
    		<td class="sec_tit">交易金额</td>
            <td id="show_jyje"></td>
    		<td class="sec_tit">我方最后交易情况</td>
            <td id="show_wfzhjyqk"></td>
		</tr>
    </table>
</body>
</html>
