<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
</head>
<body>
<!-- 主体内容 -->
<section class="content">
	<!--查询项表单 begin-->
    <div class="row">
        <div class="col-xs-12">
            <ul class="box clearfix">
                <form class="form_chag form-inline" name="vZhQzfzhv1QueryForm" id="vZhQzfzhv1QueryForm">
                	<!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
                	<!-- 请根据需要修改 查询检索项 -->
                    	<div class="form-group">
	                        <label for="dlh">登录号</label>
	                        <input type="text" class="form-control" id="dlh" name="dlh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="xm_qymc">姓名/企业名称</label>
	                        <input type="text" class="form-control" id="xm_qymc" name="xm_qymc" >
	                    </div><%--
                    	<div class="form-group">
	                        <label for="yhkrzzt">银行卡认证状态</label>
	                        <input type="text" class="form-control" id="yhkrzzt" name="yhkrzzt" >
	                    </div>
                    	<div class="form-group">
	                        <label for="wxh">微信号</label>
	                        <input type="text" class="form-control" id="wxh" name="wxh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zjlx">证件类型</label>
	                        <input type="text" class="form-control" id="zjlx" name="zjlx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="bdsjh">绑定手机号</label>
	                        <input type="text" class="form-control" id="bdsjh" name="bdsjh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cxzh">查询账号</label>
	                        <input type="text" class="form-control" id="cxzh" name="cxzh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zfzh">支付账号</label>
	                        <input type="text" class="form-control" id="zfzh" name="zfzh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zjh">证件号</label>
	                        <input type="text" class="form-control" id="zjh" name="zjh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zhlb">账号类别</label>
	                        <input type="text" class="form-control" id="zhlb" name="zhlb" >
	                    </div>
                    	<div class="form-group">
	                        <label for="yhklx">银行卡类型</label>
	                        <input type="text" class="form-control" id="yhklx" name="yhklx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zzyxq">证照有效期</label>
	                        <input type="text" class="form-control" id="zzyxq" name="zzyxq" >
	                    </div>
                    	<div class="form-group">
	                        <label for="qczd">去重字段</label>
	                        <input type="text" class="form-control" id="qczd" name="qczd" >
	                    </div>
                    	<div class="form-group">
	                        <label for="yhkssyhmc">银行卡所属银行名称</label>
	                        <input type="text" class="form-control" id="yhkssyhmc" name="yhkssyhmc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="yhkyxq">银行卡有效期</label>
	                        <input type="text" class="form-control" id="yhkyxq" name="yhkyxq" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sj">时间</label>
	                        <input type="text" class="form-control" id="sj" name="sj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="yhkqtxx">银行卡其他信息</label>
	                        <input type="text" class="form-control" id="yhkqtxx" name="yhkqtxx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="yhkkh">银行卡卡号</label>
	                        <input type="text" class="form-control" id="yhkkh" name="yhkkh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="qqh">QQ号</label>
	                        <input type="text" class="form-control" id="qqh" name="qqh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="yhkssyhjgbm">银行卡所属银行机构编码</label>
	                        <input type="text" class="form-control" id="yhkssyhjgbm" name="yhkssyhjgbm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="bdsjh_wxzt">绑定手机号《微信主体》</label>
	                        <input type="text" class="form-control" id="bdsjh_wxzt" name="bdsjh_wxzt" >
	                    </div>
                    	<div class="form-group">
	                        <label for="bdyhk_wxzt">绑定银行卡《微信主体》</label>
	                        <input type="text" class="form-control" id="bdyhk_wxzt" name="bdyhk_wxzt" >
	                    </div>--%>
                                        
                    <div class="form-group ">
                        <button type="button" style="margin-left: 8%" class="btn btn-info" onclick="search();">检索</button>
                    </div>
                </form>
            </ul>
        </div>
    </div>
	<!--查询项表单 end-->
	<!--结果展示 begin-->
	<div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="vZhQzfzhv1Grid" class="table table-bordered table-striped table-condensed">

                    </table>
                    <!--toolbar -->
                    <div class="btn-group" id="toolbar">
                        <%--<i class="btn" onclick="addVZhQzfzhv1();">
                            <i class="fa fa-plus-square fa-2x"></i>
                        </i>--%>
                        <i class="btn" onclick="exportVZhQzfzhv1();">
                            <i class="fa fa-cloud-download fa-2x"></i>
                        </i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--结果展示 end-->
    <!-- 提示模态框（Modal） -->
    <div class="modal fade" id="tipsModal" tabindex="-1" role="dialog" aria-labelledby="tipsModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="tipsModalLabel">提示</h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="tipsMessage"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" id="tipsConfirmBtn">确定</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 详情模态框（Modal） -->
    <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="showModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="showMessage">
                    <jsp:include page="vZhQzfzhv1Show.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 编辑模态框（Modal） -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="editModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="editMessage">
                    <jsp:include page="vZhQzfzhv1Form.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" id="editConfirmBtn">保存</button>
                </div>
            </div>
        </div>
    </div>
    
    <!-- 字典、树形字典展示框。如果page、form均无字典项，可无需此div -->
    <div id="treeDiv">
        <input type="text" class="form-control" id="searchTree"  placeholder="支持文本检索">
        <div id="tree"></div>
    </div>
</section>
<script>
    var ctx = "${ctx}";
</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/wxjyyjmx/vZhQzfzhv1.js"></script>
</body>
</html>