<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
</head>
<body>
<!-- 主体内容 -->
<section class="content">
	<!--查询项表单 begin-->
    <div class="row">
        <div class="col-xs-12">
            <ul class="box clearfix">
                <form class="form_chag form-inline" name="vZhWfCrzjhTjQueryForm" id="vZhWfCrzjhTjQueryForm">
                	<!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
                	<!-- 请根据需要修改 查询检索项 -->
                    	<div class="form-group">
	                        <label for="wfzfzh">我方支付账户</label>
	                        <input type="text" class="form-control" id="wfzfzh" name="wfzfzh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="rz_wf_wxh">《入账》我方微信号</label>
	                        <input type="text" class="form-control" id="rz_wf_wxh" name="rz_wf_wxh" >
	                    </div>
                    <%--	<div class="form-group">
	                        <label for="rz_wf_wxzh_x1">《入账》我方微信账号新1</label>
	                        <input type="text" class="form-control" id="rz_wf_wxzh_x1" name="rz_wf_wxzh_x1" >
	                    </div>
                    	<div class="form-group">
	                        <label for="rz_wf_gwhj">《入账》我方高危户籍</label>
	                        <input type="text" class="form-control" id="rz_wf_gwhj" name="rz_wf_gwhj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="rz_jydss">《入账》交易对手数</label>
	                        <input type="text" class="form-control" id="rz_jydss" name="rz_jydss" >
	                    </div>
                    	<div class="form-group">
	                        <label for="rz_jycs">《入账》交易次数</label>
	                        <input type="text" class="form-control" id="rz_jycs" name="rz_jycs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="rz_jyje">《入账》交易金额</label>
	                        <input type="text" class="form-control" id="rz_jyje" name="rz_jyje" >
	                    </div>
                    	<div class="form-group">
	                        <label for="rz_sdqkje">《入账》涉毒前科金额</label>
	                        <input type="text" class="form-control" id="rz_sdqkje" name="rz_sdqkje" >
	                    </div>
                    	<div class="form-group">
	                        <label for="rz_sdqkje1">《入账》涉赌前科金额</label>
	                        <input type="text" class="form-control" id="rz_sdqkje1" name="rz_sdqkje1" >
	                    </div>--%>
                    	<%--<div class="form-group">
	                        <label for="rz_yd_jydss">《入账》已调交易对手数</label>
	                        <input type="text" class="form-control" id="rz_yd_jydss" name="rz_yd_jydss" >
	                    </div>
                    	<div class="form-group">
	                        <label for="rz_df_sdzjhm_ryb">《入账》对方涉毒证件号码人员表</label>
	                        <input type="text" class="form-control" id="rz_df_sdzjhm_ryb" name="rz_df_sdzjhm_ryb" >
	                    </div>
                    	<div class="form-group">
	                        <label for="rz_mgsjl">《入账》敏感时间段</label>
	                        <input type="text" class="form-control" id="rz_mgsjl" name="rz_mgsjl" >
	                    </div>
                    	<div class="form-group">
	                        <label for="rz_zsjyl">《入账》整数交易率</label>
	                        <input type="text" class="form-control" id="rz_zsjyl" name="rz_zsjyl" >
	                    </div>
                    	<div class="form-group">
	                        <label for="rz_sdrs">《入账》涉毒人数</label>
	                        <input type="text" class="form-control" id="rz_sdrs" name="rz_sdrs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="rz_df_zjhm_wxglr">《入账》对方证件号码微信关联人</label>
	                        <input type="text" class="form-control" id="rz_df_zjhm_wxglr" name="rz_df_zjhm_wxglr" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cz_wf_wxzh_x1">《出账》我方微信账号新1</label>
	                        <input type="text" class="form-control" id="cz_wf_wxzh_x1" name="cz_wf_wxzh_x1" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cz_wf_gwhj">《出账》我方高危户籍</label>
	                        <input type="text" class="form-control" id="cz_wf_gwhj" name="cz_wf_gwhj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cz_jydss">《出账》交易对手数</label>
	                        <input type="text" class="form-control" id="cz_jydss" name="cz_jydss" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cz_jycs">《出账》交易次数</label>
	                        <input type="text" class="form-control" id="cz_jycs" name="cz_jycs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cz_jyje">《出账》交易金额</label>
	                        <input type="text" class="form-control" id="cz_jyje" name="cz_jyje" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cz_sdqkje">《出账》涉毒前科金额</label>
	                        <input type="text" class="form-control" id="cz_sdqkje" name="cz_sdqkje" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cz_sdqkje1">《出账》涉赌前科金额</label>
	                        <input type="text" class="form-control" id="cz_sdqkje1" name="cz_sdqkje1" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cz_yd_jydss">《出账》已调交易对手数</label>
	                        <input type="text" class="form-control" id="cz_yd_jydss" name="cz_yd_jydss" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cz_df_sdzjhm_ryb">《出账》对方涉毒证件号码人员表</label>
	                        <input type="text" class="form-control" id="cz_df_sdzjhm_ryb" name="cz_df_sdzjhm_ryb" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cz_mgsjl">《出账》敏感时间段</label>
	                        <input type="text" class="form-control" id="cz_mgsjl" name="cz_mgsjl" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cz_zsjyl">《出账》整数交易率</label>
	                        <input type="text" class="form-control" id="cz_zsjyl" name="cz_zsjyl" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cz_sdrs">《出账》涉毒人数</label>
	                        <input type="text" class="form-control" id="cz_sdrs" name="cz_sdrs" >
	                    </div>--%>
                    	<div class="form-group">
	                        <label for="cz_df_zjhm_wxglr">《出账》对方证件号码微信关联人</label>
	                        <input type="text" class="form-control" id="cz_df_zjhm_wxglr" name="cz_df_zjhm_wxglr" >
	                    </div>
                    	<%--<div class="form-group">
	                        <label for="zjycs">总交易次数</label>
	                        <input type="text" class="form-control" id="zjycs" name="zjycs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zjyds">总交易对手</label>
	                        <input type="text" class="form-control" id="zjyds" name="zjyds" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zjyje">总交易金额</label>
	                        <input type="text" class="form-control" id="zjyje" name="zjyje" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ydjyds">已调交易对手</label>
	                        <input type="text" class="form-control" id="ydjyds" name="ydjyds" >
	                    </div>--%>
                    	<div class="form-group">
	                        <label for="df_sdzjhm_ryb">对方涉毒证件号码_人员表</label>
	                        <input type="text" class="form-control" id="df_sdzjhm_ryb" name="df_sdzjhm_ryb" >
	                    </div>
                    	<div class="form-group">
	                        <label for="df_zjhm_wxglr">对方证件号码_微信关联人</label>
	                        <input type="text" class="form-control" id="df_zjhm_wxglr" name="df_zjhm_wxglr" >
	                    </div>
                    <%--	<div class="form-group">
	                        <label for="wf_wxzh_x1">我方微信账号新1</label>
	                        <input type="text" class="form-control" id="wf_wxzh_x1" name="wf_wxzh_x1" >
	                    </div>
                    	<div class="form-group">
	                        <label for="wf_gwhj">我方高危户籍</label>
	                        <input type="text" class="form-control" id="wf_gwhj" name="wf_gwhj" >
	                    </div>--%>
                                        
                    <div class="form-group ">
                        <button type="button" style="margin-left: 8%" class="btn btn-info" onclick="search();">检索</button>
                    </div>
                </form>
            </ul>
        </div>
    </div>
	<!--查询项表单 end-->
	<!--结果展示 begin-->
	<div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="vZhWfCrzjhTjGrid" class="table table-bordered table-striped table-condensed">

                    </table>
                    <!--toolbar -->
                    <div class="btn-group" id="toolbar">
                        <i class="btn" onclick="exportVZhWfCrzjhTj();">
                            <i class="fa fa-cloud-download fa-2x"></i>
                        </i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--结果展示 end-->
    <!-- 提示模态框（Modal） -->
    <div class="modal fade" id="tipsModal" tabindex="-1" role="dialog" aria-labelledby="tipsModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="tipsModalLabel">提示</h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="tipsMessage"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" id="tipsConfirmBtn">确定</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 详情模态框（Modal） -->
    <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="showModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="showMessage">
                    <jsp:include page="vZhWfCrzjhTjShow.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 编辑模态框（Modal） -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="editModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="editMessage">
                    <jsp:include page="vZhWfCrzjhTjForm.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" id="editConfirmBtn">保存</button>
                </div>
            </div>
        </div>
    </div>
    
    <!-- 字典、树形字典展示框。如果page、form均无字典项，可无需此div -->
    <div id="treeDiv">
        <input type="text" class="form-control" id="searchTree"  placeholder="支持文本检索">
        <div id="tree"></div>
    </div>
</section>
<script>
    var ctx = "${ctx}";
</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/wxjyyjmx/vZhWfCrzjhTj.js"></script>
</body>
</html>