<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
</head>
<body>
<!-- 主体内容 -->
<section class="content">
	<!--查询项表单 begin-->
    <div class="row">
        <div class="col-xs-12">
            <ul class="box clearfix">
                <form class="form_chag form-inline" name="vZhWxzcxxV1QueryForm" id="vZhWxzcxxV1QueryForm">
                	<!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
                	<!-- 请根据需要修改 查询检索项 -->
                    <%--	<div class="form-group">
	                        <label for="bdyhk">绑定银行卡</label>
	                        <input type="text" class="form-control" id="bdyhk" name="bdyhk" >
	                    </div>
                    	<div class="form-group">
	                        <label for="bz">备注</label>
	                        <input type="text" class="form-control" id="bz" name="bz" >
	                    </div>--%>
                    	<div class="form-group">
	                        <label for="khztzjhm">开户主体或证件号码</label>
	                        <input type="text" class="form-control" id="khztzjhm" name="khztzjhm" >
	                    </div>
                    	<%--<div class="form-group">
	                        <label for="zjyxq">证件有效期</label>
	                        <input type="text" class="form-control" id="zjyxq" name="zjyxq" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cxfksm">查询反馈说明</label>
	                        <input type="text" class="form-control" id="cxfksm" name="cxfksm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="fkjgmc">反馈机构名称</label>
	                        <input type="text" class="form-control" id="fkjgmc" name="fkjgmc" >
	                    </div>--%>
                    	<div class="form-group">
	                        <label for="cxzh">查询账号</label>
	                        <input type="text" class="form-control" id="cxzh" name="cxzh" >
	                    </div>
                    	<%--<div class="form-group">
	                        <label for="wxh">微信号</label>
	                        <input type="text" class="form-control" id="wxh" name="wxh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="shhm">商户号码</label>
	                        <input type="text" class="form-control" id="shhm" name="shhm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ly">来源</label>
	                        <input type="text" class="form-control" id="ly" name="ly" >
	                    </div>
                    	<div class="form-group">
	                        <label for="lyfj">来源分局</label>
	                        <input type="text" class="form-control" id="lyfj" name="lyfj" >
	                    </div>--%>
                    	<div class="form-group">
	                        <label for="khztxm">开户主体姓名</label>
	                        <input type="text" class="form-control" id="khztxm" name="khztxm" >
	                    </div>
                    <%--	<div class="form-group">
	                        <label for="shmc">商户名称</label>
	                        <input type="text" class="form-control" id="shmc" name="shmc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="khztzjlx">开户主体证件类型</label>
	                        <input type="text" class="form-control" id="khztzjlx" name="khztzjlx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="qqh">qq号</label>
	                        <input type="text" class="form-control" id="qqh" name="qqh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="bdsjh">绑定手机号</label>
	                        <input type="text" class="form-control" id="bdsjh" name="bdsjh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sj">时间</label>
	                        <input type="text" class="form-control" id="sj" name="sj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zcxxlrsj">注册信息录入时间</label>
	                        <input type="text" class="form-control" id="zcxxlrsj" name="zcxxlrsj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cxzh_sfssfz">查询账号是否是身份证</label>
	                        <input type="text" class="form-control" id="cxzh_sfssfz" name="cxzh_sfssfz" >
	                    </div>
                    	<div class="form-group">
	                        <label for="wxzh_x">微信账号新</label>
	                        <input type="text" class="form-control" id="wxzh_x" name="wxzh_x" >
	                    </div>
                    	<div class="form-group">
	                        <label for="qczd_x">去重字段新</label>
	                        <input type="text" class="form-control" id="qczd_x" name="qczd_x" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sfzj6">身份证件前6位</label>
	                        <input type="text" class="form-control" id="sfzj6" name="sfzj6" >
	                    </div>
                    	<div class="form-group">
	                        <label for="gwhj">高危户籍</label>
	                        <input type="text" class="form-control" id="gwhj" name="gwhj" >
	                    </div>--%>
                                        
                    <div class="form-group ">
                        <button type="button" style="margin-left: 8%" class="btn btn-info" onclick="search();">检索</button>
                    </div>
                </form>
            </ul>
        </div>
    </div>
	<!--查询项表单 end-->
	<!--结果展示 begin-->
	<div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="vZhWxzcxxV1Grid" class="table table-bordered table-striped table-condensed">

                    </table>
                    <!--toolbar -->
                    <div class="btn-group" id="toolbar">
                        <i class="btn" onclick="exportVZhWxzcxxV1();">
                            <i class="fa fa-cloud-download fa-2x"></i>
                        </i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--结果展示 end-->
    <!-- 提示模态框（Modal） -->
    <div class="modal fade" id="tipsModal" tabindex="-1" role="dialog" aria-labelledby="tipsModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="tipsModalLabel">提示</h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="tipsMessage"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" id="tipsConfirmBtn">确定</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 详情模态框（Modal） -->
    <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="showModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="showMessage">
                    <jsp:include page="vZhWxzcxxV1Show.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 编辑模态框（Modal） -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="editModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="editMessage">
                    <jsp:include page="vZhWxzcxxV1Form.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" id="editConfirmBtn">保存</button>
                </div>
            </div>
        </div>
    </div>
    
    <!-- 字典、树形字典展示框。如果page、form均无字典项，可无需此div -->
    <div id="treeDiv">
        <input type="text" class="form-control" id="searchTree"  placeholder="支持文本检索">
        <div id="tree"></div>
    </div>
</section>
<script>
    var ctx = "${ctx}";
</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/wxjyyjmx/vZhWxzcxxV1.js"></script>
</body>
</html>