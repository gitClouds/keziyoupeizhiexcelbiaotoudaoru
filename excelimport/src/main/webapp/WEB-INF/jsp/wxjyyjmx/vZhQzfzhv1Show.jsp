<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">登录号</td>
            <td id="show_dlh"></td>
    		<td class="sec_tit">姓名/企业名称</td>
            <td id="show_xm_qymc"></td>
		</tr>
		<tr>
    		<td class="sec_tit">银行卡认证状态</td>
            <td id="show_yhkrzzt"></td>
    		<td class="sec_tit">微信号</td>
            <td id="show_wxh"></td>
		</tr>
		<tr>
    		<td class="sec_tit">证件类型</td>
            <td id="show_zjlx"></td>
    		<td class="sec_tit">绑定手机号</td>
            <td id="show_bdsjh"></td>
		</tr>
		<tr>
    		<td class="sec_tit">查询账号</td>
            <td id="show_cxzh"></td>
    		<td class="sec_tit">支付账号</td>
            <td id="show_zfzh"></td>
		</tr>
		<tr>
    		<td class="sec_tit">证件号</td>
            <td id="show_zjh"></td>
    		<td class="sec_tit">账号类别</td>
            <td id="show_zhlb"></td>
		</tr>
		<tr>
    		<td class="sec_tit">银行卡类型</td>
            <td id="show_yhklx"></td>
    		<td class="sec_tit">证照有效期</td>
            <td id="show_zzyxq"></td>
		</tr>
		<tr>
    		<td class="sec_tit">去重字段</td>
            <td id="show_qczd"></td>
    		<td class="sec_tit">银行卡所属银行名称</td>
            <td id="show_yhkssyhmc"></td>
		</tr>
		<tr>
    		<td class="sec_tit">银行卡有效期</td>
            <td id="show_yhkyxq"></td>
    		<td class="sec_tit">时间</td>
            <td id="show_sj"></td>
		</tr>
		<tr>
    		<td class="sec_tit">银行卡其他信息</td>
            <td id="show_yhkqtxx"></td>
    		<td class="sec_tit">银行卡卡号</td>
            <td id="show_yhkkh"></td>
		</tr>
		<tr>
    		<td class="sec_tit">QQ号</td>
            <td id="show_qqh"></td>
    		<td class="sec_tit">银行卡所属银行机构编码</td>
            <td id="show_yhkssyhjgbm"></td>
		</tr>
		<tr>
    		<td class="sec_tit">绑定手机号《微信主体》</td>
            <td id="show_bdsjh_wxzt"></td>
    		<td class="sec_tit">绑定银行卡《微信主体》</td>
            <td id="show_bdyhk_wxzt"></td>
		</tr>
    </table>
</body>
</html>
