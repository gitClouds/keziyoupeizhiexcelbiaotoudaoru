<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">对方支付帐号</td>
            <td id="show_dfzfzh"></td>
    		<td class="sec_tit">对方微信号</td>
            <td id="show_df_wxh"></td>
		</tr>
		<tr>
    		<td class="sec_tit">对方开户名《2次查》</td>
            <td id="show_df_khxm_2cc"></td>
    		<td class="sec_tit">对方开户证件《2次查》</td>
            <td id="show_df_khzj_2cc"></td>
		</tr>
		<tr>
    		<td class="sec_tit">对方绑定手机号《2次查》</td>
            <td id="show_df_bdsjh_2cc"></td>
    		<td class="sec_tit">高危户籍《对方》</td>
            <td id="show_df_gwhj"></td>
		</tr>
		<tr>
    		<td class="sec_tit">出账次数</td>
            <td id="show_czcs"></td>
    		<td class="sec_tit">出账金额</td>
            <td id="show_czje"></td>
		</tr>
		<tr>
    		<td class="sec_tit">入账次数</td>
            <td id="show_rzcs"></td>
    		<td class="sec_tit">入账金额</td>
            <td id="show_rzje"></td>
		</tr>
		<tr>
    		<td class="sec_tit">资金关系人数</td>
            <td id="show_zjgxrs"></td>
    		<td class="sec_tit">涉毒资金关系人数</td>
            <td id="show_sdzjgxrs"></td>
		</tr>
		<tr>
    		<td class="sec_tit">对方账号入账人数</td>
            <td id="show_dfzhrzrs"></td>
    		<td class="sec_tit">对方账号出账人数</td>
            <td id="show_dfzhczrs"></td>
		</tr>
		<tr>
    		<td class="sec_tit">对方入账涉毒人数</td>
            <td id="show_dfrzsdrs"></td>
    		<td class="sec_tit">对方出账涉毒人数</td>
            <td id="show_dfczsdrs"></td>
		</tr>
		<tr>
    		<td class="sec_tit">敏感时段率</td>
            <td id="show_mgsdl"></td>
    		<td class="sec_tit">整数交易率</td>
            <td id="show_zsjyl"></td>
		</tr>
    </table>
</body>
</html>
