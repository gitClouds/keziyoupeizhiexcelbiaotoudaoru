<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">对方支付账号</td>
            <td id="show_dfzfzh"></td>
    		<td class="sec_tit">交易时间</td>
            <td id="show_jysj"></td>
		</tr>
		<tr>
    		<td class="sec_tit">对方支付账号最后活跃时间</td>
            <td id="show_dfzfzhzhhysj"></td>
    		<td class="sec_tit">我方开户证件</td>
            <td id="show_wf_khzj"></td>
		</tr>
		<tr>
    		<td class="sec_tit">交易类型</td>
            <td id="show_jylx"></td>
    		<td class="sec_tit">我方来源</td>
            <td id="show_wf_ly"></td>
		</tr>
		<tr>
    		<td class="sec_tit">我方开户姓名</td>
            <td id="show_wf_khxm"></td>
    		<td class="sec_tit">交易金额</td>
            <td id="show_jyje"></td>
		</tr>
		<tr>
    		<td class="sec_tit">最后活跃情况</td>
            <td id="show_zhhyqk"></td>
		</tr>
    </table>
</body>
</html>
