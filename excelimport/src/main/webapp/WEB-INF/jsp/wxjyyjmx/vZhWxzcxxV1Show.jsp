<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">绑定银行卡</td>
            <td id="show_bdyhk"></td>
    		<td class="sec_tit">备注</td>
            <td id="show_bz"></td>
		</tr>
		<tr>
    		<td class="sec_tit">开户主体或证件号码</td>
            <td id="show_khztzjhm"></td>
    		<td class="sec_tit">证件有效期</td>
            <td id="show_zjyxq"></td>
		</tr>
		<tr>
    		<td class="sec_tit">查询反馈说明</td>
            <td id="show_cxfksm"></td>
    		<td class="sec_tit">反馈机构名称</td>
            <td id="show_fkjgmc"></td>
		</tr>
		<tr>
    		<td class="sec_tit">查询账号</td>
            <td id="show_cxzh"></td>
    		<td class="sec_tit">微信号</td>
            <td id="show_wxh"></td>
		</tr>
		<tr>
    		<td class="sec_tit">商户号码</td>
            <td id="show_shhm"></td>
    		<td class="sec_tit">来源</td>
            <td id="show_ly"></td>
		</tr>
		<tr>
    		<td class="sec_tit">来源分局</td>
            <td id="show_lyfj"></td>
    		<td class="sec_tit">开户主体姓名</td>
            <td id="show_khztxm"></td>
		</tr>
		<tr>
    		<td class="sec_tit">商户名称</td>
            <td id="show_shmc"></td>
    		<td class="sec_tit">开户主体证件类型</td>
            <td id="show_khztzjlx"></td>
		</tr>
		<tr>
    		<td class="sec_tit">qq号</td>
            <td id="show_qqh"></td>
    		<td class="sec_tit">绑定手机号</td>
            <td id="show_bdsjh"></td>
		</tr>
		<tr>
    		<td class="sec_tit">时间</td>
            <td id="show_sj"></td>
    		<td class="sec_tit">注册信息录入时间</td>
            <td id="show_zcxxlrsj"></td>
		</tr>
		<tr>
    		<td class="sec_tit">查询账号是否是身份证</td>
            <td id="show_cxzh_sfssfz"></td>
    		<td class="sec_tit">微信账号新</td>
            <td id="show_wxzh_x"></td>
		</tr>
		<tr>
    		<td class="sec_tit">去重字段新</td>
            <td id="show_qczd_x"></td>
    		<td class="sec_tit">身份证件前6位</td>
            <td id="show_sfzj6"></td>
		</tr>
		<tr>
    		<td class="sec_tit">高危户籍</td>
            <td id="show_gwhj"></td>
		</tr>
    </table>
</body>
</html>
