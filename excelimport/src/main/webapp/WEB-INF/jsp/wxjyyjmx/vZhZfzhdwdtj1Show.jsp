<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">支付账号</td>
            <td id="show_cftzh"></td>
    		<td class="sec_tit"><我方>入账涉毒人数 </td>
            <td id="show_wf_rz_sdrs"></td>
		</tr>
		<tr>
    		<td class="sec_tit"> 最后交易时间</td>
            <td id="show_zhjysj"></td>
    		<td class="sec_tit"> 最后交易情况</td>
            <td id="show_zhjyqk"></td>
		</tr>
		<tr>
    		<td class="sec_tit">明细 </td>
            <td id="show_mx"></td>
    		<td class="sec_tit">绑定手机号新 </td>
            <td id="show_bdsjhmx"></td>
		</tr>
		<tr>
    		<td class="sec_tit">开户证件</td>
            <td id="show_khzj"></td>
    		<td class="sec_tit">开户人 </td>
            <td id="show_khr"></td>
		</tr>
		<tr>
    		<td class="sec_tit">涉毒资金关系人数 </td>
            <td id="show_sdzjgxrs"></td>
    		<td class="sec_tit">入账人数 </td>
            <td id="show_rz_rs"></td>
		</tr>
		<tr>
    		<td class="sec_tit">入账涉毒人数 </td>
            <td id="show_rz_sdrs"></td>
    		<td class="sec_tit">入账金额 </td>
            <td id="show_rz_je"></td>
		</tr>
		<tr>
    		<td class="sec_tit">入账次数 </td>
            <td id="show_rz_cs"></td>
    		<td class="sec_tit">出账人数 </td>
            <td id="show_cz_cs"></td>
		</tr>
		<tr>
    		<td class="sec_tit">出账涉毒人数 </td>
            <td id="show_cz_sdrs"></td>
    		<td class="sec_tit">出账金额 </td>
            <td id="show_cz_je"></td>
		</tr>
		<tr>
    		<td class="sec_tit">已调资金关系人数 </td>
            <td id="show_ydzjgxrs"></td>
    		<td class="sec_tit">微信号 </td>
            <td id="show_wxh"></td>
		</tr>
		<tr>
    		<td class="sec_tit">下家</td>
            <td id="show_xjx"></td>
    		<td class="sec_tit">上家 </td>
            <td id="show_sjx"></td>
		</tr>
		<tr>
    		<td class="sec_tit">涉毒好友</td>
            <td id="show_sdhy"></td>
    		<td class="sec_tit">微信通联积分描述</td>
            <td id="show_wxtljfmx"></td>
		</tr>
		<tr>
    		<td class="sec_tit">微信通联积分</td>
            <td id="show_wxtljf"></td>
    		<td class="sec_tit">微信昵称</td>
            <td id="show_wxnc"></td>
		</tr>
		<tr>
    		<td class="sec_tit">开户资料 </td>
            <td id="show_khzl"></td>
    		<td class="sec_tit">研判报告 </td>
            <td id="show_ypbg"></td>
		</tr>
		<tr>
    		<td class="sec_tit">经销商积分描述</td>
            <td id="show_jxsjfms"></td>
    		<td class="sec_tit">经销商积分 </td>
            <td id="show_jxsjf"></td>
		</tr>
		<tr>
    		<td class="sec_tit">来源案件</td>
            <td id="show_lyaj"></td>
    		<td class="sec_tit">实名手机</td>
            <td id="show_smsj"></td>
		</tr>
		<tr>
    		<td class="sec_tit">最新参考位置</td>
            <td id="show_zxckwz"></td>
    		<td class="sec_tit">夜间参考位置</td>
            <td id="show_yjckwz"></td>
		</tr>
		<tr>
    		<td class="sec_tit">收件地址</td>
            <td id="show_sjdz"></td>
    		<td class="sec_tit">阿里标签数</td>
            <td id="show_albqs"></td>
		</tr>
		<tr>
    		<td class="sec_tit">收款规律积分(我方) </td>
            <td id="show_skgljf_wf"></td>
    		<td class="sec_tit">收款规律积分(对方)</td>
            <td id="show_skgljf_df"></td>
		</tr>
		<tr>
    		<td class="sec_tit">字段注释</td>
            <td id="show_skgljf_max"></td>
    		<td class="sec_tit">积分描述新</td>
            <td id="show_jfmsx"></td>
		</tr>
		<tr>
    		<td class="sec_tit">积分新</td>
            <td id="show_jfx"></td>
    		<td class="sec_tit">研判报告1.0</td>
            <td id="show_ypbg1"></td>
		</tr>
    </table>
</body>
</html>
