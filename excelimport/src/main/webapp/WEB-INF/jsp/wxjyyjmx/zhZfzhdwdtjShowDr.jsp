<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">最后交易时间日期形</td>
            <td id="show_zhjysjrqx"></td>
    		<td class="sec_tit">已调明细</td>
            <td id="show_ydmx"></td>
		</tr>
		<tr>
    		<td class="sec_tit">昆明到港次数《证件关联》</td>
            <td id="show_kmdgcs_zjgl"></td>
    		<td class="sec_tit">对方支付账号交易次数</td>
            <td id="show_dfzfzhjycs"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《对方》最后交易时间</td>
            <td id="show_dfzhjysj"></td>
    		<td class="sec_tit">《对方》最后交易情况</td>
            <td id="show_dfzhjyqk"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《对方》最早交易时间</td>
            <td id="show_dfzzjysj"></td>
    		<td class="sec_tit">《对方》最早交易情况</td>
            <td id="show_dfzzjyqk"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《我方》最后交易余额</td>
            <td id="show_wfzhjyye"></td>
    		<td class="sec_tit">《我方》最后来源案件</td>
            <td id="show_wfzhlyaj"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《我方》最后交易时间1</td>
            <td id="show_wfzhjysj1"></td>
    		<td class="sec_tit">《我方》最后交易情况1</td>
            <td id="show_wfzhjyqk1"></td>
		</tr>
		<tr>
    		<td class="sec_tit">全部前科信息</td>
            <td id="show_qbqkxx"></td>
    		<td class="sec_tit">最近抓获时间</td>
            <td id="show_zjzhsj"></td>
		</tr>
		<tr>
    		<td class="sec_tit">最近抓获数据来源</td>
            <td id="show_zjzhsjly"></td>
    		<td class="sec_tit">前科人员户籍属地</td>
            <td id="show_qkryhjsd"></td>
		</tr>
		<tr>
    		<td class="sec_tit">最近抓获人员类别中文</td>
            <td id="show_zjzhrylbzw"></td>
    		<td class="sec_tit">前科受理单位</td>
            <td id="show_qksldw"></td>
		</tr>
		<tr>
    		<td class="sec_tit">最近抓获情况</td>
            <td id="show_zjzhqk"></td>
    		<td class="sec_tit">绑定银行卡《微信账号关联》</td>
            <td id="show_bdyhk_wxzhgl"></td>
		</tr>
		<tr>
    		<td class="sec_tit">开户主体证件号或证照号《微信账号关联》</td>
            <td id="show_khztzjhm_wxzhgl"></td>
    		<td class="sec_tit">开户主体姓名或企业名称《微信账号关联》</td>
            <td id="show_khztxm_wxzhgl"></td>
		</tr>
		<tr>
    		<td class="sec_tit">绑定手机号《微信账号关联》</td>
            <td id="show_bdsjh_wxzhgl"></td>
    		<td class="sec_tit">昆明到港次数《微信关联》</td>
            <td id="show_kmdgcs_wxgl"></td>
		</tr>
		<tr>
    		<td class="sec_tit">对方微信号</td>
            <td id="show_dfwxh"></td>
    		<td class="sec_tit">已调明细2</td>
            <td id="show_ydmx2"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《我方》最后交易时间</td>
            <td id="show_wfzhjysj"></td>
    		<td class="sec_tit">《我方》最后交易情况</td>
            <td id="show_wfzhjyqk"></td>
		</tr>
		<tr>
    		<td class="sec_tit">我方支付账号交易次数</td>
            <td id="show_wfzfzhjycs"></td>
    		<td class="sec_tit">《我方》最早交易时间</td>
            <td id="show_wfzzjysj"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《我方》最早交易情况</td>
            <td id="show_wfzzjyqk"></td>
    		<td class="sec_tit">绑定银行卡《我方支付》</td>
            <td id="show_bdyhk_wfzf"></td>
		</tr>
		<tr>
    		<td class="sec_tit">证件号《我方支付》</td>
            <td id="show_zjh_wfzf"></td>
    		<td class="sec_tit">开户姓名《我方支付》</td>
            <td id="show_khxm_wfzf"></td>
		</tr>
		<tr>
    		<td class="sec_tit">绑定手机号《我方支付》</td>
            <td id="show_bdsjh_wfzf"></td>
    		<td class="sec_tit">高位户籍《我方支付》</td>
            <td id="show_gwhj_wfzf"></td>
		</tr>
		<tr>
    		<td class="sec_tit">微信号新《我方支付》</td>
            <td id="show_wxzh_x_wfzf"></td>
    		<td class="sec_tit">昆明到港次数（我方证件）</td>
            <td id="show_kmdgcs_wfzj"></td>
		</tr>
		<tr>
    		<td class="sec_tit">财付通账号</td>
            <td id="show_cftzh"></td>
    		<td class="sec_tit">微信号</td>
            <td id="show_wxh"></td>
		</tr>
		<tr>
    		<td class="sec_tit">财付通开户人</td>
            <td id="show_cftkhr"></td>
    		<td class="sec_tit">财付通开户证件</td>
            <td id="show_cftkhzj"></td>
		</tr>
		<tr>
    		<td class="sec_tit">财付通绑定手机</td>
            <td id="show_cftbdsjh"></td>
    		<td class="sec_tit">《对方》高危户籍</td>
            <td id="show_df_gwhj"></td>
		</tr>
		<tr>
    		<td class="sec_tit">出账次数</td>
            <td id="show_czcs"></td>
    		<td class="sec_tit">出账金额</td>
            <td id="show_czje"></td>
		</tr>
		<tr>
    		<td class="sec_tit">入账次数</td>
            <td id="show_rzcs"></td>
    		<td class="sec_tit">入账金额</td>
            <td id="show_rzje"></td>
		</tr>
		<tr>
    		<td class="sec_tit">资金关系人数</td>
            <td id="show_zjgxrs"></td>
    		<td class="sec_tit">涉毒资金关系人数</td>
            <td id="show_sdzjgxrs"></td>
		</tr>
		<tr>
    		<td class="sec_tit">对方账号入账人数</td>
            <td id="show_dfzhrzrs"></td>
    		<td class="sec_tit">对方账号出账人数</td>
            <td id="show_dfzhczrs"></td>
		</tr>
		<tr>
    		<td class="sec_tit">对方入账涉毒人数</td>
            <td id="show_dfrzsdrs"></td>
    		<td class="sec_tit">对方出账涉毒人数</td>
            <td id="show_dfczsdrs"></td>
		</tr>
		<tr>
    		<td class="sec_tit">敏感时段占比%</td>
            <td id="show_mfsdzb"></td>
    		<td class="sec_tit">整数交易占比%</td>
            <td id="show_zsjyzb"></td>
		</tr>
		<tr>
    		<td class="sec_tit">是否本地户籍人员对手</td>
            <td id="show_sfbdhjryds"></td>
    		<td class="sec_tit">最后交易时间</td>
            <td id="show_zhjysj"></td>
		</tr>
		<tr>
    		<td class="sec_tit">最后交易情况</td>
            <td id="show_zhjyqk"></td>
    		<td class="sec_tit">《我方》微信号新1</td>
            <td id="show_rz_wf_wxzh_x1"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《我方》入账交易对手数</td>
            <td id="show_rz_jydss"></td>
    		<td class="sec_tit">《我方》入账交易次数</td>
            <td id="show_rz_jycs"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《我方》入账交易金额</td>
            <td id="show_rz_jyje"></td>
    		<td class="sec_tit">《我方》已调入账交易对手数</td>
            <td id="show_rz_yd_jydss"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《我方》入账涉毒对手《人员表》</td>
            <td id="show_rz_df_sdzjhm_ryb"></td>
    		<td class="sec_tit">《我方》入账涉赌人数</td>
            <td id="show_rz_sdrs"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《我方》入账涉毒对手《微信关联人》</td>
            <td id="show_rz_df_zjhm_wxglr"></td>
    		<td class="sec_tit">《我方》入账敏感时段率</td>
            <td id="show_rz_mgsjl"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《我方》入账整数交易率</td>
            <td id="show_rz_zsjyl"></td>
    		<td class="sec_tit">《我方》入账涉毒前科金额</td>
            <td id="show_rz_sdqkje"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《我方》入账涉赌前科金额</td>
            <td id="show_rz_sdqkje1"></td>
    		<td class="sec_tit">《我方》出账交易对手数</td>
            <td id="show_cz_jydss"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《我方》出账交易次数</td>
            <td id="show_cz_jycs"></td>
    		<td class="sec_tit">《我方》出账交易金额</td>
            <td id="show_cz_jyje"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《我方》出账已调交易对手数</td>
            <td id="show_cz_yd_jydss"></td>
    		<td class="sec_tit">《我方》出账涉毒对手《人员表》</td>
            <td id="show_cz_df_sdzjhm_ryb"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《我方》出账涉赌人数</td>
            <td id="show_cz_sdrs"></td>
    		<td class="sec_tit">《我方》出账涉毒对手《微信关联人》</td>
            <td id="show_cz_df_zjhm_wxglr"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《我方》出账敏感时段率</td>
            <td id="show_cz_mgsjl"></td>
    		<td class="sec_tit">《我方》出账整数交易率</td>
            <td id="show_cz_zsjyl"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《我方》出账涉毒前科金额</td>
            <td id="show_cz_sdqkje"></td>
    		<td class="sec_tit">《我方》出账涉赌前科金额</td>
            <td id="show_cz_sdqkje1"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《我方》总交易次数</td>
            <td id="show_zjycs"></td>
    		<td class="sec_tit">《我方》总交易对手</td>
            <td id="show_zjyds"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《我方》总交易金额</td>
            <td id="show_zjyje"></td>
    		<td class="sec_tit">《我方》已调交易对手</td>
            <td id="show_ydjyds"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《我方》涉毒对手《人员表》</td>
            <td id="show_df_sdzjhm_ryb"></td>
    		<td class="sec_tit">《我方》涉毒对手《微信关联人》</td>
            <td id="show_df_zjhm_wxglr"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《我方》微信账号新2</td>
            <td id="show_wf_wxzh_x1"></td>
    		<td class="sec_tit">《我方》高危户籍</td>
            <td id="show_wf_gwhj"></td>
		</tr>
		<tr>
    		<td class="sec_tit">上家</td>
            <td id="show_sj"></td>
    		<td class="sec_tit">下家</td>
            <td id="show_xj"></td>
		</tr>
		<tr>
    		<td class="sec_tit">我方上家</td>
            <td id="show_wfsj"></td>
    		<td class="sec_tit">我方下家</td>
            <td id="show_wfxj"></td>
		</tr>
		<tr>
    		<td class="sec_tit">敏感时段率新</td>
            <td id="show_mgsdlx"></td>
    		<td class="sec_tit">整数交易率新</td>
            <td id="show_zsjylx"></td>
		</tr>
		<tr>
    		<td class="sec_tit">最早交易时间(新)</td>
            <td id="show_zzjysjx"></td>
    		<td class="sec_tit">最后交易情况新</td>
            <td id="show_zhjyqkx"></td>
		</tr>
		<tr>
    		<td class="sec_tit">昆明到港次数新</td>
            <td id="show_kmdgcsx"></td>
    		<td class="sec_tit">是否调取明细A</td>
            <td id="show_sfdqmxa"></td>
		</tr>
		<tr>
    		<td class="sec_tit">绑定银行卡新A</td>
            <td id="show_bdyhkxa"></td>
    		<td class="sec_tit">最后交易时间新</td>
            <td id="show_zhjysjx"></td>
		</tr>
		<tr>
    		<td class="sec_tit">绑定手机号码新</td>
            <td id="show_bdsjhmx"></td>
    		<td class="sec_tit">财富通开户证件（新）</td>
            <td id="show_cftkhzjx"></td>
		</tr>
		<tr>
    		<td class="sec_tit">财富通开户人（新）</td>
            <td id="show_cftkhrx"></td>
    		<td class="sec_tit">出账涉毒人数占比</td>
            <td id="show_czsdrhzb"></td>
		</tr>
		<tr>
    		<td class="sec_tit">入账涉毒人数占比</td>
            <td id="show_rzsdrszb"></td>
    		<td class="sec_tit">最后交易时间日期格式</td>
            <td id="show_zhjysjrqgs"></td>
		</tr>
		<tr>
    		<td class="sec_tit">高危户籍新</td>
            <td id="show_gwhjx"></td>
    		<td class="sec_tit">涉毒资金关系人数A</td>
            <td id="show_sdzigxrsa"></td>
		</tr>
		<tr>
    		<td class="sec_tit">入账人数新A</td>
            <td id="show_rzrsxa"></td>
    		<td class="sec_tit">入账涉毒人数新A</td>
            <td id="show_rzsdrsxa"></td>
		</tr>
		<tr>
    		<td class="sec_tit">入账金额新A</td>
            <td id="show_rzjexa"></td>
    		<td class="sec_tit">入账次数新A</td>
            <td id="show_rzcsxa"></td>
		</tr>
		<tr>
    		<td class="sec_tit">出账人数新A</td>
            <td id="show_czrsxa"></td>
    		<td class="sec_tit">出账涉毒人数新A</td>
            <td id="show_czsdrsxa"></td>
		</tr>
		<tr>
    		<td class="sec_tit">出账金额新A</td>
            <td id="show_czjexa"></td>
    		<td class="sec_tit">已调资金关系人数</td>
            <td id="show_ydzjgxrs"></td>
		</tr>
		<tr>
    		<td class="sec_tit">微信号新</td>
            <td id="show_wxhx"></td>
    		<td class="sec_tit">《我方》敏感时段率</td>
            <td id="show_wfmgsdl"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《我方》整数交易率</td>
            <td id="show_wfzsjyl"></td>
    		<td class="sec_tit">开户资料</td>
            <td id="show_khzl"></td>
		</tr>
		<tr>
    		<td class="sec_tit">经销商积分描述新A</td>
            <td id="show_jxsdfmsxa"></td>
    		<td class="sec_tit">经销商积分新A</td>
            <td id="show_jxsdfxa"></td>
		</tr>
		<tr>
    		<td class="sec_tit">研判报告</td>
            <td id="show_ypbg"></td>
    		<td class="sec_tit">涉毒好友</td>
            <td id="show_sdhy"></td>
		</tr>
		<tr>
    		<td class="sec_tit">证件号码《微信通联》新</td>
            <td id="show_zjhm_wxlt_x"></td>
    		<td class="sec_tit">姓名《微信通联》新</td>
            <td id="show_xm_wxlt_x"></td>
		</tr>
		<tr>
    		<td class="sec_tit">微信通联积分描述新</td>
            <td id="show_wxtldfmsx"></td>
    		<td class="sec_tit">微信通联积分新</td>
            <td id="show_wxtldfx"></td>
		</tr>
		<tr>
    		<td class="sec_tit">微信昵称新</td>
            <td id="show_wxncx"></td>
    		<td class="sec_tit">吸毒仔积分描述</td>
            <td id="show_xdzdfms"></td>
		</tr>
		<tr>
    		<td class="sec_tit">积分《吸毒仔》</td>
            <td id="show_df_xdz"></td>
    		<td class="sec_tit">积分《分销商》旧</td>
            <td id="show_df_fxs_j"></td>
		</tr>
		<tr>
    		<td class="sec_tit">经销商积分描述</td>
            <td id="show_jxsdfms"></td>
    		<td class="sec_tit">上家新</td>
            <td id="show_sjx"></td>
		</tr>
		<tr>
    		<td class="sec_tit">下家新</td>
            <td id="show_xjx"></td>
		</tr>
    </table>
</body>
</html>
