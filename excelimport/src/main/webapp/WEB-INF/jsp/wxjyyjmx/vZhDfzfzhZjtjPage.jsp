<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
</head>
<body>
<!-- 主体内容 -->
<section class="content">
	<!--查询项表单 begin-->
    <div class="row">
        <div class="col-xs-12">
            <ul class="box clearfix">
                <form class="form_chag form-inline" name="vZhDfzfzhZjtjQueryForm" id="vZhDfzfzhZjtjQueryForm">
                	<!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
                	<!-- 请根据需要修改 查询检索项 -->
                    	<div class="form-group">
	                        <label for="dfzfzh">对方支付帐号</label>
	                        <input type="text" class="form-control" id="dfzfzh" name="dfzfzh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="df_wxh">对方微信号</label>
	                        <input type="text" class="form-control" id="df_wxh" name="df_wxh" >
	                    </div>
                    	<%--<div class="form-group">
	                        <label for="df_khxm_2cc">对方开户名《2次查》</label>
	                        <input type="text" class="form-control" id="df_khxm_2cc" name="df_khxm_2cc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="df_khzj_2cc">对方开户证件《2次查》</label>
	                        <input type="text" class="form-control" id="df_khzj_2cc" name="df_khzj_2cc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="df_bdsjh_2cc">对方绑定手机号《2次查》</label>
	                        <input type="text" class="form-control" id="df_bdsjh_2cc" name="df_bdsjh_2cc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="df_gwhj">高危户籍《对方》</label>
	                        <input type="text" class="form-control" id="df_gwhj" name="df_gwhj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="czcs">出账次数</label>
	                        <input type="text" class="form-control" id="czcs" name="czcs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="czje">出账金额</label>
	                        <input type="text" class="form-control" id="czje" name="czje" >
	                    </div>
                    	<div class="form-group">
	                        <label for="rzcs">入账次数</label>
	                        <input type="text" class="form-control" id="rzcs" name="rzcs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="rzje">入账金额</label>
	                        <input type="text" class="form-control" id="rzje" name="rzje" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zjgxrs">资金关系人数</label>
	                        <input type="text" class="form-control" id="zjgxrs" name="zjgxrs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sdzjgxrs">涉毒资金关系人数</label>
	                        <input type="text" class="form-control" id="sdzjgxrs" name="sdzjgxrs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="dfzhrzrs">对方账号入账人数</label>
	                        <input type="text" class="form-control" id="dfzhrzrs" name="dfzhrzrs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="dfzhczrs">对方账号出账人数</label>
	                        <input type="text" class="form-control" id="dfzhczrs" name="dfzhczrs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="dfrzsdrs">对方入账涉毒人数</label>
	                        <input type="text" class="form-control" id="dfrzsdrs" name="dfrzsdrs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="dfczsdrs">对方出账涉毒人数</label>
	                        <input type="text" class="form-control" id="dfczsdrs" name="dfczsdrs" >
	                    </div>--%>
                    	<div class="form-group">
	                        <label for="mgsdl">敏感时段率</label>
	                        <input type="text" class="form-control" id="mgsdl" name="mgsdl" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zsjyl">整数交易率</label>
	                        <input type="text" class="form-control" id="zsjyl" name="zsjyl" >
	                    </div>
                                        
                    <div class="form-group ">
                        <button type="button" style="margin-left: 8%" class="btn btn-info" onclick="search();">检索</button>
                    </div>
                </form>
            </ul>
        </div>
    </div>
	<!--查询项表单 end-->
	<!--结果展示 begin-->
	<div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="vZhDfzfzhZjtjGrid" class="table table-bordered table-striped table-condensed">

                    </table>
                    <!--toolbar -->
                    <div class="btn-group" id="toolbar">
                        <i class="btn" onclick="exportVZhDfzfzhZjtj();">
                            <i class="fa fa-cloud-download fa-2x"></i>
                        </i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--结果展示 end-->
    <!-- 提示模态框（Modal） -->
    <div class="modal fade" id="tipsModal" tabindex="-1" role="dialog" aria-labelledby="tipsModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="tipsModalLabel">提示</h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="tipsMessage"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" id="tipsConfirmBtn">确定</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 详情模态框（Modal） -->
    <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="showModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="showMessage">
                    <jsp:include page="vZhDfzfzhZjtjShow.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 编辑模态框（Modal） -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="editModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="editMessage">
                    <jsp:include page="vZhDfzfzhZjtjForm.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" id="editConfirmBtn">保存</button>
                </div>
            </div>
        </div>
    </div>
    
    <!-- 字典、树形字典展示框。如果page、form均无字典项，可无需此div -->
    <div id="treeDiv">
        <input type="text" class="form-control" id="searchTree"  placeholder="支持文本检索">
        <div id="tree"></div>
    </div>
</section>
<script>
    var ctx = "${ctx}";
</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/wxjyyjmx/vZhDfzfzhZjtj.js"></script>
</body>
</html>