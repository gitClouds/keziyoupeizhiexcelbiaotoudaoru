<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">我方支付账户</td>
            <td id="show_wfzfzh"></td>
    		<td class="sec_tit">《入账》我方微信号</td>
            <td id="show_rz_wf_wxh"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《入账》我方微信账号新1</td>
            <td id="show_rz_wf_wxzh_x1"></td>
    		<td class="sec_tit">《入账》我方高危户籍</td>
            <td id="show_rz_wf_gwhj"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《入账》交易对手数</td>
            <td id="show_rz_jydss"></td>
    		<td class="sec_tit">《入账》交易次数</td>
            <td id="show_rz_jycs"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《入账》交易金额</td>
            <td id="show_rz_jyje"></td>
    		<td class="sec_tit">《入账》涉毒前科金额</td>
            <td id="show_rz_sdqkje"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《入账》涉赌前科金额</td>
            <td id="show_rz_sdqkje1"></td>
    		<td class="sec_tit">《入账》已调交易对手数</td>
            <td id="show_rz_yd_jydss"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《入账》对方涉毒证件号码人员表</td>
            <td id="show_rz_df_sdzjhm_ryb"></td>
    		<td class="sec_tit">《入账》敏感时间段</td>
            <td id="show_rz_mgsjl"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《入账》整数交易率</td>
            <td id="show_rz_zsjyl"></td>
    		<td class="sec_tit">《入账》涉毒人数</td>
            <td id="show_rz_sdrs"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《入账》对方证件号码微信关联人</td>
            <td id="show_rz_df_zjhm_wxglr"></td>
    		<td class="sec_tit">《出账》我方微信账号新1</td>
            <td id="show_cz_wf_wxzh_x1"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《出账》我方高危户籍</td>
            <td id="show_cz_wf_gwhj"></td>
    		<td class="sec_tit">《出账》交易对手数</td>
            <td id="show_cz_jydss"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《出账》交易次数</td>
            <td id="show_cz_jycs"></td>
    		<td class="sec_tit">《出账》交易金额</td>
            <td id="show_cz_jyje"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《出账》涉毒前科金额</td>
            <td id="show_cz_sdqkje"></td>
    		<td class="sec_tit">《出账》涉赌前科金额</td>
            <td id="show_cz_sdqkje1"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《出账》已调交易对手数</td>
            <td id="show_cz_yd_jydss"></td>
    		<td class="sec_tit">《出账》对方涉毒证件号码人员表</td>
            <td id="show_cz_df_sdzjhm_ryb"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《出账》敏感时间段</td>
            <td id="show_cz_mgsjl"></td>
    		<td class="sec_tit">《出账》整数交易率</td>
            <td id="show_cz_zsjyl"></td>
		</tr>
		<tr>
    		<td class="sec_tit">《出账》涉毒人数</td>
            <td id="show_cz_sdrs"></td>
    		<td class="sec_tit">《出账》对方证件号码微信关联人</td>
            <td id="show_cz_df_zjhm_wxglr"></td>
		</tr>
		<tr>
    		<td class="sec_tit">总交易次数</td>
            <td id="show_zjycs"></td>
    		<td class="sec_tit">总交易对手</td>
            <td id="show_zjyds"></td>
		</tr>
		<tr>
    		<td class="sec_tit">总交易金额</td>
            <td id="show_zjyje"></td>
    		<td class="sec_tit">已调交易对手</td>
            <td id="show_ydjyds"></td>
		</tr>
		<tr>
    		<td class="sec_tit">对方涉毒证件号码_人员表</td>
            <td id="show_df_sdzjhm_ryb"></td>
    		<td class="sec_tit">对方证件号码_微信关联人</td>
            <td id="show_df_zjhm_wxglr"></td>
		</tr>
		<tr>
    		<td class="sec_tit">我方微信账号新1</td>
            <td id="show_wf_wxzh_x1"></td>
    		<td class="sec_tit">我方高危户籍</td>
            <td id="show_wf_gwhj"></td>
		</tr>
    </table>
</body>
</html>
