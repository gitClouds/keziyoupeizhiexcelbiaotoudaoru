<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
</head>
<body>
<!-- 主体内容 -->
<section class="content">
    <!--查询项表单 begin-->
    <div class="row">
        <div class="col-xs-12">
            <ul class="box clearfix">
                <form class="form_chag form-inline" name="zhZfzhdwdtjQueryForm" id="zhZfzhdwdtjQueryForm">
                    <!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
                    <!-- 请根据需要修改 查询检索项 -->
                    <%--	<div class="form-group">
	                        <label for="zhjysjrqx">最后交易时间日期形</label>
	                        <input type="text" class="form-control" id="zhjysjrqx" name="zhjysjrqx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ydmx">已调明细</label>
	                        <input type="text" class="form-control" id="ydmx" name="ydmx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="kmdgcs_zjgl">昆明到港次数《证件关联》</label>
	                        <input type="text" class="form-control" id="kmdgcs_zjgl" name="kmdgcs_zjgl" >
	                    </div>
                    	<div class="form-group">
	                        <label for="dfzfzhjycs">对方支付账号交易次数</label>
	                        <input type="text" class="form-control" id="dfzfzhjycs" name="dfzfzhjycs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="dfzhjysj">《对方》最后交易时间</label>
	                        <input type="text" class="form-control" id="dfzhjysj" name="dfzhjysj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="dfzhjyqk">《对方》最后交易情况</label>
	                        <input type="text" class="form-control" id="dfzhjyqk" name="dfzhjyqk" >
	                    </div>
                    	<div class="form-group">
	                        <label for="dfzzjysj">《对方》最早交易时间</label>
	                        <input type="text" class="form-control" id="dfzzjysj" name="dfzzjysj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="dfzzjyqk">《对方》最早交易情况</label>
	                        <input type="text" class="form-control" id="dfzzjyqk" name="dfzzjyqk" >
	                    </div>
                    	<div class="form-group">
	                        <label for="wfzhjyye">《我方》最后交易余额</label>
	                        <input type="text" class="form-control" id="wfzhjyye" name="wfzhjyye" >
	                    </div>
                    	<div class="form-group">
	                        <label for="wfzhlyaj">《我方》最后来源案件</label>
	                        <input type="text" class="form-control" id="wfzhlyaj" name="wfzhlyaj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="wfzhjysj1">《我方》最后交易时间1</label>
	                        <input type="text" class="form-control" id="wfzhjysj1" name="wfzhjysj1" >
	                    </div>
                    	<div class="form-group">
	                        <label for="wfzhjyqk1">《我方》最后交易情况1</label>
	                        <input type="text" class="form-control" id="wfzhjyqk1" name="wfzhjyqk1" >
	                    </div>
                    	<div class="form-group">
	                        <label for="qbqkxx">全部前科信息</label>
	                        <input type="text" class="form-control" id="qbqkxx" name="qbqkxx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zjzhsj">最近抓获时间</label>
	                        <input type="text" class="form-control" id="zjzhsj" name="zjzhsj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zjzhsjly">最近抓获数据来源</label>
	                        <input type="text" class="form-control" id="zjzhsjly" name="zjzhsjly" >
	                    </div>
                    	<div class="form-group">
	                        <label for="qkryhjsd">前科人员户籍属地</label>
	                        <input type="text" class="form-control" id="qkryhjsd" name="qkryhjsd" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zjzhrylbzw">最近抓获人员类别中文</label>
	                        <input type="text" class="form-control" id="zjzhrylbzw" name="zjzhrylbzw" >
	                    </div>
                    	<div class="form-group">
	                        <label for="qksldw">前科受理单位</label>
	                        <input type="text" class="form-control" id="qksldw" name="qksldw" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zjzhqk">最近抓获情况</label>
	                        <input type="text" class="form-control" id="zjzhqk" name="zjzhqk" >
	                    </div>
                    	<div class="form-group">
	                        <label for="bdyhk_wxzhgl">绑定银行卡《微信账号关联》</label>
	                        <input type="text" class="form-control" id="bdyhk_wxzhgl" name="bdyhk_wxzhgl" >
	                    </div>
                    	<div class="form-group">
	                        <label for="khztzjhm_wxzhgl">开户主体证件号或证照号《微信账号关联》</label>
	                        <input type="text" class="form-control" id="khztzjhm_wxzhgl" name="khztzjhm_wxzhgl" >
	                    </div>
                    	<div class="form-group">
	                        <label for="khztxm_wxzhgl">开户主体姓名或企业名称《微信账号关联》</label>
	                        <input type="text" class="form-control" id="khztxm_wxzhgl" name="khztxm_wxzhgl" >
	                    </div>
                    	<div class="form-group">
	                        <label for="bdsjh_wxzhgl">绑定手机号《微信账号关联》</label>
	                        <input type="text" class="form-control" id="bdsjh_wxzhgl" name="bdsjh_wxzhgl" >
	                    </div>
                    	<div class="form-group">
	                        <label for="kmdgcs_wxgl">昆明到港次数《微信关联》</label>
	                        <input type="text" class="form-control" id="kmdgcs_wxgl" name="kmdgcs_wxgl" >
	                    </div>--%>
                    <div class="form-group">
                        <label for="dfwxh">对方微信号</label>
                        <input type="text" class="form-control" id="dfwxh" name="dfwxh">
                    </div>
                    <%--<div class="form-group">
                        <label for="ydmx2">已调明细2</label>
                        <input type="text" class="form-control" id="ydmx2" name="ydmx2" >
                    </div>
                    <div class="form-group">
                        <label for="wfzhjysj">《我方》最后交易时间</label>
                        <input type="text" class="form-control" id="wfzhjysj" name="wfzhjysj" >
                    </div>
                    <div class="form-group">
                        <label for="wfzhjyqk">《我方》最后交易情况</label>
                        <input type="text" class="form-control" id="wfzhjyqk" name="wfzhjyqk" >
                    </div>
                    <div class="form-group">
                        <label for="wfzfzhjycs">我方支付账号交易次数</label>
                        <input type="text" class="form-control" id="wfzfzhjycs" name="wfzfzhjycs" >
                    </div>
                    <div class="form-group">
                        <label for="wfzzjysj">《我方》最早交易时间</label>
                        <input type="text" class="form-control" id="wfzzjysj" name="wfzzjysj" >
                    </div>
                    <div class="form-group">
                        <label for="wfzzjyqk">《我方》最早交易情况</label>
                        <input type="text" class="form-control" id="wfzzjyqk" name="wfzzjyqk" >
                    </div>
                    <div class="form-group">
                        <label for="bdyhk_wfzf">绑定银行卡《我方支付》</label>
                        <input type="text" class="form-control" id="bdyhk_wfzf" name="bdyhk_wfzf" >
                    </div>--%>
                    <div class="form-group">
                        <label for="zjh_wfzf">证件号《我方支付》</label>
                        <input type="text" class="form-control" id="zjh_wfzf" name="zjh_wfzf">
                    </div>
                    <div class="form-group">
                        <label for="khxm_wfzf">开户姓名《我方支付》</label>
                        <input type="text" class="form-control" id="khxm_wfzf" name="khxm_wfzf">
                    </div>
                    <%--<div class="form-group">
                        <label for="bdsjh_wfzf">绑定手机号《我方支付》</label>
                        <input type="text" class="form-control" id="bdsjh_wfzf" name="bdsjh_wfzf" >
                    </div>
                    <div class="form-group">
                        <label for="gwhj_wfzf">高位户籍《我方支付》</label>
                        <input type="text" class="form-control" id="gwhj_wfzf" name="gwhj_wfzf" >
                    </div>
                    <div class="form-group">
                        <label for="wxzh_x_wfzf">微信号新《我方支付》</label>
                        <input type="text" class="form-control" id="wxzh_x_wfzf" name="wxzh_x_wfzf" >
                    </div>
                    <div class="form-group">
                        <label for="kmdgcs_wfzj">昆明到港次数（我方证件）</label>
                        <input type="text" class="form-control" id="kmdgcs_wfzj" name="kmdgcs_wfzj" >
                    </div>--%>
                    <div class="form-group">
                        <label for="cftzh">财付通账号</label>
                        <input type="text" class="form-control" id="cftzh" name="cftzh">
                    </div>
                    <div class="form-group">
                        <label for="wxh">微信号</label>
                        <input type="text" class="form-control" id="wxh" name="wxh">
                    </div>
                    <%--<div class="form-group">
                        <label for="cftkhr">财付通开户人</label>
                        <input type="text" class="form-control" id="cftkhr" name="cftkhr" >
                    </div>
                    <div class="form-group">
                        <label for="cftkhzj">财付通开户证件</label>
                        <input type="text" class="form-control" id="cftkhzj" name="cftkhzj" >
                    </div>
                    <div class="form-group">
                        <label for="cftbdsjh">财付通绑定手机</label>
                        <input type="text" class="form-control" id="cftbdsjh" name="cftbdsjh" >
                    </div>
                    <div class="form-group">
                        <label for="df_gwhj">《对方》高危户籍</label>
                        <input type="text" class="form-control" id="df_gwhj" name="df_gwhj" >
                    </div>
                    <div class="form-group">
                        <label for="czcs">出账次数</label>
                        <input type="text" class="form-control" id="czcs" name="czcs" >
                    </div>
                    <div class="form-group">
                        <label for="czje">出账金额</label>
                        <input type="text" class="form-control" id="czje" name="czje" >
                    </div>
                    <div class="form-group">
                        <label for="rzcs">入账次数</label>
                        <input type="text" class="form-control" id="rzcs" name="rzcs" >
                    </div>
                    <div class="form-group">
                        <label for="rzje">入账金额</label>
                        <input type="text" class="form-control" id="rzje" name="rzje" >
                    </div>
                    <div class="form-group">
                        <label for="zjgxrs">资金关系人数</label>
                        <input type="text" class="form-control" id="zjgxrs" name="zjgxrs" >
                    </div>
                    <div class="form-group">
                        <label for="sdzjgxrs">涉毒资金关系人数</label>
                        <input type="text" class="form-control" id="sdzjgxrs" name="sdzjgxrs" >
                    </div>
                    <div class="form-group">
                        <label for="dfzhrzrs">对方账号入账人数</label>
                        <input type="text" class="form-control" id="dfzhrzrs" name="dfzhrzrs" >
                    </div>
                    <div class="form-group">
                        <label for="dfzhczrs">对方账号出账人数</label>
                        <input type="text" class="form-control" id="dfzhczrs" name="dfzhczrs" >
                    </div>
                    <div class="form-group">
                        <label for="dfrzsdrs">对方入账涉毒人数</label>
                        <input type="text" class="form-control" id="dfrzsdrs" name="dfrzsdrs" >
                    </div>
                    <div class="form-group">
                        <label for="dfczsdrs">对方出账涉毒人数</label>
                        <input type="text" class="form-control" id="dfczsdrs" name="dfczsdrs" >
                    </div>
                    <div class="form-group">
                        <label for="mfsdzb">敏感时段占比%</label>
                        <input type="text" class="form-control" id="mfsdzb" name="mfsdzb" >
                    </div>
                    <div class="form-group">
                        <label for="zsjyzb">整数交易占比%</label>
                        <input type="text" class="form-control" id="zsjyzb" name="zsjyzb" >
                    </div>
                    <div class="form-group">
                        <label for="sfbdhjryds">是否本地户籍人员对手</label>
                        <input type="text" class="form-control" id="sfbdhjryds" name="sfbdhjryds" >
                    </div>
                    <div class="form-group">
                        <label for="zhjysj">最后交易时间</label>
                        <input type="text" class="form-control" id="zhjysj" name="zhjysj" >
                    </div>
                    <div class="form-group">
                        <label for="zhjyqk">最后交易情况</label>
                        <input type="text" class="form-control" id="zhjyqk" name="zhjyqk" >
                    </div>
                    <div class="form-group">
                        <label for="rz_wf_wxzh_x1">《我方》微信号新1</label>
                        <input type="text" class="form-control" id="rz_wf_wxzh_x1" name="rz_wf_wxzh_x1" >
                    </div>
                    <div class="form-group">
                        <label for="rz_jydss">《我方》入账交易对手数</label>
                        <input type="text" class="form-control" id="rz_jydss" name="rz_jydss" >
                    </div>
                    <div class="form-group">
                        <label for="rz_jycs">《我方》入账交易次数</label>
                        <input type="text" class="form-control" id="rz_jycs" name="rz_jycs" >
                    </div>
                    <div class="form-group">
                        <label for="rz_jyje">《我方》入账交易金额</label>
                        <input type="text" class="form-control" id="rz_jyje" name="rz_jyje" >
                    </div>
                    <div class="form-group">
                        <label for="rz_yd_jydss">《我方》已调入账交易对手数</label>
                        <input type="text" class="form-control" id="rz_yd_jydss" name="rz_yd_jydss" >
                    </div>
                    <div class="form-group">
                        <label for="rz_df_sdzjhm_ryb">《我方》入账涉毒对手《人员表》</label>
                        <input type="text" class="form-control" id="rz_df_sdzjhm_ryb" name="rz_df_sdzjhm_ryb" >
                    </div>
                    <div class="form-group">
                        <label for="rz_sdrs">《我方》入账涉赌人数</label>
                        <input type="text" class="form-control" id="rz_sdrs" name="rz_sdrs" >
                    </div>
                    <div class="form-group">
                        <label for="rz_df_zjhm_wxglr">《我方》入账涉毒对手《微信关联人》</label>
                        <input type="text" class="form-control" id="rz_df_zjhm_wxglr" name="rz_df_zjhm_wxglr" >
                    </div>
                    <div class="form-group">
                        <label for="rz_mgsjl">《我方》入账敏感时段率</label>
                        <input type="text" class="form-control" id="rz_mgsjl" name="rz_mgsjl" >
                    </div>
                    <div class="form-group">
                        <label for="rz_zsjyl">《我方》入账整数交易率</label>
                        <input type="text" class="form-control" id="rz_zsjyl" name="rz_zsjyl" >
                    </div>
                    <div class="form-group">
                        <label for="rz_sdqkje">《我方》入账涉毒前科金额</label>
                        <input type="text" class="form-control" id="rz_sdqkje" name="rz_sdqkje" >
                    </div>
                    <div class="form-group">
                        <label for="rz_sdqkje1">《我方》入账涉赌前科金额</label>
                        <input type="text" class="form-control" id="rz_sdqkje1" name="rz_sdqkje1" >
                    </div>
                    <div class="form-group">
                        <label for="cz_jydss">《我方》出账交易对手数</label>
                        <input type="text" class="form-control" id="cz_jydss" name="cz_jydss" >
                    </div>
                    <div class="form-group">
                        <label for="cz_jycs">《我方》出账交易次数</label>
                        <input type="text" class="form-control" id="cz_jycs" name="cz_jycs" >
                    </div>
                    <div class="form-group">
                        <label for="cz_jyje">《我方》出账交易金额</label>
                        <input type="text" class="form-control" id="cz_jyje" name="cz_jyje" >
                    </div>
                    <div class="form-group">
                        <label for="cz_yd_jydss">《我方》出账已调交易对手数</label>
                        <input type="text" class="form-control" id="cz_yd_jydss" name="cz_yd_jydss" >
                    </div>
                    <div class="form-group">
                        <label for="cz_df_sdzjhm_ryb">《我方》出账涉毒对手《人员表》</label>
                        <input type="text" class="form-control" id="cz_df_sdzjhm_ryb" name="cz_df_sdzjhm_ryb" >
                    </div>
                    <div class="form-group">
                        <label for="cz_sdrs">《我方》出账涉赌人数</label>
                        <input type="text" class="form-control" id="cz_sdrs" name="cz_sdrs" >
                    </div>
                    <div class="form-group">
                        <label for="cz_df_zjhm_wxglr">《我方》出账涉毒对手《微信关联人》</label>
                        <input type="text" class="form-control" id="cz_df_zjhm_wxglr" name="cz_df_zjhm_wxglr" >
                    </div>
                    <div class="form-group">
                        <label for="cz_mgsjl">《我方》出账敏感时段率</label>
                        <input type="text" class="form-control" id="cz_mgsjl" name="cz_mgsjl" >
                    </div>
                    <div class="form-group">
                        <label for="cz_zsjyl">《我方》出账整数交易率</label>
                        <input type="text" class="form-control" id="cz_zsjyl" name="cz_zsjyl" >
                    </div>
                    <div class="form-group">
                        <label for="cz_sdqkje">《我方》出账涉毒前科金额</label>
                        <input type="text" class="form-control" id="cz_sdqkje" name="cz_sdqkje" >
                    </div>
                    <div class="form-group">
                        <label for="cz_sdqkje1">《我方》出账涉赌前科金额</label>
                        <input type="text" class="form-control" id="cz_sdqkje1" name="cz_sdqkje1" >
                    </div>
                    <div class="form-group">
                        <label for="zjycs">《我方》总交易次数</label>
                        <input type="text" class="form-control" id="zjycs" name="zjycs" >
                    </div>
                    <div class="form-group">
                        <label for="zjyds">《我方》总交易对手</label>
                        <input type="text" class="form-control" id="zjyds" name="zjyds" >
                    </div>
                    <div class="form-group">
                        <label for="zjyje">《我方》总交易金额</label>
                        <input type="text" class="form-control" id="zjyje" name="zjyje" >
                    </div>
                    <div class="form-group">
                        <label for="ydjyds">《我方》已调交易对手</label>
                        <input type="text" class="form-control" id="ydjyds" name="ydjyds" >
                    </div>
                    <div class="form-group">
                        <label for="df_sdzjhm_ryb">《我方》涉毒对手《人员表》</label>
                        <input type="text" class="form-control" id="df_sdzjhm_ryb" name="df_sdzjhm_ryb" >
                    </div>
                    <div class="form-group">
                        <label for="df_zjhm_wxglr">《我方》涉毒对手《微信关联人》</label>
                        <input type="text" class="form-control" id="df_zjhm_wxglr" name="df_zjhm_wxglr" >
                    </div>
                    <div class="form-group">
                        <label for="wf_wxzh_x1">《我方》微信账号新2</label>
                        <input type="text" class="form-control" id="wf_wxzh_x1" name="wf_wxzh_x1" >
                    </div>
                    <div class="form-group">
                        <label for="wf_gwhj">《我方》高危户籍</label>
                        <input type="text" class="form-control" id="wf_gwhj" name="wf_gwhj" >
                    </div>
                    <div class="form-group">
                        <label for="sj">上家</label>
                        <input type="text" class="form-control" id="sj" name="sj" >
                    </div>
                    <div class="form-group">
                        <label for="xj">下家</label>
                        <input type="text" class="form-control" id="xj" name="xj" >
                    </div>
                    <div class="form-group">
                        <label for="wfsj">我方上家</label>
                        <input type="text" class="form-control" id="wfsj" name="wfsj" >
                    </div>
                    <div class="form-group">
                        <label for="wfxj">我方下家</label>
                        <input type="text" class="form-control" id="wfxj" name="wfxj" >
                    </div>
                    <div class="form-group">
                        <label for="mgsdlx">敏感时段率新</label>
                        <input type="text" class="form-control" id="mgsdlx" name="mgsdlx" >
                    </div>
                    <div class="form-group">
                        <label for="zsjylx">整数交易率新</label>
                        <input type="text" class="form-control" id="zsjylx" name="zsjylx" >
                    </div>
                    <div class="form-group">
                        <label for="zzjysjx">最早交易时间(新)</label>
                        <input type="text" class="form-control" id="zzjysjx" name="zzjysjx" >
                    </div>
                    <div class="form-group">
                        <label for="zhjyqkx">最后交易情况新</label>
                        <input type="text" class="form-control" id="zhjyqkx" name="zhjyqkx" >
                    </div>
                    <div class="form-group">
                        <label for="kmdgcsx">昆明到港次数新</label>
                        <input type="text" class="form-control" id="kmdgcsx" name="kmdgcsx" >
                    </div>
                    <div class="form-group">
                        <label for="sfdqmxa">是否调取明细A</label>
                        <input type="text" class="form-control" id="sfdqmxa" name="sfdqmxa" >
                    </div>
                    <div class="form-group">
                        <label for="bdyhkxa">绑定银行卡新A</label>
                        <input type="text" class="form-control" id="bdyhkxa" name="bdyhkxa" >
                    </div>
                    <div class="form-group">
                        <label for="zhjysjx">最后交易时间新</label>
                        <input type="text" class="form-control" id="zhjysjx" name="zhjysjx" >
                    </div>
                    <div class="form-group">
                        <label for="bdsjhmx">绑定手机号码新</label>
                        <input type="text" class="form-control" id="bdsjhmx" name="bdsjhmx" >
                    </div>
                    <div class="form-group">
                        <label for="cftkhzjx">财富通开户证件（新）</label>
                        <input type="text" class="form-control" id="cftkhzjx" name="cftkhzjx" >
                    </div>
                    <div class="form-group">
                        <label for="cftkhrx">财富通开户人（新）</label>
                        <input type="text" class="form-control" id="cftkhrx" name="cftkhrx" >
                    </div>
                    <div class="form-group">
                        <label for="czsdrhzb">出账涉毒人数占比</label>
                        <input type="text" class="form-control" id="czsdrhzb" name="czsdrhzb" >
                    </div>
                    <div class="form-group">
                        <label for="rzsdrszb">入账涉毒人数占比</label>
                        <input type="text" class="form-control" id="rzsdrszb" name="rzsdrszb" >
                    </div>
                    <div class="form-group">
                        <label for="zhjysjrqgs">最后交易时间日期格式</label>
                        <input type="text" class="form-control" id="zhjysjrqgs" name="zhjysjrqgs" >
                    </div>
                    <div class="form-group">
                        <label for="gwhjx">高危户籍新</label>
                        <input type="text" class="form-control" id="gwhjx" name="gwhjx" >
                    </div>
                    <div class="form-group">
                        <label for="sdzigxrsa">涉毒资金关系人数A</label>
                        <input type="text" class="form-control" id="sdzigxrsa" name="sdzigxrsa" >
                    </div>
                    <div class="form-group">
                        <label for="rzrsxa">入账人数新A</label>
                        <input type="text" class="form-control" id="rzrsxa" name="rzrsxa" >
                    </div>
                    <div class="form-group">
                        <label for="rzsdrsxa">入账涉毒人数新A</label>
                        <input type="text" class="form-control" id="rzsdrsxa" name="rzsdrsxa" >
                    </div>
                    <div class="form-group">
                        <label for="rzjexa">入账金额新A</label>
                        <input type="text" class="form-control" id="rzjexa" name="rzjexa" >
                    </div>
                    <div class="form-group">
                        <label for="rzcsxa">入账次数新A</label>
                        <input type="text" class="form-control" id="rzcsxa" name="rzcsxa" >
                    </div>
                    <div class="form-group">
                        <label for="czrsxa">出账人数新A</label>
                        <input type="text" class="form-control" id="czrsxa" name="czrsxa" >
                    </div>
                    <div class="form-group">
                        <label for="czsdrsxa">出账涉毒人数新A</label>
                        <input type="text" class="form-control" id="czsdrsxa" name="czsdrsxa" >
                    </div>
                    <div class="form-group">
                        <label for="czjexa">出账金额新A</label>
                        <input type="text" class="form-control" id="czjexa" name="czjexa" >
                    </div>
                    <div class="form-group">
                        <label for="ydzjgxrs">已调资金关系人数</label>
                        <input type="text" class="form-control" id="ydzjgxrs" name="ydzjgxrs" >
                    </div>
                    <div class="form-group">
                        <label for="wxhx">微信号新</label>
                        <input type="text" class="form-control" id="wxhx" name="wxhx" >
                    </div>
                    <div class="form-group">
                        <label for="wfmgsdl">《我方》敏感时段率</label>
                        <input type="text" class="form-control" id="wfmgsdl" name="wfmgsdl" >
                    </div>
                    <div class="form-group">
                        <label for="wfzsjyl">《我方》整数交易率</label>
                        <input type="text" class="form-control" id="wfzsjyl" name="wfzsjyl" >
                    </div>
                    <div class="form-group">
                        <label for="khzl">开户资料</label>
                        <input type="text" class="form-control" id="khzl" name="khzl" >
                    </div>
                    <div class="form-group">
                        <label for="jxsdfmsxa">经销商积分描述新A</label>
                        <input type="text" class="form-control" id="jxsdfmsxa" name="jxsdfmsxa" >
                    </div>
                    <div class="form-group">
                        <label for="jxsdfxa">经销商积分新A</label>
                        <input type="text" class="form-control" id="jxsdfxa" name="jxsdfxa" >
                    </div>
                    <div class="form-group">
                        <label for="ypbg">研判报告</label>
                        <input type="text" class="form-control" id="ypbg" name="ypbg" >
                    </div>
                    <div class="form-group">
                        <label for="sdhy">涉毒好友</label>
                        <input type="text" class="form-control" id="sdhy" name="sdhy" >
                    </div>
                    <div class="form-group">
                        <label for="zjhm_wxlt_x">证件号码《微信通联》新</label>
                        <input type="text" class="form-control" id="zjhm_wxlt_x" name="zjhm_wxlt_x" >
                    </div>
                    <div class="form-group">
                        <label for="xm_wxlt_x">姓名《微信通联》新</label>
                        <input type="text" class="form-control" id="xm_wxlt_x" name="xm_wxlt_x" >
                    </div>
                    <div class="form-group">
                        <label for="wxtldfmsx">微信通联积分描述新</label>
                        <input type="text" class="form-control" id="wxtldfmsx" name="wxtldfmsx" >
                    </div>
                    <div class="form-group">
                        <label for="wxtldfx">微信通联积分新</label>
                        <input type="text" class="form-control" id="wxtldfx" name="wxtldfx" >
                    </div>
                    <div class="form-group">
                        <label for="wxncx">微信昵称新</label>
                        <input type="text" class="form-control" id="wxncx" name="wxncx" >
                    </div>
                    <div class="form-group">
                        <label for="xdzdfms">吸毒仔积分描述</label>
                        <input type="text" class="form-control" id="xdzdfms" name="xdzdfms" >
                    </div>
                    <div class="form-group">
                        <label for="df_xdz">积分《吸毒仔》</label>
                        <input type="text" class="form-control" id="df_xdz" name="df_xdz" >
                    </div>
                    <div class="form-group">
                        <label for="df_fxs_j">积分《分销商》旧</label>
                        <input type="text" class="form-control" id="df_fxs_j" name="df_fxs_j" >
                    </div>
                    <div class="form-group">
                        <label for="jxsdfms">经销商积分描述</label>
                        <input type="text" class="form-control" id="jxsdfms" name="jxsdfms" >
                    </div>
                    <div class="form-group">
                        <label for="sjx">上家新</label>
                        <input type="text" class="form-control" id="sjx" name="sjx" >
                    </div>
                    <div class="form-group">
                        <label for="xjx">下家新</label>
                        <input type="text" class="form-control" id="xjx" name="xjx" >
                    </div>
                               --%>
                    <div class="form-group ">
                        <button type="button" style="margin-left: 8%" class="btn btn-info" onclick="search();">检索</button>
                    </div>
                </form>
            </ul>
        </div>
    </div>
    <!--查询项表单 end-->
    <!--结果展示 begin-->
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="zhZfzhdwdtjGrid" class="table table-bordered table-striped table-condensed">

                    </table>
                    <!--toolbar -->
                    <div class="btn-group" id="toolbar">
                        <i class="btn" onclick="exportZhZfzhdwdtj();">
                            <i class="fa fa-cloud-download fa-2x"></i>
                        </i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--结果展示 end-->
    <!-- 提示模态框（Modal） -->
    <div class="modal fade" id="tipsModal" tabindex="-1" role="dialog" aria-labelledby="tipsModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="tipsModalLabel">提示</h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="tipsMessage"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" id="tipsConfirmBtn">确定</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 详情模态框（Modal） -->
    <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="showModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="showMessage">
                    <jsp:include page="zhZfzhdwdtjShow.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 编辑模态框（Modal） -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="editModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="editMessage">
                    <jsp:include page="zhZfzhdwdtjForm.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" id="editConfirmBtn">保存</button>
                </div>
            </div>
        </div>
    </div>

    <!-- 字典、树形字典展示框。如果page、form均无字典项，可无需此div -->
    <div id="treeDiv">
        <input type="text" class="form-control" id="searchTree" placeholder="支持文本检索">
        <div id="tree"></div>
    </div>
</section>
<script>
    var ctx = "${ctx}";
</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/wxjyyjmx/zhZfzhdwdtjDr.js"></script>
</body>
</html>