<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">姓名</td>
            <td id="show_xm"></td>
    		<td class="sec_tit">证件号码优化</td>
            <td id="show_zjhmyh"></td>
		</tr>
		<tr>
    		<td class="sec_tit">嫌疑人电话</td>
            <td id="show_xyrdh"></td>
    		<td class="sec_tit">审批时间</td>
            <td id="show_spsj"></td>
		</tr>
		<tr>
    		<td class="sec_tit">人员户籍属地</td>
            <td id="show_ryhjsd"></td>
    		<td class="sec_tit">人员地址</td>
            <td id="show_rydz"></td>
		</tr>
		<tr>
    		<td class="sec_tit">受理单位</td>
            <td id="show_sldw"></td>
    		<td class="sec_tit">备注（案情）</td>
            <td id="show_aq"></td>
		</tr>
		<tr>
    		<td class="sec_tit">案件编号证件号码姓名</td>
            <td id="show_ajbhzjhm"></td>
    		<td class="sec_tit">数据来源</td>
            <td id="show_sjly"></td>
		</tr>
		<tr>
    		<td class="sec_tit">年龄</td>
            <td id="show_ll"></td>
    		<td class="sec_tit">是否港台人员</td>
            <td id="show_sfgtry"></td>
		</tr>
		<tr>
    		<td class="sec_tit">绰号/别名</td>
            <td id="show_ch"></td>
    		<td class="sec_tit">出生日期</td>
            <td id="show_csrq"></td>
		</tr>
		<tr>
    		<td class="sec_tit">QQ</td>
            <td id="show_qq"></td>
    		<td class="sec_tit">国籍</td>
            <td id="show_gj"></td>
		</tr>
		<tr>
    		<td class="sec_tit">收否台湾</td>
            <td id="show_sftw"></td>
    		<td class="sec_tit">证件种类</td>
            <td id="show_zjzl"></td>
		</tr>
		<tr>
    		<td class="sec_tit">户籍地详址</td>
            <td id="show_hjdxz"></td>
    		<td class="sec_tit">微信</td>
            <td id="show_wx"></td>
		</tr>
		<tr>
    		<td class="sec_tit">其他证件级号码</td>
            <td id="show_qtzjhm"></td>
    		<td class="sec_tit">姓名别称</td>
            <td id="show_xmbc"></td>
		</tr>
		<tr>
    		<td class="sec_tit">人员类别中午</td>
            <td id="show_rylbzw"></td>
    		<td class="sec_tit">联系方式电话</td>
            <td id="show_lxfsdh"></td>
		</tr>
		<tr>
    		<td class="sec_tit">是否澳门</td>
            <td id="show_sfam"></td>
    		<td class="sec_tit">是否香港</td>
            <td id="show_sfxg"></td>
		</tr>
		<tr>
    		<td class="sec_tit">最新抓获去重（计算）</td>
            <td id="show_zxghqc"></td>
    		<td class="sec_tit">是否近三年（计算）</td>
            <td id="show_sfjsn"></td>
		</tr>
		<tr>
    		<td class="sec_tit">是否本地（计算）</td>
            <td id="show_sfbd"></td>
    		<td class="sec_tit">人员户籍归属新（计算）</td>
            <td id="show_ryhjgsx"></td>
		</tr>
		<tr>
    		<td class="sec_tit">户籍地详细地址新（计算）</td>
            <td id="show_hjdxxdzx"></td>
    		<td class="sec_tit">港澳台和其他人员标签</td>
            <td id="show_gathqtrybq"></td>
		</tr>
		<tr>
    		<td class="sec_tit">最近抓获情况</td>
            <td id="show_zjzhqk"></td>
    		<td class="sec_tit">是否刑事（计算）</td>
            <td id="show_sfxs"></td>
		</tr>
    </table>
</body>
</html>
