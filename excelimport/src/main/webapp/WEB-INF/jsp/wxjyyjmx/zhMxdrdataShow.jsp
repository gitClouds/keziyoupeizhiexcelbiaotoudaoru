<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">字段注释</td>
            <td id="show_id"></td>
    		<td class="sec_tit">查询账号</td>
            <td id="show_cxzh"></td>
		</tr>
		<tr>
    		<td class="sec_tit">得分</td>
            <td id="show_df"></td>
    		<td class="sec_tit">数据来源</td>
            <td id="show_sjly"></td>
		</tr>
		<tr>
    		<td class="sec_tit">录入时间</td>
            <td id="show_lrsj"></td>
    		<td class="sec_tit">录入人警号</td>
            <td id="show_lrrjh"></td>
		</tr>
		<tr>
    		<td class="sec_tit">录入人姓名</td>
            <td id="show_lrrxm"></td>
    		<td class="sec_tit">录入单位名称</td>
            <td id="show_lrrdwdm"></td>
		</tr>
		<tr>
    		<td class="sec_tit">录入单位代码</td>
            <td id="show_lrrdwmc"></td>
		</tr>
    </table>
</body>
</html>
