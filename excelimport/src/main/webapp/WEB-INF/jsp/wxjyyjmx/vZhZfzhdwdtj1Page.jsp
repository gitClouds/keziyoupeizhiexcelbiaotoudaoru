<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
</head>
<body>
<!-- 主体内容 -->
<section class="content">
	<!--查询项表单 begin-->
    <div class="row">
        <div class="col-xs-12">
            <ul class="box clearfix">
                <form class="form_chag form-inline" name="vZhZfzhdwdtj1QueryForm" id="vZhZfzhdwdtj1QueryForm">
                	<!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
                	<!-- 请根据需要修改 查询检索项 -->
                    	<div class="form-group">
	                        <label for="cftzh">支付账号</label>
	                        <input type="text" class="form-control" id="cftzh" name="cftzh" >
	                    </div>
                    <%--	<div class="form-group">
	                        <label for="wf_rz_sdrs"><我方>入账涉毒人数 </label>
	                        <input type="text" class="form-control" id="wf_rz_sdrs" name="wf_rz_sdrs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zhjysj"> 最后交易时间</label>
	                        <input type="text" class="form-control" id="zhjysj" name="zhjysj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zhjyqk"> 最后交易情况</label>
	                        <input type="text" class="form-control" id="zhjyqk" name="zhjyqk" >
	                    </div>
                    	<div class="form-group">
	                        <label for="mx">明细 </label>
	                        <input type="text" class="form-control" id="mx" name="mx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="bdsjhmx">绑定手机号新 </label>
	                        <input type="text" class="form-control" id="bdsjhmx" name="bdsjhmx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="khzj">开户证件</label>
	                        <input type="text" class="form-control" id="khzj" name="khzj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="khr">开户人 </label>
	                        <input type="text" class="form-control" id="khr" name="khr" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sdzjgxrs">涉毒资金关系人数 </label>
	                        <input type="text" class="form-control" id="sdzjgxrs" name="sdzjgxrs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="rz_rs">入账人数 </label>
	                        <input type="text" class="form-control" id="rz_rs" name="rz_rs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="rz_sdrs">入账涉毒人数 </label>
	                        <input type="text" class="form-control" id="rz_sdrs" name="rz_sdrs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="rz_je">入账金额 </label>
	                        <input type="text" class="form-control" id="rz_je" name="rz_je" >
	                    </div>
                    	<div class="form-group">
	                        <label for="rz_cs">入账次数 </label>
	                        <input type="text" class="form-control" id="rz_cs" name="rz_cs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cz_cs">出账人数 </label>
	                        <input type="text" class="form-control" id="cz_cs" name="cz_cs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cz_sdrs">出账涉毒人数 </label>
	                        <input type="text" class="form-control" id="cz_sdrs" name="cz_sdrs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cz_je">出账金额 </label>
	                        <input type="text" class="form-control" id="cz_je" name="cz_je" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ydzjgxrs">已调资金关系人数 </label>
	                        <input type="text" class="form-control" id="ydzjgxrs" name="ydzjgxrs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="wxh">微信号 </label>
	                        <input type="text" class="form-control" id="wxh" name="wxh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="xjx">下家</label>
	                        <input type="text" class="form-control" id="xjx" name="xjx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sjx">上家 </label>
	                        <input type="text" class="form-control" id="sjx" name="sjx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sdhy">涉毒好友</label>
	                        <input type="text" class="form-control" id="sdhy" name="sdhy" >
	                    </div>
                    	<div class="form-group">
	                        <label for="wxtljfmx">微信通联积分描述</label>
	                        <input type="text" class="form-control" id="wxtljfmx" name="wxtljfmx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="wxtljf">微信通联积分</label>
	                        <input type="text" class="form-control" id="wxtljf" name="wxtljf" >
	                    </div>
                    	<div class="form-group">
	                        <label for="wxnc">微信昵称</label>
	                        <input type="text" class="form-control" id="wxnc" name="wxnc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="khzl">开户资料 </label>
	                        <input type="text" class="form-control" id="khzl" name="khzl" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ypbg">研判报告 </label>
	                        <input type="text" class="form-control" id="ypbg" name="ypbg" >
	                    </div>
                    	<div class="form-group">
	                        <label for="jxsjfms">经销商积分描述</label>
	                        <input type="text" class="form-control" id="jxsjfms" name="jxsjfms" >
	                    </div>
                    	<div class="form-group">
	                        <label for="jxsjf">经销商积分 </label>
	                        <input type="text" class="form-control" id="jxsjf" name="jxsjf" >
	                    </div>
                    	<div class="form-group">
	                        <label for="lyaj">来源案件</label>
	                        <input type="text" class="form-control" id="lyaj" name="lyaj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="smsj">实名手机</label>
	                        <input type="text" class="form-control" id="smsj" name="smsj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zxckwz">最新参考位置</label>
	                        <input type="text" class="form-control" id="zxckwz" name="zxckwz" >
	                    </div>
                    	<div class="form-group">
	                        <label for="yjckwz">夜间参考位置</label>
	                        <input type="text" class="form-control" id="yjckwz" name="yjckwz" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sjdz">收件地址</label>
	                        <input type="text" class="form-control" id="sjdz" name="sjdz" >
	                    </div>
                    	<div class="form-group">
	                        <label for="albqs">阿里标签数</label>
	                        <input type="text" class="form-control" id="albqs" name="albqs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="skgljf_wf">收款规律积分(我方) </label>
	                        <input type="text" class="form-control" id="skgljf_wf" name="skgljf_wf" >
	                    </div>
                    	<div class="form-group">
	                        <label for="skgljf_df">收款规律积分(对方)</label>
	                        <input type="text" class="form-control" id="skgljf_df" name="skgljf_df" >
	                    </div>
                    	<div class="form-group">
	                        <label for="jfmsx">积分描述新</label>
	                        <input type="text" class="form-control" id="jfmsx" name="jfmsx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="jfx">积分新</label>
	                        <input type="text" class="form-control" id="jfx" name="jfx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ypbg1">研判报告1.0</label>
	                        <input type="text" class="form-control" id="ypbg1" name="ypbg1" >
	                    </div>--%>
                                        
                    <div class="form-group ">
                        <button type="button" style="margin-left: 8%" class="btn btn-info" onclick="search();">检索</button>
                    </div>
                </form>
            </ul>
        </div>
    </div>
	<!--查询项表单 end-->
	<!--结果展示 begin-->
	<div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="vZhZfzhdwdtj1Grid" class="table table-bordered table-striped table-condensed">

                    </table>
                    <!--toolbar -->
                    <div class="btn-group" id="toolbar">
                        <i class="btn" onclick="exportVZhZfzhdwdtj1();">
                            <i class="fa fa-cloud-download fa-2x"></i>
                        </i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--结果展示 end-->
    <!-- 提示模态框（Modal） -->
    <div class="modal fade" id="tipsModal" tabindex="-1" role="dialog" aria-labelledby="tipsModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="tipsModalLabel">提示</h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="tipsMessage"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" id="tipsConfirmBtn">确定</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 详情模态框（Modal） -->
    <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="showModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="showMessage">
                    <jsp:include page="vZhZfzhdwdtj1Show.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 编辑模态框（Modal） -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="editModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="editMessage">
                    <jsp:include page="vZhZfzhdwdtj1Form.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" id="editConfirmBtn">保存</button>
                </div>
            </div>
        </div>
    </div>
    
    <!-- 字典、树形字典展示框。如果page、form均无字典项，可无需此div -->
    <div id="treeDiv">
        <input type="text" class="form-control" id="searchTree"  placeholder="支持文本检索">
        <div id="tree"></div>
    </div>
</section>
<script>
    var ctx = "${ctx}";
</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/wxjyyjmx/vZhZfzhdwdtj1.js"></script>
</body>
</html>