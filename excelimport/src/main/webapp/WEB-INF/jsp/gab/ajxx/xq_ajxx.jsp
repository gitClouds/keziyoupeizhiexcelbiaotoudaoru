<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div class="banner_box" >
    <div class="banner_title">
        <h4>案件信息</h4>
    </div>
    <div class="banner_table_box">
        <table class="show_tab" cellspacing=0; cellpadding=0; border=1; style="margin-top: 12px">
            <tr>
                <td class="tab_title" colspan="2">案件编号</td>
                <td><input type="text" value="" class="form-control" id="ajbh" readonly /></td>
                <td class="tab_title">立案单位</td>
                <td><input type="text" class="form-control" value="" id="ladwmc" readonly /></td>
                <td class="tab_title">立案时间</td>
                <td><input type="text" value="" class="form-control" id="lasj" readonly /></td>
                <td class="tab_title">联系人</td>
                <td><input type="text" value="" class="form-control" id="lxr" readonly /></td>
            </tr>
            <tr>
                <td style="width: 80px">报案人</td>
                <td class="tab_title">姓名</td>
                <td><input type="text" value="" class="form-control" id="bar" readonly /></td>
                <td class="tab_title">身份证号</td>
                <td><input type="text" value="" class="form-control" id="barsfzh" readonly /></td>
                <td class="tab_title">联系电话</td>
                <td><input type="text" value="" class="form-control" id="barlxdh" readonly /></td>
                <td class="tab_title">受害人单位</td>
                <td><input type="text" value="" class="form-control" id="shrdwmc" readonly /></td>
            </tr>
        </table>
        <table class="show_tab" cellspacing=0; cellpadding=0; border=1;>
            <tr>
                <td rowspan="6" style="width: 80px">案件信息</td>
                <td class="tab_title">案件类别</td>
                <td><input type="text" value="" class="form-control" id="ajlb" readonly /></td>
                <td class="tab_title">案件子类</td>
                <td><input type="text" value="" class="form-control" id="ajlbzl" readonly /></td>
                <td class="tab_title">案发时间</td>
                <td><input type="text" value="" class="form-control" id="fasj" readonly /></td>
                <td class="tab_title">涉案金额</td>
                <td><input type="text" value="" class="form-control" id="sajz" readonly /></td>
            </tr>
            <tr>
                <td class="tab_title">案发地点</td>
                <td colspan="7"><input type="text" value="" class="form-control" id="fadxc" readonly /></td>
            </tr>
            <tr>
                <td class="tab_title">简要案情</td>
                <td colspan="7" id="jyaq" style="text-align: left;padding: 5px 10px;font-size: 14px"></td>
            </tr>
            <tr>
                <td class="tab_title">受案登记</td>
                <td colspan="7" id="sadjb" style="text-align: left;padding: 5px 10px;font-size: 14px"></td>
            </tr>
            <tr>
                <td class="tab_title">询问笔录</td>
                <td colspan="7" id="xwbl" style="text-align: left;padding: 5px 10px;font-size: 14px"></td>
            </tr>
            <tr>
                <td class="tab_title">立案决定书</td>
                <td colspan="7" id="lajds" style="text-align: left;padding: 5px 10px;font-size: 14px"></td>
            </tr>
        </table>
        <table class="show_tab" cellspacing=0; cellpadding=0; border=1;>
            <tr>
                <td rowspan="3" style="width: 80px">处置信息</td>
                <td class="tab_title">审核人</td>
                <td><input type="text" value="" class="form-control" id="shr" readonly /></td>
                <td class="tab_title">审核单位</td>
                <td><input type="text" value="" class="form-control" id="shdwmc" readonly /></td>
                <td class="tab_title">审核时间</td>
                <td><input type="text" value="" class="form-control" id="shsj" readonly /></td>
                <td class="tab_title">审核结果</td>
                <td><input type="text" value="" class="form-control" id="shzt" readonly /></td>
            </tr>
            <tr>
                <td class="tab_title">审核意见</td>
                <td colspan="7"><input type="text" value="" class="form-control" id="shyj" readonly /></td>
            </tr>
            <tr>
                <td class="tab_title">发布人</td>
                <td><input type="text" value="" class="form-control" id="fbr" readonly /></td>
                <td class="tab_title">发布单位</td>
                <td><input type="text" value="" class="form-control" id="fbdw" readonly /></td>
                <td class="tab_title">发布时间</td>
                <td colspan="3"><input type="text" value="" class="form-control" id="fbsj" readonly /></td>
            </tr>
        </table>

        <table class="show_tab sazhData" data-bean-num="0" cellspacing=0; cellpadding=0; border=1;>
            <tr>
                <td rowspan="2" style="width: 80px">涉案账号</td>
                <td class="tab_title">受害人账号</td>
                <td><input type="text" value="" class="form-control" id="show_shrzh_0" readonly /></td>
                <td class="tab_title">账号类型</td>
                <td><input type="text" value="" class="form-control" id="show_shrzhlx_0" readonly /></td>
                <td class="tab_title">转账方式</td>
                <td><input type="text" value="" class="form-control" id="show_zzfs_0" readonly /></td>
                <td class="tab_title">转账时间</td>
                <td><input type="text" value="" class="form-control" id="show_zzsj_0" readonly /></td>
            </tr>
            <tr>
                <td class="tab_title">嫌疑人账号</td>
                <td><input type="text" value="" class="form-control" id="show_xyrzh_0" readonly /></td>
                <td class="tab_title">账号类型</td>
                <td><input type="text" value="" class="form-control" id="show_xyrzhlx_0" readonly /></td>
                <td class="tab_title">返款订单号</td>
                <td><input type="text" value="" class="form-control" id="show_fkdd_0" readonly /></td>
                <td class="tab_title">汇款凭证</td>
                <td id="show_zzhkpz_0"></td>
            </tr>
        </table>
        <table class="show_tab dhData" data-bean-num="0" cellspacing=0; cellpadding=0; border=1;>
            <tr>
                <td rowspan="2" style="width: 80px">涉案电话</td>
                <td class="tab_title">受害电话</td>
                <td><input type="text" value="" class="form-control" id="show_shrdh_0" readonly /></td>
                <td class="tab_title">涉案电话</td>
                <td><input type="text" value="" class="form-control" id="show_sadhhm_0" readonly /></td>
                <td class="tab_title">运营商</td>
                <td><input type="text" value="" class="form-control" id="show_yys_0" readonly /></td>
                <td class="tab_title">归属地</td>
                <td><input type="text" value="" class="form-control" id="show_dhgsdmc_0" readonly /></td>
            </tr>
            <tr>
                <td class="tab_title">通话时间</td>
                <td><input type="text" value="" class="form-control" id="show_thsj_0" readonly /></td>
                <td class="tab_title">通话内容</td>
                <td colspan="5" id="show_thnr_0"></td>
            </tr>
        </table>
        <table class="show_tab qqData" data-bean-num="0" cellspacing=0; cellpadding=0; border=1;>
            <tr>
                <td rowspan="2" style="width: 80px">社交工具</td>
                <td class="tab_title">账号类别</td>
                <td><input type="text" value="" class="form-control" id="show_sjgjmc_0" readonly /></td>
                <td class="tab_title">涉案账号</td>
                <td><input type="text" value="" class="form-control" id="show_pzqq_0" readonly /></td>
                <td class="tab_title">登陆IP</td>
                <td><input type="text" value="" class="form-control" id="show_qqip_0" readonly /></td>
                <td class="tab_title">登陆时间</td>
                <td><input type="text" value="" class="form-control" id="show_qqdlsj_0" readonly /></td>
            </tr>
            <tr>
                <td class="tab_title">登陆日志</td>
                <td colspan="7"  id="show_dlrz_0"></td>
            </tr>
        </table>
        <table class="show_tab" cellspacing=0; cellpadding=0; border=1;>
            <tr>
                <td style="width: 80px">聊天内容</td>
                <td class="tab_title">已上传截图</td>
                <td id="ltnr"></td>
            </tr>
        </table>
        <table class="show_tab wlData" data-bean-num="0" cellspacing=0; cellpadding=0; border=1;>
            <tr>
                <td style="width: 80px">涉案网址</td>
                <td class="tab_title">网址</td>
                <td><input type="text" value="" class="form-control" id="show_sawz_0" readonly /></td>
            </tr>
        </table>
    </div>
</div>
<script>
    var ctx = "${ctx}";
    var sazhNum = 0;
    var dhNum = 0;
    var qqNum = 0;
    var wlNum = 0;
</script>
</body>
</html>
