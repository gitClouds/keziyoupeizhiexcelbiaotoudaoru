<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html>
<head>
    <title>首页</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/css/base.css">
    <style>
        .chart_banner {
            width: 98%;
            margin: 0 auto 35px;
        }
        .second_tit {
            font: 600 14px "Microsoft YaHei";
            text-indent: 10px;
            border-left: 2px solid cornflowerblue;
            margin-top: 10px;
            margin-bottom: 5px;
        }
        .bs-example {
            border: 1px solid #ddd;
            border-radius: 5px;
            padding: 0 1% 15px;
            margin-top: 20px;
        }
        .tab-content ul {
            height: 300px;
        }
        .tab-content ul li {
            width: 33%;
            float: left;
            height: 100%;
        }
        .nav-tabs li {
            height: 30px;
        }
        .nav-tabs li a{
            padding: 4px 15px;
        }
    </style>
    <script>
        var REMIND_MSG = '${msg}';
        if(''!=REMIND_MSG){alert(REMIND_MSG);window.location.href="${ctx}/login"}
    </script>
</head>
<body>

<div class="chart_banner">
    <div class="bs-example clearfix">
        <h3 class="second_tit">信息录入</h3>
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#home" onclick="jjdTjShow(0);" role="tab" data-toggle="tab">近7天</a></li>
            <li role="presentation"><a href="#profile" onclick="jjdTjShow(1);" role="tab" data-toggle="tab">近30天</a></li>
        </ul>
        <!-- 面板区 -->
        <div class="tab-content">
            <div id="line_xxlr" style="height: 300px"></div>
<%--
            <div id="line_latj" style="height: 300px"></div>
--%>
        </div>
    </div>
   <%-- <div class="bs-example clearfix">
        <h3 class="second_tit">资金查控</h3>
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#home" onclick="zjckTjShow(0);" role="tab" data-toggle="tab">查询</a></li>
            <li role="presentation"><a href="#profile" onclick="zjckTjShow(1);" role="tab" data-toggle="tab">止付</a></li>
        </ul>
        <!-- 面板区 -->
        <div class="tab-content">
            <ul class="clearfix">
                <li>
                    <div id="circle_zjcx_day" style="height: 300px;"></div>
                </li>
                <li>
                    <div id="circle_zjcx__week" style="height: 300px;"></div>
                </li>
                <li>
                    <div id="circle_zjcx_mouth" style="height: 300px;"></div>
                </li>
            </ul>
        </div>
    </div>
--%>
    <div class="bs-example clearfix hide" id="xxlr_tj_box">
        <h3 class="second_tit">信息录入统计</h3>
        <div class="form-group">
            <label for="rksj">统计日期</label>
            <input class="form-control" style="width: 20%;display: inline-block;" id="rksj" name="rksj" value="" onFocus="WdatePicker({lang:'zh-cn',isShowClear:false,readOnly:true,dateFmt:'yyyy-MM-dd'})" />
            <button type="button" style="margin-left: 8%" class="btn btn-info" onclick="xxlrTj();">统计</button>
        </div>
        <!-- 面板区 -->
        <div class="tab-content">
            <div id="xxlr_tj" style="height: 300px"></div>
        </div>
    </div>
</div>

<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/echarts.min.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/index/charts.js"></script>
<script>
    var ctx = '${ctx}';
    $(function () {
        jjdTjShow(0);
     //   zjckTjShow(0);
      /*  latjShow(0);*/
    })
</script>
<script>
    <shiro:hasPermission name="SYS_ROLE">
        var date = new Date().Format('yyyy-MM-dd');
        $("#rksj").val(date);
        $("#xxlr_tj_box").removeClass("hide");
        xxlrTj();
    </shiro:hasPermission>
</script>
</body>
</html>
