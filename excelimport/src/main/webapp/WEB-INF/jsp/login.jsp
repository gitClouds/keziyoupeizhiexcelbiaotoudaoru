<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String scheme = request.getScheme();
    String host = request.getServerName();
    String basePath = scheme + "://" + host + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <title>登录</title>
    <link rel="stylesheet" href="<%=path%>/css/base.css">
    <link rel="stylesheet" href="<%=path%>/css/login.css">
    <link rel="stylesheet" href="<%=path%>/css/pub/animate.css">
    <link rel="shortcut icon" href="<%=path%>/resources/img/favicon.ico">
</head>
<body>
<div class="sit_nav">
    <div class="sit_nav_logo fl animated bounceInLeft"></div>
    <h2 class="sit_nav_tit fl animated bounceInDown">禁毒资金智能化分析平台</h2>
</div>

<form  id="loginForm" action="<%=path%>/authenticate" method="post">
<div class="login_cent">
    <div class="login_cent_img fl animated zoomInLeft"></div>
    <div class="login_cent_ipt fr animated flipInX">
        <h4>登录</h4>
        <div class="ipt_text center">
            <span></span>
            <input type="text" name="username" placeholder="请输入您的账号">
        </div>
        <div class="ipt_password center">
            <span></span>
            <input type="password" name="password" placeholder="请输入您的密码">
        </div>
        <%--<a href="javascript:void(0);">忘记密码？</a>--%>
        <button type="submit" class="login_get_in center">登录</button>
        <a href="<%=path%>/resources/Firefox.rar" style="color: red">Firefox浏览器下载</a>
        <a href="<%=path%>/resources/chrome.rar" style="color: red">chrome浏览器下载</a>
    </div>
</div>
</form>
<div class="login_footer">
    <div class="login_footer_logo center">
        <div class="foot_logo_lt"></div>
        <span>北京金之盾信息技术有限公司</span>
        <div class="foot_logo_rt"></div>
    </div>
</div>


<script>

    var REMIND_MSG = '${msg}';
    if(''!=REMIND_MSG){alert(REMIND_MSG);}

    function submitFrm() {
        document.getElementById("loginForm").submit();
    }
</script>
</body>
</html>
