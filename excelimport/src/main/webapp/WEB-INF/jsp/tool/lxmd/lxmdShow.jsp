<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">机构号</td>
            <td id="show_jgh"></td>
    		<td class="sec_tit">地区码</td>
            <td id="show_dqm"></td>
		</tr>
		<tr>
    		<td class="sec_tit">机构名称</td>
            <td id="show_dwmc"></td>
    		<td class="sec_tit">调证手续</td>
            <td id="show_dzsx"></td>
		</tr>
		<tr>
    		<td class="sec_tit">联系人</td>
            <td id="show_lxr"></td>
    		<td class="sec_tit">固话</td>
            <td id="show_telephone"></td>
		</tr>
		<tr>
    		<td class="sec_tit">手机号</td>
            <td id="show_phone"></td>
    		<td class="sec_tit">邮箱</td>
            <td id="show_email"></td>
		</tr>
		<tr>
    		<td class="sec_tit">性质</td>
            <td id="show_xz"></td>
			<td class="sec_tit">录入时间</td>
			<td id="show_rksj"></td>
		</tr>
    </table>
</body>
</html>
