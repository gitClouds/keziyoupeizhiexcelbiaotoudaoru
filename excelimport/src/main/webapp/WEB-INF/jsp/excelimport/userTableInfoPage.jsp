<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap3-editable/css/bootstrap-editable.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <%--<link rel="stylesheet" href="${ctx}/resources/jquery-ui/jquery-ui.min.css">--%>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
    <script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
    <style>
        .loading{animation:change 10s linear 0s infinite;font-size:large;font-weight:700;}
        @keyframes change{0%   {color:#333;}25%{color:#ff0;}50%{color:#f60;}75%{color:#cf0;}100% {color:#f00;}}
        ol li{
            border: solid 1px rgba(76, 175, 255, 0.3);
            background-color: rgba(83, 223, 255, 0.1);
            margin: 1px;
        }
        ol{
            border: solid 1px rgba(44,0,255,0.3);
            background-color: rgba(83, 223, 255, 0.1);
        }
        #btnArrays button{
            border: solid 1px blue;
        }
        .cell-backcolor{
            background-color: red;
        }
        /*#userTableInfoGrid{
            table-layout: fixed;
        }*/
        /*#userTableInfoGrid td{
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            width: 500px;

        }*/
    </style>
</head>
<body>

<!-- 主体内容 -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="btn-group" id="btnArrays">
                <button class="btn btn-info" value="1">Excel数据导入</button>
                <button class="btn" style="display: none;" value="2">根据Excel创建表</button>
                <button class="btn" style="display: none;" value="3">创建表</button>
                <button class="btn" style="display: none;" value="4">创建SQL脚本配置定时任务</button>
                <button class="btn" style="display: none;" value="5">查询需导入信息表</button>
                <button class="btn" style="display: none;" value="6">查询字典表</button>
                <button class="btn" style="display: none;" value="7">查询表视图...SQL结构</button>
            </div>
        </div>
    </div>
    <!--字段配置 begin-->
    <div id="excelDataImport" class="row">
        <div class="col-xs-12">
            <h4 class="second_tit">字段配置</h4>
            <input type="hidden" id="tableName" value="">
            <input type="hidden" id="saveValue" value="">
            <input type="hidden" id="saveValueName" value="">
            <input type="hidden" id="saveFunctionType" value="">
            <ul class="box clearfix">
                <form role="form">
                    <div>
                        <div style="display: inline;" id="select_table">
                            <label class="label-default" style="width:11%;display: inline;">选择表</label>
                            <select id="selection_table" onchange="changeValue(this.value)" style="display: inline;" class="form-control box-input">
                            </select>
                        </div>
                        <div style="display: none;" id="select_sql_type">
                            <label class="label-default" style="width:11%;display: inline;">选择种类</label>
                            <select id="selection_sql_statement" onchange="querySqlStatement(this.value)" style="display: inline;" class="form-control box-input">
                                <option value="" selected="selected">请选择</option>
                                <option value="TABLE">表</option>
                                <option value="VIEW">视图</option>
                                <option value="INDEX">索引</option>
                                <option value="PROCEDURE">存储过程</option>
                                <option value="SEQUENCE">序列</option>
                                <option value="TRIGGER">触发器</option>
                                <option value="FUNCTION">函数</option>
                            </select>
                        </div>
                        <div id="readDivFile" class="btn btn-default form-inline" style="margin-left: 5%;width: 500px;display: none;">
                            <form class="form_chag" id="excelForm" name="excelForm" style="display: inline;">
                                <input type="file" name="file" id="dataFile" multiple="multiple" style="display: inline;" />
                            </form>
                            <span style="display: inline;" class="btn btn-info" onclick="readExcelFile()">确认读取文件</span>
                        </div>
                        <span id="qrdrButton" style="display: none;margin-left: 5%;" class="btn btn-info" onclick="importExcelFile()">确认导入数据</span>
                    </div>
                </form>
                <div id="label_data">
                    <label class="label-default" style="width:30%;"><span id="tableNameLable" style="color: #4cae4c;"></span>数据库表字段:</label>
                    <ol style="overflow-x: auto;white-space:nowrap;width: auto;list-style: inherit;margin: 20px;" id="ol_clomun">
                    </ol>
                </div>
                <div id="label_excel">
                    <label class="label-default" style="width:20%;">excel表头字段:</label>
                    <ol style="overflow-x: auto;white-space:nowrap;width: auto;list-style: inherit;margin: 20px;" id="ol_clomun_dy">
                    </ol>
                </div>
                <div>
                    <ol style="overflow-x: auto;white-space:nowrap;width: auto;list-style: inherit;margin: 20px;display: none;" class="ui-widget-content" id="ol_clomun_hide">
                    </ol>
                </div>
            </ul>
        </div>
    </div>
	<!--结果展示 begin-->
	<div class="row">
        <div class="col-xs-12">
            <div class="box">
                <ul><li>
                    <div style="margin: 20px;display: none;" id="runSqlDiv">
                        <textarea id="runSqlArea" autofocus="autofocus" style="height:200px;width: 80%;display: inline;" placeholder="SQL执行区域"></textarea>
                        <button class="btn btn-info" style="display: inline;margin-left: 30px;margin-bottom: 30px;" onclick="runSql()">执行SQL</button>
                        <textarea id="resultSqlArea" readonly="readonly" style="height:50px;width: 100%;" placeholder="结果显示区域"></textarea>
                    </div>
                    <form class="form_chag form-inline" name="userTableInfoQueryForm" id="userTableInfoQueryForm">
                        <!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
                        <!-- 请根据需要修改 查询检索项 -->
                        <div class="form-group">
                            <label for="colmun_value" style="width: 25%;display: inline;">全字段模糊匹配查询</label>
                            <input type="text" style="display: inline;" class="form-control" id="colmun_value" name="colmun_value" >
                            <span style="display: inline;" class="btn btn-info" onclick="clearInputAndFresh()">清空</span>
                        </div>
                        <div class="form-group" id="div_selection_edit_table" style="display: none;">
                            <label for="colmun_value" style="width: 25%;display: inline;">表</label>
                            <select id="selection_edit_table" onchange="queryCreateTablePropetry(this.value)" name="selection_edit_table" style="display: inline;" class="form-control box-input">
                            </select>
                        </div>
                        <div class="form-group" id="div_selection_task_type" style="display: none;">
                            <label for="colmun_value" style="width: 25%;display: inline;">批次/任务种类</label>
                            <select id="selection_task_type" onchange="querySqlInfo(this.value)" name="selection_task_type" style="display: inline;" class="form-control box-input">
                            </select>
                        </div>

                        <%--<div class="form-group">
                            <label for="comments">中文名表名</label>
                            <input type="text" class="form-control" id="comments" name="comments" >
                        </div>--%>

                        <div class="form-group ">
                            <button type="button" style="float: right;" class="btn btn-info" onclick="search();">检索</button>
                        </div>
                    </form>
                </li>
                </ul>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="userTableInfoGrid" class="table table-bordered table-striped table-condensed">

                    </table>
                    <!--toolbar -->
                    <div class="btn-group" id="toolbar" style="float: left;display: none;">
                        <button id="inserRowId" class="btn btn-info" onclick="insertRowData()">新增行</button>
                        <button id="deleteRowId" class="btn btn-info" onclick="deleteRowData()">删除选中行</button>
                        <button id="deleteAllRowId" class="btn btn-info" onclick="deleteAllRowData()">删除所有行</button>
                        <button id="saveAllRowId" class="btn btn-info" onclick="saveAllRowData()">保存数据</button>
                        <button id="deleteSelectionData" class="btn btn-info" onclick="deleteSelectionRowData()">删除数据</button>
                        <button id="createTab" style="display: none;" class="btn btn-info" onclick="createTab()">生成表</button>
                        <%--<i class="btn btn-info">
                            启动任务
                        </i>--%>
                        <%--<i class="btn" onclick="exportUserTableInfo();">
                            <i class="fa fa-cloud-download fa-2x"></i>
                        </i>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--结果展示 end-->
    <!-- 提示模态框（Modal） -->
    <div class="modal fade" id="tipsModal" tabindex="-1" role="dialog" aria-labelledby="tipsModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="tipsModalLabel">提示</h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="tipsMessage"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" id="tipsConfirmBtn">确定</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 详情模态框（Modal） -->
    <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="showModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="showMessage">
                    <jsp:include page="userTableInfoShow.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 编辑模态框（Modal） -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="editModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="editMessage">
                    <jsp:include page="userTableInfoForm.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" id="editConfirmBtn">保存</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 文件上传模态框（Modal） -->
    <div class="modal fade" id="fileUploadModal" tabindex="-1" role="dialog" aria-labelledby="fileUploadModalLabel" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="fileUploadModalLabel">提示</h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="fileUploadMessage" style="text-align: center;">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <%--<button type="button" class="btn btn-primary" data-toggle="modal" id="fileUploadConfirmBtn">提交</button>--%>
                </div>
            </div>
        </div>
    </div>
    <!-- 字典、树形字典展示框。如果page、form均无字典项，可无需此div -->
    <div id="treeDiv">
        <input type="text" class="form-control" id="searchTree"  placeholder="支持文本检索">
        <div id="tree"></div>
    </div>
</section>

<script>
    var ctx = "${ctx}";
    var username = "${username}";
    var column_names = null;
    function selectTable(function_type){
        $("#selection_table").empty();
        $("#saveFunctionType").val();
        $("#saveFunctionType").val(function_type);
        $.ajax({
            type : "POST", //提交方式
            url : ctx+"/excelimport/userTableInfo/listAll",//路径
            data : {"function_type":function_type},
            success : function(result) {//返回数据根据结果进行相应的处理
                if ( result ) {
                    var op = '<option value="" selected="selected">请选择表</option>';
                    $.each(result, function (index, item) {
                        var comment_name_c = "";
                        if (item.comments){
                           comment_name_c = item.comments;
                        }else {
                            comment_name_c = item.table_name;
                        }
                        op+='<option value="'+item.table_name+'">'+comment_name_c+'</option>'
                    });
                    $("#selection_table").append(op);
                } else {
                    alert("未查询到表");
                }
            }
        });
    }


    /*var bootsrapColmun = null;*/
    function changeValue(value){
        $("#readDivFile").hide();
        $("#qrdrButton").hide();
        if (value){
            $("#tableName").val(value);
            $.ajax({
                type : "POST", //提交方式
                url : ctx+"/excelimport/userTableInfo/table_column_name_list",//路径
                data : {"tableName" : value},//数据，这里使用的是Json格式进行传输
                datatype : 'JSON',
                success : function(result) {//返回数据根据结果进行相应的处理
                    if ( result && result.length > 0 ) {
                        column_names = result;
                        var COMMENTS_TAB_ = result[0].COMMENTS_TAB;
                        var col_name_ = "";
                        if (COMMENTS_TAB_){
                            col_name_ = COMMENTS_TAB_+"("+result[0].TABLE_NAME+")";
                        }else {
                            col_name_ = result[0].TABLE_NAME;
                        }
                        $("#tableNameLable").text(col_name_);
                        $("#ol_clomun").empty();
                        var li = "";
                        $.each(result, function (index, item) {
                            var num = (index+1)+'.';
                            var col_name = "";
                            if (item.COMMENTS){
                                col_name = item.COMMENTS;
                            } else{
                                col_name = item.COLUMN_NAME;
                            }
                            li+='<li style="margin-left: 20px;display: inline-block;font-size: larger; cursor: pointer;" onclick="changeBackColor(this)" value="'+item.COLUMN_NAME+'">'+col_name+'</li>'
                        });
                        $("#ol_clomun").append(li);
                        $("#readDivFile").show();
                        initUserTableInfoGrid(bootstrapCOls(result));
                    } else if (result.length <= 0){
                        queryDicColumnValue(value);
                    } else {
                        alert("未查询到表字段信息");
                    }
                }
            });

        }
    }
    var sql_statement_type = "";
    function querySqlStatement(value){
        if (value){
            sql_statement_type = value;
            $.ajax({
                type : "POST", //提交方式
                url : ctx+"/excelimport/userTableInfo/sql_statement_column_list",//路径
                data : {"sqlType" : value},//数据，这里使用的是Json格式进行传输
                datatype : 'JSON',
                success : function(result) {//返回数据根据结果进行相应的处理
                    if ( result && result.length > 0 ) {
                        initUserTableInfoGrid(bootstrapCOls(result));
                    } else {
                        alert("未查询到建表数据结构信息");
                    }
                }
            });

        }
    }

    function changeBackColor(obj){
        var className = $(obj).attr("class");
        if (className){
            $("#saveValue").val("");
            $("#saveValueName").val("");
            $(obj).removeClass("btn-info");
        }else {
            var colName = $(obj).attr("value");
            var colNameChin = $(obj).text();
            $("#saveValue").val(colName);
            $("#saveValueName").val(colNameChin);
            $(obj).addClass("btn-info");
        }
    }

    function changeBackColorDy(obj){
        var className = $(obj).attr("class");
        if (className){
            var order = $(obj).children(":first").text();
            var linode = $("#ol_clomun_hide").children("li");
            $.each(linode, function (index, item) {
                var thisOder = $(this).children(":first").text();
                var thisValue = $(this).attr("value");
                var thisValueChina = $(this).children(":first").next().text();
                if (thisOder == order){
                    $(obj).attr("value", thisValue);
                    $(obj).children(":first").next().text("");
                    $(obj).children(":first").next().next().text(thisValueChina);
                }
            })
            $(obj).removeClass("btn-info");
        }else {
            var colName = $("#saveValue").val();
            var colNameChina = $("#saveValueName").val();
            if (colName){
                $(obj).attr("value",colName);
                $(obj).children(":first").next().text($(obj).children(":first").next().next().text()+"→");
                $(obj).children(":first").next().next().text(colNameChina);
                $(obj).addClass("btn-info");
            }else {
                alert("请选取数据库表字段");
            }
        }
    }

    /*读取excel文件*/

    function readExcelFile(){
        $("#fileUploadModalLabel").text("读取文件");
        $(".progress-bar-success").attr('style', 'width:0%;');
        $("#fileUploadModal").modal();
        var readmsg = '<span id="readspan" class="loading">文件读取中.....</span>'
        $("#fileUploadMessage").html(readmsg);
        var formData = new FormData();
        var dataFile = $('#dataFile').get(0).files[0];
        formData.append("file", dataFile);
        $.ajax({
            type: "POST",
            //dataType: "json",
            contentType: false,
            processData: false,
            cache: false,
            url: ctx + "/excelimport/userTableInfo/upload",
            enctype: 'multipart/form-data',
            data: formData,
            success: function (result) {
                var data = result.data;
                var fileName = result.fileName;
                if (data){
                    if ($("#tableName").val() == "FIELD_PROPERTY_INFO_TB") {
                        $.each(data, function (index, item) {
                            insertRowData(index, item, fileName);
                        })
                    }
                    $("#ol_clomun_dy").empty();
                    $("#ol_clomun_hide").empty();
                    var lidy = "";
                    var lihide = "";
                    $.each(data, function (index, item) {
                        var num = (index+1)+'.';
                        lidy+='<li style="margin-left: 20px;display: inline-block;font-size: larger;cursor: pointer;" onclick="changeBackColorDy(this)" value="'+item+'"><span>'+num+'</span><span></span><span>'+item+'</span></li>'
                        lihide+='<li style="margin-left: 20px;display: inline-block;font-size: larger;cursor: pointer;"  value="'+item+'"><span>'+num+'</span><span>'+item+'</span></li>'
                    });
                    $("#ol_clomun_dy").append(lidy);
                    $("#ol_clomun_hide").append(lihide);
                    $("#readspan").text("文件读取成功");
                    $("#qrdrButton").show();
                    setTimeout(function(){
                        $('#fileUploadModal').modal('hide');
                    },1000);
                }
            },
            error: function () {
                $("#readspan").text("文件读取失败");
                alert("读取文件异常");
            }
        });
    }

    /*导入数据*/
    function importExcelFile(){
        $("#fileUploadModalLabel").text("数据导入");
        $(".progress-bar-success").attr('style', 'width:0%;');
        $("#fileUploadModal").modal();
        var tableName = $("#tableName").val();
        var linode = $("#ol_clomun_dy").children("li");
        var linodedata = $("#ol_clomun").children("li");
        var impHeader = [];
        var dataHeader = [];
        //取配置后的key
        $.each(linode, function (index, item) {
            var thisValue = $(this).attr("value");
            if (thisValue){
                impHeader.push(thisValue);
            }else {
                impHeader.push(index);
            }
        })
        //取数据库的key
        $.each(linodedata, function (index, item) {
            var thisValuedata = $(this).attr("value");
            if (thisValuedata){
                dataHeader.push(thisValuedata);
            }else {
                dataHeader.push(index);
            }
        })
        $("#fileUploadMessage").empty();
        var mq = '<span class="loading">数据导入中.....</span>'
        $("#fileUploadMessage").html(mq);
        $.ajax({
            type : "POST", //提交方式
            url : ctx+"/excelimport/userTableInfo/importExcel",//路径
            data : {"arrayheader[]" : impHeader, "arrayheaderdata[]" : dataHeader, "tableName" : tableName},
            success : function(result) {//返回数据根据结果进行相应的处理
                if (result.code == "1"){
                    var msgspans = '<span>共计<span style="font-size: large;color: red;">'+result.currentCount+'</span>条数据导入成功</span>'
                    $("#fileUploadMessage").html(msgspans);
                    search();
                }else {
                    var msgspanf = '<span>导入失败：'+result.msg+'</span>';
                    $("#fileUploadMessage").html(msgspanf);
                }
                setTimeout(function(){
                    $('#fileUploadModal').modal('hide');
                },5000);
            },
            error: function () {
                var msgspane = '<span>读取文件异常</span>';
                $("#fileUploadMessage").html(msgspane);
            }
        });

    }

    function selectCount(){
        $.ajax({
            type: "GET",
            url: ctx + "/excelimport/userTableInfo/importCount",
            success: function (data) {
                if (data.totalCount){
                    var totalspan = '<span>共计<span style="font-size: large;">'+data.totalCount+'</span><br></span>已导入'+data.currentCount+'条<span></span>'
                }
                $("#fileUploadModal").html(totalspan);
            },
            error: function () {
                alert("请求异常");
            }
        });
    }
    /*var timePercent = setInterval('selectCount()', 100);
    window.clearInterval(timePercent);*/

    /*当查询字典表字段值时用*/
    function queryDicColumnValue(table_name){
        if (table_name){
            $.ajax({
                type : "POST", //提交方式
                url : ctx+"/excelimport/userTableInfo/list_dic_column",//路径
                datatype : 'JSON',
                data : {"table_name" : table_name},//数据，这里使用的是Json格式进行传输
                success : function(result) {//返回数据根据结果进行相应的处理
                    column_names = result;
                    if ( result && result.length > 0 ) {
                        initUserTableInfoGrid(bootstrapCOls(result));
                    } else {
                        alert("未查询到字段信息");
                    }
                }
            });

        }
    }
    function clearInputAndFresh(){
        clearInput("colmun_value");
        search();
    }
    $(function () {
        if (username == "thunis") {
            /*$.each($("#btnArrays button"), function () {
               $(this).show();
            });*/
            $("#btnArrays button").show();
            $("#toolbar").show();
            $("#div_selection_edit_table").show();
            $("#div_selection_task_type").show();
        }
        selectTable("1");

    /*按钮切换*/
        $("#btnArrays button").click(function () {
            $("#btnArrays button").eq($(this).index()).addClass("btn-info").siblings().removeClass('btn-info');
            var lx = $(this).attr("value");
            if (lx == "1"){
                $("#tableName").val();
                if (sql_statement_type){
                    sql_statement_type = "";
                }
                $("#select_sql_type").hide();
                $("#runSqlDiv").hide();
                $("#select_table").show();
                selectTable(lx);
                search();
            }
            if (lx == "2"){
                $("#tableName").val();
                if (sql_statement_type){
                    sql_statement_type = "";
                }
                $("#select_sql_type").hide();
                $("#label_data").hide();
                $("#runSqlDiv").hide();
                $("#select_table").show();
                $("#readDivFile").show();
                selectTable("3");
                search();
                queryTableInfo();
            }
            if (lx == "3"){
                $("#tableName").val();
                $("#select_sql_type").hide();
                $("#readDivFile").hide();
                $("#qrdrButton").hide();
                $("#label_data").hide();
                $("#label_excel").hide();
                $("#runSqlDiv").hide();
                $("#select_table").show();
                selectTable(lx);
                queryTableInfo();
            }
            if (lx == "4"){
                $("#tableName").val();
                $("#select_sql_type").hide();
                $("#readDivFile").hide();
                $("#qrdrButton").hide();
                $("#label_data").hide();
                $("#label_excel").hide();
                /*$("#div_selection_edit_table").hide();*/
                $("#createTab").hide();
                $("#runSqlDiv").show();
                $("#select_table").show();
                selectTable(lx);
                search();
                querySqlBatchTypeNameInfo();
            }
            if (lx == "5"){
                $("#tableName").val();
                $("#select_sql_type").hide();
                $("#readDivFile").hide();
                $("#qrdrButton").hide();
                $("#label_data").hide();
                $("#label_excel").hide();
                $("#select_table").show();
                selectTable(lx);
                search();
            }
            if (lx == "6"){
                $("#tableName").val();
                $("#select_sql_type").hide();
                $("#readDivFile").hide();
                $("#qrdrButton").hide();
                $("#label_data").hide();
                $("#label_excel").hide();
                $("#runSqlDiv").hide();
                $("#select_table").show();
                selectTable(lx);
                search();
            }
            if (lx == "7"){
                $("#tableName").val();
                $("#select_table").hide();
                $("#readDivFile").hide();
                $("#qrdrButton").hide();
                $("#label_data").hide();
                $("#label_excel").hide();
                $("#runSqlDiv").hide();
                $("#select_sql_type").show();
                search();

            }
        });
    })

</script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<%--<script type="text/javascript" src="${ctx}/resources/jquery-ui/jquery-ui.min.js"></script>--%>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap3-editable/js/bootstrap-table-editable.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/excelimport/userTableInfo.js"></script>
<script src="${ctx}/js/excelimport/Convert_Pinyin.js"></script>
</body>
</html>