<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">ID</td>
            <td id="show_id"></td>
    		<td class="sec_tit">表名</td>
            <td id="show_table_name"></td>
		</tr>
		<tr>
    		<td class="sec_tit">类型</td>
            <td id="show_table_type"></td>
    		<td class="sec_tit">中文名（注释）</td>
            <td id="show_comments"></td>
		</tr>
    </table>
</body>
</html>
