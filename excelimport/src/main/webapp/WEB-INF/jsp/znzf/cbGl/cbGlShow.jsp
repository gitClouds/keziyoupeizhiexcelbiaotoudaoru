<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">主键PK</td>
            <td id="show_pk"></td>
    		<td class="sec_tit">串并编号</td>
            <td id="show_cbbh"></td>
		</tr>
		<tr>
    		<td class="sec_tit">卡号所属类型：1-银行，2-三方</td>
            <td id="show_sertype"></td>
    		<td class="sec_tit">接警单PK</td>
            <td id="show_jjdpk"></td>
		</tr>
		<tr>
    		<td class="sec_tit">卡号所属机构代码</td>
            <td id="show_bankcode"></td>
    		<td class="sec_tit">卡号</td>
            <td id="show_cardnumber"></td>
		</tr>
		<tr>
    		<td class="sec_tit">录入人单位代码</td>
            <td id="show_lrdwdm"></td>
    		<td class="sec_tit">录入人单位名称</td>
            <td id="show_lrdwmc"></td>
		</tr>
		<tr>
    		<td class="sec_tit">录入人姓名</td>
            <td id="show_lrrxm"></td>
    		<td class="sec_tit">录入人警号</td>
            <td id="show_lrrjh"></td>
		</tr>
		<tr>
    		<td class="sec_tit">录入时间</td>
            <td id="show_lrsj"></td>
    		<td class="sec_tit">字段注释</td>
            <td id="show_rksj"></td>
		</tr>
		<tr>
    		<td class="sec_tit">字段注释</td>
            <td id="show_yxx"></td>
    		<td class="sec_tit">字段注释</td>
            <td id="show_jltpk"></td>
		</tr>
    </table>
</body>
</html>
