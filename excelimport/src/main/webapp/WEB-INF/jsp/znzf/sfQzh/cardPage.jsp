<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html>
<head>
    <title>三方全账号绑定银行卡</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
    <link rel="stylesheet" href="${ctx}/css/pub/showBase.css">
    <style>
        .tab_title{
            width: 100px;
        }
        body{
            background-color: #fff;
            padding: 8px;
        }
        .table_list_box{
            table-layout:fixed !important;
        }
        .table{
            table-layout: fixed!important;
        }
    </style>
</head>
<body>
<input type="hidden" id="actpk" name="actpk" value="${actpk}"  />
<table id="sfJgCardGrid" class="table table-bordered table-striped table-condensed">

</table>
<div class="btn-group" id="cardToolbar">
    <i class="btn" onclick="exportSfJgCard();" title="导出">
        <i class="fa fa-cloud-download fa-2x"></i>
    </i>
</div>

<script>
    var ctx = "${ctx}";
</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/resources/common/fileUpload.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/znzf/sfJgCard.js"></script>
<script>
    $(window).resize(function () {
        $('#sfJgCardGrid').bootstrapTable('resetView');
    });
</script>
</body>
</html>
