<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html>
<head>
    <title>三方全账号详情</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
    <link rel="stylesheet" href="${ctx}/css/pub/showBase.css">
    <style>
        .tab_title{
            width: 100px;
        }
        body{
            background-color: #fff;
            padding: 8px;
        }
        .table_list_box{
            table-layout:fixed !important;
        }
        .table{
            table-layout: fixed!important;
        }
    </style>
</head>
<body onload="changeIframeHeight('sfQzhShow');">
<input type="hidden" id="pk" name="pk" value="${qq.pk}"  />
<div class="banner_box" >
    <div class="banner_table_box">
        <table class="show_tab" cellspacing=0; cellpadding=0; border=1; style="margin-top: 12px">
            <tr>
                <td class="tab_title">是否反馈</td>
                <td style="width: 18%">
                    <c:choose>
                        <c:when test="${qq.resflag==0}">
                            未反馈
                        </c:when>
                        <c:when test="${qq.resflag==1}">
                            已反馈
                        </c:when>
                        <c:when test="${qq.resflag==2}">
                            失败反馈
                        </c:when>
                    </c:choose>
                </td>
                <td class="tab_title">三方机构</td>
                <td style="width: 18%">${qq.payname}</td>
                <td class="tab_title">账号类型</td>
                <td style="width: 18%">${dicMap["010105"][qq.acctype]}</td>
            </tr>
            <tr>
                <td class="tab_title">账号</td>
                <td>${qq.accnumber}</td>
                <td class="tab_title">反馈机构</td>
                <td>${jg.feedbackorgname}</td>
                <td class="tab_title">反馈结果</td>
                <td>${dicMap["010301"][jg.resultcode]}</td>
             </tr>
            <tr>
                <td class="tab_title">反馈说明</td>
                <td>${jg.feedbackremark}</td>
                <td class="tab_title">法律文书</td>
                <td>
                <c:choose>
                    <c:when test="${qq.flws==null || qq.flws==''}">

                    </c:when>
                    <c:otherwise>
                    <div class="btn-group">
                        <i class="btn" onclick="fileUpload.fileDownLoad(this)" data-key="${ctx}/attachment/download?pk=${qq.flws}">
                            <i class="fa fa-download fa-lg"></i>
                        </i>
                    </div>
                    </c:otherwise>
                </c:choose>
                </td>
            </tr>
        </table>
    </div>
</div>
<table id="sfJgActGrid" class="table table-bordered table-striped table-condensed">

</table>
<div class="btn-group" id="toolbar">
    <i class="btn" onclick="exportSfJgAct();" title="明细导出">
        <i class="fa fa-cloud-download fa-2x"></i>
    </i>
</div>

<!-- 详情模态框（Modal） -->
<div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-show">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" id="showModalLabel"></h4><%--模态框（Modal）标题--%>
            </div>
            <div class="modal-body" id="showMessage">
                <%--<jsp:include page="${ctx}/znzf/base/show"/>--%>
                <iframe src="" id="actShowIframe" width="100%" height="600px" scrolling="no" frameborder="0"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>
<script>
    var ctx = "${ctx}";
</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/resources/common/fileUpload.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/znzf/sfJgAct.js"></script>
<script>
    $(window).resize(function () {
        $('#sfJgActGrid').bootstrapTable('resetView');
    });
    var changeIframeHeight = function (iframeId) {
        window.parent.document.getElementById(iframeId).style.height = "600px";
    }
</script>
</body>
</html>
