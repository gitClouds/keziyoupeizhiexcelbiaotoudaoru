<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html>
<head>
    <title>全账号详情</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
    <link rel="stylesheet" href="${ctx}/css/pub/showBase.css">
    <style>
        .tab_title{
            width: 100px;
        }
        body{
            background-color: transparent;
        }
        .table_list_box{
            table-layout:fixed !important;
        }
        .table{
            table-layout: fixed!important;
        }
    </style>
</head>
<body onload="changeIframeHeight('zcShowIframe');">
<div>
    <ul class="banner_tab">
        <c:choose>
            <c:when test="${list!=null && list.size()>0}">
                <c:forEach items="${list}" var="map" varStatus="status">
                    <li onclick="showQzh('${map.pk}');" <c:if test="${status.index==0}">class="tab_item_get"</c:if> >
                        <a href="javascript:void(0)" >全账号${status.index+1}</a>
                    </li>
                </c:forEach>
            </c:when>
        </c:choose>
    </ul>
</div>
<iframe src="${ctx}/znzf/sfQzh/show?pk=${(fn:length(list))>0?list[0].pk:""}" id="sfQzhShow" width="100%" scrolling="no" frameborder="0"></iframe>

<script>
    var ctx = "${ctx}";
</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/resources/common/fileUpload.js"></script>
<script src="${ctx}/js/common.js"></script>
<script>
    var changeIframeHeight = function (iframeId) {
        window.parent.document.getElementById(iframeId).style.height="690px";
    }
    $(function () {
        $(".banner_tab li").on("click",function () {
            $(this).addClass("tab_item_get").siblings().removeClass("tab_item_get");
        })
    })
    function showQzh(pk) {
        $("#sfQzhShow").attr("src",ctx + "/znzf/sfQzh/show?pk="+pk);
    }
</script>
</body>
</html>
