<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/4/19
  Time: 12:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<!-- Single button -->
<%--<div class="btn-group">
    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        止付方式 <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
        <li onclick="addBase('yhkZf','银行卡止付请求');"><a href="#">银行卡止付</a></li>
        <li onclick="addBase('sfZf','第三方止付请求');"><a href="#">第三方止付</a></li>
    </ul>
</div>--%>
<div class="btn-group">
    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        明细和主体查询 <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
        <li onclick="addBase('yhkMx','银行卡请求');"><a href="#">银行卡请求</a></li>
        <li onclick="addBase('sfMx','第三方请求');"><a href="#">第三方请求</a></li>
    </ul>
</div>
<div class="btn-group">
    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        主体查询 <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
        <li onclick="addBase('yhkZt','银行卡主体请求');"><a href="#">银行卡主体</a></li>
        <li onclick="addBase('sfZt','第三方主体请求');"><a href="#">第三方主体</a></li>
    </ul>
</div>
<div class="btn-group">
    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        第三方全账号查询 <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
        <li onclick="addTypeBase('sfQzh','03','全账号请求');"><a href="#">手机号</a></li>
        <li onclick="addTypeBase('sfQzh','02','全账号请求');"><a href="#">身份证</a></li>
        <li onclick="addTypeBase('sfQzh','04','全账号请求');"><a href="#">银行卡</a></li>
        <li onclick="addTypeBase('sfQzh','00','全账号请求');"><a href="#">微信号</a></li>
        <li onclick="addTypeBase('sfQzh','05','全账号请求');"><a href="#">登陆账号</a></li>
        <li onclick="addTypeBase('sfQzh','06','全账号请求');"><a href="#">商户号</a></li>
    </ul>
</div>
<div class="btn-group">
    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        流水号查询 <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
        <li onclick="addTypeBase('sfLs','A1','流水请求');"><a href="#">外部银行流水号</a></li>
        <li onclick="addTypeBase('sfLs','A2','流水请求');"><a href="#">支付机构订单号</a></li>
        <li onclick="addTypeBase('sfLs','A3','流水请求');"><a href="#">收单POS刷卡流水号</a></li>
        <li onclick="addTypeBase('sfLs','A41','流水请求');"><a href="#">银联交易流水号</a></li>
        <li onclick="addTypeBase('sfLs','A42','流水请求');"><a href="#">网联交易流水号</a></li>
    </ul>
</div>
</body>
</html>
