<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>图表</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <script>
        //iframe真正解决高度自适应
        function changeFrameHeight(iframeId){
            var ifm= document.getElementById(iframeId);
            ifm.height=document.documentElement.clientHeight;

        }

        window.onresize=function(){
            changeFrameHeight();

        }
    </script>
</head>
<body>
<jsp:include page="../../loading.jsp"/>
<input type="hidden" name="jjdpk" id="jjdpk" value="${jjdpk}" />
<div id="tabs1" style="position: fixed; top: 10px; left: 20px; z-index: 9;" class="btn-group" >
    <span id="ryfist" style="cursor: pointer" class="btn-lg btn-info" value="1">人员上下家关系图</span>
    <span id="ryfisttwo" style="cursor: pointer" class="btn-lg" value="3">人员关系网图</span>
    <span style="cursor: pointer" class="btn-lg" value="2">金流图</span>
</div>
<iframe src="${ctx}/znzf/jlt/mindJltPage?jjdpk=${jjdpk}" style="margin-top: 50px; display: none;" id="jlt_map" width="100%" height="100%" scrolling="no" frameborder="0"></iframe>
<iframe src="" style="margin-top: 50px; display: none;" id="rygxtb" width="100%" height="100%" scrolling="no" frameborder="0"></iframe>
<iframe src="" style="margin-top: 50px; display: none;" id="rygxtbtwo" width="100%" height="100%" scrolling="no" frameborder="0"></iframe>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/common/jsmind.js"></script>
<script src="${ctx}/resources/common/html2canvas.min.js"></script>
<script src="${ctx}/resources/common/jspdf.min.js"></script>
<script type="text/javascript" src="${ctx}/resources/layer/layer.js" charset="utf-8"></script>
<script type="text/javascript">
   $(function () {
       var jltpk = '${jltpk}';
       var cardnumber = '${cardnumber}';
       $("#tabs1 span").click(function () {
           $("#tabs1 span").eq($(this).index()).addClass("btn-info").siblings().removeClass('btn-info');
           var lx = $(this).attr("value");
           if (lx == "1") {
               $("#jlt_map").hide();
               $("#rygxtbtwo").hide();
               $("#rygxtb").show();
               document.getElementById('rygxtb').contentWindow.location.reload(true);
           }
           if (lx == "2") {
               $("#rygxtb").hide();
               $("#rygxtbtwo").hide();
               $("#jlt_map").show();
               document.getElementById('jlt_map').contentWindow.location.reload(true);
           }
           if (lx == "3") {
               $("#rygxtb").hide();
               $("#jlt_map").hide();
               $("#rygxtbtwo").show();
               document.getElementById('rygxtbtwo').contentWindow.location.reload(true);
           }
       });
        if (cardnumber){
            document.getElementById('rygxtb').src="${ctx}/znzf/jlt/relation_map_page?nlevel=0&pk="+cardnumber;
        }
        if (jltpk){
            document.getElementById('rygxtbtwo').src="${ctx}/znzf/jlt/relation_map_two_page?pk="+jltpk;
        }
   })
    window.onload = function () {
        $("#ryfist").click();
    }
</script>
</body>
</html>
