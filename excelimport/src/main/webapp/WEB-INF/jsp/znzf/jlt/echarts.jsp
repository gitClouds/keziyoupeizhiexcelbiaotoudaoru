<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <script src="${ctx}/resources/echarts.min.js" type="text/javascript" charset="UTF-8"></script>
</head>
<body>
<jsp:include page="../../loading.jsp"/>
<div id="main" style="width: 800px;height:400px;"></div>
<script type="text/javascript">
    // 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById('main'));

    // 指定图表的配置项和数据
    var option = {
        color: ['#2c28ff', '#ff213f'],
        title: {
            text: '账号关联信息'
        },
        tooltip: {},
        toolbox: {
            show : true,
            feature : {
                saveAsImage: {show: true}
            }
        },
        legend: {
            /*selectedMode: true,//不可点击
            data:['嫌疑人数量', '受害人数量']*/
            data:['嫌疑人数量', '受害人数量']
        },
        xAxis: {
            data: ["吸毒人","贩毒人","受害人","嫌疑人","窝藏人员","受害人","窝藏人员","受害人","贩毒人","窝藏人员","窝藏人员","贩毒人"],
            "axisLabel":{
                interval:0, //坐标刻度之间的显示间隔，默认就可以了（默认是不重叠）
                rotate:38   //调整数值改变倾斜的幅度（范围-90到90）
            }
        },
        yAxis: {},
        series: [{
            name: '嫌疑人数量',
            type: 'bar',
            data: [5, 20, 36, 10, 10, 20, 5, 20, 36, 10, 10, 20]
            },
            {
                name: '受害人数量',
                type: 'line',
                data: [5, 20, 36, 10, 10, 20, 5, 20, 36, 10, 10, 20]
            }]
    };

    // 使用刚指定的配置项和数据显示图表。
    myChart.setOption(option);
    setTimeout(function (){
        window.onresize = function () {
            myChart.resize();
        }
    },200)
</script>
</body>
</html>
