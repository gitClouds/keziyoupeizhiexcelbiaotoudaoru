<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>金流图</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/css/jsmind.css">
    <link rel="shortcut icon" href="${ctx}/resources/img/favicon.ico">
    <style type="text/css">
        * {
            margin: 0;
            padding: 0;
        }
        body,html {
            width: 100%;
            height: 100%;
        }
        #jsmind_container{
            width:100%;
            height: 100%;
        }
        .mind_box {
            width: 100%;
            height: 100%;
            overflow: auto;
        }
        b{
            color: red;
        }
        strong{
            color: #FF24D8;
            font-weight: 500;
        }
        jmnodes.theme-primary jmnode {
            background-color: #C9F6EB;
            color: #000;
            border-color: #BDE7DD;
        }
        jmnodes.theme-primary jmnode:hover {
            background-color: #98E7D6;
            border-color: #84C9BA;
        }
        jmnodes.theme-primary jmnode.selected{
            background-color:#6EFFE1;
            color: #000;
        }
        span:hover,jmnode:hover{
            cursor: hand;
        }
    </style>
    <style>
        #panel
        {
            z-index: 99;
            display:none;
            height: 100%;
            width: 30%;
            background-color: #fff;
            position: absolute;
            top:0px;
            right: 0px;
            box-shadow: 0 0 5px #888;
        }

    </style>
</head>
<body>
<jsp:include page="../../loading.jsp"/>
<input type="hidden" name="jjdpk" id="jjdpk" value="${jjdpk}" />
<input type="hidden" name="rypk" id="rypk" value="" />
<div id="tabs1" style="position: fixed; top: 10px; left: 20px; z-index: 9;" >
    <span class="btn-sm btn-info" onclick="expand_all();">展开</span>
    <span class="btn-sm btn-info" onclick="collapse_all();">折叠</span>
    <span class="btn-sm btn-info" onclick="screen_shot()">下载</span>
</div>
<div class="mind_box">
    <div id="jsmind_container"></div>
</div>
<%--<div id="panel">
    <jsp:include page='codePanel.jsp'/>
</div>--%>
<!-- 模态框（Modal） -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:60%;height: auto;">
        <div class="modal-content" style="border-radius:5px" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    详细信息
                </h4>
            </div>
            <div class="modal-body">
                <%--<iframe src="${ctx}/znzf/jlt/echarts_page" id="echartsShow" width="100%" scrolling="no" frameborder="0"></iframe>--%>
                    <jsp:include page="echarts.jsp"/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/common/jsmind.js"></script>
<script src="${ctx}/resources/common/html2canvas.min.js"></script>
<script src="${ctx}/resources/common/jspdf.min.js"></script>
<script type="text/javascript">
   /* $(function () {
        $("jmnode").on("click",function () {
            alert(111)
            showPanel();
        })
        $("#jsmind_container .jsmind-inner jmnodes jmnode").on("click",function () {
            alert(123);
        });
    })
    function showPanel() {
        $("#panel").slideToggle(200);
    }*/
    var ctx = "${ctx}";
    var _jm = null;
    function load_jsmind(jsonData){
        var mind = {
            meta:{
                name:'jltMind'
            },
            format:'node_array',
            data:jsonData
        };
        var options = {
            container:'jsmind_container',
            editable: false,
            theme:'primary'
        };
        _jm = jsMind.show(options,mind);
        _jm.expand_to_depth(1);
    }
    function expand_all(){
        _jm.expand_all();
    }
    function collapse_all(){
        _jm.collapse_all();
    }
    //load_jsmind();
    var searchJlt =  function(){
        var reqData={"jjdpk":$("#jjdpk").val()};
        $.ajax({
            type: "POST",
            dataType: "json",
            url: ctx+"/znzf/jlt/searchJlt" ,
            data: reqData,
            success: function (result) {
                $("#rypk").val(result[0].id);
                load_jsmind(result);
            },
            error : function() {
                alert("请求异常");
            }
        });
    }
    searchJlt();

    function screen_shot() {
        $(".window_loading").show();
        $("#jsmind_container").css("height",$(".theme-primary").height());
        $("#jsmind_container").css("width",$(".theme-primary").width());
        html2canvas(
            document.querySelector("#jsmind_container"),{allowTaint: true,scale: 2}
        ).then(function(canvas){
            debugger
            var contentWidth = canvas.width;
            var contentHeight = canvas.height;
            var pageData = canvas.toDataURL('image/jpeg', 1.0);

            var pdfX = (contentWidth + 10) / 2 * 0.75;
            var pdfY = (contentHeight + 500) / 2 * 0.75;

            var imgX = pdfX;
            var imgY = (contentHeight / 2 * 0.75);

            var PDF = new jsPDF('', 'pt', [pdfX, pdfY]);

            PDF.addImage(pageData, 'jpeg', 0, 0, imgX, imgY);
            PDF.save($("#jjdpk").val()+'.pdf');
            $("#jsmind_container").css("height","100%");
            $("#jsmind_container").css("width","100%");
            $(".window_loading").hide();
        });
    }

    //鼠标滚轮缩放
/*    window.onmousewheel = document.onmousewheel = function (e) {
         e = e || window.event;
         if (e.wheelDelta) {  //判断浏览器IE，谷歌滑轮事件
             if (e.wheelDelta > 0) { //当滑轮向上滚动时
                _jm.view.zoomIn()
             }
             if (e.wheelDelta < 0) { //当滑轮向下滚动时
                _jm.view.zoomOut();
             }
         } else if (e.detail) {  //Firefox滑轮事件
             if (e.detail > 0) { //当滑轮向下滚动时
                _jm.view.zoomOut();
             }
             if (e.detail < 0) { //当滑轮向上滚动时
                _jm.view.zoomIn()
             }
         }
     }*/
    document.oncontextmenu = function (e) {
        return false;
    }
   /*function selectNode(){
       $("jmnodes jmnode").click(function () {
           var content = $(this).text();
           $(this).attr('data-toggle','modal');
           $(this).attr('data-target','#myModal');
       });
       /!*$('#myModal').on('show.bs.modal', function () {
           $(".modal-backdrop").remove();
       });*!/
   }
   window.setTimeout(selectNode, 100);*/
</script>
</body>
</html>
