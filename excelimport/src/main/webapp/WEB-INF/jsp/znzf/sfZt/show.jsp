<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html>
<head>
    <title>三方主体详情</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
    <link rel="stylesheet" href="${ctx}/css/pub/showBase.css">
    <style>
        .tab_title{
            width: 100px;
        }
        body{
            background-color: transparent;
        }
        .table_list_box{
            table-layout:fixed !important;
        }
        .table{
            table-layout: fixed!important;
        }
    </style>
</head>
<body onload="changeIframeHeight('zcShowIframe');">
<input type="hidden" name="applicationid" id="applicationid" value="${qq.applicationid}" />
<div class="banner_box" >
    <div class="banner_table_box">
        <table class="show_tab" cellspacing=0; cellpadding=0; border=1; style="margin-top: 12px">
            <tr>
                <td class="tab_title">三方机构</td>
                <td style="width: 23%">${qq.payname}</td>
                <td class="tab_title">账/卡号</td>
                <td style="width: 22%">${qq.accnumber}</td>
                <td class="tab_title">主体名称</td>
                <td>${jg.khztxm}</td>
                <%--<td class="tab_title">业务申请编号</td>
                <td>${jg.applicationid}</td>--%>
            </tr>
            <tr>
                <td class="tab_title">证照类型</td>
                <td>${dicMap["537"][jg.khztzjlx]}</td>
                <td class="tab_title">证照号码</td>
                <td>${jg.khztzjhm}</td>
                <td class="tab_title">联系手机</td>
                <td>${jg.khztbdsjh}</td>
            </tr>
            <tr>
                <td class="tab_title">反馈机构</td>
                <td>${jg.fkjgmc}</td>
                <td class="tab_title">反馈结果</td>
                <td>${dicMap["010301"][jg.resultcode]}</td>
                <td class="tab_title">反馈说明</td>
                <td>${jg.cxfksm}</td>
            </tr>
            <tr>
                <td class="tab_title">经办人姓名</td>
                <td>${jg.jbrxm}</td>
                <td class="tab_title">经办人电话</td>
                <td>${jg.jbrdh}</td>
                <td class="tab_title">备注</td>
                <td>${jg.bz}</td>
            </tr>
            <tr>
                <td class="tab_title">是否反馈</td>
                <td>
                    <c:choose>
                        <c:when test="${qq.resflag==0}">
                            未反馈
                        </c:when>
                        <c:when test="${qq.resflag==1}">
                            已反馈
                        </c:when>
                        <c:when test="${qq.resflag==2}">
                            失败反馈
                        </c:when>
                    </c:choose>
                </td>
                <td class="tab_title">法律文书</td>
                <td colspan="3">
                    <c:choose>
                        <c:when test="${qq.flws==null || qq.flws==''}">

                        </c:when>
                        <c:otherwise>
                            <div class="btn-group">
                                <i class="btn" onclick="fileUpload.fileDownLoad(this)" data-key="${ctx}/attachment/download?pk=${qq.flws}">
                                    <i class="fa fa-download fa-lg"></i>
                                </i>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
        </table>
    </div>
</div>

<h5 style="color:#00c0ef;padding: 10px;">绑定银行卡信息</h5>
<table id="sfJgCardGrid" class="table table-bordered table-striped table-condensed">

</table>
<div class="btn-group" id="cardToolbar">
    <i class="btn" onclick="exportCard();" title="绑定银行卡导出">
        <i class="fa fa-cloud-download fa-2x"></i>
    </i>
</div>
<h5 style="color:#00c0ef;padding: 10px;">绑定信息</h5>
<table id="sfJgBindGrid" class="table table-bordered table-striped table-condensed">

</table>
<div class="btn-group" id="toolbar">
    <i class="btn" onclick="exportBind();" title="绑定信息导出">
        <i class="fa fa-cloud-download fa-2x"></i>
    </i>
</div>
<script>
    var ctx = "${ctx}";
</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/resources/common/fileUpload.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/znzf/sfJgBind.js"></script>
<script src="${ctx}/js/znzf/sfJgCard.js"></script>
<script>
    var changeIframeHeight = function (iframeId) {
        window.parent.document.getElementById(iframeId).style.height="900px";
    }
    $(window).resize(function () {
        $('#sfJgBindGrid').bootstrapTable('resetView');
        $('#sfJgCardGrid').bootstrapTable('resetView');
    });
</script>
</body>
</html>
