<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">归属银行</td>
            <td id="show_bankname"></td>
			<td class="sec_tit">需请求机构</td>
			<td id="show_payname"></td>
		</tr>
		<tr>
    		<td class="sec_tit">借贷标志</td>
            <td id="show_jdbz"></td>
    		<td class="sec_tit">验证字段</td>
            <td id="show_checkfield"></td>
		</tr>
		<tr>
    		<td class="sec_tit">需验证值</td>
            <td id="show_checkvalue"></td>
    		<td class="sec_tit">需请求类型</td>
            <td id="show_sertype"></td>
		</tr>
		<tr>
    		<td class="sec_tit">数据所在字段</td>
            <td id="show_datafield"></td>
    		<td class="sec_tit">数据所在位置</td>
            <td id="show_positionval"></td>
		</tr>
		<tr>
    		<td class="sec_tit">前一个字符</td>
            <td id="show_startpfx"></td>
    		<td class="sec_tit">后一个字符</td>
            <td id="show_endpfx"></td>
		</tr>
    </table>
</body>
</html>
