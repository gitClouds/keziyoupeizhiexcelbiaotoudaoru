<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <form class="form_chag" id="specialMetaEditForm" name="specialMetaEditForm">
        <input type="hidden" id="edit_curdType" name="curdType" >
        <input type="hidden" id="edit_pk" name="pk" >
        <table class="table table-bordered">
			<tr>
        		<td class="sec_tit">
                    <label for="edit_bankcode">归属银行</label>
                </td>
                <td>
                    <div class="form-group has-feedback form_controls" style="width: 100%;">
                        <input type="hidden" id="edit_bankcode" name="bankcode" />
                        <input type="text" class="required form-control" id="edit_bankname" name="bankname" data-jzd-type="code" data-jzd-code="539"  data-jzd-filter="" data-jzd-dm="edit_bankcode" data-jzd-mc="edit_bankname" readonly data-tip="" data-valid="isNonEmpty" data-error="不能为空" >
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_bankcode');clearInput('edit_bankname');"></span>
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_jdbz">借贷标志</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" class="required form-control" id="edit_jdbz" name="jdbz" data-tip="0:借;1:贷" data-valid="isNonEmpty||onlyInt" data-error="不能为空||只能输入数字" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_checkfield">验证字段</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_checkfield" name="checkfield" data-tip="" data-valid="isNonEmpty" data-error="不能为空" >
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_checkvalue">需验证值</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" class="required form-control" id="edit_checkvalue" name="checkvalue" data-tip="" data-valid="isNonEmpty" data-error="不能为空" >
                    </div>
                </td>
			</tr>
			<tr>
                <td class="sec_tit">
                    <label for="edit_payname">需请求机构</label>
                </td>
                <td>
                    <div class="form-group has-feedback form_controls" style="width: 100%;">
                        <input type="hidden" id="edit_paycode" name="paycode" />
                        <input type="text" class="required form-control" id="edit_payname" name="payname" data-jzd-type="code" data-jzd-code="542"  data-jzd-filter="" data-jzd-dm="edit_paycode" data-jzd-mc="edit_payname" readonly data-tip="" data-valid="isNonEmpty" data-error="不能为空" >
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_paycode');clearInput('edit_payname');"></span>
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_sertype">需请求类型</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_sertype" name="sertype" data-tip="S-19:流水;S-09:全账号" data-valid="isNonEmpty" data-error="不能为空" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_datafield">数据字段</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="form-control" id="edit_datafield" name="datafield" data-tip="流水时填写" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_positionval">数据位置</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="form-control" id="edit_positionval" name="positionval" data-tip="流水时填写;1:开头;2:结尾"  >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_startpfx">前一个字符</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="form-control" id="edit_startpfx" name="startpfx" data-tip="流水时填写;特殊标识用于获取流水号"  >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_endpfx">后一个字符</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="form-control" id="edit_endpfx" name="endpfx" data-tip="流水时填写;特殊标识用于获取流水号"  >
                    </div>
                </td>
			</tr>
        </table>
    </form>
</body>
</html>
