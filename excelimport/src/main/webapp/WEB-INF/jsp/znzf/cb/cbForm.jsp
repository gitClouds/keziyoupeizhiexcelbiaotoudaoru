<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <form class="form_chag" id="cbEditForm" name="cbEditForm">
        <input type="hidden" id="edit_curdType" name="curdType" >
        <table class="table table-bordered">
        	<!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
            <!-- 请根据需要修改 可编辑项、hidden等 -->
            <!-- 表单校验的字段需要在input外加一层 <div class="form_controls"> -->
            <!-- 表单校验的input classes + required -->
            <!-- 表单校验的input data-tip="" 为进入时提醒 -->
            <!-- 表单校验的input data-valid="" 为验证规则，多个可使用‘||’分割；具体参照jquery-validate.js-->
            <!-- 表单校验的input data-error="" 为验证不通过时的提醒，需与 data-valid一一对应 -->
            <!-- 校验需自己填写 -->
			<tr>
        		<td class="sec_tit">
                    <label for="edit_pk">主键</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_pk" name="pk" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_cbbh">串并编号</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_cbbh" name="cbbh" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_sertype">卡号所属类型：1-银行，2-三方</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_sertype" name="sertype" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_bankcode">卡号所属机构编号</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_bankcode" name="bankcode" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_bankname">卡号所属机构名称</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_bankname" name="bankname" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_cardname">姓名</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_cardname" name="cardname" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_cardnumber">卡号</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_cardnumber" name="cardnumber" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_rksj">字段注释</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_rksj" name="rksj" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_yxx">字段注释</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_yxx" name="yxx" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_bz">字段注释</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_bz" name="bz" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
                        
        </table>
    </form>
</body>
</html>
