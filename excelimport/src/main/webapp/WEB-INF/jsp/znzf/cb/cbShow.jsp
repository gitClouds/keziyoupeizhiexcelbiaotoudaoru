<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">主键</td>
            <td id="show_pk"></td>
    		<td class="sec_tit">串并编号</td>
            <td id="show_cbbh"></td>
		</tr>
		<tr>
    		<td class="sec_tit">卡号所属类型：1-银行，2-三方</td>
            <td id="show_sertype"></td>
    		<td class="sec_tit">卡号所属机构编号</td>
            <td id="show_bankcode"></td>
		</tr>
		<tr>
    		<td class="sec_tit">卡号所属机构名称</td>
            <td id="show_bankname"></td>
    		<td class="sec_tit">姓名</td>
            <td id="show_cardname"></td>
		</tr>
		<tr>
    		<td class="sec_tit">卡号</td>
            <td id="show_cardnumber"></td>
    		<td class="sec_tit">字段注释</td>
            <td id="show_rksj"></td>
		</tr>
		<tr>
    		<td class="sec_tit">字段注释</td>
            <td id="show_yxx"></td>
    		<td class="sec_tit">字段注释</td>
            <td id="show_bz"></td>
		</tr>
    </table>
</body>
</html>
