<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">主键PK</td>
            <td id="show_pk"></td>
    		<td class="sec_tit">串并编号</td>
            <td id="show_cbbh"></td>
		</tr>
		<tr>
    		<td class="sec_tit">卡号所属类型：1-银行，2-三方</td>
            <td id="show_sertype"></td>
    		<td class="sec_tit">请求PK</td>
            <td id="show_qqpk"></td>
		</tr>
		<tr>
    		<td class="sec_tit">接警单PK</td>
            <td id="show_jjdpk"></td>
    		<td class="sec_tit">MAC地址</td>
            <td id="show_macdz"></td>
		</tr>
		<tr>
    		<td class="sec_tit">请求账号</td>
            <td id="show_qqzh"></td>
    		<td class="sec_tit">请求账号姓名</td>
            <td id="show_qqzhxm"></td>
		</tr>
		<tr>
    		<td class="sec_tit">请求账号机构代码</td>
            <td id="show_qqzhjgdm"></td>
    		<td class="sec_tit">请求账号机构名称</td>
            <td id="show_qqzhjgmc"></td>
		</tr>
		<tr>
    		<td class="sec_tit">接警单号码</td>
            <td id="show_jjdhm"></td>
    		<td class="sec_tit">报案人姓名</td>
            <td id="show_barxm"></td>
		</tr>
		<tr>
    		<td class="sec_tit">受理单位代码</td>
            <td id="show_sldwdm"></td>
    		<td class="sec_tit">受理单位名称</td>
            <td id="show_sldwmc"></td>
		</tr>
		<tr>
    		<td class="sec_tit">案发时间</td>
            <td id="show_afsj"></td>
    		<td class="sec_tit">字段注释</td>
            <td id="show_rksj"></td>
		</tr>
		<tr>
    		<td class="sec_tit">字段注释</td>
            <td id="show_yxx"></td>
		</tr>
    </table>
</body>
</html>
