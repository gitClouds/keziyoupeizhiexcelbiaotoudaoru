<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
</head>
<body>
<!-- 主体内容 -->
<section class="content">
	<!--查询项表单 begin-->
    <div class="row">
        <input type="hidden" name="jjdpk" id="jjdpk" value="${jjdpk}" />
        <%--<div class="col-xs-12">
            <ul class="box clearfix">
                <form class="form_chag form-inline" name="cbMacGlQueryForm" id="cbMacGlQueryForm">

                	<!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
                	<!-- 请根据需要修改 查询检索项 -->
                    	&lt;%&ndash;<div class="form-group">
	                        <label for="cbbh">串并编号</label>
	                        <input type="text" class="form-control" id="cbbh" name="cbbh" >
	                    </div>&ndash;%&gt;
                    	&lt;%&ndash;<div class="form-group">
	                        <label for="macdz">MAC地址</label>
	                        <input type="text" class="form-control" id="macdz" name="macdz" >
	                    </div>
                    	<div class="form-group">
	                        <label for="qqzh">请求账号</label>
	                        <input type="text" class="form-control" id="qqzh" name="qqzh" >
	                    </div>&ndash;%&gt;
                    	&lt;%&ndash;<div class="form-group">
	                        <label for="qqzhjgdm">请求账号机构代码</label>
	                        <input type="text" class="form-control" id="qqzhjgdm" name="qqzhjgdm" >
	                    </div>&ndash;%&gt;
                    	&lt;%&ndash;<div class="form-group">
	                        <label for="jjdhm">接警单号</label>
	                        <input type="text" class="form-control" id="jjdhm" name="jjdhm" >
	                    </div>&ndash;%&gt;
                        &lt;%&ndash;<div class="form-group">
                            <label for="jjdpk">接警单号</label>
                            <input type="hidden" class="form-control" id="jjdpk" name="jjdpk" value="">
                        </div>&ndash;%&gt;
                    &lt;%&ndash;<div class="form-group ">
                        <button type="button" style="margin-left: 8%" class="btn btn-info" onclick="search();">检索</button>
                    </div>&ndash;%&gt;
                </form>
            </ul>
        </div>--%>
    </div>
	<!--查询项表单 end-->
	<!--结果展示 begin-->
	<div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="initCbMacGabGridPage" class="table table-bordered table-striped table-condensed">

                    </table>
                    <!--toolbar -->
                    <%--<div class="btn-group" id="toolbar">
                        <i class="btn" onclick="addCbMacGl();">
                            <i class="fa fa-plus-square fa-2x"></i>
                        </i>
                        <i class="btn" onclick="exportCbMacGl();">
                            <i class="fa fa-cloud-download fa-2x"></i>
                        </i>
                    </div>--%>
                </div>
            </div>
        </div>
    </div>
    <!--结果展示 end-->
    <!-- 提示模态框（Modal） -->
    <div class="modal fade" id="tipsModal" tabindex="-1" role="dialog" aria-labelledby="tipsModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="tipsModalLabel">提示</h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="tipsMessage"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" id="tipsConfirmBtn">确定</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 详情模态框（Modal） -->
    <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="showModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="showMessage">
                    <iframe src="" id="zcShowIframe" width="100%" scrolling="no" frameborder="0"></iframe>
                </div>
                <%--<div class="modal-body" id="showMessage">
                    <jsp:include page="cbMacGlShow.jsp"/>
                </div>--%>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 编辑模态框（Modal） -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="editModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="editMessage">
                    <jsp:include page="cbMacGlForm.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" id="editConfirmBtn">保存</button>
                </div>
            </div>
        </div>
    </div>
    
    <!-- 字典、树形字典展示框。如果page、form均无字典项，可无需此div -->
    <div id="treeDiv">
        <input type="text" class="form-control" id="searchTree"  placeholder="支持文本检索">
        <div id="tree"></div>
    </div>
</section>
<script>
    var ctx = "${ctx}";
</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-closeable-tab.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/znzf/cbMacGab.js"></script>
<script>
    $(function () {
        initCbMacGabGridPage();
    })

</script>
</body>
</html>