<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html>
<head>
    <title>流水号查询详情</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
    <link rel="stylesheet" href="${ctx}/css/pub/showBase.css">
    <style>
        .tab_title{
            width: 100px;
        }
        body{
            background-color: transparent;
        }
        .table_list_box{
            table-layout:fixed !important;
        }
        .table{
            table-layout: fixed!important;
        }
    </style>
</head>
<body onload="changeIframeHeight('zcShowIframe');">
<input type="hidden" name="applicationid" id="applicationid" value="${qq.applicationid}" />
<div class="banner_box" >
    <div class="banner_table_box">
        <table class="show_tab" cellspacing=0; cellpadding=0; border=1; style="margin-top: 12px">
            <tr>
                <td class="tab_title">三方机构</td>
                <td style="width: 23%">${qq.payname}</td>
                <td class="tab_title">流水号</td>
                <td style="width: 22%">${qq.accnumber}</td>
                <td class="tab_title">支付机构类型</td>
                <td>${dicMap["010304"][jg.zfjglx]}</td>
            </tr>
            <tr>
                <td class="tab_title">交易类型</td>
                <td>${dicMap["010305"][jg.jylx]}</td>
                <td class="tab_title">支付类型</td>
                <td>${dicMap["010306"][jg.zflx]}</td>
                <td class="tab_title">币种</td>
                <td>${jg.rmbzl}</td>
            </tr>
            <tr>
                <td class="tab_title">金额</td>
                <td>${jg.je}</td>
                <td class="tab_title">收款卡号银行</td>
                <td>${jg.skfyhjgmc}</td>
                <td class="tab_title">收款银行卡</td>
                <td>${jg.skfzh}</td>
            </tr>
            <tr>
                <td class="tab_title">收款支付帐号</td>
                <td>${jg.skfzfzh}</td>
                <td class="tab_title">消费POS机</td>
                <td>${jg.posbh}</td>
                <td class="tab_title">收款方商户号</td>
                <td>${jg.skfshhm}</td>
            </tr>
            <tr>
                <td class="tab_title">付款卡号银行</td>
                <td>${jg.fkfyhjgmc}</td>
                <td class="tab_title">付款银行卡</td>
                <td>${jg.fkfzh}</td>
                <td class="tab_title">付款支付帐号</td>
                <td>${jg.fkfzfzh}</td>
            </tr>
            <tr>
                <td class="tab_title">交易设备</td>
                <td>${dicMap["010309"][jg.jysblx]}</td>
                <td class="tab_title">交易设备号</td>
                <td>${jg.sbhm}</td>
                <td class="tab_title">支付IP</td>
                <td>${jg.sbip}</td>
            </tr>
            <tr>
                <td class="tab_title">MAC地址</td>
                <td>${jg.macdz}</td>
                <td class="tab_title">交易地点经/纬度</td>
                <td>${jg.jyddjd}/${jg.jyddwd}</td>
                <td class="tab_title">银行外部流水号</td>
                <td>${jg.wbqdlsh}</td>
            </tr>
            <tr>
                <td class="tab_title">反馈机构</td>
                <td>${jg.fkjgmc}</td>
                <td class="tab_title">反馈结果</td>
                <td>${dicMap["010301"][jg.resultcode]}</td>
                <td class="tab_title">反馈说明</td>
                <td>${jg.fksm}</td>
            </tr>
            <tr>
                <td class="tab_title">经办人姓名</td>
                <td>${jg.jbrxm}</td>
                <td class="tab_title">经办人电话</td>
                <td>${jg.jbrdh}</td>
                <td class="tab_title">备注</td>
                <td>${jg.bz}</td>
            </tr>
            <tr>
                <td class="tab_title">是否反馈</td>
                <td>
                    <c:choose>
                        <c:when test="${qq.resflag==0}">
                            未反馈
                        </c:when>
                        <c:when test="${qq.resflag==1}">
                            已反馈
                        </c:when>
                        <c:when test="${qq.resflag==2}">
                            失败反馈
                        </c:when>
                    </c:choose>
                </td>
                <td class="tab_title">法律文书</td>
                <td colspan="3">
                    <c:choose>
                    <c:when test="${qq.flws==null || qq.flws==''}">

                    </c:when>
                    <c:otherwise>
                        <div class="btn-group">
                            <i class="btn" onclick="fileUpload.fileDownLoad(this)" data-key="${ctx}/attachment/download?pk=${qq.flws}">
                                <i class="fa fa-download fa-lg"></i>
                            </i>
                        </div>
                    </c:otherwise>
                    </c:choose>
                </td>
            </tr>
        </table>
    </div>
</div>
<script>
    var ctx = "${ctx}";
</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/resources/common/fileUpload.js"></script>
<script src="${ctx}/js/common.js"></script>
<script>
    var changeIframeHeight = function (iframeId) {
        window.parent.document.getElementById(iframeId).style.height="350px";
    }
</script>
</body>
</html>
