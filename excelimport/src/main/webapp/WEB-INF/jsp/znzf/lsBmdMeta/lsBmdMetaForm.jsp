<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <form class="form_chag" id="lsBmdMetaEditForm" name="lsBmdMetaEditForm">
        <input type="hidden" id="edit_curdType" name="curdType" >
        <input type="hidden" id="edit_pk" name="pk" >
        <table class="table table-bordered">
        	<tr>
				<td class="sec_tit">
                    <label for="edit_bankcode">类型</label>
                </td>
                <td>
                    	<select name="lx" id="lx" style="width: 300px;" onchange="lxChange()">
                    		<option value="1">银行</option>
                    		<option value="2">三方</option>
                    	</select>
                </td>
        		<td class="sec_tit">
                    <label for="edit_checkvalue">银行卡号/三方账号</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="form-control" id="edit_zh" name="zh" />
                    </div>
                </td>
            </tr>
			<tr class='yh'>
        		<td class="sec_tit">
                    <label for="edit_bankcode">银行代码</label>
                </td>
                <td>
                	<%--<div class="form_controls">
                    	<input type="text" class="form-control" id="edit_bankcode" name="bankcode" />
                    </div>--%>
                    <div class="form-group has-feedback form_controls" style="width: 100%;">
                        <input type="hidden" id="edit_bankcode" name="bankcode" />
                        <input type="text" class="form-control" id="edit_bankname" name="bankname" data-jzd-type="code" data-jzd-code="539"  data-jzd-filter="" data-jzd-dm="edit_bankcode" data-jzd-mc="edit_bankname" readonly data-tip="" data-valid="isNonEmpty" data-error="不能为空" >
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_bankcode');clearInput('edit_bankname');"></span>
                    </div>
                </td>
             </tr>
             <tr class='sf' style="display: none;">
                <td class="sec_tit">
                    <label for="edit_paycode">三方代码</label>
                </td>
                <td>
                    <%--<div class="form_controls">
                        <input type="text" class="form-control" id="edit_paycode" name="paycode" />
                    </div>--%>
                    <div class="form-group has-feedback form_controls" style="width: 100%;">
                        <input type="hidden" id="edit_paycode" name="paycode" />
                        <input type="text" class="form-control" id="edit_payname" name="payname" data-jzd-type="code" data-jzd-code="542"  data-jzd-filter="" data-jzd-dm="edit_paycode" data-jzd-mc="edit_payname" readonly data-tip="" data-valid="isNonEmpty" data-error="不能为空" >
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_paycode');clearInput('edit_payname');"></span>
                    </div>
                </td>
			</tr>
			<!-- <tr>
        		<td class="sec_tit">
                    <label for="edit_checkvalue">验证值</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="form-control" id="edit_checkvalue" name="checkvalue" />
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_positionval">值位置</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" class="form-control" id="edit_positionval" name="positionval" />
                    </div>
                </td>
			</tr>
            <tr>
                <td class="sec_tit">
                    <label for="edit_checkvalue">匹配/非匹配</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" class="form-control" id="edit_yesorno" name="yesorno" />
                    </div>
                </td>
            </tr> -->
        </table>
    </form>
</body>
<script type="text/javascript">
	function lxChange(){
		var lx=$("#lx option:selected").val();
		if(lx == '1'){
			$(".yh").show();
			$(".sf").hide();
		    $("#edit_paycode").val('-');
		    $("#edit_payname").val('-');
		}else{
			$(".sf").show();
			$(".yh").hide();
			$("#edit_bankcode").val('-');
		    $("#edit_bankname").val('-');
		}
	}
</script>
</html>
