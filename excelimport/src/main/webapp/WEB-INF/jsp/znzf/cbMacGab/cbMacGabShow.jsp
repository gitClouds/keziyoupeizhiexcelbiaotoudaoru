<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">主键PK</td>
            <td id="show_pk"></td>
    		<td class="sec_tit">数据类型</td>
            <td id="show_sertype"></td>
		</tr>
		<tr>
    		<td class="sec_tit">请求PK</td>
            <td id="show_qqpk"></td>
    		<td class="sec_tit">接警单PK</td>
            <td id="show_jjdpk"></td>
		</tr>
		<tr>
    		<td class="sec_tit">业务申请编号</td>
            <td id="show_ywsqbh"></td>
    		<td class="sec_tit">MAC地址</td>
            <td id="show_macdz"></td>
		</tr>
		<tr>
    		<td class="sec_tit">请求账号</td>
            <td id="show_qqzh"></td>
    		<td class="sec_tit">请求账号姓名</td>
            <td id="show_qqzhxm"></td>
		</tr>
		<tr>
    		<td class="sec_tit">请求账号机构代码</td>
            <td id="show_qqzhjgdm"></td>
    		<td class="sec_tit">请求账号机构名称</td>
            <td id="show_qqzhjgmc"></td>
		</tr>
		<tr>
    		<td class="sec_tit">请求录入单位代码</td>
            <td id="show_qqlrdwdm"></td>
    		<td class="sec_tit">请求录入单位名称</td>
            <td id="show_qqlrdwmc"></td>
		</tr>
		<tr>
    		<td class="sec_tit">录入时间</td>
            <td id="show_lrsj"></td>
    		<td class="sec_tit">有效性</td>
            <td id="show_yxx"></td>
		</tr>
		<tr>
    		<td class="sec_tit">简要案情</td>
            <td id="show_sy"></td>
    		<td class="sec_tit">数量</td>
            <td id="show_count"></td>
		</tr>
    </table>
</body>
</html>
