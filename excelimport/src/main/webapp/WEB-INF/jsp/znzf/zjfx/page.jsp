<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html>
<head>
    <title>资金分析</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/showBase.css">
    <STYLE>
        body{
            background-color: #FFFFFF;
        }
    </STYLE>
</head>
<body>
<jsp:include page="../../loading.jsp"/>
<input type="hidden" id="show_pk" value="${jjdpk}" />
<input type="hidden" id="jjdpk" name="jjdpk" value="${jjdpk}" />
<input type="hidden" id="iszcygzs" name="iszcygzs" value="1" />
<input type="radio" class="hide" checked="checked" name="xxType" value="xyr" />
<div id="addTable" class="banner_table_box">

</div>
<div class="borderDiv borderDiv_tb">
    <div style="padding: 10px;" class="hide" id="keepOnBtn">
        <button type="button" class="btn btn-warning" onclick="keepOnAnalysis();">继续</button>
    </div>
    <div style="padding: 10px">
        <jsp:include page="../znzf_req.jsp" />
    </div>
</div>
<!-- 详情模态框（Modal） -->
<div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-show">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" id="showModalLabel"></h4><%--模态框（Modal）标题--%>
            </div>
            <div class="modal-body" id="showMessage">
                <%--<jsp:include page="${ctx}/znzf/base/show"/>--%>
                <iframe src="" id="zcShowIframe" width="100%" scrolling="no" frameborder="0"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>
<!-- 编辑模态框（Modal） -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-show">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" id="editModalLabel"></h4><%--模态框（Modal）标题--%>
            </div>
            <div class="modal-body" id="editMessage">
                <%--<jsp:include page="znzfYpslForm.jsp"/>--%>
                    <jsp:include page="form.jsp"/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" id="editConfirmBtn">提交</button>
            </div>
        </div>
    </div>
</div>
<script>
    var ctx = "${ctx}";
    var zfPerm=false;
    <shiro:hasPermission name="ROLE_ZF">
    zfPerm = true;
    </shiro:hasPermission>
</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-closeable-tab.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/resources/common/fileUpload.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/fzzx/jjdZjl.js"></script>
<script src="${ctx}/js/znzf/znzf.js"></script>
<script src="${ctx}/js/yayk/yayk.js"></script>
<script>

    $(function () {
        /*$(".banner_tab li").on("click",function () {
        $(this).addClass("tab_item_get").siblings().removeClass("tab_item_get");
    });*/
        searchLevel('0','${jjdpk}','1');
        $('#editModal').on('hidden.bs.modal', function (event) {
            $("#editConfirmBtn").unbind();
            formEditValidateReset('setUpNlevelEditForm');
        });
        formEditValidate('setUpNlevelEditForm');
    });
    <shiro:hasPermission name="ROLE_XTGL">
        $("#keepOnBtn").removeClass("hide");
    </shiro:hasPermission>
</script>
</body>
</html>
