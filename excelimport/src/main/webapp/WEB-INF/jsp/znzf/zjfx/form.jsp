<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>等级设置</title>
</head>
<body>
<form class="form_chag" id="setUpNlevelEditForm" name="setUpNlevelEditForm">
    <input type="hidden" id="jltPk" name="pk" value="" >

    <table class="table table-bordered">
        <tr>
            <td class="sec_tit">
                <label for="jlt_cardnumber">账号</label>
            </td>
            <td>
                <div class="form_controls">
                    <input type="text" class="REQUIRED form-control" id="jlt_cardnumber" name="cardnumber" data-tip="" data-valid="isNonEmpty" data-error="账号不能为空" >
                </div>
            </td>
        </tr>
        <tr>
            <td class="sec_tit">
                <label for="jlt_parentzh">上级账号</label>
            </td>
            <td>
                <div class="form_controls">
                    <input class="required form-control" id="jlt_parentzh" type="text" name="parentzh" data-tip="" data-valid="isNonEmpty" data-error="不能为空">
                </div>
            </td>
        </tr>
        <tr>
            <td class="sec_tit">
                <label for="jlt_nlevel">等级</label>
            </td>
            <td>
                <div class="form_controls">
                    <input class="required form-control" id="jlt_nlevel" type="text" name="nlevel" data-tip="" data-valid="isNonEmpty||onlyInt" data-error="不能为空||只能输入数字">
                </div>
            </td>
        </tr>
    </table>
</form>
</body>
</html>
