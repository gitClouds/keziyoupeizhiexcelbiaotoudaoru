<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
<div class="borderDiv clearfix">
    <div class="titDiv form-group" style="padding-top: 0px;margin-bottom: 0px">
        <h5>查询事由</h5>
    </div>
    <div style="clear: both"></div>
    <table class="table table-bordered">
        <tr>
            <td style="width: 55px"><label>查询事由:</label></td>
            <td colspan="3" >
                <div class="form_controls" style="display: inline-block;width: 95%">
                    <textarea style="resize:none" rows="3" class="required form-control" id="sy" name="sy" data-valid="isNonEmpty||between:2-2000" data-error="不能为空||长度为2-2000位">涉及电信诈骗案</textarea>
                </div>
            </td>
        </tr>
    </table>
</div>
<div class="borderDiv clearfix">
    <div class="titDiv form-group" style="padding-top: 0px;margin-bottom: 0px">
        <h5>经办人&协查人信息</h5>
    </div>
    <div style="clear: both"></div>
    <%--<div class="form-group">
        <label>证件类型:</label>
        <div class="form_controls">
            <input type="text" value="警官证" class="form-control" readonly >
        </div>
    </div>--%>
    <table class="table table-bordered" id="jgzTable">
        <tr>
            <th colspan="2" style="padding-left: 10%;padding-bottom:10px;font-size: 14px;">经办人</th>
            <th colspan="2" style="padding-left: 10%;padding-bottom:10px;font-size: 14px;">协查人</th>
        </tr>
        <tr>
            <td style="width: 55px;text-align: right"><label>警官证号:</label></td>
            <td>
                <div class="form_controls" style="display: inline-block;width: 45%">
                    <input type="text" value="" class="form-control" id="jbrzjhm" name="jbrzjhm" readonly >
                </div>
            </td>
            <td style="width: 55px;text-align: right"><label>警官证号:</label></td>
            <td>
                <div class="form_controls" style="display: inline-block;width: 45%">
                    <input type="text" value="" class="form-control" id="xcrzjhm" name="xcrzjhm" readonly >
                </div>
            </td>
        </tr>
        <tr>
            <td style="text-align: right"><label>姓名:</label></td>
            <td>
                <div class="form_controls" style="display: inline-block;width: 45%">
                    <input type="text" value="" class="form-control" id="jbrxm" name="jbrxm" readonly >
                </div>
            </td>
            <td style="text-align: right"><label>姓名:</label></td>
            <td>
                <div class="form_controls" style="display: inline-block;width: 45%">
                    <input type="text" value="" class="form-control" id="xcrxm" name="xcrxm" readonly >
                </div>
            </td>
        </tr>
        <tr>
            <td style="text-align: right"><label>电话:</label></td>
            <td>
                <div class="form_controls" style="display: inline-block;width: 45%">
                    <input type="text" value="" class="form-control" id="jbrdh" name="jbrdh" readonly >
                </div>
            </td>
            <td style="text-align: right"><label>电话:</label></td>
            <td>
                <div class="form_controls" style="display: inline-block;width: 45%">
                    <input type="text" value="" class="form-control" id="xcrdh" name="xcrdh" readonly >
                </div>
            </td>
        </tr>
        <tr>
            <td style="text-align: right"><label>警官证:</label></td>
            <td class="jgz_file">
                <input id="jbr_jgz" type="file" class="hidden" >
            </td>
            <td style="text-align: right"><label>警官证:</label></td>
            <td class="jgz_file">
                <input id="xcr_jgz" type="file" class="hidden" >
            </td>
        </tr>
    </table>
</div>
</body>
</html>
