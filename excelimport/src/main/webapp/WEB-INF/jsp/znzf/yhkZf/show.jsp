<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html>
<head>
    <title>银行卡止付详情</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
    <link rel="stylesheet" href="${ctx}/css/pub/showBase.css">
    <style>
        .tab_title{
            width: 100px;
        }
        body{
            background-color: transparent;
        }
    </style>
</head>
<body onload="changeIframeHeight('zcShowIframe');">
<div class="banner_box" >
    <div class="banner_table_box">
        <table class="show_tab" cellspacing=0; cellpadding=0; border=1; style="margin-top: 12px">
            <tr>
                <td class="tab_title">是否反馈</td>
                <td style="width: 22%">
                    <c:choose>
                        <c:when test="${qq.resflag==0}">
                            未反馈
                        </c:when>
                        <c:when test="${qq.resflag==1}">
                            已反馈
                        </c:when>
                        <c:when test="${qq.resflag==2}">
                            失败反馈
                        </c:when>
                    </c:choose>
                </td>
                <td class="tab_title">所属银行</td>
                <td style="width: 23%">${qq.bankname}</td>
                <td class="tab_title">账/卡号</td>
                <td>${qq.cardnumber}</td>
                <%--<td class="tab_title">业务申请编号</td>
                <td>${jg.applicationid}</td>--%>
            </tr>
            <tr>
                <td class="tab_title">账户名称</td>
                <td>${qq.accountname}</td>
                <td class="tab_title">账户余额</td>
                <td>${jg.accountbalance}</td>
                <td class="tab_title">转出金额</td>
                <td>${qq.transferamount}</td>
            </tr>
            <tr>
                <td class="tab_title">转出时间</td>
                <td><fmt:formatDate value="${qq.transfertime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
                <td class="tab_title">止付开始时间</td>
                <td><fmt:formatDate value="${qq.starttime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
                <td class="tab_title">止付截止时间</td>
                <td><fmt:formatDate value="${qq.expiretime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
            </tr>
            <tr>
                <td class="tab_title">反馈机构</td>
                <td>${jg.feedbackorgname}</td>
                <td class="tab_title">反馈说明</td>
                <td>${jg.feedbackremark}</td>
                <td class="tab_title">未能止付原因</td>
                <td>${jg.failurecause}</td>
            </tr>
            <tr>
                <td class="tab_title">止付结果</td>
                <td>${dicMap["010301"][jg.resultcode]}</td>
                <td class="tab_title">经办人姓名</td>
                <td>${jg.operatorname}</td>
                <td class="tab_title">经办人电话</td>
                <td>${jg.operatorphonenumber}</td>
            </tr>
            <tr>
                <td class="tab_title">法律文书</td>
                <td colspan="5">
                    <c:choose>
                        <c:when test="${qq.flws==null || qq.flws==''}">

                        </c:when>
                        <c:otherwise>
                            <div class="btn-group">
                                <i class="btn" onclick="fileUpload.fileDownLoad(this)" data-key="${ctx}/attachment/download?pk=${qq.flws}">
                                    <i class="fa fa-download fa-lg"></i>
                                </i>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
        </table>
    </div>
</div>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/resources/common/fileUpload.js"></script>
<script src="${ctx}/js/common.js"></script>
<script>
    var changeIframeHeight = function (iframeId) {
        window.parent.document.getElementById(iframeId).style.height="300px";
    }
</script>
</body>
</html>
