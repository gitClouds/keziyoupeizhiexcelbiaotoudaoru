<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/fileinput/css/fileinput.min.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
    <link rel="stylesheet" href="${ctx}/css/pub/form.css">
</head>
<body>
<jsp:include page="../../loading.jsp" />
<!-- 主体内容 -->
<section class="content">
    <!--查询项表单 begin-->
    <div class="row">
        <div class="col-xs-12">
            <ul class="box clearfix">
                <form class="form_chag" id="yhkZtQqEditForm" name="yhkZtQqEditForm">
                    <input type="hidden" id="xxType" name="xxType" value="${xxType}" />
                    <input type="hidden" id="nlevel" name="nlevel" value="${nlevel}" />
                    <input type="hidden" id="parentpk" name="parentpk" value="${parentpk}" />
                    <input type="hidden" id="parentzh" name="parentzh" value="${parentzh}" />
                    <input type="hidden" id="jjdPk" name="jjdPk" value="${jjdPk}" />
                    <input type="hidden" id="iszcygzs" name="iszcygzs" value="${iszcygzs}" />
                    <div class="borderDiv">
                        <div class="beanData clearfix" data-bean-num="0" style="border-bottom: 1px #ccc dashed">
                            <div class="form-group">
                                <label>主体类别:</label>
                                <label class="radio-inline">
                                    <input type="radio" id="bean_zhlb_0" name="bean[0].zhlb" value="1" checked />个人
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="bean[0].zhlb" value="2" />对公
                                </label>
                            </div>
                            <div style="clear: both;"></div>
                            <div class="form-group">
                                <label>账&nbsp;&nbsp;户&nbsp;&nbsp;名:</label>
                                <div class="form_controls">
                                    <input type="text" class="required form-control" id="bean_zhxm_0" name="bean[0].zhxm" data-valid="isNonEmpty||between:2-30" data-error="不能为空||长度为2-30位"  >
                                </div>
                            </div>
                            <div class="form-group">
                                <label>账/卡号:</label>
                                <div class="form_controls">
                                    <input type="hidden" id="bean_zhlx_0" name="bean[0].zhlx" value="" >
                                    <input type="text" onblur="getBank(this);" class="required form-control" id="bean_zh_0" name="bean[0].zh" data-valid="isNonEmpty||onlyInt||between:8-24" data-error="不能为空||只能输入数字||长度为8-24位" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label>所属银行:</label>
                                <div class="has-feedback form_controls " >
                                    <input type="hidden" value="" id="bean_zhjgdm_0" name="bean[0].zhjgdm" />
                                    <input type="text" value='' class="required form-control" id="bean_zhjgmc_0" name="bean[0].zhjgmc" data-jzd-type="code" data-jzd-code="539" data-jzd-filter="" data-jzd-dm="bean_zhjgdm_0" data-jzd-mc="bean_zhjgmc_0" readonly  data-valid="isNonEmpty" data-error="不能为空" >
                                    <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('bean_zhjgdm_0');clearInput('bean_zhjgmc_0');"></span>
                                </div>
                            </div>
                            <div style="clear: both;"></div>
                            <input type="hidden" name="bean[0].mongo" id="bean_mongo_0" value="">
                            <table class="table table-bordered" id="jgzTable">
                                <tr>
                                    <td style="width: 55px;text-align: right"><label>法律手续:</label></td>
                                    <td style="width: 110px;"><button type="button" style="float: left;" class="btn btn-info" id="bean_flws_btn_0" onclick="showFlwsFile(0);">生成法律手续</button></td>
                                    <td id="edit_flws_0" style="width: 300px;">

                                    </td>
                                    <td style="text-align: right;padding-right: 20px;font-size: 14px">
                                        <i class="fa fa-minus-square fa-2x text-red" onclick="removeZnzfBean(this);"></i>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="add_btn">
                            <i class="fa fa-plus-square fa-2x" onclick="addZnzfYhkZt();"></i>
                        </div>
                    </div>
                    <jsp:include page="../znzf_jbr.jsp"/>

                    <button type="button" style="float: right;margin-right: 4%;" class="btn btn-info" onclick="yhkZtQqEditSubmit(this);">提交</button>
                    <div style="clear: both"></div>
                </form>
            </ul>
        </div>
    </div>
    <!-- 字典、树形字典展示框。如果page、form均无字典项，可无需此div -->
    <div id="treeDiv">
        <input type="text" class="form-control" id="searchTree"  placeholder="支持文本检索">
        <div id="tree"></div>
    </div>
</section>
<script>
    var ctx = "${ctx}";
    var beanNum  = 0;
</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-closeable-tab.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/resources/bootstrap/fileinput/js/fileinput.min.js"></script>
<script src="${ctx}/resources/bootstrap/fileinput/js/zh.js"></script>
<script src="${ctx}/resources/common/fileUpload.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/znzf/znzf.js"></script>
<script src="${ctx}/js/znzf/yhkZt.js"></script>

</body>
</html>
