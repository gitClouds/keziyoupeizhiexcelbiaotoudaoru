<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html>
<head>
    <title>银行卡主体详情</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
    <link rel="stylesheet" href="${ctx}/css/pub/showBase.css">
    <style>
        .tab_title{
            width: 100px;
        }
        body{
            background-color: transparent;
        }
    </style>
</head>
<body onload="changeIframeHeight('zcShowIframe');">
<div class="banner_box" >
    <div class="banner_table_box">
        <table class="show_tab" cellspacing=0; cellpadding=0; border=1; style="margin-top: 12px">
            <tr>
                <td class="tab_title">所属银行</td>
                <td style="width: 23%">${qq.bankname}</td>
                <td class="tab_title">账/卡号</td>
                <td style="width: 22%">${qq.cardnumber}</td>
                <td class="tab_title">主体名称</td>
                <td>${jg.cxztmc}</td>
                <%--<td class="tab_title">业务申请编号</td>
                <td>${jg.applicationid}</td>--%>
            </tr>
            <tr>
                <td class="tab_title">证照类型</td>
                <td>${dicMap["537"][jg.zjlx]}</td>
                <td class="tab_title">证照号码</td>
                <td>${jg.zjlxhm}</td>
                <td class="tab_title">联系手机</td>
                <td>${jg.cxsj}</td>
            </tr>
            <tr>
                <td class="tab_title">住宅地址</td>
                <td>${jg.zzdz}</td>
                <td class="tab_title">住宅电话</td>
                <td>${jg.zzdh}</td>
                <td class="tab_title">邮箱地址</td>
                <td>${jg.yxdz}</td>
            </tr>
            <tr>
                <td class="tab_title">工作单位</td>
                <td>${jg.gzdw}</td>
                <td class="tab_title">单位地址</td>
                <td>${jg.gzdwdz}</td>
                <td class="tab_title">单位电话</td>
                <td>${jg.gzdwdh}</td>
            </tr>
            <tr>
                <td class="tab_title">代办人姓名</td>
                <td>${jg.dbrxm}</td>
                <td class="tab_title">代办人证件类型</td>
                <td>${dicMap["537"][jg.dbrzjlx]}</td>
                <td class="tab_title">代办人证件号码</td>
                <td>${jg.dbrzjlxhm}</td>
            </tr>
            <tr>
                <td class="tab_title">反馈机构</td>
                <td>${jg.fkjgmc}</td>
                <td class="tab_title">反馈结果</td>
                <td>${dicMap["010301"][jg.resultcode]}</td>
                <td class="tab_title">反馈说明</td>
                <td>${jg.fksm}</td>
            </tr>
            <tr>
                <td class="tab_title">法人代表</td>
                <td>${jg.frdb}</td>
                <td class="tab_title">法人代表证件类型</td>
                <td>${dicMap["537"][jg.frdbzjlx]}</td>
                <td class="tab_title">法人代表证件号码</td>
                <td>${jg.frdbzjlxhm}</td>
            </tr>
            <tr>
                <td class="tab_title">原工商号码</td>
                <td>${jg.khygszchm}</td>
                <td class="tab_title">国税纳税号</td>
                <td>${jg.gsnsh}</td>
                <td class="tab_title">地税纳税号</td>
                <td>${jg.dsnsh}</td>
                <%--<td class="tab_title">证照失效日期</td>
                <td>${jg.zjshrq}</td>--%>
            </tr>
            <tr>
                <td class="tab_title">经办人姓名</td>
                <td>${jg.jbrxm}</td>
                <td class="tab_title">经办人电话</td>
                <td>${jg.jbrdh}</td>
                <td class="tab_title">备注</td>
                <td>${jg.bz}</td>
            </tr>
            <tr>
                <td class="tab_title">是否反馈</td>
                <td>
                    <c:choose>
                        <c:when test="${qq.resflag==0}">
                            未反馈
                        </c:when>
                        <c:when test="${qq.resflag==1}">
                            已反馈
                        </c:when>
                        <c:when test="${qq.resflag==2}">
                            失败反馈
                        </c:when>
                    </c:choose>
                </td>
                <td class="tab_title">法律文书</td>
                <td colspan="3">
                    <c:choose>
                        <c:when test="${qq.flws==null || qq.flws==''}">

                        </c:when>
                        <c:otherwise>
                            <div class="btn-group">
                                <i class="btn" onclick="fileUpload.fileDownLoad(this)" data-key="${ctx}/attachment/download?pk=${qq.flws}">
                                    <i class="fa fa-download fa-lg"></i>
                                </i>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
        </table>
    </div>
</div>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/resources/common/fileUpload.js"></script>
<script src="${ctx}/js/common.js"></script>
<script>
    var changeIframeHeight = function (iframeId) {
        window.parent.document.getElementById(iframeId).style.height=500+"px";
    }
</script>
</body>
</html>
