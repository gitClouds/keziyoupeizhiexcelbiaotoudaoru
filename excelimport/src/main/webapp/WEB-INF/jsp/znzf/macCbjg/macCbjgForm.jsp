<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <form class="form_chag" id="macCbjgEditForm" name="macCbjgEditForm">
        <input type="hidden" id="edit_curdType" name="curdType" >
        <table class="table table-bordered">
        	<!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
            <!-- 请根据需要修改 可编辑项、hidden等 -->
            <!-- 表单校验的字段需要在input外加一层 <div class="form_controls"> -->
            <!-- 表单校验的input classes + required -->
            <!-- 表单校验的input data-tip="" 为进入时提醒 -->
            <!-- 表单校验的input data-valid="" 为验证规则，多个可使用‘||’分割；具体参照jquery-validate.js-->
            <!-- 表单校验的input data-error="" 为验证不通过时的提醒，需与 data-valid一一对应 -->
            <!-- 校验需自己填写 -->
			<tr>
        		<%--<td class="sec_tit">
                    <label for="edit_pk">主键PK</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_pk" name="pk" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>--%>
        		<td class="sec_tit">
                    <label for="edit_sertype">卡号所属类型</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" onblur="showJg();" class="required form-control" id="edit_sertype" name="sertype" data-tip="" data-valid="isNonEmpty" data-error="不能为空" >
                    </div>
                </td>
			</tr>
			<tr>
                <td class="sec_tit">
                    <label for="edit_zhjgdm">所属机构</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="hidden" value="" id="edit_zhjgdm" name="zhjgdm"  />
                        <input type="text" value='' class="required form-control" id="edit_zhjgmc" name="zhjgmc" data-jzd-type="code" data-jzd-code="542" data-jzd-filter="" data-jzd-dm="edit_zhjgdm" data-jzd-mc="edit_zhjgmc" readonly  data-valid="isNonEmpty" data-error="不能为空" >
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_zhjgdm');clearInput('edit_zhjgmc');"></span>
                        <%--<input type="text" class="required form-control" id="edit_zhjgdm" name="zhjgdm" data-tip="" data-valid="" data-error="" >--%>
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_thzf">替换字符</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_thzf" name="thzf" data-tip="" data-valid="between:0-2" data-error="长度过长" >
                    </div>
                </td>
			</tr>

        </table>
    </form>
</body>
</html>
