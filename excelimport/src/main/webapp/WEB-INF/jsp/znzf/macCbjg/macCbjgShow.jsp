<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">主键PK</td>
            <td id="show_pk"></td>
    		<td class="sec_tit">卡号所属类型</td>
            <td id="show_sertype"></td>
		</tr>
		<tr>
    		<td class="sec_tit">账户机构代码</td>
            <td id="show_zhjgdm"></td>
    		<td class="sec_tit">账户机构名称</td>
            <td id="show_zhjgmc"></td>
		</tr>
		<tr>
    		<td class="sec_tit">替换字符</td>
            <td id="show_thzf"></td>
    		<td class="sec_tit">字段注释</td>
            <td id="show_rksj"></td>
		</tr>
    </table>
</body>
</html>
