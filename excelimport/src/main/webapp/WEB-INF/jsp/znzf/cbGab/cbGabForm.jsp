<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <form class="form_chag" id="cbGabEditForm" name="cbGabEditForm">
        <input type="hidden" id="edit_curdType" name="curdType" >
        <table class="table table-bordered">
        	<!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
            <!-- 请根据需要修改 可编辑项、hidden等 -->
            <!-- 表单校验的字段需要在input外加一层 <div class="form_controls"> -->
            <!-- 表单校验的input classes + required -->
            <!-- 表单校验的input data-tip="" 为进入时提醒 -->
            <!-- 表单校验的input data-valid="" 为验证规则，多个可使用‘||’分割；具体参照jquery-validate.js-->
            <!-- 表单校验的input data-error="" 为验证不通过时的提醒，需与 data-valid一一对应 -->
            <!-- 校验需自己填写 -->
			<tr>
        		<td class="sec_tit">
                    <label for="edit_pk">主键PK</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_pk" name="pk" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_sertype">数据类型</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_sertype" name="sertype" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_jjdpk">接警单PK</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_jjdpk" name="jjdpk" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_ywsqbh">业务申请编号</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_ywsqbh" name="ywsqbh" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_bankcode">银行机构编号</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_bankcode" name="bankcode" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_cardnumber">账号</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_cardnumber" name="cardnumber" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_cardname">账号姓名</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_cardname" name="cardname" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_lrdwdm">录入单位名称</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_lrdwdm" name="lrdwdm" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_lrsj">录入时间</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_lrsj" name="lrsj" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_rksj">入库时间</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_rksj" name="rksj" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_yxx">有效性</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_yxx" name="yxx" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_sy">简要案情</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_sy" name="sy" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
                        
        </table>
    </form>
</body>
</html>
