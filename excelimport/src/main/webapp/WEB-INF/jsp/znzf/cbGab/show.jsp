<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
    <link rel="stylesheet" href="${ctx}/css/pub/showBase.css">
    <style>
        .tab_title{
            width: 100px;
        }
        body{
            background-color: transparent;
        }
        .table_list_box{
            table-layout:fixed !important;
        }
        .table{
            table-layout: fixed!important;
        }
       .fixed-table-container {
           height: 300px!important;
       }
        .modal-dialog {
            display: none;
            z-index: 99999;
            position: fixed;
            top: 1%;
            left: 50%;
            margin-left: -300px;
            box-shadow: 0 0 10px #555;
            border-radius: 5px;
        }
        .fixed-table-container {
            height: 242px!important;
        }
    </style>
</head>
<body onload="changeIframeHeight('zcShowIframe');">

    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">简要案情</h4>
            </div>
            <div class="modal-body">
                <p></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->


<input type="hidden" id="jjdpk" name="jjdpk" value="${jjdpk}"  />
<input type="hidden" id="cardnumber" name="cardnumber" value="${cardnumber}"  />
<!-- 主体内容 -->
<table id="cbGabGrids" class="table table-bordered table-striped table-condensed">

</table>
<!--toolbar -->
<%--<div class="btn-group" id="toolbar">
    <i class="btn" onclick="exportCbGab();">
        <i class="fa fa-cloud-download fa-2x"></i>
    </i>
</div>--%>
<!-- 字典、树形字典展示框。如果page、form均无字典项，可无需此div -->
<div id="treeDiv">
    <input type="text" class="form-control" id="searchTree"  placeholder="支持文本检索">
    <div id="tree"></div>
</div>
<script>
    var ctx = "${ctx}";
</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/znzf/cbGab.js"></script>
<script>
    var changeIframeHeight = function (iframeId) {
        window.parent.document.getElementById(iframeId).style.height="300px";
    }
    $(window).resize(function () {
        $('#cbGabGrids').bootstrapTable('resetView');
    });
    $(function () {
        initCbGabGrids();
        $(".btn-default,.close").on("click",function () {
            $(".modal-dialog").stop().slideToggle(300);
        })

    })
</script>
</body>
</html>