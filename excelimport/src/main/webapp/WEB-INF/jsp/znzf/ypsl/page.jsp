<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html>
<head>
    <title>研判思路</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <STYLE>
        body{
            background-color: #FFFFFF;
        }
    </STYLE>
</head>
<body onload="changeIframeHeight('zcygzsIframe');">
<input type="hidden" id="jjdpk" name="jjdpk" value="${jjdpk}" />
<table id="znzfYpslGrid" class="table table-bordered table-striped table-condensed">

</table>
<!--toolbar -->
<div class="btn-group" id="toolbar">
    <i class="btn" onclick="addZnzfYpsl();">
        <i class="fa fa-plus-square fa-2x"></i>
    </i>
</div>
<!-- 提示模态框（Modal） -->
<div class="modal fade" id="tipsModal" tabindex="-1" role="dialog" aria-labelledby="tipsModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" id="tipsModalLabel">提示</h4><%--模态框（Modal）标题--%>
            </div>
            <div class="modal-body" id="tipsMessage"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" data-toggle="modal" id="tipsConfirmBtn">确定</button>
            </div>
        </div>
    </div>
</div>
<!-- 编辑模态框（Modal） -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-show">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" id="editModalLabel"></h4><%--模态框（Modal）标题--%>
            </div>
            <div class="modal-body" id="editMessage">
                <jsp:include page="znzfYpslForm.jsp"/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" id="editConfirmBtn">提交</button>
            </div>
        </div>
    </div>
</div>
<script>
    var ctx = "${ctx}";
</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/resources/common/fileUpload.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/yayk/zcygzs.js"></script>
<script>
    var changeIframeHeight = function (iframeId) {
        window.parent.document.getElementById(iframeId).style.height=440+"px";
    }
</script>
</body>
</html>
