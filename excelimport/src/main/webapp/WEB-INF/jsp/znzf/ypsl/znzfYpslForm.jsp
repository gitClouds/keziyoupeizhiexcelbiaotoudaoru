<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <form class="form_chag" id="znzfYpslEditForm" name="znzfYpslEditForm">
        <input type="hidden" id="edit_curdType" name="curdType" >
        <input type="hidden" id="edit_pk" name="pk"  >
        <input type="hidden" id="edit_jjdpk" name="jjdpk" value="${jjdpk}"  >
        <table class="table table-bordered">
			<tr>
        		<td class="sec_tit">
                    <label for="edit_nr">内容</label>
                </td>
                <td>
                	<div class="form_controls" style="display: inline-block;width: 95%">
                        <textarea style="resize:none" rows="3" class="required form-control" id="edit_nr" name="nr" data-valid="isNonEmpty||between:2-2000" data-error="不能为空||长度为2-2000位">${jjd.jyaq}</textarea>
                    </div>
                </td>
			</tr>
        </table>
    </form>
</body>
</html>
