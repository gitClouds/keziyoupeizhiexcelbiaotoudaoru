<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">主键</td>
            <td id="show_pk"></td>
    		<td class="sec_tit">串并编号</td>
            <td id="show_cbbh"></td>
		</tr>
		<tr>
    		<td class="sec_tit">卡号所属类型：1-银行，2-三方</td>
            <td id="show_sertype"></td>
    		<td class="sec_tit">MAC地址</td>
            <td id="show_mazdz"></td>
		</tr>
		<tr>
    		<td class="sec_tit">入库时间</td>
            <td id="show_rksj"></td>
    		<td class="sec_tit">字段注释</td>
            <td id="show_yxx"></td>
		</tr>
    </table>
</body>
</html>
