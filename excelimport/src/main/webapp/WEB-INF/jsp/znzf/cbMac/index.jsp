<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
    <style>
        .modal-show {
            width: 95%;
            margin: 30px auto;
        }
    </style>
</head>
<body>
<!-- 主体内容 -->
<section class="content">
    <!--查询项表单 begin-->
    <div class="row">
        <div class="col-xs-12">
            <ul class="box clearfix">
                <form class="form_chag form-inline" name="cbJjdQueryForm" id="cbJjdQueryForm">
                    <!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
                    <!-- 请根据需要修改 查询检索项 -->
                    <div class="form-group">
                        <label for="jjdhm">接警单号</label>
                        <input type="text" class="form-control" id="jjdhm" name="jjdhm" >
                    </div>
                    <div class="form-group">
                        <label for="barxm">报案人</label>
                        <input type="text" class="form-control" id="barxm" name="barxm" >
                    </div>
                    <div class="form-group">
                        <label for="ajlbmc">案件类别</label>
                        <div class="dic_ipt">
                            <input type="hidden" id="ajlb" name="ajlb" />
                            <input type="text" class="form-control" id="ajlbmc" data-jzd-type="tree" data-jzd-code="010107" data-jzd-filter="" data-jzd-dm="ajlb" data-jzd-mc="ajlbmc" readonly >
                            <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('ajlb');clearInput('ajlbmc');"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="afsj_begin">案发时间</label>
                        <input type="text" class="Wdate form-control" id="afsj_begin" name="afsj_begin" onFocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'afsj_end\')||\'%y-%M-{%d}\'}'})" style="width: 30%!important" >
                        &nbsp;<input type="text" class="Wdate form-control" id="afsj_end" name="afsj_end" onFocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'%y-%M-{%d}',minDate:'#F{$dp.$D(\'afsj_begin\')}'})" style="width: 29%!important" >
                    </div>
                    <div class="form-group">
                        <label for="lrdwmc">录入单位</label>
                        <div class="dic_ipt">
                            <input type="hidden" id="lrdwdm" name="lrdwdm" />
                            <input type="text" class="form-control" id="lrdwmc" data-jzd-type="tree" data-jzd-code="DEPT" data-jzd-filter="" data-jzd-dm="lrdwdm" data-jzd-mc="lrdwmc" readonly >
                            <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('lrdwdm');clearInput('lrdwmc');"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="saje_begin">涉案金额</label>
                        <input type="text" class="form-control" id="saje_begin" name="saje_begin" style="width: 30%!important" >
                        &nbsp;<input type="text" class="form-control" id="saje_end" name="saje_end" style="width: 29%!important" >
                    </div>
                    <div class="form-group" >
                        <label for="nlevel">层级</label>
                        <select class="form-control" id="nlevel" name="nlevel" style="width: 60%" >
                            <option></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </div>
                    <div class="form-group" style="width: 10%;float: right">
                        <button type="button" style="margin-left: 8%" class="btn btn-info" onclick="search();">检索</button>
                        <%--<button type="reset" style="margin-left: 8%" class="btn btn-warning">重置</button>--%>
                    </div>
                </form>
            </ul>
        </div>
    </div>
    <!--查询项表单 end-->
    <!--结果展示 begin-->
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="cbJjdGrid" class="table table-bordered table-striped table-condensed">

                    </table>
                    <!--toolbar -->
                    <div class="btn-group" id="toolbar">
                        <i class="btn" onclick="exportYayk();">
                            <i class="fa fa-cloud-download fa-2x"></i>
                        </i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--结果展示 end-->
    <!-- 字典、树形字典展示框。如果page、form均无字典项，可无需此div -->
    <div id="treeDiv">
        <input type="text" class="form-control" id="searchTree"  placeholder="支持文本检索">
        <div id="tree"></div>
    </div>
</section>
<script>
    var ctx = "${ctx}";
</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-closeable-tab.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/znzf/cbMac.js"></script>
<%--<script src="${ctx}/js/yayk/yayk.js"></script>--%>
<script>
    /*var date = new Date().initDateZeroTime();
    $("#afsj_begin").val(date);*/
</script>
</body>
</html>