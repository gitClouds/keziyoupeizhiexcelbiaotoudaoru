<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <form class="form_chag" id="znzfRemindMetaEditForm" name="znzfRemindMetaEditForm">
        <input type="hidden" id="edit_curdType" name="curdType" />
        <input type="hidden" id="edit_pk" name="pk" />
        <table class="table table-bordered">
			<tr>
        		<td class="sec_tit">
                    <label for="edit_bankcode">机构代码</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class=" form-control" id="edit_bankcode" name="bankcode" />
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_remindtype">提醒类型值</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" class=" form-control" id="edit_remindtype" name="remindtype" />
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_checkfield">验证字段</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class=" form-control" id="edit_checkfield" name="checkfield" />
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_checkdata">验证值</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class=" form-control" id="edit_checkdata" name="checkdata" />
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_positionval">值所在位置</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class=" form-control" id="edit_positionval" name="positionval" />
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_remark">提醒内容</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" class=" form-control" id="edit_remark" name="remark" />
                    </div>
                </td>
			</tr>
        </table>
    </form>
</body>
</html>
