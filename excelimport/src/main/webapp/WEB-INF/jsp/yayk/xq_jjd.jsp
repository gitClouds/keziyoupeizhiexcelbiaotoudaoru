<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <style>
        #myFocus{
             position: absolute;
             top: 20px;
             right: 30px;
         }
        #macCbGab{
            position: absolute;
            top: 20px;
            right: 110px;
        }
    </style>
</head>
<body>
<div class="banner_box" >
    <div class="banner_title">
        <h4>基本信息</h4>
        <span class="btn" id="macCbGab" onclick="macCbGab(this);" data-type="2" >MAC部串并</span>
        <span class="btn btn-info" id="myFocus" onclick="userFocus(this);" data-type="0" >关注</span>
    </div>
    <ul class="banner_tab">
        <li class="tab_item_get" onclick="jjdTab(1);">接警信息</li>
        <li onclick="jjdTab(2);">研判报告</li>
        <%--<li>案件信息</li>
        <li>网侦交互</li>
        <li>技侦交互</li>
        <li>资金返还</li>
        <li>法律文书</li>--%>
    </ul>
    <div class="banner_table_box" id="jjd_jbxx">
        <table class="show_tab" cellspacing=0; cellpadding=0; border=1; style="margin-top: 12px">
            <tr>
                <td class="tab_title" colspan="2">接警单号</td>
                <td><input type="text" value="" class="form-control" id="show_jjdhm" readonly /></td>
                <td class="tab_title">止付金额</td>
                <td><input type="text" value="" class="form-control" id="show_zfje" readonly /></td>
                <td class="tab_title">受理单位</td>
                <td><input type="text" class="form-control" value="" id="show_sldwmc" readonly /></td>
                <td class="tab_title">报警时间</td>
                <td><input type="text" value="" class="form-control" id="show_jjsj" readonly /></td>
            </tr>
            <tr>
                <td style="width: 80px">报案人</td>
                <td class="tab_title">姓名</td>
                <td><input type="text" value="" class="form-control" id="show_barxm" readonly /></td>
                <td class="tab_title">身份证号</td>
                <td><input type="text" value="" class="form-control" id="show_barsfzh" readonly /></td>
                <td class="tab_title">联系电话</td>
                <td><input type="text" value="" class="form-control" id="show_barlxdh" readonly /></td>
                <td class="tab_title">接警方式</td>
                <td><input type="text" value="" class="form-control" id="show_jjfs" readonly /></td>
            </tr>
        </table>
        <table class="show_tab" cellspacing=0; cellpadding=0; border=1;>
            <tr>
                <td rowspan="4" style="width: 80px">案件信息</td>
                <td class="tab_title">案件类型</td>
                <td><input type="text" value="" class="form-control" id="show_ajlx" readonly /></td>
                <td class="tab_title">案件类别</td>
                <td><input type="text" value="" class="form-control" id="show_ajlb" readonly /></td>
                <td class="tab_title">案发时间</td>
                <td><input type="text" value="" class="form-control" id="show_afsj" readonly /></td>
                <td class="tab_title">涉案金额</td>
                <td><input type="text" value="" class="form-control" id="show_saje" readonly /></td>
            </tr>
            <tr>
                <td class="tab_title">案发地点</td>
                <td colspan="7"><input type="text" value="" class="form-control" id="show_afdd" readonly /></td>
            </tr>
            <tr>
                <td class="tab_title">简要案情</td>
                <td colspan="7" id="show_jyaq" style="text-align: left;padding:8px 12px;font-size:14px;color:#555;"></td>
            </tr>
            <tr>
                <td class="tab_title">备注信息</td>
                <td colspan="7" id="show_bz" style="text-align: left"></td>
            </tr>
        </table>
        <table class="show_tab shrData" data-bean-num="0" cellspacing=0; cellpadding=0; border=1;>
            <tr>
                <td rowspan="3" style="width: 80px">受害人信息</td>
                <td class="tab_title">银行卡号</td>
                <td><input type="text" value="" class="form-control" id="show_shr_yhk_0" readonly /></td>
                <td class="tab_title">持卡人</td>
                <td><input type="text" value="" class="form-control" id="show_shr_ckr_0" readonly /></td>
                <td class="tab_title">身份证号</td>
                <td><input type="text" value="" class="form-control" id="show_shr_sfzh_0" readonly /></td>
                <td class="tab_title">资金流出</td>
                <td><input type="text" value="" class="form-control" id="show_shr_zzfs_0" readonly /></td>
            </tr>
            <tr>
                <td class="tab_title">转账时间</td>
                <td><input type="text" value="" class="form-control" id="show_shr_zzsj_0" readonly /></td>
                <td class="tab_title">转账金额</td>
                <td><input type="text" value="" class="form-control" id="show_shr_zzje_0" readonly /></td>
                <td class="tab_title">支付宝</td>
                <td><input type="text" value="" class="form-control" id="show_shr_zfb_0" readonly /></td>
                <td class="tab_title">财付通</td>
                <td><input type="text" value="" class="form-control" id="show_shr_cft_0" readonly /></td>
            </tr>
            <tr>
                <td class="tab_title">微信账号</td>
                <td><input type="text" value="" class="form-control" id="show_shr_wx_0" readonly /></td>
                <td class="tab_title">电话号码</td>
                <td><input type="text" value="" class="form-control" id="show_shr_dh_0" readonly /></td>
                <td class="tab_title">交易单号</td>
                <td><input type="text" value="" class="form-control" id="show_shr_jydh_0" readonly /></td>
                <td class="tab_title">QQ号</td>
                <td><input type="text" value="" class="form-control" id="show_shr_qq_0" readonly /></td>
            </tr>
        </table>
        <table class="show_tab shxxData" data-bean-num="0" cellspacing=0; cellpadding=0; border=1;>
            <tr>
                <td style="width: 80px">商户信息</td>
                <td class="tab_title">商户名称</td>
                <td><input type="text" value="" class="form-control" id="show_shxx_shmc_0" readonly /></td>
                <td class="tab_title">联系方式</td>
                <td><input type="text" value="" class="form-control" id="show_shxx_lxfs_0" readonly /></td>
                <td class="tab_title">商户号</td>
                <td><input type="text" value="" class="form-control" id="show_shxx_shh_0" readonly /></td>
                <td class="tab_title">交易单号</td>
                <td><input type="text" value="" class="form-control" id="show_shxx_jydh_0" readonly /></td>
            </tr>
        </table>
        <table class="show_tab xyrData" data-bean-num="0" cellspacing=0; cellpadding=0; border=1;>
            <tr>
                <td rowspan="3" style="width: 80px">嫌疑人信息</td>
                <td class="tab_title">银行卡号</td>
                <td><input type="text" value="" class="form-control" id="show_xyr_yhk_0" readonly /></td>
                <td class="tab_title">持卡人</td>
                <td><input type="text" value="" class="form-control" id="show_xyr_ckr_0" readonly /></td>
                <td class="tab_title">身份证号</td>
                <td><input type="text" value="" class="form-control" id="show_xyr_sfzh_0" readonly /></td>
                <td class="tab_title">资金流出</td>
                <td><input type="text" value="" class="form-control" id="show_xyr_zzfs_0" readonly /></td>
            </tr>
            <tr>
                <td class="tab_title">转账时间</td>
                <td><input type="text" value="" class="form-control" id="show_xyr_zzsj_0" readonly /></td>
                <td class="tab_title">转账金额</td>
                <td><input type="text" value="" class="form-control" id="show_xyr_zzje_0" readonly /></td>
                <td class="tab_title">支付宝</td>
                <td><input type="text" value="" class="form-control" id="show_xyr_zfb_0" readonly /></td>
                <td class="tab_title">财付通</td>
                <td><input type="text" value="" class="form-control" id="show_xyr_cft_0" readonly /></td>
            </tr>
            <tr>
                <td class="tab_title">微信账号</td>
                <td><input type="text" value="" class="form-control" id="show_xyr_wx_0" readonly /></td>
                <td class="tab_title">电话号码</td>
                <td><input type="text" value="" class="form-control" id="show_xyr_dh_0" readonly /></td>
                <td class="tab_title">交易单号</td>
                <td><input type="text" value="" class="form-control" id="show_xyr_jydh_0" readonly /></td>
                <td class="tab_title">QQ号</td>
                <td><input type="text" value="" class="form-control" id="show_xyr_qq_0" readonly /></td>
            </tr>
        </table>
    </div>
    <div class="banner_table_box" id="jjd_ypbg" style="display: none">
        <span class="btn btn-sm btn-info" style="margin: 10px;" id="generatYpbgBtn" onclick="generatYpbg();" >生成研判报告</span>
        <table id="ypbgGrid" class="table table-bordered table-striped table-condensed">

        </table>
    </div>
</div>
<script>
    var ctx = "${ctx}";
    var shrNum = 0;
    var xyrNum = 0;
    var shxxNum = 0;

    function jjdTab(sType) {
        if (sType==1){
            $("#jjd_jbxx").show();
            $("#jjd_ypbg").hide();
        }else if (sType==2){
            $("#jjd_ypbg").show();
            $("#jjd_jbxx").hide();
        }
    }
</script>
</body>
</html>
