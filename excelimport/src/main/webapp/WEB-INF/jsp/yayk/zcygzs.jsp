<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>侦查员工作室</title>
</head>
<body>
<div class="banner_box" >
    <div class="banner_title">
        <h4>侦查员工作室</h4>
    </div>
    <ul class="banner_tab">
        <li class="tab_item_get" onclick="switchZcygzsTab('ypsl','','${jjdpk}');">研判思路</li>
        <%-- <li id="zcygzs_zjfx" onclick="switchZcygzsTab('zjfx','','${jjdpk}');">资金分析</li> --%>
        <li class="non_tab" onclick="switchZcygzsTab('swdt','','${jjdpk}');">金流图</li>
        <li onclick="switchZcygzsTab('sjsc','1','${jjdpk}');">银行数据</li>
        <li onclick="switchZcygzsTab('sjsc','2','${jjdpk}');">第三方数据</li>
        <li onclick="switchZcygzsTab('sjsc','3','${jjdpk}');">视频图像</li>
        <li onclick="switchZcygzsTab('sjsc','4','${jjdpk}');">人员信息</li>
        <li onclick="switchZcygzsTab('sjsc','5','${jjdpk}');">话单数据</li>
        <li onclick="switchZcygzsTab('sjsc','6','${jjdpk}');">网络信息</li>
        <li onclick="switchZcygzsTab('sjsc','7','${jjdpk}');">文档存储</li>
    </ul>
    <div class="banner_table_box" id="banner_zcygzs_box">
        <iframe src="" id="zcygzsIframe" width="100%" scrolling="no" frameborder="0"></iframe>
    </div>
</div>
</body>
</html>
