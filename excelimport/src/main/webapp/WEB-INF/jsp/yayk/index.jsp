<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
    <style>
        .modal-show {
            width: 95%;
            margin: 30px auto;
        }
    </style>
</head>
<body>
<!-- 主体内容 -->
<section class="content">
    <!--查询项表单 begin-->
    <div class="row">
        <div class="col-xs-12">
            <ul class="box clearfix">
                <form class="form_chag form-inline" name="yaykQueryForm" id="yaykQueryForm">
                    <!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
                    <!-- 请根据需要修改 查询检索项 -->
                    <div class="form-group">
                        <label for="zh">三方账号</label>
                        <input type="text" class="form-control" id="zh" name="zh" >
                    </div>
                   	<div class="form-group">
                        <label for="xm">户主姓名</label>
                        <input type="text" class="form-control" id="xm" name="xm" >
                    </div>
                 	<div class="form-group" >
                        <label for="nlevel">层级</label>
                        <select class="form-control" id="cj" name="cj" style="width: 60%" >
                            <option></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="xm">录入人</label>
                        <input type="text" class="form-control" id="lrrxm" name="lrrxm" >
                    </div>
                    <div class="form-group" style="width: 10%;float: right">
                        <button type="button" style="margin-left: 8%" class="btn btn-info" onclick="search();">检索</button>
                        <%--<button type="reset" style="margin-left: 8%" class="btn btn-warning">重置</button>--%>
                    </div>
                </form>
            </ul>
        </div>
    </div>
    <!--查询项表单 end-->
    <!--结果展示 begin-->
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="yaykGrid" class="table table-bordered table-striped table-condensed">

                    </table>
                    <!--toolbar -->
                    <!-- <div class="btn-group" id="toolbar">
                        <i class="btn" onclick="exportYayk();">
                            <i class="fa fa-cloud-download fa-2x"></i>
                        </i>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
    <!--结果展示 end-->
    <!-- 字典、树形字典展示框。如果page、form均无字典项，可无需此div -->
    <div id="treeDiv">
        <input type="text" class="form-control" id="searchTree"  placeholder="支持文本检索">
        <div id="tree"></div>
    </div>
</section>
<script>
    var ctx = "${ctx}";
</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-closeable-tab.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/yayk/yayk.js"></script>
<script>
    /*var date = new Date().initDateZeroTime();
    $("#afsj_begin").val(date);*/
</script>
</body>
</html>