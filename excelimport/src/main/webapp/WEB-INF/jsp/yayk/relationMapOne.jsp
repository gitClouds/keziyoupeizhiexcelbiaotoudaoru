<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/jquery-ui/jquery-ui.min.css">
    <script type="text/javascript" src="${ctx}/resources/group/js/plugins/d3/d3.v3.min.js"></script>
    <script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
    <link rel="stylesheet" href="${ctx}/resources/group/css/demo.css"/>
    <script>
        //iframe真正解决高度自适应
        function changeFrameHeight(iframeId){
            var ifm= document.getElementById(iframeId);
            ifm.height=document.documentElement.clientHeight;

        }

        window.onresize=function(){
            changeFrameHeight();

        }
    </script>
</head>
<body>

<div>
    <div class="row">
        <div class="col-xs-12">
            <ul class="box clearfix">
                <form style="margin-top: 10px" class="form_chag form-inline" id="mxQueryForm" name="mxQueryForm">
                    <div class="form-group" style="width: 100%;margin-bottom: 10px;">
                        <input type="hidden" name="pk" value="${pk}">
                        <label for="money">交易金额</label>
                        <input type="text" style="width: 15%!important" value="" class="required form-control" id="money" name="money"
                               data-valid="isNonEmpty||onlyInt" data-error="不能为空||只能输入数字">
                        <b>元的倍数</b>
                        <label for="select_nlevel">层级</label>
                        <select class="form-control" id="select_nlevel">
                            <option selected="selected" value="">请选择</option>
                        </select>&nbsp;
                        <label for="begin_time">交易时间</label>
                        <input type="text" class="Wdate form-control" id="begin_time" name="begin_time" onFocus="WdatePicker({lang:'zh-cn',readOnly:true,dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'end_time\')||\'%y-%M-{%d}\'}'})" style="width: 15%!important" >
                        <b>至</b>
                        <input type="text" class="Wdate form-control" id="end_time" name="end_time" onFocus="WdatePicker({lang:'zh-cn',readOnly:true,dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'%y-%M-{%d}',minDate:'#F{$dp.$D(\'begin_time\')}'})" style="width: 15%!important" >
                        <div style="width: 8%;float: right;">
                            <button type="button" class="btn btn-info" onclick="sx();">查询</button>
                        </div>
                    </div>
                    <%--  <div class="form-group" style="width: 60%">
                          <label for="times">交易时间段</label>
                          <input type="text" style="width: 15%!important" value="0" class="required form-control" id="jysjb" name="jysjb" data-valid="isNonEmpty||onlyInt||between:1-2||maxIntVal:24" data-error="不能为空||只能输入数字||长度为1-2||不能大于24">
                          -
                          <input type="text" style="width: 15%!important" value="9" class="required form-control" id="jysje" name="jysje" data-valid="isNonEmpty||onlyInt||between:1-2||maxIntVal:24" data-error="不能为空||只能输入数字||长度为1-2||不能大于24">
                          <b>点</b>

                      </div>--%>
                </form>
            </ul>
        </div>
    </div>
</div>
<jsp:include page="../loading.jsp"/>
<!-- 模态框（Modal） -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" id="mx_dialog" style="width:90%;height: auto;">
        <div class="modal-content" style="border-radius:5px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    明细信息
                </h4>
            </div>
            <div class="modal-body">
                <%--<iframe src="${ctx}/znzf/jlt/echarts_page" id="echartsShow" width="100%" scrolling="no" frameborder="0"></iframe>--%>
                <%--<jsp:include page="echarts.jsp"/>--%>
                <iframe src="" id="zcShowIframe" width="100%"  scrolling="no" frameborder="0"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>

<!-- 公用模态框（Modal） -->
<div class="modal fade" id="myModal_public" tabindex="-1" role="dialog" aria-labelledby="myModalLabel_public" aria-hidden="true">
    <div class="modal-dialog" id="public_dialog" style="width:90%;">
        <div class="modal-content" style="border-radius:5px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel_public">
                    明细信息
                </h4>
            </div>
            <div class="modal-body">
                <%--<iframe src="${ctx}/znzf/jlt/echarts_page" id="echartsShow" width="100%" scrolling="no" frameborder="0"></iframe>--%>
                <%--<jsp:include page="echarts.jsp"/>--%>
                <iframe src="" id="publicShowIframe" width="100%" height="100%" onload="changeFrameHeight('publicShowIframe')" frameborder="0"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>
<script type="text/javascript">
    var ctx = '${ctx}';
    var pk = "${pk}";
    var url = "${ctx}/znzf/jlt/relation_map_data?pk=" + pk;

    function sx() {
        var money = $("#money").val();
        var begin_time = $("#begin_time").val();
        var end_time = $("#end_time").val();
        var nlevel = $('#select_nlevel').val();
        if ((begin_time != "" && end_time == "")
            || (end_time != "" && begin_time == "")) {
            alert("请选择时间范围!");
            return;
        }
        var page_url = "${ctx}/znzf/jlt/relation_map_two_page?pk=" + pk + "&money=" + money + "&begin_time=" + begin_time + "&end_time=" + end_time + "&nlevel=" +nlevel;
        window.location.href=page_url;

        /*d3.select("svg").remove();*/

    }
    var data_url = "${ctx}/znzf/jlt/relation_map_data?pk=${pk}&money=${money}&begin_time=${begin_time}&end_time=${end_time}&nlevel=${nlevel}";
    d3.json(data_url, function (json) {
        function GroupExplorer(wrapper, config) {
            var defaultConfig = {
                data: {"nodes": [], "links": []},
                width: window.innerWidth,
                height: window.innerHeight - 17,
                distance: 100
            };
            $.extend(true, defaultConfig, config);
            defaultConfig.data.links.forEach(function (e) {
                if (typeof e.source != "number" && typeof e.target != "number") {
                    var sourceNode = defaultConfig.data.nodes.filter(function (n) {
                            return n.name === e.source;
                        })[0],
                        targetNode = defaultConfig.data.nodes.filter(function (n) {
                            return n.name === e.target;
                        })[0];
                    e.source = sourceNode;
                    e.target = targetNode;
                }
            });
            var _this = this, highlighted = null, dependsNode = [], dependsLinkAndText = [];
            this.color = d3.scale.category20();
            var zoom = d3.behavior.zoom()
                .scaleExtent([0.2, 10])
                .on("zoom", function () {
                    _this.zoomed();
                });

            this.vis = d3.select("body").append("svg:svg")
                .attr("width", defaultConfig.width)
                .attr("height", defaultConfig.height)
                .call(zoom).on("dblclick.zoom", null);

            this.vis = this.vis.append('g').attr('class', 'all')
                .attr("width", defaultConfig.width)
                .attr("height", defaultConfig.height)


            this.force = d3.layout.force()
                .nodes(defaultConfig.data.nodes)
                .links(defaultConfig.data.links)
                .gravity(.0001)
                .distance(defaultConfig.distance)
                .charge(function (d) {
                    return (-10 * d.index)
                })
                .size([defaultConfig.width, defaultConfig.height])
                .start();
            this.vis.append("svg:defs").selectAll("marker")
                .data(["end"])
                .enter().append("svg:marker")
                .attr("id", "arrow")
                .attr('class', 'arrow')
                .attr("viewBox", "0 -5 10 10")
                .attr("refX", 27)
                .attr("refY", 0)
                .attr("markerWidth", 9)
                .attr("markerHeight", 16)
                .attr("markerUnits", "userSpaceOnUse")
                .attr("orient", "auto")
                .append("svg:path")
                .attr("d", "M0,-5L10,0L0,5")
                .attr('fill', '#666');

            this.link = this.vis.selectAll("line.link")
                .data(defaultConfig.data.links)
                .enter().append("svg:line")
                .attr("class", "link")
                .attr('stroke-width', 1)
                .attr("x1", function (d) {
                    return d.source.x;
                })
                .attr("y1", function (d) {
                    return d.source.y;
                })
                .attr("x2", function (d) {
                    return d.target.x;
                })
                .attr("y2", function (d) {
                    return d.target.y;
                })
                .attr("marker-end", "url(#arrow)")
                .attr('stroke', '#999');

            var dragstart = function (d, i) {
                _this.force.stop();
                d3.event.sourceEvent.stopPropagation();
            };

            var dragmove = function (d, i) {
                d.px += d3.event.dx;
                d.py += d3.event.dy;
                d.x += d3.event.dx;
                d.y += d3.event.dy;
                _this.tick();
            };

            var dragend = function (d, i) {
                d.fixed = true;
                _this.tick();
                _this.force.resume();
            };

            this.nodeDrag = d3.behavior.drag()
                .on("dragstart", dragstart)
                .on("drag", dragmove)
                .on("dragend", dragend);


            this.highlightObject = function (obj) {
                if (obj) {
                    var objIndex = obj.index;
                    dependsNode = dependsNode.concat([objIndex]);
                    dependsLinkAndText = dependsLinkAndText.concat([objIndex]);
                    defaultConfig.data.links.forEach(function (lkItem) {
                        if (objIndex == lkItem['source']['index']) {
                            dependsNode = dependsNode.concat([lkItem.target.index])
                        } else if (objIndex == lkItem['target']['index']) {
                            dependsNode = dependsNode.concat([lkItem.source.index])
                        }
                    });
                    _this.node.classed('inactive', function (d) {
                        return (dependsNode.indexOf(d.index) == -1)
                    });
                    _this.link.classed('inactive', function (d) {

                        return ((dependsLinkAndText.indexOf(d.source.index) == -1) && (dependsLinkAndText.indexOf(d.target.index) == -1))
                    });

                    _this.linetext.classed('inactive', function (d) {
                        return ((dependsLinkAndText.indexOf(d.source.index) == -1) && (dependsLinkAndText.indexOf(d.target.index) == -1))
                    });
                } else {
                    _this.node.classed('inactive', false);
                    _this.link.classed('inactive', false);
                    _this.linetext.classed('inactive', false);
                }
            };

            this.highlightToolTip = function (obj) {
                if (obj) {
                    _this.tooltip.html("<div class='title'>" + obj.name + "的资料</div><table class='detail-info'><tr><td class='td-label' >身份证：</td><td>" + obj.cardid + "</td></tr>" +
                        "<tr><td class='td-label' >账号：</td><td >" + obj.cardnumber + "</td></tr>" +
                        "<tr><td class='td-label' >电话：</td><td >" + obj.phone + "</td></tr>"+
                        "<tr><td class='td-label' >主体:</td><td style='padding: 5px;'><a style='margin: 5px;' href='javascript:void(0);' onclick='ztcx("+JSON.stringify(obj)+")' id='ztcx' class='btn-sm btn-info'  title='主体查询'>主体查询</a></td></tr>" +
                        "<tr><td class='td-label' >禁毒人员查询:</td><td style='padding: 5px;'><a style='margin: 5px;' href='javascript:void(0);' onclick='xdsdrycx("+JSON.stringify(obj)+")' id='xdsdrycx' class='btn-sm btn-info'  title='禁毒人员查询'>禁毒人员查询</a></td></tr>" +
                        "<tr><td class='td-label' >主体请求重发:</td><td style='padding: 5px;'><a style='margin: 5px;' href='javascript:void(0);' onclick='ztqqcf("+JSON.stringify(obj)+")' id='ztqqcf' class='btn-sm btn-info'  title='主体请求重发'>主体请求重发</a></td></tr>" +
                        "</table>")
                        .style("left", (d3.event.pageX + 20) + "px")
                        .style("top", (d3.event.pageY - 20) + "px")
                        .style("opacity", 1.0);
                    _this.tooltip.html();
                } else {
                    _this.tooltip.style("opacity", 0.0);
                }
            };

            this.tooltip = d3.select("body").append("div")
                .attr("class", "tooltip")
                .attr("opacity", 0.0)
                .on('dblclick', function () {
                    d3.event.stopPropagation();
                })
                .on('mouseover', function () {
                    if (_this.node.mouseoutTimeout) {
                        clearTimeout(_this.node.mouseoutTimeout);
                        _this.node.mouseoutTimeout = null;
                    }
                })
                .on('mouseout', function () {
                    if (_this.node.mouseoutTimeout) {
                        clearTimeout(_this.node.mouseoutTimeout);
                        _this.node.mouseoutTimeout = null;
                    }
                    _this.node.mouseoutTimeout = setTimeout(function () {
                        _this.highlightToolTip(null);
                    }, 300);
                });

            this.node = this.vis.selectAll("g.node")
                .data(defaultConfig.data.nodes)
                .enter().append("svg:g")
                .attr("class", "node")
                .call(_this.nodeDrag)
                .on('mouseover', function (d) {
                    if (_this.node.mouseoutTimeout) {
                        clearTimeout(_this.node.mouseoutTimeout);
                        _this.node.mouseoutTimeout = null;
                    }
                    _this.highlightToolTip(d);
                })
                .on('mouseout', function () {
                    if (_this.node.mouseoutTimeout) {
                        clearTimeout(_this.node.mouseoutTimeout);
                        _this.node.mouseoutTimeout = null;
                    }
                    _this.node.mouseoutTimeout = setTimeout(function () {
                        _this.highlightToolTip(null);
                    }, 300);
                })
                .on('dblclick', function (d) {
                    _this.highlightObject(d);
                    d3.event.stopPropagation();
                });
            d3.select("body").on('dblclick', function () {
                dependsNode = dependsLinkAndText = [];
                _this.highlightObject(null);
            });


            this.node.append("svg:image")
                .attr("class", "circle")
                .attr("xlink:href", function (d) {
                    if (d.isqk == '1') {
                        return "${ctx}/resources/img/qkperson.png";
                    } else {
                        return "${ctx}/resources/img/person.png";
                    }
                })
                .attr("x", "-15px")
                .attr("y", "-15px")
                .attr("width", "30px")
                .attr("height", "30px");
            this.node.append("svg:text")
                .text(function (d) {
                    if (d.rylx == '1') {
                        return '吸';
                    } else if (d.rylx == '2') {
                        return '涉';
                    } else if (d.rylx == '3') {
                        return '吸涉';
                    }
                })
                .attr("x", function (d) {
                    if (d.rylx == '1') {
                        return '-20px';
                    } else if (d.rylx == '2') {
                        return '5px';
                    } else {
                        return '-15px'
                    }
                })
                .attr("y", "5px")
                .attr('fill', function (d) {
                    if (d.rylx == '1') {
                        return '#ffa649';
                    } else if (d.rylx == '2') {
                        return '#ff213f';
                    } else {
                        return "rgb(241,39,255)";
                    }
                })
                .attr('style', 'cursor:default');

            this.node.append("svg:text")
                .attr("class", "nodetext")
                .attr("dy", "30px")
                .attr('text-anchor', 'middle')
                .text(function (d) {
                    if (d.isqk == '2') {
                        $(this).parent('.node').hide();
                    }
                    return d.name;
                })
                .attr('fill', function (d, i) {
                    return _this.color(i);
                });

            this.linetext = this.vis.selectAll('.linetext')
                .data(defaultConfig.data.links)
                .enter()
                .append("text")
                .attr("class", "linetext")
                .attr("x", function (d) {
                    return (d.source.x + d.target.x) / 2
                })
                .attr("y", function (d) {
                    return (d.source.y + d.target.y) / 2
                })
                .text(function (d) {
                    return d.relation
                })
                .attr('fill', function (d, i) {
                    return _this.color(i);
                })
                /*.attr('data-toggle','modal')
                .attr('data-target','#myModal')*/
                .attr('onclick', function (d) {
                    return 'showModel(' + JSON.stringify(d) + ')';
                })
                .attr('style', 'cursor:pointer')
                .call(this.force.drag);

            this.zoomed = function () {
                _this.vis.attr("transform", "translate(" + d3.event.translate + ") scale(" + d3.event.scale + ")")
            };


            var findMaxWeightNode = function () {
                var baseWeight = 1, baseNode;
                defaultConfig.data.nodes.forEach(function (item) {
                    if (item.weight > baseWeight) {
                        baseWeight = item.weight
                        baseNode = item
                    }
                });
                return baseNode;
            };

            this.tick = function () {
                var findMaxWeightNodeIndex = findMaxWeightNode().index;
                defaultConfig.data.nodes[findMaxWeightNodeIndex].x = defaultConfig.width / 2;
                defaultConfig.data.nodes[findMaxWeightNodeIndex].y = defaultConfig.height / 2;
                _this.link.attr("x1", function (d) {
                    return d.source.x;
                })
                    .attr("y1", function (d) {
                        return d.source.y;
                    })
                    .attr("x2", function (d) {
                        return d.target.x
                    })
                    .attr("y2", function (d) {
                        return d.target.y;
                    });
                _this.linetext.attr("x", function (d) {
                    return (d.source.x + d.target.x) / 2
                })
                    .attr("y", function (d) {
                        return (d.source.y + d.target.y) / 2
                    });
                _this.node.attr("transform", function (d) {
                    return "translate(" + d.x + "," + d.y + ")";
                });
            };
            _this.force.on("tick", this.tick);

        }

        new GroupExplorer('body', {
            data: json
        });
    });

</script>
<script>
    $(function () {

    })

    //明细信息查询
    function showModel(d) {
        console.info(d);
        var info = d;
        var qqlx = info.source.qqlx;
        var mxqqpk = info.source.mxqqpk;
        var zh = info.source.cardnumber;
        var xjzh = info.xjzh;
        var zcType = '';
        if (qqlx && qqlx == '1') {
            zcType = "yhkMx";
        }
        if (qqlx && qqlx == '2') {
            zcType = "sfMx";
        }
        if (!mxqqpk){
            return;
        }
        /*$("#mx_dialog").css({"width":"900px"});*/
        $("#zcShowIframe").attr("src", "${ctx}/znzf/base/show?zcType=" + zcType + "&pk=" + mxqqpk + "&zh=" + zh + "&xjzh=" + xjzh);
        $("#myModal").modal();
    }

    //下拉框
    var op = '';
    for (var i=1; i<=5; i++){
        op+='<option value="'+i+'">'+i+'级</option>';
    }
    $("#select_nlevel").append(op);

    //禁毒人员查询
    function xdsdrycx(obj) {
        var ztqqpk = obj.ztqqpk;
        var qqlx = obj.qqlx;
        var pk = obj.pk;
        var nlevel = obj.nlevel;
        if (!pk){
            return;
        }
        $.ajax({
            type: "get",
            dataType: "json",
            data:{ztpk:ztqqpk,sertype:qqlx,pk:pk},
            url: ctx+"/yayk/xdSdRyCx" ,
            beforeSend: function () {
                loading("查询中，请稍后......");
            },
            success: function (data) {
                top.layer.close(indexLoading);
                if (data.success){
                    top.layer.alert("查询成功："+data.msg, {icon: 1, title: '提示'});
                }else{
                    if(data.code =='1'){
                        top.layer.alert("查询失败："+data.msg, {icon: 2, title: '提示'});
                    }else if(data.code =='2'){
                        top.layer.confirm("查询失败："+data.msg+'需要提供身份证号号才能查询，确认继续吗？', {icon: 2, title: '提示'},function(r){
                            top.layer.prompt({title: '请输入身份证号码，点确认查询', formType: 0}, function(text, index){
                                $.ajax({
                                    type: "get",
                                    dataType: "json",
                                    data:{sfzh:text,pk:pk},
                                    url: ctx+"/yayk/xdSdRyCx2" ,
                                    beforeSend: function () {
                                        loading("查询中，请稍后......");
                                    },
                                    success: function (data) {
                                        top.layer.close(indexLoading);
                                        if (data.success){
                                            top.layer.alert("查询成功："+data.msg, {icon: 1, title: '提示'});
                                            top.layer.close(index);
                                        }else{
                                            top.layer.alert("查询失败：请求异常", {icon: 2, title: '提示'});
                                        }
                                    },
                                    error : function() {
                                        top.layer.alert("查询失败：请求异常", {icon: 2, title: '提示'});
                                    }
                                });
                            });
                        });
                    }else{
                        top.layer.alert("查询失败：请求异常", {icon: 2, title: '提示'});
                    }
                }
            },
            error : function() {
                top.layer.alert("查询失败：请求异常", {icon: 2, title: '提示'});
            }
        });

    }
    function loading(msg) {
        indexLoading = top.layer.msg(msg, {
            icon: 16,
            shade: [0.1, '#666666'],
            time: false  //不自动关闭
        })
    }

    //主体查询
    function ztcx(obj) {
        var ztqqpk = obj.ztqqpk;
        var qqlx = obj.qqlx;
        var cardnumber = obj.cardnumber;
        console.info(qqlx);
        if (!ztqqpk){
            return;
        }
        $("#myModalLabel_public").text("主体信息");
        /*$("#public_dialog").css({"width":"800px"});*/
        var zcType = "";
        if(qqlx && qqlx == '1'){
            zcType="yhkZt";
        }
        if(qqlx && qqlx == '2'){
            zcType="sfZt";
        }
        $("#publicShowIframe").attr("src",ctx+"/znzf/base/show?zcType="+zcType+"&pk="+ztqqpk+"&zh="+cardnumber);
        $("#myModal_public").modal();
    }

    //主题请求重发
    function ztqqcf(obj) {
        var ztqqpk = obj.ztqqpk;
        var qqlx = obj.qqlx;
        if (!ztqqpk){
            return;
        }
        $.ajax({
            type: "get",
            dataType: "json",
            data:{ztpk:ztqqpk,sertype:qqlx},
            url: ctx+"/yayk/ztQqSend" ,
            success: function (data) {
                if (data.success){
                    top.layer.alert("重发成功："+data.msg, {icon: 1, title: '提示'});
                }else{
                    if(data.code =='0' || data.code =='1'){
                        top.layer.alert("重发失败："+data.msg, {icon: 2, title: '提示'});
                    }else{
                        top.layer.alert("重发失败：请求异常", {icon: 2, title: '提示'});
                    }
                }
            },
            error : function() {
                top.layer.alert("重发失败：请求异常", {icon: 2, title: '提示'});
            }
        });
    }

    /*window.onload = function (ev) {
        $(".modal").draggable();
    }*/
</script>
<script type="text/javascript" src="${ctx}/resources/layer/layer.js" charset="utf-8"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${ctx}/resources/jquery-ui/jquery-ui.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-closeable-tab.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
</body>
</html>