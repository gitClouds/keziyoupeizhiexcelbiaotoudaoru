<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
    <link rel="stylesheet" href="${ctx}/css/pub/showBase.css">
</head>
<body>
<input type="hidden" name="jjdpk" id="jjdpk" value="${jjdpk}" />
<!-- 主体内容 -->
<section class="content">
    <%--<jsp:include page="xq_jjd.jsp"/>
    <div style="height: 5px"></div>--%>
    <jsp:include page="xq_zfcxdj.jsp"/>
    <div style="height: 5px"></div>
    <jsp:include page="xq_zc.jsp"/>
    <div style="height: 5px"></div>
    <%--<jsp:include page="xq_cb.jsp"/>
    <div style="height: 5px"></div>--%>
    <jsp:include page="zcygzs.jsp"/>

    <!-- 详情模态框（Modal） -->
    <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="showModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="showMessage">
                    <%--<jsp:include page="${ctx}/znzf/base/show"/>--%>
                    <iframe src="" id="zcShowIframe" width="100%" scrolling="no" frameborder="0"></iframe>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 提示模态框（Modal） -->
    <div class="modal fade" id="tipsModal" tabindex="-1" role="dialog" aria-labelledby="tipsModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="tipsModalLabel">提示</h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="tipsMessage"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" id="tipsConfirmBtn">确定</button>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    var ctx = "${ctx}";
    var zfPerm=false;
    <shiro:hasPermission name="ROLE_ZF">
        zfPerm = true;
    </shiro:hasPermission>
</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-closeable-tab.js"></script>
<script src="${ctx}/resources/common/fileUpload.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/znzf/znzf.js"></script>
<script src="${ctx}/js/yayk/yayk.js"></script>
<script src="${ctx}/js/fzzx/jjd.js"></script>
<script src="${ctx}/js/yayk/zcygzs.js"></script>
<script src="${ctx}/js/znzf/cbGab.js"></script>
<script src="${ctx}/js/znzf/cbGl.js"></script>
<script src="${ctx}/js/znzf/cbMacGl.js"></script>
<script src="${ctx}/js/znzf/cbMacGab.js"></script>
<script src="${ctx}/js/fzzx/ypbg.js"></script>
<script>
    $(function () {
        $(".banner_tab li").on("click",function () {
            if(!$(this).hasClass('non_tab')){
                $(this).addClass("tab_item_get").siblings().removeClass("tab_item_get");
            }
        });

        jjdShow($("#jjdpk").val());
        showFocus();
        switchZfCxDjTab('Y-07,S-07,Y-08,S-08,S-09,S-19');
        searchLevel('0',$("#jjdpk").val(),'0');
        switchZcygzsTab('ypsl','','${jjdpk}');
        initCbGabGrid();
        initCbGlGrid();
        initCbMacGlGrid();
        initCbMacGabGrid();
        showSfCbMacGab();
        initJjdYpbgGrid();
    });

    <shiro:hasPermission name="ROLE_XTGL">
        $("#keepOnButton").removeClass("hide");
        $("#exportCbMacGabXq").removeClass("hide");
    </shiro:hasPermission>
</script>
</body>
</html>
