<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html>
<head>
    <title>串并页面</title>
</head>
<body>
<%--<input type="hidden" id="jjdpk" name="jjdpk" value="${jjdpk}"  />--%>
<div class="banner_box" >
    <div class="banner_title">
        <h4>串并信息</h4>
    </div>
    <div class="banner_table_box">
        <h5 style="color:#00c0ef;padding: 10px;">系统内串并</h5>
        <table id="xtncb" class="table table-bordered table-striped table-condensed"></table>
        <h5 style="color:#00c0ef;padding: 10px 0 0 10px;margin-bottom: 0px;margin-top: 20px">公安部电信诈骗案件侦办平台串并</h5>
        <table id="gabcb" class="table table-bordered table-striped table-condensed"></table>
        <h5 style="color:#00c0ef;padding: 10px 0 0 10px;margin-bottom: 0px;margin-top: 20px">系统内MAC串并</h5>
        <table id="cbMacGlGrid" class="table table-bordered table-striped table-condensed"></table>
        <h5 style="color:#00c0ef;padding: 10px 0 0 10px;margin-bottom: 0px;margin-top: 20px">公安部电信诈骗案件侦办平台MAC串并
            <i class="btn text-black hide" id="exportCbMacGabXq" onclick="exportCbMacGabXq();" style="float: right">
                <i class="fa fa-cloud-download fa-2x"></i>
            </i>
        </h5>
        <table id="cbMacGabGrid" class="table table-bordered table-striped table-condensed"></table>
        <form class="form_chag form-inline" name="cbMacGabQueryFormXq" id="cbMacGabQueryFormXq">
            <input type="hidden" name="jjdpk" id="jjdpkexcel" value="${jjdpk}" />
        </form>
        <%--<div class="btn-group hide" id="toolbar">
            <i class="btn" onclick="exportCbGab();" title="导出">
                <i class="fa fa-cloud-download fa-2x"></i>
            </i>
        </div>--%>
    </div>
    <%--<iframe src="${ctx}/znzf/cbGab/page?jjdpk=${jjdpk}" id="xtncbIframe" width="100%" scrolling="no" frameborder="0"></iframe>--%>
    <%--<iframe src="${ctx}/znzf/cbGab/page?jjdpk=${jjdpk}" id="gabcbIframe" width="100%" scrolling="no" frameborder="0"></iframe>--%>
</div>
</body>
</html>
