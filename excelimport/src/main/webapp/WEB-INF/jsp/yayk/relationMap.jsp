<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/jquery-ui/jquery-ui.min.css">
    <script type="text/javascript" src="${ctx}/resources/group/js/plugins/d3/d3.v3.min.js"></script>
    <script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
    <link rel="stylesheet" href="${ctx}/resources/group/css/demo.css"/>
    <script>
        //iframe真正解决高度自适应
        function changeFrameHeight(iframeId){
            var ifm= document.getElementById(iframeId);
            ifm.height=document.documentElement.clientHeight;

        }

        window.onresize=function(){
            changeFrameHeight();

        }
    </script>
</head>
<body style="margin: 0px;">

<div>
    <div class="row">
        <div class="col-xs-12">
            <ul class="box clearfix">
                <form style="margin-top: 10px" class="form_chag form-inline" id="mxQueryForm" name="mxQueryForm">
                    <div class="form-group" style="width: 100%;margin-bottom: 10px;">
                        <input type="hidden" name="pk" value="${pk}">
                        <label for="money">交易金额</label>
                        <input type="text" style="width: 15%!important" value="" class="required form-control" id="money" name="money"
                               data-valid="isNonEmpty||onlyInt" data-error="不能为空||只能输入数字">
                        <b>元的倍数</b>
                        <label for="select_nlevel">出入账类型</label>
                        <select class="form-control" id="select_nlevel">
                            <%--<option selected="selected" value="">请选择</option>--%>
                            <option selected="selected" value="0">出账</option>
                            <option value="1">入账</option>
                        </select>&nbsp;
                        <label for="begin_time">交易时间</label>
                        <input type="text" class="Wdate form-control" id="begin_time" name="begin_time" value="${begin_time}" onFocus="WdatePicker({lang:'zh-cn',readOnly:true,dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'end_time\')||\'%y-%M-{%d}\'}'})" style="width: 15%!important" >
                        <b>至</b>
                        <input type="text" class="Wdate form-control" id="end_time" name="end_time" value="${end_time}" onFocus="WdatePicker({lang:'zh-cn',readOnly:true,dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'%y-%M-{%d}',minDate:'#F{$dp.$D(\'begin_time\')}'})" style="width: 15%!important" >
                        <div style="width: 8%;float: right;">
                            <button type="button" class="btn btn-info" onclick="sx();">查询</button>
                        </div>
                    </div>
                    <%--  <div class="form-group" style="width: 60%">
                          <label for="times">交易时间段</label>
                          <input type="text" style="width: 15%!important" value="0" class="required form-control" id="jysjb" name="jysjb" data-valid="isNonEmpty||onlyInt||between:1-2||maxIntVal:24" data-error="不能为空||只能输入数字||长度为1-2||不能大于24">
                          -
                          <input type="text" style="width: 15%!important" value="9" class="required form-control" id="jysje" name="jysje" data-valid="isNonEmpty||onlyInt||between:1-2||maxIntVal:24" data-error="不能为空||只能输入数字||长度为1-2||不能大于24">
                          <b>点</b>

                      </div>--%>
                </form>
            </ul>
        </div>
    </div>
</div>
<jsp:include page="../loading.jsp"/>
<!-- 模态框（Modal） -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" id="mx_dialog" style="width:90%;height: auto;">
        <div class="modal-content" style="border-radius:5px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    明细信息
                </h4>
            </div>
            <div class="modal-body">
                <%--<iframe src="${ctx}/znzf/jlt/echarts_page" id="echartsShow" width="100%" scrolling="no" frameborder="0"></iframe>--%>
                <%--<jsp:include page="echarts.jsp"/>--%>
                <iframe src="" id="zcShowIframe" width="100%"  scrolling="no" frameborder="0"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>

<!-- 公用模态框（Modal） -->
<div class="modal fade" id="myModal_public" tabindex="-1" role="dialog" aria-labelledby="myModalLabel_public" aria-hidden="true">
    <div class="modal-dialog" id="public_dialog" style="width:90%;">
        <div class="modal-content" style="border-radius:5px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel_public">
                    明细信息
                </h4>
            </div>
            <div class="modal-body">
                <%--<iframe src="${ctx}/znzf/jlt/echarts_page" id="echartsShow" width="100%" scrolling="no" frameborder="0"></iframe>--%>
                <%--<jsp:include page="echarts.jsp"/>--%>
                <iframe src="" id="publicShowIframe" width="100%" height="100%" onload="changeFrameHeight('publicShowIframe')" frameborder="0"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>
<!-- 力导向图div -->
<div id="divid"></div>
<script type="text/javascript">
    var ctx = '${ctx}';
    var pk = "${pk}";
    var url = "${ctx}/znzf/jlt/relation_map_data?pk=" + pk;

    function sx() {
        debugger;
        var money = $("#money").val();
        var begin_time = $("#begin_time").val();
        var end_time = $("#end_time").val();
        var nlevel = $('#select_nlevel').val();
        if ((begin_time != "" && end_time == "")
            || (end_time != "" && begin_time == "")) {
            alert("请选择时间范围!");
            return;
        }
        var page_url = "${ctx}/znzf/jlt/relation_map_page?pk=" + pk + "&money=" + money + "&begin_time=" + begin_time + "&end_time=" + end_time + "&nlevel=" +nlevel;
        window.location.href=page_url;

        /*d3.select("svg").remove();*/

    }
    <%--var data_url = "${ctx}/znzf/jlt/relation_map_data?pk=${pk}&money=${money}&begin_time=${begin_time}&end_time=${end_time}&nlevel=${nlevel}";--%>
    /*d3.json(data_url, function (json) {
        function GroupExplorer(wrapper, config) {
            var defaultConfig = {
                data: {"nodes": [], "links": []},
                width: window.innerWidth,
                height: window.innerHeight - 17,
                distance: 100
            };
            $.extend(true, defaultConfig, config);
            defaultConfig.data.links.forEach(function (e) {
                if (typeof e.source != "number" && typeof e.target != "number") {
                    var sourceNode = defaultConfig.data.nodes.filter(function (n) {
                            return n.name === e.source;
                        })[0],
                        targetNode = defaultConfig.data.nodes.filter(function (n) {
                            return n.name === e.target;
                        })[0];
                    e.source = sourceNode;
                    e.target = targetNode;
                }
            });
            var _this = this, highlighted = null, dependsNode = [], dependsLinkAndText = [];
            this.color = d3.scale.category20();
            var zoom = d3.behavior.zoom()
                .scaleExtent([0.2, 10])
                .on("zoom", function () {
                    _this.zoomed();
                });

            this.vis = d3.select("body").append("svg:svg")
                .attr("width", defaultConfig.width)
                .attr("height", defaultConfig.height)
                .call(zoom).on("dblclick.zoom", null);

            this.vis = this.vis.append('g').attr('class', 'all')
                .attr("width", defaultConfig.width)
                .attr("height", defaultConfig.height)


            this.force = d3.layout.force()
                .nodes(defaultConfig.data.nodes)
                .links(defaultConfig.data.links)
                .gravity(.0001)
                .distance(defaultConfig.distance)
                .charge(function (d) {
                    return (-10 * d.index)
                })
                .size([defaultConfig.width, defaultConfig.height])
                .start();
            this.vis.append("svg:defs").selectAll("marker")
                .data(["end"])
                .enter().append("svg:marker")
                .attr("id", "arrow")
                .attr('class', 'arrow')
                .attr("viewBox", "0 -5 10 10")
                .attr("refX", 27)
                .attr("refY", 0)
                .attr("markerWidth", 9)
                .attr("markerHeight", 16)
                .attr("markerUnits", "userSpaceOnUse")
                .attr("orient", "auto")
                .append("svg:path")
                .attr("d", "M0,-5L10,0L0,5")
                .attr('fill', '#666');

            this.link = this.vis.selectAll("line.link")
                .data(defaultConfig.data.links)
                .enter().append("svg:line")
                .attr("class", "link")
                .attr('stroke-width', 1)
                .attr("x1", function (d) {
                    return d.source.x;
                })
                .attr("y1", function (d) {
                    return d.source.y;
                })
                .attr("x2", function (d) {
                    return d.target.x;
                })
                .attr("y2", function (d) {
                    return d.target.y;
                })
                .attr("marker-end", "url(#arrow)")
                .attr('stroke', '#999');

            var dragstart = function (d, i) {
                _this.force.stop();
                d3.event.sourceEvent.stopPropagation();
            };

            var dragmove = function (d, i) {
                d.px += d3.event.dx;
                d.py += d3.event.dy;
                d.x += d3.event.dx;
                d.y += d3.event.dy;
                _this.tick();
            };

            var dragend = function (d, i) {
                d.fixed = true;
                _this.tick();
                _this.force.resume();
            };

            this.nodeDrag = d3.behavior.drag()
                .on("dragstart", dragstart)
                .on("drag", dragmove)
                .on("dragend", dragend);


            this.highlightObject = function (obj) {
                if (obj) {
                    var objIndex = obj.index;
                    dependsNode = dependsNode.concat([objIndex]);
                    dependsLinkAndText = dependsLinkAndText.concat([objIndex]);
                    defaultConfig.data.links.forEach(function (lkItem) {
                        if (objIndex == lkItem['source']['index']) {
                            dependsNode = dependsNode.concat([lkItem.target.index])
                        } else if (objIndex == lkItem['target']['index']) {
                            dependsNode = dependsNode.concat([lkItem.source.index])
                        }
                    });
                    _this.node.classed('inactive', function (d) {
                        return (dependsNode.indexOf(d.index) == -1)
                    });
                    _this.link.classed('inactive', function (d) {

                        return ((dependsLinkAndText.indexOf(d.source.index) == -1) && (dependsLinkAndText.indexOf(d.target.index) == -1))
                    });

                    _this.linetext.classed('inactive', function (d) {
                        return ((dependsLinkAndText.indexOf(d.source.index) == -1) && (dependsLinkAndText.indexOf(d.target.index) == -1))
                    });
                } else {
                    _this.node.classed('inactive', false);
                    _this.link.classed('inactive', false);
                    _this.linetext.classed('inactive', false);
                }
            };

            this.highlightToolTip = function (obj) {
                if (obj) {
                    _this.tooltip.html("<div class='title'>" + obj.name + "的资料</div><table class='detail-info'><tr><td class='td-label' >身份证：</td><td>" + obj.cardid + "</td></tr>" +
                        "<tr><td class='td-label' >账号：</td><td >" + obj.cardnumber + "</td></tr>" +
                        "<tr><td class='td-label' >电话：</td><td >" + obj.phone + "</td></tr>"+
                        "<tr><td class='td-label' >主体:</td><td style='padding: 5px;'><a style='margin: 5px;' href='javascript:void(0);' onclick='ztcx("+JSON.stringify(obj)+")' id='ztcx' class='btn-sm btn-info'  title='主体查询'>主体查询</a></td></tr>" +
                        "<tr><td class='td-label' >禁毒人员查询:</td><td style='padding: 5px;'><a style='margin: 5px;' href='javascript:void(0);' onclick='xdsdrycx("+JSON.stringify(obj)+")' id='xdsdrycx' class='btn-sm btn-info'  title='禁毒人员查询'>禁毒人员查询</a></td></tr>" +
                        "<tr><td class='td-label' >主体请求重发:</td><td style='padding: 5px;'><a style='margin: 5px;' href='javascript:void(0);' onclick='ztqqcf("+JSON.stringify(obj)+")' id='ztqqcf' class='btn-sm btn-info'  title='主体请求重发'>主体请求重发</a></td></tr>" +
                        "</table>")
                        .style("left", (d3.event.pageX + 20) + "px")
                        .style("top", (d3.event.pageY - 20) + "px")
                        .style("opacity", 1.0);
                    _this.tooltip.html();
                } else {
                    _this.tooltip.style("opacity", 0.0);
                }
            };

            this.tooltip = d3.select("body").append("div")
                .attr("class", "tooltip")
                .attr("opacity", 0.0)
                .on('dblclick', function () {
                    d3.event.stopPropagation();
                })
                .on('mouseover', function () {
                    if (_this.node.mouseoutTimeout) {
                        clearTimeout(_this.node.mouseoutTimeout);
                        _this.node.mouseoutTimeout = null;
                    }
                })
                .on('mouseout', function () {
                    if (_this.node.mouseoutTimeout) {
                        clearTimeout(_this.node.mouseoutTimeout);
                        _this.node.mouseoutTimeout = null;
                    }
                    _this.node.mouseoutTimeout = setTimeout(function () {
                        _this.highlightToolTip(null);
                    }, 300);
                });

            this.node = this.vis.selectAll("g.node")
                .data(defaultConfig.data.nodes)
                .enter().append("svg:g")
                .attr("class", "node")
                .call(_this.nodeDrag)
                .on('mouseover', function (d) {
                    if (_this.node.mouseoutTimeout) {
                        clearTimeout(_this.node.mouseoutTimeout);
                        _this.node.mouseoutTimeout = null;
                    }
                    _this.highlightToolTip(d);
                })
                .on('mouseout', function () {
                    if (_this.node.mouseoutTimeout) {
                        clearTimeout(_this.node.mouseoutTimeout);
                        _this.node.mouseoutTimeout = null;
                    }
                    _this.node.mouseoutTimeout = setTimeout(function () {
                        _this.highlightToolTip(null);
                    }, 300);
                })
                .on('dblclick', function (d) {
                    _this.highlightObject(d);
                    d3.event.stopPropagation();
                });
            d3.select("body").on('dblclick', function () {
                dependsNode = dependsLinkAndText = [];
                _this.highlightObject(null);
            });


            this.node.append("svg:image")
                .attr("class", "circle")
                .attr("xlink:href", function (d) {
                    if (d.isqk == '1') {
                        return "<%--${ctx}--%>/resources/img/qkperson.png";
                    } else {
                        return "<%--${ctx}--%>/resources/img/person.png";
                    }
                })
                .attr("x", "-15px")
                .attr("y", "-15px")
                .attr("width", "30px")
                .attr("height", "30px");
            this.node.append("svg:text")
                .text(function (d) {
                    if (d.rylx == '1') {
                        return '吸';
                    } else if (d.rylx == '2') {
                        return '涉';
                    } else if (d.rylx == '3') {
                        return '吸涉';
                    }
                })
                .attr("x", function (d) {
                    if (d.rylx == '1') {
                        return '-20px';
                    } else if (d.rylx == '2') {
                        return '5px';
                    } else {
                        return '-15px'
                    }
                })
                .attr("y", "5px")
                .attr('fill', function (d) {
                    if (d.rylx == '1') {
                        return '#ffa649';
                    } else if (d.rylx == '2') {
                        return '#ff213f';
                    } else {
                        return "rgb(241,39,255)";
                    }
                })
                .attr('style', 'cursor:default');

            this.node.append("svg:text")
                .attr("class", "nodetext")
                .attr("dy", "30px")
                .attr('text-anchor', 'middle')
                .text(function (d) {
                    if (d.isqk == '2') {
                        $(this).parent('.node').hide();
                    }
                    return d.name;
                })
                .attr('fill', function (d, i) {
                    return _this.color(i);
                });

            this.linetext = this.vis.selectAll('.linetext')
                .data(defaultConfig.data.links)
                .enter()
                .append("text")
                .attr("class", "linetext")
                .attr("x", function (d) {
                    return (d.source.x + d.target.x) / 2
                })
                .attr("y", function (d) {
                    return (d.source.y + d.target.y) / 2
                })
                .text(function (d) {
                    return d.relation
                })
                .attr('fill', function (d, i) {
                    return _this.color(i);
                })
                /!*.attr('data-toggle','modal')
                .attr('data-target','#myModal')*!/
                .attr('onclick', function (d) {
                    return 'showModel(' + JSON.stringify(d) + ')';
                })
                .attr('style', 'cursor:pointer')
                .call(this.force.drag);

            this.zoomed = function () {
                _this.vis.attr("transform", "translate(" + d3.event.translate + ") scale(" + d3.event.scale + ")")
            };


            var findMaxWeightNode = function () {
                var baseWeight = 1, baseNode;
                defaultConfig.data.nodes.forEach(function (item) {
                    if (item.weight > baseWeight) {
                        baseWeight = item.weight
                        baseNode = item
                    }
                });
                return baseNode;
            };

            this.tick = function () {
                var findMaxWeightNodeIndex = findMaxWeightNode().index;
                defaultConfig.data.nodes[findMaxWeightNodeIndex].x = defaultConfig.width / 2;
                defaultConfig.data.nodes[findMaxWeightNodeIndex].y = defaultConfig.height / 2;
                _this.link.attr("x1", function (d) {
                    return d.source.x;
                })
                    .attr("y1", function (d) {
                        return d.source.y;
                    })
                    .attr("x2", function (d) {
                        return d.target.x
                    })
                    .attr("y2", function (d) {
                        return d.target.y;
                    });
                _this.linetext.attr("x", function (d) {
                    return (d.source.x + d.target.x) / 2
                })
                    .attr("y", function (d) {
                        return (d.source.y + d.target.y) / 2
                    });
                _this.node.attr("transform", function (d) {
                    return "translate(" + d.x + "," + d.y + ")";
                });
            };
            _this.force.on("tick", this.tick);

        }

        new GroupExplorer('body', {
            data: json
        });
    });*/

    $(document).ready(function () {
        showChart();
    })
    function showChart() {
        var data = ${result};
        if (data){
            var options = {};
            options.backgroundColor = "rgba(44,202,255,0)";
            options.nodesFontType = "SimSun";
            options.nodesFontSize = 20;
            options.lineFontType = "SimHei";
            options.lineFontSize = 12;
            options.lineColor = "#000000";
            options.showExamples = true;
            options.examplesX = 20;
            options.examplesY = 450;
            options.examplesFontColor = "#000000";
            drawChart("divid", options, data);
        }
    }


    function drawChart(divid, options, datas, dataFilter) {
        var backgroundColor = options.backgroundColor; //背景颜色
        var nodesFontType = options.nodesFontType; //节点字体
        var nodesFontSize = options.nodesFontSize; //节点字号
        var lineFontType = options.lineFontType; //关系字体
        var lineFontSize = options.lineFontSize; //关系字号
        var lineColor = options.lineColor; //连线颜色
        var examplesFontColor = options.examplesFontColor; //关系示例字体颜色
        var pageWidth = window.innerWidth;
        var pageHeight = window.innerHeight;
        var width = pageWidth; //画布宽
        var height = pageHeight; //画布高
        var svgChart = d3.select("svg");
        svgChart.remove();

        var tip = $(".tooltip");
        if (tip.length > 0) {
            tip.remove();
        }


        var sourceDatas = {};
        sourceDatas.links = [];
        for (var i = 0; i < datas.links.length; i++) {
            var jsonObj = {};
            jsonObj.source = datas.links[i].source;
            jsonObj.target = datas.links[i].target;
            jsonObj.relation = datas.links[i].relation;
            jsonObj.sourceId = datas.links[i].sourceId;
            jsonObj.targetId = datas.links[i].targetId;
            jsonObj.sourceImg = datas.links[i].sourceImg;
            jsonObj.targetImg = datas.links[i].targetImg;
            jsonObj.sourceColor = datas.links[i].sourceColor;
            jsonObj.targetColor = datas.links[i].targetColor;
            jsonObj.sourceRadius = datas.links[i].sourceRadius;
            jsonObj.targetRadius = datas.links[i].targetRadius;
            jsonObj.lineColor = datas.links[i].lineColor;
            sourceDatas.links.push(jsonObj);
        }
        var resourceLinks = sourceDatas.links;


        if (dataFilter != undefined && dataFilter.length > 0) {
            var indexArray = [];
            for (var i = 0; i < dataFilter.length; i++) {
                for (var j = 0; j < resourceLinks.length; j++) {
                    if (resourceLinks[j].relation == dataFilter[i].relation && dataFilter[i].isShow == "false") {
                        indexArray.push(j);
                    }
                }
            }
            if (indexArray.length > 0) {
                var tempArray = [];
                for (var j = 0; j < resourceLinks.length; j++) {
                    for (var i = 0; i < indexArray.length; i++) {
                        if (indexArray[i] != j) {
                            if (i == indexArray.length - 1) {
                                tempArray.push(resourceLinks[j]);
                                break;
                            }
                            continue;
                        } else {
                            break;
                        }

                    }
                }
                resourceLinks = tempArray;
            }
        }
        var links = resourceLinks;

        //关系分组
        var linkGroup = {};
        //对连接线进行统计和分组，不区分连接线的方向，只要属于同两个实体，即认为是同一组
        var linkmap = {};
        for (var i = 0; i < links.length; i++) {
            var key = links[i].source < links[i].target ? links[i].source + ':' + links[i].target : links[i].target + ':' + links[i].source;
            if (!linkmap.hasOwnProperty(key)) {
                linkmap[key] = 0;
            }
            linkmap[key] += 1;
            if (!linkGroup.hasOwnProperty(key)) {
                linkGroup[key] = [];
            }
            linkGroup[key].push(links[i]);
        }
        //为每一条连接线分配size属性，同时对每一组连接线进行编号
        for (var i = 0; i < links.length; i++) {
            var key = links[i].source < links[i].target ? links[i].source + ':' + links[i].target : links[i].target + ':' + links[i].source;
            links[i].size = linkmap[key];
            //同一组的关系进行编号
            var group = linkGroup[key];
            //给节点分配编号
            setLinkNumber(group);
        }

        //节点
        var nodes = {};
        //关系对应颜色
        var relationColor = {};

        for (var i = 0; i < links.length; i++) {
            links[i].source = nodes[links[i].source] || (nodes[links[i].source] = { name: links[i].source, color: links[i].sourceColor, image: links[i].sourceImg, radius: links[i].sourceRadius });
            links[i].target = nodes[links[i].target] || (nodes[links[i].target] = { name: links[i].target, color: links[i].targetColor, image: links[i].targetImg, radius: links[i].targetRadius });
        }

        var sourceData = datas.links;
        for (var i = 0; i < sourceData.length; i++) {
            relationColor[sourceData[i].relation] = { "relation": sourceData[i].relation, "lineColor": sourceData[i].lineColor };
        }


        nodes = d3.values(nodes);
        relationColor = d3.values(relationColor);

        var examples_x = parseFloat(options.examplesX); //关系示例坐标x
        var examples_y = parseFloat(options.examplesY); //关系示例坐标y
        var examplesLength = 80;
        var examplesSize = Math.floor((width - examples_x) / examplesLength);
        var examplesRow = relationColor.length % examplesSize == 0 ? relationColor.length / examplesSize : Math.ceil(relationColor.length / examplesSize);
        //计算关系示列位置
        for (var i = 1; i <= relationColor.length; i++) {
            var num = i % examplesSize == 0 ? examplesSize : i % examplesSize;
            relationColor[i - 1].x = examples_x + (num - 1) * examplesLength;
            relationColor[i - 1].y = examples_y + 20 * Math.ceil(i / examplesSize);
        }
        if (dataFilter == undefined) {
            dataFilter = [];
            for (var i = 0; i < relationColor.length; i++) {
                dataFilter.push({ "relation": relationColor[i].relation, "isShow": "true" });
            }
        }


        //绑定相连节点
        for (var i = 0; i < nodes.length; i++) {
            for (var j = 0; j < links.length; j++) {
                if (nodes[i].name == links[j].source.name) {
                    nodes[i][links[j].target.name] = { name: links[j].target.name };
                }
                if (nodes[i].name == links[j].target.name) {
                    nodes[i][links[j].source.name] = { name: links[j].source.name };
                }
            }
        }

        var zoom = d3.behavior.zoom()
            .scaleExtent([0.8,10])
            .on("zoom",function(){
                zoomed();
            });

        var svg = d3.select("#" + divid).append("svg")
            .attr("width", width)
            .attr("height", height)
            .attr("style", "background-color:" + backgroundColor)
            .call(zoom).on("dblclick.zoom", null);
        /*var gs = svg.select("g").call(zoom).on("dblclick.zoom", null);*/

        var zoomed = function (){
            /*gs.attr("transform","translate("+d3.event.translate+")scale("+d3.event.scale+")");*/
            svg.attr("transform","scale("+d3.event.scale+")");
        } //d3.event.translate 是平移的坐标值，d3.event.scale 是缩放的值。

        if (options.showExamples == "true") {
            var examples = svg.selectAll(".examples")
                .data(relationColor)
                .enter()
                .append("svg:g")
                .attr("fill-opacity", function (d) {
                    for (var i = 0; i < dataFilter.length; i++) {
                        if (d.relation == dataFilter[i].relation && dataFilter[i].isShow == "false") {
                            return 0.2;
                        }
                    }
                    return 1;
                })
                .on("click", function (d) {
                    for (var i = 0; i < dataFilter.length; i++) {
                        if (dataFilter[i].relation == d.relation) {
                            if (dataFilter[i].isShow == "true") {
                                dataFilter[i].isShow = "false";
                            } else {
                                dataFilter[i].isShow = "true";
                            }
                        }
                    }
                    drawChart(divid, options, datas, dataFilter);
                });


            examples.append("svg:path")
                .attr("d", function (d) {
                    var x1 = d.x;
                    var y1 = d.y;
                    var x2 = x1 + 20;
                    var y2 = y1;
                    return 'M' + x1 + ' ' + y1 + ' L ' + x2 + ' ' + y2;
                })
                .style("stroke", function (d) {
                    if (d.lineColor == "") {
                        return lineColor;
                    } else {
                        return d.lineColor;
                    }
                })
                .style("stroke-width", 2.5);
            examples.append("svg:text")
                .style("font-size", "10px")
                .style("fill", examplesFontColor)
                .attr("x", function (d) {
                    if (d.relation.length > 3) {
                        return d.x + 20 + 14 * 4 / 2;
                    }
                    return d.x + 20 + 14 * d.relation.length / 2;
                })
                .attr("y", function (d) {
                    return d.y + 5;
                })
                .attr('text-anchor', "middle")
                .text(function (d) {
                    if (d.relation.length > 3) {
                        return d.relation.substring(0, 3) + "...";
                    }
                    return d.relation;
                })
                .on("mouseover", function (d) {
                    tooltip.html("<span>" + d.relation + "</span>")
                        .style("left", (d3.event.pageX) + "px")
                        .style("top", (d3.event.pageY + 20) + "px")
                        .style("display", "block")
                        .style("position", "absolute")
                        .style("opacity", 1.0);
                })
                .on("mouseout", function (d, i) {
                    tooltip.style("opacity", 0.0);
                });
        }



        //D3力导向布局
        var force = d3.layout.force()
            .nodes(nodes)
            .links(links)
            .size([width, height])
            .linkDistance(200)
            .charge(-1500)
            .start();


        //边
        var edges_path = svg.selectAll(".edgepath")
            .data(links)
            .enter()
            .append("path")
            .attr("marker-end", function (d, i) {
                var arrowMarker = svg.append("marker")
                    .attr("id", "arrow" + i)
                    .attr("markerUnits", "userSpaceOnUse")
                    .attr("markerWidth", "16")
                    .attr("markerHeight", "15")
                    .attr("viewBox", "0 0 12 12")
                    .attr("refX", 9)
                    .attr("refY", 6)
                    .attr("orient", "auto")
                    .append("svg:path")
                    .attr("d", "M2,2 L10,6 L2,10 L6,6 L2,2")
                    .attr("fill", function () {
                        return d.lineColor = "" ? lineColor : d.lineColor;
                    });

                return "url(#arrow" + i + ")";
            })
            .style("stroke", function (d) {
                if (d.lineColor == "") {
                    return lineColor;
                } else {
                    return d.lineColor;
                }
            })
            .style("stroke-width", 1.5)
            .on("mouseover", function (d) {
                //影藏其它连线上文字
                edges_text.style("fill-opacity", function (edge) {
                    if (edge === d) {
                        return 1;
                    }
                    return 0;
                })
                edges_path.style("stroke-width", function (edge) {
                    if (edge === d) {
                        return 4;
                    }
                    return 1.5;
                })
            })
            .on("mouseout", function (d, i) {
                //显示连线上的文字
                edges_text.style("fill-opacity", 1);
                edges_path.style("stroke-width", 1.5);
            });


        //边上的文字（人物之间的关系）
        var edges_text = svg.selectAll(".linetext")
            .data(links)
            .enter()
            .append("svg:g")
            .attr("class", "linetext")
            .attr("fill-opacity", 1);
        edges_text.append("svg:text")
            .style("font-size", (lineFontSize + "px"))
            .style("font-family", lineFontType)
            .style("fill", "#000000")
            .attr("y", ".31em")
            .attr('text-anchor', "middle")
            .text(function (d) {
                return d.relation;
            });

        edges_text.insert('rect', 'text')
            .attr('width', function (d) {
                return d.relation.length * lineFontSize;
            })
            .attr('height', function (d) {
                return lineFontSize;
            })
            .attr("y", "-.6em")
            .attr('x', function (d) {
                return -d.relation.length * lineFontSize / 2;
            })
            .style('fill', '#fff');

        // 圆形图片节点（人物头像）
        var circle = svg.selectAll("circle")
            .data(nodes)
            .enter()
            .append("circle")
            .style("stroke", function (d) {
                if (d.color == "") {
                    return "#EE8262";
                }
                return d.color;
            })
            .style("stroke-width", "2px")
            .attr("r", function (d) {
                return d.radius;
            })
            .attr("fill", function (d, i) {
                //节点图片不为空是添加背景色
                if (d.image == "") {
                    if (d.color == "") {
                        return "#EE8262";
                    }
                    return d.color;
                } else {
                    //创建圆形图片
                    var defs = svg.append("defs").attr("id", "imgdefs")

                    var catpattern = defs.append("pattern")
                        .attr("id", "catpattern" + i)
                        .attr("height", 1)
                        .attr("width", 1)

                    catpattern.append("image")
                    /* .attr("x", - (img_w / 2 - radius))
                    .attr("y", - (img_h / 2 - radius)) */
                        .attr("width", d.radius * 2)
                        .attr("height", d.radius * 2)
                        .attr("xlink:href", d.image)

                    return "url(#catpattern" + i + ")";
                }

            }).attr("data-container", "body")
            .attr("data-toggle", "popover")
            .attr("data-placement", "right")
            .attr("data-html", "true")
            .attr("title", "交易详情")
            .attr("data-content", function (d, i) {
                return "<table><tr><td>主体账号:</td><td>"+datas.links[i].BANK_CARD_NUM+"</td></tr><tr><td>交易金额:</td><td>"+datas.links[i].JYJE+"</td></tr><tr><td>交易余额:</td><td>"+datas.links[i].YE+"</td></tr></table>"
            })
            .on("mouseover", function (d, i) {
                //影藏其它连线上文字
                edges_text.style("fill-opacity", function (edge) {
                    if (edge.source === d || edge.target === d) {
                        return 1;
                    }
                    if (edge.source !== d && edge.target !== d) {
                        return 0;
                    }
                })
                //其它节点亮度调低
                circle.style("opacity", function (edge) {
                    var v = d.name;
                    if (edge.name == v || (edge[v] != undefined && edge[v].name == v)) {
                        return 1;
                    } else {
                        return 0.2;
                    }
                })
                //其他连先亮度调低
                edges_path.style("opacity", function (edge) {
                    if (edge.source === d || edge.target === d) {
                        return 1;
                    }
                    if (edge.source !== d && edge.target !== d) {
                        return 0.2;
                    }
                })
                //其他节点文字亮度调低
                nodes_text.style("opacity", function (edge) {
                    var v = d.name;
                    if (edge.name == v || (edge[v] != undefined && edge[v].name == v)) {
                        return 1;
                    } else {
                        return 0.2;
                    }
                })

            })
            .on("mouseout", function (d, i) {
                //显示连线上的文字
                edges_text.style("fill-opacity", 1);
                edges_path.style("opacity", 1);
                circle.style("opacity", 1);
                nodes_text.style("opacity", 1);
                tooltip.style("opacity", 0.0);

            })
            .call(force.drag);

        var tooltip = d3.select("body").append("div")
            .attr("class", "tooltip")
            .attr("opacity", 0.0);


        var nodes_text = svg.selectAll(".nodetext")
            .data(nodes)
            .enter()
            .append("text")
            .style("font-size", (nodesFontSize + "px"))
            .style("font-family", nodesFontType).text(function (d) {
                if (d.name) {
                    return d.name;
                }else {
                    "";
                }
            })
            /*.attr('x', function (d) {
                var name = d.name;
                //如果小于四个字符，不换行
                if (name.length < 4) {
                    d3.select(this).append('tspan')
                        .attr("dx", -nodesFontSize * (name.length / 2))
                        .text(function () { return name; });
                } else if (name.length >= 4 && name.length <= 6) {
                    var top = d.name.substring(0, 3);
                    var bot = d.name.substring(3, name.length);

                    d3.select(this).append('tspan')
                        .attr("dx", -nodesFontSize * 1.5)
                        .attr("dy", -nodesFontSize * 0.5)
                        .text(function () { return top; });

                    d3.select(this).append('tspan')
                        .attr("dx", -(nodesFontSize * name.length / 2))
                        .attr("dy", nodesFontSize)
                        .text(function () { return bot; });
                } else if (name.length > 7) {
                    var top = d.name.substring(0, 3);
                    var mid = d.name.substring(3, 6);
                    var bot = d.name.substring(6, name.length);

                    d3.select(this).append('tspan')
                        .attr("dx", -nodesFontSize * 1.5)
                        .attr("dy", -nodesFontSize * 0.5)
                        .text(function () { return top; });


                    d3.select(this).append('tspan')
                        .attr("dx", -nodesFontSize * 3)
                        .attr("dy", nodesFontSize)
                        .text(function () { return mid; });

                    d3.select(this).append('tspan')
                        .attr("dx", -nodesFontSize * 2)
                        .attr("dy", nodesFontSize)
                        .text(function () { return "..."; });
                }
            })*/
            .on("mouseover", function (d, i) {
                //影藏其它连线上文字
                edges_text.style("fill-opacity", function (edge) {
                    if (edge.source === d || edge.target === d) {
                        return 1;
                    }
                    if (edge.source !== d && edge.target !== d) {
                        return 0;
                    }
                })
                //其他节点亮度调低
                circle.style("opacity", function (edge) {
                    var v = d.name;
                    if (edge.name == v || (edge[v] != undefined && edge[v].name == v)) {
                        return 1;
                    } else {
                        return 0.2;
                    }
                })
                //其他连线亮度调低
                edges_path.style("opacity", function (edge) {
                    if (edge.source === d || edge.target === d) {
                        return 1;
                    }
                    if (edge.source !== d && edge.target !== d) {
                        return 0.2;
                    }
                })
                //其他节点文字亮度调低
                nodes_text.style("opacity", function (edge) {
                    var v = d.name;
                    if (edge.name == v || (edge[v] != undefined && edge[v].name == v)) {
                        return 1;
                    } else {
                        return 0.2;
                    }
                })
                tooltip.html("<span>" + d.name + "</span>")
                    .style("left", (d3.event.pageX) + "px")
                    .style("top", (d3.event.pageY + 20) + "px")
                    .style("display", "block")
                    .style("opacity", 1.0);
            })
            .on("mouseout", function (d, i) {
                //显示连线上的文字
                edges_text.style("fill-opacity", 1);
                edges_path.style("opacity", 1);
                circle.style("opacity", 1);
                nodes_text.style("opacity", 1);
                tooltip.style("opacity", 0.0);

            })
            .call(force.drag);

        var drag = force.drag()
            .on("dragstart", function (d, i) {
                d.fixed = true;    //拖拽开始后设定被拖拽对象为固定
            })
            .on("dragend", function (d, i) {
            })
            .on("drag", function (d, i) {
            });

        //力学图运动开始时
        force.on("start", function () {
        });

        //力学图运动结束时
        force.on("end", function () {
        });

        force.on("tick", function () {
            //限制结点的边界
            nodes.forEach(function (d, i) {
                d.x = d.x - 45 < 0 ? 45 : d.x;
                d.x = d.x + 45 > width ? width - 45 : d.x;
                d.y = d.y - 45 < 0 ? 45 : d.y;
                d.y = d.y + 45 > height ? height - 45 : d.y;
            });

            edges_path.attr("d", function (d) {
                var tan = Math.abs((d.target.y - d.source.y) / (d.target.x - d.source.x)); //圆心连线tan值
                var x1 = d.target.x - d.source.x > 0 ? Math.sqrt(d.sourceRadius * d.sourceRadius / (tan * tan + 1)) + d.source.x :
                    d.source.x - Math.sqrt(d.sourceRadius * d.sourceRadius / (tan * tan + 1)); //起点x坐标
                var y1 = d.target.y - d.source.y > 0 ? Math.sqrt(d.sourceRadius * d.sourceRadius * tan * tan / (tan * tan + 1)) + d.source.y :
                    d.source.y - Math.sqrt(d.sourceRadius * d.sourceRadius * tan * tan / (tan * tan + 1)); //起点y坐标
                var x2 = d.target.x - d.source.x > 0 ? d.target.x - Math.sqrt(d.targetRadius * d.targetRadius / (1 + tan * tan)) :
                    d.target.x + Math.sqrt(d.targetRadius * d.targetRadius / (1 + tan * tan));//终点x坐标
                var y2 = d.target.y - d.source.y > 0 ? d.target.y - Math.sqrt(d.targetRadius * d.targetRadius * tan * tan / (1 + tan * tan)) :
                    d.target.y + Math.sqrt(d.targetRadius * d.targetRadius * tan * tan / (1 + tan * tan));//终点y坐标
                if (d.target.x - d.source.x == 0 || tan == 0) { //斜率无穷大的情况或为0时
                    y1 = d.target.y - d.source.y > 0 ? d.source.y + d.sourceRadius : d.source.y - d.sourceRadius;
                    y2 = d.target.y - d.source.y > 0 ? d.target.y - d.targetRadius : d.target.y + d.targetRadius;
                }
                if (d.linknum == 0) {//设置编号为0的连接线为直线，其他连接线会均分在两边
                    d.x_start = x1;
                    d.y_start = y1;
                    d.x_end = x2;
                    d.y_end = y2;
                    return 'M' + x1 + ' ' + y1 + ' L ' + x2 + ' ' + y2;
                }
                var a = d.sourceRadius > d.targetRadius ? d.targetRadius * d.linknum / 3 : d.sourceRadius * d.linknum / 3;
                var xm = d.target.x - d.source.x > 0 ? d.source.x + Math.sqrt((d.sourceRadius * d.sourceRadius - a * a) / (1 + tan * tan)) :
                    d.source.x - Math.sqrt((d.sourceRadius * d.sourceRadius - a * a) / (1 + tan * tan));
                var ym = d.target.y - d.source.y > 0 ? d.source.y + Math.sqrt((d.sourceRadius * d.sourceRadius - a * a) * tan * tan / (1 + tan * tan)) :
                    d.source.y - Math.sqrt((d.sourceRadius * d.sourceRadius - a * a) * tan * tan / (1 + tan * tan));
                var xn = d.target.x - d.source.x > 0 ? d.target.x - Math.sqrt((d.targetRadius * d.targetRadius - a * a) / (1 + tan * tan)) :
                    d.target.x + Math.sqrt((d.targetRadius * d.targetRadius - a * a) / (1 + tan * tan));
                var yn = d.target.y - d.source.y > 0 ? d.target.y - Math.sqrt((d.targetRadius * d.targetRadius - a * a) * tan * tan / (1 + tan * tan)) :
                    d.target.y + Math.sqrt((d.targetRadius * d.targetRadius - a * a) * tan * tan / (1 + tan * tan));
                if (d.target.x - d.source.x == 0 || tan == 0) {//斜率无穷大或为0时
                    ym = d.target.y - d.source.y > 0 ? d.source.y + Math.sqrt(d.sourceRadius * d.sourceRadius - a * a) : d.source.y - Math.sqrt(d.sourceRadius * d.sourceRadius - a * a);
                    yn = d.target.y - d.source.y > 0 ? d.target.y - Math.sqrt(d.targetRadius * d.targetRadius - a * a) : d.target.y + Math.sqrt(d.targetRadius * d.targetRadius - a * a);
                }

                var k = (x1 - x2) / (y2 - y1);//连线垂线的斜率
                var dx = Math.sqrt(a * a / (1 + k * k)); //相对垂点x轴距离
                var dy = Math.sqrt(a * a * k * k / (1 + k * k)); //相对垂点y轴距离
                if ((y2 - y1) == 0) {
                    dx = 0;
                    dy = Math.sqrt(a * a);
                }
                if (a > 0) {
                    var xs = k > 0 ? xm - dx : xm + dx;
                    var ys = ym - dy;
                    var xt = k > 0 ? xn - dx : xn + dx;
                    var yt = yn - dy;
                } else {
                    var xs = k > 0 ? xm + dx : xm - dx;
                    var ys = ym + dy;
                    var xt = k > 0 ? xn + dx : xn - dx;
                    var yt = yn + dy;
                }
                //记录连线起始和终止坐标，用于定位线上文字
                d.x_start = xs;
                d.y_start = ys;
                d.x_end = xt;
                d.y_end = yt;
                return 'M' + xs + ' ' + ys + ' L ' + xt + ' ' + yt;
            });

            //更新连接线上文字的位置
            edges_text.attr("transform", function (d) {
                return "translate(" + (d.x_start + d.x_end) / 2 + "," + (d.y_start + d.y_end) / 2 + ")" + " rotate(" + Math.atan((d.y_end - d.y_start) / (d.x_end - d.x_start)) * 180 / Math.PI + ")";
            });


            //更新结点图片和文字
            circle.attr("cx", function (d) { return d.x });
            circle.attr("cy", function (d) { return d.y });

            nodes_text.attr("x", function (d) { return d.x });
            nodes_text.attr("y", function (d) { return d.y });
        });
    }


    function setLinkNumber(group) {
        if (group.length == 0) return;
        if (group.length == 1) {
            group[0].linknum = 0;
            return;
        }
        var maxLinkNumber = group.length % 2 == 0 ? group.length / 2 : (group.length - 1) / 2;
        if (group.length % 2 == 0) {
            var startLinkNum = -maxLinkNumber + 0.5;
            for (var i = 0; i < group.length; i++) {
                group[i].linknum = startLinkNum++;
            }
        } else {
            var startLinkNum = -maxLinkNumber;
            for (var i = 0; i < group.length; i++) {
                group[i].linknum = startLinkNum++;
            }
        }

    }

</script>
<script>
    $(function () {

    })

    //明细信息查询
    function showModel(d) {
        console.info(d);
        var info = d;
        var qqlx = info.source.qqlx;
        var mxqqpk = info.source.mxqqpk;
        var zh = info.source.cardnumber;
        var xjzh = info.xjzh;
        var zcType = '';
        if (qqlx && qqlx == '1') {
            zcType = "yhkMx";
        }
        if (qqlx && qqlx == '2') {
            zcType = "sfMx";
        }
        if (!mxqqpk){
            return;
        }
        /*$("#mx_dialog").css({"width":"900px"});*/
        $("#zcShowIframe").attr("src", "${ctx}/znzf/base/show?zcType=" + zcType + "&pk=" + mxqqpk + "&zh=" + zh + "&xjzh=" + xjzh);
        $("#myModal").modal();
    }

    //下拉框
    /*var op = '';
    for (var i=1; i<=5; i++){
        op+='<option value="'+i+'">'+i+'级</option>';
    }
    $("#select_nlevel").append(op);*/

    //禁毒人员查询
    function xdsdrycx(obj) {
        var ztqqpk = obj.ztqqpk;
        var qqlx = obj.qqlx;
        var pk = obj.pk;
        var nlevel = obj.nlevel;
        if (!pk){
            return;
        }
        $.ajax({
            type: "get",
            dataType: "json",
            data:{ztpk:ztqqpk,sertype:qqlx,pk:pk},
            url: ctx+"/yayk/xdSdRyCx" ,
            beforeSend: function () {
                loading("查询中，请稍后......");
            },
            success: function (data) {
                top.layer.close(indexLoading);
                if (data.success){
                    top.layer.alert("查询成功："+data.msg, {icon: 1, title: '提示'});
                }else{
                    if(data.code =='1'){
                        top.layer.alert("查询失败："+data.msg, {icon: 2, title: '提示'});
                    }else if(data.code =='2'){
                        top.layer.confirm("查询失败："+data.msg+'需要提供身份证号号才能查询，确认继续吗？', {icon: 2, title: '提示'},function(r){
                            top.layer.prompt({title: '请输入身份证号码，点确认查询', formType: 0}, function(text, index){
                                $.ajax({
                                    type: "get",
                                    dataType: "json",
                                    data:{sfzh:text,pk:pk},
                                    url: ctx+"/yayk/xdSdRyCx2" ,
                                    beforeSend: function () {
                                        loading("查询中，请稍后......");
                                    },
                                    success: function (data) {
                                        top.layer.close(indexLoading);
                                        if (data.success){
                                            top.layer.alert("查询成功："+data.msg, {icon: 1, title: '提示'});
                                            top.layer.close(index);
                                        }else{
                                            top.layer.alert("查询失败：请求异常", {icon: 2, title: '提示'});
                                        }
                                    },
                                    error : function() {
                                        top.layer.alert("查询失败：请求异常", {icon: 2, title: '提示'});
                                    }
                                });
                            });
                        });
                    }else{
                        top.layer.alert("查询失败：请求异常", {icon: 2, title: '提示'});
                    }
                }
            },
            error : function() {
                top.layer.alert("查询失败：请求异常", {icon: 2, title: '提示'});
            }
        });

    }
    function loading(msg) {
        indexLoading = top.layer.msg(msg, {
            icon: 16,
            shade: [0.1, '#666666'],
            time: false  //不自动关闭
        })
    }

    //主体查询
    function ztcx(obj) {
        var ztqqpk = obj.ztqqpk;
        var qqlx = obj.qqlx;
        var cardnumber = obj.cardnumber;
        console.info(qqlx);
        if (!ztqqpk){
            return;
        }
        $("#myModalLabel_public").text("主体信息");
        /*$("#public_dialog").css({"width":"800px"});*/
        var zcType = "";
        if(qqlx && qqlx == '1'){
            zcType="yhkZt";
        }
        if(qqlx && qqlx == '2'){
            zcType="sfZt";
        }
        $("#publicShowIframe").attr("src",ctx+"/znzf/base/show?zcType="+zcType+"&pk="+ztqqpk+"&zh="+cardnumber);
        $("#myModal_public").modal();
    }

    //主题请求重发
    function ztqqcf(obj) {
        var ztqqpk = obj.ztqqpk;
        var qqlx = obj.qqlx;
        if (!ztqqpk){
            return;
        }
        $.ajax({
            type: "get",
            dataType: "json",
            data:{ztpk:ztqqpk,sertype:qqlx},
            url: ctx+"/yayk/ztQqSend" ,
            success: function (data) {
                if (data.success){
                    top.layer.alert("重发成功："+data.msg, {icon: 1, title: '提示'});
                }else{
                    if(data.code =='0' || data.code =='1'){
                        top.layer.alert("重发失败："+data.msg, {icon: 2, title: '提示'});
                    }else{
                        top.layer.alert("重发失败：请求异常", {icon: 2, title: '提示'});
                    }
                }
            },
            error : function() {
                top.layer.alert("重发失败：请求异常", {icon: 2, title: '提示'});
            }
        });
    }

    //获取渲染数据
    /*function getMapData() {
        var result = null;
        if (data_url){
            $.ajax({
                type: "post",
                /!*dataType: "json",*!/
                url: data_url ,
                success: function (data) {
                    if (data){
                        result = data;
                    }
                },
                error : function() {
                    top.layer.alert("请求异常", {icon: 2, title: '提示'});
                }
            });
        }
        return result;
    }*/
    /*window.onload = function (ev) {
        $("[data-toggle='popover']").popover();
    }*/
    $(function () {
        $("[data-toggle='popover']").popover();

    });

</script>
<script type="text/javascript" src="${ctx}/resources/layer/layer.js" charset="utf-8"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${ctx}/resources/jquery-ui/jquery-ui.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-closeable-tab.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
</body>
</html>