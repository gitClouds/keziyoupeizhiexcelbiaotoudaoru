<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">主键</td>
            <td id="show_pk"></td>
    		<td class="sec_tit">接警单号</td>
            <td id="show_jjdhm"></td>
		</tr>
		<tr>
    		<td class="sec_tit">案件编号</td>
            <td id="show_ajbh"></td>
    		<td class="sec_tit">案件ID（预留字段）</td>
            <td id="show_ajid"></td>
		</tr>
		<tr>
    		<td class="sec_tit">止付金额</td>
            <td id="show_zfje"></td>
    		<td class="sec_tit">警单性质</td>
            <td id="show_jdxz"></td>
		</tr>
		<tr>
    		<td class="sec_tit">受理单位</td>
            <td id="show_sldw"></td>
    		<td class="sec_tit">受理单位名称</td>
            <td id="show_sldwmc"></td>
		</tr>
		<tr>
    		<td class="sec_tit">接警时间</td>
            <td id="show_jjsj"></td>
    		<td class="sec_tit">报案人姓名</td>
            <td id="show_barxm"></td>
		</tr>
		<tr>
    		<td class="sec_tit">报案人身份证</td>
            <td id="show_barsfzh"></td>
    		<td class="sec_tit">报案人联系电话</td>
            <td id="show_barlxdh"></td>
		</tr>
		<tr>
    		<td class="sec_tit">接警方式</td>
            <td id="show_jjfs"></td>
    		<td class="sec_tit">案件类型</td>
            <td id="show_ajlx"></td>
		</tr>
		<tr>
    		<td class="sec_tit">案件类别</td>
            <td id="show_ajlb"></td>
    		<td class="sec_tit">案发时间</td>
            <td id="show_afsj"></td>
		</tr>
		<tr>
    		<td class="sec_tit">涉案金额</td>
            <td id="show_saje"></td>
    		<td class="sec_tit">案发地点</td>
            <td id="show_afdd"></td>
		</tr>
		<tr>
    		<td class="sec_tit">简要案情</td>
            <td id="show_jyaq"></td>
    		<td class="sec_tit">涉案网址</td>
            <td id="show_sawz"></td>
		</tr>
		<tr>
    		<td class="sec_tit">备注</td>
            <td id="show_bz"></td>
    		<td class="sec_tit">状态</td>
            <td id="show_zt"></td>
		</tr>
		<tr>
    		<td class="sec_tit">录入时间</td>
            <td id="show_cjsj"></td>
    		<td class="sec_tit">修改时间</td>
            <td id="show_xgsj"></td>
		</tr>
		<tr>
    		<td class="sec_tit">区域</td>
            <td id="show_area"></td>
    		<td class="sec_tit">录入单位代码</td>
            <td id="show_lrdwdm"></td>
		</tr>
		<tr>
    		<td class="sec_tit">录入单位</td>
            <td id="show_lrdwmc"></td>
    		<td class="sec_tit">录入人姓名</td>
            <td id="show_lrrxm"></td>
		</tr>
		<tr>
    		<td class="sec_tit">录入人警号</td>
            <td id="show_lrrjh"></td>
    		<td class="sec_tit">立案单位代码</td>
            <td id="show_ladwdm"></td>
		</tr>
		<tr>
    		<td class="sec_tit">立案单位名称</td>
            <td id="show_ladwmc"></td>
    		<td class="sec_tit">立案时间</td>
            <td id="show_lasj"></td>
		</tr>
		<tr>
    		<td class="sec_tit">联系人（立案）</td>
            <td id="show_lxr"></td>
    		<td class="sec_tit">联系电话（立案）</td>
            <td id="show_lxdh"></td>
		</tr>
		<tr>
    		<td class="sec_tit">联系人手机号码</td>
            <td id="show_lxrsj"></td>
    		<td class="sec_tit"> 案件类别子类</td>
            <td id="show_ajlbzl"></td>
		</tr>
    </table>
</body>
</html>
