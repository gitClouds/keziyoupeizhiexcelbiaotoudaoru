<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html>
<head>
    <title>案件信息</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
    <link rel="stylesheet" href="${ctx}/css/pub/form.css">
    <style>
        .table-bordered{
            font-size: 12px;
            margin-bottom: 5px;
            border: 0px;
            margin-top: 12px;
        }
        .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td{
            padding: 0px;
            border: 0px;
        }
        .titDiv h5{
            font-weight: 700;font-size: 15px;
            color: #606c84;
            display: inline-block;
            margin-left: 10px;
        }
        .form_chag .form-group{
            padding-top: 12px;
            width: 22%;
            padding-right: 2px;
            font-size: 12px;
        }
        .form_chag .form-group .form_controls{
            display: inline-block;
        }
        .borderDiv{
            padding: 5px;
            border: 1px solid #ccc;
            margin-bottom: 10px;
        }
        .add_btn{
            text-align: right;
            padding: 5px 10px;
        }
        .add_btn i{
            margin: 0 10px;
        }
        .add_btn .fa:hover{
            color: #00a7d0;
        }
    </style>
</head>
<body>
<!-- 主体内容 -->
<section class="content">
    <!--查询项表单 begin-->
    <div class="row">
        <div class="col-xs-12">
            <ul class="box clearfix">
    <form class="form_chag" id="ajxxEditForm" name="ajxxEditForm">
        <input type="hidden" id="curdType" name="curdType" value="${curdType}" >
        <input type="hidden" id="pk" name="pk" value="${ajxx.pk}" >
        <div class="borderDiv clearfix">
            <div class="form-group">
                <label for="jjdhm">接警单号:</label>
                <div class="form_controls">
                    <input type="text"  value="${ajxx.jjdhm}" id="jjdhm" name="jjdhm" class="required form-control" data-valid="isNonEmpty||onlyEnAndInt||between:10-32" data-error="不能为空||只能输入数字和字母||长度为10-32位" >
                </div>
            </div>
            <div class="form-group">
                <label for="ajbh">案件编号:</label>
                <div class="form_controls">
                    <input type="text"  value="${ajxx.ajbh}" id="ajbh" name="ajbh" class="required form-control" data-valid="isNonEmpty||onlyEnAndInt||between:10-32" data-error="不能为空||只能输入数字和字母||长度为10-32位" >
                </div>
            </div>
            <div class="form-group">
                <label for="ajlbmc">案件类别:</label>
                <div class="has-feedback form_controls " >
                    <input type="hidden" value="${ajxx.ajlb}" id="ajlb" name="ajlb" />
                    <input type="text" value='${dicMap["010107"][fn:trim(ajxx.ajlb)]}' class="required form-control" id="ajlbmc" name="ajlbmc" data-jzd-type="tree" data-jzd-code="010107" data-jzd-filter="" data-jzd-dm="ajlb" data-jzd-mc="ajlbmc" readonly  data-valid="isNonEmpty" data-error="不能为空" >
                    <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('ajlb');clearInput('ajlbmc');"></span>
                </div>
            </div>
            <div class="form-group">
                <label for="saje">涉案金额:</label>
                <div class="form_controls">
                    <input type="text"  value="${ajxx.saje}" id="saje" name="saje" class="required form-control" data-valid="onlyNum" data-error="请输入有效金额" >
                </div>
            </div>
            <div class="form-group">
                <label for="afsj">案发时间:</label>
                <div class="form_controls">
                    <input type="text"  value="<fmt:formatDate value="${ajxx.afsj}" pattern="yyyy-MM-dd HH:mm:ss"/>" id="afsj" name="afsj" onFocus="WdatePicker({lang:'zh-cn',isShowClear:false,dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'%y-%M-%d'})" class="Wdate required form-control" data-valid="isNonEmpty" data-error="不能为空" >
                </div>
            </div>
            <div class="form-group">
                <label for="sldwmc">发案地区划:</label>
                <div class="has-feedback form_controls">
                    <input type="hidden" value="${ajxx.sldw}" id="sldw" name="sldw" />
                    <input type="text" value="${ajxx.sldwmc}" class="required form-control" id="sldwmc" name="sldwmc"  data-jzd-type="tree" data-jzd-code="DEPT" data-jzd-filter="" data-jzd-dm="sldw" data-jzd-mc="sldwmc" readonly data-valid="isNonEmpty" data-error="不能为空" >
                    <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('sldw');clearInput('sldwmc');"></span>
                </div>
            </div>
            <div class="form-group">
                <label for="afdd">发案地详址:</label>
                <div class="form_controls">
                    <input type="text"  value="${ajxx.afdd}" id="afdd" name="afdd" class="required form-control" data-valid="isNonEmpty||between:5-200" data-error="不能为空||长度为5-200位">
                </div>
            </div>
            <table class="table table-bordered">
                <tr>
                    <td style="width: 55px"><label for="jyaq">简要案情:</label></td>
                    <td colspan="3" >
                        <div class="form_controls" style="display: inline-block;width: 95%">
                            <textarea style="resize:none" rows="3" class="required form-control" id="jyaq" name="jyaq" data-valid="isNonEmpty||between:2-2000" data-error="不能为空||长度为2-2000位">${ajxx.jyaq}</textarea>
                        </div>
                    </td>
                </tr>
            </table>
            <div class="form-group">
                <label for="barxm">报案人姓名:</label>
                <div class="form_controls">
                    <input type="text"  value="${ajxx.barxm}" id="barxm" name="barxm" class="required form-control" data-valid="isNonEmpty||onlyZh||between:2-10" data-error="不能为空||只能输入中文||长度为2-10位" >
                </div>
            </div>
            <div class="form-group">
                <label for="jjsj">报案时间:</label>
                <div class="form_controls">
                    <input type="text"  value="<fmt:formatDate value="${ajxx.jjsj}" pattern="yyyy-MM-dd HH:mm:ss"/>" id="jjsj" name="jjsj" onFocus="WdatePicker({lang:'zh-cn',isShowClear:false,dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'%y-%M-%d'})" class="Wdate required form-control" data-valid="isNonEmpty" data-error="不能为空" >
                </div>
            </div>

            <div class="form-group">
                <label for="barsfzh">报案人身份证号:</label>
                <div class="form_controls">
                    <input type="text"  value="${ajxx.barsfzh}" id="barsfzh" name="barsfzh" class="required form-control"  data-valid="isNonEmpty||isCardNo" data-error="不能为空||请输入正确的身份证号码" >
                </div>
            </div>
            <div class="form-group">
                <label for="barlxdh">报案人联系电话:</label>
                <div class="form_controls">
                    <input type="text"  value="${ajxx.barlxdh}" id="barlxdh" name="barlxdh" class="required form-control"  data-valid="isNonEmpty||onlyInt||between:10-12" data-error="不能为空||只能输入数字||长度为10-12位"  >
                </div>
            </div>
        </div>
        <div class="borderDiv clearfix">
            <div class="form-group ">
                <label for="ladwmc">立案单位:</label>
                <div class="has-feedback form_controls">
                    <input type="hidden" value="${ajxx.ladwdm}" id="ladwdm" name="ladwdm" />
                    <input type="text" value="${ajxx.ladwmc}" class="required form-control" id="ladwmc" name="ladwmc"  data-jzd-type="tree" data-jzd-code="DEPT" data-jzd-filter="" data-jzd-dm="ladwdm" data-jzd-mc="ladwmc" readonly data-valid="isNonEmpty" data-error="不能为空" >
                    <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('ladwdm');clearInput('ladwdmmc');"></span>
                </div>
            </div>
            <div class="form-group">
                <label for="lasj">立案时间:</label>
                <div class="form_controls">
                    <input type="text"  value="<fmt:formatDate value="${ajxx.lasj}" pattern="yyyy-MM-dd HH:mm:ss"/>" id="lasj" name="lasj" onFocus="WdatePicker({lang:'zh-cn',isShowClear:false,dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'%y-%M-%d'})" class="Wdate required form-control" data-valid="isNonEmpty" data-error="不能为空" >
                </div>
            </div>
        </div>
        <div class="borderDiv clearfix">
            <div class="form-group">
                <label for="lxr">联系人（立案）:</label>
                <div class="form_controls">
                    <input type="text"  value="${ajxx.lxr}" id="lxr" name="lxr" class="required form-control" data-valid="isNonEmpty||onlyZh||between:2-10" data-error="不能为空||只能输入中文||长度为2-10位" >
                </div>
            </div>
            <div class="form-group">
                <label for="lxdh">联系电话（立案）:</label>
                <div class="form_controls">
                    <input type="text"  value="${ajxx.lxdh}" id="lxdh" name="lxdh" class="required form-control"  data-valid="isNonEmpty||onlyInt||between:10-12" data-error="不能为空||只能输入数字||长度为10-12位"  >
                </div>
            </div>

        </div>
        <button type="button" style="float: right;margin-right: 4%;" class="btn btn-info" onclick="ajxxEditSubmit();">提交</button>
        <div style="clear: both"></div>
    </form>
    </ul>
    </div>
    </div>
    <!-- 字典、树形字典展示框。如果page、form均无字典项，可无需此div -->
    <div id="treeDiv">
        <input type="text" class="form-control" id="searchTree"  placeholder="支持文本检索">
        <div id="tree"></div>
    </div>
    </section>
    <script>
        var ctx = "${ctx}";
    </script>
    <script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
    <script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
    <script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
    <script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
    <script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
    <script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
    <script src="${ctx}/resources/common/jquery-validate.js"></script>
    <script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
    <script src="${ctx}/js/common.js"></script>
    <script src="${ctx}/js/fzzx/ajxx.js"></script>
</body>
</html>
