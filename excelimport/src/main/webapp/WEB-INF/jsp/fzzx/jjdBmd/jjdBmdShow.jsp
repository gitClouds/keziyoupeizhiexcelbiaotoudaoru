<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">账号</td>
            <td id="show_zh"></td>
			<td class="sec_tit">账号名称</td>
			<td id="show_zhmc"></td>
		</tr>
		<tr>
    		<td class="sec_tit">录入单位</td>
            <td id="show_lrdwmc"></td>
			<td class="sec_tit">录入人</td>
			<td id="show_lrrxm"></td>
		</tr>
		<tr>
    		<td class="sec_tit">录入时间</td>
            <td id="show_cjsj"></td>
    		<td class="sec_tit">修改时间</td>
            <td id="show_xgsj"></td>
		</tr>
		<tr>
    		<td class="sec_tit">修改单位</td>
            <td id="show_xgdwmc"></td>
			<td class="sec_tit">修改人</td>
			<td id="show_xgr"></td>
		</tr>
		<tr>
			<td class="sec_tit">备注</td>
			<td id="show_bz" colspan="3"></td>
		</tr>
    </table>
</body>
</html>
