<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <form class="form_chag" id="jjdBmdEditForm" name="jjdBmdEditForm">
        <input type="hidden" id="edit_curdType" name="curdType" >
        <input type="hidden" id="edit_pk" name="pk">
        <table class="table table-bordered">
        	<!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
            <!-- 请根据需要修改 可编辑项、hidden等 -->
            <!-- 表单校验的字段需要在input外加一层 <div class="form_controls"> -->
            <!-- 表单校验的input classes + required -->
            <!-- 表单校验的input data-tip="" 为进入时提醒 -->
            <!-- 表单校验的input data-valid="" 为验证规则，多个可使用‘||’分割；具体参照jquery-validate.js-->
            <!-- 表单校验的input data-error="" 为验证不通过时的提醒，需与 data-valid一一对应 -->
            <!-- 校验需自己填写 -->
			<tr>
        		<td class="sec_tit">
                    <label for="edit_zh">账号</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_zh" name="zh" data-valid="isNonEmpty||between:6-100" data-error="不能为空||长度为6-100位" >
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_zhmc">账号名称</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" class="required form-control" id="edit_zhmc" name="zhmc" data-valid="isNonEmpty||between:2-100" data-error="不能为空||长度为2-100位" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_bz">备注</label>
                </td>
                <td colspan="3">
                	<div class="form_controls">
                        <textarea style="resize:none" rows="2" class="required form-control" id="edit_bz" name="bz" data-valid="between:2-2000" data-error="长度为2-2000位"></textarea>
                    </div>
                </td>
			</tr>

        </table>
    </form>
</body>
</html>
