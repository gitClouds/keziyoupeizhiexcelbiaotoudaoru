<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
    <link rel="stylesheet" href="${ctx}/css/pub/form.css">
    <style>
        .table-bordered{
            font-size: 12px;
            margin-bottom: 5px;
            border: 0px;
            margin-top: 12px;
        }
        .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td{
            padding: 0px;
            border: 0px;
        }
        .titDiv h5{
            font-weight: 700;font-size: 15px;
            color: #606c84;
            display: inline-block;
            margin-left: 10px;
        }
        .form_chag .form-group{
            padding-top: 12px;
            width: 19%;
            padding-right: 2px;
            font-size: 12px;
        }
        .form_chag .form-group .form_controls{
            display: inline-block;
        }
        .borderDiv{
            padding: 5px;
            border: 1px solid #ccc;
            margin-bottom: 10px;
        }
        .add_btn{
            text-align: right;
            padding: 5px 10px;
        }
        .add_btn i{
            margin: 0 10px;
        }
        .add_btn .fa:hover{
            color: #00a7d0;
        }
    </style>
</head>
<body>
<jsp:include page="../../loading.jsp"/>
<!-- 主体内容 -->
<section class="content">
    <!--查询项表单 begin-->
    <div class="row">
        <div class="col-xs-12">
            <ul class="box clearfix">
    <form class="form_chag" id="jjdEditForm" name="jjdEditForm">
        <input type="hidden" id="curdType" name="curdType" value="${curdType}" >
        <input type="hidden" id="pk" name="pk" value="${jjd.pk}" >

        <div class="borderDiv clearfix">
            <div class="form-group">
                <label for="jjdhm">接警单号:</label>
                <div class="form_controls">
                    <input type="text"  value="${jjd.jjdhm}" class="required form-control" id="jjdhm" name="jjdhm" data-valid="isNonEmpty||onlyEnAndInt||between:10-32" data-error="不能为空||只能输入数字和字母||长度为10-32位" >
                </div>
            </div>
            <div class="form-group">
                <label for="zfje">止付金额:</label>
                <div class="form_controls">
                    <input type="text" value="${jjd.zfje}" class="required form-control" id="zfje" name="zfje" data-valid="onlyNum" data-error="请输入有效金额" >
                </div>
            </div>
            <div class="form-group ">
                <label for="jdxzmc">警单性质:</label>
                <div class="has-feedback form_controls " >
                    <input type="hidden" value="${jjd.jdxz}" id="jdxz" name="jdxz" />
                    <input type="text" value='${dicMap["010101"][fn:trim(jjd.jdxz)]}' class="required form-control" id="jdxzmc" name="jdxzmc" data-jzd-type="code" data-jzd-code="010101" data-jzd-filter="" data-jzd-dm="jdxz" data-jzd-mc="jdxzmc" readonly  data-valid="isNonEmpty" data-error="不能为空" >
                    <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('jdxz');clearInput('jdxzmc');"></span>
                </div>
            </div>
            <div class="form-group ">
                <label for="sldwmc">受理单位:</label>
                <div class="has-feedback form_controls">
                    <input type="hidden" value="${jjd.sldw}" id="sldw" name="sldw" />
                    <input type="text" value="${jjd.sldwmc}" class="required form-control" id="sldwmc" name="sldwmc"  data-jzd-type="tree" data-jzd-code="DEPT" data-jzd-filter="" data-jzd-dm="sldw" data-jzd-mc="sldwmc" readonly data-valid="isNonEmpty" data-error="不能为空" >
                    <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('sldw');clearInput('sldwmc');"></span>
                </div>
            </div>
            <div class="form-group">
                <label for="jjsj">报警时间:</label>
                <div class="form_controls">
                    <input type="text" value="<fmt:formatDate value="${jjd.jjsj}" pattern="yyyy-MM-dd HH:mm:ss"/>" class="Wdate required form-control" id="jjsj" name="jjsj" onFocus="WdatePicker({lang:'zh-cn',isShowClear:false,dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'%y-%M-%d'})" data-valid="isNonEmpty" data-error="不能为空" >
                </div>
            </div>
        </div>
        <div class="borderDiv clearfix">
            <div class="titDiv form-group" style="padding-top: 0px;margin-bottom: 0px">
                <i class="fa fa-user fa-2x"></i><h5>报案人信息</h5>
            </div>
            <div style="clear: both"></div>
            <div class="form-group">
                <label for="barxm">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;姓名:</label>
                <div class="form_controls">
                    <input type="text" value="${jjd.barxm}" class="required form-control" id="barxm" name="barxm" data-valid="isNonEmpty||onlyZh||between:2-10" data-error="不能为空||只能输入中文||长度为2-10位" >
                </div>
            </div>
            <div class="form-group">
                <label for="barsfzh">身份证号:</label>
                <div class="form_controls">
                    <input type="text" value="${jjd.barsfzh}" class="required form-control" id="barsfzh" name="barsfzh" data-valid="isCardNo" data-error="请输入正确的身份证号码" >
                </div>
            </div>
            <div class="form-group">
                <label for="barlxdh">联系电话:</label>
                <div class="form_controls">
                    <input type="text" value="${jjd.barlxdh}" class="required form-control" id="barlxdh" name="barlxdh" data-valid="isNonEmpty||onlyInt||between:10-12" data-error="不能为空||只能输入数字||长度为10-12位" >
                </div>
            </div>
            <div class="form-group">
                <label for="jjfs">接警方式:</label>
                <div class="has-feedback form_controls " >
                    <input type="hidden" value="${jjd.jjfs}" id="jjfs" name="jjfs" />
                    <input type="text" value='${dicMap["010102"][fn:trim(jjd.jjfs)]}' class="required form-control" id="jjfsmc" name="jjfsmc" data-jzd-type="code" data-jzd-code="010102" data-jzd-filter="" data-jzd-dm="jjfs" data-jzd-mc="jjfsmc" readonly  data-valid="isNonEmpty" data-error="不能为空" >
                    <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('jjfs');clearInput('jjfsmc');"></span>
                </div>
            </div>
        </div>
        <div class="borderDiv clearfix">
            <div class="titDiv form-group" style="padding-top: 0px;margin-bottom: 0px">
                <i class="fa fa-file-text fa-2x"></i><h5>案件信息</h5>
            </div>
            <div style="clear: both"></div>
            <div class="form-group">
                <label for="ajlx">案件类型:</label>
                <div class="has-feedback form_controls " >
                    <input type="hidden" value="${jjd.ajlx}" id="ajlx" name="ajlx" />
                    <input type="text" value='${dicMap["010103"][fn:trim(jjd.ajlx)]}' class="required form-control" id="ajlxmc" name="ajlxmc" data-jzd-type="code" data-jzd-code="010103" data-jzd-filter="" data-jzd-dm="ajlx" data-jzd-mc="ajlxmc" readonly  data-valid="isNonEmpty" data-error="不能为空" >
                    <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('ajlx');clearInput('ajlxmc');"></span>
                </div>
            </div>
            <div class="form-group">
                <label for="ajlb">案件类别:</label>
                <div class="has-feedback form_controls " >
                    <input type="hidden" value="${jjd.ajlb}" id="ajlb" name="ajlb" />
                    <input type="text" value='${dicMap["010107"][fn:trim(jjd.ajlb)]}' class="required form-control" id="ajlbmc" name="ajlbmc" data-jzd-type="tree" data-jzd-code="010107" data-jzd-filter="" data-jzd-dm="ajlb" data-jzd-mc="ajlbmc" readonly  data-valid="isNonEmpty" data-error="不能为空" >
                    <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('ajlb');clearInput('ajlbmc');"></span>
                </div>
            </div>
            <div class="form-group">
                <label for="afsj">案发时间:</label>
                <div class="form_controls">
                    <input type="text" value="<fmt:formatDate value="${jjd.afsj}" pattern="yyyy-MM-dd HH:mm:ss"/>" class="Wdate required form-control" id="afsj" name="afsj" onFocus="WdatePicker({lang:'zh-cn',isShowClear:false,dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'%y-%M-%d'})"  data-valid="isNonEmpty" data-error="不能为空" >
                </div>
            </div>
            <div class="form-group">
                <label for="saje">涉案金额:</label>
                <div class="form_controls">
                    <input type="text" value="${jjd.saje}" class="required form-control" id="saje" name="saje" data-valid="isNonEmpty||onlyNum" data-error="不能为空||请输入有效金额" >
                </div>
            </div>
            <div class="form-group">
                <label for="afdd">案发地点:</label>
                <div class="form_controls">
                    <input type="text" value="${jjd.afdd}" class="required form-control" id="afdd" name="afdd" data-valid="isNonEmpty||between:5-200" data-error="不能为空||长度为5-200位" >
                </div>
            </div>
            <table class="table table-bordered">
                <tr>
                    <td style="width: 55px"><label for="jyaq">简要案情:</label></td>
                    <td colspan="3" >
                        <div class="form_controls" style="display: inline-block;width: 95%">
                            <textarea style="resize:none" rows="3" class="required form-control" id="jyaq" name="jyaq" data-valid="isNonEmpty||between:2-2000" data-error="不能为空||长度为2-2000位">${jjd.jyaq}</textarea>
                        </div>
                    </td>
                </tr>
            </table>
            <table class="table table-bordered" style="margin-top: 16px">
                <tr>
                    <td style="width: 55px;"><label for="bz">备注:</label></td>
                    <td colspan="3" >
                        <div class="form_controls" style="display: inline-block;width: 95%">
                            <textarea style="resize:none" rows="2" class="required form-control" id="bz" name="bz" data-valid="between:2-2000" data-error="长度为2-2000位">${jjd.bz}</textarea>
                        </div>
                    </td>
                </tr>
            </table>
            <div class="titDiv form-group" style="padding-top: 0px;margin-bottom: 0px">
                <h5>受害人信息</h5>
            </div>
            <div class="borderDiv" style="clear: both">
    <c:choose>
        <c:when test="${shrList!=null && shrList.size()>0}">
            <c:forEach items="${shrList}" var="obj" varStatus="status">
                <div class="shrData" data-bean-num="${status.index}" style="border-bottom: 1px #ccc dashed">
                    <div class="form-group">
                        <label for="shr_yhk_${status.index}">银行卡号:</label>
                        <div class="form_controls">
                            <input type="hidden" value="${obj.pk}" id="shr_pk_${status.index}" name="shr[${status.index}].pk" />
                            <input type="text" value="${obj.yhk}" class="required form-control" id="shr_yhk_${status.index}" name="shr[${status.index}].yhk" data-valid="onlyInt" data-error="请输入有效卡号" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="shr_ckr_${status.index}">持卡人:</label>
                        <div class="form_controls">
                            <input type="text" value="${obj.ckr}" class="form-control" id="shr_ckr_${status.index}" name="shr[${status.index}].ckr" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="shr_sfzh_${status.index}">身份证:</label>
                        <div class="form_controls">
                            <input type="text" value="${obj.sfzh}" class="required form-control" id="shr_sfzh_${status.index}" name="shr[${status.index}].sfzh" data-valid="isCardNo" data-error="请输入有效身份证" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="shr_zzfs_${status.index}">资金流出:</label>
                        <div class="has-feedback form_controls " >
                            <input type="hidden" value="${obj.zzfs}" id="shr_zzfs_${status.index}" name="shr[${status.index}].zzfs" />
                            <input type="text" value="${dicMap["010104"][fn:trim(obj.zzfs)]}" class="required form-control" id="shr_zzfsmc_${status.index}" data-jzd-type="code" data-jzd-code="010104" data-jzd-filter="" data-jzd-dm="shr_zzfs_${status.index}" data-jzd-mc="shr_zzfsmc_${status.index}" data-valid="isNonEmpty" data-error="不能为空" readonly  >
                            <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('shr_zzfs_${status.index}');clearInput('shr_zzfsmc_${status.index}');"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="shr_zzsj_${status.index}">转账时间:</label>
                        <div class="form_controls">
                            <input type="text" value="<fmt:formatDate value="${obj.zzsj}" pattern="yyyy-MM-dd HH:mm:ss"/>" class="required form-control" id="shr_zzsj_${status.index}" name="shr[${status.index}].zzsj" onFocus="WdatePicker({lang:'zh-cn',isShowClear:false,dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'%y-%M-%d'})"  data-valid="isNonEmpty" data-error="不能为空"  >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="shr_zzje_${status.index}">转账金额:</label>
                        <div class="form_controls">
                            <input type="text" value="${obj.zzje}" class="required form-control" id="shr_zzje_${status.index}" name="shr[${status.index}].zzje" data-valid="onlyNum" data-error="请输入有效金额" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="shr_zfb_${status.index}">支付宝:</label>
                        <div class="form_controls">
                            <input type="text" value="${obj.zfb}" class="form-control" id="shr_zfb_${status.index}" name="shr[${status.index}].zfb" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="shr_cft_${status.index}">财付通:</label>
                        <div class="form_controls">
                            <input type="text" value="${obj.cft}" class="form-control" id="shr_cft_${status.index}" name="shr[${status.index}].cft" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="shr_wx_${status.index}">微信账号:</label>
                        <div class="form_controls">
                            <input type="text" value="${obj.wx}" class="form-control" id="shr_wx_${status.index}" name="shr[${status.index}].wx" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="shr_dh_${status.index}">电话号码:</label>
                        <div class="form_controls">
                            <input type="text" value="${obj.dh}" class="form-control" id="shr_dh_${status.index}" name="shr[${status.index}].dh" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="shr_jydh_${status.index}">交易单号:</label>
                        <div class="form_controls">
                            <input type="text" value="${obj.jydh}" class="form-control" id="shr_jydh_${status.index}" name="shr[${status.index}].jydh" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="shr_qq_${status.index}">&nbsp;QQ号:</label>
                        <div class="form_controls">
                            <input type="text" value="${obj.qq}" class="form-control" id="shr_qq_${status.index}" name="shr[${status.index}].qq" >
                        </div>
                    </div>
                    <div class="form-group">
                        <i class="fa fa-minus-square fa-3x text-red" onclick="removeShr(this);"></i>
                    </div>
                    <div style="clear: both"></div>
                </div>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <div class="shrData" data-bean-num="0" style="border-bottom: 1px #ccc dashed">
                <div class="form-group">
                    <label for="shr_yhk_0">银行卡号:</label>
                    <div class="form_controls">
                        <input type="hidden" id="shr_pk_0" name="shr[0].pk" value="" />
                        <input type="text" class="required form-control" id="shr_yhk_0" name="shr[0].yhk" data-valid="onlyInt" data-error="请输入有效卡号" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="shr_ckr_0">持卡人:</label>
                    <div class="form_controls">
                        <input type="text" class="form-control" id="shr_ckr_0" name="shr[0].ckr" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="shr_sfzh_0">身份证:</label>
                    <div class="form_controls">
                        <input type="text" class="required form-control" id="shr_sfzh_0" name="shr[0].sfzh" data-valid="isCardNo" data-error="请输入有效身份证" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="shr_zzfs_0">资金流出:</label>
                    <div class="has-feedback form_controls " >
                        <input type="hidden" id="shr_zzfs_0" name="shr[0].zzfs" />
                        <input type="text" class="required form-control" id="shr_zzfsmc_0" data-jzd-type="code" data-jzd-code="010104" data-jzd-filter="" data-jzd-dm="shr_zzfs_0" data-jzd-mc="shr_zzfsmc_0"  data-valid="isNonEmpty" data-error="不能为空"  readonly  >
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('shr_zzfs_0');clearInput('shr_zzfsmc_0');"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="shr_zzsj_0">转账时间:</label>
                    <div class="form_controls">
                        <input type="text" class="required form-control" id="shr_zzsj_0" name="shr[0].zzsj" onFocus="WdatePicker({lang:'zh-cn',isShowClear:false,dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'%y-%M-%d'})" data-valid="isNonEmpty" data-error="不能为空"  >
                    </div>
                </div>
                <div class="form-group">
                    <label for="shr_zzje_0">转账金额:</label>
                    <div class="form_controls">
                        <input type="text" class="required form-control" id="shr_zzje_0" name="shr[0].zzje" data-valid="onlyNum" data-error="请输入有效金额" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="shr_zfb_0">支付宝:</label>
                    <div class="form_controls">
                        <input type="text" class="form-control" id="shr_zfb_0" name="shr[0].zfb" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="shr_cft_0">财付通:</label>
                    <div class="form_controls">
                        <input type="text" class="form-control" id="shr_cft_0" name="shr[0].cft" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="shr_wx_0">微信账号:</label>
                    <div class="form_controls">
                        <input type="text" class="form-control" id="shr_wx_0" name="shr[0].wx" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="shr_dh_0">电话号码:</label>
                    <div class="form_controls">
                        <input type="text" class="form-control" id="shr_dh_0" name="shr[0].dh" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="shr_jydh_0">交易单号:</label>
                    <div class="form_controls">
                        <input type="text" class="form-control" id="shr_jydh_0" name="shr[0].jydh" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="shr_qq_0">&nbsp;QQ号:</label>
                    <div class="form_controls">
                        <input type="text" class="form-control" id="shr_qq_0" name="shr[0].qq" >
                    </div>
                </div>
                <div class="form-group">
                    <i class="fa fa-minus-square fa-3x text-red" onclick="removeShr(this);"></i>
                </div>
                <div style="clear: both"></div>
            </div>
        </c:otherwise>
    </c:choose>
                <div class="add_btn">
                    <i class="fa fa-plus-square fa-2x" onclick="addShr();"></i>
                </div>
            </div>
            <div class="titDiv form-group" style="padding-top: 0px;margin-bottom: 0px">
                <h5>嫌疑人信息</h5>
            </div>
            <div class="borderDiv" style="clear: both">
    <c:choose>
        <c:when test="${xyrList!=null && xyrList.size()>0}">
            <c:forEach items="${xyrList}" var="obj" varStatus="status">
            <div class="xyrData" data-bean-num="${status.index}" style="border-bottom: 1px #ccc dashed">
                <div class="form-group">
                    <label for="xyr_yhk_${status.index}">银行卡号:</label>
                    <div class="form_controls">
                        <input type="hidden" value="${obj.pk}" id="xyr_pk_${status.index}" name="xyr[${status.index}].pk" value="" />
                        <input type="text" value="${obj.yhk}" class="required form-control" id="xyr_yhk_${status.index}" name="xyr[${status.index}].yhk" data-valid="onlyInt" data-error="请输入有效卡号" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="xyr_ckr_${status.index}">持卡人:</label>
                    <div class="form_controls">
                        <input type="text" value="${obj.ckr}" class="form-control" id="xyr_ckr_${status.index}" name="xyr[${status.index}].ckr" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="xyr_sfzh_${status.index}">身份证:</label>
                    <div class="form_controls">
                        <input type="text" value="${obj.sfzh}" class="required form-control" id="xyr_sfzh_${status.index}" name="xyr[${status.index}].sfzh" data-valid="isCardNo" data-error="请输入有效身份证" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="xyr_zzfs_${status.index}">转账方式:</label>
                    <div class="has-feedback form_controls " >
                        <input type="hidden" value="${obj.zzfs}" id="xyr_zzfs_${status.index}" name="xyr[${status.index}].zzfs" />
                        <input type="text" value="${dicMap["010104"][fn:trim(obj.zzfs)]}" class="required form-control" id="xyr_zzfsmc_${status.index}" data-jzd-type="code" data-jzd-code="010104" data-jzd-filter="" data-jzd-dm="xyr_zzfs_${status.index}" data-jzd-mc="xyr_zzfsmc_${status.index}" readonly  data-valid="isNonEmpty" data-error="不能为空"  >
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('xyr_zzfs_${status.index}');clearInput('xyr_zzfsmc_${status.index}');"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="xyr_zzsj_${status.index}">转账时间:</label>
                    <div class="form_controls">
                        <input type="text" value="<fmt:formatDate value="${obj.zzsj}" pattern="yyyy-MM-dd HH:mm:ss"/>" class="required form-control" id="xyr_zzsj_${status.index}" name="xyr[${status.index}].zzsj" onFocus="WdatePicker({lang:'zh-cn',isShowClear:false,dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'%y-%M-%d'})" data-valid="isNonEmpty" data-error="不能为空" >
                    </div>
                </div>

                <div class="form-group">
                    <label for="xyr_zzje_${status.index}">转账金额:</label>
                    <div class="form_controls">
                        <input type="text" value="${obj.zzje}" class="required form-control" id="xyr_zzje_${status.index}" name="xyr[${status.index}].zzje" data-valid="onlyNum" data-error="请输入有效金额" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="xyr_zfb_${status.index}">支付宝:</label>
                    <div class="form_controls">
                        <input type="text" value="${obj.zfb}" class="form-control" id="xyr_zfb_${status.index}" name="xyr[${status.index}].zfb" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="xyr_cft_${status.index}">财付通:</label>
                    <div class="form_controls">
                        <input type="text" value="${obj.cft}" class="form-control" id="xyr_cft_${status.index}" name="xyr[${status.index}].cft" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="xyr_wx_${status.index}">微信账号:</label>
                    <div class="form_controls">
                        <input type="text" value="${obj.wx}" class="form-control" id="xyr_wx_${status.index}" name="xyr[${status.index}].wx" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="xyr_dh_${status.index}">电话号码:</label>
                    <div class="form_controls">
                        <input type="text" value="${obj.dh}" class="form-control" id="xyr_dh_${status.index}" name="xyr[${status.index}].dh" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="xyr_jydh_${status.index}">交易单号:</label>
                    <div class="form_controls">
                        <input type="text" value="${obj.jydh}" class="form-control" id="xyr_jydh_${status.index}" name="xyr[${status.index}].jydh" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="xyr_qq_${status.index}">&nbsp;QQ号:</label>
                    <div class="form_controls">
                        <input type="text" value="${obj.qq}" class="form-control" id="xyr_qq_${status.index}" name="xyr[${status.index}].qq" >
                    </div>
                </div>
                <div class="form-group">
                    <i class="fa fa-minus-square fa-3x text-red" onclick="removeXyr(this);"></i>
                </div>
                <div style="clear: both;"></div>
            </div>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <div class="xyrData" data-bean-num="0" style="border-bottom: 1px #ccc dashed">
                <div class="form-group">
                    <label for="xyr_yhk_0">银行卡号:</label>
                    <div class="form_controls">
                        <input type="hidden" id="xyr_pk_0" name="xyr[0].pk" value="" />
                        <input type="text" class="required form-control" id="xyr_yhk_0" name="xyr[0].yhk" data-valid="onlyInt" data-error="请输入有效卡号" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="xyr_ckr_0">持卡人:</label>
                    <div class="form_controls">
                        <input type="text" class="form-control" id="xyr_ckr_0" name="xyr[0].ckr" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="xyr_sfzh_0">身份证:</label>
                    <div class="form_controls">
                        <input type="text" class="required form-control" id="xyr_sfzh_0" name="xyr[0].sfzh" data-valid="isCardNo" data-error="请输入有效身份证" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="xyr_zzfs_0">转账方式:</label>
                    <div class="has-feedback form_controls " >
                        <input type="hidden" id="xyr_zzfs_0" name="xyr[0].zzfs" />
                        <input type="text" class="required form-control" id="xyr_zzfsmc_0" data-jzd-type="code" data-jzd-code="010104" data-jzd-filter="" data-jzd-dm="xyr_zzfs_0" data-jzd-mc="xyr_zzfsmc_0" readonly  data-valid="isNonEmpty" data-error="不能为空" >
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('xyr_zzfs_0');clearInput('xyr_zzfsmc_0');"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="xyr_zzsj_0">转账时间:</label>
                    <div class="form_controls">
                        <input type="text" class="required form-control" id="xyr_zzsj_0" name="xyr[0].zzsj" onFocus="WdatePicker({lang:'zh-cn',isShowClear:false,dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'%y-%M-%d'})" data-valid="isNonEmpty" data-error="不能为空" >
                    </div>
                </div>

                <div class="form-group">
                    <label for="xyr_zzje_0">转账金额:</label>
                    <div class="form_controls">
                        <input type="text" class="required form-control" id="xyr_zzje_0" name="xyr[0].zzje" data-valid="onlyNum" data-error="请输入有效金额" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="xyr_zfb_0">支付宝:</label>
                    <div class="form_controls">
                        <input type="text" class="form-control" id="xyr_zfb_0" name="xyr[0].zfb" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="xyr_cft_0">财付通:</label>
                    <div class="form_controls">
                        <input type="text" class="form-control" id="xyr_cft_0" name="xyr[0].cft" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="xyr_wx_0">微信账号:</label>
                    <div class="form_controls">
                        <input type="text" class="form-control" id="xyr_wx_0" name="xyr[0].wx" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="xyr_dh_0">电话号码:</label>
                    <div class="form_controls">
                        <input type="text" class="form-control" id="xyr_dh_0" name="xyr[0].dh" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="xyr_jydh_0">交易单号:</label>
                    <div class="form_controls">
                        <input type="text" class="form-control" id="xyr_jydh_0" name="xyr[0].jydh" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="xyr_qq_0">&nbsp;QQ号:</label>
                    <div class="form_controls">
                        <input type="text" class="form-control" id="xyr_qq_0" name="xyr[0].qq" >
                    </div>
                </div>
                <div class="form-group">
                    <i class="fa fa-minus-square fa-3x text-red" onclick="removeXyr(this);"></i>
                </div>
                <div style="clear: both;"></div>
            </div>
        </c:otherwise>
    </c:choose>
                <div class="add_btn">
                    <i class="fa fa-plus-square fa-2x" onclick="addXyr();"></i>
                </div>
            </div>
            <div class="titDiv form-group" style="padding-top: 0px;margin-bottom: 0px">
                <h5>商户信息</h5>
            </div>
            <div class="borderDiv" style="clear: both">
        <c:choose>
            <c:when test="${shxxList!=null && shxxList.size()>0}">
                <c:forEach items="${shxxList}" var="obj" varStatus="status">
                    <div class="shxxData" data-bean-num="${status.index}" style="border-bottom: 1px #ccc dashed">
                        <div class="form-group">
                            <label for="shxx_shmc_${status.index}">商户名称:</label>
                            <div class="form_controls">
                                <input type="hidden" value="${obj.pk}" id="shxx_pk_${status.index}" name="shxx[${status.index}].pk"  />
                                <input type="text" value="${obj.shmc}" class="form-control" id="shxx_shmc_${status.index}" name="shxx[${status.index}].shmc"  >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shxx_lxfs_${status.index}">联系方式:</label>
                            <div class="form_controls">
                                <input type="text" value="${obj.lxfs}" class="form-control" id="shxx_lxfs_${status.index}" name="shxx[${status.index}].lxfs"  >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shxx_shh_${status.index}">商户号:</label>
                            <div class="form_controls">
                                <input type="text" value="${obj.shh}" class="form-control" id="shxx_shh_${status.index}" name="shxx[${status.index}].shh"  >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shxx_jydh_${status.index}">交易单号:</label>
                            <div class="form_controls">
                                <input type="text" value="${obj.jydh}" class="form-control" id="shxx_jydh_${status.index}" name="shxx[${status.index}].jydh"  >
                            </div>
                        </div>
                        <div class="form-group">
                            <i class="fa fa-minus-square fa-3x text-red" onclick="removeShxx(this);"></i>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <div class="shxxData" data-bean-num="0" style="border-bottom: 1px #ccc dashed">
                    <div class="form-group">
                        <label for="shxx_shmc_0">商户名称:</label>
                        <div class="form_controls">
                            <input type="hidden" id="shxx_pk_0" name="shxx[0].pk" value="" />
                            <input type="text" class="form-control" id="shxx_shmc_0" name="shxx[0].shmc"  >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="shxx_lxfs_0">联系方式:</label>
                        <div class="form_controls">
                            <input type="text" class="form-control" id="shxx_lxfs_0" name="shxx[0].lxfs"  >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="shxx_shh_0">商户号:</label>
                        <div class="form_controls">
                            <input type="text" class="form-control" id="shxx_shh_0" name="shxx[0].shh"  >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="shxx_jydh_0">交易单号:</label>
                        <div class="form_controls">
                            <input type="text" class="form-control" id="shxx_jydh_0" name="shxx[0].jydh"  >
                        </div>
                    </div>
                    <div class="form-group">
                        <i class="fa fa-minus-square fa-3x text-red" onclick="removeShxx(this);"></i>
                    </div>
                    <div style="clear: both;"></div>
                </div>
            </c:otherwise>
        </c:choose>
                <div class="add_btn">
                    <i class="fa fa-plus-square fa-2x" onclick="addShxx();"></i>
                </div>
            </div>
            <div class="titDiv form-group" style="padding-top: 0px;margin-bottom: 0px">
                <h5>其他信息</h5>
            </div>
            <div class="borderDiv" style="clear: both">
                <table class="table table-bordered" style="margin-top: 16px">
                    <tr>
                        <td style="width: 55px"><label for="sawz">涉案网址:</label></td>
                        <td colspan="3" >
                            <div class="form_controls" style="display: inline-block;width: 95%">
                                <input type="text" value="${jjd.sawz}" class="required form-control" id="sawz" name="sawz" data-valid="between:2-100" data-error="长度为2-100位" >
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <button type="button" style="float: right;margin-right: 4%;" class="btn btn-info" onclick="jjdEditSubmit();">提交</button>
            <div style="clear: both"></div>
        </div>
    </form>
            </ul>
        </div>
    </div>
    <!-- 字典、树形字典展示框。如果page、form均无字典项，可无需此div -->
    <div id="treeDiv">
        <input type="text" class="form-control" id="searchTree"  placeholder="支持文本检索">
        <div id="tree"></div>
    </div>
</section>
<script>
    var ctx = "${ctx}";
    var shrNum = parseInt("${shrList.size()}") - 1;
    if (!shrNum){
        shrNum = 0;
    }
    var xyrNum =parseInt("${xyrList.size()}") - 1;
    if (!xyrNum){
        xyrNum = 0;
    }
    var shxxNum =parseInt("${shxxList.size()}") - 1;
    if (!shxxNum){
        shxxNum = 0;
    }
</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/fzzx/jjd.js"></script>
</body>
</html>
