<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
    <style>
        .modal-show {
            width: 95%;
            margin: 30px auto;
        }
    </style>
</head>
<body>
<!-- 主体内容 -->
<section class="content">
	<!--查询项表单 begin-->
    <div class="row">
        <div class="col-xs-12">
            <ul class="box clearfix">
                <form class="form_chag form-inline" name="jjdQueryForm" id="jjdQueryForm">
                	<!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
                	<!-- 请根据需要修改 查询检索项 -->
                    	<div class="form-group">
	                        <label for="jjdhm">接警单号</label>
	                        <input type="text" class="form-control" id="jjdhm" name="jjdhm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ajbh">案件编号</label>
	                        <input type="text" class="form-control" id="ajbh" name="ajbh" >
	                    </div>
                    	<%--<div class="form-group">
	                        <label for="jjfsmc">接警方式</label>
                            <div class="dic_ipt">
                                <input type="hidden" id="jjfs" name="jjfs" />
                                <input type="text" class="form-control" id="jjfsmc" data-jzd-type="code" data-jzd-code="010102" data-jzd-filter="" data-jzd-dm="jjfs" data-jzd-mc="jjfsmc" readonly >
                                <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('jjfs');clearInput('jjfsmc');"></span>
                            </div>
	                    </div>
                    	<div class="form-group">
	                        <label for="ajlxmc">案件类型</label>
                            <div class="dic_ipt">
                                <input type="hidden" id="ajlx" name="ajlx" />
                                <input type="text" class="form-control" id="ajlxmc" data-jzd-type="code" data-jzd-code="010103" data-jzd-filter="" data-jzd-dm="ajlx" data-jzd-mc="ajlxmc" readonly >
                                <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('ajlx');clearInput('ajlxmc');"></span>
                            </div>
	                    </div>--%>
                        <div class="form-group">
                            <label for="barxm">报案人</label>
                            <input type="text" class="form-control" id="barxm" name="barxm" >
                        </div>
                    	<div class="form-group">
	                        <label for="ajlbmc">案件类别</label>
                            <div class="dic_ipt">
                                <input type="hidden" id="ajlb" name="ajlb" />
                                <input type="text" class="form-control" id="ajlbmc" data-jzd-type="tree" data-jzd-code="010107" data-jzd-filter="" data-jzd-dm="ajlb" data-jzd-mc="ajlbmc" readonly >
                                <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('ajlb');clearInput('ajlbmc');"></span>
                            </div>
	                    </div>
                    	<div class="form-group">
	                        <label for="afsj_begin">案发时间</label>
                            <input type="text" class="Wdate form-control" id="afsj_begin" name="afsj_begin" onFocus="WdatePicker({lang:'zh-cn',readOnly:true,dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'afsj_end\')||\'%y-%M-{%d}\'}'})" style="width: 30%!important" >
                            &nbsp;<input type="text" class="Wdate form-control" id="afsj_end" name="afsj_end" onFocus="WdatePicker({lang:'zh-cn',readOnly:true,dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'%y-%M-{%d}',minDate:'#F{$dp.$D(\'afsj_begin\')}'})" style="width: 29%!important" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sldwmc">受理单位</label>
                            <div class="dic_ipt">
                                <input type="hidden" id="sldw" name="sldw" />
                                <input type="text" class="form-control" id="sldwmc" data-jzd-type="tree" data-jzd-code="DEPT" data-jzd-filter="" data-jzd-dm="sldw" data-jzd-mc="sldwmc" readonly >
                                <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('sldw');clearInput('sldwmc');"></span>
                            </div>
	                    </div>
                        <div class="form-group">
                            <label for="jjsj_begin">接警时间</label>
                            <input type="text" class="Wdate form-control" id="jjsj_begin" name="jjsj_begin" onFocus="WdatePicker({lang:'zh-cn',readOnly:true,dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'jjsj_end\')||\'%y-%M-{%d}\'}'})" style="width: 30%!important" >
                            &nbsp;<input type="text" class="Wdate form-control" id="jjsj_end" name="jjsj_end" onFocus="WdatePicker({lang:'zh-cn',readOnly:true,dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'%y-%M-{%d}',minDate:'#F{$dp.$D(\'jjsj_begin\')}'})" style="width: 29%!important" >
                        </div>
                        <div class="form-group">
                            <label for="saje_begin">涉案金额</label>
                            <input type="text" class="form-control" id="saje_begin" name="saje_begin" style="width: 30%!important" >
                            &nbsp;<input type="text" class="form-control" id="saje_end" name="saje_end" style="width: 29%!important" >
                        </div>
                    <div class="form-group ">
                        <button type="button" style="margin-left: 8%" class="btn btn-info" onclick="search();">检索</button>
                    </div>
                </form>
            </ul>
        </div>
    </div>
	<!--查询项表单 end-->
	<!--结果展示 begin-->
	<div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="jjdGrid" class="table table-bordered table-striped table-condensed">

                    </table>
                    <!--toolbar -->
                    <div class="btn-group" id="toolbar">
                        <i class="btn" onclick="addJjd();">
                            <i class="fa fa-plus-square fa-2x"></i>
                        </i>
                        <i class="btn" onclick="exportJjd();">
                            <i class="fa fa-cloud-download fa-2x"></i>
                        </i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--结果展示 end-->
    <!-- 提示模态框（Modal） -->
    <div class="modal fade" id="tipsModal" tabindex="-1" role="dialog" aria-labelledby="tipsModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="tipsModalLabel">提示</h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="tipsMessage"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" id="tipsConfirmBtn">确定</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 详情模态框（Modal） -->
    <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="showModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="showMessage">
                    <jsp:include page="jjdShow.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 编辑模态框（Modal） -->
    <%--<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="editModalLabel"></h4>&lt;%&ndash;模态框（Modal）标题&ndash;%&gt;
                </div>
                <div class="modal-body" id="editMessage">
                    <jsp:include page="jjdForm.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" id="editConfirmBtn">提交</button>
                </div>
            </div>
        </div>
    </div>--%>
    
    <!-- 字典、树形字典展示框。如果page、form均无字典项，可无需此div -->
    <div id="treeDiv">
        <input type="text" class="form-control" id="searchTree"  placeholder="支持文本检索">
        <div id="tree"></div>
    </div>
</section>
<script>
    var ctx = "${ctx}";
    var fxfsPerm=false;
    <shiro:hasPermission name="ROLE_XTGL">
    fxfsPerm = true;
    </shiro:hasPermission>
</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-closeable-tab.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/fzzx/jjd.js"></script>
<script src="${ctx}/js/fzzx/jjdZjl.js"></script>
<%--<script>
    var date = new Date().initDateZeroTime();
    $("#afsj_begin").val(date);
</script>--%>

<script>
    $(function () {
        var h = $(".content").height();
    })
</script>
</body>
</html>