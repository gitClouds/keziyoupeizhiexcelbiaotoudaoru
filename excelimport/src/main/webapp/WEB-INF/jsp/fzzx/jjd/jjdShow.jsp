<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
	<style>
		.titDiv h5{
			font-weight: 700;font-size: 15px;
			color: #606c84;
			display: inline-block;
			margin-left: 10px;
		}
		.form_chag .form-group1{
			float: left;
			padding: 5px 0 5px 5px;
			width: 20%;
			padding-right: 2px;
			font-size: 12px;
		}
		.form_chag .form-group1 input{
			display: inline-block;
			width: 70%;
			border: 0px;
		}
		.borderDiv{
			border: 1px solid #ccc;
			margin: 0 5px 10px 5px;
		}
		.borderDiv_tb{
			border-left: 0px;
			border-right: 0px;
		}
	</style>
</head>
<body>
<!-- 主体内容 -->
	<!--查询项表单 begin-->
	<div class="row">
		<div class="col-xs-12">
			<ul class="box clearfix" style="border-top:0px;">
				<div class="form_chag" >
					<div class="borderDiv clearfix" style="padding: 5px 0;">
						<div class="form-group1">
							<label for="show_jjdhm">接警单号:</label>
							<input type="hidden" value="" id="show_pk" />
							<input type="text" value="" class="form-control" id="show_jjdhm" readonly />
						</div>
						<div class="form-group1">
							<label for="show_zfje">止付金额:</label>
							<input type="text" value="" class="form-control" id="show_zfje" readonly />
						</div>
						<div class="form-group1 ">
							<label for="show_jdxz">警单性质:</label>
							<input type="text" class="form-control" value="" id="show_jdxz" readonly />
						</div>
						<div class="form-group1 ">
							<label for="show_sldwmc">受理单位:</label>
							<input type="text" class="form-control" value="" id="show_sldwmc" readonly />
						</div>
						<div class="form-group1">
							<label for="show_jjsj">报警时间:</label>
							<input type="text" value="" class="form-control" id="show_jjsj" readonly />
						</div>
					</div>
					<div class="borderDiv clearfix">
						<div class="titDiv form-group1" style="padding-top: 0px;margin-bottom: 0px">
							<i class="fa fa-user fa-2x"></i><h5>报案人信息</h5>
						</div>
						<div style="clear: both"></div>
						<div class="form-group1">
							<label for="show_barxm">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;姓名:</label>
							<input type="text" value="" class="form-control" id="show_barxm" readonly />
						</div>
						<div class="form-group1">
							<label for="show_barsfzh">身份证号:</label>
							<input type="text" value="" class="form-control" id="show_barsfzh" readonly />
						</div>
						<div class="form-group1">
							<label for="show_barlxdh">联系电话:</label>
							<input type="text" value="" class="form-control" id="show_barlxdh" readonly />
						</div>
						<div class="form-group1">
							<label for="show_jjfs">接警方式:</label>
							<input type="text" value="" class="form-control" id="show_jjfs" readonly />
						</div>
					</div>
					<div class="borderDiv clearfix">
						<div class="titDiv form-group1" style="padding-top: 0px;margin-bottom: 0px">
							<i class="fa fa-file-text fa-2x"></i><h5>案件信息</h5>
						</div>
						<div style="clear: both"></div>
						<div class="form-group1">
							<label for="show_ajlx">案件类型:</label>
							<input type="text" value="" class="form-control" id="show_ajlx" readonly />
						</div>
						<div class="form-group1">
							<label for="show_ajlb">案件类别:</label>
								<input type="text" class="form-control" id="show_ajlb" readonly />
						</div>
						<div class="form-group1">
							<label for="show_afsj">案发时间:</label>
							<input type="text" class="form-control" id="show_afsj" readonly />
						</div>
						<div class="form-group1">
							<label for="show_saje">涉案金额:</label>
							<input type="text" value="" class="form-control" id="show_saje" readonly />
						</div>
						<div class="form-group1">
							<label for="show_afdd">案发地点:</label>
							<input type="text" value="" class="form-control" id="show_afdd" readonly />
						</div>
						<table class="table table-bordered" style="border: 0px;margin-bottom: 5px">
							<tr>
								<td style="width: 60px;font-size: 12px;padding: 0 0 0 5px;border: 0px;"><label for="show_jyaq">简要案情:</label></td>
								<td colspan="3" style="padding: 0px;border: 0px;">
									<div class="form_controls" style="display: inline-block;width: 98%">
										<textarea style="resize:none" rows="3" class="form-control" id="show_jyaq" ></textarea>
									</div>
								</td>
							</tr>
						</table>
						<table class="table table-bordered" style="border: 0px;margin-bottom: 5px">
							<tr>
								<td style="width: 60px;font-size: 12px;padding: 0 0 0 5px;border: 0px;text-align: right"><label for="show_bz">备注:</label></td>
								<td colspan="3" style="padding: 0px;border: 0px;">
									<div class="form_controls" style="display: inline-block;width: 98%">
										<textarea style="resize:none" rows="2" class="form-control" id="show_bz"></textarea>
									</div>
								</td>
							</tr>
						</table>
						<div class="borderDiv borderDiv_tb" >
							<div class="titDiv form-group1" style="padding-top: 0px;margin-bottom: 0px">
								<h5>受害人信息</h5>
							</div>
							<div style="clear: both"></div>
							<div class="shrData" data-bean-num="0" style="border-top: 1px #ccc dashed">
								<div class="form-group1">
									<label for="show_shr_yhk_0">银行卡号:</label>
									<input type="text" value="" class="form-control" id="show_shr_yhk_0" readonly />
								</div>
								<div class="form-group1">
									<label for="show_shr_ckr_0">持卡人:</label>
									<input type="text" class="form-control" id="show_shr_ckr_0" readonly >
								</div>
								<div class="form-group1">
									<label for="show_shr_sfzh_0">身份证:</label>
									<input type="text" class="form-control" id="show_shr_sfzh_0" readonly >
								</div>
								<div class="form-group1">
									<label for="show_shr_zzfs_0">资金流出:</label>
									<input type="text" class="form-control" id="show_shr_zzfs_0" readonly  >
								</div>
								<div class="form-group1">
									<label for="show_shr_zzsj_0">转账时间:</label>
									<input type="text" class="form-control" id="show_shr_zzsj_0"  readonly>
								</div>
								<div class="form-group1">
									<label for="show_shr_zzje_0">转账金额:</label>
									<input type="text" class="form-control" id="show_shr_zzje_0" readonly >
								</div>
								<div class="form-group1">
									<label for="show_shr_zfb_0">支付宝:</label>
									<input type="text" class="form-control" id="show_shr_zfb_0" readonly >
								</div>
								<div class="form-group1">
									<label for="show_shr_cft_0">财付通:</label>
									<input type="text" class="form-control" id="show_shr_cft_0" readonly>
								</div>
								<div class="form-group1">
									<label for="show_shr_wx_0">微信账号:</label>
									<input type="text" class="form-control" id="show_shr_wx_0" readonly >
								</div>
								<div class="form-group1">
									<label for="show_shr_dh_0">电话号码:</label>
									<input type="text" class="form-control" id="show_shr_dh_0" readonly >
								</div>
								<div class="form-group1">
									<label for="show_shr_jydh_0">交易单号:</label>
									<input type="text" class="form-control" id="show_shr_jydh_0" readonly >
								</div>
								<div class="form-group1">
									<label for="show_shr_qq_0">&nbsp;QQ号:</label>
									<input type="text" class="form-control" id="show_shr_qq_0" readonly >
								</div>
								<div style="clear: both"></div>
							</div>
						</div>

						<div class="borderDiv borderDiv_tb" >
							<div class="titDiv form-group1" style="padding-top: 0px;margin-bottom: 0px">
								<h5>嫌疑人信息</h5>
							</div>
							<div style="clear: both"></div>
							<div class="xyrData" data-bean-num="0" style="border-top: 1px #ccc dashed">
								<div class="form-group1">
									<label for="show_xyr_yhk_0">银行卡号:</label>
									<input type="text" value="" class="form-control" id="show_xyr_yhk_0" readonly />
								</div>
								<div class="form-group1">
									<label for="show_xyr_ckr_0">持卡人:</label>
									<input type="text" class="form-control" id="show_xyr_ckr_0" readonly >
								</div>
								<div class="form-group1">
									<label for="show_xyr_sfzh_0">身份证:</label>
									<input type="text" class="form-control" id="show_xyr_sfzh_0" readonly >
								</div>
								<div class="form-group1">
									<label for="show_xyr_zzfs_0">资金流出:</label>
									<input type="text" class="form-control" id="show_xyr_zzfs_0" readonly  >
								</div>
								<div class="form-group1">
									<label for="show_xyr_zzsj_0">转账时间:</label>
									<input type="text" class="form-control" id="show_xyr_zzsj_0"  readonly>
								</div>
								<div class="form-group1">
									<label for="show_xyr_zzje_0">转账金额:</label>
									<input type="text" class="form-control" id="show_xyr_zzje_0" readonly >
								</div>
								<div class="form-group1">
									<label for="show_xyr_zfb_0">支付宝:</label>
									<input type="text" class="form-control" id="show_xyr_zfb_0" readonly >
								</div>
								<div class="form-group1">
									<label for="show_xyr_cft_0">财付通:</label>
									<input type="text" class="form-control" id="show_xyr_cft_0" readonly>
								</div>
								<div class="form-group1">
									<label for="show_xyr_wx_0">微信账号:</label>
									<input type="text" class="form-control" id="show_xyr_wx_0" readonly >
								</div>
								<div class="form-group1">
									<label for="show_xyr_dh_0">电话号码:</label>
									<input type="text" class="form-control" id="show_xyr_dh_0" readonly >
								</div>
								<div class="form-group1">
									<label for="show_xyr_jydh_0">交易单号:</label>
									<input type="text" class="form-control" id="show_xyr_jydh_0" readonly >
								</div>
								<div class="form-group1">
									<label for="show_xyr_qq_0">&nbsp;QQ号:</label>
									<input type="text" class="form-control" id="show_xyr_qq_0" readonly >
								</div>
								<div style="clear: both"></div>
							</div>
						</div>

						<div class="borderDiv borderDiv_tb" >
							<div class="titDiv form-group1" style="padding-top: 0px;margin-bottom: 0px">
								<h5>商户信息</h5>
							</div>
							<div style="clear: both"></div>
							<div class="shxxData" data-bean-num="0" style="border-top: 1px #ccc dashed">
								<div class="form-group1">
									<label for="show_shxx_shmc_0">商户名称:</label>
									<input type="text" value="" class="form-control" id="show_shxx_shmc_0" readonly />
								</div>
								<div class="form-group1">
									<label for="show_shxx_lxfs_0">联系方式:</label>
									<input type="text" class="form-control" id="show_shxx_lxfs_0" readonly >
								</div>
								<div class="form-group1">
									<label for="show_shxx_shh_0">商户号:</label>
									<input type="text" class="form-control" id="show_shxx_shh_0" readonly >
								</div>
								<div class="form-group1">
									<label for="show_shxx_jydh_0">交易单号:</label>
									<input type="text" class="form-control" id="show_shxx_jydh_0" readonly  >
								</div>
								<div style="clear: both"></div>
							</div>
						</div>
						<div class="borderDiv borderDiv_tb">
							<div class="titDiv form-group1" style="padding-top: 0px;margin-bottom: 0px;">
								<h5 style="font-size: 14px;font-weight: 700;color: #000;">信息选择</h5>

							</div>
							<div style="clear: both"></div>
							<div style="border-top: 1px #ccc dashed;padding: 10px">
								<label class="radio-inline">
								<input type="radio" name="xxType" value="shr" />受害人信息
								</label>
								<label class="radio-inline">
								<input type="radio" name="xxType" value="xyr" />嫌疑人信息
								</label>
							</div>
						</div>
						<div class="borderDiv borderDiv_tb">
							<div class="titDiv form-group1" style="padding-top: 0px;margin-bottom: 0px;">
								<h5 style="font-size: 14px;font-weight: 700;color: #000;">信息操作</h5>
							</div>
							<div style="clear: both"></div>
							<div style="border-top: 1px #ccc dashed;padding: 10px">
								<jsp:include page="../../znzf/znzf_req.jsp"/>
							</div>
						</div>

						<div class="borderDiv borderDiv_tb">
							<div class="titDiv form-group1" style="padding-top: 0px;margin-bottom: 0px">
								<h5>其他信息</h5>
							</div>
							<div style="clear: both"></div>
							<div style="border-top: 1px #ccc dashed">
								<div class="form-group1" style="width: 100%">
									<label for="show_sawz">涉案网址:</label>
									<input type="text" value="" class="form-control" id="show_sawz" readonly />
								</div>
								<div style="clear: both"></div>
							</div>
						</div>
					</div>
				</div>
			</ul>
		</div>
	</div>
<script>
    var shrNum =  0;
    var xyrNum = 0;
    var shxxNum = 0;
</script>
</body>
</html>
