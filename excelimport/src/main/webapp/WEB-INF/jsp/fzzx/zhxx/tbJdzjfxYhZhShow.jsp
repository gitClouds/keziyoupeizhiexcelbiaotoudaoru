<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">警情银行卡ID</td>
            <td id="show_id"></td>
    		<td class="sec_tit">银行账号</td>
            <td id="show_zh"></td>
		</tr>
		<tr>
    		<td class="sec_tit">户主姓名</td>
            <td id="show_xm"></td>
    		<td class="sec_tit">户主证件类型</td>
            <td id="show_zjlx"></td>
		</tr>
		<tr>
    		<td class="sec_tit">户主的证件号码</td>
            <td id="show_zjhm"></td>
    		<td class="sec_tit">账号所属银行名称</td>
            <td id="show_jgmc"></td>
		</tr>
		<tr>
    		<td class="sec_tit">账户所属银行代码</td>
            <td id="show_jgdm"></td>
    		<td class="sec_tit">转账时间</td>
            <td id="show_zzsj"></td>
		</tr>
		<tr>
    		<td class="sec_tit">转账金额</td>
            <td id="show_zzje"></td>
    		<td class="sec_tit">录入人</td>
            <td id="show_lrr"></td>
		</tr>
		<tr>
    		<td class="sec_tit">录入时间</td>
            <td id="show_lrsj"></td>
    		<td class="sec_tit">所属关系，1：受害人，2：嫌疑人</td>
            <td id="show_ssgx"></td>
		</tr>
		<tr>
    		<td class="sec_tit">账户类型 01-个人 02：对公</td>
            <td id="show_zhlb"></td>
    		<td class="sec_tit">录入人姓名</td>
            <td id="show_lrrxm"></td>
		</tr>
		<tr>
    		<td class="sec_tit">录入人单位</td>
            <td id="show_lrdw"></td>
    		<td class="sec_tit">录入单位名称</td>
            <td id="show_lrrdwmc"></td>
		</tr>
		<tr>
    		<td class="sec_tit">修改人姓名</td>
            <td id="show_xgrxm"></td>
    		<td class="sec_tit">修改人</td>
            <td id="show_xgr"></td>
		</tr>
		<tr>
    		<td class="sec_tit">修改人单位代码</td>
            <td id="show_xgrdwdm"></td>
    		<td class="sec_tit">修改人单位名称</td>
            <td id="show_xgrdwmc"></td>
		</tr>
		<tr>
    		<td class="sec_tit">修改时间</td>
            <td id="show_xgsj"></td>
    		<td class="sec_tit">有效性</td>
            <td id="show_yxx"></td>
		</tr>
		<tr>
    		<td class="sec_tit">转账方式</td>
            <td id="show_zzfs"></td>
    		<td class="sec_tit">是否已止付 0：未止付；1：已止付</td>
            <td id="show_sfyzf"></td>
		</tr>
		<tr>
    		<td class="sec_tit">是否已冻结 0：未冻结； 1：已冻结</td>
            <td id="show_sfydj"></td>
    		<td class="sec_tit">余额</td>
            <td id="show_ye"></td>
		</tr>
		<tr>
    		<td class="sec_tit">层级</td>
            <td id="show_cj"></td>
    		<td class="sec_tit">是否为延伸卡号 0：否； 1：是</td>
            <td id="show_sfys"></td>
		</tr>
		<tr>
    		<td class="sec_tit">是否资金返还 0：否 1：是</td>
            <td id="show_zjfh"></td>
    		<td class="sec_tit">账号类型：1：银行卡 3：pos机</td>
            <td id="show_zhlx"></td>
		</tr>
		<tr>
    		<td class="sec_tit">返还金额</td>
            <td id="show_fhje"></td>
    		<td class="sec_tit">是否下发</td>
            <td id="show_sfxf"></td>
		</tr>
		<tr>
    		<td class="sec_tit">实际返还金额</td>
            <td id="show_sfje"></td>
    		<td class="sec_tit">资金返还状态 0：未下发 1：已下发 2：已完成</td>
            <td id="show_zjfhzt"></td>
		</tr>
		<tr>
    		<td class="sec_tit">字段注释</td>
            <td id="show_yhksqbh"></td>
    		<td class="sec_tit">字段注释</td>
            <td id="show_kszfbs"></td>
		</tr>
		<tr>
    		<td class="sec_tit">字段注释</td>
            <td id="show_jjbh"></td>
		</tr>
    </table>
</body>
</html>
