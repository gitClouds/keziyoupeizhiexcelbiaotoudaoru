<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
    <link rel="stylesheet" href="${ctx}/css/pub/showBase.css">
</head>
<body>
<section class="content">
	<div><h3>历史明细数据</h3></div>
	<div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
					<table id="sfMxJgLstGrid" class="table table-bordered table-striped table-condensed">
					</table>
					<!-- <div class="btn-group" id="toolbar">
					    <i class="btn" onclick="exportSfMxJgLst();" title="明细导出">
					        <i class="fa fa-cloud-download fa-2x"></i>
					    </i>
					</div> -->
                </div>
            </div>
        </div>
    </div>
	<c:if test="${ismxqq == 'y'}">
    <div><h3>最新明细查询结果</h3></div>
	<div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
					<div class="banner_box" >
					    <div class="banner_table_box">
					        <table class="show_tab" cellspacing=0; cellpadding=0; border=1; style="margin-top: 12px">
					            <tr>
					                <td class="tab_title">是否反馈</td>
					                <td style="width: 18%">
					                    <c:choose>
					                        <c:when test="${mxqq.resflag==0}">
					                            未反馈
					                        </c:when>
					                        <c:when test="${mxqq.resflag==1}">
					                            已反馈
					                        </c:when>
					                        <c:when test="${mxqq.resflag==2}">
					                            失败反馈
					                        </c:when>
					                    </c:choose>
					                </td>
					                <td class="tab_title">三方机构</td>
					                <td style="width: 18%">${mxqq.payname}</td>
					                <td class="tab_title">账户类型</td>
					                <td style="width: 18%">${dicMap["010302"][mxjg.accounttype]}</td>
					            </tr>
					            <tr>
					                <td class="tab_title">账/卡号</td>
					                <td>${mxqq.accnumber}</td>
					                <td class="tab_title">账户状态</td>
					                <td>${dicMap["010303"][mxjg.accountstatus]}</td>
					                <td class="tab_title">账户描述</td>
					                <td>${mxjg.accountinfo}</td>
					            </tr>
					            <tr>
					                <td class="tab_title">查询起始时间</td>
					                <td><fmt:formatDate value="${mxqq.starttime}" pattern="yyyy-MM-dd"/></td>
					                <td class="tab_title">查询截止时间</td>
					                <td><fmt:formatDate value="${mxqq.expiretime}" pattern="yyyy-MM-dd"/></td>
					                <td class="tab_title">最后交易时间</td>
					                <td><fmt:formatDate value="${mxjg.lasttransactiontime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					            </tr>
					            <tr>
					                <td class="tab_title">开户IP</td>
					                <td>${mxjg.accountopenip}</td>
					                <td class="tab_title">开户设备MAC</td>
					                <td>${mxjg.accountopenmac}</td>
					                <td class="tab_title">开户时间</td>
					                <td><fmt:formatDate value="${mxjg.accountopentime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					            </tr>
					            <tr>
					                <td class="tab_title">账户余额</td>
					                <td>${mxjg.accountbalance}</td>
					                <td class="tab_title">经办人姓名</td>
					                <td>${mxjg.operatorname}</td>
					                <td class="tab_title">经办人电话</td>
					                <td>${mxjg.operatorphonenumber}</td>
					            </tr>
					            <tr>
					                <td class="tab_title">反馈机构</td>
					                <td>${mxjg.feedbackorgname}</td>
					                <td class="tab_title">反馈结果</td>
					                <c:choose>
					                	<c:when test="${mxqq.feedbackremark == '' || mxqq.feedbackremark == '-' || mxqq.feedbackremark == null }">
					               			<td>${dicMap["010301"][mxqq.resultcode]}</td> 	
					                	</c:when>
					                	<c:otherwise>
					                		<td>${mxqq.feedbackremark}</td>
					                	</c:otherwise>
					                </c:choose>
					                <td class="tab_title">反馈说明</td>
					                <td>${mxjg.feedbackremark}</td>
					            </tr>
					            <tr>
					                <td class="tab_title">法律文书</td>
					                <td colspan="5">
					                    <c:choose>
					                    <c:when test="${mxqq.flws==null || mxqq.flws==''}">
					
					                    </c:when>
					                    <c:otherwise>
					                        <div class="btn-group">
					                            <i class="btn" onclick="fileUpload.fileDownLoad(this)" data-key="${ctx}/attachment/download?pk=${mxqq.flws}">
					                                <i class="fa fa-download fa-lg"></i>
					                            </i>
					                        </div>
					                    </c:otherwise>
					                    </c:choose>
					                </td>
					            </tr>
					        </table>
					    </div>
					</div>
                </div>
            </div>
        </div>
    </div>
	</c:if>
	<c:if test="${isqzhqq == 'y'}">
	<div><h3>全账户查询结果</h3></div>
	<div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                	<div class="banner_box" >
					    <div class="banner_table_box">
					        <table class="show_tab" cellspacing=0; cellpadding=0; border=1; style="margin-top: 12px">
					            <tr>
					                <td class="tab_title">是否反馈</td>
					                <td style="width: 18%">
					                    <c:choose>
					                        <c:when test="${qzhqq.resflag==0}">
					                            未反馈
					                        </c:when>
					                        <c:when test="${qzhqq.resflag==1}">
					                            已反馈
					                        </c:when>
					                        <c:when test="${qzhqq.resflag==2}">
					                            失败反馈
					                        </c:when>
					                    </c:choose>
					                </td>
					                <td class="tab_title">三方机构</td>
					                <td style="width: 18%">${qzhqq.payname}</td>
					                <td class="tab_title">账号类型</td>
					                <td style="width: 18%">${dicMap["010105"][qzhqq.acctype]}</td>
					            </tr>
					            <tr>
					                <td class="tab_title">账号</td>
					                <td>${qzhqq.accnumber}</td>
					                <td class="tab_title">反馈机构</td>
					                <td>${qzhjg.feedbackorgname}</td>
					                <td class="tab_title">反馈结果</td>
					                <c:choose>
					                	<c:when test="${qzhqq.feedbackremark == '' || qzhqq.feedbackremark == '-' || qzhqq.feedbackremark == null }">
					               			<td>${dicMap["010301"][qzhqq.resultcode]}</td> 	
					                	</c:when>
					                	<c:otherwise>
					                		<td>${qzhqq.feedbackremark}</td>
					                	</c:otherwise>
					                </c:choose>
					             </tr>
					            <tr>
					                <td class="tab_title">反馈说明</td>
					                <td>${qzhjg.feedbackremark}</td>
					                <td class="tab_title">法律文书</td>
					                <td>
					                <c:choose>
					                    <c:when test="${qzhqq.flws==null || qzhqq.flws==''}">
					
					                    </c:when>
					                    <c:otherwise>
					                    <div class="btn-group">
					                        <i class="btn" onclick="fileUpload.fileDownLoad(this)" data-key="${ctx}/attachment/download?pk=${qzhqq.flws}">
					                            <i class="fa fa-download fa-lg"></i>
					                        </i>
					                    </div>
					                    </c:otherwise>
					                </c:choose>
					                </td>
					            </tr>
					        </table>
					    </div>
					</div>
					 <table id="sfJgActGrid" class="table table-bordered table-striped table-condensed">

					</table>
    			</div>
    		</div>
    	</div>
    </div>
    </c:if>
</section>
<script>
var ctx = "${ctx}";
var id = "${id}"
var dic = ${dict}
</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/resources/common/fileUpload.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/fzzx/zhxx/sfMxLst.js"></script>
<script src="${ctx}/js/fzzx/zhxx/sfJgAct.js"></script>

</body>
</html>