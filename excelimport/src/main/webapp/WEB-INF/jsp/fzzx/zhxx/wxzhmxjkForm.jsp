<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <form class="form_chag" id="tbJdzjfxSfZhEditForm" name="tbJdzjfxSfZhEditForm">
        <input type="hidden" id="edit_curdType" name="curdType" >
        <input type="hidden" id="edit_id" name="id">
        <input type="hidden" id="edit_lx" name="lx" value="1">
        <input type="hidden" id="edit_sjly" name="sjly" value="1">
        <table class="table table-bordered">
        	<!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
            <!-- 请根据需要修改 可编辑项、hidden等 -->
            <!-- 表单校验的字段需要在input外加一层 <div class="form_controls"> -->
            <!-- 表单校验的input classes + required -->
            <!-- 表单校验的input data-tip="" 为进入时提醒 -->
            <!-- 表单校验的input data-valid="" 为验证规则，多个可使用‘||’分割；具体参照jquery-validate.js-->
            <!-- 表单校验的input data-error="" 为验证不通过时的提醒，需与 data-valid一一对应 -->
            <!-- 校验需自己填写 -->
			<tr>
        		<td class="sec_tit">
                    <label for="edit_zh">账号</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="form-control" id="edit_zh" name="zh" data-tip="" data-valid="isNonEmpty" data-error="不能为空" onblur="valiZh()" >
                    </div>
                </td>
                <td class="sec_tit">
                <label for="edit_zjhm">身份证号</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" class="form-control" id="edit_zjhm" name="zjhm" onblur="valiZjhm()">
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="jgdm">账号所属机构名称<span style="color: red;">*</span></label>
                </td>
                <td>
                    <div class="has-feedback form_controls " >
                        <input type="hidden"  id="jgdm" name="jgdm" />
                        <input type="text"  class="required form-control" id="jgmc" name="jgmc" data-jzd-type="code" data-jzd-code="542" data-jzd-filter="" data-jzd-dm="jgdm" data-jzd-mc="jgmc" readonly  data-valid="isNonEmpty" data-error="不能为空" onblur="valiZhZjhm()">
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('jgdm');clearInput('jgmc');"></span>
                    </div>
                </td>
                 <td class="sec_tit">
                    <label for="edit_lxfs">短信发送电话号码</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" class="form-control" id="edit_lxfs" name="lxfs" placeholder ="格式例如：11111111111,11111111111（英文逗号）">
                    </div>
                </td>
			</tr>
			<tr>
                 <td class="sec_tit">
                    <label for="edit_jjbh">备注</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" class="form-control" id="edit_jjbh" name="jjbh" data-valid="isNonEmpty" >
                    </div>
                </td>
			</tr>
        </table>
    </form>
</body>
</html>
