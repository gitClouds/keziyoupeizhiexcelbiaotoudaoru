<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
</head>
<body>
<!-- 主体内容 -->
<section class="content">
	<!--查询项表单 begin-->
    <div class="row">
        <div class="col-xs-12">
            <ul class="box clearfix">
                <form class="form_chag form-inline" name="tbJdzjfxYhZhQueryForm" id="tbJdzjfxYhZhQueryForm">
                	<!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
                	<!-- 请根据需要修改 查询检索项 -->
                    	<div class="form-group">
	                        <label for="id">警情银行卡ID</label>
	                        <input type="text" class="form-control" id="id" name="id" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zh">银行账号</label>
	                        <input type="text" class="form-control" id="zh" name="zh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="xm">户主姓名</label>
	                        <input type="text" class="form-control" id="xm" name="xm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zjlx">户主证件类型</label>
	                        <input type="text" class="form-control" id="zjlx" name="zjlx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zjhm">户主的证件号码</label>
	                        <input type="text" class="form-control" id="zjhm" name="zjhm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="jgmc">账号所属银行名称</label>
	                        <input type="text" class="form-control" id="jgmc" name="jgmc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="jgdm">账户所属银行代码</label>
	                        <input type="text" class="form-control" id="jgdm" name="jgdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zzsj">转账时间</label>
	                        <input type="text" class="form-control" id="zzsj" name="zzsj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zzje">转账金额</label>
	                        <input type="text" class="form-control" id="zzje" name="zzje" >
	                    </div>
                    	<div class="form-group">
	                        <label for="lrr">录入人</label>
	                        <input type="text" class="form-control" id="lrr" name="lrr" >
	                    </div>
                    	<div class="form-group">
	                        <label for="lrsj">录入时间</label>
	                        <input type="text" class="form-control" id="lrsj" name="lrsj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ssgx">所属关系，1：受害人，2：嫌疑人</label>
	                        <input type="text" class="form-control" id="ssgx" name="ssgx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zhlb">账户类型 01-个人 02：对公</label>
	                        <input type="text" class="form-control" id="zhlb" name="zhlb" >
	                    </div>
                    	<div class="form-group">
	                        <label for="lrrxm">录入人姓名</label>
	                        <input type="text" class="form-control" id="lrrxm" name="lrrxm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="lrdw">录入人单位</label>
	                        <input type="text" class="form-control" id="lrdw" name="lrdw" >
	                    </div>
                    	<div class="form-group">
	                        <label for="lrrdwmc">录入单位名称</label>
	                        <input type="text" class="form-control" id="lrrdwmc" name="lrrdwmc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="xgrxm">修改人姓名</label>
	                        <input type="text" class="form-control" id="xgrxm" name="xgrxm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="xgr">修改人</label>
	                        <input type="text" class="form-control" id="xgr" name="xgr" >
	                    </div>
                    	<div class="form-group">
	                        <label for="xgrdwdm">修改人单位代码</label>
	                        <input type="text" class="form-control" id="xgrdwdm" name="xgrdwdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="xgrdwmc">修改人单位名称</label>
	                        <input type="text" class="form-control" id="xgrdwmc" name="xgrdwmc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="xgsj">修改时间</label>
	                        <input type="text" class="form-control" id="xgsj" name="xgsj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="yxx">有效性</label>
	                        <input type="text" class="form-control" id="yxx" name="yxx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zzfs">转账方式</label>
	                        <input type="text" class="form-control" id="zzfs" name="zzfs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sfyzf">是否已止付 0：未止付；1：已止付</label>
	                        <input type="text" class="form-control" id="sfyzf" name="sfyzf" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sfydj">是否已冻结 0：未冻结； 1：已冻结</label>
	                        <input type="text" class="form-control" id="sfydj" name="sfydj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ye">余额</label>
	                        <input type="text" class="form-control" id="ye" name="ye" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cj">层级</label>
	                        <input type="text" class="form-control" id="cj" name="cj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sfys">是否为延伸卡号 0：否； 1：是</label>
	                        <input type="text" class="form-control" id="sfys" name="sfys" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zjfh">是否资金返还 0：否 1：是</label>
	                        <input type="text" class="form-control" id="zjfh" name="zjfh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zhlx">账号类型：1：银行卡 3：pos机</label>
	                        <input type="text" class="form-control" id="zhlx" name="zhlx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="fhje">返还金额</label>
	                        <input type="text" class="form-control" id="fhje" name="fhje" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sfxf">是否下发</label>
	                        <input type="text" class="form-control" id="sfxf" name="sfxf" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sfje">实际返还金额</label>
	                        <input type="text" class="form-control" id="sfje" name="sfje" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zjfhzt">资金返还状态 0：未下发 1：已下发 2：已完成</label>
	                        <input type="text" class="form-control" id="zjfhzt" name="zjfhzt" >
	                    </div>
                    	<div class="form-group">
	                        <label for="yhksqbh">字段</label>
	                        <input type="text" class="form-control" id="yhksqbh" name="yhksqbh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="kszfbs">字段</label>
	                        <input type="text" class="form-control" id="kszfbs" name="kszfbs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="jjbh">字段</label>
	                        <input type="text" class="form-control" id="jjbh" name="jjbh" >
	                    </div>
                                        
                    <div class="form-group ">
                        <button type="button" style="margin-left: 8%" class="btn btn-info" onclick="search();">检索</button>
                    </div>
                </form>
            </ul>
        </div>
    </div>
	<!--查询项表单 end-->
	<!--结果展示 begin-->
	<div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="tbJdzjfxYhZhGrid" class="table table-bordered table-striped table-condensed">

                    </table>
                    <!--toolbar -->
                    <div class="btn-group" id="toolbar">
                        <i class="btn" onclick="addTbJdzjfxYhZh();">
                            <i class="fa fa-plus-square fa-2x"></i>
                        </i>
                        <i class="btn" onclick="exportTbJdzjfxYhZh();">
                            <i class="fa fa-cloud-download fa-2x"></i>
                        </i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--结果展示 end-->
    <!-- 提示模态框（Modal） -->
    <div class="modal fade" id="tipsModal" tabindex="-1" role="dialog" aria-labelledby="tipsModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="tipsModalLabel">提示</h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="tipsMessage"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" id="tipsConfirmBtn">确定</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 详情模态框（Modal） -->
    <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="showModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="showMessage">
                    <jsp:include page="tbJdzjfxYhZhShow.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 编辑模态框（Modal） -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="editModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="editMessage">
                    <jsp:include page="tbJdzjfxYhZhForm.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" id="editConfirmBtn">保存</button>
                </div>
            </div>
        </div>
    </div>
    
    <!-- 字典、树形字典展示框。如果page、form均无字典项，可无需此div -->
    <div id="treeDiv">
        <input type="text" class="form-control" id="searchTree"  placeholder="支持文本检索">
        <div id="tree"></div>
    </div>
</section>
<script>
    var ctx = "${ctx}";
</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/fzzx/zhxx/tbJdzjfxYhZh.js"></script>
</body>
</html>