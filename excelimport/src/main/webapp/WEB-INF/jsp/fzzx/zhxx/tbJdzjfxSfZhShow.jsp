<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">账号</td>
            <td id="show_zh"></td>
			<td class="sec_tit">户主姓名</td>
			<td id="show_xm"></td>
		</tr>
		<tr>
			<td class="sec_tit">证件类型</td>
			<td id="show_zjlx"></td>
			<td class="sec_tit">证件号码</td>
			<td id="show_zjhm"></td>
		</tr>
		<tr>
    		<td class="sec_tit">账号所属机构名称</td>
            <td id="show_jgmc"></td>
			<td class="sec_tit">余额</td>
			<td id="show_ye"></td>
		</tr>
		<tr>
    		<td class="sec_tit">转账时间</td>
            <td id="show_zzsj"></td>
			<td class="sec_tit">转账金额</td>
			<td id="show_zzje"></td>
		</tr>
		<tr>
    		<td class="sec_tit">录入人</td>
            <td id="show_lrr"></td>
			<td class="sec_tit">录入时间</td>
			<td id="show_lrsj"></td>
		</tr>
		<tr>
    		<td class="sec_tit">录入人姓名</td>
            <td id="show_lrrxm"></td>
			<td class="sec_tit">录入单位名称</td>
			<td id="show_lrrdwmc"></td>
		</tr>
		<tr>
    		<td class="sec_tit">修改人</td>
            <td id="show_xgr"></td>
			<td class="sec_tit">修改人单位名称</td>
			<td id="show_xgrdwmc"></td>
		</tr>
		<tr>
			<td class="sec_tit">修改时间</td>
			<td id="show_xgsj"></td>
			<td class="sec_tit">联系方式</td>
			<td id="show_lxfs"></td>
		</tr>
		<tr>
			<td class="sec_tit">账户类别</td>
			<td id="show_zhlbmc"></td>
			<td class="sec_tit">人员类型</td>
			<td id="show_rylbmc"></td>
		</tr>
    </table>
</body>
</html>
