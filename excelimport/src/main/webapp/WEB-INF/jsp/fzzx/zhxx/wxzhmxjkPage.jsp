<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <%--<link rel="stylesheet" href="${ctx}/resources/bootstrap/fileinput/css/fileinput.min.css">--%>
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
    <%--<link rel="stylesheet" href="${ctx}/css/pub/form.css">--%>
</head>
<body>
<!-- 主体内容 -->
<section class="content">
	<!--查询项表单 begin-->
    <div class="row">
        <div class="col-xs-12">
            <ul class="box clearfix">
                <form class="form_chag form-inline" name="tbJdzjfxSfZhQueryForm" id="tbJdzjfxSfZhQueryForm">
                    <input type="hidden" id="lx" name="lx" value="1">
                	<!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
                	<!-- 请根据需要修改 查询检索项 -->
                    	<div class="form-group">
	                        <label for="zh">微信账号</label>
	                        <input type="text" class="form-control" id="zh" name="zh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zjhm">身份证号</label>
	                        <input type="text" class="form-control" id="zjhm" name="zjhm" >
	                    </div>
                    <div class="form-group ">
                        <button type="button" style="margin-left: 8%" class="btn btn-info" onclick="search();">检索</button>
                    </div>
                </form>
            </ul>
        </div>
    </div>
	<!--查询项表单 end-->
	<!--结果展示 begin-->
    
	<div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="tbJdzjfxSfZhGrid" class="table table-bordered table-striped table-condensed">

                    </table>
                    <!--toolbar -->
                    <div class="btn-group" id="toolbar">
                        <i class="btn" id="add_i" onclick="addTbJdzjfxSfZh();">
                            <i class="fa fa-plus-square fa-2x"><span id="add" style="font-size: 20px"></span></i>
                        </i>
                       <!--  <i class="btn" id="exp_i" onclick="exportTbJdzjfxSfZh();">
                            <i class="fa fa-cloud-download fa-2x"><span id="exp" style="font-size: 20px"></span></i>
                        </i>
                        <i class="btn" id="imp_i" onclick="importTbJdzjfxSfZh();">
                            <i class="fa fa-upload fa-2x"><span id="imp" style="font-size: 20px"></span></i>
                        </i>
                        <i class="btn" id="template_i" onclick="exportTbJdzjfxSfZhTemplate();">
                            <i class="fa fa-download fa-2x"><span id="templageDownload" style="font-size: 20px"></span></i>
                        </i>
                        <i class="btn" id="templateyh_i" style="display: none;" onclick="exportTbJdzjfxYhZhTemplate();">
                            <i class="fa fa-download  fa-2x"><span id="templageYhDownload" style="font-size: 20px"></span></i>
                        </i>
                        <i class="btn" id="plfx_i"   onclick="plFx();">
                            <i class="fa fa fa-check-circle-o  fa-2x"><span id="plfx" style="font-size: 20px"></span></i>
                        </i> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--结果展示 end-->
    <!-- 提示模态框（Modal） -->
    <div class="modal fade" id="tipsModal" tabindex="-1" role="dialog" aria-labelledby="tipsModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="tipsModalLabel">提示</h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="tipsMessage"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" id="tipsConfirmBtn">确定</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 详情模态框（Modal） -->
    <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="showModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="showMessage">
                    <jsp:include page="tbJdzjfxSfZhShow.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 编辑模态框（Modal） -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="editModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="editMessage">
                    <jsp:include page="wxzhmxjkForm.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" id="editConfirmBtn">保存</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 文件上传模态框（Modal） -->
    <div class="modal fade" id="fileUploadModal" tabindex="-1" role="dialog" aria-labelledby="fileUploadModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="fileUploadModalLabel">提示</h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="fileUploadMessage">
                    <jsp:include page="form.jsp"/>
                    <div class="progress progress-striped active" id="process" style="display: none;">
                        <div class="progress-bar progress-bar-success" role="progressbar"
                             aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
                             style="width: 0%;">
                            <span class="sr-only">0% 完成</span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" id="fileUploadConfirmBtn">提交</button>
                </div>
            </div>
        </div>
    </div>
    <%--<div class="modal fade" id="fileUploadModal" tabindex="-1" role="dialog" aria-labelledby="fileUploadModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="fileUploadModalLabel"></h4>&lt;%&ndash;模态框（Modal）标题&ndash;%&gt;
                </div>
                <div class="modal-body" id="fileUploadMessage">
                    <jsp:include page="form.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" id="fileUploadConfirmBtn">提交</button>
                </div>
            </div>
        </div>
    </div>--%>
    <!-- 字典、树形字典展示框。如果page、form均无字典项，可无需此div -->
    <div id="treeDiv">
        <input type="text" class="form-control" id="searchTree"  placeholder="支持文本检索">
        <div id="tree"></div>
    </div>
</section>
<script>
    var ctx = "${ctx}";
    var username = "${username}";
</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/bootstrap/fileinput/js/fileinput.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-closeable-tab.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/resources/common/fileUpload.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/fzzx/zhxx/wxzhmxjk.js"></script>
</body>
</html>