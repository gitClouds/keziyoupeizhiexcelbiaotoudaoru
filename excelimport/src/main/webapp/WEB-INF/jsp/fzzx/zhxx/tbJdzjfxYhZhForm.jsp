<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <form class="form_chag" id="tbJdzjfxYhZhEditForm" name="tbJdzjfxYhZhEditForm">
        <input type="hidden" id="edit_curdType" name="curdType" >
        <table class="table table-bordered">
        	<!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
            <!-- 请根据需要修改 可编辑项、hidden等 -->
            <!-- 表单校验的字段需要在input外加一层 <div class="form_controls"> -->
            <!-- 表单校验的input classes + required -->
            <!-- 表单校验的input data-tip="" 为进入时提醒 -->
            <!-- 表单校验的input data-valid="" 为验证规则，多个可使用‘||’分割；具体参照jquery-validate.js-->
            <!-- 表单校验的input data-error="" 为验证不通过时的提醒，需与 data-valid一一对应 -->
            <!-- 校验需自己填写 -->
			<tr>
        		<td class="sec_tit">
                    <label for="edit_id">警情银行卡ID</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_id" name="id" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_zh">银行账号</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_zh" name="zh" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_xm">户主姓名</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_xm" name="xm" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_zjlx">户主证件类型</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_zjlx" name="zjlx" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_zjhm">户主的证件号码</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_zjhm" name="zjhm" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_jgmc">账号所属银行名称</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_jgmc" name="jgmc" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_jgdm">账户所属银行代码</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_jgdm" name="jgdm" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_zzsj">转账时间</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_zzsj" name="zzsj" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_zzje">转账金额</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_zzje" name="zzje" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_lrr">录入人</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_lrr" name="lrr" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_lrsj">录入时间</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_lrsj" name="lrsj" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_ssgx">所属关系，1：受害人，2：嫌疑人</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_ssgx" name="ssgx" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_zhlb">账户类型 01-个人 02：对公</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_zhlb" name="zhlb" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_lrrxm">录入人姓名</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_lrrxm" name="lrrxm" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_lrdw">录入人单位</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_lrdw" name="lrdw" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_lrrdwmc">录入单位名称</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_lrrdwmc" name="lrrdwmc" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_xgrxm">修改人姓名</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_xgrxm" name="xgrxm" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_xgr">修改人</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_xgr" name="xgr" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_xgrdwdm">修改人单位代码</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_xgrdwdm" name="xgrdwdm" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_xgrdwmc">修改人单位名称</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_xgrdwmc" name="xgrdwmc" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_xgsj">修改时间</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_xgsj" name="xgsj" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_yxx">有效性</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_yxx" name="yxx" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_zzfs">转账方式</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_zzfs" name="zzfs" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_sfyzf">是否已止付 0：未止付；1：已止付</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_sfyzf" name="sfyzf" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_sfydj">是否已冻结 0：未冻结； 1：已冻结</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_sfydj" name="sfydj" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_ye">余额</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_ye" name="ye" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_cj">层级</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_cj" name="cj" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_sfys">是否为延伸卡号 0：否； 1：是</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_sfys" name="sfys" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_zjfh">是否资金返还 0：否 1：是</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_zjfh" name="zjfh" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_zhlx">账号类型：1：银行卡 3：pos机</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_zhlx" name="zhlx" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_fhje">返还金额</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_fhje" name="fhje" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_sfxf">是否下发</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_sfxf" name="sfxf" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_sfje">实际返还金额</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_sfje" name="sfje" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_zjfhzt">资金返还状态 0：未下发 1：已下发 2：已完成</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_zjfhzt" name="zjfhzt" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_yhksqbh">字段注释</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_yhksqbh" name="yhksqbh" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_kszfbs">字段注释</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_kszfbs" name="kszfbs" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_jjbh">字段注释</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_jjbh" name="jjbh" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
                        
        </table>
    </form>
</body>
</html>
