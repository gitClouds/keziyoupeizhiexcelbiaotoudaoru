<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <form class="form_chag" id="tbJdzjfxSfZhEditForm" name="tbJdzjfxSfZhEditForm">
        <input type="hidden" id="edit_curdType" name="curdType" >
        <input type="hidden" id="edit_id" name="id">
        <input type="hidden" id="edit_lx" name="lx" value="1">
        <table class="table table-bordered">
        	<!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
            <!-- 请根据需要修改 可编辑项、hidden等 -->
            <!-- 表单校验的字段需要在input外加一层 <div class="form_controls"> -->
            <!-- 表单校验的input classes + required -->
            <!-- 表单校验的input data-tip="" 为进入时提醒 -->
            <!-- 表单校验的input data-valid="" 为验证规则，多个可使用‘||’分割；具体参照jquery-validate.js-->
            <!-- 表单校验的input data-error="" 为验证不通过时的提醒，需与 data-valid一一对应 -->
            <!-- 校验需自己填写 -->
			<tr>
        		<td class="sec_tit">
                    <label for="edit_zh">账号<%--<span style="color: red;">*</span>--%></label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="form-control" id="edit_zh" name="zh" data-tip="">
                    </div>
                </td><td class="sec_tit">
                <label for="edit_xm">户主姓名</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" class="form-control" id="edit_xm" name="xm" >
                    </div>
                </td>

			</tr>
            <tr>
                <td class="sec_tit">
                    <label for="zjlx">证件类型</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="hidden"  id="zjlx" name="zjlx"/>
                        <input type="text"  class="form-control" id="zjmc" name="zjmc" data-jzd-type="code" data-jzd-code="529" data-jzd-filter="" data-jzd-dm="zjlx" data-jzd-mc="zjmc" readonly>
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('zjlx');clearInput('zjmc');"></span>
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_zjhm">证件号码</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" class="form-control" id="edit_zjhm" name="zjhm" >
                    </div>
                </td>

            </tr>

			<tr>

        		<td class="sec_tit">
                    <label for="jgdm">账号所属机构名称<span style="color: red;">*</span></label>
                </td>
                <td>
                    <div class="has-feedback form_controls " >
                        <%--value="${jjd.ajlx}" value='${dicMap["010103"][fn:trim(jjd.ajlx)]}'--%>
                        <input type="hidden"  id="jgdm" name="jgdm" />
                        <input type="text"  class="required form-control" id="jgmc" name="jgmc" data-jzd-type="code" data-jzd-code="542" data-jzd-filter="" data-jzd-dm="jgdm" data-jzd-mc="jgmc" readonly  data-valid="isNonEmpty" data-error="不能为空" >
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('jgdm');clearInput('jgmc');"></span>
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_ye">余额</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" class="form-control" id="edit_ye" name="ye"  >
                    </div>
                </td>
			</tr>
			<tr>

        		<td class="sec_tit">
                    <label for="zzsj">转账时间</label>
                </td>
                <td>
                    <div class="form_controls">
                        <%--value="<fmt:formatDate value="${ajxx.lasj}" pattern="yyyy-MM-dd HH:mm:ss"/>" --%>
                        <input type="text" value="<fmt:formatDate value='' pattern='yyyy-MM-dd HH:mm:ss'/>" id="zzsj" name="zzsj" onFocus="WdatePicker({lang:'zh-cn',isShowClear:false,dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'%y-%M-%d'})" class="form-control"  >
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_zzje">转账金额</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" class="form-control" id="edit_zzje" name="zzje"  >
                    </div>
                </td>
			</tr>
            <tr>
                <td class="sec_tit">
                    <label for="edit_lxfs">联系方式</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" class="form-control" id="edit_lxfs" name="lxfs" >
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_zhlb">账户类别<span style="color: red;">*</span></label>
                </td>
                <td>
                    <div class="form_controls">
                        <select id="edit_zhlb" name="zhlb" class="required form-control" data-tip="" data-valid="isNonEmpty" data-error="不能为空" >
                            <option value="01">个人</option>
                            <option value="02">对公</option>
                        </select>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="sec_tit">
                    <label for="edit_rylx">人员类型</label>
                </td>
                <td>
                    <div class="form_controls">
                        <select id="edit_rylx" name="rylx" class="form-control">
                            <option value="1">吸毒</option>
                            <option value="2">涉毒</option>
                            <option value="3">吸涉毒</option>
                        </select>
                    </div>
                </td>
            </tr>

        </table>
    </form>
</body>
</html>
