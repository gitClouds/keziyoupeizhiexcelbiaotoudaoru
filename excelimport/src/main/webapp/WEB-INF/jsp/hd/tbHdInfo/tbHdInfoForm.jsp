<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <form class="form_chag" id="tbHdInfoEditForm" name="tbHdInfoEditForm">
        <input type="hidden" id="edit_curdType" name="curdType" value="">
        <input type="hidden" id="edit_hd_id" name="hd_id" value="">
        <table class="table table-bordered">
        	<!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
            <!-- 请根据需要修改 可编辑项、hidden等 -->
            <!-- 表单校验的字段需要在input外加一层 <div class="form_controls"> -->
            <!-- 表单校验的input classes + required -->
            <!-- 表单校验的input data-tip="" 为进入时提醒 -->
            <!-- 表单校验的input data-valid="" 为验证规则，多个可使用‘||’分割；具体参照jquery-validate.js-->
            <!-- 表单校验的input data-error="" 为验证不通过时的提醒，需与 data-valid一一对应 -->
            <!-- 校验需自己填写 -->
			<tr>
        		<td class="sec_tit">
                    <label for="edit_hd_hm">手机号/身份证号</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_hd_hm" name="hd_hm" data-tip="" data-valid="isNonEmpty" data-error="不能为空" >
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_hd_hz">户主</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" class="required form-control" id="edit_hd_hz" name="hd_hz" data-tip="" data-valid="isNonEmpty" data-error="不能为空" >
                    </div>
                </td>
			</tr>
            <%--<tr>
                <td class="sec_tit">
                    <label for="edit_fj">附件上传</label>
                </td>
                <td>
                    <input type="hidden" name="fjmongo" id="fj_mongo" value="">
                    <div class="form_controls" id="edit_fj">
                    </div>
                </td>
            </tr>--%>
        </table>
    </form>
</body>
</html>
