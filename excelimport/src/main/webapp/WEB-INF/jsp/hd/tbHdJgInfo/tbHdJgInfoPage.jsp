<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/fileinput/css/fileinput.min.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
</head>
<body>
<!-- 主体内容 -->
<section class="content">
	<!--查询项表单 begin-->
    <div class="row">
        <div class="col-xs-12">
            <ul class="box clearfix">
                <form class="form_chag form-inline" name="tbHdJgInfoQueryForm" id="tbHdJgInfoQueryForm">
                    <input type="hidden" id="hd_id" name="hd_id" value="${hd_id_pre}">
                	<!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
                	<!-- 请根据需要修改 查询检索项 -->
                    	<div class="form-group">
	                        <label for="jg_hm">号码</label>
	                        <input type="text" class="form-control" id="jg_hm" name="jg_hm" >
	                    </div>
                    	<%--<div class="form-group">
	                        <label for="jg_hmlb">号码类别</label>
	                        &lt;%&ndash;<input type="text" class="form-control" id="jg_hmlb" name="jg_hmlb" >&ndash;%&gt;
                            <div class="form-group">
                                <section id="jg_hmlb" class="form-control" name="jg_hmlb">
                                    &lt;%&ndash;<option value='01'<c:if test="${userOperateDefind.operateType eq '媒体' }">selected</c:if>>媒体</option>&ndash;%&gt;
                                        <option value='01'>电话号码</option>
                                        <option value='01'>身份证号</option>
                                </section>
                            </div>
	                    </div>
                    	<div class="form-group">
	                        <label for="jg_yyslx">运营商类型</label>
	                        &lt;%&ndash;<input type="text" class="form-control" id="jg_yyslx" name="jg_yyslx" >&ndash;%&gt;
                            <section id="jg_yyslx" name="jg_yyslx">
                                <option value='01'>移动</option>
                                <option value='02'>联通</option>
                                <option value='03'>电信</option>
                                <option value='04'>广电</option>
                            </section>
	                    </div>
                    	<div class="form-group">
	                        <label for="jg_title">文件名称</label>
	                        <input type="text" class="form-control" id="jg_title" name="jg_title" >
	                    </div>--%>
                    	<div class="form-group">
	                        <label for="jg_begin">录入时间</label>
	                        <%--<input type="text" class="form-control" id="jg_lrsj" name="jg_lrsj" >--%>
                            <input type="text" class="Wdate form-control" id="jg_begin" name="jg_begin" onFocus="WdatePicker({lang:'zh-cn',readOnly:true,dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'jg_end\')||\'%y-%M-{%d}\'}'})" style="width: 30%!important" >
                            &nbsp;<input type="text" class="Wdate form-control" id="jg_end" name="jg_end" onFocus="WdatePicker({lang:'zh-cn',readOnly:true,dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'%y-%M-{%d}',minDate:'#F{$dp.$D(\'jg_begin\')}'})" style="width: 29%!important" >
	                    </div>
                    <div class="form-group ">
                        <button type="button" style="margin-left: 8%" class="btn btn-info" onclick="search();">检索</button>
                    </div>
                </form>
            </ul>
        </div>
    </div>
	<!--查询项表单 end-->
	<!--结果展示 begin-->
	<div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="tbHdJgInfoGrid" class="table table-bordered table-striped table-condensed">

                    </table>
                    <!--toolbar -->
                    <div class="btn-group" id="toolbar">
                        <i class="btn" onclick="addTbHdJgInfo();">
                            <i class="fa fa-plus-square fa-2x"></i>
                        </i>
                        <i class="btn" onclick="exportTbHdJgInfo();">
                            <i class="fa fa-cloud-download fa-2x"></i>
                        </i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--结果展示 end-->
    <!-- 提示模态框（Modal） -->
    <div class="modal fade" id="tipsModal" tabindex="-1" role="dialog" aria-labelledby="tipsModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="tipsModalLabel">提示</h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="tipsMessage"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" id="tipsConfirmBtn">确定</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 详情模态框（Modal） -->
    <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="showModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="showMessage">
                    <jsp:include page="tbHdJgInfoShow.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 编辑模态框（Modal） -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="editModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="editMessage">
                    <jsp:include page="tbHdJgInfoForm.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" id="editConfirmBtn">保存</button>
                </div>
            </div>
        </div>
    </div>
    
    <!-- 字典、树形字典展示框。如果page、form均无字典项，可无需此div -->
    <div id="treeDiv">
        <input type="text" class="form-control" id="searchTree"  placeholder="支持文本检索">
        <div id="tree"></div>
    </div>
</section>
<script>
    var ctx = "${ctx}";
    var username = "${username}";
    var dicMap = "${dicMap}";
</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/resources/bootstrap/fileinput/js/fileinput.min.js"></script>
<script src="${ctx}/resources/bootstrap/fileinput/js/zh.js"></script>
<script src="${ctx}/resources/common/fileMultipleUpload.js"></script>
<script src="${ctx}/js/hd/tbHdJgInfo.js"></script>
</body>
</html>