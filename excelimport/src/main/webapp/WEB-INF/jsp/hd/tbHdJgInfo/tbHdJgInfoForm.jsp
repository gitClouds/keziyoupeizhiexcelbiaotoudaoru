<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <form class="form_chag" id="tbHdJgInfoEditForm" name="tbHdJgInfoEditForm">
        <input type="hidden" id="edit_curdType" name="curdType" >
        <input type="hidden" id="edit_jg_id" name="jg_id" >
        <input type="hidden" id="form_hd_id" name="hd_id" value="${hd_id_pre}">
        <table class="table table-bordered">
        	<!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
            <!-- 请根据需要修改 可编辑项、hidden等 -->
            <!-- 表单校验的字段需要在input外加一层 <div class="form_controls"> -->
            <!-- 表单校验的input classes + required -->
            <!-- 表单校验的input data-tip="" 为进入时提醒 -->
            <!-- 表单校验的input data-valid="" 为验证规则，多个可使用‘||’分割；具体参照jquery-validate.js-->
            <!-- 表单校验的input data-error="" 为验证不通过时的提醒，需与 data-valid一一对应 -->
            <!-- 校验需自己填写 -->
			<tr>
        		<td class="sec_tit">
                    <label for="edit_jg_hm">身份证号/电话号码</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_jg_hm" name="jg_hm" data-tip="" data-valid="isNonEmpty" data-error="不能为空" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_jg_hmlb">号码类别</label>
                </td>
                <td>
                	<%--<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_jg_hmlb" name="jg_hmlb" data-tip="" data-valid="isNonEmpty" data-error="不能为空" >
                    </div>--%>
                    <div class="has-feedback form_controls " >
                        <%--value="${jjd.ajlx}" value='${dicMap["010103"][fn:trim(jjd.ajlx)]}'--%>
                        <input type="hidden"  id="edit_jg_hmlb" name="jg_hmlb" />
                        <input type="text"  class="required form-control" id="edit_jg_hmmc" name="edit_jg_hmmc" data-jzd-type="code" data-jzd-code="100001" data-jzd-filter="" data-jzd-dm="edit_jg_hmlb" data-jzd-mc="edit_jg_hmmc" readonly  data-valid="isNonEmpty" data-error="不能为空" >
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_jg_hmlb');clearInput('edit_jg_hmmc');"></span>
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_jg_yyslx">运营商类型</label>
                </td>
                <td>
                	<%--<div class="form_controls">
                    	<input type="text" class="required form-control" id="edit_jg_yyslx" name="jg_yyslx" data-tip="" data-valid="isNonEmpty" data-error="不能为空" >
                    </div>--%>
                        <div class="has-feedback form_controls " >
                            <%--value="${jjd.ajlx}" value='${dicMap["010103"][fn:trim(jjd.ajlx)]}'--%>
                            <input type="hidden"  id="edit_jg_yyslx" name="jg_yyslx" />
                            <input type="text"  class="required form-control" id="edit_jg_yysmc" name="edit_jg_yysmc" data-jzd-type="code" data-jzd-code="100002" data-jzd-filter="" data-jzd-dm="edit_jg_yyslx" data-jzd-mc="edit_jg_yysmc" readonly  data-valid="isNonEmpty" data-error="不能为空" >
                            <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_jg_yyslx');clearInput('edit_jg_yysmc');"></span>
                        </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_jg_filepassword">压缩文件密码</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" class="required form-control" id="edit_jg_filepassword" name="jg_filepassword" data-tip="" data-valid="isNonEmpty" data-error="不能为空" >
                    </div>
                </td>
			</tr>
            <tr>
                <td class="sec_tit">
                    <label for="edit_hd">附件上传</label>
                    <input type="hidden" id="jg_title" name="jg_title">
                </td>
                <td colspan="3">
                    <input type="hidden" name="fj_mong_id" id="hd_mongo" value="">
                    <div class="form_controls" id="edit_hd">
                    </div>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
