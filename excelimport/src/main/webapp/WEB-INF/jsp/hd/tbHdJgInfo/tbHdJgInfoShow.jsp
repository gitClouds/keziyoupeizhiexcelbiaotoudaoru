<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">身份证号/电话号码</td>
            <td id="show_jg_hm"></td>
    		<td class="sec_tit">号码类别</td>
            <td id="show_jg_hmlb"></td>
		</tr>
		<tr>
    		<td class="sec_tit">运营商类型</td>
            <td id="show_jg_yyslx"></td>
    		<td class="sec_tit">创建时间</td>
            <td id="show_jg_createtime"></td>
		</tr>
		<tr>
    		<td class="sec_tit">号码描述</td>
            <td id="show_jg_evidendesc"></td>
    		<td class="sec_tit">文件名称</td>
            <td id="show_jg_title"></td>
		</tr>
		<tr>
			<td class="sec_tit">录入时间</td>
			<td id="show_jg_lrsj"></td>
    		<td class="sec_tit">压缩文件密码</td>
            <td id="show_jg_filepassword"></td>
		</tr>
		<tr>
			<td class="sec_tit">附件</td>
			<td colspan="3">
				<input id="show_hd" type="file" >
			</td>
		</tr>
    </table>
</body>
</html>
