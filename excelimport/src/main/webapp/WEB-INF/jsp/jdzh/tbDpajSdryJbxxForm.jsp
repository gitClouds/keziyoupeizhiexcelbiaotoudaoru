<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html>
<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
</head>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-closeable-tab.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/jdzh/jdzh.js"></script>
<script>
    $(function () {
        var curdType = '${type}';
        var pk = '${pk}';
        $("#editConfirmBtn").on("click",function(){
            tbDpajSdryJbxxEditSubmit();
        });
        $("#type").val(curdType);
        $("#pk").val(pk);
    });
    var ctx = "${ctx}";
</script>
<body>
    <form class="form_chag" id="tbDpajSdryJbxxEditForm" name="tbDpajSdryJbxxEditForm">
        <input type="hidden" id="type" name="curdType" >
        <input type="hidden" id="pk" name="pk" >
        <c:if test="${type=='insert'}">
            <input type="hidden" id="yxx" name="yxx" value="1">
            <input type="hidden" id="ly" name="ly" value="2">
            <input type="hidden" id="czbz" name="czbz" value="2222222">
        </c:if>
        <table class="table table-bordered">
			<tr>
        		<td class="sec_tit">
                    <label for="edit_xyrbh">嫌疑人人编号<i class="req"> *</i></label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" onblur="check(this,1)" value="${sdry.xyrbh}" class="required form-control" id="edit_xyrbh" name="xyrbh" data-valid="isNonEmpty" data-error="不能为空" >
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_wsbh">法律文书编号</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" value="${sdry.wsbh}" class="form-control" id="edit_wsbh" name="wsbh" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>

			</tr>
			<tr>
                <td class="sec_tit">
                    <label for="edit_xm">姓名<i class="req"> *</i></label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" value="${sdry.xm}" class="required form-control" id="edit_xm" name="xm" data-valid="isNonEmpty" data-error="不能为空" >
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_xmhypy">姓名/绰号拼音</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" value="${sdry.xmhypy}" class="form-control" id="edit_xmhypy" name="xmhypy" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_cym">曾用名</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" value="${sdry.cym}" class="form-control" id="edit_cym" name="cym"  data-valid="" data-error="">
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_cyzjdm">证件种类<i class="req"> *</i></label>
                </td>
                <td>
                    <div class="has-feedback form_controls" >
                        <input type="hidden" value="${sdry.cyzjdm}" id="edit_cyzjdm" name="cyzjdm" />
                        <input type="text" value='${dicMap["CYZJDM"][sdry.cyzjdm]}' class="required form-control" id="cyzjmc" data-jzd-type="code" data-jzd-code="CYZJDM" data-jzd-filter="" data-jzd-dm="edit_cyzjdm" data-jzd-mc="cyzjmc" data-valid="isNonEmpty" data-error="不能为空" readonly>
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_cyzjdm');clearInput('cyzjmc');"></span>
                    </div>
                </td>
			</tr>
			<tr>
                <td class="sec_tit">
                    <label for="edit_zjhm">证件号码<i class="req"> *</i></label>
                </td>
                <td>
                	<div class="form_controls">
                        <c:if test="${sdry.cyzjdm=='111'}">
                    	    <input type="text" value="${sdry.zjhm}" class="required form-control" id="edit_zjhm" name="zjhm" data-tip="" data-valid="isNonEmpty" data-error="不能为空" >
                        </c:if>
                        <c:if test="${sdry.cyzjdm!='111'}">
                            <input type="text" value="${sdry.qtzjhm}" class="required form-control" id="edit_zjhm" name="zjhm" data-tip="" data-valid="isNonEmpty" data-error="不能为空" >
                        </c:if>
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_nl">年龄</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" value="${sdry.nl}" class="form-control" id="edit_nl" name="nl" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_hjd_xzqh">户籍所在地行政区划</label>
                </td>
                <td>
                    <div class="has-feedback form_controls" >
                        <input type="hidden" value="${sdry.hjd_xzqh}" id="edit_hjd_xzqh" name="hjd_xzqh" />
                        <input type="text" value='${dicMap["XZQH"][sdry.hjd_xzqh]}' class="form-control" id="hjd_xzqhmc" name="hjd_xzqhmc" data-jzd-type="tree" data-jzd-code="XZQH" data-jzd-filter="" data-jzd-dm="edit_hjd_xzqh" data-jzd-mc="hjd_xzqhmc" data-valid="isNonEmpty" data-error="不能为空" readonly>
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_hjd_xzqh');clearInput('hjd_xzqhmc');"></span>
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_hjd_dwdm">户籍地派出所</label>
                </td>
                <td>
                    <div class="has-feedback form_controls" >
                        <input type="hidden" value="${sdry.hjd_dwdm}" id="edit_hjd_dwdm" name="hjd_dwdm" />
                        <input type="text" value='${sdry.hjd_dwmc}' class="form-control" id="hjd_dwmc" name="hjd_dwmc" data-jzd-type="tree" data-jzd-code="DEPT" data-jzd-filter="" data-jzd-dm="edit_hjd_dwdm" data-jzd-mc="hjd_dwmc" data-valid="isNonEmpty" data-error="不能为空" readonly>
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_hjd_dwdm');clearInput('hjd_dwmc');"></span>
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_sjjzd_xzqh">实际居住地行政区划</label>
                </td>
                <td>
                    <div class="has-feedback form_controls" >
                        <input type="hidden" value="${sdry.sjjzd_xzqh}" id="edit_sjjzd_xzqh" name="sjjzd_xzqh" />
                        <input type="text" value='${dicMap["XZQH"][sdry.sjjzd_xzqh]}' class="form-control" id="sjjzd_xzqhmc" name="sjjzd_xzqhmc" data-jzd-type="tree" data-jzd-code="XZQH" data-jzd-filter="" data-jzd-dm="edit_sjjzd_xzqh" data-jzd-mc="sjjzd_xzqhmc" data-valid="isNonEmpty" data-error="不能为空" readonly>
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_sjjzd_xzqh');clearInput('sjjzd_xzqhmc');"></span>
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_sjjzd_dwdm">实际居住地派出所</label>
                </td>
                <td>
                    <div class="has-feedback form_controls" >
                        <input type="hidden" value="${sdry.sjjzd_dwdm}" id="edit_sjjzd_dwdm" name="sjjzd_dwdm" />
                        <input type="text" value='${sdry.sjjzd_dwmc}' class="form-control" id="sjjzd_dwmc" name="sjjzd_dwmc" data-jzd-type="tree" data-jzd-code="DEPT" data-jzd-filter="" data-jzd-dm="edit_sjjzd_dwdm" data-jzd-mc="sjjzd_dwmc" data-valid="isNonEmpty" data-error="不能为空" readonly>
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_sjjzd_dwdm');clearInput('sjjzd_dwmc');"></span>
                    </div>
                </td>
			</tr>
            <tr>
                <td class="sec_tit">
                    <label for="edit_nl">户籍所在地详址</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" value="${sdry.hjdxz}" class="form-control" id="edit_hjdxz" name="hjdxz" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_nl">实际居住地详址</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" value="${sdry.sjjzdxz}" class="form-control" id="edit_sjjzdxz" name="sjjzdxz" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
            </tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_qtdz_xzqh">其他住址详址</label>
                </td>
                <td>
                    <div class="has-feedback form_controls" >
                        <input type="hidden" value="${sdry.qtdz_xzqh}" id="edit_qtdz_xzqh" name="qtdz_xzqh" />
                        <input type="text" value='${sdry.qtzzxz}' class="form-control" id="qtzzxz" name="qtzzxz" data-jzd-type="tree" data-jzd-code="XZQH" data-jzd-filter="" data-jzd-dm="edit_qtdz_xzqh" data-jzd-mc="qtzzxz" data-valid="isNonEmpty" data-error="不能为空" readonly>
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_qtdz_xzqh');clearInput('qtzzxz');"></span>
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_bmch">绰号/别名</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" value="${sdry.bmch}" class="form-control" id="edit_bmch" name="bmch" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_xbdm">性别</label>
                </td>
                <td>
                    <div class="has-feedback form_controls" >
                        <input type="hidden" value="${sdry.xbdm}" id="edit_xbdm" name="xbdm" />
                        <input type="text" value='${dicMap["XB"][sdry.xbdm]}' class="form-control" id="xbmc" data-jzd-type="code" data-jzd-code="XB" data-jzd-filter="" data-jzd-dm="edit_xbdm" data-jzd-mc="xbmc" data-valid="isNonEmpty" data-error="不能为空" readonly>
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_xbdm');clearInput('xbmc');"></span>
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_csrq">出生日期</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" value="<fmt:formatDate value="${sdry.csrq}" pattern="yyyy-MM-dd"/>" class="Wdate form-control" id="edit_csrq" name="csrq" onFocus="WdatePicker({lang:'zh-cn',readOnly:true,dateFmt:'yyyy-MM-dd'})" style="!important" data-valid="" data-error="">
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_csdssxdm">出生地</label>
                </td>
                <td>
                    <div class="has-feedback form_controls" >
                        <input type="hidden" value="${sdry.csdssxdm}" id="edit_csdssxdm" name="csdssxdm" />
                        <input type="text" value='${dicMap["XZQH"][sdry.csdssxdm]}' class="form-control" id="csdssxmc" data-jzd-type="tree" data-jzd-code="XZQH" data-jzd-filter="" data-jzd-dm="edit_csdssxdm" data-jzd-mc="csdssxmc" data-valid="" data-error="" readonly>
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_csdssxdm');clearInput('csdssxmc');"></span>
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_grsfdm">身份</label>
                </td>
                <td>
                    <div class="has-feedback form_controls" >
                        <input type="hidden" value="${sdry.grsfdm}" id="edit_grsfdm" name="grsfdm" />
                        <input type="text" value='${dicMap["CYZK"][sdry.grsfdm]}' class="form-control" id="grsfmc" data-jzd-type="code" data-jzd-code="CYZK" data-jzd-filter="" data-jzd-dm="edit_grsfdm" data-jzd-mc="grsfmc" data-valid="" data-error="" readonly>
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_grsfdm');clearInput('grsfmc');"></span>
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_xldm">文化程度</label>
                </td>
                <td>
                    <div class="has-feedback form_controls" >
                        <input type="hidden" value="${sdry.xldm}" id="edit_xldm" name="xldm" />
                        <input type="text" value='${dicMap["WHCD"][sdry.xldm]}' class="form-control" id="xlmc" data-jzd-type="code" data-jzd-code="WHCD" data-jzd-filter="" data-jzd-dm="edit_xldm" data-jzd-mc="xlmc" data-valid="" data-error="" readonly>
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_xldm');clearInput('xlmc');"></span>
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_mzdm">民族</label>
                </td>
                <td>
                    <div class="has-feedback form_controls" >
                        <input type="hidden" value="${sdry.mzdm}" id="edit_mzdm" name="mzdm" />
                        <input type="text" value='${dicMap["MZ"][sdry.mzdm]}' class="form-control" id="mzmc" data-jzd-type="code" data-jzd-code="MZ" data-jzd-filter="" data-jzd-dm="edit_mzdm" data-jzd-mc="mzmc" data-valid="" data-error="" readonly>
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_mzdm');clearInput('mzmc');"></span>
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_fwcs">服务场所</label>
                </td>
                <td>
                    <div class="has-feedback form_controls" >
                        <input type="hidden" value="${sdry.fwcs}" id="edit_fwcs" name="fwcs" />
                        <input type="text" value='${dicMap["CSLX"][sdry.fwcs]}' class="form-control" id="fwcsmc" data-jzd-type="code" data-jzd-code="CSLX" data-jzd-filter="" data-jzd-dm="edit_fwcs" data-jzd-mc="fwcsmc" data-valid="" data-error="" readonly>
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_fwcs');clearInput('fwcsmc');"></span>
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_tmtzms">特殊体貌特征</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" value="${sdry.tmtzms}" class="form-control" id="edit_tmtzms" name="tmtzms" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
                <td class="sec_tit">
                    <label for="edit_br_lxdh">本人联系方法</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" value="${sdry.br_lxdh}" class="form-control" id="edit_br_lxdh" name="br_lxdh" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_lxrxx">联系人信息</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" value="${sdry.lxrxx}" class="form-control" id="edit_lxrxx" name="lxrxx" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
                <td class="sec_tit">
                    <label for="edit_ryxz">人员现状</label>
                </td>
                <td>
                    <div class="has-feedback form_controls" >
                        <input type="hidden" value="${sdry.ryxz}" id="edit_ryxz" name="ryxz" />
                        <input type="text" value='${dicMap["RYZT"][sdry.ryxz]}' class="form-control" id="ryxzmc" data-jzd-type="code" data-jzd-code="RYZT" data-jzd-filter="" data-jzd-dm="edit_ryxz" data-jzd-mc="ryxzmc" data-valid="" data-error="" readonly>
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_ryxz');clearInput('ryxzmc');"></span>
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_xzdjrq">现状登记日期</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" value="<fmt:formatDate value="${sdry.xzdjrq}" pattern="yyyy-MM-dd HH:mm:ss"/>" class="form-control" id="edit_xzdjrq" name="xzdjrq" onFocus="WdatePicker({lang:'zh-cn',readOnly:true,dateFmt:'yyyy-MM-dd HH:mm:ss'})" style="!important" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
                <td class="sec_tit">
                    <label for="edit_ccxdrq">初次吸毒日期</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" value="<fmt:formatDate value="${sdry.ccxdrq}" pattern="yyyy-MM-dd"/>" class="form-control" id="edit_ccxdrq" name="ccxdrq" onFocus="WdatePicker({lang:'zh-cn',readOnly:true,dateFmt:'yyyy-MM-dd'})" style="!important" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_dpfzxyrlx">毒品犯罪嫌疑人类型</label>
                </td>
                <td>
                    <div class="has-feedback form_controls" >
                        <input type="hidden" value='${sdry.dpfzxyrlx}' id="edit_dpfzxyrlx" name="dpfzxyrlx" />
                        <input type="text" value='${dicMap["DPFZXYRLX"][sdry.dpfzxyrlx]}' class="form-control" id="dpfzxyrmc" data-jzd-type="code" data-jzd-code="DPFZXYRLX" data-jzd-filter="" data-jzd-dm="edit_dpfzxyrlx" data-jzd-mc="dpfzxyrmc" data-valid="" data-error="" readonly>
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_dpfzxyrlx');clearInput('dpfzxyrmc');"></span>
                    </div>
                </td>
			</tr>
			<tr>
                <td class="sec_tit">
                    <label for="edit_bz">备注</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" value="${sdry.bz}" class="form-control" id="edit_bz" name="bz" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_txdz">通讯地址</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" value="${sdry.txdz}" class="form-control" id="edit_txdz" name="txdz" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
                <td class="sec_tit">
                    <label for="edit_djr_xm">登记人</label>
                </td>
                <td>
                    <div class="form_controls">
                        <c:if test="${type=='update'}">
                            <input type="text" value="${sdry.djr_xm}" class="form-control" id="edit_djr_xm" name="djr_xm" data-valid="" data-error="" readonly>
                        </c:if>
                        <c:if test="${type=='insert'}">
                            <input type="text" value="${djrxm}" class="form-control" id="edit_djr_xm" name="djr_xm" data-valid="" data-error="" readonly>
                        </c:if>
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_djrq">登记日期</label>
                </td>
                <td>
                	<div class="form_controls">
                        <c:if test="${type=='update'}">
                    	    <input type="text" value="<fmt:formatDate value="${sdry.djrq}" pattern="yyyy-MM-dd HH:mm:ss"/>" class="form-control" id="edit_djrq" name="djrq"  data-tip="" data-valid="" data-error="" readonly>
                        </c:if>
                        <c:if test="${type=='insert'}">
                            <input type="text" value="<fmt:formatDate value="${now}" pattern="yyyy-MM-dd HH:mm:ss"/>" class="form-control" id="edit_djrq" name="djrq"  data-tip="" data-valid="" data-error="" readonly>
                        </c:if>
                    </div>
                </td>
			</tr>
			<tr> <td class="sec_tit">
                <label for="edit_tbdw_dwmc">填报单位名称</label>
            </td>
                <td>
                    <div class="form_controls">
                        <c:if test="${type=='update'}">
                            <div class="has-feedback form_controls" >
                                <input type="hidden" value="${sdry.tbdw_dwdm}" id="edit_tbdw_dwdm" name="tbdw_dwdm" />
                                <input type="text" value='${sdry.tbdw_dwmc}' class="form-control" id="tbdw_dwmc" name="tbdw_dwmc" data-jzd-type="tree" data-jzd-code="DEPT" data-jzd-filter="" data-jzd-dm="edit_tbdw_dwdm" data-jzd-mc="tbdw_dwmc" data-valid="isNonEmpty" data-error="不能为空" readonly>
                                <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_tbdw_dwdm');clearInput('tbdw_dwmc');"></span>
                            </div>
                        </c:if>
                        <c:if test="${type=='insert'}">
                            <input type="text" value="${djdw}" class="form-control" id="edit_tbdw_dwmc" name="tbdw_dwmc" data-valid="" data-error="" readonly>
                        </c:if>
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_xxdm">血型</label>
                </td>
                <td>
                    <div class="has-feedback form_controls" >
                        <input type="hidden" value="${sdry.xxdm}" id="edit_xxdm" name="xxdm" />
                        <input type="text" value='${dicMap["XX"][sdry.xxdm]}' class="form-control" id="xxdmmc" data-jzd-type="code" data-jzd-code="XX" data-jzd-filter="" data-jzd-dm="edit_xxdm" data-jzd-mc="xxdmmc" data-valid="" data-error="" readonly>
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_xxdm');clearInput('xxdmmc');"></span>
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_yqtxqx">刑期</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" value="${sdry.yqtxqx}" class="form-control" id="edit_yqtxqx" name="yqtxqx" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_ryjsdm">人员角色</label>
                </td>
                <td>
                    <div class="has-feedback form_controls" >
                        <input type="hidden" value="${sdry.ryjsdm}" id="edit_ryjsdm" name="ryjsdm" />
                        <input type="text" value='${dicMap["RYJS"][sdry.ryjsdm]}' class="form-control" id="ryjsdmmc" data-jzd-type="code" data-jzd-code="RYJS" data-jzd-filter="" data-jzd-dm="edit_ryjsdm" data-jzd-mc="ryjsdmmc" data-valid="" data-error="" readonly>
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_ryjsdm');clearInput('ryjsdmmc');"></span>
                    </div>
                </td>
			</tr>
			<tr>
                <td class="sec_tit">
                    <label for="edit_bhsbh">保护伞编号</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" value="${sdry.bhsbh}" class="form-control" id="edit_bhsbh" name="bhsbh" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_wlsfxx">网络身份信息</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" value="${sdry.wlsfxx}" class="form-control" id="edit_wlsfxx" name="wlsfxx" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_lxrlxfs">联系人联系方式</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" value="${sdry.lxrlxfs}" class="form-control" id="edit_lxrlxfs" name="lxrlxfs" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_lxrgx">与涉毒人员关系</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" value="${sdry.lxrgx}" class="form-control" id="edit_lxrgx" name="lxrgx" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
            <c:if test="${type=='update'}">
                <tr>
                    <td class="sec_tit">
                        <label for="edit_bdxgr">本地修改人姓名</label>
                    </td>
                    <td>
                        <div class="form_controls">
                            <input type="text" value="${djrxm}" class="form-control" id="edit_bdxgr" name="bdxgr" data-tip="" data-valid="" data-error="" readonly>
                        </div>
                    </td>
                    <td class="sec_tit">
                        <label for="edit_bdxgrq">本地修改日期</label>
                    </td>
                    <td>
                        <div class="form_controls">
                            <input type="text" value="<fmt:formatDate value="${now}" pattern="yyyy-MM-dd HH:mm:ss"/>" class="form-control" id="edit_bdxgrq" name="bdxgrq" data-tip="" data-valid="" data-error="" readonly>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="sec_tit">
                        <label for="edit_bdxgrdwmc">本地修改人单位名称</label>
                    </td>
                    <td>
                        <div class="form_controls">
                            <input type="text" value="${djdw}" class="form-control" id="edit_bdxgrdwmc" name="bdxgrdwmc" data-tip="" data-valid="" data-error="" readonly>
                        </div>
                    </td>
                    <td class="sec_tit">
                        <label for="edit_bdxgrdwdm">本地修改人单位代码</label>
                    </td>
                    <td>
                        <div class="form_controls">
                            <input type="text" value="${djdwdm}" class="form-control" id="edit_bdxgrdwdm" name="bdxgrdwdm" data-tip="" data-valid="" data-error="" readonly>
                        </div>
                    </td>
                </tr>
            </c:if>
        </table>
    </form>
    <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="editConfirmBtn">保存</button>
    </div>
    <!-- 字典、树形字典展示框。如果page、form均无字典项，可无需此div -->
    <div id="treeDiv">
        <input type="text" class="form-control" id="searchTree"  placeholder="支持文本检索">
        <div id="tree"></div>
    </div>
</body>
</html>
