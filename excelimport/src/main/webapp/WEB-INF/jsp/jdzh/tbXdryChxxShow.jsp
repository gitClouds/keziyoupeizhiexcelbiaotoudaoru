<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html>
<head>
	<meta charset="UTF-8">
	<title>title</title>
	<link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
	<link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
	<link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
	<link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
	<link rel="stylesheet" href="${ctx}/css/pub/page.css">
	<link rel="stylesheet" href="${ctx}/css/pub/code.css">
</head>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script>
	$(function () {
		$("#jdzhchajxx", parent.document).height(document.body.scrollHeight);
		var dic = ${dict};
		var pafs = '${xdch.bclydpzl}';
		var pafss = pafs.split("/");
		pafss.splice(0,1);
		var str = '';
		if (pafs!=null&&pafs!=''){
			$.each(pafss,function(index,value){
				var fy = dic["DPMC"][value];
				if (fy!=undefined&&fy!=''){
					str += fy+',';
				}
			});
			str = str.substring(0,str.length-1);
			$("#show_bclydpzl").text(str);
		}
		var yxjg = '${xdch.yxjg}';
		var yxjgs = yxjg.split("/");
		yxjgs.splice(0,1);
		var str = '';
		if (yxjg!=null&&yxjg!=''){
			$.each(yxjgs,function(index,value){
				var fy = dic["YXJG"][value];
				if (fy!=undefined&&fy!=''){
					str += fy+',';
				}
			});
			str = str.substring(0,str.length-1);
			$("#show_yxjg").text(str);
		}
		var czqk = '${xdch.czqk}';
		var czqks = czqk.split("/");
		czqks.splice(0,1);
		var str = '';
		if (czqk!=null&&czqk!=''){
			$.each(czqks,function(index,value){
				var fy = dic["CZQK"][value];
				if (fy!=undefined&&fy!=''){
					str += fy+',';
				}
			});
			str = str.substring(0,str.length-1);
			$("#show_czqk").text(str);
		}
	});
</script>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">人员编号</td>
            <td id="show_xyrbh">${xdch.xyrbh}</td>
			<td class="sec_tit">毒品来源</td>
			<td id="show_dplydm">${dicMap["DPLY"][xdch.dplydm]}</td>
		</tr>
		<tr>
    		<td class="sec_tit">查获日期</td>
            <td id="show_chrq"><fmt:formatDate value="${xdch.chrq}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
    		<td class="sec_tit">查获单位_行政区划代码</td>
            <td id="show_chdw_xzqhdm">${xdch.chdw_xzqhdm}</td>
		</tr>
		<tr>
    		<td class="sec_tit">查获单位区划内详细地址</td>
            <td id="show_chdw_qhnxxdz">${xdch.chdw_qhnxxdz}</td>
    		<td class="sec_tit">查获单位单位名称</td>
            <td id="show_chdw_dwmc">${xdch.chdw_dwmc}</td>
		</tr>
		<tr>
    		<td class="sec_tit">吸毒场所</td>
            <td id="show_xdcszldm">${dicMap["XDCS"][xdch.xdcszldm]}</td>
    		<td class="sec_tit">本次滥用毒品种类</td>
            <td id="show_bclydpzl"></td>
		</tr>
		<tr>
			<td class="sec_tit">违法事实简要案情</td>
			<td colspan="3" id="show_wfss_jyaq">${xdch.wfss_jyaq}</td>
		</tr>
		<tr>
    		<td class="sec_tit">查获类型</td>
            <td id="show_xdrychlxdm">${dicMap["CHYY"][xdch.xdrychlxdm]}</td>
    		<td class="sec_tit">查获来源</td>
            <td id="show_xdrychlydm">${dicMap["CHLY"][xdch.xdrychlydm]}</td>
		</tr>
		<tr>
    		<td class="sec_tit">尿检结果</td>
            <td id="show_njjgdm">
				<c:if test="${xdch.njjgdm=='1'}">
					阳性
				</c:if>
				<c:if test="${xdch.njjgdm=='2'}">
					阴性
				</c:if>
			</td>
			<td class="sec_tit">备注</td>
			<td id="show_bz">${xdch.bz}</td>
		</tr>
		<tr>
    		<td class="sec_tit">复吸次数</td>
            <td id="show_xfcs">${xdch.xfcs}</td>
    		<td class="sec_tit">娱乐场所查获序号</td>
            <td id="show_ylcschxh">${xdch.ylcschxh}</td>
		</tr>
		<tr>
    		<td class="sec_tit">娱乐场所名称</td>
            <td id="show_ylcsmc">${xdch.ylcsmc}</td>
    		<td class="sec_tit">是否特殊人群</td>
            <td id="show_sftsrq">
				<c:if test="${xdch.sftsrq==0}">
					否
				</c:if>
				<c:if test="${xdch.sftsrq==1}">
					是
				</c:if>
			</td>
		</tr>
		<tr>
    		<td class="sec_tit">特殊人群种类</td>
            <td id="show_tsrqzl">${dicMap["TSRQ"][xdch.tsrqzl]}</td>
    		<td class="sec_tit">阳性结果</td>
            <td id="show_yxjg"></td>
		</tr>
		<tr>
    		<td class="sec_tit">是否疾病</td>
            <td id="show_sfjb">
				<c:if test="${xdch.sfjb==0}">
					否
				</c:if>
				<c:if test="${xdch.sfjb==1}">
					是
				</c:if>
			</td>
    		<td class="sec_tit">疾病类型</td>
            <td id="show_jblx">${dicMap["JBLX"][xdch.jblx]}</td>
		</tr>
		<tr>
    		<td class="sec_tit">是否末次查获</td>
            <td id="show_sfmcch">
				<c:if test="${xdch.sfmcch==0}">
					否
				</c:if>
				<c:if test="${xdch.sfmcch==1}">
					是
				</c:if>
			</td>
    		<td class="sec_tit">查获次数</td>
            <td id="show_chcs">${xdch.chcs}</td>
		</tr>
		<tr>
    		<td class="sec_tit">填表人姓名</td>
            <td id="show_tbr_xm">${xdch.tbr_xm}</td>
    		<td class="sec_tit">填表人联系电话</td>
            <td id="show_tbr_lxdh">${xdch.tbr_lxdh}</td>
		</tr>
		<tr>
    		<td class="sec_tit">审核人姓名</td>
            <td id="show_shr_xm">${xdch.shr_xm}</td>
    		<td class="sec_tit">填表单位单位名称</td>
            <td id="show_tbdw_dwmc">${xdch.tbdw_dwmc}</td>
		</tr>
		<tr>
    		<td class="sec_tit">填表日期</td>
            <td id="show_tbrq"><fmt:formatDate value="${sdch.tbrq}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
    		<td class="sec_tit">录入人姓名</td>
            <td id="show_lrr_xm">${xdch.lrr_xm}</td>
		</tr>
		<tr>
    		<td class="sec_tit">录入单位单位名称</td>
            <td id="show_lrdw_dwmc">${xdch.lrdw_dwmc}</td>
    		<td class="sec_tit">录入时间</td>
            <td id="show_lrsj"><fmt:formatDate value="${sdch.lrsj}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
		</tr>
		<tr>
    		<td class="sec_tit">修改部署单位</td>
            <td id="show_xgbsdw">${xdch.xgbsdw}</td>
    		<td class="sec_tit">归属单位</td>
            <td id="show_gsdw">${xdch.gsdw}</td>
		</tr>
		<tr>
    		<td class="sec_tit">嫌疑人类别代码</td>
            <td id="show_xyrlb">${dicMap["XYRLB"][xdch.xyrlb]}</td>
			<td class="sec_tit">吞食异物描述</td>
			<td id="show_tsywms">${xdch.tsywms}</td>
		</tr>
		<tr>
    		<td class="sec_tit">是否吞食异物</td>
            <td id="show_sftsyw">
				<c:if test="${xdch.sftsyw==0}">
					否
				</c:if>
				<c:if test="${xdch.sftsyw==1}">
					是
				</c:if>
			</td>
    		<td class="sec_tit">对应的所有处置情况</td>
            <td id="show_czqk"></td>
		</tr>
		<tr>
    		<td class="sec_tit">对应的所有处置情况名称</td>
            <td id="show_czqkmc">${xdch.czqkmc}</td>
    		<td class="sec_tit">本次查获之前的管控现状代码</td>
            <td id="show_bczhzqgkxzdm">${dicMap["GKXZ"][xdch.bczhzqgkxzdm]}</td>
		</tr>
		<tr>
			<td class="sec_tit">内外网标志</td>
			<td id="show_inoutbz">${xdch.inoutbz}</td>
    		<td class="sec_tit">来源</td>
            <td id="show_ly">
				<c:if test="${xdch.ly==1}">
					接口下发
				</c:if>
				<c:if test="${xdch.ly==2}">
					本地录入
				</c:if>
			</td>
		</tr>
    </table>
</body>
</html>
