<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>title</title>
	<link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
	<link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
	<link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
	<link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
	<link rel="stylesheet" href="${ctx}/css/pub/page.css">
	<link rel="stylesheet" href="${ctx}/css/pub/code.css">
</head>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-closeable-tab.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/js/common.js"></script>
<body>
<section class="content">
<div class="banner_title">
	<h3 style="color: #00acd6">基本信息</h3>
</div>
<c:if test="${empty xdry}">
	<h6 style="color: red">无人员信息</h6>
</c:if>
<c:if test="${not empty xdry}">
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">人员类型</td>
            <td id="show_xdryly">${dicMap["XDRYLY"][xdry.xdryly]}</td>
			<td class="sec_tit">人员编号</td>
			<td id="show_xyrbh">${xdry.xyrbh}</td>
		</tr>
		<tr>
    		<td class="sec_tit">姓名</td>
            <td id="show_xm">${xdry.xm}</td>
			<td class="sec_tit">证件种类</td>
			<td id="show_cyzjdm">${dicMap["CYZJDM"][xdry.cyzjdm]}</td>
		</tr>
		<tr>
    		<td class="sec_tit">身份证号</td>
            <td id="show_sfzhm18">${xdry.sfzhm18}</td>
			<td class="sec_tit">其他证件号码</td>
			<td id="show_zjhm">${xdry.zjhm}</td>
		</tr>
		<tr>
    		<td class="sec_tit">别名/绰号</td>
            <td id="show_bmch">${xdry.bmch}</td>
			<td class="sec_tit">工作单位名称</td>
			<td id="show_gzdw_dwmc">${xdry.gzdw_dwmc}</td>
		</tr>
		<tr>
    		<td class="sec_tit">性别</td>
            <td id="show_xbdm">${dicMap["XB"][xdry.xbdm]}</td>
    		<td class="sec_tit">出生日期</td>
            <td id="show_csrq"><fmt:formatDate value="${xdry.csrq}" pattern="yyyy-MM-dd"/></td>
		</tr>
		<tr>
    		<td class="sec_tit">民族</td>
            <td id="show_mzdm">${dicMap["MZ"][xdry.mzdm]}</td>
    		<td class="sec_tit">身高</td>
            <td id="show_sg">${xdry.sg}</td>
		</tr>
		<tr>
    		<td class="sec_tit">国籍</td>
            <td id="show_jggjdqdm">${dicMap["GJ"][xdry.jggjdqdm]}</td>
    		<td class="sec_tit">文化程度</td>
            <td id="show_xldm">${dicMap["WHCD"][xdry.xldm]}</td>
		</tr>
		<tr>
    		<td class="sec_tit">指纹编号</td>
            <td id="show_szzwbh">${xdry.szzwbh}</td>
    		<td class="sec_tit">DNA编号</td>
            <td id="show_rydnabh">${xdry.rydnabh}</td>
		</tr>
		<tr>
    		<td class="sec_tit">户籍地址详址</td>
            <td id="show_hjdz_dzmc">${xdry.hjdz_dzmc}</td>
			<td class="sec_tit">户籍地址公安机关名称</td>
			<td id="show_hjdz_gajgmc">${xdry.hjdz_gajgmc}</td>
		</tr>
		<tr>
    		<td class="sec_tit">实际居住地详址</td>
            <td id="show_sjjzd_dzmc">${xdry.sjjzd_dzmc}</td>
    		<td class="sec_tit">实际居住地公安机关名称</td>
            <td id="show_sjjzd_gajgmc">${xdry.sjjzd_gajgmc}</td>
		</tr>
		<tr>
    		<td class="sec_tit">录入人姓名</td>
            <td id="show_lrr_xm">${xdry.lrr_xm}</td>
    		<td class="sec_tit">录入单位单位名称</td>
            <td id="show_lrdw_dwmc">${xdry.lrdw_dwmc}</td>
		</tr>
		<tr>
    		<td class="sec_tit">录入时间</td>
            <td id="show_lrsj"><fmt:formatDate value="${xdry.lrsj}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
    		<td class="sec_tit">是否828库人员</td>
            <td id="show_sf828k">
				<c:if test="${xdry.sf828k==0}">
					否
				</c:if>
				<c:if test="${xdry.sf828k==1}">
					是
				</c:if>
			</td>
		</tr>
		<tr>
    		<td class="sec_tit">涉毒人员编号</td>
            <td id="show_sdrybh">${xdry.sdrybh}</td>
			<td class="sec_tit">归属单位</td>
			<td id="show_gsdw">${xdry.gsdw}</td>
		</tr>
		<tr>
    		<td class="sec_tit">从业状况</td>
            <td id="show_cyzk">${dicMap["CYZK"][xdry.cyzk]}</td>
			<td class="sec_tit">来源</td>
			<td id="show_ly">
				<c:if test="${xdry.ly==1}">
					接口下发
				</c:if>
				<c:if test="${xdry.ly==2}">
					本地录入
				</c:if>
			</td>
		</tr>
		<tr>
			<td class="sec_tit">本地修改日期</td>
			<td id="show_bdxgrq">${xdry.bdxgrq}</td>
			<td class="sec_tit">本地修改人姓名</td>
			<td id="show_bdxgr">${xdry.bdxgr}</td>
		</tr>
		<tr>
			<td class="sec_tit">本地修改人单位名称</td>
			<td id="show_bdxgrdwmc">${xdry.bdxgrdwmc}</td>
			<td class="sec_tit">本地修改人单位代码</td>
			<td id="show_bdxgrdwdm">${xdry.bdxgrdwdm}</td>
		</tr>
    </table>
</c:if>
	<div style="height: 5px"></div>
	<div class="banner_title">
		<h3 style="color: #00acd6">查获信息</h3>
	</div>
	<c:if test="${empty chxxs}">
		<h6 style="color: red">无查获信息</h6>
	</c:if>
	<c:if test="${not empty chxxs}">
		<div style="height: 5px"></div>
		<div>
			<ul class="banner_tab">
				<c:forEach items="${chxxs}" var="map" varStatus="status">
					<li id="tab${status.index+1}" onclick="showajch('${map.chxh}','${map.xyrbh}','${lx}',this);" <c:if test="${status.index==0}">class="tab_item_get"</c:if> >
						<a href="javascript:void(0)" >查获${status.index+1}</a>
					</li>
				</c:forEach>
			</ul>
		</div>
		<iframe src="${ctx}/jdzh/jdzhchajxx?ajbh=${chxxs[0].chxh}&xyrbh=${chxxs[0].xyrbh}&lx=${lx}" id="jdzhchajxx" scrolling="no" width="100%" frameborder="0"></iframe>
	</c:if>
</section>
	<script>
		var ctx = "${ctx}";
		function showajch(ajbh,xyrbh,lx,a) {
			$(a).addClass("tab_item_get").siblings().removeClass("tab_item_get");
			$("#jdzhchajxx").attr("src",ctx + "/jdzh/jdzhchajxx/?ajbh="+ajbh+"&xyrbh="+xyrbh+"&lx="+lx);
		}
	</script>
</body>
</html>
