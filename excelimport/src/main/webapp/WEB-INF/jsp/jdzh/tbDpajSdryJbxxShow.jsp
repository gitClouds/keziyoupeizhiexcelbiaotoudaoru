<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>title</title>
	<link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
	<link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
	<link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
	<link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
	<link rel="stylesheet" href="${ctx}/css/pub/page.css">
	<link rel="stylesheet" href="${ctx}/css/pub/code.css">
</head>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-closeable-tab.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/js/common.js"></script>
<body>
<section class="content">
		<div class="banner_title">
			<h3 style="color: #00acd6">基本信息</h3>
		</div>
<c:if test="${empty sdry}">
	<h6 style="color: red">无人员信息</h6>
</c:if>
<c:if test="${not empty sdry}">
    <table class="table table-bordered" >
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">涉毒人员编号</td>
            <td id="show_xyrbh">${sdry.xyrbh}</td>
			<td class="sec_tit">填报单位名称</td>
			<td id="show_tbdw_dwmc">${sdry.tbdw_dwmc}</td>
		</tr>
		<tr>
    		<td class="sec_tit">登记人</td>
            <td id="show_djr_xm">${sdry.djr_xm}</td>
			<td class="sec_tit">登记日期</td>
			<td id="show_djrq"><fmt:formatDate value="${sdry.djrq}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
		</tr>
		<tr>
    		<td class="sec_tit">姓名</td>
            <td id="show_xm">${sdry.xm}</td>
    		<td class="sec_tit">曾用名</td>
            <td id="show_cym">${sdry.cym}</td>
		</tr>
		<tr>
    		<td class="sec_tit">证件种类</td>
            <td id="show_cyzjdm">${dicMap["CYZJDM"][sdry.cyzjdm]}</td>
    		<td class="sec_tit">证件号码</td>
            <td id="show_zjhm">
			<c:if test="${sdry.cyzjdm=='111'}">
				${sdry.zjhm}
			</c:if>
			<c:if test="${sdry.cyzjdm!='111'}">
				${sdry.qtzjhm}
			</c:if>
			</td>
		</tr>
		<tr>
    		<td class="sec_tit">户籍所在地详址</td>
            <td id="show_hjdxz">${sdry.hjdxz}</td>
			<td class="sec_tit">户籍地派出所</td>
			<td id="show_hjd_dwmc">${sdry.hjd_dwmc}</td>
		</tr>
		<tr>
    		<td class="sec_tit">实际居住地详址</td>
            <td id="show_sjjzdxz">${sdry.sjjzdxz}</td>
			<td class="sec_tit">实际居住地派出所</td>
			<td id="show_sjjzd_dwmc">${sdry.sjjzd_dwmc}</td>
		</tr>
		<tr>
    		<td class="sec_tit">其他住址详址</td>
            <td id="show_qtzzxz">${sdry.qtzzxz}</td>
			<td class="sec_tit">绰号/别名</td>
			<td id="show_bmch">${sdry.bmch}</td>
		</tr>
		<tr>
    		<td class="sec_tit">性别</td>
            <td id="show_xbdm">${dicMap["XB"][sdry.xbdm]}</td>
			<td class="sec_tit">出生日期</td>
			<td id="show_csrq"><fmt:formatDate value="${sdry.csrq}" pattern="yyyy-MM-dd"/></td>
		</tr>
		<tr>
			<td class="sec_tit">年龄</td>
			<td id="show_nl">${sdry.nl}</td>
    		<td class="sec_tit">出生地</td>
            <td id="show_csdssxdm">${dicMap["XZQH"][sdry.csdssxdm]}</td>
		</tr>
		<tr>
    		<td class="sec_tit">身份</td>
            <td id="show_grsfdm">${dicMap["CYZK"][sdry.grsfdm]}</td>
    		<td class="sec_tit">文化程度</td>
            <td id="show_xldm">${dicMap["WHCD"][sdry.xldm]}</td>
		</tr>
		<tr>
    		<td class="sec_tit">民族</td>
            <td id="show_mzdm">${dicMap["MZ"][sdry.mzdm]}</td>
    		<td class="sec_tit">服务场所</td>
            <td id="show_fwcs">${dicMap["CSLX"][sdry.fwcs]}</td>
		</tr>
		<tr>
			<td class="sec_tit">姓名/绰号拼音</td>
			<td id="show_xmhypy">${sdry.xmhypy}</td>
    		<td class="sec_tit">特殊体貌特征</td>
            <td id="show_tmtzms">${sdry.tmtzms}</td>
		</tr>
		<tr>
    		<td class="sec_tit">本人联系方法</td>
            <td id="show_br_lxdh">${sdry.br_lxdh}</td>
    		<td class="sec_tit">联系人信息</td>
            <td id="show_lxrxx">${sdry.lxrxx}</td>
		</tr>
		<tr>
    		<td class="sec_tit">人员现状</td>
            <td id="show_ryxz">${dicMap["RYZT"][sdry.ryxz]}</td>
    		<td class="sec_tit">现状登记日期</td>
            <td id="show_xzdjrq"><fmt:formatDate value="${sdry.xzdjrq}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
		</tr>
		<tr>
    		<td class="sec_tit">初次吸毒日期</td>
            <td id="show_ccxdrq"><fmt:formatDate value="${sdry.ccxdrq}" pattern="yyyy-MM-dd"/></td>
    		<td class="sec_tit">毒品犯罪嫌疑人类型</td>
            <td id="show_dpfzxyrlx">${dicMap["DPFZXYRLX"][sdry.dpfzxyrlx]}</td>
		</tr>
		<tr>
    		<td class="sec_tit">备注</td>
            <td id="show_bz">${sdry.bz}</td>
			<td class="sec_tit">法律文书编号</td>
			<td id="show_wsbh">${sdry.wsbh}</td>
		</tr>
		<tr>
    		<td class="sec_tit">刑期</td>
            <td id="show_yqtxqx">${sdry.yqtxqx}</td>
			<td class="sec_tit">人员角色</td>
			<td id="show_ryjsdm">${dicMap["RYJS"][sdry.ryjsdm]}</td>
		</tr>
		<tr>
    		<td class="sec_tit">通讯地址</td>
            <td id="show_txdz">${sdry.txdz}</td>
			<td class="sec_tit">血型</td>
			<td id="show_xxdm">${dicMap["XX"][sdry.xxdm]}</td>
		</tr>
		<tr>
    		<td class="sec_tit">网络身份信息</td>
            <td id="show_wlsfxx">${sdry.wlsfxx}</td>
			<td class="sec_tit">保护伞编号</td>
			<td id="show_bhsbh">${sdry.bhsbh}</td>
		</tr>
		<tr>
			<td class="sec_tit">联系人联系方式</td>
			<td id="show_lxrlxfs">${sdry.lxrlxfs}</td>
			<td class="sec_tit">与涉毒人员关系</td>
			<td id="show_lxrgx">${sdry.lxrgx}</td>
		</tr>
		<tr>
			<td class="sec_tit">来源</td>
			<td id="show_ly">
				<c:if test="${sdry.ly==1}">
					接口下发
				</c:if>
				<c:if test="${sdry.ly==2}">
					本地录入
				</c:if>
			</td>
			<td class="sec_tit">本地修改日期</td>
			<td id="show_bdxgrq"><fmt:formatDate value="${sdry.bdxgrq}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
		</tr>
		<tr>
			<td class="sec_tit">本地修改人姓名</td>
			<td id="show_bdxgr">${sdry.bdxgr}</td>
			<td class="sec_tit">本地修改人单位名称</td>
			<td id="show_bdxgrdwmc">${sdry.bdxgrdwmc}</td>
		</tr>
    </table>
</c:if>
	<div style="height: 5px"></div>
	<div class="banner_title">
		<h3 style="color: #00acd6">案件信息</h3>
	</div>
	<c:if test="${empty chxxs}">
		<h6 style="color: red">无案件查获信息</h6>
	</c:if>
	<c:if test="${not empty chxxs}">
		<div style="height: 5px"></div>
		<div>
			<ul class="banner_tab">
				<c:forEach items="${chxxs}" var="map" varStatus="status">
					<li onclick="showajch('${map.ajbh}','${map.xyrbh}','${lx}',this);" <c:if test="${status.index==0}">class="tab_item_get"</c:if> >
						<a href="javascript:void(0)" >案件${status.index+1}</a>
					</li>
				</c:forEach>
			</ul>
		</div>
		<iframe src="${ctx}/jdzh/jdzhchajxx?ajbh=${chxxs[0].ajbh}&xyrbh=${chxxs[0].xyrbh}&lx=${lx}" id="jdzhchajxx" scrolling="no" width="100%" frameborder="0"></iframe>
	</c:if>
</section>
	<script>
		var ctx = "${ctx}";
		function showajch(ajbh,xyrbh,lx,a) {
			$(a).addClass("tab_item_get").siblings().removeClass("tab_item_get");
			$("#jdzhchajxx").attr("src",ctx + "/jdzh/jdzhchajxx/?ajbh="+ajbh+"&xyrbh="+xyrbh+"&lx="+lx);
		}
	</script>
</body>
</html>
