<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
</head>
<body>
<!-- 主体内容 -->
<section class="content">
	<!--查询项表单 begin-->
    <div class="row">
        <div class="col-xs-12">
            <ul class="box clearfix">
                <form class="form_chag form-inline" name="tbXdryChxxQueryForm" id="tbXdryChxxQueryForm">
                	<!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
                	<!-- 请根据需要修改 查询检索项 -->
                    	<div class="form-group">
	                        <label for="chxh">查获序号--DEXXXXX--</label>
	                        <input type="text" class="form-control" id="chxh" name="chxh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="xyrbh">人员编号--DE00038--编号规则为：R+XXXXXXXXXXXX(公安机关机构代码)+XXXXXX(年月)+XXXX(顺序号)              </label>
	                        <input type="text" class="form-control" id="xyrbh" name="xyrbh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="chrq">查获日期--DE01160--</label>
	                        <input type="text" class="form-control" id="chrq" name="chrq" >
	                    </div>
                    	<div class="form-group">
	                        <label for="chdw_xzqhdm">  查获单位_行政区划代码--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码</label>
	                        <input type="text" class="form-control" id="chdw_xzqhdm" name="chdw_xzqhdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="chdw_qhnxxdz">  查获单位_区划内详细地址--DE00076--</label>
	                        <input type="text" class="form-control" id="chdw_qhnxxdz" name="chdw_qhnxxdz" >
	                    </div>
                    	<div class="form-group">
	                        <label for="chdw_dwmc">  查获单位_单位名称--DE00065--</label>
	                        <input type="text" class="form-control" id="chdw_dwmc" name="chdw_dwmc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="xdcszldm">吸毒场所--DE01135--采用GA/TXXXX《吸毒场所种类代码》</label>
	                        <input type="text" class="form-control" id="xdcszldm" name="xdcszldm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="bclydpzl">本次滥用毒品种类--DEXXXXX--</label>
	                        <input type="text" class="form-control" id="bclydpzl" name="bclydpzl" >
	                    </div>
                    	<div class="form-group">
	                        <label for="dplydm">毒品来源--DE00166--采用GA 332.3《禁毒信息管理代码 第3部分:毒品来源代码》</label>
	                        <input type="text" class="form-control" id="dplydm" name="dplydm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="wfss_jyaq">违法事实_简要案情--DE00100--</label>
	                        <input type="text" class="form-control" id="wfss_jyaq" name="wfss_jyaq" >
	                    </div>
                    	<div class="form-group">
	                        <label for="xdrychlxdm">查获类型--DE01133--吸毒人员查获类型代码</label>
	                        <input type="text" class="form-control" id="xdrychlxdm" name="xdrychlxdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="xdrychlydm">查获来源--DE01132--采用GA/TXXXX《吸毒人员查获来源代码》</label>
	                        <input type="text" class="form-control" id="xdrychlydm" name="xdrychlydm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="njjgdm">尿检结果--DE01137--尿检结果代码</label>
	                        <input type="text" class="form-control" id="njjgdm" name="njjgdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="xdfsdm">吸毒方式--DE01136--采用GA/TXXXX《吸毒方式代码》</label>
	                        <input type="text" class="form-control" id="xdfsdm" name="xdfsdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="xfcs">复吸次数----</label>
	                        <input type="text" class="form-control" id="xfcs" name="xfcs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ylcschxh">娱乐场所查获序号----</label>
	                        <input type="text" class="form-control" id="ylcschxh" name="ylcschxh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ylcsmc">娱乐场所名称----</label>
	                        <input type="text" class="form-control" id="ylcsmc" name="ylcsmc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sftsrq">是否特殊人群----</label>
	                        <input type="text" class="form-control" id="sftsrq" name="sftsrq" >
	                    </div>
                    	<div class="form-group">
	                        <label for="tsrqzl">特殊人群种类----</label>
	                        <input type="text" class="form-control" id="tsrqzl" name="tsrqzl" >
	                    </div>
                    	<div class="form-group">
	                        <label for="yxjg">阳性结果----</label>
	                        <input type="text" class="form-control" id="yxjg" name="yxjg" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sfjb">是否疾病----</label>
	                        <input type="text" class="form-control" id="sfjb" name="sfjb" >
	                    </div>
                    	<div class="form-group">
	                        <label for="jblx">疾病类型----</label>
	                        <input type="text" class="form-control" id="jblx" name="jblx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sfmcch">是否末次查获----</label>
	                        <input type="text" class="form-control" id="sfmcch" name="sfmcch" >
	                    </div>
                    	<div class="form-group">
	                        <label for="chcs">查获次数----</label>
	                        <input type="text" class="form-control" id="chcs" name="chcs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="tbr_xm">  填表人_姓名--DE00002--</label>
	                        <input type="text" class="form-control" id="tbr_xm" name="tbr_xm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="tbr_lxdh">  填表人_联系电话--DE00216--固定电话号码、移动电话号码</label>
	                        <input type="text" class="form-control" id="tbr_lxdh" name="tbr_lxdh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="shr_xm">审核人_姓名--DE00002--</label>
	                        <input type="text" class="form-control" id="shr_xm" name="shr_xm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="tbdw_dwmc">填表单位_单位名称--DE00065--</label>
	                        <input type="text" class="form-control" id="tbdw_dwmc" name="tbdw_dwmc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="tbrq">填表日期--DE01158--</label>
	                        <input type="text" class="form-control" id="tbrq" name="tbrq" >
	                    </div>
                    	<div class="form-group">
	                        <label for="lrr_xm">录入人_姓名--DE00002--</label>
	                        <input type="text" class="form-control" id="lrr_xm" name="lrr_xm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="lrdw_dwmc">录入单位_单位名称--DE00065--</label>
	                        <input type="text" class="form-control" id="lrdw_dwmc" name="lrdw_dwmc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="lrsj">录入时间--DE00739--</label>
	                        <input type="text" class="form-control" id="lrsj" name="lrsj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="bz">备注--DE00503--</label>
	                        <input type="text" class="form-control" id="bz" name="bz" >
	                    </div>
                    	<div class="form-group">
	                        <label for="rybs">人员标识--DEXXXXX--</label>
	                        <input type="text" class="form-control" id="rybs" name="rybs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="xgbsdw">修改部署单位--DEXXXXX--</label>
	                        <input type="text" class="form-control" id="xgbsdw" name="xgbsdw" >
	                    </div>
                    	<div class="form-group">
	                        <label for="gsdw">归属单位--DEXXXXX--</label>
	                        <input type="text" class="form-control" id="gsdw" name="gsdw" >
	                    </div>
                    	<div class="form-group">
	                        <label for="gxsjc">更新时间戳--DEXXXXX--</label>
	                        <input type="text" class="form-control" id="gxsjc" name="gxsjc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="czbs">操作标识--DEXXXXX--</label>
	                        <input type="text" class="form-control" id="czbs" name="czbs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="inoutbz">内外网标志--DEXXXXX--</label>
	                        <input type="text" class="form-control" id="inoutbz" name="inoutbz" >
	                    </div>
                    	<div class="form-group">
	                        <label for="yxx">有效性--DEXXXXX--</label>
	                        <input type="text" class="form-control" id="yxx" name="yxx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="gxsj">更新时间--DEXXXXX--</label>
	                        <input type="text" class="form-control" id="gxsj" name="gxsj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="chdw_dwdm">  查获单位_单位代码--DE00065--</label>
	                        <input type="text" class="form-control" id="chdw_dwdm" name="chdw_dwdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="lrdw_dwdm">录入单位_单位代码--DE00065--</label>
	                        <input type="text" class="form-control" id="lrdw_dwdm" name="lrdw_dwdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="tbdw_dwdm">填表单位_单位代码--DE00065--</label>
	                        <input type="text" class="form-control" id="tbdw_dwdm" name="tbdw_dwdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="xyrlb">嫌疑人类别代码---XYRLB</label>
	                        <input type="text" class="form-control" id="xyrlb" name="xyrlb" >
	                    </div>
                    	<div class="form-group">
	                        <label for="fjmc">字段</label>
	                        <input type="text" class="form-control" id="fjmc" name="fjmc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="wlgjfjxh">字段</label>
	                        <input type="text" class="form-control" id="wlgjfjxh" name="wlgjfjxh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="chjclx">字段</label>
	                        <input type="text" class="form-control" id="chjclx" name="chjclx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="chjc_dwdm">字段</label>
	                        <input type="text" class="form-control" id="chjc_dwdm" name="chjc_dwdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="chjc_dwmc">字段</label>
	                        <input type="text" class="form-control" id="chjc_dwmc" name="chjc_dwmc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="chsnl">字段</label>
	                        <input type="text" class="form-control" id="chsnl" name="chsnl" >
	                    </div>
                    	<div class="form-group">
	                        <label for="tsywms">吞食异物描述</label>
	                        <input type="text" class="form-control" id="tsywms" name="tsywms" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sftsyw">是否吞食异物</label>
	                        <input type="text" class="form-control" id="sftsyw" name="sftsyw" >
	                    </div>
                    	<div class="form-group">
	                        <label for="czqk">对应的所有处置情况</label>
	                        <input type="text" class="form-control" id="czqk" name="czqk" >
	                    </div>
                    	<div class="form-group">
	                        <label for="czqkmc">对应的所有处置情况名称</label>
	                        <input type="text" class="form-control" id="czqkmc" name="czqkmc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="bczhzqgkxzdm">本次查获之前的管控现状代码</label>
	                        <input type="text" class="form-control" id="bczhzqgkxzdm" name="bczhzqgkxzdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ly">来源(1：接口下发2：本地录入)</label>
	                        <input type="text" class="form-control" id="ly" name="ly" >
	                    </div>
                                        
                    <div class="form-group ">
                        <button type="button" style="margin-left: 8%" class="btn btn-info" onclick="search();">检索</button>
                    </div>
                </form>
            </ul>
        </div>
    </div>
	<!--查询项表单 end-->
	<!--结果展示 begin-->
	<div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="tbXdryChxxGrid" class="table table-bordered table-striped table-condensed">

                    </table>
                    <!--toolbar -->
                    <div class="btn-group" id="toolbar">
                        <i class="btn" onclick="addTbXdryChxx();">
                            <i class="fa fa-plus-square fa-2x"></i>
                        </i>
                        <i class="btn" onclick="exportTbXdryChxx();">
                            <i class="fa fa-cloud-download fa-2x"></i>
                        </i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--结果展示 end-->
    <!-- 提示模态框（Modal） -->
    <div class="modal fade" id="tipsModal" tabindex="-1" role="dialog" aria-labelledby="tipsModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="tipsModalLabel">提示</h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="tipsMessage"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" id="tipsConfirmBtn">确定</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 详情模态框（Modal） -->
    <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="showModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="showMessage">
                    <jsp:include page="tbXdryChxxShow.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 编辑模态框（Modal） -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="editModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="editMessage">
                    <jsp:include page="tbXdryChxxForm.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" id="editConfirmBtn">保存</button>
                </div>
            </div>
        </div>
    </div>
    
    <!-- 字典、树形字典展示框。如果page、form均无字典项，可无需此div -->
    <div id="treeDiv">
        <input type="text" class="form-control" id="searchTree"  placeholder="支持文本检索">
        <div id="tree"></div>
    </div>
</section>
<script>
    var ctx = "${ctx}";
</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/jdzh/tbXdryChxx.js"></script>
</body>
</html>