<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
</head>
<body>
<!-- 主体内容 -->
<section class="content">
	<!--查询项表单 begin-->
    <div class="row">
        <div class="col-xs-12">
            <ul class="box clearfix">
                <form class="form_chag form-inline" name="tbDpajSdryChxxQueryForm" id="tbDpajSdryChxxQueryForm">
                	<!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
                	<!-- 请根据需要修改 查询检索项 -->
                    	<div class="form-group">
	                        <label for="jlbh">记录编号--DExxxxx--编号规则为“XXXXXXXXXXXX（公安机关机构代码）+YYYYMM（年月）+XXXXXXXXXXXXXX（顺序号）”                         </label>
	                        <input type="text" class="form-control" id="jlbh" name="jlbh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ajbh">立案编号--DE00092--编号规则为“A+XXXXXXXXXXXX（公安机关机构代码）+YYYYMM（年月）+XXXX（顺序号）”                         </label>
	                        <input type="text" class="form-control" id="ajbh" name="ajbh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="xyrbh">涉毒人员编号--DE00038--编号规则为：R+XXXXXXXXXXXX(公安机关机构代码)+XXXXXX(年月)+XXXX(顺序号)              </label>
	                        <input type="text" class="form-control" id="xyrbh" name="xyrbh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="tbdw_dwmc">填报单位名称--DE00065--</label>
	                        <input type="text" class="form-control" id="tbdw_dwmc" name="tbdw_dwmc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="djr_xm">登记人--DE00002--</label>
	                        <input type="text" class="form-control" id="djr_xm" name="djr_xm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="djrq">登记日期--DE00554--</label>
	                        <input type="text" class="form-control" id="djrq" name="djrq" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sdlx">涉毒类型--DExxxxx--</label>
	                        <input type="text" class="form-control" id="sdlx" name="sdlx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="chrq">查获日期--DE00554--</label>
	                        <input type="text" class="form-control" id="chrq" name="chrq" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ddmc">查获地点--DE00768--</label>
	                        <input type="text" class="form-control" id="ddmc" name="ddmc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="hdqh_xzqh">活动区域--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码</label>
	                        <input type="text" class="form-control" id="hdqh_xzqh" name="hdqh_xzqh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="chdw_dwmc">查获单位--DE00065--</label>
	                        <input type="text" class="form-control" id="chdw_dwmc" name="chdw_dwmc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="czqk">处置情况--DExxxxx--</label>
	                        <input type="text" class="form-control" id="czqk" name="czqk" >
	                    </div>
                    	<div class="form-group">
	                        <label for="chd_xzqh">查获地区--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码</label>
	                        <input type="text" class="form-control" id="chd_xzqh" name="chd_xzqh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="czrq">处置日期--DE00554--</label>
	                        <input type="text" class="form-control" id="czrq" name="czrq" >
	                    </div>
                    	<div class="form-group">
	                        <label for="jyaq">违法犯罪事实--DE00100--</label>
	                        <input type="text" class="form-control" id="jyaq" name="jyaq" >
	                    </div>
                    	<div class="form-group">
	                        <label for="khyhmc">银行卡发卡银行--DE00243--</label>
	                        <input type="text" class="form-control" id="khyhmc" name="khyhmc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="xykh">银行卡号--DExxxxx--</label>
	                        <input type="text" class="form-control" id="xykh" name="xykh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="yhzh">银行账号--DE00241--</label>
	                        <input type="text" class="form-control" id="yhzh" name="yhzh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="jdchphm">车牌号--DE00307--</label>
	                        <input type="text" class="form-control" id="jdchphm" name="jdchphm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="xdjlhcjgdm">是否吸毒--DE00838--采用GA/T XXXXX-XXXX《吸毒记录核查结果代码》</label>
	                        <input type="text" class="form-control" id="xdjlhcjgdm" name="xdjlhcjgdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ztrybh">在逃人员编号--DExxxxx、DE00618--</label>
	                        <input type="text" class="form-control" id="ztrybh" name="ztrybh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="lad_xzqh">立案地--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码</label>
	                        <input type="text" class="form-control" id="lad_xzqh" name="lad_xzqh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="swzjrq">上网追逃日期--DE00554--</label>
	                        <input type="text" class="form-control" id="swzjrq" name="swzjrq" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zhd_xzqh">抓获地--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码</label>
	                        <input type="text" class="form-control" id="zhd_xzqh" name="zhd_xzqh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="tjjbdm">通缉级别--DE00232--采用GA240.54-2003《通缉级别代码》</label>
	                        <input type="text" class="form-control" id="tjjbdm" name="tjjbdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zhfsdm">抓获方式--DE01017--采用 GA/T XXX《抓获方式代码》</label>
	                        <input type="text" class="form-control" id="zhfsdm" name="zhfsdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="xstgfsdm">抓获线索来源--DE00170--采用GA 332.9 《禁毒信息管理代码 第9部分:线索提供方式代码》</label>
	                        <input type="text" class="form-control" id="xstgfsdm" name="xstgfsdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zcxwnrms">抓获经过--DE01012--</label>
	                        <input type="text" class="form-control" id="zcxwnrms" name="zcxwnrms" >
	                    </div>
                    	<div class="form-group">
	                        <label for="hjd_xzqh">户籍地--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码</label>
	                        <input type="text" class="form-control" id="hjd_xzqh" name="hjd_xzqh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sftp">是否逃跑--DExxxxx--</label>
	                        <input type="text" class="form-control" id="sftp" name="sftp" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sphzxqk">审判后执行情况--DExxxxx--</label>
	                        <input type="text" class="form-control" id="sphzxqk" name="sphzxqk" >
	                    </div>
                    	<div class="form-group">
	                        <label for="yjd_xzqh">移交地区--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码</label>
	                        <input type="text" class="form-control" id="yjd_xzqh" name="yjd_xzqh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="jhzazzy">结伙作案中作用--DExxxxx--</label>
	                        <input type="text" class="form-control" id="jhzazzy" name="jhzazzy" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sftsrq">是/否特殊人群--DExxxxx--</label>
	                        <input type="text" class="form-control" id="sftsrq" name="sftsrq" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sfgwyjry">是/否高危预警人员--DExxxxx--</label>
	                        <input type="text" class="form-control" id="sfgwyjry" name="sfgwyjry" >
	                    </div>
                    	<div class="form-group">
	                        <label for="dbzh">逮捕证号--DExxxxx--</label>
	                        <input type="text" class="form-control" id="dbzh" name="dbzh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="chdwdm">查获单位代码</label>
	                        <input type="text" class="form-control" id="chdwdm" name="chdwdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="tbdw_dwdm">填报单位</label>
	                        <input type="text" class="form-control" id="tbdw_dwdm" name="tbdw_dwdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="xqn">刑期-年</label>
	                        <input type="text" class="form-control" id="xqn" name="xqn" >
	                    </div>
                    	<div class="form-group">
	                        <label for="xqy">刑期-月</label>
	                        <input type="text" class="form-control" id="xqy" name="xqy" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zxqzrq">执行截止日期</label>
	                        <input type="text" class="form-control" id="zxqzrq" name="zxqzrq" >
	                    </div>
                    	<div class="form-group">
	                        <label for="gxsj">更新时间</label>
	                        <input type="text" class="form-control" id="gxsj" name="gxsj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="yxx">有效性</label>
	                        <input type="text" class="form-control" id="yxx" name="yxx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="gxsjc">更新时间戳--DEXXXXX--</label>
	                        <input type="text" class="form-control" id="gxsjc" name="gxsjc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="xgbsdw">修改部署单位--DEXXXXX--</label>
	                        <input type="text" class="form-control" id="xgbsdw" name="xgbsdw" >
	                    </div>
                    	<div class="form-group">
	                        <label for="flwsbh">法律文书编号</label>
	                        <input type="text" class="form-control" id="flwsbh" name="flwsbh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sfzf">是否主犯</label>
	                        <input type="text" class="form-control" id="sfzf" name="sfzf" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sfzt">是否在逃</label>
	                        <input type="text" class="form-control" id="sfzt" name="sfzt" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sftsrqlx">字段</label>
	                        <input type="text" class="form-control" id="sftsrqlx" name="sftsrqlx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sfjhgk">字段</label>
	                        <input type="text" class="form-control" id="sfjhgk" name="sfjhgk" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zhsj">字段</label>
	                        <input type="text" class="form-control" id="zhsj" name="zhsj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zhd">字段</label>
	                        <input type="text" class="form-control" id="zhd" name="zhd" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zhd_xxdq">字段</label>
	                        <input type="text" class="form-control" id="zhd_xxdq" name="zhd_xxdq" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zhd_xxdz">字段</label>
	                        <input type="text" class="form-control" id="zhd_xxdz" name="zhd_xxdz" >
	                    </div>
                    	<div class="form-group">
	                        <label for="fzqk">字段</label>
	                        <input type="text" class="form-control" id="fzqk" name="fzqk" >
	                    </div>
                    	<div class="form-group">
	                        <label for="fzlx">字段</label>
	                        <input type="text" class="form-control" id="fzlx" name="fzlx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="czbs">操作标识</label>
	                        <input type="text" class="form-control" id="czbs" name="czbs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ryly">字段</label>
	                        <input type="text" class="form-control" id="ryly" name="ryly" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cszt">传输状态。0:未传输。1:已传输（新老系统数据传输）</label>
	                        <input type="text" class="form-control" id="cszt" name="cszt" >
	                    </div>
                    	<div class="form-group">
	                        <label for="gjdm">字段</label>
	                        <input type="text" class="form-control" id="gjdm" name="gjdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sdryjlbh">字段</label>
	                        <input type="text" class="form-control" id="sdryjlbh" name="sdryjlbh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="chdwxc">字段</label>
	                        <input type="text" class="form-control" id="chdwxc" name="chdwxc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sftsrq_old">旧系统特殊人群代码</label>
	                        <input type="text" class="form-control" id="sftsrq_old" name="sftsrq_old" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sjly">字段</label>
	                        <input type="text" class="form-control" id="sjly" name="sjly" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ly">来源(1：接口下发2：本地录入)</label>
	                        <input type="text" class="form-control" id="ly" name="ly" >
	                    </div>
                                        
                    <div class="form-group ">
                        <button type="button" style="margin-left: 8%" class="btn btn-info" onclick="search();">检索</button>
                    </div>
                </form>
            </ul>
        </div>
    </div>
	<!--查询项表单 end-->
	<!--结果展示 begin-->
	<div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="tbDpajSdryChxxGrid" class="table table-bordered table-striped table-condensed">

                    </table>
                    <!--toolbar -->
                    <div class="btn-group" id="toolbar">
                        <i class="btn" onclick="addTbDpajSdryChxx();">
                            <i class="fa fa-plus-square fa-2x"></i>
                        </i>
                        <i class="btn" onclick="exportTbDpajSdryChxx();">
                            <i class="fa fa-cloud-download fa-2x"></i>
                        </i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--结果展示 end-->
    <!-- 提示模态框（Modal） -->
    <div class="modal fade" id="tipsModal" tabindex="-1" role="dialog" aria-labelledby="tipsModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="tipsModalLabel">提示</h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="tipsMessage"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" id="tipsConfirmBtn">确定</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 详情模态框（Modal） -->
    <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="showModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="showMessage">
                    <jsp:include page="tbDpajSdryChxxShow.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 编辑模态框（Modal） -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="editModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="editMessage">
                    <jsp:include page="tbDpajSdryChxxForm.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" id="editConfirmBtn">保存</button>
                </div>
            </div>
        </div>
    </div>
    
    <!-- 字典、树形字典展示框。如果page、form均无字典项，可无需此div -->
    <div id="treeDiv">
        <input type="text" class="form-control" id="searchTree"  placeholder="支持文本检索">
        <div id="tree"></div>
    </div>
</section>
<script>
    var ctx = "${ctx}";
</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/jdzh/tbDpajSdryChxx.js"></script>
</body>
</html>