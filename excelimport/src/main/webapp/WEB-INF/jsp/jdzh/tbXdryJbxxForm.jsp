<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html>
<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
</head>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-closeable-tab.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/jdzh/jdzh.js"></script>
<script>
    $(function () {
        var curdType = '${type}';
        var pk = '${pk}';
        $("#editConfirmBtn").on("click",function(){
            tbXdryJbxxEditSubmit();
        });
        $("#type").val(curdType);
        $("#pk").val(pk);
    });
    var ctx = "${ctx}";
</script>
<body>
    <form class="form_chag" id="tbXdryJbxxEditForm" name="tbXdryJbxxEditForm">
        <input type="hidden" id="type" name="curdType" >
        <input type="hidden" id="pk" name="pk" >
        <c:if test="${type=='insert'}">
            <input type="hidden" id="yxx" name="yxx" value="1">
            <input type="hidden" id="ly" name="ly" value="2">
            <input type="hidden" id="ccbz" name="ccbz" value="2222222">
        </c:if>
        <table class="table table-bordered">
			<tr>
        		<td class="sec_tit">
                    <label for="edit_xdryly">人员类型<i class="req"> *</i></label>
                </td>
                <td>
                    <div class="has-feedback form_controls" >
                        <input type="hidden" value="${xdry.xdryly}" id="edit_xdryly" name="xdryly" />
                        <input type="text" value='${dicMap["XDRYLY"][xdry.xdryly]}' class="required form-control" id="xdrylymc" data-jzd-type="code" data-jzd-code="XDRYLY" data-jzd-filter="" data-jzd-dm="edit_xdryly" data-jzd-mc="xdrylymc" data-valid="isNonEmpty" data-error="不能为空" readonly>
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_xdryly');clearInput('xdrylymc');"></span>
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_xyrbh">嫌疑人人编号<i class="req"> *</i></label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" onblur="check(this,2)" value="${xdry.xyrbh}" class="required form-control" id="edit_xyrbh" name="xyrbh" data-tip="" data-valid="isNonEmpty" data-error="不能为空" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_xm">姓名<i class="req"> *</i></label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" value="${xdry.xm}" class="required form-control" id="edit_xm" name="xm" data-tip="" data-valid="isNonEmpty" data-error="不能为空" >
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_cyzjdm">证件种类<i class="req"> *</i></label>
                </td>
                <td>
                    <div class="has-feedback form_controls" >
                        <input type="hidden" value="${xdry.cyzjdm}" id="edit_cyzjdm" name="cyzjdm" />
                        <input type="text" value='${dicMap["CYZJDM"][xdry.cyzjdm]}' class="required form-control" id="cyzjmc" data-jzd-type="code" data-jzd-code="CYZJDM" data-jzd-filter="" data-jzd-dm="edit_cyzjdm" data-jzd-mc="cyzjmc" data-valid="isNonEmpty" data-error="不能为空" readonly>
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_cyzjdm');clearInput('cyzjmc');"></span>
                    </div>
                </td>
			</tr>
			<tr>
                <td class="sec_tit">
                    <label for="edit_zjhm">证件号码<i class="req"> *</i></label>
                </td>
                <td>
                    <c:if test="${xdry.cyzjdm=='111'}">
                        <input type="text" value="${xdry.sfzhm18}" class="required form-control" id="edit_zjhm" name="zjhm" data-tip="" data-valid="isNonEmpty" data-error="不能为空" >
                    </c:if>
                    <c:if test="${xdry.cyzjdm!='111'}">
                        <input type="text" value="${xdry.zjhm}" class="required form-control" id="edit_zjhm" name="zjhm" data-tip="" data-valid="isNonEmpty" data-error="不能为空" >
                    </c:if>
                </td>
                <td class="sec_tit">
                    <label for="edit_bmch">别名/绰号</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" value="${xdry.bmch}" class="form-control" id="edit_bmch" name="bmch" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
                <td class="sec_tit">
                    <label for="edit_cyzk">从业状况</label>
                </td>
                <td>
                    <div class="has-feedback form_controls" >
                        <input type="hidden" value="${xdry.cyzk}" id="edit_cyzk" name="cyzk" />
                        <input type="text" value='${dicMap["CYZK"][xdry.cyzk]}' class="form-control" id="cyzkmc" data-jzd-type="code" data-jzd-code="CYZK" data-jzd-filter="" data-jzd-dm="edit_cyzk" data-jzd-mc="cyzkmc" data-valid="" data-error="" readonly>
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_cyzk');clearInput('cyzkmc');"></span>
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_xbdm">性别</label>
                </td>
                <td>
                    <div class="has-feedback form_controls" >
                        <input type="hidden" value="${xdry.xbdm}" id="edit_xbdm" name="xbdm" />
                        <input type="text" value='${dicMap["XB"][xdry.xbdm]}' class="form-control" id="xbmc" data-jzd-type="code" data-jzd-code="XB" data-jzd-filter="" data-jzd-dm="edit_xbdm" data-jzd-mc="xbmc" data-valid="isNonEmpty" data-error="不能为空" readonly>
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_xbdm');clearInput('xbmc');"></span>
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_csrq">出生日期</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" value="<fmt:formatDate value="${xdry.csrq}" pattern="yyyy-MM-dd"/>" class="form-control" id="edit_csrq" name="csrq" onFocus="WdatePicker({lang:'zh-cn',readOnly:true,dateFmt:'yyyy-MM-dd'})" style="!important" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_mzdm">民族</label>
                </td>
                <td>
                    <div class="has-feedback form_controls" >
                        <input type="hidden" value="${xdry.mzdm}" id="edit_mzdm" name="mzdm" />
                        <input type="text" value='${dicMap["MZ"][xdry.mzdm]}' class="form-control" id="mzmc" data-jzd-type="code" data-jzd-code="MZ" data-jzd-filter="" data-jzd-dm="edit_mzdm" data-jzd-mc="mzmc" data-valid="" data-error="" readonly>
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_mzdm');clearInput('mzmc');"></span>
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_xldm">文化程度</label>
                </td>
                <td>
                    <div class="has-feedback form_controls" >
                        <input type="hidden" value="${xdry.xldm}" id="edit_xldm" name="xldm" />
                        <input type="text" value='${dicMap["WHCD"][xdry.xldm]}' class="form-control" id="xlmc" data-jzd-type="code" data-jzd-code="WHCD" data-jzd-filter="" data-jzd-dm="edit_xldm" data-jzd-mc="xlmc" data-valid="" data-error="" readonly>
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_xldm');clearInput('xlmc');"></span>
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_gzdw_dwmc">工作单位名称</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" value="${xdry.gzdw_dwmc}" class="form-control" id="edit_gzdw_dwmc" name="gzdw_dwmc" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_szzwbh">指纹编号</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" value="${xdry.szzwbh}" class="form-control" id="edit_szzwbh" name="szzwbh" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_rydnabh">DNA编号</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" value="${xdry.rydnabh}" class="form-control" id="edit_rydnabh" name="rydnabh" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_hjdz_xzqhdm">户籍地址</label>
                </td>
                <td>
                    <div class="has-feedback form_controls" >
                        <input type="hidden" value="${xdry.hjdz_xzqhdm}" id="edit_hjdz_xzqhdm" name="hjdz_xzqhdm" />
                        <input type="text" value='${xdry.hjdz_xzqhmc}' class="form-control" id="hjdz_xzqhmc" name="hjdz_xzqhmc" data-jzd-type="tree" data-jzd-code="XZQH" data-jzd-filter="" data-jzd-dm="edit_hjdz_xzqhdm" data-jzd-mc="hjdz_xzqhmc" data-valid="isNonEmpty" data-error="不能为空" readonly>
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_hjdz_xzqhdm');clearInput('hjdz_xzqhmc');"></span>
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_hjdz_gajgdm">户籍地址公安机关名称</label>
                </td>
                <td>
                    <div class="has-feedback form_controls" >
                        <input type="hidden" value="${xdry.hjdz_gajgdm}" id="edit_hjdz_gajgdm" name="hjdz_gajgdm" />
                        <input type="text" value='${xdry.hjdz_gajgmc}' class="form-control" id="hjdz_gajgmc" name="hjdz_gajgmc" data-jzd-type="tree" data-jzd-code="DEPT" data-jzd-filter="" data-jzd-dm="edit_hjdz_gajgdm" data-jzd-mc="hjdz_gajgmc" data-valid="isNonEmpty" data-error="不能为空" readonly>
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_hjdz_gajgdm');clearInput('hjdz_gajgmc');"></span>
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_sjjzd_xzqhdm">实际居住地</label>
                </td>
                <td>
                    <div class="has-feedback form_controls">
                        <input type="hidden" value="${xdry.sjjzd_xzqhdm}" id="edit_sjjzd_xzqhdm" name="sjjzd_xzqhdm" />
                        <input type="text" value='${xdry.sjjzd_xzqhmc}' class="form-control" id="sjjzd_xzqhmc" name="sjjzd_xzqhmc" data-jzd-type="tree" data-jzd-code="XZQH" data-jzd-filter="" data-jzd-dm="edit_sjjzd_xzqhdm" data-jzd-mc="sjjzd_xzqhmc" data-valid="isNonEmpty" data-error="不能为空" readonly>
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_sjjzd_xzqhdm');clearInput('sjjzd_xzqhmc');"></span>
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_sjjzd_gajgdm">实际居住地公安机关名称</label>
                </td>
                <td>
                    <div class="has-feedback form_controls" >
                        <input type="hidden" value="${xdry.sjjzd_gajgdm}" id="edit_sjjzd_gajgdm" name="sjjzd_gajgdm" />
                        <input type="text" value='${xdry.sjjzd_gajgmc}' class="form-control" id="sjjzd_gajgmc" name="sjjzd_gajgmc" data-jzd-type="tree" data-jzd-code="DEPT" data-jzd-filter="" data-jzd-dm="edit_sjjzd_gajgdm" data-jzd-mc="sjjzd_gajgmc" data-valid="isNonEmpty" data-error="不能为空" readonly>
                        <span class="glyphicon glyphicon-remove form-control-feedback" onclick="clearInput('edit_sjjzd_gajgdm');clearInput('sjjzd_gajgmc');"></span>
                    </div>
                </td>
			</tr>
            <tr>
                <td class="sec_tit">
                    <label for="edit_hjdz_dzmc">户籍详址</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" value="${xdry.hjdz_dzmc}" class="form-control" id="edit_hjdz_dzmc" name="hjdz_dzmc" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_sjjzd_dzmc">实际居住地详址</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" value="${xdry.sjjzd_dzmc}" class="form-control" id="edit_sjjzd_dzmc" name="sjjzd_dzmc" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
            </tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_lrr_xm">录入人姓名</label>
                </td>
                <td>
                	<div class="form_controls">
                        <c:if test="${type=='update'}">
                            <input type="text" value="${xdry.lrr_xm}" class="form-control" id="edit_lrr_xm" name="lrr_xm" data-valid="" data-error="" readonly>
                        </c:if>
                        <c:if test="${type=='insert'}">
                            <input type="text" value="${djdw}" class="form-control" id="edit_lrr_xm" name="lrr_xm" data-valid="" data-error="" readonly>
                        </c:if>
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_lrdw_dwmc">录入单位单位名称</label>
                </td>
                <td>
                	<div class="form_controls">
                        <c:if test="${type=='update'}">
                            <input type="text" value="${xdry.lrdw_dwmc}" class="form-control" id="edit_lrdw_dwmc" name="lrdw_dwmc" data-valid="" data-error="" readonly>
                        </c:if>
                        <c:if test="${type=='insert'}">
                            <input type="text" value="${djdw}" class="form-control" id="edit_lrdw_dwmc" name="lrdw_dwmc" data-valid="" data-error="" readonly>
                        </c:if>
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_lrsj">录入时间</label>
                </td>
                <td>
                	<div class="form_controls">
                        <c:if test="${type=='update'}">
                            <input type="text" value="<fmt:formatDate value="${xdry.lrsj}" pattern="yyyy-MM-dd HH:mm:ss"/>" class="form-control" id="edit_lrsj" name="lrsj"  data-tip="" data-valid="" data-error="" readonly>
                        </c:if>
                        <c:if test="${type=='insert'}">
                            <input type="text" value="<fmt:formatDate value="${now}" pattern="yyyy-MM-dd HH:mm:ss"/>" class="form-control" id="edit_lrsj" name="lrsj"  data-tip="" data-valid="" data-error="" readonly>
                        </c:if>
                    </div>
                </td>
        		<td class="sec_tit">
                    <label for="edit_sf828k">是否828库人员</label>
                </td>
                <td>
                	<div class="form_controls">
                        <select class="form-control" id="edit_sf828k" name="sf828k">
                                <option></option>
                            <c:choose>
                                <c:when test="${xdry.sf828k==0}">
                                    <option value="0" selected>否</option>
                                    <option value="1">是</option>
                                </c:when >
                                <c:when test="${xdry.sf828k==1}">
                                    <option value="0">否</option>
                                    <option value="1" selected>是</option>
                                </c:when >
                                <c:otherwise>
                                    <option value="0">否</option>
                                    <option value="1">是</option>
                                </c:otherwise>
                            </c:choose>
                        </select>
                    </div>
                </td>
			</tr>
			<tr>
        		<td class="sec_tit">
                    <label for="edit_sdrybh">涉毒人员编号</label>
                </td>
                <td>
                	<div class="form_controls">
                    	<input type="text" value="${xdry.sdrybh}" class="form-control" id="edit_sdrybh" name="sdrybh" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
                <td class="sec_tit">
                    <label for="edit_gsdw">归属单位</label>
                </td>
                <td>
                    <div class="form_controls">
                        <input type="text" value="${xdry.gsdw}" class="form-control" id="edit_gsdw" name="gsdw" data-tip="" data-valid="" data-error="" >
                    </div>
                </td>
			</tr>
            <c:if test="${type=='update'}">
                <tr>
                    <td class="sec_tit">
                        <label for="edit_bdxgr">本地修改人姓名</label>
                    </td>
                    <td>
                        <div class="form_controls">
                            <input type="text" value="${djrxm}" class="form-control" id="edit_bdxgr" name="bdxgr" data-tip="" data-valid="" data-error="" readonly>
                        </div>
                    </td>
                    <td class="sec_tit">
                        <label for="edit_bdxgrq">本地修改日期</label>
                    </td>
                    <td>
                        <div class="form_controls">
                            <input type="text" value="<fmt:formatDate value="${now}" pattern="yyyy-MM-dd HH:mm:ss"/>" class="form-control" id="edit_bdxgrq" name="bdxgrq" data-tip="" data-valid="" data-error="" readonly>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="sec_tit">
                        <label for="edit_bdxgrdwmc">本地修改人单位名称</label>
                    </td>
                    <td>
                        <div class="form_controls">
                            <input type="text" value="${djdw}" class="form-control" id="edit_bdxgrdwmc" name="bdxgrdwmc" data-tip="" data-valid="" data-error="" readonly>
                        </div>
                    </td>
                    <td class="sec_tit">
                        <label for="edit_bdxgrdwdm">本地修改人单位代码</label>
                    </td>
                    <td>
                        <div class="form_controls">
                            <input type="text" value="${djdwdm}" class="form-control" id="edit_bdxgrdwdm" name="bdxgrdwdm" data-tip="" data-valid="" data-error="" readonly>
                        </div>
                    </td>
                </tr>
            </c:if>
        </table>
    </form>
    <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="editConfirmBtn">保存</button>
    </div>
    <!-- 字典、树形字典展示框。如果page、form均无字典项，可无需此div -->
    <div id="treeDiv">
        <input type="text" class="form-control" id="searchTree"  placeholder="支持文本检索">
        <div id="tree"></div>
    </div>
</body>
</html>
