<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html>
<head>
    <title>title</title>
</head>
<script>
	$(function () {
		var dic = ${dict};
		var sdlx = '${sdch.sdlx}';
		var sdlxs = sdlx.split("/");
		sdlxs.splice(0,1);
		var str = '';
		if (sdlx!=null&&sdlx!=''){
			$.each(sdlxs,function(index,value){
				var fy = dic["SDLX"][value];
				if (fy!=undefined&&fy!=''){
					str += fy+',';
				}
			});
			str = str.substring(0,str.length-1);
			$("#show_sdlx").text(str);
		}
		var czqk = '${sdch.czqk}';
		var czqks = czqk.split("/");
		czqks.splice(0,1);
		var str = '';
		if (czqk!=null&&czqk!=''){
			$.each(czqks,function(index,value){
				var fy = dic["CZQK"][value];
				if (fy!=undefined&&fy!=''){
					str += fy+',';
				}
			});
			str = str.substring(0,str.length-1);
			$("#show_czqk").text(str);
		}
	});
</script>
<div class="banner_title">
	<h3 style="color: #00acd6">查获信息</h3>
</div>
<c:if test="${empty sdch}">
	<h6 style="color: red">无查获信息</h6>
</c:if>
<c:if test="${not empty sdch}">
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">立案编号</td>
            <td id="show_ajbh">${sdch.ajbh}</td>
			<td class="sec_tit">涉毒人员编号</td>
			<td id="show_xyrbh">${sdch.xyrbh}</td>
		</tr>
		<tr>
    		<td class="sec_tit">填报单位名称</td>
            <td id="show_tbdw_dwmc">${sdch.tbdw_dwmc}</td>
			<td class="sec_tit">登记人</td>
			<td id="show_djr_xm">${sdch.djr_xm}</td>
		</tr>
		<tr>
    		<td class="sec_tit">登记日期</td>
            <td id="show_djrq"><fmt:formatDate value="${sdch.djrq}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
			<td class="sec_tit">涉毒类型</td>
			<td id="show_sdlx"></td>
		</tr>
		<tr>
    		<td class="sec_tit">查获日期</td>
            <td id="show_chrq"><fmt:formatDate value="${sdch.chrq}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
			<td class="sec_tit">查获地点</td>
			<td id="show_ddmc">${sdch.ddmc}</td>
		</tr>
		<tr>
    		<td class="sec_tit">查获单位</td>
            <td id="show_chdw_dwmc">${sdch.chdw_dwmc}</td>
    		<td class="sec_tit">处置情况</td>
            <td id="show_czqk"></td>
		</tr>
		<tr>
    		<td class="sec_tit">查获地区</td>
            <td id="show_chd_xzqh">${dicMap["XZQH"][sdch.chd_xzqh]}</td>
    		<td class="sec_tit">处置日期</td>
            <td id="show_czrq"><fmt:formatDate value="${sdch.czrq}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
		</tr>
		<tr>
    		<td class="sec_tit">违法犯罪事实</td>
            <td colspan="3" id="show_jyaq">${sdch.jyaq}</td>
		</tr>
		<tr>
			<td class="sec_tit">活动区域</td>
			<td id="show_hdqh_xzqh">${dicMap["XZQH"][sdch.hdqh_xzqh]}</td>
			<td class="sec_tit">银行卡发卡银行</td>
			<td id="show_khyhmc">${sdch.khyhmc}</td>
		</tr>
		<tr>
    		<td class="sec_tit">银行卡号</td>
            <td id="show_xykh">${sdch.xykh}</td>
    		<td class="sec_tit">银行账号</td>
            <td id="show_yhzh">${sdch.yhzh}</td>
		</tr>
		<tr>
    		<td class="sec_tit">车牌号</td>
            <td id="show_jdchphm">${sdch.jdchphm}</td>
    		<td class="sec_tit">是否吸毒</td>
            <td id="show_xdjlhcjgdm">
				<c:if test="${sdch.xdjlhcjgdm=='0'}">
					否
				</c:if>
				<c:if test="${sdch.xdjlhcjgdm=='1'}">
					是
				</c:if>
			</td>
		</tr>
		<tr>
    		<td class="sec_tit">在逃人员编号</td>
            <td id="show_ztrybh">${sdch.ztrybh}</td>
    		<td class="sec_tit">立案地</td>
            <td id="show_lad_xzqh">${dicMap["XZQH"][sdch.lad_xzqh]}</td>
		</tr>
		<tr>
    		<td class="sec_tit">上网追逃日期</td>
            <td id="show_swzjrq"><fmt:formatDate value="${sdch.swzjrq}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
    		<td class="sec_tit">抓获地</td>
            <td id="show_zhd_xzqh">${dicMap["XZQH"][sdch.zhd_xzqh]}</td>
		</tr>
		<tr>
    		<td class="sec_tit">抓获方式</td>
            <td id="show_zhfsdm">${dicMap["ZHFSDM"][sdch.zhfsdm]}</td>
			<td class="sec_tit">抓获线索来源</td>
			<td id="show_xstgfsdm">${dicMap["XSTGFSDM"][sdch.xstgfsdm]}</td>
		</tr>
		<tr>
    		<td class="sec_tit">抓获经过</td>
            <td id="show_zcxwnrms">${sdch.zcxwnrms}</td>
			<td class="sec_tit">户籍地</td>
			<td id="show_hjd_xzqh">${dicMap["XZQH"][sdch.hjd_xzqh]}</td>
		</tr>
		<tr>
    		<td class="sec_tit">是否逃跑</td>
            <td id="show_sftp">
				<c:if test="${sdch.sftp=='0'}">
					否
				</c:if>
				<c:if test="${sdch.sftp=='1'}">
					是
				</c:if>
			</td>
			<td class="sec_tit">审判后执行情况</td>
			<td id="show_sphzxqk">${sdch.sphzxqk}</td>
		</tr>
		<tr>
    		<td class="sec_tit">移交地区</td>
            <td id="show_yjd_xzqh">${dicMap["XZQH"][sdch.yjd_xzqh]}</td>
			<td class="sec_tit">结伙作案中作用</td>
			<td id="show_jhzazzy">${sdch.jhzazzy}</td>
		</tr>
		<tr>
    		<td class="sec_tit">是/否特殊人群</td>
            <td id="show_sftsrq">
				<c:if test="${sdch.sftsrq=='0'}">
					否
				</c:if>
				<c:if test="${sdch.sftsrq=='1'}">
					是
				</c:if>
			</td>
			<td class="sec_tit">是/否高危预警人员</td>
			<td id="show_sfgwyjry">
				<c:if test="${sdch.sfgwyjry=='0'}">
					否
				</c:if>
				<c:if test="${sdch.sfgwyjry=='1'}">
					是
				</c:if>
			</td>
		</tr>
		<tr>
    		<td class="sec_tit">逮捕证号</td>
            <td id="show_dbzh">${sdch.dbzh}</td>
			<td class="sec_tit">查获单位代码</td>
			<td id="show_chdwdm">${sdch.chdwdm}</td>
		</tr>
		<tr>
    		<td class="sec_tit">填报单位</td>
            <td id="show_tbdw_dwdm">${sdch.tbdw_dwdm}</td>
			<td class="sec_tit">执行截止日期</td>
			<td id="show_zxqzrq"><fmt:formatDate value="${sdch.zxqzrq}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
		</tr>
		<tr>
    		<td class="sec_tit">刑期-年</td>
            <td id="show_xqn">${sdch.xqn}</td>
    		<td class="sec_tit">刑期-月</td>
            <td id="show_xqy">${sdch.xqy}</td>
		</tr>
		<tr>
    		<td class="sec_tit">法律文书编号</td>
            <td id="show_flwsbh">${sdch.flwsbh}</td>
			<td class="sec_tit">是否主犯</td>
			<td id="show_sfzf">
				<c:if test="${sdch.sfzf=='0'}">
					否
				</c:if>
				<c:if test="${sdch.sfzf=='1'}">
					是
				</c:if>
			</td>
		</tr>
		<tr>
    		<td class="sec_tit">是否在逃</td>
            <td id="show_sfzt">
				<c:if test="${sdch.sfzt=='0'}">
					否
				</c:if>
				<c:if test="${sdch.sfzt=='1'}">
					是
				</c:if>
			</td>
			<td class="sec_tit">来源</td>
			<td id="show_ly">
				<c:if test="${sdch.ly==1}">
					接口下发
				</c:if>
				<c:if test="${sdch.ly==2}">
					本地录入
				</c:if>
			</td>
		</tr>
    </table>
</c:if>
</html>
