<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html>
<head>
	<meta charset="UTF-8">
	<title>title</title>
	<link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
	<link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
	<link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
	<link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
	<link rel="stylesheet" href="${ctx}/css/pub/page.css">
	<link rel="stylesheet" href="${ctx}/css/pub/code.css">
</head>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script>
    $(function () {
		$("#jdzhchajxx", parent.document).height(document.body.scrollHeight);
        var dic = ${dict};
        var pafs = '${sdaj.pafsdm}';
        var pafss = pafs.split("/");
        pafss.splice(0,1);
        var str = '';
        if (pafs!=null&&pafs!=''){
            $.each(pafss,function(index,value){
            	var fy = dic["PAFSDM"][value];
            	if (fy!=undefined&&fy!=''){
					str += fy+',';
				}
            });
            str = str.substring(0,str.length-1);
            $("#show_pafsdm").text(str);
        }
    });
</script>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">立案编号</td>
            <td id="show_ajbh">${sdaj.ajbh}</td>
			<td class="sec_tit">案件名称</td>
			<td id="show_ajmc">${sdaj.ajmc}</td>
		</tr>
		<tr>
    		<td class="sec_tit">案件类别</td>
            <td id="show_ajlbdm">${dicMap["AJLBDM"][sdaj.ajlbdm]}</td>
			<td class="sec_tit">发现时间</td>
			<td id="show_fxsj"><fmt:formatDate value="${sdaj.fxsj}" pattern="yyyy-MM-dd"/></td>
		</tr>
		<tr>
    		<td class="sec_tit">发案地点详址</td>
            <td id="show_fazdxz">${sdaj.fazdxz}</td>
    		<td class="sec_tit">破案地区</td>
            <td id="show_padq">${dicMap["XZQH"][sdaj.padq]}</td>
		</tr>
		<tr>
    		<td class="sec_tit">立案/承办单位名称</td>
            <td id="show_ladw_dwmc">${sdaj.ladw_dwmc}</td>
			<td class="sec_tit">立案日期</td>
			<td id="show_larq"><fmt:formatDate value="${sdaj.larq}" pattern="yyyy-MM-dd"/></td>
		</tr>
		<tr>
    		<td class="sec_tit">承办人</td>
            <td id="show_cbrxm">${sdaj.cbrxm}</td>
    		<td class="sec_tit">承办人联系信息</td>
            <td id="show_cbr_lxdh">${sdaj.cbr_lxdh}</td>
		</tr>
		<tr>
    		<td class="sec_tit">破案单位名称</td>
            <td id="show_padw_dwmc">${sdaj.padw_dwmc}</td>
    		<td class="sec_tit">破案部门</td>
            <td id="show_badwlxdm">${dicMap["BADWLXDM"][sdaj.badwlxdm]}</td>
		</tr>
		<tr>
    		<td class="sec_tit">破案日期</td>
            <td id="show_parq"><fmt:formatDate value="${sdaj.parq}" pattern="yyyy-MM-dd"/></td>
    		<td class="sec_tit">藏毒方式代码</td>
            <td id="show_cdfsdm">${dicMap["CDFSDM"][sdaj.cdfsdm]}</td>
		</tr>
		<tr>
    		<td class="sec_tit">贩运方式代码</td>
            <td id="show_fyfsdm">${dicMap["FYFSDM"][sdaj.fyfsdm]}</td>
    		<td class="sec_tit">是否团伙/集团作案</td>
            <td id="show_sfjt">
				<c:if test="${sdaj.sfjt==0}">
					否
				</c:if>
				<c:if test="${sdaj.sfjt==1}">
					是
				</c:if>
			</td>
		</tr>
		<tr>
    		<td class="sec_tit">作案团伙/集团类型</td>
            <td id="show_fzthxzdm">${dicMap["FZTHXZDM"][sdaj.fzthxzdm]}</td>
    		<td class="sec_tit">作案团伙/集团名称</td>
            <td id="show_fzthmc">${sdaj.fzthmc}</td>
		</tr>
		<tr>
    		<td class="sec_tit">简要案情</td>
			<td colspan="3" id="show_jyaq">${sdaj.jyaq}</td>
		</tr>
		<tr>
			<td class="sec_tit">见证人提供情况</td>
			<td id="show_jzrtgqk">${sdaj.jzrtgqk}</td>
			<td class="sec_tit">破案方式</td>
			<td id="show_pafsdm"></td>
		</tr>
		<tr>
    		<td class="sec_tit">作案人情况分析</td>
            <td id="show_zarqkfx">${sdaj.zarqkfx}</td>
    		<td class="sec_tit">侦察措施</td>
            <td id="show_asjzcxwlbdm">${sdaj.asjzcxwlbdm}</td>
		</tr>
		<tr>
    		<td class="sec_tit">查结情况</td>
            <td colspan="3" id="show_cjqk">${sdaj.cjqk}</td>
		</tr>
		<tr>
    		<td class="sec_tit">查结日期</td>
            <td id="show_cjrq"><fmt:formatDate value="${sdaj.cjrq}" pattern="yyyy-MM-dd"/></td>
    		<td class="sec_tit">公开方式</td>
            <td id="show_gkfs">${sdaj.gkfs}</td>
		</tr>
		<tr>
    		<td class="sec_tit">完全公开日期</td>
            <td id="show_wqgkrq"><fmt:formatDate value="${sdaj.wqgkrq}" pattern="yyyy-MM-dd"/></td>
			<td class="sec_tit">案件阶段</td>
			<td id="show_zcjddm">${dicMap["ZCJDDM"][sdaj.zcjddm]}</td>
		</tr>
		<tr>
    		<td class="sec_tit">填报单位代码</td>
            <td id="show_tbdw_dwdm">${sdaj.tbdw_dwdm}</td>
			<td class="sec_tit">填报单位名称</td>
			<td id="show_tbdw_dwmc">${sdaj.tbdw_dwmc}</td>
		</tr>
		<tr>
    		<td class="sec_tit">登记人</td>
            <td id="show_djr_xm">${sdaj.djr_xm}</td>
			<td class="sec_tit">登记日期</td>
			<td id="show_djrq"><fmt:formatDate value="${sdaj.djrq}" pattern="yyyy-MM-dd"/></td>
		</tr>
		<tr>
    		<td class="sec_tit">审核单位名称</td>
            <td id="show_shdw_dwmc">${sdaj.shdw_dwmc}</td>
    		<td class="sec_tit">审核单位代码</td>
            <td id="show_shdw_dwdm">${sdaj.shdw_dwdm}</td>
		</tr>
		<tr>
    		<td class="sec_tit">缴毒总量（克）</td>
            <td id="show_jhdp">${sdaj.jhdp}</td>
    		<td class="sec_tit">情报提供单位</td>
            <td id="show_qbtgdw_dwmc">${sdaj.qbtgdw_dwmc}</td>
		</tr>
		<tr>
    		<td class="sec_tit">情报提供单位代码</td>
            <td id="show_qbtgdw_dwdm">${sdaj.qbtgdw_dwdm}</td>
    		<td class="sec_tit">贩运渠道说明</td>
            <td id="show_fyqdsm">${sdaj.fyqdsm}</td>
		</tr>
		<tr>
    		<td class="sec_tit">存在不合格</td>
            <td id="show_pdbz">
				<c:if test="${sdaj.pdbz=='0'}">
					否
				</c:if>
				<c:if test="${sdaj.pdbz=='1'}">
					是
				</c:if>
			</td>
    		<td class="sec_tit">案件代号</td>
            <td id="show_ajdh">${sdaj.ajdh}</td>
		</tr>
		<tr>
    		<td class="sec_tit">案件性质</td>
            <td id="show_ajxz">${dicMap["AJXZ"][sdaj.ajxz]}</td>
    		<td class="sec_tit">主侦单位名称</td>
            <td id="show_zzdw_dwmc">${sdaj.zzdw_dwmc}</td>
		</tr>
		<tr>
    		<td class="sec_tit">主侦单位代码</td>
            <td id="show_zzdw_dwdm">${sdaj.zzdw_dwdm}</td>
    		<td class="sec_tit">指导单位名称</td>
            <td id="show_zddw_dwmc">${sdaj.zddw_dwmc}</td>
		</tr>
		<tr>
    		<td class="sec_tit">指导单位代码</td>
            <td id="show_zddw_dwdm">${sdaj.zddw_dwdm}</td>
			<td class="sec_tit">备注</td>
			<td id="show_bz">${sdaj.bz}</td>
		</tr>
		<tr>
    		<td class="sec_tit">涉案地区</td>
            <td id="show_sadq_xzqh">${dicMap["XZQH"][sdaj.sadq_xzqh]}</td>
    		<td class="sec_tit">负责人</td>
            <td id="show_fzrxm">${sdaj.fzrxm}</td>
		</tr>
		<tr>
    		<td class="sec_tit">负责人电话</td>
            <td id="show_fzr_lxdh">${sdaj.fzr_lxdh}</td>
    		<td class="sec_tit">线索来源</td>
            <td id="show_xsly">${dicMap["XSLY"][sdaj.xsly]}</td>
		</tr>
		<tr>
    		<td class="sec_tit">线索来源单位代码</td>
            <td id="show_xsly_dwdm">${sdaj.xsly_dwdm}</td>
    		<td class="sec_tit">线索来源单位名称</td>
            <td id="show_xsly_dwmc">${sdaj.xsly_dwmc}</td>
		</tr>
		<tr>
    		<td class="sec_tit">其它线索来源</td>
            <td id="show_qyxsly">${dicMap["XSLY"][sdaj.qyxsly]}</td>
    		<td class="sec_tit">目前侦查工作进展情况</td>
            <td id="show_mqzcgzjzqk">${sdaj.mqzcgzjzqk}</td>
		</tr>
		<tr>
    		<td class="sec_tit">侦查工作中遇到的主要问题</td>
            <td id="show_zcgzzwt">${sdaj.zcgzzwt}</td>
    		<td class="sec_tit">下一步工作建议</td>
            <td id="show_xybgzjy">${sdaj.xybgzjy}</td>
		</tr>
		<tr>
    		<td class="sec_tit">请示信息内容</td>
            <td id="show_inforcontent">${sdaj.inforcontent}</td>
    		<td class="sec_tit">督办级别</td>
            <td id="show_asjdbjbdm">
				<c:if test="${sdaj.asjdbjbdm==0}">
					部级
				</c:if>
				<c:if test="${sdaj.asjdbjbdm==1}">
					省级
				</c:if>
				<c:if test="${sdaj.asjdbjbdm==2}">
					市级
				</c:if>
			</td>
		</tr>
		<tr>
    		<td class="sec_tit">确立时间</td>
            <td id="show_qlsj"><fmt:formatDate value="${sdaj.qlsj}" pattern="yyyy-MM-dd"/></td>
    		<td class="sec_tit">再次申报请示</td>
            <td id="show_zcsbqs">${sdaj.zcsbqs}</td>
		</tr>
		<tr>
    		<td class="sec_tit">申请联合请示</td>
            <td id="show_sqlhqs">${sdaj.sqlhqs}</td>
			<td class="sec_tit">联合办案</td>
			<td id="show_lhba">${sdaj.lhba}</td>
		</tr>
		<tr>
			<td class="sec_tit">联合协查机构</td>
			<td id="show_lhzc_gajgjgdm">${sdaj.lhzc_gajgjgdm}</td>
    		<td class="sec_tit">破案确立时间</td>
            <td id="show_paqlsj"><fmt:formatDate value="${sdaj.paqlsj}" pattern="yyyy-MM-dd"/></td>
		</tr>
		<tr>
    		<td class="sec_tit">撤销请示</td>
            <td id="show_cxqs">${sdaj.cxqs}</td>
    		<td class="sec_tit">立案/承办地区</td>
            <td id="show_lacbdq">${sdaj.lacbdq}</td>
		</tr>
		<tr>
			<td class="sec_tit">撤案日期</td>
			<td id="show_cxajrq"><fmt:formatDate value="${sdaj.cxajrq}" pattern="yyyy-MM-dd"/></td>
    		<td class="sec_tit">当前审核结果</td>
            <td id="show_dqshjg">${dicMap["DQSHJG"][sdaj.dqshjg]}</td>
		</tr>
		<tr>
    		<td class="sec_tit">修改部署单位</td>
            <td id="show_xgbsdw">${sdaj.xgbsdw}</td>
			<td class="sec_tit">其他说明</td>
			<td id="show_qtsm">${sdaj.qtsm}</td>
		</tr>
		<tr>
    		<td class="sec_tit">是否互联网案件</td>
            <td id="show_sfhlwaj">
				<c:if test="${sdaj.sfhlwaj==0}">
					否
				</c:if>
				<c:if test="${sdaj.sfhlwaj==1}">
					是
				</c:if>
			</td>
    		<td class="sec_tit">互联网案件类型</td>
            <td id="show_hlwajlx">${dicMap["HLWAJLX"][sdaj.hlwajlx]}</td>
		</tr>
		<tr>
    		<td class="sec_tit">是否涉嫌洗钱</td>
            <td id="show_sfsxxq">
				<c:if test="${sdaj.sfsxxq==0}">
					否
				</c:if>
				<c:if test="${sdaj.sfsxxq==1}">
					是
				</c:if>
			</td>
			<td class="sec_tit">涉毒洗钱方式</td>
			<td id="show_sdxqfs">${dicMap["SDXQFS"][sdaj.sdxqfs]}</td>
		</tr>
		<tr>
    		<td class="sec_tit">其他说明(洗钱)</td>
            <td id="show_xqqtsm">${sdaj.xqqtsm}</td>
			<td class="sec_tit">是否黑社会性质</td>
			<td id="show_sfhsh">
				<c:if test="${sdaj.sfhsh==0}">
					否
				</c:if>
				<c:if test="${sdaj.sfhsh==1}">
					是
				</c:if>
			</td>
		</tr>
		<tr>
    		<td class="sec_tit">是否武装贩毒</td>
            <td id="show_sfwzfd">
				<c:if test="${sdaj.sfwzfd==0}">
					否
				</c:if>
				<c:if test="${sdaj.sfwzfd==1}">
					是
				</c:if>
			</td>
			<td class="sec_tit">目标案件操作级别</td>
			<td id="show_mbajzt">
				<c:if test="${sdaj.mbajzt==1}">
					县建议
				</c:if>
				<c:if test="${sdaj.mbajzt==2}">
					县审核
				</c:if>
				<c:if test="${sdaj.mbajzt==3}">
					市建议
				</c:if>
				<c:if test="${sdaj.mbajzt==4}">
					市审核
				</c:if>
				<c:if test="${sdaj.mbajzt==5}">
					省建议
				</c:if>
				<c:if test="${sdaj.mbajzt==6}">
					省审核
				</c:if>
				<c:if test="${sdaj.mbajzt==7}">
					部建议
				</c:if>
				<c:if test="${sdaj.mbajzt==8}">
					部审核
				</c:if>
			</td>
		</tr>
		<tr>
    		<td class="sec_tit">目标案件操作类别</td>
            <td id="show_ajdj">
				<c:if test="${sdaj.ajdj==0}">
					申请目标案件
				</c:if>
				<c:if test="${sdaj.ajdj==1}">
					申请降级
				</c:if>
				<c:if test="${sdaj.ajdj==2}">
					申请破案
				</c:if>
				<c:if test="${sdaj.ajdj==3}">
					申请撤销
				</c:if>
				<c:if test="${sdaj.ajdj==4}">
					已确立
				</c:if>
			</td>
			<td class="sec_tit">案件类型</td>
			<td id="show_ajlx">${dicMap["AJLX"][sdaj.ajlx]}</td>
		</tr>
		<tr>
    		<td class="sec_tit">部级目标案件编号</td>
            <td id="show_b_m_jlbh">${sdaj.b_m_jlbh}</td>
			<td class="sec_tit">省级目标案件编号</td>
			<td id="show_s_m_jlbh">${sdaj.s_m_jlbh}</td>
		</tr>
		<tr>
    		<td class="sec_tit">市级目标案件编号</td>
            <td id="show_c_m_jlbh">${sdaj.c_m_jlbh}</td>
			<td class="sec_tit">目标案件录入单位级别</td>
			<td id="show_mbajsbdj">
				<c:if test="${sdaj.mbajsbdj==0}">
					部级
				</c:if>
				<c:if test="${sdaj.mbajsbdj==1}">
					省级
				</c:if>
				<c:if test="${sdaj.mbajsbdj==2}">
					市级
				</c:if>
				<c:if test="${sdaj.mbajsbdj==3}">
					县级
				</c:if>
				<c:if test="${sdaj.mbajsbdj==4}">
					派出所
				</c:if>
			</td>
		</tr>
		<tr>
    		<td class="sec_tit">是否群众举报</td>
            <td id="show_sfqzjb">
				<c:if test="${sdaj.sfqzjb==0}">
					否
				</c:if>
				<c:if test="${sdaj.sfqzjb==1}">
					是
				</c:if>
			</td>
			<td class="sec_tit">降级申报请示</td>
			<td id="show_jjsqxx">${sdaj.jjsqxx}</td>
		</tr>
		<tr>
    		<td class="sec_tit">上报单位名称</td>
            <td id="show_sbdw_dwmc">${sdaj.sbdw_dwmc}</td>
			<td class="sec_tit">上报单位代码</td>
			<td id="show_sbdw_dwdm">${sdaj.sbdw_dwdm}</td>
		</tr>
		<tr>
    		<td class="sec_tit">联系人</td>
            <td id="show_lxr">${sdaj.lxr}</td>
			<td class="sec_tit">联系人电话</td>
			<td id="show_lxr_dh">${sdaj.lxr_dh}</td>
		</tr>
		<tr>
    		<td class="sec_tit">部级申报目标案件请示信息内容</td>
            <td id="show_b_inforcontent">${sdaj.b_inforcontent}</td>
			<td class="sec_tit">省级申报目标案件请示信息内容</td>
			<td id="show_s_inforcontent">${sdaj.s_inforcontent}</td>
		</tr>
		<tr>
    		<td class="sec_tit">市级申报目标案件请示信息内容</td>
            <td id="show_c_inforcontent">${sdaj.c_inforcontent}</td>
			<td class="sec_tit">县级申报目标案件请示信息内容</td>
			<td id="show_x_inforcontent">${sdaj.x_inforcontent}</td>
		</tr>
		<tr>
			<td class="sec_tit">转移清洗毒资方式</td>
			<td id="show_zyqxdzfs">${dicMap["ZYQXDZFS"][sdaj.zyqxdzfs]}</td>
			<td class="sec_tit">查缉站点编号</td>
			<td id="show_cjzdbh">${sdaj.cjzdbh}</td>
		</tr>
		<tr>
			<td class="sec_tit">特情人员编号</td>
			<td id="show_tqrybh">${sdaj.tqrybh}</td>
			<td class="sec_tit">情报产品编号</td>
			<td id="show_qbcpbh">${sdaj.qbcpbh}</td>
		</tr>
		<tr>
    		<td class="sec_tit">部级申报目标案件请示信息时间</td>
            <td id="show_b_inforcontent_sqsj"><fmt:formatDate value="${sdaj.b_inforcontent_sqsj}" pattern="yyyy-MM-dd"/></td>
			<td class="sec_tit">省级申报目标案件请示信息时间</td>
			<td id="show_s_inforcontent_sqsj"><fmt:formatDate value="${sdaj.s_inforcontent_sqsj}" pattern="yyyy-MM-dd"/></td>
		</tr>
		<tr>
    		<td class="sec_tit">市级申报目标案件请示信息时间</td>
            <td id="show_c_inforcontent_sqsj"><fmt:formatDate value="${sdaj.c_inforcontent_sqsj}" pattern="yyyy-MM-dd"/></td>
			<td class="sec_tit">县级申报目标案件请示信息时间</td>
			<td id="show_x_inforcontent_sqsj"><fmt:formatDate value="${sdaj.x_inforcontent_sqsj}" pattern="yyyy-MM-dd"/></td>
		</tr>
		<tr>
			<td class="sec_tit">部级目标案件确立时间</td>
			<td id="show_bm_qlsj"><fmt:formatDate value="${sdaj.bm_qlsj}" pattern="yyyy-MM-dd"/></td>
			<td class="sec_tit">省级目标案件确立时间</td>
			<td id="show_sm_qlsj"><fmt:formatDate value="${sdaj.sm_qlsj}" pattern="yyyy-MM-dd"/></td>
		</tr>
		<tr>
			<td class="sec_tit">市级目标案件确立时间</td>
			<td id="show_cm_qlsj"><fmt:formatDate value="${sdaj.cm_qlsj}" pattern="yyyy-MM-dd"/></td>
			<td class="sec_tit">前期处置措施</td>
			<td id="show_qqczcs">${sdaj.qqczcs}</td>
		</tr>
		<tr>
			<td class="sec_tit">种子来源</td>
			<td id="show_zzly">${dicMap["ZZLY"][sdaj.zzly]}</td>
			<td class="sec_tit">来源</td>
			<td id="show_ly">
				<c:if test="${sdaj.ly==1}">
					接口下发
				</c:if>
				<c:if test="${sdaj.ly==2}">
					本地录入
				</c:if>
			</td>
		</tr>
    </table>
<div style="height: 5px"></div>
<jsp:include page="tbDpajSdryChxxShow.jsp"/>
</body>
</html>
