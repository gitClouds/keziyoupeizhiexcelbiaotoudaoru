<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>title</title>
</head>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<%--<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-closeable-tab.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/js/common.js"></script>--%>
<script src="${ctx}/resources/echarts.min.js"></script>
<body>
<!-- 主体内容 -->
<section class="content">
    <div id="main" style="width: 100%;height:100%;"></div>
</section>
<script>
    var ctx = "${ctx}";
    var myChart = echarts.init(document.getElementById('main'));
    myChart.showLoading();
    var categories = [{name:'非涉/吸毒'},{name:'涉毒'},{name:'吸毒'},{name:'涉/吸毒'}];
    var option = {
        title: {
            text: '人员关系分析图',
            textStyle:{
                fontSize:22
            },
            top: 'bottom',
            left: 'right'
        },
        tooltip: {
            trigger: 'item',
            formatter: function (params) {//连接线上提示文字格式化
                if (params.data.name) {//注意判断，else是将节点的文字也初始化成想要的格式
                    return "账户机构："+params.data.jgmc+'<br />'+"账号："+params.data.value;
                } else {
                    var sourcename = params.data.source;
                    var targetname = params.data.target;
                    sourcename = sourcename.substring(sourcename.indexOf("/")+1,sourcename.length);
                    targetname = targetname.substring(targetname.indexOf("/")+1,targetname.length);
                    return sourcename+'给'+targetname+"转账共"+params.data.zzcs+"次，总金额："+params.data.zzje+"元";
                }
            }
        },
        color:['#EE9A00','#2F4F4F','#7D9EC0','#CD4F39'],
        legend: [{
            // selectedMode: 'single',
            data: categories.map(function (a) {
                return a.name;
            })
        }],
        //animation: false,
        series : [
            {
                type: 'graph',
                layout: 'force',
                zoom: 1.8,
                data: ${result},
                links: ${links},
                categories: categories,
                focusNodeAdjacency: true,
                roam: true,
                label: {
                    position: 'right'
                },
                force: {
                    repulsion: 1200,
                    edgeLength: [200, 150]
                },
                label: {//图形上的文本标签，可用于说明图形的一些数据信息
                    normal: {
                        show : true,//显示
                        //position: 'right',//相对于节点标签的位置，默认在节点中间
                        //回调函数，你期望节点标签上显示什么
                        formatter: function(params){
                            return params.data.xm;
                        }
                    }
                },
                itemStyle: {
                    borderColor: '#fff',
                    borderWidth: 1,
                    shadowBlur: 10,
                    shadowColor: 'rgba(0, 0, 0, 0.3)'
                },
                lineStyle: {
                    // 关系边的公用线条样式。其中 lineStyle.color 支持设置为'source'或者'target'特殊值，此时边会自动取源节点或目标节点的颜色作为自己的颜色。
                    normal: {
                        color: 'source',
                        width: 2,
                        type: 'solid',
                        //opacity: 0.5,
                        curveness: 0.1
                    }
                },
                edgeLabel: {
                    normal: {
                        show: true,
                        textStyle: {
                            fontSize: 14
                        },
                        formatter: function(param) {        // 标签内容
                            return param.data.gxfx;
                        }
                    }
                },
                edgeSymbol: ['none', 'arrow'],
                edgeSymbolSize: [0,12],
            }
        ]
    };

    myChart.setOption(option);
    myChart.hideLoading();
</script>
</body>
</html>