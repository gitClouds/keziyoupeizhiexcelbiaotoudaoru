<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-table.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-treeview.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="stylesheet" href="${ctx}/css/pub/code.css">
</head>
<body>
<!-- 主体内容 -->
<section class="content">
	<!--查询项表单 begin-->
    <div class="row">
        <div class="col-xs-12">
            <ul class="box clearfix">
                <form class="form_chag form-inline" name="tbDpajJbxxQueryForm" id="tbDpajJbxxQueryForm">
                	<!-- 生成字段全部为input:text，若需要修改为字典、时间等，请对应修改 -->
                	<!-- 请根据需要修改 查询检索项 -->
                    	<div class="form-group">
	                        <label for="jlbh">记录编号--DExxxxx--编号规则为“XXXXXXXXXXXX（公安机关机构代码）+YYYYMM（年月）+XXXXXXXXXXXXXX（顺序号）”                         </label>
	                        <input type="text" class="form-control" id="jlbh" name="jlbh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ajbh">立案编号--DE00092--编号规则为“A+XXXXXXXXXXXX（公安机关机构代码）+YYYYMM（年月）+XXXX（顺序号）”                         </label>
	                        <input type="text" class="form-control" id="ajbh" name="ajbh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ajmc">案件名称--DE00094--</label>
	                        <input type="text" class="form-control" id="ajmc" name="ajmc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ajlbdm">案件类别--DE00093--采用GA 240.1《刑事犯罪信息管理代码 第1部分: 案件类别代码》</label>
	                        <input type="text" class="form-control" id="ajlbdm" name="ajlbdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="fxsj">发现时间--DE01085、DE01121--</label>
	                        <input type="text" class="form-control" id="fxsj" name="fxsj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="dzmc">发案地点--DE00075--</label>
	                        <input type="text" class="form-control" id="dzmc" name="dzmc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="fazdxz">发案地点详址--DE00076--</label>
	                        <input type="text" class="form-control" id="fazdxz" name="fazdxz" >
	                    </div>
                    	<div class="form-group">
	                        <label for="padq">破案地区--DE00076--</label>
	                        <input type="text" class="form-control" id="padq" name="padq" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ladw_dwdm">立案单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码</label>
	                        <input type="text" class="form-control" id="ladw_dwdm" name="ladw_dwdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ladw_dwmc">立案/承办单位名称--DE00065--</label>
	                        <input type="text" class="form-control" id="ladw_dwmc" name="ladw_dwmc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cbrxm">承办人--DE00002--</label>
	                        <input type="text" class="form-control" id="cbrxm" name="cbrxm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cbr_lxdh">承办人联系信息--DE00216--固定电话号码、移动电话号码</label>
	                        <input type="text" class="form-control" id="cbr_lxdh" name="cbr_lxdh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="larq">立案日期--DE00220--</label>
	                        <input type="text" class="form-control" id="larq" name="larq" >
	                    </div>
                    	<div class="form-group">
	                        <label for="padw_dwdm">破案单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码</label>
	                        <input type="text" class="form-control" id="padw_dwdm" name="padw_dwdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="padw_dwmc">破案单位名称--DE00065--</label>
	                        <input type="text" class="form-control" id="padw_dwmc" name="padw_dwmc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="badwlxdm">破案部门--DE00063--采用GA 300.15《看守所在押人员信息管理代码 第15部分: 办案单位类型代码》</label>
	                        <input type="text" class="form-control" id="badwlxdm" name="badwlxdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="parq">破案日期--DE00223--</label>
	                        <input type="text" class="form-control" id="parq" name="parq" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cdfsdm">藏毒方式代码--DE00171--采用GA 332.10《禁毒信息管理代码 第10部分:藏毒方式代码》</label>
	                        <input type="text" class="form-control" id="cdfsdm" name="cdfsdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="fyfsdm">贩运方式代码--DExxxxx--</label>
	                        <input type="text" class="form-control" id="fyfsdm" name="fyfsdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sfjt">是否团伙/集团作案--DExxxxx--</label>
	                        <input type="text" class="form-control" id="sfjt" name="sfjt" >
	                    </div>
                    	<div class="form-group">
	                        <label for="fzthxzdm">作案团伙/集团类型--DE00998--采用 GA/T XXX《犯罪团伙性质代码》</label>
	                        <input type="text" class="form-control" id="fzthxzdm" name="fzthxzdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="fzthmc">作案团伙/集团名称--DE00997--</label>
	                        <input type="text" class="form-control" id="fzthmc" name="fzthmc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="jyaq">案情--DE00100--</label>
	                        <input type="text" class="form-control" id="jyaq" name="jyaq" >
	                    </div>
                    	<div class="form-group">
	                        <label for="jzrtgqk">见证人提供情况--DE00521--</label>
	                        <input type="text" class="form-control" id="jzrtgqk" name="jzrtgqk" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zarqkfx">作案人情况分析--DE00521--</label>
	                        <input type="text" class="form-control" id="zarqkfx" name="zarqkfx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="asjzcxwlbdm">侦察措施--DE01009--采用 GA/T XXX《案事件侦查行为分类与代码》</label>
	                        <input type="text" class="form-control" id="asjzcxwlbdm" name="asjzcxwlbdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="pafsdm">破案方式代码--DE00097--采用GA 240.18《刑事犯罪信息管理代码 第18部分: 破案方式代码》</label>
	                        <input type="text" class="form-control" id="pafsdm" name="pafsdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cxajyydm">撤案原因--DE00099--采用GA 398.7《经济犯罪案件管理信息系统技术规范 第7部分：撤销案件原因代码》</label>
	                        <input type="text" class="form-control" id="cxajyydm" name="cxajyydm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cxajrq">撤案日期--DE01102--</label>
	                        <input type="text" class="form-control" id="cxajrq" name="cxajrq" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cjqk">查结情况--DE00521--</label>
	                        <input type="text" class="form-control" id="cjqk" name="cjqk" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cjrq">查结日期--DE00101--</label>
	                        <input type="text" class="form-control" id="cjrq" name="cjrq" >
	                    </div>
                    	<div class="form-group">
	                        <label for="gkfs">公开方式--DExxxxx--</label>
	                        <input type="text" class="form-control" id="gkfs" name="gkfs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="wqgkrq">完全公开日期--DE00101--</label>
	                        <input type="text" class="form-control" id="wqgkrq" name="wqgkrq" >
	                    </div>
                    	<div class="form-group">
	                        <label for="syztdm">可查状态--DE00596--采用GA/T 2000.40《公安信息代码 第40部分：使用状态代码》</label>
	                        <input type="text" class="form-control" id="syztdm" name="syztdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cxdw_dwmc">查询单位名称--DE00065--</label>
	                        <input type="text" class="form-control" id="cxdw_dwmc" name="cxdw_dwmc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cxdw_dwdm">查询单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码</label>
	                        <input type="text" class="form-control" id="cxdw_dwdm" name="cxdw_dwdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zcjddm">案件阶段--DE00096--采用GA 398.5《经济犯罪案件管理信息系统技术规范 第5部分：侦查工作阶段代码》</label>
	                        <input type="text" class="form-control" id="zcjddm" name="zcjddm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="tbdw_dwdm">填报单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码</label>
	                        <input type="text" class="form-control" id="tbdw_dwdm" name="tbdw_dwdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="tbdw_dwmc">填报单位名称--DE00065--</label>
	                        <input type="text" class="form-control" id="tbdw_dwmc" name="tbdw_dwmc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="djr_xm">登记人--DE00002--</label>
	                        <input type="text" class="form-control" id="djr_xm" name="djr_xm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="djrq">登记日期--DE00524--</label>
	                        <input type="text" class="form-control" id="djrq" name="djrq" >
	                    </div>
                    	<div class="form-group">
	                        <label for="bz">备注--DE00503--</label>
	                        <input type="text" class="form-control" id="bz" name="bz" >
	                    </div>
                    	<div class="form-group">
	                        <label for="shdw_dwmc">审核单位名称--DE00065--</label>
	                        <input type="text" class="form-control" id="shdw_dwmc" name="shdw_dwmc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="shdw_dwdm">审核单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码</label>
	                        <input type="text" class="form-control" id="shdw_dwdm" name="shdw_dwdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="jhdp">缴毒总量（克）--DExxxxx--</label>
	                        <input type="text" class="form-control" id="jhdp" name="jhdp" >
	                    </div>
                    	<div class="form-group">
	                        <label for="qbtgdw_dwmc">情报提供单位--DE00065--</label>
	                        <input type="text" class="form-control" id="qbtgdw_dwmc" name="qbtgdw_dwmc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="qbtgdw_dwdm">情报提供单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码</label>
	                        <input type="text" class="form-control" id="qbtgdw_dwdm" name="qbtgdw_dwdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="fyqdsm">贩运渠道说明--DE00521--</label>
	                        <input type="text" class="form-control" id="fyqdsm" name="fyqdsm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="pdbz">存在不合格--DE00742--判断标识</label>
	                        <input type="text" class="form-control" id="pdbz" name="pdbz" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ajdh">案件代号--DExxxxx--</label>
	                        <input type="text" class="form-control" id="ajdh" name="ajdh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ajxz">案件性质--DExxxxx--</label>
	                        <input type="text" class="form-control" id="ajxz" name="ajxz" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zzdw_dwmc">主侦单位名称--DE00060--</label>
	                        <input type="text" class="form-control" id="zzdw_dwmc" name="zzdw_dwmc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zzdw_dwdm">主侦单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码</label>
	                        <input type="text" class="form-control" id="zzdw_dwdm" name="zzdw_dwdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zddw_dwmc">指导单位名称--DE00060--</label>
	                        <input type="text" class="form-control" id="zddw_dwmc" name="zddw_dwmc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zddw_dwdm">指导单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码</label>
	                        <input type="text" class="form-control" id="zddw_dwdm" name="zddw_dwdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="gjhdqdm">涉案国家--DE00069--采用GB/T 2659《世界各国和地区名称代码》中全部三位字母代码</label>
	                        <input type="text" class="form-control" id="gjhdqdm" name="gjhdqdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sadq_xzqh">涉案地区--DE00070--采用GB/T 2260《中国人民共和国行政区划代码》中的全部数字代码</label>
	                        <input type="text" class="form-control" id="sadq_xzqh" name="sadq_xzqh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="fzrxm">负责人--DE00002--</label>
	                        <input type="text" class="form-control" id="fzrxm" name="fzrxm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="fzr_lxdh">负责人电话--DE00216--固定电话号码、移动电话号码</label>
	                        <input type="text" class="form-control" id="fzr_lxdh" name="fzr_lxdh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="xsly">线索来源----</label>
	                        <input type="text" class="form-control" id="xsly" name="xsly" >
	                    </div>
                    	<div class="form-group">
	                        <label for="xsly_dwdm">线索来源单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码</label>
	                        <input type="text" class="form-control" id="xsly_dwdm" name="xsly_dwdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="xsly_dwmc">线索来源单位名称--DE00060--</label>
	                        <input type="text" class="form-control" id="xsly_dwmc" name="xsly_dwmc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="qyxsly">其它线索来源--DExxxxx--</label>
	                        <input type="text" class="form-control" id="qyxsly" name="qyxsly" >
	                    </div>
                    	<div class="form-group">
	                        <label for="mqzcgzjzqk">目前侦查工作进展情况--DE01012--</label>
	                        <input type="text" class="form-control" id="mqzcgzjzqk" name="mqzcgzjzqk" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zcgzzwt">侦查工作中遇到的主要问题--DE01012--</label>
	                        <input type="text" class="form-control" id="zcgzzwt" name="zcgzzwt" >
	                    </div>
                    	<div class="form-group">
	                        <label for="xybgzjy">下一步工作建议--DE01012--</label>
	                        <input type="text" class="form-control" id="xybgzjy" name="xybgzjy" >
	                    </div>
                    	<div class="form-group">
	                        <label for="inforcontent">请示信息内容(DZWJNR)--DE01076--</label>
	                        <input type="text" class="form-control" id="inforcontent" name="inforcontent" >
	                    </div>
                    	<div class="form-group">
	                        <label for="asjdbjbdm">督办级别--DE00091--采用GA 398.3《经济犯罪案件管理信息系统技术规范 第3部分：督办级别代码》0 部级  1省级  2市级。默认为空</label>
	                        <input type="text" class="form-control" id="asjdbjbdm" name="asjdbjbdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="pazt">破案状态--DExxxxx--</label>
	                        <input type="text" class="form-control" id="pazt" name="pazt" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ywlb">业务类别--DExxxxx--</label>
	                        <input type="text" class="form-control" id="ywlb" name="ywlb" >
	                    </div>
                    	<div class="form-group">
	                        <label for="qlsj">确立时间--DE00101--</label>
	                        <input type="text" class="form-control" id="qlsj" name="qlsj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zcsbqs">再次申报请示--DE01012--</label>
	                        <input type="text" class="form-control" id="zcsbqs" name="zcsbqs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sqlhqs">申请联合请示--DE01012--</label>
	                        <input type="text" class="form-control" id="sqlhqs" name="sqlhqs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sqlhspbs">申请联合审批标识--DExxxxx--</label>
	                        <input type="text" class="form-control" id="sqlhspbs" name="sqlhspbs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="lhba">联合办案--DExxxxx--</label>
	                        <input type="text" class="form-control" id="lhba" name="lhba" >
	                    </div>
                    	<div class="form-group">
	                        <label for="lhzc_gajgjgdm">联合协查机构--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码</label>
	                        <input type="text" class="form-control" id="lhzc_gajgjgdm" name="lhzc_gajgjgdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ajpasqb">案件破案申请表--DE01012--</label>
	                        <input type="text" class="form-control" id="ajpasqb" name="ajpasqb" >
	                    </div>
                    	<div class="form-group">
	                        <label for="paqlsj">破案确立时间--DE00554--</label>
	                        <input type="text" class="form-control" id="paqlsj" name="paqlsj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cxqs">撤销请示--DExxxxx--</label>
	                        <input type="text" class="form-control" id="cxqs" name="cxqs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="lacbdq">立案/承办地区</label>
	                        <input type="text" class="form-control" id="lacbdq" name="lacbdq" >
	                    </div>
                    	<div class="form-group">
	                        <label for="dqshjg">当前审核结果</label>
	                        <input type="text" class="form-control" id="dqshjg" name="dqshjg" >
	                    </div>
                    	<div class="form-group">
	                        <label for="shbz">审核标志</label>
	                        <input type="text" class="form-control" id="shbz" name="shbz" >
	                    </div>
                    	<div class="form-group">
	                        <label for="gxsj">更新时间</label>
	                        <input type="text" class="form-control" id="gxsj" name="gxsj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="yxx">有效性</label>
	                        <input type="text" class="form-control" id="yxx" name="yxx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="gxsjc">更新时间戳--DEXXXXX--</label>
	                        <input type="text" class="form-control" id="gxsjc" name="gxsjc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="xgbsdw">修改部署单位--DEXXXXX--</label>
	                        <input type="text" class="form-control" id="xgbsdw" name="xgbsdw" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sfhlwaj">是否互联网案件--46</label>
	                        <input type="text" class="form-control" id="sfhlwaj" name="sfhlwaj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="hlwajlx">互联网案件类型--860</label>
	                        <input type="text" class="form-control" id="hlwajlx" name="hlwajlx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="qtsm">其他说明</label>
	                        <input type="text" class="form-control" id="qtsm" name="qtsm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sfsxxq">是否涉嫌洗钱--46</label>
	                        <input type="text" class="form-control" id="sfsxxq" name="sfsxxq" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sdxqfs">涉毒洗钱方式--861</label>
	                        <input type="text" class="form-control" id="sdxqfs" name="sdxqfs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="xqqtsm">其他说明(洗钱)</label>
	                        <input type="text" class="form-control" id="xqqtsm" name="xqqtsm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sfhsh">是否黑社会性质--DExxxxx--</label>
	                        <input type="text" class="form-control" id="sfhsh" name="sfhsh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sfwzfd">是否武装贩毒--DExxxxx--</label>
	                        <input type="text" class="form-control" id="sfwzfd" name="sfwzfd" >
	                    </div>
                    	<div class="form-group">
	                        <label for="mbajzt">目标案件操作级别： 1县建议。2县审核。3市建议。4市审核。5省建议。6省审核。7部建议。8部审核。默认为空</label>
	                        <input type="text" class="form-control" id="mbajzt" name="mbajzt" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ajdj">目标案件操作类别：  0申请目标案件。 1申请降级。2申请破案。3申请撤销。4已确立 。默认为空</label>
	                        <input type="text" class="form-control" id="ajdj" name="ajdj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ajlx">案件类型 --DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码</label>
	                        <input type="text" class="form-control" id="ajlx" name="ajlx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="b_m_jlbh">部级目标案件编号--DExxxxx--编号规则为“XXXX（行政代码）+YYYY（年）+XXXX（顺序号）”</label>
	                        <input type="text" class="form-control" id="b_m_jlbh" name="b_m_jlbh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="s_m_jlbh">省级目标案件编号--DExxxxx--编号规则为“XXXX（行政代码）+YYYY（年）+XXXX（顺序号）”</label>
	                        <input type="text" class="form-control" id="s_m_jlbh" name="s_m_jlbh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="c_m_jlbh">市级目标案件编号--DExxxxx--编号规则为“XXXX（行政代码）+YYYY（年）+XXXX（顺序号）”</label>
	                        <input type="text" class="form-control" id="c_m_jlbh" name="c_m_jlbh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="mbajsbdj">目标案件录入单位级别  0 部级  1省级  2市级  3县级   4派出所</label>
	                        <input type="text" class="form-control" id="mbajsbdj" name="mbajsbdj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="xcxs">宣传形式</label>
	                        <input type="text" class="form-control" id="xcxs" name="xcxs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sfxc">是否宣传--46</label>
	                        <input type="text" class="form-control" id="sfxc" name="sfxc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sfqzjb">是否群众举报--46</label>
	                        <input type="text" class="form-control" id="sfqzjb" name="sfqzjb" >
	                    </div>
                    	<div class="form-group">
	                        <label for="jjsqxx">降级申报请示</label>
	                        <input type="text" class="form-control" id="jjsqxx" name="jjsqxx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sbdw_dwmc">上报单位名称--DE00060--</label>
	                        <input type="text" class="form-control" id="sbdw_dwmc" name="sbdw_dwmc" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sbdw_dwdm">上报单位代码--DE00060--采用GA 380《全国公安机关机构代码编码规则》统一编制的代码</label>
	                        <input type="text" class="form-control" id="sbdw_dwdm" name="sbdw_dwdm" >
	                    </div>
                    	<div class="form-group">
	                        <label for="lxr">联系人</label>
	                        <input type="text" class="form-control" id="lxr" name="lxr" >
	                    </div>
                    	<div class="form-group">
	                        <label for="lxr_dh">联系人电话</label>
	                        <input type="text" class="form-control" id="lxr_dh" name="lxr_dh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="b_inforcontent">部级申报目标案件请示信息内容(DZWJNR)--DE01076--</label>
	                        <input type="text" class="form-control" id="b_inforcontent" name="b_inforcontent" >
	                    </div>
                    	<div class="form-group">
	                        <label for="s_inforcontent">省级申报目标案件请示信息内容(DZWJNR)--DE01076--</label>
	                        <input type="text" class="form-control" id="s_inforcontent" name="s_inforcontent" >
	                    </div>
                    	<div class="form-group">
	                        <label for="c_inforcontent">市级申报目标案件请示信息内容(DZWJNR)--DE01076--</label>
	                        <input type="text" class="form-control" id="c_inforcontent" name="c_inforcontent" >
	                    </div>
                    	<div class="form-group">
	                        <label for="x_inforcontent">县级申报目标案件请示信息内容(DZWJNR)--DE01076--</label>
	                        <input type="text" class="form-control" id="x_inforcontent" name="x_inforcontent" >
	                    </div>
                    	<div class="form-group">
	                        <label for="czbs">操作标识</label>
	                        <input type="text" class="form-control" id="czbs" name="czbs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zyqxdzfs">转移清洗毒资方式</label>
	                        <input type="text" class="form-control" id="zyqxdzfs" name="zyqxdzfs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cjzdbh">查缉站点编号</label>
	                        <input type="text" class="form-control" id="cjzdbh" name="cjzdbh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="tqrybh">特情人员编号</label>
	                        <input type="text" class="form-control" id="tqrybh" name="tqrybh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="qbcpbh">情报产品编号</label>
	                        <input type="text" class="form-control" id="qbcpbh" name="qbcpbh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zsjjsqxx">指示降级信息</label>
	                        <input type="text" class="form-control" id="zsjjsqxx" name="zsjjsqxx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="b_inforcontent_sqsj">部级申报目标案件请示信息时间</label>
	                        <input type="text" class="form-control" id="b_inforcontent_sqsj" name="b_inforcontent_sqsj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="s_inforcontent_sqsj">省级申报目标案件请示信息时间</label>
	                        <input type="text" class="form-control" id="s_inforcontent_sqsj" name="s_inforcontent_sqsj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="c_inforcontent_sqsj">市级申报目标案件请示信息时间</label>
	                        <input type="text" class="form-control" id="c_inforcontent_sqsj" name="c_inforcontent_sqsj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="x_inforcontent_sqsj">县级申报目标案件请示信息时间</label>
	                        <input type="text" class="form-control" id="x_inforcontent_sqsj" name="x_inforcontent_sqsj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="bsxm_lx">报送项目类型</label>
	                        <input type="text" class="form-control" id="bsxm_lx" name="bsxm_lx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="bm_qlsj">部级目标案件确立时间</label>
	                        <input type="text" class="form-control" id="bm_qlsj" name="bm_qlsj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sm_qlsj">省级目标案件确立时间</label>
	                        <input type="text" class="form-control" id="sm_qlsj" name="sm_qlsj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cm_qlsj">市级目标案件确立时间</label>
	                        <input type="text" class="form-control" id="cm_qlsj" name="cm_qlsj" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cszt">传输状态。0:未传输。1:已传输（新老系统数据传输）</label>
	                        <input type="text" class="form-control" id="cszt" name="cszt" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cbrxm2">字段</label>
	                        <input type="text" class="form-control" id="cbrxm2" name="cbrxm2" >
	                    </div>
                    	<div class="form-group">
	                        <label for="cbr_lxdh2">字段</label>
	                        <input type="text" class="form-control" id="cbr_lxdh2" name="cbr_lxdh2" >
	                    </div>
                    	<div class="form-group">
	                        <label for="pafsdm_old">旧系统中破案方式代码--DE00097--采用GA 240.18《刑事犯罪信息管理代码 第18部分: 破案方式代码》</label>
	                        <input type="text" class="form-control" id="pafsdm_old" name="pafsdm_old" >
	                    </div>
                    	<div class="form-group">
	                        <label for="n_yxh">字段</label>
	                        <input type="text" class="form-control" id="n_yxh" name="n_yxh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="sjly">字段</label>
	                        <input type="text" class="form-control" id="sjly" name="sjly" >
	                    </div>
                    	<div class="form-group">
	                        <label for="qqczcs">前期处置措施</label>
	                        <input type="text" class="form-control" id="qqczcs" name="qqczcs" >
	                    </div>
                    	<div class="form-group">
	                        <label for="zzly">种子来源</label>
	                        <input type="text" class="form-control" id="zzly" name="zzly" >
	                    </div>
                    	<div class="form-group">
	                        <label for="mbajlb">目标案件类别</label>
	                        <input type="text" class="form-control" id="mbajlb" name="mbajlb" >
	                    </div>
                    	<div class="form-group">
	                        <label for="mbajxh">目标案件序号</label>
	                        <input type="text" class="form-control" id="mbajxh" name="mbajxh" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ajly">案件来源</label>
	                        <input type="text" class="form-control" id="ajly" name="ajly" >
	                    </div>
                    	<div class="form-group">
	                        <label for="jzcd_ajlx">禁种铲毒案件类型</label>
	                        <input type="text" class="form-control" id="jzcd_ajlx" name="jzcd_ajlx" >
	                    </div>
                    	<div class="form-group">
	                        <label for="ly">来源(1：接口下发2：本地录入)</label>
	                        <input type="text" class="form-control" id="ly" name="ly" >
	                    </div>
                                        
                    <div class="form-group ">
                        <button type="button" style="margin-left: 8%" class="btn btn-info" onclick="search();">检索</button>
                    </div>
                </form>
            </ul>
        </div>
    </div>
	<!--查询项表单 end-->
	<!--结果展示 begin-->
	<div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="tbDpajJbxxGrid" class="table table-bordered table-striped table-condensed">

                    </table>
                    <!--toolbar -->
                    <div class="btn-group" id="toolbar">
                        <i class="btn" onclick="addTbDpajJbxx();">
                            <i class="fa fa-plus-square fa-2x"></i>
                        </i>
                        <i class="btn" onclick="exportTbDpajJbxx();">
                            <i class="fa fa-cloud-download fa-2x"></i>
                        </i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--结果展示 end-->
    <!-- 提示模态框（Modal） -->
    <div class="modal fade" id="tipsModal" tabindex="-1" role="dialog" aria-labelledby="tipsModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="tipsModalLabel">提示</h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="tipsMessage"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" id="tipsConfirmBtn">确定</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 详情模态框（Modal） -->
    <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="showModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="showMessage">
                    <jsp:include page="tbDpajJbxxShow.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 编辑模态框（Modal） -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-show">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="editModalLabel"></h4><%--模态框（Modal）标题--%>
                </div>
                <div class="modal-body" id="editMessage">
                    <jsp:include page="tbDpajJbxxForm.jsp"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" id="editConfirmBtn">保存</button>
                </div>
            </div>
        </div>
    </div>
    
    <!-- 字典、树形字典展示框。如果page、form均无字典项，可无需此div -->
    <div id="treeDiv">
        <input type="text" class="form-control" id="searchTree"  placeholder="支持文本检索">
        <div id="tree"></div>
    </div>
</section>
<script>
    var ctx = "${ctx}";
</script>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-treeview.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/resources/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/Jdzh/tbDpajJbxx.js"></script>
</body>
</html>