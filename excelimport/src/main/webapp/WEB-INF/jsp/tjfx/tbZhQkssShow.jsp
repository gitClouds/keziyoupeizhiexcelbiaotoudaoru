<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>title</title>
</head>
<body>
    <table class="table table-bordered">
        <!-- 请根据需要修改 显示项、字段名等 -->
		<tr>
    		<td class="sec_tit">账号</td>
            <td id="show_zh"></td>
    		<td class="sec_tit">姓名</td>
            <td id="show_xm"></td>
		</tr>
		<tr>
    		<td class="sec_tit">证件号码</td>
            <td id="show_zjhm"></td>
    		<td class="sec_tit">证件类型</td>
            <td id="show_zjlx"></td>
		</tr>
		<tr>
    		<td class="sec_tit">账号机构代码</td>
            <td id="show_jgdm"></td>
    		<td class="sec_tit">账户机构名称</td>
            <td id="show_jgmc"></td>
		</tr>
		<tr>
    		<td class="sec_tit">账号类型1：银行卡2：三方</td>
            <td id="show_zhlx"></td>
    		<td class="sec_tit">业务类型qqlx</td>
            <td id="show_ywlx"></td>
		</tr>
		<tr>
    		<td class="sec_tit">业务发送人姓名</td>
            <td id="show_ywfsrxm"></td>
    		<td class="sec_tit">业务发送单位名称</td>
            <td id="show_ywfsrdwmc"></td>
		</tr>
    </table>
</body>
</html>
