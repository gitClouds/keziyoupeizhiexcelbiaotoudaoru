<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>禁毒资金智能化分析平台</title>
    <script>var ctx = '${ctx}'</script>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/AdminLTE.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/_all-skins.min.css">
    <link rel="stylesheet" href="${ctx}/resources/bootstrap/css/bootstrap-tab.css">
    <link rel="stylesheet" href="${ctx}/css/pub/page.css">
    <link rel="shortcut icon" href="${ctx}/resources/img/favicon.ico">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <style>
        body, html {
            height: 100% !important;
            overflow: hidden;
        }

        .pop {position: fixed;right: 10px;bottom: 10px;box-shadow: 0 5px 15px rgba(0, 0, 0, .5);}

        .popHead {background: #fff;width: 260px;border: 1px solid #e0e0e0;font-size: 14px;padding: 10px;}

        .popClose {float: right;color: #f25656;}

        .popHead span {font-size: 15px;}

        .popContext {height: auto;padding: 15px;}

        .content-wrapper {
            background-color: #F5FFFA;
            overflow: hidden;
        }

        /*  .tabs-cent {
              overflow: auto;
          }*/
        .wrapper {
            height: 100% !important;
            overflow: hidden;
        }

        .main-header .navbar {
            margin-left: 250px;
        }

        .main-header .logo {
            width: 250px;
        }

        .main-sidebar {
            width: 250px;
            height: 100%;
            overflow: auto;
        }

        .content-wrapper, .main-footer {
            margin-left: 250px;
        }

        .main-sidebar::-webkit-scrollbar {
            width: 4px;
            height: 1px;
        }

        .main-sidebar::-webkit-scrollbar-thumb {
            border-radius: 4px;
            -webkit-box-shadow: inset 0 0 5px rgba(0, 0, 0, 0.2);
            background: #535353;
        }

        .main-sidebar::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 5px rgba(0, 0, 0, 0.2);
            border-radius: 10px;
            background: #555;
        }

        .main-header .notice-toggle {
            float: left;
            background-color: transparent;
            background-image: none;
            padding: 18px 15px;
            font-family: fontAwesome;
            color: #ffffff;
            min-height: 50px;
        }

        .main-header .notice-toggle:hover {
            background-color: #337ab7;
        }

        #container {
            width: 500px;
            height: 820px;
            margin: 0 auto;
        }

        .feiseo {
            width: 360px;
            height: 37px;
            margin-left: auto;
            margin-right: auto;
            position: relative;
        }

        .feiseo input {
            width: 100%;
            height: 130%;
            border: 0px;
            outline: none;
            padding-left: 7%;
            padding-right: 3%;
            background-color: #f5fffa;
            color: #000;
            border-radius: 40px;
        }

        .feiseo button {
            position: absolute;
            right: 5px;
            top: 9px;
            color: #5b6e81;
        }

        .banbutt {
            position: absolute;
            right: 0px;
            background-color: transparent;
            border-width: 0px;
            outline: none;
        }

        .btn {
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 20px;
            font-weight: 400;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-top-width: 1px;
            border-right-width: 1px;
            border-bottom-width: 1px;
            border-left-width: 1px;
            border-radius: 4px;
        }
    </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <!--顶部导航部分-->
    <header class="main-header">
        <!-- Logo -->
        <a href="javascript:void(0);" class="logo">
            <span class="logo-lg">
                <img src="${ctx}/resources/img/favicon.png" style="margin-top: 5px">
            </span>
        </a>
        <nav class="navbar navbar-static-top">
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button"></a>
            <%--
                        <a href="javascript:showNotice();" class="notice-toggle" role="button" title="注意事项"><i class='fa fa-star fa-lg'></i></a>
            --%>
            <%--   <a href="${ctx}/resources/files/禁毒资金分析—操作手册.pdf" download target="_blank" class="notice-toggle" title="操作手册下载">操作手册</a>
            --%>
            <div class="feiseo" style=" float: left; padding-top:10px;padding-left: 70px;">
                <input placeholder="全文检索" id="gjz">
                <button class="btn banbutt" type="button" onclick="qwjs()"><i class="fa fa-search"></i></button>
            </div>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <%--<li class="dropdown">
                        <a href="#">
                            <span>在线人数</span>
                        </a>
                    </li>--%>
                    <li class="dropdown user user-menu">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-user-circle fa-lg"></i>&nbsp;&nbsp;<span>${info.xm}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-header" style="height: 100px">
                                <p>
                                    ${info.xm}
                                    <small>${info.jgmc}</small>
                                    <small id="nDate"></small>
                                </p>
                            </li>
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="javascript:showChangePwdModal();" class="btn btn-default btn-flat">修改密码</a>&nbsp;
                                    <a href="javascript:resetPwd();" class="btn btn-default btn-flat">密码重置</a>
                                </div>
                                <div class="pull-right">
                                    <a href="javascript:logout();" class="btn btn-default btn-flat">注销</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- 左侧部分 -->
    <aside class="main-sidebar">
        <section class="sidebar">
            <ul class="sidebar-menu" data-widget="tree" id="syncMenu">
                <!-- 首页暂时屏蔽 -->
                <input type="text" id="sscd" style="font-size:12px;margin-top:1px;width:99%;height:30px;border-radius:6px;" oninput="sscd()" placeholder="输入菜单名称搜索">
                <li class="treeview menu-open gd" onclick="initTab();">
                    <a href="javascript:void(0);"><i class="fa fa-home"></i><span>首页</span></a>
                </li>
                <li class="treeview" onclick="qsjs()">
                    <a href="javascript:void(0);" target="_blank"><i class="glyphicon glyphicon-search"></i><span>全文检索</span></a>
                </li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- 主体内容部分 -->
    <div class="content-wrapper">
        <ul class="nav nav-tabs" id="ifTab_list">
            <!-- 首页暂时屏蔽 -->
            <li id="ifTab_seed_homePage" ifrmId="ifTab_container_homePage" onclick="iframeTab.tabClick(this);" style="height: 40px;line-height: 40px;">
                <a href="#" style="position: relative;padding:2px 23px 2px 15px;height: 40px;line-height: 40px;">首页</a>
            </li>
            <li id="closeTable" ifrmId="" onclick="gbTab(0)" style="float:right;height: 40px;line-height: 40px;">
                <a href="#" style="position: relative;padding:2px 23px 2px 15px;height: 40px;line-height: 40px;">标签页关闭</a>

                <ul class="treeview-menu" style="display: none;cursor:pointer;background: #3c8dbc;color: white" id="gbTab">
                    <li class="tree_form" onclick="gbTab(1)">当前标签页</li>
                    <li class="tree_form" onclick="gbTab(2)">其他标签页</li>
                    <li class="tree_form" onclick="gbTab(3)">全部标签页</li>
                </ul>

            </li>
        </ul>
        <div class="tabs-cent" id="ifTab_center">
            <!-- 首页暂时屏蔽 默认界面设置为接警单 -->
            <iframe src="${ctx}/index/indexPage" height="100%" style="display: block;" id="ifTab_container_homePage" scrolling="yes" frameborder="0"></iframe>
        </div>
    </div>
</div>

<div class="modal fade" id="tipModal" tabindex="-1" role="dialog" aria-labelledby="tipModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" id="tipModalLabel">提示</h4>
            </div>
            <div class="modal-body" id="tipMessage"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" data-toggle="modal" id="tipConfirmBtn">确定</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="cModal" tabindex="-1" role="dialog" aria-labelledby="cModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" id="cModalLabel">修改密码</h4>
            </div>
            <div class="modal-body" id="cMessage">
                <jsp:include page="system/user/changePwd.jsp"/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" id="cConfirmBtn">提交</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="noticeModal" tabindex="-1" role="dialog" aria-labelledby="noticeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-show">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" id="noticeModalLabel"><i class='fa fa-warning' style='color: #c87f0a'></i> 注意事项
                </h4>
            </div>
            <div class="modal-body" id="noticeMessage">
                <jsp:include page='notice.jsp'/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>
<div id="pop" class="panel panel-default pop" hidden>
    <div id="popHead" class="popHead">
        <a href="javascript:void(0);" class="popClose" title="关闭" onclick="closePop()">关闭</a>
        <span></span>
    </div>
    <div id="popTxt" class="popContext"></div>
</div>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/adminlte.min.js"></script>
<script src="${ctx}/resources/bootstrap/js/bootstrap-closeable-tab.js"></script>
<script src="${ctx}/resources/common/jquery-validate.js"></script>
<script src="${ctx}/js/common.js"></script>
<script src="${ctx}/js/index/index.js" charset="UTF-8"></script>
<script src="${ctx}/js/system/changePwd.js"></script>
<script type="text/javascript" src="${ctx}/resources/layer/layer.js" charset="utf-8"></script>
<script charset="UTF-8">


    function pop(title, text, time) {
        $("#popHead span").html(title);
        $("#popTxt").html(text);

        $("#pop").show(100, "linear", function () {
            /* setTimeout("closePop()", time * 1000);*/
        });
    }

    function closePop() {
        $("#pop").hide();
    }

    $(function () {
        $("#sscd").focus();
        $("#gjz").keydown(function (event) {
            if (event.keyCode == 13) {
                qwjs();
            }
        });
        // xxTs();
        // setInterval("xxTs()", 10 * 1000);

    });

    function xxTs() {
        $.ajax({
            url: ctx + "/index/xxInfo",
            type: "post",
            data: {},
            success: function (data) {
                var showText = "";
                var list = data.list;
                $.each(list, function (index) {
                    showText += "<span style=' cursor: pointer;' onclick=\"openXx('" + list[index].xx_id + "')\">" + list[index].xx_nr + "</span></br>";
                });
                if (showText != "") {
                    pop("您有新的消息!", showText, 10);
                }

            }
        });
    }

    function openXx(id) {
        closePop();
        var item = {
            'id': 'screenData',
            'name': '微信账号明细数据',
            'url': ctx + '/zhxx/tbJdzjfxSfZh/wxzhmxjkDetailPage?xxid=' + id
        };
        iframeTab.addIframe(item);
    }

    initTab();
    //document.oncontextmenu=new Function("event.returnValue=false");
    var nowDate = new Date().Format('yyyy-MM-dd');
    $("#nDate").text(nowDate);
    showNotice();
    getLevelMenu(1, '', $("#syncMenu")[0]);

    function showNotice() {
        /* $("#noticeModal").modal('toggle');*/
    }

    function qwjs() {
        var gjz = $("#gjz").val();
        window.open('${ctx}/es/es/searchResult.html?gjz=' + gjz);
    }

    function sscd() {
        var text = $("#sscd").val();
        var b = $("#sscd").prop("outerHTML");
        var c = "<li class='treeview menu-open' onclick='initTab();'><a href='javascript:void(0);'><i class='fa fa-home'></i><span>首页</span></a></li>";
        //if (event.keyCode == "13") {
        if (text != '' && text != null) {
            $.ajax({
                url: ctx + "/system/menu/sscd",
                type: "post",
                data: "cdmc=" + text.trim().toUpperCase(),
                //async: false,
                success: function (data) {
                    $("ul.sidebar-menu *").not("#sscd").addClass("yc");
                    $(".yc").remove();
                    if (!$.isEmptyObject(data)) {
                        var a = '';
                        $.each(data, function (index) {
                            a += getCode(data[index].pageId, data[index].ul, data[index].icon, data[index].name);
                        });
                        //$("#syncMenu").html(b + c + a);
                        //$("#sscd").val(text);
                        $("#syncMenu").append(a);
                    } else {
                        $("#syncMenu").append("</br></br>");
                        var d = "<li id='mypp' style='color: white;margin-left:80px;'>没有匹配菜单</li>";
                        //$("#syncMenu").html(b + c + '</br>' + d);
                        //$("#sscd").val(text);
                        $("#syncMenu").append(d);
                    }
                }
            });
        } else {
            $("#syncMenu").html(b + c);
            getLevelMenu(1, '', $("#syncMenu")[0]);
        }
        $("#sscd").focus();
        //}
    }

    function gbTab(i) {
        if (i == 0) {
            $("#gbTab").toggle();
        } else if (i == 1) {
            //关闭当前
            $("#ifTab_list>li").each(function () {
                if ($(this).hasClass('active')) {
                    $(this).find("i").click();
                }
            });
        } else if (i == 2) {
            $("#ifTab_list>li").each(function () {
                if (!$(this).hasClass('active')) {
                    $(this).find("i").click();
                }
            });
        } else {
            $("#ifTab_list>li").each(function () {
                $(this).find("i").click();
            });
        }
    }
</script>
</body>
</html>
