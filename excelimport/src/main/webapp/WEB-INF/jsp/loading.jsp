<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/resources/include.jsp"%>
<html>
<head>
    <title>Title</title>
    <style>
        .window_loading {
            position: fixed;
            display: block;
            width: 100%;
            height: 100%;
            z-index: 99999;
            background: radial-gradient(rgba(255,255,255,0.1), rgba(0,0,0,0.6));
        }
        .loader {
            position: absolute;
            top: 0px;
            bottom: 0px;
            left: 0px;
            right: 0px;
            margin: auto;
            width: 175px;
            height: 100px;
        }
        .loader span {
            display: block;
            background: #ccc;
            width: 7px;
            height: 10%;
            border-radius: 14px;
            margin-right: 5px;
            float: left;
            margin-top: 25%;
        }
        .loader span:last-child {
            margin-right: 0px;
        }
        .loader span:nth-child(1) {
            animation: load 2.5s 1.4s infinite linear;
        }
        .loader span:nth-child(2) {
            animation: load 2.5s 1.2s infinite linear;
        }
        .loader span:nth-child(3) {
            animation: load 2.5s 1s infinite linear;
        }
        .loader span:nth-child(4) {
            animation: load 2.5s 0.8s infinite linear;
        }
        .loader span:nth-child(5) {
            animation: load 2.5s 0.6s infinite linear;
        }
        .loader span:nth-child(6) {
            animation: load 2.5s 0.4s infinite linear;
        }
        .loader span:nth-child(7) {
            animation: load 2.5s 0.2s infinite linear;
        }
        .loader span:nth-child(8) {
            animation: load 2.5s 0s infinite linear;
        }
        .loader span:nth-child(9) {
            animation: load 2.5s 0.2s infinite linear;
        }
        .loader span:nth-child(10) {
            animation: load 2.5s 0.4s infinite linear;
        }
        .loader span:nth-child(11) {
            animation: load 2.5s 0.6s infinite linear;
        }
        .loader span:nth-child(12) {
            animation: load 2.5s 0.8s infinite linear;
        }
        .loader span:nth-child(13) {
            animation: load 2.5s 1s infinite linear;
        }
        .loader span:nth-child(14) {
            animation: load 2.5s 1.2s infinite linear;
        }
        .loader span:nth-child(15) {
            animation: load 2.5s 1.4s infinite linear;
        }
        @keyframes load {
            0% {
                background: #ccc;
                margin-top: 25%;
                height: 10%;
            }
            50% {
                background: #444;
                height: 100%;
                margin-top: 0%;
            }
            100% {
                background: #ccc;
                height: 10%;
                margin-top: 25%;
            }
        }
    </style>
</head>
<body>
<div class="window_loading" style="display: none;">
    <div class="loader">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<script src="${ctx}/resources/bootstrap/js/jquery.min.js"></script>
<script>
    $(function () {
        $(".window_loading").on("click",function (e) {
            e.stopPropagation();
        })
    })
</script>

</body>
</html>
