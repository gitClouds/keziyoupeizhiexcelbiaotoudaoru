<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <title>cx</title>
</head>
<body style="margin: 0px;padding: 0px;">
<div style="width: 800px;font-family: SimSun;padding: 96px 0;" align="center">
    <div style="border: 3px solid #000;margin-left:40px;padding: 1px;width: 608px;">
        <table style="border: 1px solid #000;width: 608px;padding: 0;">
            <tr>
                <td style="font-size: 20px;border: 0px;font-weight: bold;text-align: center;height: 32px"></td>
            </tr>
            <tr>
                <td style="font-size: 20px;border: 0px;font-weight: bold;text-align: center;height: 32px">${(dwmc)!'-'}</td>
            </tr>
            <tr>
                <td style="font-size: 36px;border: 0px;font-weight: bold;text-align: center;height: 70px;">协助查询财产通知书</td>
            </tr>
            <tr>
                <td style="font-size:16px;border: 0px;text-align: right;height: 32px">${(dwjc)!'-'}调证字[${(dzyear)!'-'}]${(dzz)!'-'}号</td>
            </tr>
            <tr>
                <td style="font-size:16px;border: 0px;text-align: left;height: 32px">
                    <p style="text-decoration:underline;margin: 0;">${(yhjg)!'-'}：</p>
                </td>
            </tr>
            <tr>
                <td style="font-size:16px;border: 0px;text-align: left;height: 32px;text-indent:2em;line-height:200%;letter-spacing: 1px;">
                    因侦查犯罪需要，根据《中华人民共和国刑事诉讼法》第一百四十二条之规定，我局派员前往你处查询犯罪嫌疑人<span style="text-decoration:underline;"> ${(shr)!'-'} </span>（性别<span style="display: inline-block;border-bottom:1px solid #000;width: 20px;"></span>出生日期<span style="display: inline-block;border-bottom:1px solid #000;width: 60px;"></span>）的财产，请予协助！
                </td>
            </tr>
            <tr>
                <td style="font-size:16px;border: 0px;text-align: left;text-indent:2em;height: 32px;">财产种类：</td>
            </tr>
            <tr>
                <td style="font-size:16px;border: 0px;text-align: left;height: 32px;text-indent:2em;line-height:200%;letter-spacing: 1px;">
                    查询线索：<span style="text-decoration:underline;">${(zhlx)!'-'}：${(zh)!'-'} 的交易明细、开户信息、联系方式。  </span>
                </td>
            </tr>
            <tr>
                <td style="font-size: 20px;border: 0px;font-weight: bold;text-align: center;height: 96px"></td>
            </tr>
            <tr>
                <td style="font-size:16px;border: 0px;text-align: right;height: 32px;padding-right: 32px;">${(dwmc)!'-'}</td>
            </tr>
            <tr>
                <td style="font-size:16px;border: 0px;text-align: right;height: 32px;padding-right: 32px;">${(nowDate)!'-'}</td>
            </tr>
            <tr>
                <td style="font-size:16px;border: 0px;text-align: right;height: 32px;padding-right: 32px;">
                    <img src="data:image/png;base64,${cxz}" width="22%" style="z-index: 99;margin-top: -70px;margin-right: 25px;transform: rotate(33deg);" />
                </td>
            </tr>
            <tr>
                <td style="font-size: 20px;border: 0px;font-weight: bold;text-align: center;height: 64px"></td>
            </tr>
        </table>
    </div>
    <div style="text-align: left;margin-left:40px;text-indent:1em;width: 608px;font-size: 12px;">
        此联交原证据持有人
    </div>
</div>
</body>
</html>
